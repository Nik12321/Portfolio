<?php

use Bitrix\Main\EventManager;

if (class_exists('bws_recount_project_prices')) {
    return;
}

class bws_recount_project_prices extends CModule
{

    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;

    var $MODULE_ID = "bws.recount.project.prices";
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    function __construct()
    {
        //Устанавливаем данные версии
        $arModuleVersion = [];
        include(__DIR__ . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        //Устанавливаем описание модуля
        $this->MODULE_NAME = GetMessage("TNA_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("TNA_DESCRIPTION");
        $this->PARTNER_NAME = GetMessage("TNA_AUTHOR");
        $this->PARTNER_URI = GetMessage("TNA_PARTNER_URI");
    }

    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
        self::InstallJsFiles();
        self::InstallAdminFiles();
        RegisterModuleDependences('main', 'OnBuildGlobalMenu', $this->MODULE_ID, 'bwsGlobalMenu', 'OnBuildGlobalMenu');
    }

    function DoUninstall()
    {
        self::UnInstallJsFiles();
        self::UnInstallAdminFiles();
        UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', $this->MODULE_ID, 'bwsGlobalMenu', 'OnBuildGlobalMenu');
        UnRegisterModule($this->MODULE_ID);
    }

    function InstallJsFiles()
    {
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $this->MODULE_ID . "/js",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/bws/" . $this->MODULE_ID,
            true, true
        );
    }

    function UnInstallJsFiles()
    {
        DeleteDirFilesEx("/bitrix/js/bws/" . $this->MODULE_ID);
    }

    function InstallAdminFiles($arParams = array())
    {
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/admin')) {
            if ($dir = opendir($p)) {
                while (false !== $item = readdir($dir)) {
                    if ($item == '..' || $item == '.') {
                        continue;
                    }
                    file_put_contents(
                        $file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $item,
                        '<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/' . $this->MODULE_ID . '/admin/' . $item . '");?>'
                    );
                }
                closedir($dir);
            }
        }
        return true;
    }

    function UnInstallAdminFiles()
    {
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/admin')) {
            if ($dir = opendir($p)) {
                while (false !== $item = readdir($dir)) {
                    if ($item == '..' || $item == '.')
                        continue;
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $item);
                }
                closedir($dir);
            }
        }
        return true;
    }

}