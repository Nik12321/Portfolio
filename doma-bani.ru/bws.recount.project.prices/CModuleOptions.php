<?php
class CModuleOptions
{
    private $moduleId = '';
    public $arCurOptionValues = [];

    public function CModuleOptions($moduleId)
    {
        $this->moduleId = $moduleId;
    }

    public function SaveOptions($arOptions)
    {
        foreach ($arOptions as $name => $value) {
            if (!is_array($value) && $name) {
                COption::SetOptionString($this->moduleId, $name, $value);
            } elseif ($name) {
                $json = json_encode($value);
                COption::SetOptionString($this->moduleId, $name, $json);
            }
        }
    }

    public function GetCurOptionValues($arOptions)
    {
        foreach ($arOptions as $params) {
            foreach ($params as $section) {
                foreach ($section as $key => $inputGroup) {
                    if ($key === "DYNAMIC_TABLE") {
                        $this->arCurOptionValues[$inputGroup["VARIABLE"]] = json_decode(COption::GetOptionString($this->moduleId, $inputGroup["VARIABLE"]));
                    } else {
                        foreach($inputGroup as $input) {
                            $this->arCurOptionValues[$input["VARIABLE"]] = COption::GetOptionString($this->moduleId, $input["VARIABLE"], $input['DEFAULT']);
                        }
                    }
                }
            }
        }
    }
}