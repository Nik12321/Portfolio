document.addEventListener("DOMContentLoaded", function () {
    const addBtns = document.querySelectorAll("[name='addNewLine']");
    addBtns.forEach(function (btn) {
        btn.addEventListener("click", function (e) {
            var table = e.target.previousElementSibling;
            if (table) {
                let trs = table.querySelectorAll("tr"),
                    countOfTrs = trs.length,
                    currentTr = trs[countOfTrs - 1],
                    countOfChilden = currentTr.children.length,
                    currentIndex = countOfChilden * countOfTrs,
                    name = currentTr.getAttribute("name"),
                    clone = currentTr.cloneNode(true);

                for (let i = 0; i < clone.children.length; i++) {
                    let child = clone.children[i].querySelector("input");
                    child.setAttribute("name", "pole_" + name + "[" + currentIndex + "]");
                    child.setAttribute("value", "");
                    currentIndex++;
                }
                insertAfter(currentTr, clone);
            }
        });
    });
});

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}