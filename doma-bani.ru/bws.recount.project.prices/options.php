<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
Loc::loadMessages(__FILE__);

// получаем идентификатор модуля
$request = HttpApplication::getInstance()->getContext()->getRequest();
$moduleId = htmlspecialchars($request['mid'] != '' ? $request['mid'] : $request['id']);

// подключаем наш модуль
Loader::includeModule($moduleId);

//Основные настройки модуля
$aTabs = [
    [
        'DIV'     => 'edit1',
        'TAB'     => "Основные настройки",
        'TITLE'   => "Настройки модуля перевода",
        'OPTIONS' => [
            [
                'yandexKey', // имя элемента формы
                "Ключ от Yandex.Cloud.API ", // поясняющий текст
                '', // значение по умолчанию «да»
                ['text', 40] // тип элемента формы — checkbox
            ]
        ]
    ],
];

//Создаем форму:
$tabControl = new CAdminTabControl('tabControl', $aTabs);
?>

<?php $tabControl->begin();?>
    <form action="<?=$APPLICATION->getCurPage(); ?>?mid=<?=$moduleId?>&lang=<?=LANGUAGE_ID?>" method="post">
        <?=bitrix_sessid_post()?>
        <?php
        foreach ($aTabs as $aTab) { // цикл по вкладкам
            if ($aTab['OPTIONS']) {
                $tabControl->beginNextTab();
                __AdmSettingsDrawList($moduleId, $aTab['OPTIONS']);
            }
        }
        $tabControl->buttons();
        ?>
        <input type="submit" name="apply" value="Сохранить настройки" class="adm-btn-save"/>
        <input type="submit" name="default" value="Установить по умолчанию"/>
    </form>

<?php $tabControl->end();?>

<?php
//Обработка данных после submit-а:
if ($request->isPost() && check_bitrix_sessid()) {
    foreach ($aTabs as $aTab) {
        foreach ($aTab['OPTIONS'] as $arOption) {
            if (!is_array($arOption)) {
                continue;
            }
            if ($arOption['note']) {
                continue;
            }
            if ($request['apply']) {
                $optionValue = $request->getPost($arOption[0]);
                if ($optionValue) {
                    $optionValue = trim($optionValue);
                    Option::set($moduleId, $arOption[0], $optionValue);
                }
            } elseif ($request['default']) {
                Option::set($moduleId, $arOption[0], $arOption[2]);
            }
        }
    }
    LocalRedirect($APPLICATION->getCurPage().'?mid='.$moduleId.'&lang='.LANGUAGE_ID);
}
?>