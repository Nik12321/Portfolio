<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
//Подключаем модуль
include($_SERVER['DOCUMENT_ROOT'] . '/PHPExcel/PHPExcel.php');
$ID = $_POST["ID"];
$iblockId = $_POST["iblockId"];
if ($iblockId && $ID) {
    $project = CIBlockElement::GetList(false, ["IBLOCK_ID" => $iblockId, "ID" => $ID], false, false, [
        "PROPERTY_FUNDAMENT_TYPE",
        "PROPERTY_KO_PERCENT",
        "PROPERTY_SQUARE_K",
        "PROPERTY_SQUARE_H",
        "PROPERTY_FUNDAMENT_N",
        "PROPERTY_BAZA_DIAM",
        "PROPERTY_BAZA_BRUS_1",
        "PROPERTY_BAZA_BRUS_2",
    ])->Fetch();

    if ($project && count($project)) {
        if (
            ($project["PROPERTY_SQUARE_K_VALUE"] || $project["PROPERTY_SQUARE_K_VALUE"] === "0")
            && ($project["PROPERTY_SQUARE_H_VALUE"] || $project["PROPERTY_SQUARE_H_VALUE"] === "0")
            && ($project["PROPERTY_FUNDAMENT_N_VALUE"] || $project["PROPERTY_FUNDAMENT_N_VALUE"] === "0")
            && ($project["PROPERTY_BAZA_DIAM_VALUE"] || $project["PROPERTY_BAZA_DIAM_VALUE"] === "0")
            && ($project["PROPERTY_BAZA_BRUS_1_VALUE"] || $project["PROPERTY_BAZA_BRUS_1_VALUE"] === "0")
            && ($project["PROPERTY_BAZA_BRUS_2_VALUE"] || $project["PROPERTY_BAZA_BRUS_2_VALUE"] === "0")
            && ($project["PROPERTY_FUNDAMENT_TYPE_VALUE"] || $project["PROPERTY_FUNDAMENT_TYPE_VALUE"] === "0")
            && ($project["PROPERTY_KO_PERCENT_VALUE"] || $project["PROPERTY_KO_PERCENT_VALUE"] === "0")
        ) {

            //Открываем файл
            $pExcel = PHPExcel_IOFactory::load("1_СЧИТАЛКА_ДЕРЕВЯШКИ_ДЛЯ_САЙТА.xlsx");
            //Выбираем лист, с которым будем работать;
            $pExcel->setActiveSheetIndex(33);
            //Получаем активный лист;
            $aSheet = $pExcel->getActiveSheet();


            //Данные с проекта + фундамент:
            $aSheet->setCellValue("J2", $project["PROPERTY_SQUARE_K_VALUE"]); //S крыши
            $aSheet->setCellValue("J3", $project["PROPERTY_SQUARE_H_VALUE"]); //S дома
            $aSheet->setCellValue("J4", $project["PROPERTY_FUNDAMENT_N_VALUE"]); //Фундамент
            $aSheet->setCellValue("J6", $project["PROPERTY_BAZA_DIAM_VALUE"]); //база диам 20
            $aSheet->setCellValue("J7", $project["PROPERTY_BAZA_BRUS_1_VALUE"]); //база диам 145х140
            $aSheet->setCellValue("J8", $project["PROPERTY_BAZA_BRUS_2_VALUE"]); //база диам 145х190

            if ($project["PROPERTY_FUNDAMENT_TYPE_VALUE"] == "Дом") {
                $aSheet->setCellValue("C5", 1); //ДОМ
                $aSheet->setCellValue("C6", 0); //БАНЯ
            } else {
                $aSheet->setCellValue("C5", 0); //ДОМ
                $aSheet->setCellValue("C6", 1); //БАНЯ
            }

            if (intval($project["PROPERTY_KO_PERCENT_VALUE"]) === 1) {
                $aSheet->setCellValue("G6", 1); //Тип 1
                $aSheet->setCellValue("G7", 0); //Тип 2
                $aSheet->setCellValue("G8", 0); //Тип 3
            } elseif (intval($project["PROPERTY_KO_PERCENT_VALUE"]) === 2) {
                $aSheet->setCellValue("G6", 0); //Тип 1
                $aSheet->setCellValue("G7", 1); //Тип 2
                $aSheet->setCellValue("G8", 0); //Тип 3
            } else {
                $aSheet->setCellValue("G6", 0); //Тип 1
                $aSheet->setCellValue("G7", 0); //Тип 2
                $aSheet->setCellValue("G8", 1); //Тип 3
            }

            //Крыша коэффициент:
            $aSheet->setCellValue("H6", COption::GetOptionString("bws.recount.project.prices", "type1")); //Тип 1
            $aSheet->setCellValue("H7", COption::GetOptionString("bws.recount.project.prices", "type2")); //Тип 2
            $aSheet->setCellValue("H8", COption::GetOptionString("bws.recount.project.prices", "type3")); //Тип 3


            ////Кровля:
            $aSheet->setCellValue("C3", COption::GetOptionString("bws.recount.project.prices", "ondulin")); //Брас м2
            $aSheet->setCellValue("E3", COption::GetOptionString("bws.recount.project.prices", "metalTile")); //Металлочерепица
            $aSheet->setCellValue("G3", COption::GetOptionString("bws.recount.project.prices", "flexible")); //Гибкая

            //Фундамент:
            $aSheet->setCellValue("D5", COption::GetOptionString("bws.recount.project.prices", "house")); //ДОМ цена за м2
            $aSheet->setCellValue("D6", COption::GetOptionString("bws.recount.project.prices", "bath")); //БАНЯ цена за м2

            //Таблица
            $tableData = [
                12 => "pineWood", //Бревно хвоя
                13 => "pineBrus", //Брус хвоя
                14 => "weedWood", //Листва бревно
                15 => "weedBrus", //Листва брус
                16 => "kedrWood", //Кедр бревно
                17 => "kedrBrus", //Кедр брус
                18 => "dry", //Камерной сушки
                19 => "clean", //Клеенка
                20 => "lamel", //2х ламельный
            ];

            $valueThatDefinePercent = $project["PROPERTY_SQUARE_H_VALUE"];
            for ($i = 12; $i <= 20; $i++) {
                $aSheet->setCellValue("D" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Full")); //Доставка-полная
                $aSheet->setCellValue("E" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Part")); //Доставка-часть
                $aSheet->setCellValue("G" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "EnterPrice")); //Цена за вход
                if ($valueThatDefinePercent < 50) {
                    $aSheet->setCellValue("I" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Proc1")); //Прибыль в %
                } elseif ($valueThatDefinePercent < 100) {
                    $aSheet->setCellValue("I" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Proc2")); //Прибыль в %
                } elseif ($valueThatDefinePercent < 150) {
                    $aSheet->setCellValue("I" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Proc3")); //Прибыль в %
                } elseif ($valueThatDefinePercent < 200) {
                    $aSheet->setCellValue("I" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Proc4")); //Прибыль в %
                } elseif ($valueThatDefinePercent < 250) {
                    $aSheet->setCellValue("I" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Proc5")); //Прибыль в %
                } elseif ($valueThatDefinePercent < 300) {
                    $aSheet->setCellValue("I" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Proc6")); //Прибыль в %
                } else {
                    $aSheet->setCellValue("I" . $i, COption::GetOptionString("bws.recount.project.prices", $tableData[$i] . "Proc7")); //Прибыль в %
                } 
            }

            //За работу цена:
            $aSheet->setCellValue("D23", COption::GetOptionString("bws.recount.project.prices", "forWorkWood")); //Бревно
            $aSheet->setCellValue("E23", COption::GetOptionString("bws.recount.project.prices", "forWorkBrus")); //Брус
            $aSheet->setCellValue("G23", COption::GetOptionString("bws.recount.project.prices", "forWorkDischarge")); //Разгрузка

            //Мелочевка:
            $aSheet->setCellValue("P12", COption::GetOptionString("bws.recount.project.prices", "vatin")); //Ватин
            $aSheet->setCellValue("P13", COption::GetOptionString("bws.recount.project.prices", "shkant")); //Шканты
            $aSheet->setCellValue("P14", COption::GetOptionString("bws.recount.project.prices", "skolz")); //Скользячки
            $aSheet->setCellValue("P15", COption::GetOptionString("bws.recount.project.prices", "plenka")); //Пленка
            $aSheet->setCellValue("P16", COption::GetOptionString("bws.recount.project.prices", "lesa")); //Леса
            $aSheet->setCellValue("P17", COption::GetOptionString("bws.recount.project.prices", "deliveryMoscow")); //доставка Москва
            $aSheet->setCellValue("P18", COption::GetOptionString("bws.recount.project.prices", "lagiAndBalki")); //Лаги и балки
            $aSheet->setCellValue("P19", COption::GetOptionString("bws.recount.project.prices", "stropila")); //Стропила, обр. и проч.

            //Сохранение файла:
            $objWriter = PHPExcel_IOFactory::createWriter($pExcel, 'Excel2007');
            $currPath = sys_get_temp_dir() . "/1_СЧИТАЛКА_ДЕРЕВЯШКИ_ДЛЯ_САЙТА.xlsx";
            $newPath = $_SERVER['DOCUMENT_ROOT'] . "/bitrix/admin/ajax/1_СЧИТАЛКА_ДЕРЕВЯШКИ_ДЛЯ_САЙТА.xlsx";
            $objWriter->save($currPath);
            if (copy($currPath, $newPath)) {
                $pExcel = PHPExcel_IOFactory::load("1_СЧИТАЛКА_ДЕРЕВЯШКИ_ДЛЯ_САЙТА.xlsx");
                //выбираем лист, с которым будем работать;
                $pExcel->setActiveSheetIndex(33);
                //получаем активный лист;
                $aSheet = $pExcel->getActiveSheet();


                $basePrice =  intval($aSheet->getCell("T1")->getCalculatedValue()); //Базовая цена
                $foundationPrice = intval($aSheet->getCell("T3")->getCalculatedValue()); //Фундамент
                $krovlya = [
                    intval($aSheet->getCell("T4")->getCalculatedValue()),
                    intval($aSheet->getCell("T5")->getCalculatedValue()),
                    intval($aSheet->getCell("T6")->getCalculatedValue()),
                    intval($aSheet->getCell("T7")->getCalculatedValue())
                ]; //Кровля

                $sosnaEl = [
                    intval($aSheet->getCell("T10")->getCalculatedValue()),
                    intval($aSheet->getCell("T11")->getCalculatedValue()),
                    intval($aSheet->getCell("T12")->getCalculatedValue()),
                    intval($aSheet->getCell("T13")->getCalculatedValue()),
                    intval($aSheet->getCell("T14")->getCalculatedValue()),
                    intval($aSheet->getCell("T15")->getCalculatedValue())
                ];

                $listvenica = [
                    intval($aSheet->getCell("T16")->getCalculatedValue()),
                    intval($aSheet->getCell("T17")->getCalculatedValue()),
                    intval($aSheet->getCell("T18")->getCalculatedValue()),
                    intval($aSheet->getCell("T19")->getCalculatedValue()),
                    intval($aSheet->getCell("T20")->getCalculatedValue()),
                    intval($aSheet->getCell("T21")->getCalculatedValue())
                ];

                $kedr = [
                    intval($aSheet->getCell("T22")->getCalculatedValue()),
                    intval($aSheet->getCell("T23")->getCalculatedValue()),
                    intval($aSheet->getCell("T24")->getCalculatedValue()),
                    intval($aSheet->getCell("T25")->getCalculatedValue()),
                    intval($aSheet->getCell("T26")->getCalculatedValue()),
                    intval($aSheet->getCell("T27")->getCalculatedValue())
                ];

                $xvoyaSize = [
                    intval($aSheet->getCell("T29")->getCalculatedValue()),
                    intval($aSheet->getCell("T30")->getCalculatedValue())
                ];

                $listvaSize = [
                    intval($aSheet->getCell("T31")->getCalculatedValue()),
                    intval($aSheet->getCell("T32")->getCalculatedValue())
                ];

                $kedrSize = [
                    intval($aSheet->getCell("T33")->getCalculatedValue()),
                    intval($aSheet->getCell("T34")->getCalculatedValue())
                ];

                $KSSize = [
                    intval($aSheet->getCell("T36")->getCalculatedValue()),
                    intval($aSheet->getCell("T37")->getCalculatedValue())
                ];

                $kleenka = [
                    intval($aSheet->getCell("T39")->getCalculatedValue()),
                    intval($aSheet->getCell("T40")->getCalculatedValue()),
                    intval($aSheet->getCell("T41")->getCalculatedValue()),
                    intval($aSheet->getCell("T42")->getCalculatedValue()),
                    intval($aSheet->getCell("T43")->getCalculatedValue()),
                    intval($aSheet->getCell("T44")->getCalculatedValue()),
                    intval($aSheet->getCell("T45")->getCalculatedValue())
                ];
                if ($basePrice && $foundationPrice) {
                    CIBlockElement::SetPropertyValuesEx($ID, $iblockId, [
                        "ATT_PRICE" => $basePrice,
                        "ATT_FUNDAMENT" => $foundationPrice,
                        "ATT_KROVLYA" => $krovlya,
                        "ATT_WOOD_SOSNA" => $sosnaEl,
                        "ATT_WOOD_LISTVENICA" => $listvenica,
                        "ATT_WOOD_KEDR" => $kedr,
                        "ATT_BRUS_NEW_NATURE_SOSNA" => $xvoyaSize,
                        "ATT_BRUS_NEW_NATURE_LISTVENICA" => $listvaSize,
                        "ATT_BRUS_NEW_NATURE_KEDR" => $kedrSize,
                        "ATT_BRUS_NEW_KAMERNIY_SOSNA" => $KSSize,
                        "ATT_BRUS_NEW_KLEENIY_SOSNA" => $kleenka
                    ]);
                    echo "success (!!!) :" . $ID;
                } else {
                    echo "error (!!!) :" . $ID;
                }
            }
        } else {
            echo "error (!!!) :" . $ID;
        }
    }
}