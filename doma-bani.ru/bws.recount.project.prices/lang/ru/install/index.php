<?php
$MESS["TNA_NAME"] = "Пересчет цен с использованием xlsl файлов";
$MESS["TNA_DESCRIPTION"] = "Использует заранее подготовленный xlsl файл, чтобы пересчитать цены проектов";
$MESS["TNA_AUTHOR"] = "Никита Тимофеев";
$MESS["TNA_PARTNER_URI"] = "https://vk.com/nikitostim";