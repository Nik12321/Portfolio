<?php
@define("MODULE_ID", basename(dirname(dirname(__FILE__))));
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Web\HttpClient;

//Подключаем параметры раздела в админке
$includeParameters = str_replace(basename(__FILE__), 'params/' . basename(__FILE__), __FILE__);
include($includeParameters);

//Подключаем класс, с помощью которого мы сохраним наши параметры и получим текущие значения
require_once(str_replace("admin/" . basename(__FILE__), "CModuleOptions.php", __FILE__));

//Подключаем класс, где получаем значения по умолчанию
$moduleOptions = new CModuleOptions(MODULE_ID);

\Bitrix\Main\UI\Extension::load("ui.progressbar");
\Bitrix\Main\UI\Extension::load("ui.alerts");

$res = false;
//Сохраняем значения, если был выполнен POST запрос. Проверяем, корректны ли значения
if ($REQUEST_METHOD == "POST" && $save != "" && check_bitrix_sessid()) {
    $errors = [];
    foreach ($arPole as $tabs) {
        foreach ($tabs as $section) {
            foreach ($section as $key => $inputGroup) {
                if ($key === "DYNAMIC_TABLE") {
                    $groups = array_chunk($_POST['pole_' . $inputGroup['VARIABLE']], ($inputGroup["LINE_SIZE"]) ? $inputGroup["LINE_SIZE"] : 1);
                    $newArray = [];
                    foreach ($groups as $groupKey => $group) {
                        if (count(array_diff($group, [''])) !== count($group)) {
                            unset($groups[$groupKey]);
                        } else {
                            $newArray = array_merge($newArray, $group);
                        }
                    }
                    $ar[$inputGroup['VARIABLE']] = $newArray;
                } else {
                    foreach ($inputGroup as $input) {
                        if ($input["VARIABLE"] !== "static") {
                            if ($input['NOTEMPTY'] == 'Y' && $_POST['pole_' . $input['VARIABLE']] === '') {
                                if (!$errors["MESSAGE"]) {
                                    $errors["MESSAGE"] = "Не заданы все табличные значения";
                                }
                            }
                            $ar[$input['VARIABLE']] = $_POST['pole_' . $input['VARIABLE']];
                        }
                    }
                }
            }
        }
    }
    if (!count($errors)) {
        $res = true;
        $moduleOptions->SaveOptions($ar);
    } else {
        $res = false;
        $errorClass = new CAdminMessage($errors);
    }
}

//Получаем значения параметров
$moduleOptions->GetCurOptionValues($arPole);

//Создаем форму:
$tabControl = new CAdminTabControl('tabControl', $aTabs);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

if ($errorClass) {
    echo $errorClass->Show();
}
?>

<script src="/bitrix/js/bws/<?= MODULE_ID ?>/dynamicFields.js"></script>

<?php if ($res) : ?>
    <?php
    $iblockId = CIBlock::GetList([], ['CODE' => "project"], false)->Fetch()["ID"];
    $number = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => $iblockId,
            "!PROPERTY_FUNDAMENT_TYPE" => false,
            "!PROPERTY_KO_PERCENT" => false,
            "!PROPERTY_SQUARE_K" => false,
            "!PROPERTY_SQUARE_H" => false,
            "!PROPERTY_FUNDAMENT_N" => false,
            "!PROPERTY_BAZA_DIAM" => false,
            "!PROPERTY_BAZA_BRUS_1" => false,
            "!PROPERTY_BAZA_BRUS_2" => false
        ],
        false,
        false,
        ["ID"]
    )->SelectedRowsCount();
    $ids = [];
    $start = 0;
    if ($iblockId && $number) {
        //Прописать извлечение элементов
        $elements = CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => $iblockId,
                "!PROPERTY_FUNDAMENT_TYPE" => false,
                "!PROPERTY_KO_PERCENT" => false,
                "!PROPERTY_SQUARE_K" => false,
                "!PROPERTY_SQUARE_H" => false,
                "!PROPERTY_FUNDAMENT_N" => false,
                "!PROPERTY_BAZA_DIAM" => false,
                "!PROPERTY_BAZA_BRUS_1" => false,
                "!PROPERTY_BAZA_BRUS_2" => false
            ],
            false,
            false,
            ["ID"]
        );
        while ($element = $elements->Fetch()) {
            $ids[] = $element["ID"];
        }
        $url = "ajax/ajaxWood.php";
        $end = $number;
    }
    ?>
    <?php if ($end && count($ids)) : ?>
        <div id="progressBarSection">
            <div class="ui-alert ui-alert-warning">
                <span class="ui-alert-message"><strong>Внимание!</strong> Не перезагружайте страницу до окончания обработки!</span>
            </div>
            <div class="ui-progressbar ui-progressbar-warning ui-progressbar-bg">
                <div class="ui-progressbar-text-before">Выполняется обработка</div>
                <div class="ui-progressbar-track">
                    <div class="ui-progressbar-bar" id="progressBarWood" style="width:0%;"></div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<?php if ($success != "") : ?>
    <div class="ui-alert ui-alert-success">
        <span class="ui-alert-message"><strong>Обработка успешно завершена!</strong></span>
    </div>
<?php endif; ?>
<form method="POST" Action="<?= $APPLICATION->GetCurPage() ?>" ENCTYPE="multipart/form-data" name="post_form">
    <?= bitrix_sessid_post() ?>
    <?php $tabControl->Begin(); ?>
    <?php foreach ($aTabs as $tab) : ?>
        <?php $tabControl->BeginNextTab(); ?>
        <?php foreach ($arPole[$tab['DIV']] as $sectionName => $section) : ?>
            <tr class="heading">
                <td colspan="100%">
                    <b>
                        <?= GetMessage($sectionName); ?>
                    </b>
                </td>
            </tr>
            <?php foreach ($section as $key => $inputGroup) : ?>
                <?php if ($key === "DYNAMIC_TABLE") : ?>
                    <tr>
                        <td colspan="100%">
                            <table width="100%">
                                <?php
                                switch ($inputGroup['TYPE']) {
                                    case 'INPUT':
                                        if ($inputGroup['SIZE'] == 'S') {
                                            $params = 'maxlength="10" size="7"';
                                        } elseif ($inputGroup['SIZE'] == 'M') {
                                            $params = 'maxlength="20" size="20"';
                                        } else {
                                            $params = 'style="width: 100%;"';
                                        }
                                        if ($ar[$inputGroup['VARIABLE']] !== NULL) {
                                            $value = $ar[$inputGroup['VARIABLE']];
                                        } elseif ($moduleOptions->arCurOptionValues[$inputGroup['VARIABLE']]) {
                                            $value = $moduleOptions->arCurOptionValues[$inputGroup['VARIABLE']];
                                        }
                                        $numberOfRows = count($value) / $inputGroup["LINE_SIZE"];
                                        if (!$numberOfRows) {
                                            $numberOfRows = 1;
                                        }
                                        $curIndex = 0;
                                        ?>
                                        <?php for ($i = 0; $i < $numberOfRows; $i++) : ?>
                                        <tr name="<?= $inputGroup['VARIABLE'] ?>">
                                            <?php for ($j = 0; $j < $inputGroup["LINE_SIZE"]; $j++) : ?>
                                                <td width="<?= 100 / $inputGroup["LINE_SIZE"] ?>%">
                                                    <div style='width: 80%'>
                                                        <input type="text"
                                                               name="pole_<?= $inputGroup['VARIABLE'] ?>[<?= $curIndex ?>]"
                                                               value="<?= $value[$curIndex] ?>"
                                                            <?= $params ?>
                                                        />
                                                    </div>
                                                </td>
                                                <?php $curIndex++; ?>
                                            <?php endfor; ?>
                                        </tr>
                                    <?php endfor; ?>
                                        <?php
                                        unset($value);
                                        unset($curIndex);
                                        break;
                                }
                                ?>
                            </table>
                            <input type="button" name="addNewLine" value="Добавить ряд" class="adm-btn-save"
                                   style="width:100%">
                        </td>
                    </tr>
                <?php else : ?>
                    <tr>
                        <?php if (count($inputGroup) > 1 && is_array($inputGroup)) : ?>
                            <td colspan="100%">
                                <table width="100%">
                                    <tr>
                                        <?php foreach ($inputGroup as $input) : ?>
                                            <td width="<?= 100 / count($inputGroup) ?>%">
                                                <?php if ($input["VARIABLE"] === "static") : ?>
                                                    <?php if ($input['NOTEMPTY'] == 'Y') : ?>
                                                        <strong>
                                                            <span class="required">*</span>
                                                            <?= $input['NAME'] ?>
                                                        </strong>
                                                    <?php else : ?>
                                                        <?= $input['NAME'] ?>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <?php
                                                    switch ($input['TYPE']) {
                                                        case 'INPUT':
                                                            if ($input['SIZE'] == 'S') {
                                                                $params = 'maxlength="10" size="7"';
                                                            } elseif ($input['SIZE'] == 'M') {
                                                                $params = 'maxlength="20" size="20"';
                                                            } else {
                                                                $params = 'style="width: 100%;"';
                                                            }

                                                            if ($ar[$input['VARIABLE']] !== NULL) {
                                                                $value = $ar[$input['VARIABLE']];
                                                            } elseif ($moduleOptions->arCurOptionValues[$input['VARIABLE']]) {
                                                                $value = $moduleOptions->arCurOptionValues[$input['VARIABLE']];
                                                            } else {
                                                                $value = $input['DEFAULT'];
                                                            }
                                                            ?>
                                                            <div style='width: 80%'>
                                                                <input type="text" <?= $params ?>
                                                                       name="pole_<?= $input['VARIABLE'] ?>"
                                                                       id="<?= $input['VARIABLE'] ?>"
                                                                       value="<?= $value ?>"/>
                                                            </div>
                                                            <?php
                                                        unset($value);
                                                            break;
                                                    }
                                                    ?>
                                                <?php endif; ?>
                                            </td>
                                        <?php endforeach; ?>
                                    </tr>
                                </table>
                            </td>
                        <?php elseif (count($inputGroup) == 1): ?>
                            <?php foreach ($inputGroup as $input) : ?>
                                <td width="28%">
                                    <?php if ($input['NOTEMPTY'] == 'Y') : ?>
                                        <strong>
                                            <span class="required">*</span>
                                            <?= $input['NAME'] ?>
                                        </strong>
                                    <?php else : ?>
                                        <?= $input['NAME'] ?>
                                    <?php endif; ?>
                                </td>
                                <td width="70%">
                                    <?
                                    switch ($input['TYPE']) {
                                        case 'INPUT':
                                            if ($input['SIZE'] == 'S') {
                                                $params = 'maxlength="10" size="7"';
                                            } elseif ($input['SIZE'] == 'M') {
                                                $params = 'maxlength="20" size="20"';
                                            } else {
                                                $params = 'style="width: 100%;"';
                                            }

                                            if ($ar[$input['VARIABLE']] !== NULL) {
                                                $value = $ar[$input['VARIABLE']];
                                            } elseif ($moduleOptions->arCurOptionValues[$input['VARIABLE']]) {
                                                $value = $moduleOptions->arCurOptionValues[$input['VARIABLE']];
                                            } else {
                                                $value = $input['DEFAULT'];
                                            }
                                            ?>
                                            <div style='width: 80%'>
                                                <input type="text" <?= $params ?> name="pole_<?= $input['VARIABLE'] ?>"
                                                       id="<?= $input['VARIABLE'] ?>" value="<?= $value ?>"/>
                                            </div>
                                            <?php
                                        unset($value);
                                            break;
                                    }
                                    ?>
                                </td>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
    <?php $tabControl->Buttons(); ?>
    <input type="submit" name="save" value="Обработка проектов" title="" class="adm-btn-save">
    <?php $tabControl->end(); ?>
    <?php $tabControl->ShowWarnings("post_frm", $errorClass, $errors); ?>
</form>
<form method="POST" Action="<?= $APPLICATION->GetCurPage() ?>" style="display: none;">
    <input type="submit" name="success" id="completeRequests">
</form>
<?php if ($res && count($ids) && $iblockId && $end) : ?>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            var progressBar = document.getElementById("progressBarWood"),
                progressBarSection = document.getElementById("progressBarSection"),
                successBtn = document.getElementById("completeRequests"),
                start = 0,
                end = <?=$end?>,
                ids = [
                    <?php foreach ($ids as $id) :?>
                    <?=$id?>,
                    <?php endforeach;?>
                ];
            let xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    if ((end - (start + 1)) == 0 && (xmlHttp.responseText === "1" || 1)) {
                        successBtn.click();
                    } else if (xmlHttp.responseText === "1" || 1) {
                        progressBar.style.width = ((100 / end) * (start + 1)) + "%";
                        start++;
                        proccesProject(ids[start]);
                    } else {
                        progressBarSection.innerHTML = "<div class=\"ui-alert ui-alert-danger\">\n" +
                            "<span class=\"ui-alert-message\"><strong>Возникла ошибка: </strong>" + xmlHttp.responseText + "</span>\n" +
                            "</div>";
                    }
                }
            };
            proccesProject(ids[start]);

            function proccesProject(id) {
                var formData = new FormData();
                formData.append("iblockId", "<?=$iblockId?>");
                formData.append("ID", id);
                formData.append("start", start);
                formData.append("end", "<?=$end?>");
                xmlHttp.open("post", "<?=$url?>");
                xmlHttp.send(formData);
            }
        });
    </script>
<?php endif; ?>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>
