<?php
@define("MODULE_ID", basename(dirname(dirname(__FILE__))));

$aTabs = [
    [
        "DIV" => "mainSettings",
        "TAB" => "Настройки",
        "TITLE" => "Настройки параметров пересчета"
    ],
];

$arPole = [
    'mainSettings' => [
        "BWS_COEFFICIENT" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Тип1",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Тип2", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Тип3",  'NOTEMPTY' => 'Y']
            ],
            [
                [
                    'VARIABLE' => 'managerType1',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerType2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerType3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ]
        ],
        "BWS_FOUNDATION" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Дом",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Баня", 'NOTEMPTY' => 'Y']
            ],
            [
                [
                    'VARIABLE' => 'managerHouse',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerBath',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ]
        ],
        "BWS_ROOF" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "BRAAS",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Металлочерепица", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Гибкая",  'NOTEMPTY' => 'Y']
            ],
            [
                [
                    'VARIABLE' => 'managerOndulin',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerMetalTile',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerFlexible',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ]
        ],
        "BWS_SMALL_THINGS" => [
            [
                [
                    'VARIABLE' => 'managerVatin',
                    'NAME' => "Ватин",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'managerShkant',
                    'NAME' => "Шканты",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'managerSkolz',
                    'NAME' => "Скользячки",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'managerPlenka',
                    'NAME' => "Пленка",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'managerLesa',
                    'NAME' => "Леса",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'managerDeliveryMoscow',
                    'NAME' => "Доставка Москва",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'managerLagiAndBalki',
                    'NAME' => "Лаги и балки",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'managerStropila',
                    'NAME' => "Стропила, обр. и проч.",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'managerLagiAndBalkiSuhie',
                    'NAME' => "Лаги и балки сухие",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
        ],
        "BWS_DELIVERY" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Доставка"],
                ['VARIABLE' => 'static', 'NAME' => "Полная", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Часть",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Цена на вход",  'NOTEMPTY' => 'Y'],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Бревно хвоя"],
                [
                    'VARIABLE' => 'managerPineWoodFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineWoodPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineWoodEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Брус хвоя"],
                [
                    'VARIABLE' => 'managerPineBrusFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineBrusPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineBrusEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Листва бревно"],
                [
                    'VARIABLE' => 'managerWeedWoodFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedWoodPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedWoodEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Листва брус"],
                [
                    'VARIABLE' => 'managerWeedBrusFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedBrusPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedBrusEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Кедр бревно"],
                [
                    'VARIABLE' => 'managerKedrWoodFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrWoodPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrWoodEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Кедр брус"],
                [
                    'VARIABLE' => 'managerKedrBrusFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrBrusPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrBrusEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Камерной сушки"],
                [
                    'VARIABLE' => 'managerDryFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerDryPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerDryEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Клеенка"],
                [
                    'VARIABLE' => 'managerCleanFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerCleanPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerCleanEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "2-х ламельный"],
                [
                    'VARIABLE' => 'managerLamelFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerLamelPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerLamelEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ]
        ],
        "BWS_PRICE_FOR_WORK" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Бревно",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Брус весь", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Разгрузка",  'NOTEMPTY' => 'Y']
            ],
            [
                [
                    'VARIABLE' => 'managerForWorkWood',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerForWorkBrus',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerForWorkDischarge',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ]
        ],
        "BWS_PROC_FOR_METER" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Доставка"],
                ['VARIABLE' => 'static', 'NAME' => "0-50м2", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "50-100м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "100-150м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "150-200м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "200-250м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "250-300м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "300-0000м2",  'NOTEMPTY' => 'Y']
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Бревно хвоя"],
                [
                    'VARIABLE' => "managerPineWoodProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineWoodProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineWoodProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineWoodProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineWoodProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineWoodProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineWoodProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Брус хвоя"],
                [
                    'VARIABLE' => "managerPineBrusProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineBrusProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineBrusProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineBrusProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineBrusProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineBrusProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerPineBrusProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Листва бревно"],
                [
                    'VARIABLE' => "managerWeedWoodProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedWoodProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedWoodProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedWoodProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedWoodProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedWoodProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedWoodProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Листва брус"],
                [
                    'VARIABLE' => "managerWeedBrusProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedBrusProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedBrusProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedBrusProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedBrusProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedBrusProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerWeedBrusProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Кедр бревно"],
                [
                    'VARIABLE' => "managerKedrWoodProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrWoodProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrWoodProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrWoodProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrWoodProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrWoodProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrWoodProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Кедр брус"],
                [
                    'VARIABLE' => "managerKedrBrusProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrBrusProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrBrusProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrBrusProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrBrusProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrBrusProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerKedrBrusProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Камерной сушки"],
                [
                    'VARIABLE' => "managerDryProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerDryProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerDryProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerDryProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerDryProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerDryProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerDryProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Клеенка"],
                [
                    'VARIABLE' => "managerCleanProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerCleanProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerCleanProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerCleanProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerCleanProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerCleanProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerCleanProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "2-х ламельный"],
                [
                    'VARIABLE' => "managerLamelProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerLamelProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerLamelProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerLamelProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerLamelProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerLamelProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'managerLamelProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ]
        ],
    ]
];