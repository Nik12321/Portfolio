<?php
@define("MODULE_ID", basename(dirname(dirname(__FILE__))));

$aTabs = [
    [
        "DIV" => "mainSettings",
        "TAB" => "Настройки",
        "TITLE" => "Настройки параметров пересчета"
    ],
];

$arPole = [
    'mainSettings' => [
        "BWS_COEFFICIENT" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Тип1",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Тип2", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Тип3",  'NOTEMPTY' => 'Y']
            ],
            [
                [
                    'VARIABLE' => 'type1',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'type2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'type3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ]
        ],
        "BWS_FOUNDATION" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Дом",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Баня", 'NOTEMPTY' => 'Y']
            ],
            [
                [
                    'VARIABLE' => 'house',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'bath',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ]
        ],
        "BWS_ROOF" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "BRAAS",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Металлочерепица", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Гибкая",  'NOTEMPTY' => 'Y']
            ],
            [
                [
                    'VARIABLE' => 'ondulin',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'metalTile',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'flexible',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ]
        ],
        "BWS_SMALL_THINGS" => [
            [
                [
                    'VARIABLE' => 'vatin',
                    'NAME' => "Ватин",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'shkant',
                    'NAME' => "Шканты",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'skolz',
                    'NAME' => "Скользячки",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'plenka',
                    'NAME' => "Пленка",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'lesa',
                    'NAME' => "Леса",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'deliveryMoscow',
                    'NAME' => "Доставка Москва",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'lagiAndBalki',
                    'NAME' => "Лаги и балки ест. вл.",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'stropila',
                    'NAME' => "Стропила, обр. и проч.",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                [
                    'VARIABLE' => 'lagiAndBalkiSuhie',
                    'NAME' => "Лаги и балки сухие",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
        ],
        "BWS_DELIVERY" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Доставка"],
                ['VARIABLE' => 'static', 'NAME' => "Полная", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Часть",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Цена на вход",  'NOTEMPTY' => 'Y'],
//                ['VARIABLE' => 'static', 'NAME' => "Прибыль в %",  'NOTEMPTY' => 'Y']
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Бревно хвоя"],
                [
                    'VARIABLE' => 'pineWoodFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineWoodPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineWoodEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'pineWoodResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Брус хвоя"],
                [
                    'VARIABLE' => 'pineBrusFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineBrusPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineBrusEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'pineBrusResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Листва бревно"],
                [
                    'VARIABLE' => 'weedWoodFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedWoodPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedWoodEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'weedWoodResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Листва брус"],
                [
                    'VARIABLE' => 'weedBrusFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedBrusPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedBrusEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'weedBrusResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Кедр бревно"],
                [
                    'VARIABLE' => 'kedrWoodFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrWoodPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrWoodEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'kedrWoodResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Кедр брус"],
                [
                    'VARIABLE' => 'kedrBrusFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrBrusPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrBrusEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'kedrBrusResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Камерной сушки"],
                [
                    'VARIABLE' => 'dryFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'dryPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'dryEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'dryResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Клеенка"],
                [
                    'VARIABLE' => 'cleanFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'cleanPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'cleanEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'cleanResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "2-х ламельный"],
                [
                    'VARIABLE' => 'lamelFull',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'lamelPart',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'lamelEnterPrice',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
//                [
//                    'VARIABLE' => 'lamelResPercent',
//                    'TYPE' => 'INPUT',
//                    'NOTEMPTY' => 'Y',
//                    'SIZE' => 'L',
//                    'DEFAULT' => 0
//                ],
            ]
        ],
        "BWS_PRICE_FOR_WORK" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Бревно",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Брус весь", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "Разгрузка",  'NOTEMPTY' => 'Y']
            ],
            [
                [
                    'VARIABLE' => 'forWorkWood',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'forWorkBrus',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'forWorkDischarge',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ]
        ],
        "BWS_PROC_FOR_INTERVAL" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Доставка"],
                ['VARIABLE' => 'static', 'NAME' => "0-50м2", 'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "50-100м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "100-150м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "150-200м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "200-250м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "250-300м2",  'NOTEMPTY' => 'Y'],
                ['VARIABLE' => 'static', 'NAME' => "300-0000м2",  'NOTEMPTY' => 'Y']
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Бревно хвоя"],
                [
                    'VARIABLE' => "pineWoodProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineWoodProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineWoodProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineWoodProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineWoodProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineWoodProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineWoodProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Брус хвоя"],
                [
                    'VARIABLE' => "pineBrusProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineBrusProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineBrusProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineBrusProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineBrusProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineBrusProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'pineBrusProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Листва бревно"],
                [
                    'VARIABLE' => "weedWoodProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedWoodProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedWoodProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedWoodProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedWoodProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedWoodProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedWoodProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Листва брус"],
                [
                    'VARIABLE' => "weedBrusProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedBrusProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedBrusProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedBrusProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedBrusProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedBrusProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'weedBrusProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Кедр бревно"],
                [
                    'VARIABLE' => "kedrWoodProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrWoodProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrWoodProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrWoodProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrWoodProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrWoodProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrWoodProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Кедр брус"],
                [
                    'VARIABLE' => "kedrBrusProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrBrusProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrBrusProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrBrusProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrBrusProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrBrusProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'kedrBrusProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Камерной сушки"],
                [
                    'VARIABLE' => "dryProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'dryProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'dryProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'dryProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'dryProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'dryProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'dryProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "Клеенка"],
                [
                    'VARIABLE' => "cleanProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'cleanProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'cleanProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'cleanProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'cleanProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'cleanProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'cleanProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ],
            [
                ['VARIABLE' => 'static', 'NAME' => "2-х ламельный"],
                [
                    'VARIABLE' => "lamelProc1",
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'lamelProc2',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'lamelProc3',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'lamelProc4',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'lamelProc5',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'lamelProc6',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ],
                [
                    'VARIABLE' => 'lamelProc7',
                    'TYPE' => 'INPUT',
                    'NOTEMPTY' => 'Y',
                    'SIZE' => 'L',
                    'DEFAULT' => 0
                ]
            ]
        ],
        "BWS_DISCOUNTS" => [
            [
                ['VARIABLE' => 'static', 'NAME' => "Нижняя граница S кв.м.",  'NOTEMPTY' => 'N'],
                ['VARIABLE' => 'static', 'NAME' => " Верхняя граница S кв.м.", 'NOTEMPTY' => 'N'],
                ['VARIABLE' => 'static', 'NAME' => "Скидка руб/кв.м.",  'NOTEMPTY' => 'N']
            ],
            "DYNAMIC_TABLE" => [
                'VARIABLE' => 'bws_discount',
                'TYPE' => 'INPUT',
                'NOTEMPTY' => 'N',
                "LINE_SIZE" => 3,
                "ADD_BTN" => "Y",
            ]
        ],
    ]
];