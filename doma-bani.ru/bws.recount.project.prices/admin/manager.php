<?php
@define("MODULE_ID", basename(dirname(dirname(__FILE__))));
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Web\HttpClient;

//Подключаем параметры раздела в админке
$includeParameters = str_replace( basename(__FILE__),'params/'. basename(__FILE__), __FILE__);
include($includeParameters);

//Подключаем класс, с помощью которого мы сохраним наши параметры и получим текущие значения
require_once(str_replace( "admin/" . basename(__FILE__),"CModuleOptions.php", __FILE__));

//Подключаем класс, где получаем значения по умолчанию
$moduleOptions = new CModuleOptions(MODULE_ID);
\Bitrix\Main\UI\Extension::load("ui.alerts");

$res = false;
//Сохраняем значения, если был выполнен POST запрос. Проверяем, корректны ли значения
if ($REQUEST_METHOD == "POST" && $save != "" && check_bitrix_sessid() ) {
    $errors = [];
    foreach ($arPole as $tabs) {
        foreach ($tabs as $section) {
            foreach ($section as $inputGroup) {
                foreach($inputGroup as $input) {
                    if ($input["VARIABLE"] !== "static") {
                        if ($input['NOTEMPTY'] == 'Y' && $_POST['pole_' . $input['VARIABLE']] === '') {
                            if (!$errors["MESSAGE"]) {
                                $errors["MESSAGE"] = "Не заданы все табличные значения";
                            }
                        }
                        $ar[$input['VARIABLE']] = $_POST['pole_'.$input['VARIABLE']];
                    }
                }
            }
        }
    }
    if (!count($errors)) {
        $res = true;
        $moduleOptions->SaveOptions($ar);
    } else {
        $res = false;
        $errorClass = new CAdminMessage($errors);
    }
}

//Получаем значения параметров
$moduleOptions->GetCurOptionValues($arPole);
//Создаем форму:
$tabControl = new CAdminTabControl('tabControl', $aTabs);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

if ($errorClass) {
    echo $errorClass->Show();
}
?>
<?php if ($res != "") :?>
    <div class="ui-alert ui-alert-success">
        <span class="ui-alert-message"><strong>Параметры сохранены!</strong></span>
    </div>
<?php endif;?>
<form method="POST" Action="<?=$APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
    <?=bitrix_sessid_post()?>
    <?php $tabControl->Begin();?>
    <?php foreach ( $aTabs as $tab ) :?>
        <?php $tabControl->BeginNextTab();?>
        <?php foreach ($arPole[$tab['DIV']] as $sectionName => $section) :?>
            <tr class="heading">
                <td colspan="100%">
                    <b>
                        <?=GetMessage($sectionName);?>
                    </b>
                </td>
            </tr>
            <?php foreach ($section as $inputGroup) :?>
                <tr>
                    <?php if (count($inputGroup) > 1 && is_array($inputGroup)) :?>
                        <td colspan="100%">
                            <table width="100%">
                                <tr>
                                    <?php foreach ($inputGroup as $input) :?>
                                        <td width="<?=100 / count($inputGroup)?>%">
                                            <?php if ($input["VARIABLE"] === "static") :?>
                                                <?php if($input['NOTEMPTY'] == 'Y') : ?>
                                                    <strong>
                                                        <span class="required">*</span>
                                                        <?=$input['NAME']?>
                                                    </strong>
                                                <?php else :?>
                                                    <?=$input['NAME']?>
                                                <?php endif;?>
                                            <?php else :?>
                                                <?php
                                                switch($input['TYPE']) {
                                                    case 'INPUT':
                                                        if ($input['SIZE'] == 'S') {
                                                            $params = 'maxlength="10" size="7"';
                                                        } elseif ($input['SIZE'] == 'M') {
                                                            $params = 'maxlength="20" size="20"';
                                                        } else {
                                                            $params = 'style="width: 100%;"';
                                                        }

                                                        if ($ar[$input['VARIABLE']] !== NULL) {
                                                            $value = $ar[$input['VARIABLE']];
                                                        } elseif ($moduleOptions->arCurOptionValues[$input['VARIABLE']]) {
                                                            $value = $moduleOptions->arCurOptionValues[$input['VARIABLE']];
                                                        } else {
                                                            $value = $input['DEFAULT'];
                                                        }
                                                        ?>
                                                        <div style='width: 80%'>
                                                            <input type="text" <?=$params?> name="pole_<?=$input['VARIABLE']?>" id="<?=$input['VARIABLE']?>" value="<?=$value?>"/>
                                                        </div>
                                                        <?php
                                                        break;
                                                }
                                                ?>
                                            <?php endif;?>
                                        </td>
                                    <?php endforeach;?>
                                </tr>
                            </table>
                        </td>
                    <?php elseif (count($inputGroup) == 1):?>
                        <?php foreach ($inputGroup as $input) :?>
                            <td width="28%">
                                <?php if($input['NOTEMPTY'] == 'Y') : ?>
                                    <strong>
                                        <span class="required">*</span>
                                        <?=$input['NAME']?>
                                    </strong>
                                <?php else :?>
                                    <?=$input['NAME']?>
                                <?php endif;?>
                            </td>
                            <td width="70%">
                                <?
                                switch($input['TYPE']) {
                                    case 'INPUT':
                                        if ($input['SIZE'] == 'S') {
                                            $params = 'maxlength="10" size="7"';
                                        } elseif ($input['SIZE'] == 'M') {
                                            $params = 'maxlength="20" size="20"';
                                        } else {
                                            $params = 'style="width: 100%;"';
                                        }

                                        if ($ar[$input['VARIABLE']] !== NULL) {
                                            $value = $ar[$input['VARIABLE']];
                                        } elseif ($moduleOptions->arCurOptionValues[$input['VARIABLE']]) {
                                            $value = $moduleOptions->arCurOptionValues[$input['VARIABLE']];
                                        } else {
                                            $value = $input['DEFAULT'];
                                        }
                                        ?>
                                        <div style='width: 80%'>
                                            <input type="text" <?=$params?> name="pole_<?=$input['VARIABLE']?>" id="<?=$input['VARIABLE']?>" value="<?=$value?>"/>
                                        </div>
                                        <?php
                                        break;
                                }
                                ?>
                            </td>
                        <?php endforeach;?>
                    <?php endif;?>
                </tr>
            <?php endforeach;?>
        <?php endforeach;?>
    <?php endforeach;?>
    <?php $tabControl->Buttons();?>
    <input type="submit" name="save" value="Сохранить параметры калькулятора" title="" class="adm-btn-save">
    <?php $tabControl->end();?>
    <?php $tabControl->ShowWarnings("post_frm", $errorClass, $errors);?>
</form>
<form method="POST" Action="<?=$APPLICATION->GetCurPage()?>" style="display: none;">
    <input type="submit" name="success" id="save">
</form>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
