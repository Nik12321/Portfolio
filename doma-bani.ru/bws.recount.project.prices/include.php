<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

//Раздел в админке
class bwsGlobalMenu
{
    function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        $MODULE_ID = basename(dirname(__FILE__));
        $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/js/bws/'.$MODULE_ID.'/moduleGlobalMenu.css');
        $aModuleMenu[] = [
            'text' => 'Пересчет цен',
            'title' => 'Пересчет цен',
            'parent_menu' => 'global_menu_services',
            'sort' => 50,
            'url' => '',
            'icon' => '',
            'page_icon' => '',
            'items_id' => $MODULE_ID."_items",
            'items' => [
                [
                    'text' => 'Деревяшки',
                    'url' => '/bitrix/admin/' . $MODULE_ID. '_' . "wood.php",
                    'sort' => '10',
                ],
                [
                    'text' => 'Менеджерский калькулятор',
                    'url' => '/bitrix/admin/' . $MODULE_ID. '_' . "manager.php",
                    'sort' => '10',
                ]
            ]
        ];
    }
}