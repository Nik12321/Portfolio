<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("ROBOTS", "noindex, nofollow");
$APPLICATION->SetTitle("Пользовательское соглашение");
$APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/agreement.php",
    "COMPONENT_TEMPLATE" => ".default"
],
    false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");