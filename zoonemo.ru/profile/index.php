<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Профиль");
global $USER;
?>
<?php if ($USER->IsAuthorized()) :?>
    <?php
    $APPLICATION->IncludeComponent("bitrix:main.profile", "userEdit", [
        "CHECK_RIGHTS" => "N",
        "SEND_INFO" => "Y",
        "SET_TITLE" => "Y",
        "USER_PROPERTY" => [],
        "USER_PROPERTY_NAME" => "",
        "COMPONENT_TEMPLATE" => "userEdit"
    ],
        false
    );
    ?>
    <h3> Мои подписки:</h3>
    <div style="border : 1px solid #e8e8e8">
        <?php
        $APPLICATION->IncludeComponent("bitrix:catalog.product.subscribe.list", "subscriptions", [
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "N",
            "LINE_ELEMENT_COUNT" => "3",
            "COMPONENT_TEMPLATE" => "subscriptions"
        ],
            false
        );
        ?>
    </div>
<?php else :?>
    <p style="color: red">Для просмотра личной страницы необходимо авторизоваться.</p>
<?php endif;?>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>