<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список заказов");
global $USER;
?>
<?if ($USER->IsAuthorized()) :?>
<?$APPLICATION->IncludeComponent(
	"custom:sale.personal.order", 
	".default", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"CUSTOM_SELECT_PROPS" => array(
		),
		"DETAIL_HIDE_USER_INFO" => array(
			0 => "0",
		),
		"DISALLOW_CANCEL" => "N",
		"HISTORIC_STATUSES" => array(
		),
		"NAV_TEMPLATE" => "",
		"ORDERS_PER_PAGE" => "5",
		"ORDER_DEFAULT_SORT" => "ID",
		"PATH_TO_BASKET" => "/shop/cart",
		"PATH_TO_CATALOG" => "/shop/",
		"PATH_TO_PAYMENT" => "/personal/order/payment/",
		"REFRESH_PRICES" => "Y",
		"RESTRICT_CHANGE_PAYSYSTEM" => array(
			0 => "0",
		),
		"SAVE_IN_SESSION" => "Y",
		"SEF_FOLDER" => "/profile/orderList/",
		"SEF_MODE" => "N",
		"SET_TITLE" => "Y",
		"STATUS_COLOR_F" => "green",
		"STATUS_COLOR_N" => "yellow",
		"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
		"COMPONENT_TEMPLATE" => ".default",
		"PROP_1" => array(
		),
		"STATUS_COLOR_FU" => "gray",
		"STATUS_COLOR_RE" => "gray",
		"STATUS_COLOR_U" => "gray",
		"STATUS_COLOR_UU" => "gray",
		"STATUS_COLOR_DG" => "gray"
	),
	false
);?><br>
<?else :?>
    <p style="color: red">Для просмотра личной страницы необходимо авторизоваться.</p>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>