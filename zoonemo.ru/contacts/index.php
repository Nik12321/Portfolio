<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
$code=$APPLICATION->CaptchaGetCode();
?>
    <h1 class="page_title">Контакты</h1>
    <div class="spiski_pzf">
        <div class="contacts">
            <div class="row">
                <div class="col-md-7">
                    <div class="item"><span class="title">Интернет-магазин</span>
                        <h4>Режим работы</h4>
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/schedule.php",
                                "COMPONENT_TEMPLATE" => ".default"
                            ],
                            false
                        );
                        ?>
                        <ul class="contacts_list">
                            <?php
                            $APPLICATION->IncludeComponent("bitrix:main.include", "phone_number_detail_1", [
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "standard.php",
                                    "PATH" => "/include/phoneNumberDetail1.php",
                                    "COMPONENT_TEMPLATE" => "phone_number_detail_1"
                                ],
                                false
                            );
                            $APPLICATION->IncludeComponent("bitrix:main.include", "email_detail", [
                                "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",	// Шаблон области по умолчанию
                                "PATH" => "/include/emailDetail.php",	// Путь к файлу области
                                "COMPONENT_TEMPLATE" => ".default"
                            ],
                                false
                            );
                            $APPLICATION->IncludeComponent("bitrix:main.include", "skype_detail", [
                                "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",	// Шаблон области по умолчанию
                                "PATH" => "/include/skypeDetail.php",	// Путь к файлу области
                                "COMPONENT_TEMPLATE" => ".default"
                            ],
                                false
                            );
                            $APPLICATION->IncludeComponent("bitrix:main.include", "whatsAppDetail", [
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/whatsappDetail.php",
                                "COMPONENT_TEMPLATE" => ".default"
                            ],
                                false
                            );
                            ?>
                        </ul>
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/detailSchedule.php",
                                "COMPONENT_TEMPLATE" => ".default"
                            ],
                            false
                        );
                        ?>
                        <a class="s_btn" href="/delivery/">Условия доставки и оплаты</a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="item"><span class="title">Обратная связь</span>
                        <div class="res1">
                            <form class="contact1" action="/ajax/feedback.php" method="post">
                                <input name="f" type="hidden" value="contacts" />
                                <label for="name">Ваше имя</label>
                                <input id="name" name="name" type="text" />
                                <label for="phone">Ваш телефон:</label>
                                <input id="phone" name="phone" type="tel" class="phoneContactsInput"/>
                                <input type="hidden" id="captcha_sid_contact" name="captcha_sid_contact" value="<?=$code;?>" />
                                <img id="CaptchaImageContact" src="/bitrix/tools/captcha.php?captcha_sid=<?=$code;?>" alt="CAPTCHA" />
                                <div id="captcha_control_contact"></div>
                                <input type="text" name="captcha_contact" placeholder="Текст с картинки" />
                                <label for="text"> Ваше сообщение</label>
                                <textarea id="text" rows="13" name="comment"></textarea>

                                <input class="s_btn type2" type="submit" value="Отправить" />
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                    jQuery(function($){
                        $('.phoneContactsInput').mask('+7 (999) 999-99-99');
                        $('.phoneContactsInput').on("keyup", function(event) {
                            var value = $(event.target).val();
                            if (value[4] === "7" && value[0] == "+") {
                                $('.phoneContactsInput').unmask();
                                $('.phoneContactsInput').mask('+7 (999) 999-99-99');
                                $('.phoneContactsInput').focus();
                            }
                            if (value[4] === "8" && value[0] == "+") {
                                $('.phoneContactsInput').unmask();
                                $('.phoneContactsInput').mask('8 (999) 999-99-99');
                                $('.phoneContactsInput').focus();
                            }
                            if (value[3] === "7" && value[0] == "8") {
                                $('.phoneContactsInput').unmask();
                                $('.phoneContactsInput').mask('+7 (999) 999-99-99');
                                $('.phoneContactsInput').focus();
                            }
                            if (value[3] === "8" && value[0] == "8") {
                                $('.phoneContactsInput').unmask();
                                $('.phoneContactsInput').mask('8 (999) 999-99-99');
                                $('.phoneContactsInput').focus();
                            }
                        });
                    });
                </script>
            </div>
            <h2 class="page_title">Наши розничные магазины</h2>
            <div class="col-md-6">
                <div class="item"><span class="title">Зоомагазин М. ВДНХ</span>
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/address1.php",
                            "COMPONENT_TEMPLATE" => ".default"
                        ],
                        false
                    );
                    $APPLICATION->IncludeComponent("bitrix:main.include", "phone_number_detail_2", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/phoneNumberDetail2.php",
                            "COMPONENT_TEMPLATE" => "phone_number_detail_2"
                        ],
                        false
                    );
                    ?>
                    <div class="to_road">
                        <p>Как добраться? <a class="b_btn reverse collapse type2" href="#">Развернуть</a></p>
                    </div>
                    <div class="road">
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/road1.php",
                            "COMPONENT_TEMPLATE" => ".default"
                        ],
                            false
                        );
                        ?>
                    </div>
                </div>
                <div class="magazine_foto">
                </div>
            </div>
            <div class="col-md-6">
                <div class="item"><span class="title">Зоомагазин м. Преображенская Площадь</span>
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/address2.php",
                            "COMPONENT_TEMPLATE" => ".default"
                        ],
                        false
                    );
                    $APPLICATION->IncludeComponent("bitrix:main.include", "phone_number_detail_2", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/phoneNumberDetail3.php",
                            "COMPONENT_TEMPLATE" => "phone_number_detail_2"
                        ],
                        false
                    );
                    ?>
                    <div class="to_road">
                        <p>Как добраться? <a class="b_btn reverse collapse type2" href="#">Развернуть</a></p>
                    </div>
                    <div class="road">
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/road2.php",
                                "COMPONENT_TEMPLATE" => ".default"
                            ],
                            false
                        );
                        ?>
                    </div>
                </div>
                <div class="magazine_foto">
                </div>
                <div class="col-md-6">
                    <div class="item">
                        <span class="title">Зоомагазин м. Выхино</span>
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/address3.php",
                                "COMPONENT_TEMPLATE" => ".default"
                            ],
                            false
                        );
                        $APPLICATION->IncludeComponent("bitrix:main.include", "phone_number_detail_2", [
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/phoneNumberDetail4.php",
                                "COMPONENT_TEMPLATE" => "phone_number_detail_2"
                            ],
                            false
                        );
                        ?>
                        <div class="to_road">
                            <p>Как добраться? <a class="b_btn reverse collapse type2" href="#">Развернуть</a></p>
                        </div>
                        <div class="road">
                            <?php
                            $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "standard.php",
                                    "PATH" => "/include/road3.php",
                                    "COMPONENT_TEMPLATE" => ".default"
                                ],
                                false
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>