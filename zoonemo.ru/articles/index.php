<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Полезная информация");
$APPLICATION->SetTitle("Полезная информация");
$APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "standard.php",
        "PATH" => "/include/articles.php",
        "COMPONENT_TEMPLATE" => ".default"
    ],
    false
);
$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "usefulInformation", [
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COUNT_ELEMENTS" => "N",
        "FILTER_NAME" => "sectionsFilter",
        "IBLOCK_ID" => "17",
        "IBLOCK_TYPE" => "Useful_information",
        "SECTION_CODE" => "",
        "SECTION_FIELDS" => [
            0 => "TIMESTAMP_X",
            1 => "",
        ],
        "SECTION_ID" => $_REQUEST["SECTION_CODE"],
        "SECTION_URL" => "",
        "SECTION_USER_FIELDS" => [
            0 => "",
            1 => "",
        ],
        "SHOW_PARENT_NAME" => "Y",
        "TOP_DEPTH" => "1",
        "VIEW_MODE" => "LINE",
        "COMPONENT_TEMPLATE" => "usefulInformation",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO"
    ],
    false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");