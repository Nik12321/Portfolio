<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Изготовление аквариумов");
?>
    <h1 class="page_title">Аквариумы на заказ по индивидуальным размерам</h1>
<?php
$APPLICATION->IncludeComponent("bitrix:main.include", "", [
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/makingAquariums.php"
    ]
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>