<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Фотогалерея"); ?>
<?
$colors = array("#000000", "#d522f0", "#008000");
$color = $colors[0];
CModule::IncludeModule("fileman");
CMedialib::Init();
require_once 'MedialibCollectionTools.php';
$arCollections = Array();
$collection_id = 8;
$allIndex[] = array();
MedialibCollectionTools::GetRecursive($collection_id,$arCollections,Array('NAME'=>'ASC'),Array('ACTIVE' => 'Y'), $allIndex);
$allCollections = CMedialibCollection::GetList(array('arOrder'=>Array('NAME'=>'ASC'),'arFilter' => array('ACTIVE' => 'Y')));
$indexOfMainCollection = 0;
?>
<h1 class="page_title">Фотогалерея</h1>
<div class="spiski_pzf">

    <? foreach($allIndex as $index) : ?>
        <?
        $arItems = CMedialibItem::GetList(array('arCollections' => array($index), 'minId' => $index ));
        $obj = MedialibCollectionTools::searchItem($allCollections, $index);
        ?>
        <?if ($obj["PARENT_ID"] != 0) : ?>
            <div>
        <span style="font-size: small">
        <?if ($obj["PARENT_ID"] == $indexOfMainCollection) : ?>
        <?$color=next($colors);?>
                <strong>
                    <span style="color: #000000"><?=$obj["NAME"];?></span>
                </strong>
        <?else :?>
            <span style="<?='color: ' . $color;?>"><?=$obj["NAME"];?></span>
        <?endif;?>
            </span>
            </div>
            <div>
                <br>
            </div>
            <div>
                <div class="photoalbum">
                    <?foreach($arItems as $item) : ?>
                        <?php
                        $tempFile = $_SERVER['DOCUMENT_ROOT'] . "/upload/smallCache/" . "small" .  $item["NAME"];
                        $photo= CFile::ResizeImageFile(
                            ($_SERVER['DOCUMENT_ROOT']. $item["PATH"]),
                            $tempFile,
                            array("width" => 100, "height" => 100),
                            BX_RESIZE_IMAGE_EXACT,
                            false,
                            100);
                        ?>
                       <a href="<?=$item["PATH"];?>">
                           <img src="<?="/upload/smallCache/" . "small" .  $item["NAME"];?>" alt="">
                       </a>
                    <?endforeach;?>
                </div>
            </div>
            <div>
                <br>
            </div>
        <? else : $indexOfMainCollection = $obj["ID"];?>
        <?endif;?>
    <?endforeach;?>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>