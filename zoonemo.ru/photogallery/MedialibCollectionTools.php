<?php
class MedialibCollectionTools {
    function GetRecursive($collection_id=0,&$arCollections,$arOrder,$arFilter, &$allIndex) {
        $allIndex[] = $collection_id;
        $arFilterTmp=$arFilter;
        $arFilterTmp["PARENT_ID"] = $collection_id;
        $rsCollections = CMedialibCollection::GetList(array('arOrder'=>$arOrder,'arFilter' => $arFilterTmp));

        foreach($rsCollections as $arCollection) {
            $arCollections[] = $arCollection;
            self::GetRecursive($arCollection["ID"],$arCollections,$arOrder,$arFilter, $allIndex);
        }

    }
    function searchItem($arCollections, $index) {
        foreach($arCollections as $item) {
            if ($item["ID"] == $index)
                return $item;
            else
                continue;
        }
        return 0;
    }
}