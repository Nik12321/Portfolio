<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->IncludeComponent("bitrix:news.list", "mainSlider", [
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "ADD_SECTIONS_CHAIN" => "Y",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "36000000",
    "CACHE_TYPE" => "A",
    "CHECK_DATES" => "Y",
    "DETAIL_URL" => "",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "FIELD_CODE" => [
        0 => "",
        1 => "",
    ],
    "FILTER_NAME" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "IBLOCK_ID" => "3",
    "IBLOCK_TYPE" => "Sliders",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
    "INCLUDE_SUBSECTIONS" => "Y",
    "MESSAGE_404" => "",
    "NEWS_COUNT" => "15",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => ".default",
    "PAGER_TITLE" => "Новости",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "PREVIEW_TRUNCATE_LEN" => "10",
    "PROPERTY_CODE" => [
        0 => "SUB_TITLE",
        1 => ""
    ],
    "SET_BROWSER_TITLE" => "N",
    "SET_LAST_MODIFIED" => "N",
    "SET_META_DESCRIPTION" => "N",
    "SET_META_KEYWORDS" => "N",
    "SET_STATUS_404" => "Y",
    "SET_TITLE" => "N",
    "SHOW_404" => "N",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_BY2" => "SORT",
    "SORT_ORDER1" => "DESC",
    "SORT_ORDER2" => "ASC",
    "STRICT_SECTION_CHECK" => "N",
    "COMPONENT_TEMPLATE" => "mainSlider",
    "COMPOSITE_FRAME_MODE" => "A",
    "COMPOSITE_FRAME_TYPE" => "AUTO"
],
    false
);
?>
    <div class="menu_bar">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mobile-none">
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:menu", "mainMenuPreview", [
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "MainMenu",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => [],
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "MainMenuPreview",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "mainMenuPreview"
                    ],
                        false
                    );
                    ?>
                </div>
                <div class="col-md-12 mobile-display">
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:menu", "mainMenuPreviewMobile", [
                        "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "MainMenu",    // Тип меню для остальных уровней
                        "DELAY" => "N",    // Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "2",    // Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                        "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "Y",    // Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                        "ROOT_MENU_TYPE" => "MainMenuPreviewMobile",    // Тип меню для первого уровня
                        "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "COMPONENT_TEMPLATE" => "mainMenuPreviewMobile"
                    ],
                        false
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--Конец вывода-->

    <!--Вывод категорий магазина для PC версии-->
<?php
$APPLICATION->IncludeComponent("bitrix:menu", "shopMenuPreview", [
    "ALLOW_MULTI_SELECT" => "N",
    "CHILD_MENU_TYPE" => "Main_shop_menu_preview",
    "DELAY" => "N",
    "MAX_LEVEL" => "1",
    "MENU_CACHE_GET_VARS" => [],
    "MENU_CACHE_TIME" => "3600",
    "MENU_CACHE_TYPE" => "Y",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "ROOT_MENU_TYPE" => "MainShopMenuPreview",
    "USE_EXT" => "N",
    "COMPONENT_TEMPLATE" => "shopMenuPreview",
    "COMPOSITE_FRAME_MODE" => "Y",
    "COMPOSITE_FRAME_TYPE" => "STATIC"
],
    false
);
$APPLICATION->IncludeComponent("bitrix:menu", "submenuShopPreview", [
    "ALLOW_MULTI_SELECT" => "N",
    "CHILD_MENU_TYPE" => "left",
    "DELAY" => "N",
    "MAX_LEVEL" => "1",
    "MENU_CACHE_GET_VARS" => [],
    "MENU_CACHE_TIME" => "3600",
    "MENU_CACHE_TYPE" => "Y",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "ROOT_MENU_TYPE" => "MainShopSubmenuPreview",
    "USE_EXT" => "N",
    "COMPONENT_TEMPLATE" => "submenuShopPreview"
],
    false
);
?>
    <!--Конец вывода-->
    <div class="container">
        <div class="row">
            <div class="col-md-12 last_news">
                <?php
                $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/include/mainPageDescription.php",
                    "COMPONENT_TEMPLATE" => ".default"
                ],
                    false
                );
                ?>
            </div>
        </div>
    </div>
    <!--Поиск по магазину-->

    <h3>Популярные товары</h3>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="search_bar">
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:catalog.search", "find_products", [
                        "ACTION_VARIABLE" => "action",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BASKET_URL" => "",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "N",
                        "CONVERT_CURRENCY" => "N",
                        "DETAIL_URL" => "#SITE_DIR#",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_COMPARE" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "HIDE_NOT_AVAILABLE" => "L",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "L",
                        "IBLOCK_ID" => "13",
                        "IBLOCK_TYPE" => "Catalogs",
                        "LINE_ELEMENT_COUNT" => "3",
                        "NO_WORD_LOGIC" => "N",
                        "OFFERS_CART_PROPERTIES" => [],
                        "OFFERS_FIELD_CODE" => [
                            0 => "",
                            1 => "",
                        ],
                        "OFFERS_LIMIT" => "5",
                        "OFFERS_PROPERTY_CODE" => [
                            0 => "",
                            1 => "",
                        ],
                        "OFFERS_SORT_FIELD" => "sort",
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Товары",
                        "PAGE_ELEMENT_COUNT" => "15",
                        "PRICE_CODE" => [],
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPERTIES" => "",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PROPERTY_CODE" => [
                            0 => "",
                            1 => "ARTNUMBER",
                            2 => "",
                        ],
                        "RESTART" => "N",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_URL" => "#SITE_DIR#",
                        "SHOW_PRICE_COUNT" => "1",
                        "USE_LANGUAGE_GUESS" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "COMPONENT_TEMPLATE" => "find_products",
                        "COMPOSITE_FRAME_MODE" => "Y",
                        "COMPOSITE_FRAME_TYPE" => "STATIC"
                    ],
                        false
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!--Конец вывода-->

    <!--Вывод популярных товаров-->
    <div class="container">
        <?php
        $intSectionID = $APPLICATION->IncludeComponent("bitrix:catalog.section", "mostPopularProducts", [
            "ACTION_VARIABLE" => "action",
            "ADD_PICT_PROP" => "-",
            "ADD_PROPERTIES_TO_BASKET" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/personal/basket.php",
            "BROWSER_TITLE" => "-",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "N",
            "COMPATIBLE_MODE" => "Y",
            "CONVERT_CURRENCY" => "N",
            "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
            "DETAIL_URL" => "",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_COMPARE" => "N",
            "DISPLAY_TOP_PAGER" => "Y",
            "ELEMENT_SORT_FIELD" => "shows",
            "ELEMENT_SORT_ORDER" => "desc",
            "ELEMENT_SORT_FIELD2" => "",
            "ELEMENT_SORT_ORDER2" => "",
            "ENLARGE_PRODUCT" => "STRICT",
            "FILTER_NAME" => "arrFilter",
            "HIDE_NOT_AVAILABLE" => "N",
            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
            "IBLOCK_ID" => "13",
            "IBLOCK_TYPE" => "Catalogs",
            "INCLUDE_SUBSECTIONS" => "A",
            "LABEL_PROP" => [],
            "LAZY_LOAD" => "N",
            "LINE_ELEMENT_COUNT" => "3",
            "LOAD_ON_SCROLL" => "N",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_FIELD_CODE" => [
                0 => "",
                1 => "",
            ],
            "OFFERS_LIMIT" => "5",
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER" => "asc",
            "OFFERS_SORT_ORDER2" => "desc",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Товары",
            "PAGE_ELEMENT_COUNT" => "18",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => [0 => "cost"],
            "PRICE_VAT_INCLUDE" => "N",
            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
            "PRODUCT_DISPLAY_MODE" => "N",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "PROPERTY_CODE_MOBILE" => [],
            "SECTION_CODE" => "",
            "SECTION_ID" => "9047",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "SECTION_URL" => "",
            "SECTION_USER_FIELDS" => [],
            "SEF_MODE" => "N",
            "SET_BROWSER_TITLE" => "Y",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SHOW_ALL_WO_SECTION" => "N",
            "SHOW_CLOSE_POPUP" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_FROM_SECTION" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "SHOW_SLIDER" => "N",
            "SLIDER_INTERVAL" => "3000",
            "SLIDER_PROGRESS" => "N",
            "TEMPLATE_THEME" => "blue",
            "USE_ENHANCED_ECOMMERCE" => "N",
            "USE_MAIN_ELEMENT_SECTION" => "N",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "COMPONENT_TEMPLATE" => "mostPopularProducts"
        ],
            false
        );
        ?>
    </div>
    <!--Конец вывода-->

    <!--Вывод последних новостей и виджетов-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="last_news">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="title">
                                <span>Последние новости</span>
                                <a href="/news/">Читать все новости</a>
                            </div>
                            <?php
                            $APPLICATION->IncludeComponent("bitrix:news.list", "lastNews", [
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "Y",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "N",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => [
                                    0 => "",
                                    1 => "",
                                ],
                                "FILTER_NAME" => "",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "6",
                                "IBLOCK_TYPE" => "News",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "4",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => [
                                    0 => "",
                                    1 => "",
                                ],
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "Y",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_BY2" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "DESC",
                                "STRICT_SECTION_CHECK" => "N",
                                "COMPONENT_TEMPLATE" => "lastNews",
                                "COMPOSITE_FRAME_MODE" => "A",
                                "COMPOSITE_FRAME_TYPE" => "AUTO",
                                "TEMPLATE_THEME" => "blue",
                                "MEDIA_PROPERTY" => "",
                                "SLIDER_PROPERTY" => "",
                                "SEARCH_PAGE" => "/search/",
                                "USE_RATING" => "N",
                                "USE_SHARE" => "N"
                            ],
                                false
                            );
                            ?>
                        </div>
                        <div class="col-lg-6">
                            <noindex>
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Group("vk_groups", {
                                        mode: 0,
                                        width: "250",
                                        height: "320",
                                        color1: 'FFFFFF',
                                        color2: '2B587A',
                                        color3: '5B7FA6'
                                    }, 52804999);
                                    VK.Widgets.Group("vk_groups", {
                                        mode: 0,
                                        width: "250",
                                        height: "320",
                                        color1: 'FFFFFF',
                                        color2: '2B587A',
                                        color3: '5B7FA6'
                                    }, 52784757);
                                </script>
                            </noindex>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Конец вывода-->


    <!--Вывод сервисных центров-->
<?php
$APPLICATION->IncludeComponent("bitrix:news.list", "serviceCentres", [
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "ADD_SECTIONS_CHAIN" => "Y",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "36000000",
    "CACHE_TYPE" => "A",
    "CHECK_DATES" => "Y",
    "DETAIL_URL" => "",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "FIELD_CODE" => [
        0 => "",
        1 => "",
    ],
    "FILTER_NAME" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "IBLOCK_ID" => "15",
    "IBLOCK_TYPE" => "Service_centres",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
    "INCLUDE_SUBSECTIONS" => "Y",
    "MESSAGE_404" => "",
    "NEWS_COUNT" => "20",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => ".default",
    "PAGER_TITLE" => "Новости",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "PREVIEW_TRUNCATE_LEN" => "",
    "PROPERTY_CODE" => [
        0 => "LINK_TO_ANOTHER_SITE",
        1 => "",
    ],
    "SET_BROWSER_TITLE" => "Y",
    "SET_LAST_MODIFIED" => "N",
    "SET_META_DESCRIPTION" => "Y",
    "SET_META_KEYWORDS" => "Y",
    "SET_STATUS_404" => "Y",
    "SET_TITLE" => "N",
    "SHOW_404" => "N",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_BY2" => "SORT",
    "SORT_ORDER1" => "DESC",
    "SORT_ORDER2" => "ASC",
    "STRICT_SECTION_CHECK" => "N",
    "COMPONENT_TEMPLATE" => "serviceCentres",
    "COMPOSITE_FRAME_MODE" => "Y",
    "COMPOSITE_FRAME_TYPE" => "AUTO"
],
    false
);
?>
    <!--Конец вывода-->

<?php
$APPLICATION->SetPageProperty("description", "Интернет-магазин аквариумов и аквариумного оборудования Немо. Доставка по Москве товаров для кошек, собак, птиц. Ветеринарная аптека и витамины по доступным ценам. Оборудование для груминга. ");
$APPLICATION->SetPageProperty("title", "Магазин аквариумистики и аквариумного оборудования | Зоомагазин Немо: аквариумистика, товары для кошек и собак, террариумистика");
$APPLICATION->SetPageProperty("keywords", "магазин аквариумистики, оборудование для аквариума, товары для аквариума, товары для собак, витамины для собак");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>