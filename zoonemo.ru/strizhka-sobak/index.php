<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Стрижка собак");
$APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "standard.php",
    "PATH" => "/include/strizhka-sobak.php",
    "COMPONENT_TEMPLATE" => ".default"
],
    false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");

