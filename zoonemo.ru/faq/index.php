<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty('title',"Вопрос-ответ");
CModule::IncludeModule("iblock");
$code = $APPLICATION->CaptchaGetCode();
$filter =  ['IBLOCK_CODE' => 'FAQ', "ACTIVE" => "Y", "ACTIVE_DATE"=>"Y", "PROPERTY_CONFIRMATION" => "1"];
if (!$_GET["ELEMENT_CODE"]) {
    $cnt = CIBlockElement::GetList([], $filter, [], false, ['ID', 'NAME']);
    $navPageCount = $cnt / 10;
    if (!is_integer($navPageCount)) {
        $navPageCount = intval($navPageCount) + 1;
    }
    $navResult = new CDBResult();
    $navResult->NavPageCount = $navPageCount;  // Общее количество страниц
    if ($_GET["PAGEN_1"]) {
        $navResult->NavPageNomer = $_GET["PAGEN_1"];
    } else {
        $navResult->NavPageNomer = 1;
    }
    $navResult->NavNum = 1; //Номер пагинатора. Используется для формирования ссылок на страницы
    $navResult->NavPageSize = 10; //Количество записей выводимых на одной странице
    $navResult->NavRecordCount = $cnt; //Общее количество записей  PAGE_ELEMENT_COUNT
}
?>
    <h1 class="page_title">Вопрос-ответ</h1>
<?php if(!$_GET["ELEMENT_CODE"]) :?>
    <div class="spiski_pzf">
    <div class="faq_form">
        <div class="res1">
            <div class="">
                <a href="#" class="b_btn collapse">Развернуть форму</a>
                <p>Вы можете задать любой вопрос на тему нашей продукции или работы интернет магазина.<br>
                    Мы постараемся ответить на него как можно быстрее и подробнее.</p>
            </div>
            <form action="/sendFAQ.php" method="POST" class="collapsed faq" id="collapse">
                <div class="row">
                    <div class="col-md-6 col-md-push-6">
                        <label for="title">Тема</label>
                        <input type="text" name="title" value="" />
                        <label for="name">Ваше имя</label>
                        <input type="text" name="name" value="" />
                        <label for="email">Ваш Email: (Не публикуется)</label>
                        <input type="text" name="email" value="" />
                    </div>
                    <div class="col-md-6 col-md-pull-6">
                        <label for="text"> Ваше сообщение</label>
                        <textarea name="text" rows="13"></textarea>
                        <input type="text" name="captcha" placeholder="Текст с картинки" />
                        <div class="captcha">
                            <input type="hidden" id="captcha_sid" name="captcha_sid" value="<?=$code;?>" />
                            <img id="CaptchaImage" src="/bitrix/tools/captcha.php?captcha_sid=<?=$code;?>" alt="CAPTCHA" />
                            <div id="captcha_control"></div>
                        </div>
                        <input type="submit" value="Задать вопрос" class="s_btn type2" />
                    </div>
                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </div>
    <h2 class="page_title">Вопрос-ответ</h2>
<?php endif;?>
<?php
$APPLICATION->IncludeComponent(
    "bitrix:news",
    "detail_FAQ",
    [
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BROWSER_TITLE" => "-",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
        "DETAIL_DISPLAY_TOP_PAGER" => "N",
        "DETAIL_FIELD_CODE" => [
            0 => "",
            1 => "",
        ],
        "DETAIL_PAGER_SHOW_ALL" => "Y",
        "DETAIL_PAGER_TEMPLATE" => "",
        "DETAIL_PAGER_TITLE" => "Страница",
        "DETAIL_PROPERTY_CODE" => [
            0 => "CONFIRMATION",
            1 => "",
        ],
        "DETAIL_SET_CANONICAL_URL" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "IBLOCK_ID" => "7",
        "IBLOCK_TYPE" => "Contact_with_user",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "LIST_FIELD_CODE" => [
            0 => "DATE_CREATE",
            1 => "",
        ],
        "LIST_PROPERTY_CODE" => [
            0 => "NAME",
            1 => "",
        ],
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "NEWS_COUNT" => "10",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PREVIEW_TRUNCATE_LEN" => "",
        "SEF_FOLDER" => "/faq/",
        "SEF_MODE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_STATUS_404" => "Y",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "[object Object]",
        "STRICT_SECTION_CHECK" => "N",
        "USE_CATEGORIES" => "N",
        "USE_FILTER" => "N",
        "USE_PERMISSIONS" => "N",
        "USE_RATING" => "N",
        "USE_REVIEW" => "N",
        "USE_RSS" => "N",
        "USE_SEARCH" => "N",
        "USE_SHARE" => "N",
        "COMPONENT_TEMPLATE" => "detail_FAQ",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "VARIABLE_ALIASES" => [
            "SECTION_ID" => "SECTION_ID",
            "ELEMENT_ID" => "ELEMENT_CODE",
        ]
    ],
    false
);
?>
    </div>
    <div class="pagination">
        <?php
        if ($navPageCount > 1) {
            $APPLICATION->IncludeComponent("bitrix:system.pagenavigation", "customPagination", [
                "NAV_RESULT" => $navResult,
                "COMPONENT_TEMPLATE" => "customPagination",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO"
            ],
                false
            );
        }
        ?>
    </div>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>