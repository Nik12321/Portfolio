<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$codeLength = intval($_POST["length"]);
$phone = $_POST["phone"];
if ($phone && $codeLength) {
    $phone = str_replace(array("(", ")", "+7", " ", "-"), array("", "","7", "", ""), $_POST["phone"]);
    $phone[0] = "7";
    $startInterval = "1";
    $endInterval = "9";
    for($i = 1; $i < $codeLength; $i++) {
        $startInterval .= "0";
        $endInterval .= "9";
    }
    $code = rand(intval($startInterval), intval($endInterval));
    $success = "Y";
    $body = file_get_contents("https://sms.ru/sms/send?api_id=0B5EE528-4C43-3480-ED9F-C9230497B27A&to=" . $phone . "&msg=".urlencode("Код подтверждения: " . $code)."&json=1");
    echo $code;
}
else {
    echo "failure";
}
