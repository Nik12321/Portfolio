<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$cost = $_POST["costs"]; //цены на все доступные тп
$id = $_POST["ids"]; //id всех тп
$arResult = array();
$arResult["OFFERS"] = array();
$parentID = "";
if(CModule::IncludeModule("iblock") && CModule::IncludeModule("catalog")) {
    foreach($id as $id_s) {
        $element = CIBlockElement::GetList(Array(), Array("IBLOCK_CODE"=>"catalog_offers", "ID"=>$id_s), false, array("*", "DETAIL_PAGE_URL"));
        if($arItem = $element->GetNextElement()) {
            if (!$parentID) {
                $parentID = CCatalogSku::GetProductInfo($id_s);
                $parentElement = CIBlockElement::GetList(Array(), Array("IBLOCK_CODE"=>"catalog", "ID"=>$parentID["ID"]), false, array("*", "DETAIL_PAGE_URL"));
                $parentFields = $parentElement->GetNextElement();
                $arResult = $parentFields->GetFields();
                $arResult["PROPERTIES"] = $parentFields->GetProperties();
            }
            $fields = $arItem->GetFields();
            $arResult["OFFERS"][$fields["ID"]] = $fields;
            $arResult["OFFERS"][$fields["ID"]]["PROPERTIES"] = $arItem->GetProperties();
        }
        else {
            $element = CIBlockElement::GetList(Array(), Array("IBLOCK_CODE"=>"catalog", "ID"=>$id_s), false, array("*", "DETAIL_PAGE_URL"));
            $arItem = $element->GetNextElement();
            $fields = $arItem->GetFields();
            $arResult = $fields;
            $arResult["PROPERTIES"] = $arItem->GetProperties();
            unset( $arResult["PROPERTIES"]["STATUS_GROUP"]);
        }
    }
    if (count($arResult["OFFERS"]) > 1) {
        unset($arResult["PROPERTIES"]["article"]);
        unset($arResult["PROPERTIES"]["photoName"]);
        unset($arResult["PROPERTIES"]["recommended"]);
        if (intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) !== -1) {
            unset($arResult["PROPERTIES"]["status"]["VALUE"]);
        }
        $allPrices = [];
        $allId = [];
        $allPropertiesName = [];
        $allPropertiesValue = [];
        $allPropertiesImages = [];
        $allStatusValue = [];
        $allDiscountValue = [];
        foreach ($arResult["OFFERS"] as $key => &$offer) {
            $discount = 0;
            unset($offer["PROPERTIES"]["CML2_LINK"]);
            unset($offer["PROPERTIES"]["warranty"]);
            unset($offer["PROPERTIES"]["delivery"]);
            $offer["TRADE_NAME"] = $offer["PROPERTIES"]["name"]["VALUE"];
            unset($offer["PROPERTIES"]["name"]);
            //Если не задано наименование, то такой товар мы не должны выводить как тп
            if (!$offer["TRADE_NAME"]) {
                unset($arResult["OFFERS"][$key]);
                continue;
            }


            if (intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) !== -1) {
                if ($offer["PROPERTIES"]["status"]["VALUE"] == 1) {
                    $allStatusValue[] = 1;
                }
                else {
                    $allStatusValue[] = 0;
                }
            }
            else {
                $allStatusValue[] = -1;
            }
            $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
                $offer["ID"],
                $USER->GetUserGroupArray(),
                "N",
                1,
                SITE_ID
            );
            if (!$arDiscounts) {
                $allDiscountValue[] = 0;
            }
            else {
                foreach ($arDiscounts as $discount) {
                    if ($discount["VALUE"]) {
                        $allDiscountValue[] = $discount["VALUE"];
                        $discount = $discount["VALUE"];
                    }
                    break;
                }
            }

            $offer["PRICE"] = +CPrice::GetBasePrice($offer["ID"])["PRICE"];
            if ($discount) {
                $offer["PRICE"] = ceil((+$offer["PRICE"] ) * (1 - (+$discount / 100)));
            }

            $allPrices[] = +$offer["PRICE"];
            $allId[] = $offer["ID"];
            $propsName = [];
            $propsValue = [];
            $propsImage = [];
            foreach($offer["PROPERTIES"] as $propKey => $prop) {
                if ($prop["CODE"] == "photoName") {


                    if ($prop["VALUE"] &&  $offer["DETAIL_PICTURE"]) {
                        $imgData = array_merge(array($offer["DETAIL_PICTURE"]), $prop["VALUE"]);
                    }
                    else if ($prop["VALUE"] && $arResult["DETAIL_PICTURE"]) {
                        $imgData = array_merge(array($arResult["DETAIL_PICTURE"]),  $prop["VALUE"]);
                    }
                    else if ($prop["VALUE"]) {
                        $imgData = $prop["VALUE"];
                    }
                    else if ($offer["DETAIL_PICTURE"]) {
                        $imgData = array($offer["DETAIL_PICTURE"]);

                    }
                    else {
                        $imgData = array($arResult["DETAIL_PICTURE"]);
                    }

                    foreach($imgData as $imageKey => $image) {

                        $arFilters = Array(
                            array("name"=> "watermark",
                                "position"=>"bl",
                                'type'=>'text',
                                "coefficient" => "5",
                                'text' =>  $offer["XML_ID"],
                                'color' => 'B12A0F',
                                "font"=> $_SERVER["DOCUMENT_ROOT"]. DEFAULT_TEMPLATE_PATH ."/fonts/OpenSans-Bold.ttf",
                                "use_copyright"=>"Y")
                        );
                        if ($image["FILE_NAME"]) {
                            $photo = $image;
                        }
                        else {
                            $photo = CFile::GetFileArray($image);
                            $photo =  $photo["ID"];
                        }
                        $newPhoto = CFile::ResizeImageGet(
                            $photo,
                            array("width"=> 600, "height" => 600),
                            BX_RESIZE_IMAGE_EXACT,
                            true,
                            $arFilters
                        );
                        $originalPhoto = CFile::ResizeImageGet(
                            $photo,
                            array("width"=> $file["WIDTH"], "height" => $file["HEIGHT"]),
                            BX_RESIZE_IMAGE_EXACT,
                            true,
                            $arFilters
                        );
                        if ($newPhoto) {
                            $propsImage[] = array($newPhoto['src'], $originalPhoto['src']);
                            $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey] = array();
                            $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey]["NEW"] = $newPhoto['src'];
                        }
                        else {
                            $propsImage[] = $originalPhoto['src'];
                            $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey] = array();
                            $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey]["NEW"] = $originalPhoto['src'];
                        }
                        $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey]["ORIGIN"] = $originalPhoto['src'];
                    }
                }
                else if ($prop["VALUE"] && $prop["CODE"] != "status") {
                    $propsName[$prop["NAME"]] = $prop["NAME"];
                    $propsValue[$prop["NAME"]] = $prop["VALUE"];
                }
                else {
                    unset($offer["PROPERTIES"][$propKey]);
                }
            }
            foreach($arResult["PROPERTIES"] as $prop) {
                if ($prop["CODE"] != 'photoName'  && $prop["CODE"] != 'file' && $prop["CODE"] != 'youtube' && $prop["CODE"] != 'status' && !$propsName[$prop["CODE"]] && $prop["VALUE"]) {
                    $propsName[$prop["CODE"]] = $prop["NAME"];
                    $propsValue[$prop["CODE"]] = $prop["VALUE"];
                    $arResult["OFFERS"][$key]["PROPERTIES"][$prop["CODE"]]["VALUE"] = $prop["VALUE"];
                    $arResult["OFFERS"][$key]["PROPERTIES"][$prop["CODE"]]["NAME"] = $prop["NAME"];
                }
            }
            $allPropertiesName[] = $propsName;
            $allPropertiesValue[] = $propsValue;
            $allPropertiesImages[] = $propsImage;
        }
        unset($offer);
        $arResult['allPrices'] = $allPrices;
        $arResult['allId'] = $allId;
        $arResult['allPropertiesName'] = $allPropertiesName;
        $arResult['allPropertiesValue'] = $allPropertiesValue;
        $arResult['allPropertiesImages'] = $allPropertiesImages;
        $arResult['allStatusValue'] = $allStatusValue;
        $arResult['allDiscountValue'] = $allDiscountValue;
    }
    else if (count($arResult["OFFERS"]) == 1) {
        $arResult["PRODUCT_MODE"] = "SIMPLE_TP";
        foreach ($arResult["OFFERS"] as $key=>&$offer) {

            $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
                $offer["ID"],
                $USER->GetUserGroupArray(),
                "N",
                1,
                SITE_ID
            );
            foreach ($arDiscounts as $discount) {
                if ($discount["VALUE"]) {
                    $arResult["DISCOUNT"] = $discount["VALUE"];
                }
                break;
            }
            $offer["PRICE"] = +CPrice::GetBasePrice($offer["ID"])["PRICE"];
            if ($arResult["DISCOUNT"]) {
                $offer["PRICE"] = ceil ((+$offer["PRICE"] ) * (1 - (+$arResult["DISCOUNT"] / 100)));
            }
            $arResult["PRICE"] = +$offer["PRICE"];
            $arResult["ID"] =  $arResult["OFFERS"]["ID"];
            $arResult["OFFERS"] = $offer;
            break;
        }


        foreach ($arResult["OFFERS"]["PROPERTIES"] as $key => $prop) {
            if ($prop["VALUE"] && $arResult["PROPERTIES"][$prop["CODE"]]) {
                $arResult["PROPERTIES"][$prop["CODE"]] = $prop;
            }
            else if ($prop["VALUE"]) {
                $arResult["PROPERTIES"][$prop["CODE"]] = $prop;
            }
        }
        if ($arResult["OFFERS"]["DETAIL_PICTURE"]) {
            $arResult["DETAIL_PICTURE"] = $arResult["OFFERS"]["DETAIL_PICTURE"];
        }
        unset($arResult["OFFERS"]);
        unset($arResult["PROPERTIES"]["CML2_LINK"]);
        unset($arResult["PROPERTIES"]["warranty"]);
        unset($arResult["PROPERTIES"]["delivery"]);
        unset($arResult["PROPERTIES"]["name"]);

        //TODO Удалить, если изменения окажутся некорректными
        if (!$arResult["PROPERTIES"]["photoName"]["VALUE"]) {
            $arResult["PROPERTIES"]["photoName"]["VALUE"] = array($arResult["DETAIL_PICTURE"]);
        }
        else {
            $arResult["PROPERTIES"]["photoName"]["VALUE"] = array_merge(array($arResult["DETAIL_PICTURE"]),  $arResult["PROPERTIES"]["photoName"]["VALUE"]);
        }

        foreach ($arResult["PROPERTIES"]["photoName"]["VALUE"] as $key=>$val) {
            if ($val["FILE_NAME"]) {
                $photo = $val;
            }
            else {
                $photo = CFile::GetFileArray($val);
                $photo =  $photo["ID"];
            }
            $arFilters = Array(
                array("name"=> "watermark",
                    "position"=>"bl",
                    'type'=>'text',
                    "coefficient" => "5",
                    'text' =>  $arResult["XML_ID"],
                    'color' => 'B12A0F',
                    "font"=> $_SERVER["DOCUMENT_ROOT"]. DEFAULT_TEMPLATE_PATH ."/fonts/OpenSans-Bold.ttf",
                    "use_copyright"=>"Y")
            );
            $newPhoto = CFile::ResizeImageGet(
                $photo,
                array("width"=> 600, "height" => 600),
                BX_RESIZE_IMAGE_EXACT,
                true,
                $arFilters
            );
            $originalPhoto = CFile::ResizeImageGet(
                $photo,
                array("width"=> $file["WIDTH"], "height" => $file["HEIGHT"]),
                BX_RESIZE_IMAGE_EXACT,
                true,
                $arFilters
            );
            $arResult["PROPERTIES"]["photoName"]["VALUE"][$key] = null;
            if ($newPhoto) {
                $arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["NEW"] = $newPhoto['src'];
            }
            else {
                $arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["NEW"] = $originalPhoto['src'];
            }
            $arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["ORIGIN"] = $originalPhoto['src'];
        }
    }
    else {
        $arResult["PRODUCT_MODE"] = "SIMPLE_TP";

        $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
            $arResult["ID"],
            $USER->GetUserGroupArray(),
            "N",
            1,
            SITE_ID
        );
        foreach ($arDiscounts as $discount) {
            if ($discount["VALUE"]) {
                $arResult["DISCOUNT"] = $discount["VALUE"];
            }
            break;
        }
        $arResult["PRICE"] = +CPrice::GetBasePrice($arResult["ID"])["PRICE"];
        if ($arResult["DISCOUNT"]) {
            $arResult["PRICE"] = ceil((+$arResult["PRICE"] ) * (1 - (+$arResult["DISCOUNT"] / 100)));
        }

        $arResult["SUBSCRIBE_ID"] = $arResult["ID"];


        if (!$arResult["PROPERTIES"]["photoName"]["VALUE"]) {
            $arResult["PROPERTIES"]["photoName"]["VALUE"] = array($arResult["DETAIL_PICTURE"]);
        }
        else {
            $arResult["PROPERTIES"]["photoName"]["VALUE"] = array_merge(array($arResult["DETAIL_PICTURE"]),  $arResult["PROPERTIES"]["photoName"]["VALUE"]);
        }

//        if (!$arResult["PROPERTIES"]["photoName"]["VALUE"]) {
//            $arResult["PROPERTIES"]["photoName"]["VALUE"] = array($arResult["DETAIL_PICTURE"]);
//        }
        foreach ($arResult["PROPERTIES"]["photoName"]["VALUE"] as $key=>$val) {
            if ($val["FILE_NAME"]) {
                $photo = $val;
            }
            else {
                $photo = CFile::GetFileArray($val);
                $photo =  $photo["ID"];
            }
            $arFilters = Array(
                array("name"=> "watermark",
                    "position"=>"bl",
                    'type'=>'text',
                    "coefficient" => "5",
                    'text' =>  $arResult["XML_ID"],
                    'color' => 'B12A0F',
                    "font"=> $_SERVER["DOCUMENT_ROOT"]. DEFAULT_TEMPLATE_PATH ."/fonts/OpenSans-Bold.ttf",
                    "use_copyright"=>"Y")
            );
            $newPhoto = CFile::ResizeImageGet(
                $photo,
                array("width"=> 600, "height" => 600),
                BX_RESIZE_IMAGE_EXACT,
                true,
                $arFilters
            );
            $originalPhoto = CFile::ResizeImageGet(
                $photo,
                array("width"=> $file["WIDTH"], "height" => $file["HEIGHT"]),
                BX_RESIZE_IMAGE_EXACT,
                true,
                $arFilters
            );
            $arResult["PROPERTIES"]["photoName"]["VALUE"][$key] = null;
            if ($newPhoto) {
                $arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["NEW"] = $newPhoto['src'];
            }
            else {
                $arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["NEW"] = $originalPhoto['src'];
            }
            $arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["ORIGIN"] = $originalPhoto['src'];
        }
    }

    if ($arResult["PRODUCT_MODE"] == "SIMPLE_TP") { //Если тп одно, то это обычный товар
        $res = "<div class='lol' style='background: rgb(0, 0, 0); z-index: 25;  opacity: 0.6; width: 100%; height: 100%; position: fixed; top: 0px;'></div>";
        $res .= " <div class=\"arcticmodal-container\" style=\"z-index: 50;\"><table class=\"arcticmodal-container_i\"><tbody><tr><td class=\"arcticmodal-container_i2\"><div class=\"big_item container\" data-role=\"quick-view-product\" data-offer-id=\"\">
        <div class=\"row\">
            <div class=\"title\">
                <h1>{$arResult['NAME']}</h1>";
        if(intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 1) : //Если количество товаров положительное число, то...
            $res .= "<div class=\"we_have\">
                    <span>В наличии <i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i></span>
                </div>";
        elseif (intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 0) : //иначе
            $res .= "<div class=\"zakaz\"> 
					<span>Доставка до 3-х дней
						<i><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAaCAYAAABCfffNAAAFEElEQVRIS6WWe2yTVRjGn+e0o90UwVsCa7kNjEaMIRoNtwgGowEFRIMKalsZmchNJIN2I5qJwloHKqKQEIGuiEQXJBrARP9QEw1egsrNhOAFWVeI6EAytrX0O4/5JhDEdYCev75857zv73vO977POQzEQl9Q/A3QZlruaKyr/wkXMfpEw3c4oi9TcvAT1Hya7yqEpdHIIzB2OC1HgCimsB/GxtO1G776V+DkyZ7gIP9IWc8EGQw2UgmNndm4dMPeLiFnJoPPlF/l+J3Bxup5UGrzO5OaazaecOcDCx8NkkWTBE0EcTuIbV468/PWOx2QbYrXL7koyJlFpVWRGyi9SzKWrk1uD0TDIRFTDLRH4D0AbgbweU9/65g/2y673hobo+NEm17amC4EYicTDMTCKUK5dDxVHoiFF0nql0mkKoLRyFTBPihjNlzpO7k9+0cvtl9+YrkVv83Ek+suBYJgVeQBKy2Wx3svT8lPj7PYOrbmcMnA/di3j2hocM4qj0YeITW2V7Nv+s41a051BupMCfrGpl7poOg9AB80xetfDUQjywh7KJ1IrTg/SVm0okcO2eUw2G2N2ZJZsr7x/DWdQtxFwVgoamnu7ObD/fl2jBb0qE7lnsks3/T72SQ1o729s/2HGek5AIMkfYxi/7xMzZrWc0EFIYGFkaEw2gSYcmZbdlh/8ZtwTDLzUvLj/gsivXIGo0lNBXgFidfomC8tnRcBbG5K1G+7KEhpLDKN0FqBiUw8GQvEIhFQYyHsJjBJwK2nE73VFK9/3H0ujYVmEizr6W+t3lfTkDsD6lRJaU1FCduzb0AYQuI3I8x3iHIAt0F4m8SNAmafTtJCcHw6nvy0dOG060nnRXr0QnppaneXkEBVaBjElwGzCFCFgIOk+llh2eF4/c6+0fBgh9hOwa2yzYae1Yfi637uUFMVqiVwtKk29QoAue86V1IVLqc0J5/XCK/XzIN0C8DvRDVn4qlVg+bM8bV1PzHSkTl8pHb9D+fuf6A6dCfECgdm7pHa9UcLQvpUP36TdUy9ga22hr/AchWIDwGWFTl44WBd8kihxrtm4bTuPuO8TuCjdLx+Y0EIJk/2lJYVrzagSRf/OiOQ7bedFrusUZ4y3zbFkw1deVUwGnpMhsPl81W65Vy4hKvDY2DxKgB3r4cA2gtxK6kB/pM9nv1x5cpsIVDvWPhuAzxH8al0IrmnIKSjwtqyW0EayDwtYxOUPgNwtQHfaYwnv/kHxFU/qGQooPsoMwDEl6DdBethQUiHxVeF5lO8p4e/dfzxbPFsiKMAHgPUzGzrovQrDW2uBVkUjbPSKJIOwF0gTkIaCWIMxb1dQoILItfJuLavxQCvFbAMwgESfks20upzEf2McNyCO41Bd1iNE9AfxNciUjnH4zZvl4PBWCgh8GEIzSBX+NRtS4vH+r02N4NgVFDCgIcFhSH6/z7GTUO6Lnmgy2Y8F1taFR5Bi60wnCtrP6NhAmKzT92q25mrIFQJYhegjcqd2vYPAz2d6EJKXD+6y/1i2PwT8HiXQ3jodOwSWc8GepwUyCeblia/v6RD6zwlE4xQCY+mWIcrXXP8u8FUZ2nWUXYtjJn1vyB9FoQHWg/eA7mMyu+APHUCjjHnrZQvPx/QsJy/beLRmoaW/6yk4wCLRuaIqiRQ7fVzW3uWRQY2TGGepFmZROr9rsrngv+kI9i9b5UVzwbpOvIxEEWw6AZy6YUsxg3/C/J2RwwHA5jyAAAAAElFTkSuQmCC\" alt=\"\"></i>
					</span>
                    <span class=\"sub_text\">Цена товара<br>может измениться</span>
                </div>";
        else :
            $res .= " <div class=\"no_zakaz\">
                    <span>Нет в наличии</span>
                </div>";
        endif;
        $res .= " </div>
            <div class=\"row\">
                <div class=\"col-md-5\">
                    <div class=\"image\">
                        <div class=\"main_img\">";
        if ($arResult["DISCOUNT"]) {
            $res .= "<div class=\"skidka_div\"><div class=\"skidka\">-{$arResult["DISCOUNT"]}%</div></div><div class=\"skidka_div\"></div> <div class=\"skidka\" style=\"display:none;\" data-role=\"skidka\"></div>";
        }
        $res .= "<a><img src=\"{$arResult["PROPERTIES"]["photoName"]["VALUE"][0]["ORIGIN"]}\"></a></div> <div class=\"description\">  <span class=\"title\">Описание</span>";
        $arResult["DETAIL_TEXT"] = strip_tags($arResult["DETAIL_TEXT"]);
        if (strlen($arResult["DETAIL_TEXT"]) > 250) {
            $arResult["DETAIL_TEXT"] = substr($arResult["DETAIL_TEXT"], 0, 250) . "...";
        }
        $res .= "<p id='description'>{$arResult['DETAIL_TEXT']}</p></div><a href=\"{$arResult['DETAIL_PAGE_URL']}\" class=\"s_btn reverse\">Подробнее</a></div></div>
                <div class=\"col-md-7\">
                    <div class=\"item_info changeMarket\">                        
                        <form method=\"post\" action=\"/changeMarket.php\">";
        $res .= "<div class=\"spinEdit\" style=\"float: left\">
                               <input class=\"noSelect quantity\" type=\"text\" value=\"1\" name=\"count\" data-id=\"{$arResult["PRICE"]}\" data-price=\"{$arResult["PRICE"]}\"/>
                                    <div class=\"spinedit\"><div class=\"minus_prod_det minus-btn icon-chevron-down\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></div><div class=\"plus_prod_det plus-btn icon-chevron-up\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></div></div>
                             </div>
                                    <input style=\"display: none\" class=\"cost\" value = \"{$arResult["PRICE"]}\"/>
                                   <input style=\"display: none\" class=\"id\" value = \"{$arResult["ID"]}\"/>
                            <div class=\"price_block\" style=\"float: left\">
                            
                                <span class=\"price\">
                                    <span class=\"sum\" price=\"{$arResult["PRICE"]}\">{$arResult["PRICE"]}</span> 
                                    <i class=\"fa fa-rub fa-3\"></i>
                                </span>";
        if (intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 1 || intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 0) {
            $res .= " <input type=\"submit\" class=\"b_btn reverse addToCart\" align=\"absmiddle\" value=\"В корзину\" data-role=\"buy-btn\">";
        }
        else {
            $res .= " <input type=\"button\" class=\"b_btn\" value=\"Недоступно\"  data-role=\"buy-btn\">";
        }
        $res .= "</div></form></div>
                        <table class=\"propertys\">
                            <tbody> ";
        //И выводим последовательно свойства товара и его тп
        unset($arResult["PROPERTIES"]["photoName"]);
        foreach($arResult["PROPERTIES"] as $prop) {
            if(str_replace(' ', '', $prop["VALUE"])  && $prop["CODE"] != "file" && $prop["CODE"] != "youtube" && $prop["CODE"] != "status") {
                $res .= " <tr valign=\"top\">
                                        <td nowrap=\"nowrap\">{$prop['NAME']}</td>
                                        <td width=\"100%\">{$prop['VALUE']}</td>
                                    </tr>";
            }
        }
        $res .= "                     
                        </tbody></table>
                    

                </div>
                <div class=\"navigates\">
                    <a href=\"#\" class=\"prev\" >
                        <i class=\"fa fa-angle-left\"></i>
                    </a>
                    <a href=\"#\" class=\"next\"> 
                        <i class=\"fa fa-angle-right\"></i>
                    </a>
                </div>
                <a href=\"#\" class=\"close\">
                    <svg id=\"SvgjsSvg1072\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"25\" height=\"25\" viewBox=\"0 0 25 25\">
                        <defs id=\"SvgjsDefs1073\"></defs>
                        <path id=\"SvgjsPath1074\" d=\"M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z \" fill=\"#7e7e7e\" fill-opacity=\"1\" transform=\"matrix(1,0,0,1,-320,-200)\"></path>
                    </svg>
                </a>
            </div>
        </div>
    </div></td></tr></tbody></table></div>";
        echo ($res);
    }
    else {
        $item = reset($arResult["OFFERS"]);
        $selected_offers = 0;
        $res = "<div class='lol' style='background: rgb(0, 0, 0); z-index: 25;  opacity: 0.6; width: 100%; height: 100%; position: fixed; top: 0px;'></div>";
        $res .= " <div class=\"arcticmodal-container\" style=\"z-index: 50;\"><table class=\"arcticmodal-container_i\"><tbody><tr><td class=\"arcticmodal-container_i2\"><div class=\"big_item container\" data-role=\"quick-view-product\" data-id=\"113940\" data-offer-id=\"\">
        <div class=\"row\">
            <div class=\"title\">
                <h1>{$arResult['NAME']}</h1>";
        if(intval(trim($arResult['allStatusValue'][0])) === 1) : //Если количество товаров положительное число, то...
            $res .= "<div class=\"we_have\">
                    <span>В наличии <i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i></span>
                </div>";
        elseif (intval(trim($arResult['allStatusValue'][0])) === 0) : //иначе
            $res .= "<div class=\"zakaz\"> 
					<span>Доставка до 3-х дней
						<i><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAaCAYAAABCfffNAAAFEElEQVRIS6WWe2yTVRjGn+e0o90UwVsCa7kNjEaMIRoNtwgGowEFRIMKalsZmchNJIN2I5qJwloHKqKQEIGuiEQXJBrARP9QEw1egsrNhOAFWVeI6EAytrX0O4/5JhDEdYCev75857zv73vO977POQzEQl9Q/A3QZlruaKyr/wkXMfpEw3c4oi9TcvAT1Hya7yqEpdHIIzB2OC1HgCimsB/GxtO1G776V+DkyZ7gIP9IWc8EGQw2UgmNndm4dMPeLiFnJoPPlF/l+J3Bxup5UGrzO5OaazaecOcDCx8NkkWTBE0EcTuIbV468/PWOx2QbYrXL7koyJlFpVWRGyi9SzKWrk1uD0TDIRFTDLRH4D0AbgbweU9/65g/2y673hobo+NEm17amC4EYicTDMTCKUK5dDxVHoiFF0nql0mkKoLRyFTBPihjNlzpO7k9+0cvtl9+YrkVv83Ek+suBYJgVeQBKy2Wx3svT8lPj7PYOrbmcMnA/di3j2hocM4qj0YeITW2V7Nv+s41a051BupMCfrGpl7poOg9AB80xetfDUQjywh7KJ1IrTg/SVm0okcO2eUw2G2N2ZJZsr7x/DWdQtxFwVgoamnu7ObD/fl2jBb0qE7lnsks3/T72SQ1o729s/2HGek5AIMkfYxi/7xMzZrWc0EFIYGFkaEw2gSYcmZbdlh/8ZtwTDLzUvLj/gsivXIGo0lNBXgFidfomC8tnRcBbG5K1G+7KEhpLDKN0FqBiUw8GQvEIhFQYyHsJjBJwK2nE73VFK9/3H0ujYVmEizr6W+t3lfTkDsD6lRJaU1FCduzb0AYQuI3I8x3iHIAt0F4m8SNAmafTtJCcHw6nvy0dOG060nnRXr0QnppaneXkEBVaBjElwGzCFCFgIOk+llh2eF4/c6+0fBgh9hOwa2yzYae1Yfi637uUFMVqiVwtKk29QoAue86V1IVLqc0J5/XCK/XzIN0C8DvRDVn4qlVg+bM8bV1PzHSkTl8pHb9D+fuf6A6dCfECgdm7pHa9UcLQvpUP36TdUy9ga22hr/AchWIDwGWFTl44WBd8kihxrtm4bTuPuO8TuCjdLx+Y0EIJk/2lJYVrzagSRf/OiOQ7bedFrusUZ4y3zbFkw1deVUwGnpMhsPl81W65Vy4hKvDY2DxKgB3r4cA2gtxK6kB/pM9nv1x5cpsIVDvWPhuAzxH8al0IrmnIKSjwtqyW0EayDwtYxOUPgNwtQHfaYwnv/kHxFU/qGQooPsoMwDEl6DdBethQUiHxVeF5lO8p4e/dfzxbPFsiKMAHgPUzGzrovQrDW2uBVkUjbPSKJIOwF0gTkIaCWIMxb1dQoILItfJuLavxQCvFbAMwgESfks20upzEf2McNyCO41Bd1iNE9AfxNciUjnH4zZvl4PBWCgh8GEIzSBX+NRtS4vH+r02N4NgVFDCgIcFhSH6/z7GTUO6Lnmgy2Y8F1taFR5Bi60wnCtrP6NhAmKzT92q25mrIFQJYhegjcqd2vYPAz2d6EJKXD+6y/1i2PwT8HiXQ3jodOwSWc8GepwUyCeblia/v6RD6zwlE4xQCY+mWIcrXXP8u8FUZ2nWUXYtjJn1vyB9FoQHWg/eA7mMyu+APHUCjjHnrZQvPx/QsJy/beLRmoaW/6yk4wCLRuaIqiRQ7fVzW3uWRQY2TGGepFmZROr9rsrngv+kI9i9b5UVzwbpOvIxEEWw6AZy6YUsxg3/C/J2RwwHA5jyAAAAAElFTkSuQmCC\" alt=\"\"></i>
					</span>
                    <span class=\"sub_text\">Цена товара<br>может измениться</span>
                </div>";
        else :
            $res .= " <div class=\"no_zakaz\">
                    <span>Нет в наличии</span>
                </div>";
        endif;
        unset($item["PROPERTIES"]["status"]);
        $res .= "</div>
            <div class=\"row\">
                <div class=\"col-md-5\">
                    <div class=\"image\">
                            <div class=\"main_img\">";
         if ($arResult['allDiscountValue'][0]) {
             $res .= "<div class=\"skidka_div\"><div class=\"skidka\">-{$arResult['allDiscountValue'][0]}%</div></div><div class=\"skidka_div\"><div class=\"skidka\" style=\"display:none;\" data-role=\"skidka\"></div></div>";
         }
        $res .= "<div class='rphoto'><a>
                                <img src='{$item["PROPERTIES"]["photoName"]["VALUE"][0]["ORIGIN"]}' >
                            </a>
                        </div>
                        </div>
						<div class=\"description\">
                        <span class=\"title\">Описание</span>";
        $arResult["DETAIL_TEXT"] = strip_tags($arResult["DETAIL_TEXT"]);
        if (strlen($arResult["DETAIL_TEXT"]) > 250) {
            $arResult["DETAIL_TEXT"] = substr($arResult["DETAIL_TEXT"], 0, 250) . "...";
        }
        $res .= "<p id='description'>{$arResult['DETAIL_TEXT']}</p></div>
                        <a href=\"{$arResult['DETAIL_PAGE_URL']}\" class=\"s_btn reverse\">Подробнее</a>
                    </div>
                </div>
                <div class=\"col-md-7\">
                    <div class=\"item_info changeMarketQuick\">
                        <form method=\"post\" action=\"/ajax/changeMarket.php\">
                         <input class=\"allPrices\" type=\"hidden\" name=\"allPrices\" value=\"" . htmlspecialchars(json_encode($arResult['allPrices'], ENT_NOQUOTES )) . "\">
                        <input class=\"allId\" type=\"hidden\" name=\"allId\" value=\"" . htmlspecialchars(json_encode($arResult['allId'], ENT_NOQUOTES )). "\">
                        <input class=\"allPropertiesName\" type=\"hidden\" name=\"allPropertiesName\" value='" .  htmlspecialchars(json_encode($arResult['allPropertiesName'], ENT_NOQUOTES )). "'>
                        <input class=\"allPropertiesValue\" type=\"hidden\" name=\"allPropertiesValue\" value='" .  htmlspecialchars(json_encode($arResult['allPropertiesValue'], ENT_NOQUOTES )). "'>
                        <input class=\"allPropertiesImages\" type=\"hidden\" name=\"allPropertiesImages\" value='" .  htmlspecialchars(json_encode($arResult['allPropertiesImages'], ENT_NOQUOTES )). "'>
                        <input class=\"allStatusValue\" type=\"hidden\" name=\"allStatusValue\" value='". htmlspecialchars(json_encode($arResult['allStatusValue'], ENT_NOQUOTES )) . "'>
                        <input class=\"allDiscountValue\" type=\"hidden\" name=\"allDiscountValue\" value='". htmlspecialchars(json_encode($arResult['allDiscountValue'], ENT_NOQUOTES )) . "'>
                          <table class=\"acrticle_select custom_chekbox\">
                            <tbody>
                            <tr>
                                <th>Артикул</th>
                                <th>Наименование</th>
                            </tr>";
        $index = 0;
        foreach ($arResult["OFFERS"] as $offer) {
            $res .= " <tr>
                                              <td nowrap=\"nowrap\">";
            if($index == $selected_offers)
                $res .= "<input value=\"{$index}\" type=\"radio\" name=\"select\" id=\"{$offer["ID"]}\"  class=\"select\" checked>";
            else
                $res .= "<input value=\"{$index}\" type=\"radio\" name=\"select\" id=\"{$offer["ID"]}\"  class=\"select\">";
            $res .= "
                                                        <label for=\"{$offer["ID"]}\" class=\"css-label radGroup2\">
                                                           {$offer["PROPERTIES"]["article"]["VALUE"]}
                                                        </label>
                                              </td>
                                              <td>
                                                {$offer["TRADE_NAME"]}
                                              </td>
                                              </tr>";
            $index++;
        }
        $res .="
                            <input style=\"display: none\" class=\"cost\" value = \"{$arResult['allPrices'][0]}\"/>
                            <input style=\"display: none\" class=\"id\" value = \"{$item["ID"]}\"/>
                            </tbody>
                            </table>";
        $res .= "
                            <div class=\"count_block\">
                            <div class=\"spinEdit\">
                                <input class=\"input-quantity spinedit noSelect quantity\" value=\"1\"><div class=\"spinedit\"><div class=\"minus-btn icon-chevron-down minus_prod_det\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></div><div class=\"plus_prod_det plus-btn icon-chevron-up\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></div></div>
                            </div>
                            </div>
                            <div class=\"price_block\" style=\"float: left\">

                                <span class=\"price\">
                                    <span class=\"sum\" price=\"{$arResult['allPrices'][0]}\">{$arResult['allPrices'][0]}</span>
                                    <i class=\"fa fa-rub fa-3\"></i>
                                </span>";
        if(intval(trim($arResult['allStatusValue'][0])) === 1 || intval(trim($arResult['allStatusValue'][0])) === 0) {
            $res .= " <input type=\"submit\" class=\"b_btn addToCart\" value=\"В корзину\" name=\"submit\" data-role=\"buy-btn\" data-text-in-cart=\"Добавлено\">";
        }
        else {
            $res .= " <input type=\"button\" class=\"b_btn\" value=\"Недоступно\"  data-role=\"buy-btn\">";
        }
        $res .= "</div>
                        </form>
                    </div>
                        <table class=\"propertys\">
                            <tbody> ";
        unset($arResult["PROPERTIES"]["PHOTO"]);
        foreach($item["PROPERTIES"]  as $prop) {
            if(str_replace(' ', '', $prop["VALUE"])  && $prop["CODE"] != "photoName" && $prop["CODE"] != "name") {
                $res .= " <tr valign=\"top\">
                                            <td class=\"propertyName\" nowrap=\"nowrap\">{$prop['NAME']}</td>
                                            <td class=\"propertyValue\" width=\"100%\">{$prop['VALUE']}</td>
                                        </tr>";
            }
        }
        $res .= "
                        </tbody></table>


                </div>
                <div class=\"navigates\">
                    <a href=\"#\" class=\"prev\" >
                        <i class=\"fa fa-angle-left\"></i>
                    </a>
                    <a href=\"#\" class=\"next\">
                        <i class=\"fa fa-angle-right\"></i>
                    </a>
                </div>
                <a href=\"#\" class=\"close\">
                    <svg id=\"SvgjsSvg1072\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"25\" height=\"25\" viewBox=\"0 0 25 25\">
                        <defs id=\"SvgjsDefs1073\"></defs>
                        <path id=\"SvgjsPath1074\" d=\"M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z \" fill=\"#7e7e7e\" fill-opacity=\"1\" transform=\"matrix(1,0,0,1,-320,-200)\"></path>
                    </svg>
                </a>
            </div>
        </div>
    </div></td></tr></tbody></table></div>";
        echo ($res);
    }
}
else
    echo "Error, cannot connect";
?>