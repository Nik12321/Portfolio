<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$author = htmlspecialcharsbx($_POST["author"]);
$email = htmlspecialcharsbx($_POST["email"]);
$content = htmlspecialcharsbx($_POST["content"]);

if ($APPLICATION->CaptchaCheckCode($_POST["captcha"], $_POST["captcha_sid"]))
    $captcha = "Y";
else if (empty($_POST["captcha"]))
    $captcha = "R";
else {
    $captcha = "N";
}

$captcha_code = $APPLICATION->CaptchaGetCode();

if (empty($author) || empty($email) || empty($content) || $captcha != "Y")
    $success = "N";
else
    $success = "Y";


$resc = CIBlock::GetList(Array(), Array('=CODE' => "Review"), false);
$iblock_id = 0;
if($arrc = $resc->Fetch()) {
    $iblock_id = $arrc["ID"];
}
else {
    $success = "N";
}

$arEventFields = array (
    "author"=>$author,
    "email"=>$email,
    "content"=>$content,
    "captcha"=>$captcha,
    "captcha_code"=>$captcha_code,
    "SUCCESS"=> $success
);
if ($success == "Y") {
    if(CModule::IncludeModule("iblock")) {
        $newEl = new \CIBlockElement;
        $PROP = array();
        $PROP["AUTHOR"] = $author;
        $PROP["EMAIL"] = $email;
        $PROP["REVIEW"] = $content;
        $name = $content;
        $arLoadProductArray = Array(
            "IBLOCK_ID"      => $iblock_id,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $name,
            "ACTIVE"         => "Y"
        );
        $newEl->Add($arLoadProductArray);
        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];
        $arEventFieldsMessage = array(
            "EMAIL" =>  $PROP["EMAIL"],
            "NAME" => $PROP["AUTHOR"],
            "TITLE" => "Пользователь заполнил форму 'Отзывы'!",
            "MESSAGE" => "Текст отзыва: " . $PROP["REVIEW"],
            "DEFAULT_EMAIL_FROM" => $mail_to,
            "SITE_NAME" =>  $arSite["NAME"],
            "SERVER_NAME" => ""

        );
        $arEventFieldsMessage["MESSAGE"] .= "<br>Связаться с пользователем можно по почте: " . $PROP["EMAIL"];
        CEvent::Send("USER_USED_FAQ_OR_REVIEW_FORM", "s1", $arEventFieldsMessage, "N", "");
    }
}
echo json_encode($arEventFields);
?>