<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$cost = htmlspecialcharsbx($_POST["cost"]);
$id = htmlspecialcharsbx($_POST["id"]);
if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog"))
{
    $arFields = array(
        "PRODUCT_ID" => $id,
        "PRODUCT_PRICE_ID" => 0,
        "PRICE" => $cost,
        "CURRENCY" => "RUB",
        "QUANTITY" => 1,
        "LID" => LANG,
        "DELAY" => "N",
        "CAN_BUY" => "Y"
    );
    if (Add2BasketByProductID($id,1))
        echo 1;
    else {
        if ($ex = $APPLICATION->GetException())
            echo '<br>'.$ex->GetString();
    }
}
else
    echo 0;
?>