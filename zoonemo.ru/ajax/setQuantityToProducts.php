<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
$ids = $_POST["ids"];
$quantity = array();
foreach ($ids as $id) {
    $dbBasketItems = CSaleBasket::GetList(false, array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "PRODUCT_ID" => $id, "LID" => SITE_ID, "ORDER_ID" => "NULL", "DELAY" => "N"), false, false, array("ID", "QUANTITY"));
    if ($arItems = $dbBasketItems->Fetch()) {
        $quantity[] = $arItems["QUANTITY"];
    }
    else {
        $quantity[] = 0;
    }
}
$arEventFields = array (
    "id" => $ids,
    "quantity" => $quantity
);
echo json_encode($arEventFields);