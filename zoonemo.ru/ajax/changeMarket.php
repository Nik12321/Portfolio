<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$radio = $_POST["message_pri"];
$allPrices = (array) json_decode($_POST["allPrices"]);
$allId = (array) json_decode($_POST["allId"]);
$allPropertiesName = (array) json_decode($_POST["allPropertiesName"], true);
$allPropertiesValue = (array) json_decode($_POST["allPropertiesValue"], true);
$count = count($allPropertiesName[$radio]);
$allStatusValue = (array) json_decode($_POST["allStatusValue"], true);
$allDiscountValue = (array) json_decode($_POST["allDiscountValue"], true);
for ($i = 0; $i < $count; $i++) {
    if (!str_replace(' ', '', $allPropertiesValue[$radio][$i]) || $allPropertiesName[$radio][$i] == "Доставка" || $allPropertiesName[$radio][$i] == "Гарантия") {
        unset($allPropertiesValue[$radio][$i]);
        unset($allPropertiesName[$radio][$i]);
    }
}
$allPropertiesValue[$radio] = array_values($allPropertiesValue[$radio]);
$allPropertiesName[$radio] = array_values($allPropertiesName[$radio]);
$allPropertiesImages = (array)  json_decode($_POST["allPropertiesImages"]);
if (!$_POST["message_pri"] && $_POST["message_pri"] !== "0")
    $success = "N";
else
    $success = "Y";
$arEventFields = array (
    "RADIO"=>$radio,
    "SUCCESS"=>$success,
    "PRICE"=>$allPrices[$radio],
    "ID"=>$allId[$radio],
    "PROPERTIES_NAME"=>$allPropertiesName[$radio],
    "PROPERTIES_VALUE"=>$allPropertiesValue[$radio],
    "PROPERTIES_IMAGES"=>$allPropertiesImages[$radio],
    "STATUS" => $allStatusValue[$radio],
    "DISCOUNT" => $allDiscountValue[$radio],
    "debug" => $allPropertiesImages
);
echo json_encode($arEventFields);
?>