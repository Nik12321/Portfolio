<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$name = htmlspecialcharsbx($_POST["name"]);
$email = htmlspecialcharsbx($_POST["email"]);
$text = htmlspecialcharsbx($_POST["text"]);
$file = $_FILES["file"];
if ($APPLICATION->CaptchaCheckCode($_POST["captcha"], $_POST["captcha_sid"]))
    $captcha = "Y";
else if (empty($_POST["captcha"]))
    $captcha = "R";
else {
    $captcha = "N";
}

$captcha_code = $APPLICATION->CaptchaGetCode();

if (empty($name) || empty($email) || $captcha != "Y" || empty($text))
    $success = "N";
else
    $success = "Y";


$resc = CIBlock::GetList(Array(), Array('=CODE' => "ads"), false);
$iblock_id = 0;
if($arrc = $resc->Fetch()) {
    $iblock_id = $arrc["ID"];
}
else {
    $success = "N";
}


$arEventFields = array (
    "name"=>$name,
    "email"=>$email,
    "captcha"=>$captcha,
    "text"=>$text,
    "captcha_code"=>$captcha_code,
    "SUCCESS"=> $success
);
if ($success == "Y") {
    if(CModule::IncludeModule("iblock")) {
        $newEl = new \CIBlockElement;
        $PROP = array();
        $PROP["NAME"] = $name;
        $PROP["EMAIL"] = $email;
        $PROP["IMAGE"] = $file;
        $PROP["ADSTEXT"] = $text;
        $arLoadProductArray = Array(
            "IBLOCK_ID"      => $iblock_id,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $text,
            "ACTIVE"         => "Y",
            "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/" . $file["tmp_name"])
        );
        $newEl->Add($arLoadProductArray);
    }
}
echo json_encode($arEventFields);