<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$phone = str_replace(array("(", ")", "+7", " ", "-"), array("", "","7", "", ""), $_POST["phone"]);
if ($phone[0] == "8") {
    $phone[0] = "7";
}
$filter = Array("PERSONAL_PHONE" => $phone);
$rsUsers = CUser::GetList($by = "ID ", $order="asc", $filter); // выбираем пользователей
if ($user = $rsUsers->Fetch()) {
    if ($user["PERSONAL_PHONE"] != $phone) {
        $success = "N";
        $error = "Не найдено учетной записи с данным номером телефона";
    }
    else {
        $success = "Y";
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPASDFGHJKLZXCVBNM';
        $code = substr(str_shuffle($permitted_chars), 0, 10);
        $body = file_get_contents("https://sms.ru/sms/send?api_id=0B5EE528-4C43-3480-ED9F-C9230497B27A&to=" . $phone . "&msg=".urlencode("Новый пароль: " . $code)."&json=1");
        $updateUser = new CUser();
        $fields = Array(
            "PASSWORD"          => $code,
            "CONFIRM_PASSWORD"  => $code,
        );
        $updateUser->Update($user["ID"], $fields);
    }
}
else {
    $success = "N";
    $error = "Не найдено учетной записи с данным номером телефона";
}
$arFields = array(
    "SUCCESS" => ($success) ? $success : "N",
    "ERROR" => ($error) ? $error : ""
);
echo json_encode($arFields);