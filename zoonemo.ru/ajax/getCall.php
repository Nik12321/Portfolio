<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$name = htmlspecialcharsbx($_POST["name"]);
$phone = htmlspecialcharsbx($_POST["phoneForm"]);

if ($APPLICATION->CaptchaCheckCode($_POST["captchaCheck"], $_POST["captcha_sid"]))
    $captcha = "Y";
else if (empty($_POST["captchaCheck"]))
    $captcha = "R";
else {
    $captcha = "N";
}
$captcha_code = $APPLICATION->CaptchaGetCode();
if (empty($phone) || $captcha != "Y" )
    $success = "N";
else
    $success = "Y";

$resc = CIBlock::GetList(Array(), Array('=CODE' => "CallsFormUsers"), false);
$iblock_id = 0;
if($arrc = $resc->Fetch()) {
    $iblock_id = $arrc["ID"];
}
else {
    $success = "N";
}


$arEventFields = array (
    "name"=>$name,
    "phone"=>$phone,
    "captcha"=> $captcha,
    "captcha_code"=>$captcha_code,
    "SUCCESS"=> $success
);
if ($success == "Y") {
    if(CModule::IncludeModule("iblock")) {
        $newEl = new \CIBlockElement;
        $PROP = array();
        $PROP[42] = $phone;
        $PROP[43] = $name;
        $cnt = CIBlockElement::GetList(
            array(),
            array("IBLOCK_ID"=>$iblock_id),
            array(),
            false,
            array('ID', 'NAME'));
        $cnt += 1;
        $code = "callOrder_" .  $cnt;
        $recordName = "Заказанный звонок - " . $cnt;
        $arLoadProductArray = Array(
            "MODIFIED_BY"    => "", // элемент изменен текущим пользователем
            "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
            "IBLOCK_ID"      => $iblock_id,
            "CODE" => $code,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $recordName,
            "ACTIVE"         => "Y",
            "DETAIL_TEXT"    => "",
            "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/" . $file["tmp_name"])
        );
        $newEl->Add($arLoadProductArray);


        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];

        $arEventFieldsSend = array(
            "NAME" => $name,
            "PHONE" =>  $phone,
            "DEFAULT_EMAIL_FROM" => $mail_to,
            "SITE_NAME" => "",
            "SERVER_NAME" => $EMAIL,
        );
        CEvent::Send("NEW_PHONE_REQUEST", "s1", $arEventFieldsSend, "Y", "", $file);

    }
}
echo json_encode($arEventFields);