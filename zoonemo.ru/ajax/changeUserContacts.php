<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$NAME = htmlspecialcharsbx($_POST["NAME"]);
$LAST_NAME = htmlspecialcharsbx($_POST["LAST_NAME"]);
$SECOND_NAME = htmlspecialcharsbx($_POST["SECOND_NAME"]);
$PERSONAL_STATE= htmlspecialcharsbx($_POST["PERSONAL_STATE"]);
$PERSONAL_CITY = htmlspecialcharsbx($_POST["PERSONAL_CITY"]);
$PERSONAL_ZIP = htmlspecialcharsbx($_POST["PERSONAL_ZIP"]);
$PERSONAL_STREET = htmlspecialcharsbx($_POST["PERSONAL_STREET"]);
$UF_HOMENUMBER = htmlspecialcharsbx($_POST["UF_HOMENUMBER"]);
$PERSONAL_COUNTRY = htmlspecialcharsbx($_POST["UF_COUNTRY"]);
$UF_HOUSING = htmlspecialcharsbx($_POST["UF_HOUSING"]);
$UF_NOSQUARE = htmlspecialcharsbx($_POST["UF_NOSQUARE"]);
$UF_ENTRANCE = htmlspecialcharsbx($_POST["UF_ENTRANCE"]);
$UF_FLOOR = htmlspecialcharsbx($_POST["UF_FLOOR"]);

global $USER;
$ID = $USER->GetID();





$success = 1;
$arEventFields = array (
    "NAME" => $NAME,
    "LAST_NAME" =>$LAST_NAME ,
    "SECOND_NAME"=> $SECOND_NAME,
    "PERSONAL_STATE" => $PERSONAL_STATE,
    "UF_COUNTRY" => $PERSONAL_COUNTRY,
    "UF_HOMENUMBER" => $UF_HOMENUMBER,
    "PERSONAL_CITY" => $PERSONAL_CITY,
    "PERSONAL_ZIP" => $PERSONAL_ZIP,
    "PERSONAL_STREET" => $PERSONAL_STREET,
    "UF_HOUSING"=> $UF_HOUSING,
    "UF_NOSQUARE"=> $UF_NOSQUARE,
    "UF_ENTRANCE"=> $UF_ENTRANCE,
    "UF_FLOOR" => $UF_FLOOR,
);

$resultEvent = array("success" => $success);
if ($success == 1)
    $USER->Update($ID, $arEventFields);

echo json_encode($resultEvent);
?>