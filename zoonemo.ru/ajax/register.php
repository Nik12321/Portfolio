<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$login = $email = htmlspecialcharsbx($_POST["REGISTER"]["EMAIL"]);
$password = htmlspecialcharsbx($_POST["password"]);
$confirm = htmlspecialcharsbx($_POST["REGISTER"]["CONFIRM_PASSWORD"]);
if ($APPLICATION->CaptchaCheckCode($_POST["captcha_word"], $_POST["captcha_sid"]))
    $captcha = "Y";
else if (empty($_POST["captcha_word"]))
    $captcha = "R";
else {
    $captcha = "N";
}

$captcha_code = $APPLICATION->CaptchaGetCode();
if (strlen($password) < 6)
    $passwordLengthError = 1;
else
    $passwordLengthError = 0;
if ($password != $confirm)
    $confirmError = 1;
else
    $confirmError = 0;
if (!preg_match('/[a-zA-Z][a-zA-Z0-9]+@[a-zA-Z]+[a-z-A-Z0-9]\.(ru|com)/', $email))
    $emailError = 1;
else
    $emailError = 0;
if ($passwordLengthError ||  $confirmError || $emailError || $captcha != "Y")
    $success = "F";
else {
    global $USER;
    $arResult = $USER->Register($login, "", "",  $password, $confirm, $email,SITE_ID,  $captcha_word = $_POST["captcha_word"], $captcha_sid = $_POST["captcha_sid"]);
    if($arResult["TYPE"] == "ERROR") {
        $success = "F";
        $alreadyExist = $arResult["MESSAGE"];
    }
    else {
        $ID = $USER->GetID();
        $USER->Logout();
        $permittedChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $confirmCode =  substr(str_shuffle($permittedChars), 0, 8);
        $USER->Update($ID, array ("ACTIVE" => "N", "CONFIRM_CODE" => $confirmCode));

        $rsUsers = CUser::GetList($by = "NAME", $order="asc", array("ID" => $ID)); // выбираем пользователей
        $user = $rsUsers->Fetch();


        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];
        $arEventFields = array(
            "USER_ID" => $ID,
            "EMAIL" => $user["EMAIL"],
            "CONFIRM_CODE" => $user["CONFIRM_CODE"],
            "DEFAULT_EMAIL_FROM" => $mail_to,
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => $_SERVER["SERVER_NAME"]
        );
        logToFile("/_log/userdata.txt", $arEventFields);
        logToFile("/_log/data.txt", $user);
        CEvent::Send("NEW_USER_CONFIRM", "s1", $arEventFields, "Y");

        $alreadyExist = 0;
        $success = "Y";
    }
}
if (!$alreadyExist)
    $alreadyExist = 0;
$arEventFields = array (
    "passwordLengthError"=>$passwordLengthError,
    "confirmError"=> $confirmError,
    "captchaError" => $captcha,
    "emailError"=> $emailError,
    "alreadyExist"=>$alreadyExist,
    "captcha_code" => $captcha_code,
    "SUCCESS"=>$success
);
echo json_encode($arEventFields);
?>