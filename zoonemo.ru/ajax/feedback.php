<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$name = htmlspecialcharsbx($_POST["name"]);
$phone= htmlspecialcharsbx($_POST["phone"]);
$comment = htmlspecialcharsbx($_POST["comment"]);

if ($APPLICATION->CaptchaCheckCode(htmlspecialcharsbx($_POST["captcha_contact"]), htmlspecialcharsbx($_POST["captcha_sid_contact"]))) {
    $captcha = "Y";
} else if (empty($_POST["captcha_contact"])) {
    $captcha = "R";
} else {
    $captcha = "N";
}

if (empty($name) || empty($phone) || empty($comment) || $captcha != "Y") {
    $success = "N";
} else {
    $success = "Y";
}

$resc = CIBlock::GetList(Array(), Array('=CODE' => "Feedback"), false);
$iblock_id = 0;
if($arrc = $resc->Fetch()) {
    $iblock_id = $arrc["ID"];
} else {
    $success = "N";
}


$captcha_code = $APPLICATION->CaptchaGetCode();
$arEventFields = [
    "name"=>$name,
    "phone"=>$phone,
    "comment"=>$comment,
    "captcha" => $captcha,
    "captcha_code"=>$captcha_code,
    "SUCCESS"=> $success
];

if ($success == "Y") {
    $arAttr = array (
        "NAME"=>$name,
        "PHONE"=>$phone,
        "COMMENT"=>$comment,
    );
    CEvent::Send("GET_MESSAGE_FROM_USER", "s1", $arAttr, "N", "", array($id));
    if(CModule::IncludeModule("iblock")) {
        $newEl = new \CIBlockElement;
        $PROP = array();
        $PROP["PHONE"] = $phone;
        $PROP["NAME"] = $name;
        $PROP["MESSAGE"] = $comment;
        $arLoadProductArray = [
            "IBLOCK_ID"      => $iblock_id,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $comment,
            "ACTIVE"         => "Y"
        ];
        $newEl->Add($arLoadProductArray);
        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];
        $arEventSendFields = [
            "NAME" => $name,
            "PHONE" => $phone,
            "MESSAGE" => $comment,
            "DEFAULT_EMAIL_FROM" => $mail_to,
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => ""
        ];
        CEvent::Send("FEEDBACK_FORM_REQUEST", "s1", $arEventSendFields, "N", "");
    }
}
echo json_encode($arEventFields);