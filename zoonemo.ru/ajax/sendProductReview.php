<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$author = htmlspecialcharsbx($_POST["author"]);
$email = htmlspecialcharsbx($_POST["email"]);
$content = htmlspecialcharsbx($_POST["content"]);
$productID = htmlspecialcharsbx($_POST["productID"]);
if ($APPLICATION->CaptchaCheckCode($_POST["captcha"], $_POST["captcha_sid"]))
    $captcha = "Y";
else if (empty($_POST["captcha"]))
    $captcha = "R";
else {
    $captcha = "N";
}

$captcha_code = $APPLICATION->CaptchaGetCode();

if (!$author || !$email || !$content || $captcha != "Y" || !$productID || $email == -1)
    $success = "N";
else
    $success = "Y";

$resc = CIBlock::GetList(Array(), Array('=CODE' => "ProductReview"), false);
$iblock_id = 0;
if($arrc = $resc->Fetch()) {
    $iblock_id = $arrc["ID"];
}
else {
    $success = "N";
}

$arEventFields = array (
    "author"=>$author,
    "email"=>$email,
    "content"=>$content,
    "captcha"=>$captcha,
    "captcha_code"=>$captcha_code,
    "SUCCESS"=> $success,
    "productID"=>$productID
);

if ($success == "Y") {
    if(CModule::IncludeModule("iblock")) {
        $newEl = new \CIBlockElement;
        $PROP = array();
        $PROP["NAME"] = $author;
        $PROP["PRODUCT_ID"] = $productID;
        $PROP["EMAIL"] = $email;
        $PROP["REVIEW"] = $content;
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
            "IBLOCK_ID"      => $iblock_id,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $content,
            "ACTIVE"         => "Y"
        );
        $newEl->Add($arLoadProductArray);
    }
}
echo json_encode($arEventFields);
?>