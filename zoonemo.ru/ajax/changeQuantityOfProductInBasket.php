<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
$price = $_POST["price"];
$id = $_POST["id"];
$quantity = $_POST["quantity"];
$operation = $_POST["operation"];
$return = array();
if ($id) {
    $dbBasketItems = CSaleBasket::GetList(false, array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "PRODUCT_ID" => $id, "LID" => SITE_ID, "ORDER_ID" => "NULL", "DELAY" => "N"), false, false, array("ID", "QUANTITY"));
    if ($arItems = $dbBasketItems->Fetch()) {
        $arFields = array("QUANTITY" =>   ($operation == "plus") ? ($arItems["QUANTITY"] + 1) : ($arItems["QUANTITY"] - 1));
        if (CSaleBasket::Update($arItems["ID"], $arFields)) {
            $success = "Y";
        }
        else {
            $success = "N";
            $errorMessage = "Не удалось изменить запись в корзине (UPDATE error)";
        }
    }
    else if ($operation == "plus") {
        Add2BasketByProductID($id,1);
        $success = "Y";
    }
}
else {
    $success = "N";
    $errorMessage = "Не удается получить id товара";
}
$arEventFields = array (
    "errorMessage" => $errorMessage,
    "SUCCESS"=> $success
);
echo json_encode($arEventFields);