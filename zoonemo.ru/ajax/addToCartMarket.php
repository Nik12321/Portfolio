<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$quantity = $_POST["quantity"];
$cost = $_POST["cost"];
$id = $_POST["id"];

if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog"))
{
    $q = 0;
    if (is_array($cost)) {
        for ($i = 0; $i < count($cost); $i++) {
            if ($quantity[$i] == 0)
                continue;
            $arFields = array(
                "PRODUCT_ID" => $id[$i],
                "PRODUCT_PRICE_ID" => 0,
                "PRICE" => $cost[$i],
                "CURRENCY" => "RUB",
                "QUANTITY" => $quantity[$i],
                "LID" => LANG,
                "DELAY" => "N",
                "CAN_BUY" => "Y"
            );
            $code = Add2BasketByProductID($id[$i], $quantity[$i]);
        }
    }
    else {
        $arFields = array(
            "PRODUCT_ID" => $id,
            "PRODUCT_PRICE_ID" => 0,
            "PRICE" => $cost,
            "CURRENCY" => "RUB",
            "QUANTITY" => $quantity,
            "LID" => LANG,
            "DELAY" => "N",
            "CAN_BUY" => "Y"
        );
        $code = Add2BasketByProductID($id, $quantity);
    }
    echo $code;
}
else
    echo "";