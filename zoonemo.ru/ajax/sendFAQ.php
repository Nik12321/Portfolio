<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$title = htmlspecialcharsbx($_POST["title"]);
$name = htmlspecialcharsbx($_POST["name"]);
$email = htmlspecialcharsbx($_POST["email"]);
$text = htmlspecialcharsbx($_POST["text"]);


if ($APPLICATION->CaptchaCheckCode($_POST["captcha"], $_POST["captcha_sid"]))
    $captcha = "Y";
else if (empty($_POST["captcha"]))
    $captcha = "R";
else {
    $captcha = "N";
}

$captcha_code = $APPLICATION->CaptchaGetCode();

if (empty($name) || empty($title) || empty($email) || $captcha != "Y" || empty($text))
    $success = "N";
else
    $success = "Y";


$resc = CIBlock::GetList(Array(), Array('=CODE' => "FAQ"), false);
$iblock_id = 0;
if($arrc = $resc->Fetch()) {
    $iblock_id = $arrc["ID"];
}
else {
    $success = "N";
}


$arEventFields = array (
    "title"=>$title,
    "name"=>$name,
    "email"=>$email,
    "captcha"=>$captcha,
    "text"=>$text,
    "captcha_code"=>$captcha_code,
    "SUCCESS"=> $success
);
if ($success == "Y") {
    if(CModule::IncludeModule("iblock")) {
        $newEl = new \CIBlockElement;
        $PROP = array();
        $PROP["THEME"] = $title;
        $PROP["NAME"] = $name;
        $PROP["EMAIL"] = $email;
        $PROP["QUESTION"] = $text;
        $arLoadProductArray = Array(
            "IBLOCK_ID"      => $iblock_id,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $text,
            "ACTIVE"         => "Y"
        );

        $newEl->Add($arLoadProductArray);
        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];
        $arEventFieldsMessage = array(
            "THEME" => $PROP["THEME"],
            "EMAIL" =>  $PROP["EMAIL"],
            "NAME" => $PROP["NAME"],
            "TITLE" => "Пользователь заполнил форму 'Вопрос-ответ'!",
            "MESSAGE" => "Вопрос: " .$PROP["QUESTION"],
            "DEFAULT_EMAIL_FROM" => $mail_to,
            "SITE_NAME" =>  $arSite["NAME"],
            "SERVER_NAME" => ""

        );
        $arEventFieldsMessage["MESSAGE"] .= "<br>Связаться с пользователем можно по почте: " . $PROP["EMAIL"];
        CEvent::Send("USER_USED_FAQ_OR_REVIEW_FORM", "s1", $arEventFieldsMessage, "N", "");

    }
}
echo json_encode($arEventFields);
?>