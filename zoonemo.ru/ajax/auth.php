<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$login = htmlspecialcharsbx($_POST["USER_LOGIN"]);
$password = htmlspecialcharsbx($_POST["USER_PASSWORD"]);
if (strlen($login) == 0)
    $loginLengthError = 1;
else
    $loginLengthError = 0;
if (strlen($password) == 0)
    $passwordLengthError = 1;
else
    $passwordLengthError = 0;


//if ($loginLengthError || $passwordLengthError)  {
//    $success = "F";
//}
//else {
//    global $USER;
//    $arAuthResult = $USER->Login($login, $password, "N");
//    if ($arAuthResult["TYPE"] == "ERROR") {
//        $alreadyExist = $arAuthResult["MESSAGE"];
//        $success = "N";
//    }
//    else {
//        $alreadyExist = 0;
//        $success = "Y";
//    }
//}

if ($loginLengthError || $passwordLengthError)  {
    $success = "F";
}
else {
    $rsUser = CUser::GetByLogin($login);
    if ($arUser = $rsUser->Fetch())
    {
        if(strlen($arUser["PASSWORD"]) > 32)
        {
            $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
            $db_password = substr($arUser["PASSWORD"], -32);
        }
        else
        {
            $salt = "";
            $db_password = $arUser["PASSWORD"];
        }

        $user_password =  md5($salt.$password);

        global $USER;
        if ( $user_password == $db_password )
        {
            $login_password_correct = true;
            $USER->Authorize($arUser["ID"]);
            $success = "Y";
        }
        else {
            $arAuthResult = $USER->Login($login, $password, "N");
            $alreadyExist = $arAuthResult["MESSAGE"];
            $success = "N";
        }
    }
    else {
        $success = "N";
        $alreadyExist = "Неверный логин или пароль.";
    }
}

$arEventFields = array (
    "loginLengthError"=>$loginLengthError,
    "passwordLengthError"=>$passwordLengthError,
    "alreadyExist"=>$alreadyExist,
    "SUCCESS"=>$success
);
echo json_encode($arEventFields);
?>