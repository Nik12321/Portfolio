<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$code = $_POST["code"];
$userCode = $_POST["userCode"];
$phone = $_POST["phone"];
global $USER;
$userClass = new CUser;
if ($phone && $userCode && $code) {
    if ($userCode == $code) {
        $phone = str_replace(array("(", ")", "+7", " ", "-"), array("", "","7", "", ""), $phone);
        $phone[0] = "7";

        $user = CUser::GetList($by = "NAME", $order="asc",  Array("LOGIN" => $phone))->Fetch();
        if ($user) {
            if ($USER->Authorize($user["ID"])) {
                echo json_encode(array(
                    "SUCCESS" => "Y",
                    "MESSAGE" => "Найден существующий пользователь"
                ));
            }
            else {
                echo json_encode(array(
                    "SUCCESS" => "N",
                    "MESSAGE" => "Не удалось авторизовать найденного пользователя. Перезагрузите страницу и попробуйте снова"
                ));
            }
        }
        else {
            if ($ID = $userClass->Add(array("LOGIN" => $phone, "EMAIL"=> $phone . "@zoonemo.ru", "PASSWORD" => "FAKEPASSWORD", "CONFIRM_PASSWORD" => "FAKEPASSWORD", "PERSONAL_PHONE"=> $phone))) {
                echo json_encode(array(
                    "SUCCESS" => "Y",
                    "MESSAGE" => "Создан новый пользователь"
                ));
                $USER->Authorize($ID);
            }
            else {
                echo json_encode(array(
                    "SUCCESS" => "N",
                    "LAST_ERROR" => $userClass->LAST_ERROR,
                    "MESSAGE" => "Не удалось создать профиль для введенного номера телефона"
                ));
            }
        }
    }
    else {
        echo json_encode(array(
            "SUCCESS" => "N",
            "MESSAGE" => "Неверный код",
            "CODE" => $code,
            "USER_CODE" => $userCode
        ));
    }
}
else {
    echo json_encode(array(
        "SUCCESS" => "N",
        "MESSAGE" => "Не удалось получить введенный код и/или номер телефона. Перегрузите страницу и попробуйте снова"
    ));
}