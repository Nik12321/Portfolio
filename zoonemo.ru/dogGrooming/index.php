<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Стрижка собак");
?>
    <h1 class="page_title">Стрижка собак</h1>
<?php
$APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/include/dogGrooming.php",
    "COMPONENT_TEMPLATE" => ".default"
],
    false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>