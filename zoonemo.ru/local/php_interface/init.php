<?php
define("DEFAULT_TEMPLATE_PATH", "/local/templates/.default");
CModule::IncludeModule("iblock");
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');
CModule::IncludeModule('itconstruct.uncachedarea');
$incFile = __DIR__."/agents.php"; if(file_exists($incFile)) require_once($incFile);
$incFile = __DIR__."/AllProductDiscount.php"; if(file_exists($incFile)) require_once($incFile);
$incFile = __DIR__."/uncached_handlers.php"; if(file_exists($incFile)) require_once($incFile);



AddEventHandler("sale", "OnBeforeOrderAdd","getCorrectUser");
//Обработчик проверяет, существует ли пользователь с данной почтой и привязывает заказ к нему
function getCorrectUser(&$arFields)
{
    //Если не существует id пользователя в заказе - возвращаем false
    if (!$arFields["USER_ID"]) {
        return false;
    }
    $alreadyExistedUser = "";
    $user = new CUser;
    //Ищем созданного пользователя по id
    $rsUsers = CUser::GetList($by = "NAME", $order = "desc", $filter = array("ID" => $arFields["USER_ID"]));
    if ($arUser = $rsUsers->Fetch()) {
        $dateRegister = new \Bitrix\Main\Type\DateTime($arUser["DATE_REGISTER"]);
        $dateLogin = new \Bitrix\Main\Type\DateTime($arUser["LAST_LOGIN"]);
        $time = $dateLogin->getTimestamp() - $dateRegister->getTimestamp();
        //Если у пользователя логин и персональный телефон совпадают - то пользователь 100% существует и успешно авторизован
        if ($arUser["PERSONAL_PHONE"] == $arUser["LOGIN"] || $time >= 2) {
            return true;
        }
        //Если нет, мы пробуем найти по логину другие учетные записи
        $phone = str_replace(
            ["(", ")", "+7", " ", "-"],
            ["", "","7", "", ""],
            $arFields["ORDER_PROP"][3]
        );
        $phone[0] = "7";
        $user->Update($arFields["USER_ID"], Array("PERSONAL_PHONE" => $phone));
        $rsUsers2 = CUser::GetList($by = "NAME", $order = "desc", $filter = array("LOGIN" => $phone));
        while ($arUser2 = $rsUsers2->Fetch()) {
            //Если id учетной записи не совпадает с id созданного пользователя, значит, это исходный аккаунт.
            if ($arFields["USER_ID"] != $arUser2["ID"])  {
                $alreadyExistedUser = $arUser2;
                break;
            }
        }
        //Если мы нашли аккаунт с зарегистрированной почтой, то удаляем созданный аккаунт и меняем параметры у созданного заказа
        if ($alreadyExistedUser["ID"]) {
            $arFields["USER_ID"] = $alreadyExistedUser["ID"];
            $fields = Array(
                "PERSONAL_PHONE" => $phone
            );
            if (!$user->Update($arFields["USER_ID"], $fields)) {
                logToFile("/_logs/testOrder.txt", "Не удалось обновить свойство для существующего пользователя");
            }
        } else {  //Иначе - меняем логин пользователя:
            $fields = Array(
                "LOGIN" => $phone,
                "EMAIL" => $phone . "@zoonemo.ru",
                "PERSONAL_PHONE" => $phone
            );
            if (!$user->Update($arFields["USER_ID"], $fields)) {
                logToFile("/_logs/testOrder.txt", "Не удалось обновить свойство для нового пользователя");
            }
        }
    } else {
        return false;
    }
}


AddEventHandler("sale", "OnSaleComponentOrderOneStepComplete","getCorrectDataForOrderAndDeleteUser");
//Обработчик проверяет, есть ли пользователь с данным емайлом, но без заказов, и удаляет его
//При оформлении заказа добавляет свойство *внешний код* для отображения в админке
function getCorrectDataForOrderAndDeleteUser($ID, $arOrder, $arParams)
{
    $dbBasketItems = CSaleBasket::GetList(
        ["NAME" => "ASC", "ID" => "ASC"],
        ["ORDER_ID" => $ID],
        false,
        false,
        ["PRODUCT_ID", "PRODUCT_XML_ID", "NAME", "ID"]
    );
    while ($arItems = $dbBasketItems->GetNext()) {
        $arRes = CIBlockElement::GetList(
            false,
            ["ID" => $arItems["PRODUCT_ID"], "IBLOCK_ID" => [13,14]],
            false,
            false,
            ["*"]);
        while ($arOb = $arRes->GetNextElement()) {
            $prop = $arOb->GetProperties();
            if ($prop["article"]["VALUE"]) {
                $xml_id = 0;
                if (
                    preg_match('/^\w*/', $arItems["PRODUCT_XML_ID"], $matches) === 1
                    && $arOb->GetFields()["IBLOCK_ID"] == 14
                ) {
                    $xml_id = preg_replace('/^\w*#/', '', $arItems["PRODUCT_XML_ID"], -1, $count);
                } else {
                    $xml_id = $arItems["PRODUCT_XML_ID"];
                }
                $arFields = [
                    "PROPS" => [
                        ["NAME" => "Внешний код", "CODE" => "external_id", "VALUE" => $xml_id]
                    ],
                    "NAME" => $arItems["NAME"] . " " . $prop["article"]["VALUE"],
                ];
                CSaleBasket::Update($arItems["ID"], $arFields);
            }
        }
    }

    $userDB = CUser::GetList(
        $by = "NAME",
        $order = "desc",
        $filter = ["ID" => $arOrder["USER_ID"]]
    );
    if ($userData = $userDB->Fetch()) {
        if ($userData["PERSONAL_PHONE"]) {
            $rsUsers = CUser::GetList(
                $by = "NAME",
                $order = "desc",
                $filter = ["PERSONAL_PHONE" => $userData["PERSONAL_PHONE"]]
            );
            while ($arUser = $rsUsers->Fetch()) {
                if ($arUser["ID"] != $arOrder["USER_ID"]) {
                    CUser::Delete($arUser["ID"]);
                }
            }
        }
    }
}


AddEventHandler("iblock", "OnIBlockElementUpdate", "checkProductStatus");
//Обработчик проверяет, изменилось ли свойство *статус наличия* с отрицательного на положительное
function checkProductStatus($newFields)
{
    if ($newFields["ID"]) {
        //Находим товар
        $res = CIBlockElement::GetList(false, array("ID" => $newFields["ID"]), false, false);
        if ($ob = $res->GetNextElement()) {
            $properties = $ob->GetProperties();
            if ($newFields["PROPERTY_VALUES"][$properties["status"]["ID"]]) {
                $old = $properties["status"]["VALUE"];
                $new = -100;
                foreach ($newFields["PROPERTY_VALUES"][$properties["status"]["ID"]] as $val) {
                    $new = $val["VALUE"];
                    break;
                }
                //Если старый статус = -1, а новый > -1 - делаем рассылку
                if ($old == -1 && $new >= 0) {
                    $fields = $ob->GetFields();
                    //Фунцкция рассылки
                    \Bitrix\Catalog\SubscribeTable::runAgentToSendNotice($fields["ID"]);
                }
            }
        }
    }
}


AddEventHandler("main", "OnBeforeUserLogin", "OnBeforeUserLogin");
function OnBeforeUserLogin(&$arFields)
{
    logToFile("/_logs/test.txt", 123123);
	if (check_email($arFields["LOGIN"])) { // вход по емейлу
		if ($user = \Bitrix\Main\UserTable::getList(array("filter" => array("=EMAIL" => $arFields["LOGIN"])))->Fetch()) {
			$arFields["LOGIN"] = $user["LOGIN"];
		}
	} else {
		$str = $arFields["LOGIN"];

		// вход по номеру телефона
		if (strlen($str) > 5) {
			if ($user = \Bitrix\Main\UserTable::getList(array("filter" => array("=PERSONAL_PHONE" => $str)))->Fetch()) {

				$needConfirmPhone = \Bitrix\Main\Config\Option::get("grain.customsettings","need_phone_confirm") == "Y";

				if($needConfirmPhone) {
					$rsUser = CUser::GetByID($user["ID"]);
					$arUser = $rsUser->Fetch();

					if($arUser['UF_PHONE_STATUS']) {
						$arFields["LOGIN"] = $user["LOGIN"];
					}
				} else {
					$arFields["LOGIN"] = $user["LOGIN"];
				}
			}
		}
	}
}


AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserRegisterHandler");
// обработчик, который меняет данные пользователя после регистрации. В данном случае логин и почта должны совпадать
function OnBeforeUserRegisterHandler(&$arFields)
{
	$arFields["LOGIN"] = $arFields["EMAIL"];
}


AddEventHandler("iblock", "OnBeforeIBlockElementDelete","CheckOwnerBeforeDeleteElement");
//Запрет на удаление товаров, но не торговых предложений
function CheckOwnerBeforeDeleteElement($ID)
{
    global $APPLICATION;
    $rsElement = CIBlockElement::GetByID($ID);
    $arElement = $rsElement->GetNext();

    if ($arElement["IBLOCK_ID"] == 13) {
        $APPLICATION->ThrowException('Вы не можете удалить обычные товары, только деактивировать!');
        return false;
    }
}


AddEventHandler("iblock", "OnBeforeIBlockSectionDelete","CheckOwnerBeforeDeleteSection");
//Запрет на удаление разделов
function CheckOwnerBeforeDeleteSection($ID)
{
    global $APPLICATION;
    $APPLICATION->ThrowException('Вы не можете удалить разделы, только деактивировать!');
    return false;
}


AddEventHandler("main", "OnEndBufferContent", "deleteKernelJs");
//Оптимизирует размер js
function deleteKernelJs(&$content) {
    global $USER, $APPLICATION;
    if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
    if($APPLICATION->GetProperty("save_kernel") == "Y") return;
    $arPatternsToRemove = Array(
        '/<script.+?src=".+?kernel_main\/kernel_main\.js\?\d+"><\/s cript\>/',
        '/<script.+?src=".+?bitrix\/js\/main\/core\/core[^"]+"><\/s cript\>/',
        '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/s cript>/',
        '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/s cript>/',
        '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/s cript>/',
    );
    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

AddEventHandler("main", "OnEndBufferContent", "deleteKernelCss");
//Оптимизирует размер css
function deleteKernelCss(&$content)
{
    global $USER, $APPLICATION;
    if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
    if($APPLICATION->GetProperty("save_kernel") == "Y") return;

    $arPatternsToRemove = Array(
        '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/js\/main\/core\/css\/core[^"]+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/styles.css[^"]+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/template_styles.css[^"]+"[^>]+>/',
    );

    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

AddEventHandler("main", "OnProlog", "oldURLRedirects");
//Обработка редиректов с ссылок старого сайта на ссылки нового
function oldURLRedirects()
{
    $matches = array();
    $url = $_SERVER["REDIRECT_URL"];
        if (preg_match('/^(\/shop\/goods\/\d+\/?(har|comments|instruction|about)*)$/', $url, $matches) === 1) {
        $url = str_replace(array("/about", "/har", "/comments", "/instructions"), array("#about", "#har", "#comments", "#instructions"), $url);
        preg_match('/\d+/', $url, $matches);
        $id = $matches[0];
        if ($id) {
            $arFilter = Array("EXTERNAL_ID" => $id, "IBLOCK_ID" => 13); //Фильтр по внешнему коду
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("ID", "IBLOCK_SECTION_ID"));
            while ($ob = $res->GetNextElement()) {
                $field = $ob->GetFields();
                $str = preg_replace('/^(\/shop\/goods\/\d+\/?)/', "/shop/products/" . $field["IBLOCK_SECTION_ID"] . "/" . $field["ID"], $url);
                if ($str !== NULL) {
                    LocalRedirect("https://www.zoonemo.ru" . $str,"301");
                }
            }
        }
    } elseif (preg_match('/^(\/shop\/goods\/category\/\d+\/?)$/', $url, $matches) === 1) {
        preg_match('/\d+/', $url, $matches);
        $id = $matches[0];
        if ($id) {
            $arFilter = Array("EXTERNAL_ID" => $id, "IBLOCK_ID" => 13); //Фильтр по внешнему коду
            $res = CIBlockSection::GetList(Array(), $arFilter, false, false, Array("ID"));
            while ($ob = $res->GetNextElement()) {
                $field = $ob->GetFields();
                $str = preg_replace('/^(\/shop\/goods\/category\/\d+\/?)$/', "/shop/products/" . $field["ID"] . "/", $url);
                if ($str !== NULL) {
                    LocalRedirect("https://www.zoonemo.ru" . $str,"301");
                }
            }
        }
    }
    if (preg_match('/(iblock_element_edit.php)$/', $_SERVER["SCRIPT_NAME"], $matches) === 1) {
        if ($_GET["type"] === "Offers" && $_GET["ID"] && $_GET["IBLOCK_ID"] == 14) {
            $offerID = $_GET["ID"];
            $mxResult = CCatalogSku::GetProductInfo($offerID);
            if ($mxResult["ID"]) {
                $url = "iblock_element_edit.php?IBLOCK_ID=13&type=Catalogs&ID=" . $mxResult["ID"];
                unset($_GET["IBLOCK_ID"]);
                unset($_GET["ID"]);
                unset($_GET["type"]);
                foreach ($_GET as $key => $value) {
                    $url .= "&" . $key . "=" . $value;
                }
                LocalRedirect($url);
            }
        }
    }
}


AddEventHandler("sale", "OnSaleStatusOrder", "OrderHandler");
//При изменении статуса заказа обрабатывает отправку почтовых сообщений
function OrderHandler($order_id, $status)
{
    if ($status == "RE") {
        // Получаем параметры заказа
        $arOrder = CSaleOrder::GetByID($order_id);
        //Получаем свойства заказа
        $dbOrderProps = CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
            )
        );

        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];
        $arStatus = CSaleStatus::GetByID($status);
        $arStatus_opis = $arStatus["DESCRIPTION"];
        $arStatus = $arStatus["NAME"];
        $EMAIL = "";
        $file  = "";
        while ($arProps = $dbOrderProps->Fetch()) {
            if($arProps["CODE"] == "email") {
                $EMAIL = $arProps["VALUE"];
            }
            if ($arProps["NAME"] == "Накладная с заказом") {
                $file = $arProps["VALUE_ORIG"];
            }
        }
        $arEventFields = array(
            "ORDER_ID" => $order_id,
            "ORDER_DATE" =>  date('Y-m-d H:i:s'),
            "ORDER_STATUS" => $arStatus,
            "ORDER_DESCRIPTION" => $arStatus_opis,
            "EMAIL" => $EMAIL,
            "TEXT" => "",
            "SALE_EMAIL" => $mail_to,
            "ORDER_PUBLIC_URL" => "",
            "DEFAULT_EMAIL_FROM" => "",
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => "",
            "SALE" => (intval($arOrder["PRICE"]) - intval($arOrder["PRICE_DELIVERY"])) . " " . $arOrder["CURRENCY"],
        );
        CEvent::Send("PAYMENT_PENDING_STATUS", "s1", $arEventFields, "N", "", $file);
    }
    if ($status == "N") {
        // Получаем параметры заказа
        $arOrder = CSaleOrder::GetByID($order_id);
        //Получаем свойства заказа
        $dbOrderProps = CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
            )
        );

        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];


        $arStatus = CSaleStatus::GetByID($status);

        $arStatus_opis = $arStatus["DESCRIPTION"];
        $arStatus = $arStatus["NAME"];

        $EMAIL = "";
        $PHONE = "";
        $FIO = "";
        while ($arProps = $dbOrderProps->Fetch()) {
            if($arProps["CODE"] == "email") {
                $EMAIL = $arProps["VALUE"];
            }
            if($arProps["CODE"] == "phone") {
                $PHONE = $arProps["VALUE"];
            }
            if($arProps["CODE"] == "fio") {
                $FIO = $arProps["VALUE"];
            }
        }
        $arEventFields = array(
            "ORDER_ID" => $order_id,
            "ORDER_DATE" =>  date('Y-m-d H:i:s'),
            "ORDER_STATUS" => $arStatus,
            "ORDER_DESCRIPTION" => $arStatus_opis,
            "EMAIL" => $EMAIL,
            "TEXT" => "",
            "SALE_EMAIL" => $mail_to,
            "ORDER_PUBLIC_URL" => "",
            "DEFAULT_EMAIL_FROM" => "",
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => "",
            "SALE" => (intval($arOrder["PRICE"]) - intval($arOrder["PRICE_DELIVERY"])) . " " . $arOrder["CURRENCY"],
            "USER_NAME" => $FIO,
            "PHONE" => $PHONE
        );
        CEvent::Send("NEW_ORDER_STATUS", "s1", $arEventFields, "N");
    }

    if ($status == "F") {
        //Получаем свойства заказа
        $dbOrderProps = CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
                "CODE" => array("email")
            )
        );

        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];


        $EMAIL = "";
        while ($arProps = $dbOrderProps->Fetch()) {
            if($arProps["CODE"] == "email") {
                $EMAIL = $arProps["VALUE"];
            }
        }
        $text = "";
        $dbBasketItems = CSaleBasket::GetList(
           false,
            array(
                "ORDER_ID" => $order_id
            ),
            false,
            false,
            array("NAME", "DETAIL_PAGE_URL")
        );
        while ($arItems = $dbBasketItems->Fetch()) {
            $text .= "Товар: " . "<a href='" . $arItems["DETAIL_PAGE_URL"] ."'>". $arItems["NAME"] . "</a><br>";
        }
        $arEventFields = array(
            "ORDER_ID" => $order_id,
            "ORDER_DATE" =>  date('Y-m-d H:i:s'),
            "EMAIL" => $EMAIL,
            "TEXT" => $text,
            "SALE_EMAIL" => $mail_to,
            "DEFAULT_EMAIL_FROM" => "",
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => "",
        );
        CEvent::Send("SALE_SUCCESS_END", "s1", $arEventFields, "N");
    }


    if ($status == "FU") {
        //Получаем свойства заказа
        $dbOrderProps = CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id,
                "CODE" => array("email")
            )
        );
        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];
        $EMAIL = "";
        while ($arProps = $dbOrderProps->Fetch()) {
            if($arProps["CODE"] == "email") {
                $EMAIL = $arProps["VALUE"];
            }
        }
        $arEventFields = array(
            "ORDER_ID" => $order_id,
            "ORDER_DATE" =>  date('Y-m-d H:i:s'),
            "SALE_EMAIL" => $mail_to,
            "EMAIL" => $EMAIL,
            "DEFAULT_EMAIL_FROM" => "",
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => "",
        );
        CEvent::Send("SALE_USER_CANNOT_ANSWER", "s1", $arEventFields, "Y");
    }
}


AddEventHandler("sale", "OnSaleDeliveryOrder", "DeliveryHandler");
//При изменении статуса доставки обрабатывает отправление соответсвующих почтовых уведомлений
function DeliveryHandler($order_id, $status)
{
    if ($status == "Y") {
        //Получаем свойства заказа
        $dbOrderProps = CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
                "ORDER_ID" => $order_id
            )
        );

        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];

        $EMAIL = "";
        $transNumber = "";
        $track = "";
        while ($arProps = $dbOrderProps->Fetch()) {
            if ($arProps["NAME"] == "Транспортная") {
                $transNumber = $arProps["VALUE"];
                continue;
            }
            if ($arProps["CODE"] == "email") {
                $EMAIL = $arProps["VALUE"];
                continue;
            }
            if ($arProps["NAME"] == "Трек") {
                $track = $arProps["VALUE"];
                continue;
            }
        }
        $arEventFields = array(
            "ORDER_ID" => $order_id,
            "ORDER_DATE" =>  date('Y-m-d H:i:s'),
            "EMAIL" => $EMAIL,
            "SALE_EMAIL" => $mail_to,
            "ORDER_PUBLIC_URL" => "",
            "DEFAULT_EMAIL_FROM" => "",
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => "",
            "TRANSPORT_NUBMER" => $transNumber,
            "TRACK" => $track,
        );
        CEvent::Send("ORDER_DELIVERY_ON_WAY", "s1", $arEventFields, "N");
    }
}

function debug($data)
{
    echo '<pre>' . print_r($data, 1) . '</pre>';
}


function logToFile($filepath, $var, $unlink = false)
{
	$logFile = $_SERVER["DOCUMENT_ROOT"].$filepath;
	$dirpath = dirname($logFile);
	
	if(!file_exists($dirpath)) {
		mkdir($dirpath);
	}
	
	if($unlink) {
		unlink($logFile);
	}
	file_put_contents($logFile,print_r($var,true)."\n\n",FILE_APPEND);
}