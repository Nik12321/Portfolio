<?
use Bitrix\Main\Entity\ExpressionField;
use itc\CUncachedArea;

itc\CUncachedArea::registerCallback('productBuyBlock', function ($key, $data) {
    if (!CModule::IncludeModule('sale')) return array();
    if (!CModule::IncludeModule('catalog')) return array();
    if (!CModule::IncludeModule('iblock')) return array();
    $ouput = array();
    foreach ($data as $item) {
        $subkey = itc\CUncachedArea::getSubkey($item);
        if ($item['id'] <= 0) {
            continue;
        }
        else {
            global $USER;
            $id = $item['id'];
            $buyBlockHTML = $item["buyBlockHTML"];
            $offersIds = $item['offersIds'];
            if (count($offersIds)) {
                $offersIds[] = $id;
                $finalPrice = 0;
                $discount = "";
                foreach ($offersIds as $id_s) {
                    $price = CCatalogProduct::GetOptimalPrice($id_s, 1, $USER->GetUserGroupArray(), "N");
                    if (!$finalPrice || $finalPrice > $price["RESULT_PRICE"]["DISCOUNT_PRICE"]) {
                        $finalPrice = $price["RESULT_PRICE"]["DISCOUNT_PRICE"];
                    }
                    if (!$discount && $price["RESULT_PRICE"]["PERCENT"] > 0) {
                        $discount = intval($price["RESULT_PRICE"]["PERCENT"]);
                    }
                }
                $buyBlockHTML = str_replace("#DISCOUNT#", ($discount) ? "<div class=\"skidka_div\"><div class=\"skidka\">-" . $discount . "%</div></div><div class=\"skidka_div\"></div>" : "", $buyBlockHTML);
            }
            else {
                $price = CCatalogProduct::GetOptimalPrice($id, 1, $USER->GetUserGroupArray(), "N");
                $finalPrice = $price["RESULT_PRICE"]["DISCOUNT_PRICE"];
                $buyBlockHTML = str_replace("#DISCOUNT#", ($price["RESULT_PRICE"]["PERCENT"]) ? "<div class=\"skidka_div\"><div class=\"skidka\">-" . intval($price["RESULT_PRICE"]["PERCENT"]) . "%</div></div><div class=\"skidka_div\"></div>" : "", $buyBlockHTML);
            }
            $buyBlockHTML = str_replace("#PRICE#", $finalPrice, $buyBlockHTML);
            $ouput[$subkey] = $buyBlockHTML;
        }
    }
    return $ouput;
});

itc\CUncachedArea::registerCallback('productBuyOfferBlock', function ($key, $data) {
    if (!CModule::IncludeModule('sale')) return array();
    if (!CModule::IncludeModule('catalog')) return array();
    if (!CModule::IncludeModule('iblock')) return array();
    $ouput = array();
    foreach ($data as $item) {
        $subkey = itc\CUncachedArea::getSubkey($item);
        if ($item['id'] <= 0) {
            continue;
        }
        else {
            global $USER;
            $id = $item['id'];
            $offerPriceBlockHTML = $item["offerPriceBlockHTML"];
            $offersIds = $item['offersIds'];
            $offersIds[] = $id;
            foreach ($offersIds as $id_s) {
                $price = CCatalogProduct::GetOptimalPrice($id_s, 1, $USER->GetUserGroupArray(), "N");
                $offerPriceBlockHTML = str_replace("#OFFER_PRICE_" . $id_s . "#", $price["RESULT_PRICE"]["DISCOUNT_PRICE"], $offerPriceBlockHTML);
            }
            $ouput[$subkey] = $offerPriceBlockHTML;
        }
    }
    return $ouput;
});


itc\CUncachedArea::registerCallback('productOfferBlock', function ($key, $data) {
    if (!CModule::IncludeModule('sale')) return array();
    if (!CModule::IncludeModule('catalog')) return array();
    if (!CModule::IncludeModule('iblock')) return array();
    $ouput = array();
    foreach ($data as $item) {
        $subkey = itc\CUncachedArea::getSubkey($item);
        if ($item['id'] <= 0) {
            continue;
        }
        else {
            global $USER;
            $buyBlockHTML = $item["buyBlockHTML"];
            $offersIds = $item["offersIds"];
            $allDiscounts = $allPrices = array();
            if (count($offersIds)) {
                foreach ($offersIds as $id_s) {
                    $price = CCatalogProduct::GetOptimalPrice($id_s, 1, $USER->GetUserGroupArray(), "N");
                    $allPrices[] = $price["RESULT_PRICE"]["DISCOUNT_PRICE"];
                    $allDiscounts[] = intval($price["RESULT_PRICE"]["PERCENT"]);
                }
                $buyBlockHTML = str_replace("#OFFERS_DISCOUNT#", htmlspecialchars(json_encode($allDiscounts, ENT_NOQUOTES )), $buyBlockHTML);
                $buyBlockHTML = str_replace("#OFFERS_PRICES#", htmlspecialchars(json_encode($allPrices, ENT_NOQUOTES )), $buyBlockHTML);

                $buyBlockHTML = str_replace("#DISCOUNT#", ($allDiscounts[0]) ? "<div class=\"skidka_div\"><div class=\"skidka\">-" . $allDiscounts[0] . "%</div></div><div class=\"skidka_div\"></div>" : "", $buyBlockHTML);
                $buyBlockHTML = str_replace("#PRICE#", $allPrices[0], $buyBlockHTML);
            }
            $ouput[$subkey] = $buyBlockHTML;
        }
    }
    return $ouput;
});
?>