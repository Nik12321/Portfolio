<?

$mainIblockId = 13;
$skuIblockId = 14;

$needToClearWrongPrices = false;

function updateProductProps($productArrs, &$mapElementsByXmlID = [], &$results = [], &$logFilepath = null)
{
    global $needToClearWrongPrices;
    global $APPLICATION;

    if ($productArrs["ID"] && isset($mapElementsByXmlID[$productArrs["ID"]])) {
        logToFile($logFilepath, "Updating product with XML_ID: ".$productArrs["ID"]);

        if(isset($mapElementsByXmlID[$productArrs["ID"]]['sku'], $mapElementsByXmlID[$productArrs["ID"]]['main'])) {
            // sku and main.

            $mainElementId = $mapElementsByXmlID[$productArrs["ID"]]['main']["ID"];
            $mainElementArticle = $mapElementsByXmlID[$productArrs["ID"]]['main']["PROPERTY_ARTICLE_VALUE"];
            $mainElementStatus = $mapElementsByXmlID[$productArrs["ID"]]['main']["PROPERTY_STATUS_VALUE"];

            $skuElementId = $mapElementsByXmlID[$productArrs["ID"]]['sku']["ID"];
            $skuElementArticle = $mapElementsByXmlID[$productArrs["ID"]]['sku']["PROPERTY_ARTICLE_VALUE"];
            $skuElementStatus = $mapElementsByXmlID[$productArrs["ID"]]['sku']["PROPERTY_STATUS_VALUE"];

            $skuElementPrice = $mapElementsByXmlID[$productArrs["ID"]]['sku']["CATALOG_PRICE_1"];
            $skuElementPriceId = $mapElementsByXmlID[$productArrs["ID"]]['sku']["CATALOG_PRICE_ID_1"];
            $skuElementQuantity = $mapElementsByXmlID[$productArrs["ID"]]['sku']["CATALOG_QUANTITY"];

            $fields = Array(
                "PRODUCT_ID" => $skuElementId,
                "CATALOG_GROUP_ID" => 1,
                "PRICE" => $productArrs["PRICE"],
                "CURRENCY" => "RUB"
            );

            if($needToClearWrongPrices) {
                CPrice::DeleteByProduct($skuElementId);
                logToFile($logFilepath, "Deleting all prices for product...");
            }

            if($productArrs["PRICE"] != $skuElementPrice) {
                if($needToClearWrongPrices || !$skuElementPriceId) {
                    if (CPrice::Add($fields)) {
                        //$indexPrice++;

                        //echo "Price was added: \n";
                        logToFile($logFilepath, "Price was added: ".$productArrs["PRICE"]);
                    } else {
                        //echo $APPLICATION->GetException()." !!! \n";
                        logToFile($logFilepath, "Price add exception: ". $APPLICATION->GetException()." !!! ");
                    }
                } else {
                    if (CPrice::Update($skuElementPriceId, $fields)) {
                        //$indexPrice++;

                        //echo "Price was changed and updated: \n";
                        logToFile($logFilepath, "Price was changed and updated. Old: ".$skuElementPrice.", New: ".$productArrs["PRICE"]);
                    } else {
                        //echo $APPLICATION->GetException()." !!! \n";
                        logToFile($logFilepath, "Price update exception: ". $APPLICATION->GetException()." !!! ");
                    }
                }
            }
            // check changes for ARTICLE:
            // main
            if(isset($productArrs["ARTICLE"]) && trim($productArrs["ARTICLE"]) && trim($productArrs["ARTICLE"]) != $mainElementArticle) {
                CIBlockElement::SetPropertyValuesEx($mainElementId, false, array("article" => trim($productArrs["ARTICLE"])));

                logToFile($logFilepath, "Article was changed and updated - main. Old: ".$mainElementArticle.", New: ".trim($productArrs["ARTICLE"]));
            }
            // sku
            if(isset($productArrs["ARTICLE"]) && trim($productArrs["ARTICLE"]) && trim($productArrs["ARTICLE"]) != $skuElementArticle) {
                CIBlockElement::SetPropertyValuesEx($skuElementId, false, array("article" => trim($productArrs["ARTICLE"])));

                logToFile($logFilepath, "Article was changed and updated - sku. Old: ".$skuElementArticle.", New: ".trim($productArrs["ARTICLE"]));
            }


            // check changes for QUANTITY
            if($productArrs["HAVE"] != $skuElementQuantity || 1) {
                if($productArrs["HAVE"] != $skuElementQuantity) {
                    $fields = array('QUANTITY' => $productArrs["HAVE"]);// зарезервированное количество
                    CCatalogProduct::Update($skuElementId, $fields);
                    logToFile($logFilepath, "QUANTITY was changed and updated - sku. Old: ".$skuElementQuantity.", New: ".$productArrs["HAVE"]);
                }

                if ($productArrs["HAVE"] == 0 && $mainElementStatus != 0 && $mainElementStatus != -1 && $mainElementStatus != -2) {
                    CIBlockElement::SetPropertyValuesEx($mainElementId, false, array("status" => 0));

                    logToFile($logFilepath, "Status was changed and updated");
                }
                else if ($productArrs["HAVE"] > 0 && $mainElementStatus != 1) {
                    CIBlockElement::SetPropertyValuesEx($mainElementId, false, array("status" => 1));

                    logToFile($logFilepath, "Status was changed and updated");
                }

                // эти проверки для выставления статуса наличия торговому предложению (оно независимо от других ТП.).
                if ($productArrs["HAVE"] == 0 && $skuElementStatus != 0 && $skuElementStatus != -1 && $mainElementStatus != -2) {
                    CIBlockElement::SetPropertyValuesEx($skuElementId, false, array("status" => 0));

                    logToFile($logFilepath, "Status was changed and updated");
                }
                else if ($productArrs["HAVE"] > 0 && $skuElementStatus != 1) {
                    CIBlockElement::SetPropertyValuesEx($skuElementId, false, array("status" => 1));

                    logToFile($logFilepath, "Status was changed and updated");
                }
            }

            $results["HANDLED_SKU_AND_MAIN"][] = $productArrs["ID"];

        } else if((isset($mapElementsByXmlID[$productArrs["ID"]]['main']) && !isset($mapElementsByXmlID[$productArrs["ID"]]['sku']))
            ||
            (!isset($mapElementsByXmlID[$productArrs["ID"]]['main']) && isset($mapElementsByXmlID[$productArrs["ID"]]['sku']))
        ) {
            // only main or only sku.
            $type = isset($mapElementsByXmlID[$productArrs["ID"]]['main']) ? 'main' : 'sku';

            $mainElementId = $mapElementsByXmlID[$productArrs["ID"]][$type]["ID"];
            $mainElementArticle = $mapElementsByXmlID[$productArrs["ID"]][$type]["PROPERTY_ARTICLE_VALUE"];
            $mainElementStatus = $mapElementsByXmlID[$productArrs["ID"]][$type]["PROPERTY_STATUS_VALUE"];

            $mainElementPrice = $mapElementsByXmlID[$productArrs["ID"]][$type]["CATALOG_PRICE_1"];
            $mainElementPriceId = $mapElementsByXmlID[$productArrs["ID"]][$type]["CATALOG_PRICE_ID_1"];
            $mainElementQuantity = $mapElementsByXmlID[$productArrs["ID"]][$type]["CATALOG_QUANTITY"];

            $fields = Array(
                "PRODUCT_ID" => $mainElementId,
                "CATALOG_GROUP_ID" => 1,
                "PRICE" => $productArrs["PRICE"],
                "CURRENCY" => "RUB"
            );

            if($needToClearWrongPrices) {
                CPrice::DeleteByProduct($mainElementId);
            }

            if($productArrs["PRICE"] != $mainElementPrice) {
                if($needToClearWrongPrices || !$mainElementPriceId) {
                    if (CPrice::Add($fields)) {
                        //$indexPrice++;

                        //echo "Price was added: \n";
                        logToFile($logFilepath, "Price was added: ".$productArrs["PRICE"]);
                    } else {
                        logToFile($logFilepath, "Price add exception: ". $APPLICATION->GetException()." !!! ");
                    }
                } else {
                    if (CPrice::Update($mainElementPriceId, $fields)) {
                        logToFile($logFilepath, "Price was changed and updated. Old: ".$mainElementPrice.", New: ".$productArrs["PRICE"]);
                    } else {
                        logToFile($logFilepath, "Price update exception: ". $APPLICATION->GetException()." !!! ");
                    }
                }

            }

            // check changes for ARTICLE
            if(isset($productArrs["ARTICLE"]) && trim($productArrs["ARTICLE"]) && trim($productArrs["ARTICLE"]) != $mainElementArticle) {
                CIBlockElement::SetPropertyValuesEx($mainElementId, false, array("article" => trim($productArrs["ARTICLE"])));

                logToFile($logFilepath, "Article was changed and updated. Old: ".$mainElementArticle.", New: ".trim($productArrs["ARTICLE"]));
            }


            // check changes for QUANTITY
            if($productArrs["HAVE"] != $mainElementQuantity || 1) {
                if($productArrs["HAVE"] != $mainElementQuantity) {
                    $fields = array('QUANTITY' => $productArrs["HAVE"]);// зарезервированное количество
                    CCatalogProduct::Update($mainElementId, $fields);
                    logToFile($logFilepath, "QUANTITY was changed and updated. Old: ".$mainElementQuantity.", New: ".$productArrs["HAVE"]);
                }

                if ($productArrs["HAVE"] == 0 && $mainElementStatus != 0 && $mainElementStatus != -1 && $mainElementStatus != -2) {
                    CIBlockElement::SetPropertyValuesEx($mainElementId, false, array("status" => 0));

                    logToFile($logFilepath, "Status was changed and updated");
                }
                else if ($productArrs["HAVE"] > 0 && $mainElementStatus != 1) {
                    CIBlockElement::SetPropertyValuesEx($mainElementId, false, array("status" => 1));

                    logToFile($logFilepath, "Status was changed and updated");
                }
            }

            $results["HANDLED"][] = $productArrs["ID"];
        } else {
            logToFile($logFilepath, "main or sku element not found...");
            logToFile($logFilepath, print_r($productArrs));
        }

    } else {
        logToFile($logFilepath, "Product ID is empty or mapElementsByXmlID is not exist this element: ".$productArrs["ID"]);

        $results["NOT_FOUND"][] = $productArrs["ID"];
    }
}

function getElementsByXmlIDMap()
{
    $rows = [];

    global $mainIblockId, $skuIblockId;

    foreach([$mainIblockId, $skuIblockId] as $curIblockId) {
        $arFilter = array(
            'IBLOCK_ID' => $curIblockId,
            'ACTIVE' => 'Y',
            '!XML_ID' => false,
        );
        $arSelect = array("ID", "IBLOCK_ID", "XML_ID","PROPERTY_article","CATALOG_QUANTITY","CATALOG_GROUP_1","PROPERTY_status");

        $rsElement = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);

        while ($obElement = $rsElement->Fetch()) {
            // торговые предложения тоже нужны.
            if (stripos($obElement['XML_ID'], '#') && 0)
            {
                $xmlId = substr($obElement['XML_ID'], 0, stripos($obElement['XML_ID'], '#'));
            }
            else
            {
                $xmlId = (string) trim($obElement['XML_ID']);
            }

            $resultRow = $obElement;
            // если у товара есть торговое предложение, то у самого товара нужно будет обновить только Артикул,
            // а цену и количество - у торгового предложения с этим же внешним кодом.
            if($obElement['IBLOCK_ID'] == $mainIblockId) {
                $rows['byXML'][$xmlId]['main'] = $resultRow;
            } else if($obElement['IBLOCK_ID'] == $skuIblockId) {
                $rows['byXML'][$xmlId]['sku'] = $resultRow;
            }
        }
    }

    unset($rsElement);
    unset($obElement);

    return $rows;
}

function updateProductDataRowByRow() {
    $_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/ext_www/zoonemo.ru";
    $fileDir = $_SERVER["DOCUMENT_ROOT"] . '/script/';
    $files = scandir($fileDir);
    $startTime = microtime(true);
    $logFilepath = "/_logs/1c_sync_logs_".date('Y-m-d').".txt";
    ini_set('memory_limit','512M');
    $skip = 0;
    $limit = 1010000;


    $mapElements = getElementsByXmlIDMap();
    $mapElementsByXmlID = [];
    if(isset($mapElements["byXML"])) {
        $mapElementsByXmlID = $mapElements["byXML"];
    }

    $results = [];

    logToFile($logFilepath, "Count of summary product and offers: ".count($mapElementsByXmlID));
    $allKeys = $allFileNames = [];
    foreach ($files as $filename) {
        if (preg_match('/[0-9|_]+_START_updateProduct.txt$/', $filename))
        {
            $allFileNames[] = $filename;
            logToFile($logFilepath, "Handling file: ". $filename);

            $handle = fopen($_SERVER["DOCUMENT_ROOT"] . '/script/'.$filename, "r");
            $index = 0;

            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    // process the line read.
                    $index++;

                    if($index <= $skip) {
                        continue;
                    }

                    $productData = [];
                    $tmp = explode(",",trim($line));

                    $productData["ID"] = isset($tmp[0]) ? trim($tmp[0],'" ') : NULL;
                    $productData["HAVE"] = isset($tmp[1]) ? trim($tmp[1],'" ') : NULL;
                    $productData["PRICE"] = isset($tmp[2]) ? trim($tmp[2],'" ') : NULL;
                    $productData["ARTICLE"] = isset($tmp[3]) ? trim($tmp[3],'" ') : NULL;

                    logToFile($logFilepath, $index .". ".$productData["ID"]);

                    updateProductProps($productData, $mapElementsByXmlID, $results, $logFilepath);
                    $allKeys[] = $productData["ID"];


                    if($index > $limit + $skip) {
                        break;
                    }
                }

                fclose($handle);
            } else {
                // error opening the file.
                logToFile($logFilepath, 'Error opening file: ' . $filename);
            }

            $newName = 'updateProduct' . date('Y_m_d_H_i_s') . "_END.txt";
            rename($_SERVER["DOCUMENT_ROOT"] . '/script/'. $filename, $_SERVER["DOCUMENT_ROOT"] . '/script/' . $newName);

            logToFile($logFilepath, 'File was renamed from: ' . $filename . ', to: ' . $newName);

            //break;
        }
    }
    //Сохраняем все xml_id в файл
    if (count($allKeys)) {
        logToFile("/_logs/forAgent.txt", count($allFileNames));
        if (count($allFileNames) === 2 && !file_exists($_SERVER["DOCUMENT_ROOT"] . '/script/UpdatedElements.txt')) {
            if (abs(filectime($allFileNames[0]) - filectime($allFileNames[1])) < 1200) {
                $data = serialize($allKeys);
                file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/script/UpdatedElements.txt', $data);
            }
        } 
        else {
            unlink($_SERVER["DOCUMENT_ROOT"] . '/script/UpdatedElements.txt');
        }
    }
    logToFile($logFilepath, $results);
    logToFile($logFilepath, "Time taken: " . (microtime(true) - $startTime));
    return __METHOD__ . "();";
}

//Агент меняет статус всех товаров, что отсутсвуют в выгрузке, на -1
function agent_changeStatusForProductsFromFile() {
    $_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/ext_www/zoonemo.ru";
    global $mainIblockId;
    $logFilepath = "/_logs/1c_change_status".date('Y-m-d').".txt";
    $logFilepathFilesData = "/_logs/1c_change_status_data".date('Y-m-d').".txt";
    $fileDir = $_SERVER["DOCUMENT_ROOT"] . '/script/UpdatedElements.txt';
    $file = file_get_contents($fileDir);
    $data = unserialize($file);
    $data = array_unique($data);
    $allDataIdes = [];
    if (count($data)) {
        logToFile($logFilepath, ["ELEMENT_COUNT" => count($data)]);
        $arFilter = ['IBLOCK_ID' => $mainIblockId, 'ACTIVE' => 'Y', '!XML_ID' => $data];
        $arSelect = ["ID", "IBLOCK_ID", "XML_ID","PROPERTY_status"];
        $rsElement = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        $indexUpdate = 0;
        $indexNotUpdate = 0;
        while ($obElement = $rsElement->Fetch()) {
            logToFile($logFilepath, $obElement["ID"]);
            if (trim($obElement["PROPERTY_STATUS_VALUE"]) !== "" && trim($obElement["PROPERTY_STATUS_VALUE"]) !== "-1"  && trim($obElement["PROPERTY_STATUS_VALUE"]) !== "-2") {
                $allDataIdes[] = $obElement["ID"];
                $indexUpdate++;
                logToFile($logFilepathFilesData, "Element that have good status:");
                logToFile($logFilepathFilesData, $obElement);
            }
            else {
                $indexNotUpdate++;
                logToFile($logFilepathFilesData, "Element that have bad status:");
                logToFile($logFilepathFilesData, $obElement);
            }
        }
        logToFile($logFilepath, "Total elements, that need update: " . $indexUpdate);
        logToFile($logFilepath, "Total elements, that already have status -1 or -2: " . $indexNotUpdate);
    }
    else {
        logToFile($logFilepath, "We do not have file for updating");
    }
    foreach ($allDataIdes as $id) {
        //TODO раскомментировать, Когда проверки покажут хорошие результаты
        CIBlockElement::SetPropertyValuesEx($id, false, array("status" => -1));
    }
    if (! rename($_SERVER["DOCUMENT_ROOT"] . '/script/UpdatedElements.txt', $_SERVER["DOCUMENT_ROOT"] . '/script/UpdatedElements' . date('Y-m-d').".txt")) {
        unlink($_SERVER["DOCUMENT_ROOT"] . '/script/UpdatedElements.txt');
    }
    return __METHOD__ . "();";
}

//Агент проверяет, изменился ли файл, который в мобильной версии выводит телефон клиента
function agent_checkMobileOrder() {
    $_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/ext_www/zoonemo.ru";
    $logFilepath = "/_logs/mobileOrderIsChanged.txt";
    $fileName = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sale/general/mobile_order.php";
    if (file_exists($fileName)) {
        if (86400 > (strtotime("now") - filemtime($fileName))) {
            logToFile($logFilepath, "File changed by bitrix update or tools!");
            logToFile($logFilepath,  date("F d Y H:i:s.", filemtime($fileName)));
        }
    }
    else {
        logToFile($logFilepath, "Error, cannot find file mobile_order.php!");
    }
    return __METHOD__ . "();";
}

//Агент выставляет значения свойства для фильтрации всем товарам:
function agent_recountGroupStatus() {
    $elements = CIBlockElement::GetList(false, ['IBLOCK_ID' => 13], false, false, ["ID", "PROPERTY_status", "PROPERTY_STATUS_GROUP"]);
    while ($element = $elements->Fetch()) {
        $element["PROPERTY_STATUS_VALUE"] = trim($element["PROPERTY_STATUS_VALUE"]);
        switch ($element["PROPERTY_STATUS_VALUE"]) {
            case "0":
            case "1" :
                if (intval($element["PROPERTY_STATUS_GROUP_VALUE"]) !== 1) {
                    CIBlockElement::SetPropertyValuesEx($element["ID"], false, array("STATUS_GROUP" => 1));
                }
                break;
            case "-1":
            case "-2":
            default:
                if ($element["PROPERTY_STATUS_GROUP_VALUE"] !== '0') {
                    CIBlockElement::SetPropertyValuesEx($element["ID"], false, array("STATUS_GROUP" => 0));
                }
                break;
        }
    }
    return __METHOD__ . "();";
}

//Агент заполняет свойство external_id для каталога товаров
function agent_addExternalIdToProducts() {
    $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => array(13), "PROPERTY_external_id" => false), false, false, array("ID", "XML_ID", "PROPERTY_external_id"));
    while ($element = $elements->Fetch()) {
        CIBlockElement::SetPropertyValuesEx($element["ID"], false, array("external_id" => $element["XML_ID"]));
    }
    $offers = CIBlockElement::GetList(false, array("IBLOCK_ID" => array(14), "PROPERTY_external_id" => false), false, false, array("ID", "XML_ID", "PROPERTY_external_id"));
    while ($offer = $offers->Fetch()) {
        CIBlockElement::SetPropertyValuesEx($offer["ID"], false, array("external_id" => $offer["XML_ID"]));
    }
    return __METHOD__ . "();";
}
?>