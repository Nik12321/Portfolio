<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
use Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<head>
    <?php
    header('Content-Type: text/html; charset=utf-8');
    date_default_timezone_set('Europe/Moscow'); //Необходимо установить часовой пояс
    $APPLICATION->ShowHead();
    $APPLICATION->SetTitle('Зоомагазин "Немо"');
    ?>
    <title><?php $APPLICATION->ShowTitle();?></title>
    <?php
    Asset::getInstance()->addString('<link rel="shortcut icon" href="'. DEFAULT_TEMPLATE_PATH. '/img/fav.png">');
    Asset::getInstance()->addString('<link rel="preload" as="font" type="font/woff2" crossorigin="anonymous" href="'. DEFAULT_TEMPLATE_PATH. '/fonts/HelveticaNeue/HelveticaNeueCyr-Roman.otf">');
    Asset::getInstance()->addString('<link rel="preload" as="font" type="font/woff2" crossorigin="anonymous" href="'. DEFAULT_TEMPLATE_PATH. '/fonts/HelveticaNeue/HelveticaNeueCyr-Medium.otf">');
    Asset::getInstance()->addString('<link rel="preload" as="font" type="font/woff2" crossorigin="anonymous" href="'. DEFAULT_TEMPLATE_PATH. '/fonts/lightcase/lightcase.woff">');
    Asset::getInstance()->addString('<link rel="preload" as="font" type="font/woff2" crossorigin="anonymous" href="'. DEFAULT_TEMPLATE_PATH. '/fonts/HelveticaNeue/HelveticaNeueCyr-Bold.otf">');
    Asset::getInstance()->addString('<link rel="preload" as="font" type="font/woff2" crossorigin="anonymous" href="'. DEFAULT_TEMPLATE_PATH. '/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0">');
    Asset::getInstance()->addString('<meta name="yandex-verification" content="cc2b910213a52564" />');
    Asset::getInstance()->addString('<meta http-equiv="Content-Type"  charset="UTF-8">');
    Asset::getInstance()->addString('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
    Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">');
    Asset::getInstance()->addString('<meta name="google-site-verification" content="rw1KuonxLi0QaFGBBjbafWxk9yp0eCna9zAumN3zuig"/>');
    Asset::getInstance()->addString('<meta name="cmsmagazine" content="8b8f98ba6e95d71cf42e9faa2011c4a5"/>');
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH . '/css/style_old.css');
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH . '/css/main.css');
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH . '/font-awesome/css/font-awesome.css');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . '/js/jquery3-1-1.min.js');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . '/js/jquery.maskedinput.min.js');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . '/js/notify.min.js');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . '/js/owlCaorusel2-2-0.js');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . '/js/scripts.js');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . '/js/openapi.js');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . '/js/jquery.lazyload.js');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . '/js/jquery.maskedinput.js');

    ?>
</head>

<body>
<script src="https://www.googletagmanager.com/gtag/js?id=UA-2761653-65"></script>
<script defer>
    $('.phoneCall').mask('+7 (999) 999-99-99');
    $('.phoneCall').on("keyup", function(event) {
        var value = $(event.target).val();
        if (value[4] === "7" && value[0] == "+") {
            $('.phoneCall').unmask();
            $('.phoneCall').mask('+7 (999) 999-99-99');
            $('.phoneCall').focus();
        }
        if (value[4] === "8" && value[0] == "+") {
            $('.phoneCall').unmask();
            $('.phoneCall').mask('8 (999) 999-99-99');
            $('.phoneCall').focus();
        }
        if (value[3] === "7" && value[0] == "8") {
            $('.phoneCall').unmask();
            $('.phoneCall').mask('+7 (999) 999-99-99');
            $('.phoneCall').focus();
        }
        if (value[3] === "8" && value[0] == "8") {
            $('.phoneCall').unmask();
            $('.phoneCall').mask('8 (999) 999-99-99');
            $('.phoneCall').focus();
        }
    });

    var fired = false;
    document.jivositeloaded=0;
    var widget_id = 'pDQUP6vqVJ';
    var d=document;
    var w=window;

    function l() {
        console.log(123);
        var s = d.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = '//code.jivosite.com/script/widget/'+widget_id;
        var ss = document.getElementsByTagName('script')[0];
        ss.parentNode.insertBefore(s, ss);
    }
    function zy(){
        //удаляем EventListeners
        if (w.detachEvent){//поддержка IE8
            w.detachEvent('onscroll',zy);
            w.detachEvent('onmousemove',zy);
            w.detachEvent('ontouchmove',zy);
            w.detachEvent('onresize',zy);
        } else {
            w.removeEventListener("scroll", zy, false);
            w.removeEventListener("mousemove", zy, false);
            w.removeEventListener("touchmove", zy, false);
            w.removeEventListener("resize", zy, false);
        }
        //запускаем функцию загрузки JivoSite
        if(d.readyState=='complete') {
            l();
        } else {
            if(w.attachEvent) {
                w.attachEvent('onload',l);
            } else {
                w.addEventListener('load',l,false);
            }
        }
        var cookie_date = new Date();
        cookie_date.setTime ( cookie_date.getTime()+60*60*28*1000); //24 часа для Москвы
        d.cookie = "JivoSiteLoaded=1;path=/;expires=" + cookie_date.toGMTString();
    }
    window.addEventListener("DOMContentLoaded", () => {
        document.jivositeloaded=0;
        var widget_id = 'pDQUP6vqVJ';
        var d=document;
        var w=window;
        // поддержка IE8
        if (w.attachEvent){
            w.attachEvent('onscroll',zy);
            w.attachEvent('onmousemove',zy);
            w.attachEvent('ontouchmove',zy);
            w.attachEvent('onresize',zy);
        } else {
            w.addEventListener("scroll", zy, {capture: false, passive: true});
            w.addEventListener("mousemove", zy, {capture: false, passive: true});
            w.addEventListener("touchmove", zy, {capture: false, passive: true});
            w.addEventListener("resize", zy, {capture: false, passive: true});
        }

        window.addEventListener('scroll', () => {
            if (fired === false) {
                fired = true;
                setTimeout(() => {
                    <!-- Global site tag (gtag.js) - Google Analytics -->
                    window.dataLayer = window.dataLayer || [];
                    function gtag() { dataLayer.push(arguments); }
                    gtag('js', new Date());
                    gtag('config', 'UA-2761653-65');
                    <!-- Global site tag (gtag.js) - Google Analytics -->

                    <!-- Yandex.Metrika counter -->
                    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
                    ym(21199375, "init", {
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        ecommerce:"dataLayer"
                    });
                    <!-- Yandex.Metrika counter -->
                }, 1000)
            }
        });
    });
</script>
<script type="application/ld+json" defer>
    {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Zoonemo",
        "alternateName": "Зоомагазин Немо",
        "url": "https://www.zoonemo.ru/",
        "logo": "https://www.zoonemo.ru/local/templates/.default/img/logo_extend.png",
        "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+7(926)216-66-80",
            "contactType": "customer service",
            "areaServed": "RU",
            "availableLanguage": "Russian"
        },{
            "@type": "ContactPoint",
            "telephone": "+7(495)682-40-60",
            "contactType": "customer service",
            "areaServed": "RU",
            "availableLanguage": "Russian"
        },{
            "@type": "ContactPoint",
            "telephone": "+7(499)168-63-24",
            "contactType": "customer service",
            "areaServed": "RU",
            "availableLanguage": "Russian"
        },{
            "@type": "ContactPoint",
            "telephone": "+7(499)373-94-93",
            "contactType": "customer service",
            "areaServed": "RU",
            "availableLanguage": "Russian"
        }
        ]
    }
</script>
<div id="panel"><?php $APPLICATION->ShowPanel();?></div>
<!-- template/header.html START -->
<header>
    <!--Вывод расписания для мобильной версии-->
    <div class="mini-shedule_pzf mobile-display">
        <?php
        $APPLICATION->IncludeComponent("bitrix:main.include", "detailsMainShedule", [
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "standard.php",
                "PATH" => "/include/scheduleLabel.php",
                "COMPONENT_TEMPLATE" => "detailsMainShedule"
            ],
            false
        );
        ?>
    </div>
    <div class="topbar">
        <div class="container">
            <div class="row">
                <!--Логотип компании-->
                <div class="col-xs-12 col-sm-1">
                    <a href="/" class="logo">
                        <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/logo.png" alt="logo">
                    </a>
                </div>
                <!--Номер телефона #1-->
                <div class="col-md-2 col-sm-3 col-xs-6 tar">
                    <i class="phone mobile-none">
                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24.031 24">
                            <path id="phone-9" class="cls-1" d="M522.647,14.591L520.428,17.7s0.882,0.656.921,0.687c0.994,0.747-1.927,4.8-2.948,4.1l-0.932-.692L515.233,24.9l0.943,0.7c3.314,2.344,10.591-7.8,7.412-10.309C523.536,15.252,522.65,14.594,522.647,14.591ZM509.3,26.481a11.995,11.995,0,1,0,5.2-16.133A12,12,0,0,0,509.3,26.481Zm1.779-.912a10,10,0,1,1,13.46,4.327A9.994,9.994,0,0,1,511.081,25.57Z" transform="translate(-507.969 -9)" />
                        </svg>
                    </i>
                    <?php
                    $day = date(w); //Получаем день недели
                    if($day < 5 && $day != 0) {
                        $operationMode = "/include/phoneWeekdaysLabel.php";
                    } else {
                        $operationMode = "/include/phoneWeekendsLabel.php";
                    }
                    $APPLICATION->IncludeComponent("bitrix:main.include", "detailsMainPhone1", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => $operationMode,
                            "COMPONENT_TEMPLATE" => "detailsMainPhone1"
                        ],
                        false
                    );
                    ?>
                </div>
                <!--Номер телефона #2-->
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <img class="mobile-none" src="https://img.icons8.com/office/16/000000/whatsapp.png">
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:main.include", "detailsMainPhone2", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/phone2Label.php",
                            "COMPONENT_TEMPLATE" => "detailsMainPhone2"
                        ],
                        false
                    );
                    ?>
                </div>
                <div class="col-sm-5 col-xs-12 col-sm-offset-0 col-md-offset-1 w100">
                    <ul class="menu">
                        <!--Если пользователь не авторизован, выводим форму регистрации и авторизации-->
                        <?php
                        $frame = new \Bitrix\Main\Page\FrameHelper("callback-modal");
                        $frame->begin();//вызов компонента
                        ?>
                        <?php if(!$USER->IsAuthorized()) :?>
                            <li>
                                <a href="javascript://" onclick="open_dialog('wind_enter');" class="lgn">
                                    <span>Войти</span>
                                    <i class="user">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20.625" height="21" viewBox="0 0 20.625 21">
                                            <path id="user-19" class="cls-1" d="M890.317,28.25a8.526,8.526,0,0,1-6.662-3.229,2.026,2.026,0,0,1,1.605-1.09c1.93-.453,3.834-0.858,2.917-2.577-2.712-5.092-.772-7.979,2.14-7.979,2.857,0,4.844,2.78,2.141,7.979-0.89,1.708.945,2.114,2.916,2.577a2.039,2.039,0,0,1,1.609,1.086A8.527,8.527,0,0,1,890.317,28.25Zm0-19.25a10.5,10.5,0,1,0,10.318,10.5A10.41,10.41,0,0,0,890.317,9Z" transform="translate(-880 -9)" />
                                        </svg>
                                    </i>
                                </a>
                            </li>
                            <!--Иначе - ссылку на личный кабинет-->
                        <?php else :?>
                            <li class="log-m">
                                <a class="lgn" href="/profile/">
                                    <span>Личный кабинет</span>
                                    <i class="user">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20.625" height="21" viewBox="0 0 20.625 21">
                                            <path id="user-19" class="cls-1" d="M890.317,28.25a8.526,8.526,0,0,1-6.662-3.229,2.026,2.026,0,0,1,1.605-1.09c1.93-.453,3.834-0.858,2.917-2.577-2.712-5.092-.772-7.979,2.14-7.979,2.857,0,4.844,2.78,2.141,7.979-0.89,1.708.945,2.114,2.916,2.577a2.039,2.039,0,0,1,1.609,1.086A8.527,8.527,0,0,1,890.317,28.25Zm0-19.25a10.5,10.5,0,1,0,10.318,10.5A10.41,10.41,0,0,0,890.317,9Z" transform="translate(-880 -9)"></path>
                                        </svg>
                                    </i>
                                </a>
                            </li>
                            <li class="log-n">
                                <a href="?logout=yes">Выйти</a>
                            </li>
                        <?php endif;?>
                        <?php $frame->beginStub();?>
                        <div>Загрузка формы</div>
                        <?php $frame->end();?>
                        <!--Виджет для заказа звонка-->
                        <li class="mobile-display">
                            <a href="javascript://" class="fix-form_pzf oneclick_btn b_btn"></a>
                        </li>
                        <!--Количество товаров в корзине-->
                        <?php
                        if (CModule::IncludeModule("sale")) {
                            $arBasketItems = [];
                            $dbBasketItems = CSaleBasket::GetList(
                                ["NAME" => "ASC", "ID" => "ASC"],
                                ["FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL"],
                                false,
                                false,
                                [
                                    "ID",
                                    "CALLBACK_FUNC",
                                    "MODULE",
                                    "PRODUCT_ID",
                                    "QUANTITY",
                                    "DELAY",
                                    "CAN_BUY",
                                    "PRICE",
                                    "WEIGHT"
                                ]
                            );
                            $cntBasketItems = 0;
                            while ($arItems = $dbBasketItems->Fetch()) {
                                if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
                                    CSaleBasket::UpdatePrice($arItems["ID"],
                                        $arItems["CALLBACK_FUNC"],
                                        $arItems["MODULE"],
                                        $arItems["PRODUCT_ID"],
                                        $arItems["QUANTITY"]);
                                    $arItems = CSaleBasket::GetByID($arItems["ID"]);
                                }
                                $cntBasketItems += $arItems["QUANTITY"];
                            }
                        }
                        ?>
                        <li>
                            <a href="/shop/cart/" class="crt">
                                <span class="count" id="cart-count"><?=$cntBasketItems;?></span>
                                <span>Корзина</span>
                                <i class="cart">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="18" viewBox="0 0 24 18">
                                        <path id="shopping-cart-3" class="cls-1" d="M1065.8,10l-3.43,12h-12.59l0.84,2h13.23l3.48-12h1.93l0.74-2h-4.2Zm-4.96,10,1.97-7H1046l2.94,7h11.9Zm-1.34,5a1.5,1.5,0,1,0,1.5,1.5A1.5,1.5,0,0,0,1059.5,25Zm-3.5,1.5a1.5,1.5,0,1,1-1.5-1.5A1.5,1.5,0,0,1,1056,26.5Z" transform="translate(-1046 -10)" />
                                    </svg>
                                </i>
                            </a>
                        </li>
                        <li class="mobile-display c-menu">
                            <div class="slicknav_menu">
                                <div class="menu__button slicknav_btn slicknav_collapsed">
                                    <span class="slicknav_menutxt">Каталог</span>
                                    <span class="slicknav_icon">
                                    <span class="slicknav_icon-bar"></span>
                                    <span class="slicknav_icon-bar"></span>
                                    <span class="slicknav_icon-bar"></span>
                                    </span>
                                </div>
                            </div>
                            <ul class="aside_menu m2">
                                <ul class="submenu">
                                    <?php
                                    $APPLICATION->IncludeComponent("bitrix:menu", "headerMobileShopMenu", [
                                            "ALLOW_MULTI_SELECT" => "N",
                                            "CHILD_MENU_TYPE" => "Main_shop_menu_preview",
                                            "DELAY" => "N",
                                            "MAX_LEVEL" => "1",
                                            "MENU_CACHE_GET_VARS" => [],
                                            "MENU_CACHE_TIME" => "3600",
                                            "MENU_CACHE_TYPE" => "N",
                                            "MENU_CACHE_USE_GROUPS" => "Y",
                                            "ROOT_MENU_TYPE" => "MainShopMenuPreview",
                                            "USE_EXT" => "N",
                                            "COMPONENT_TEMPLATE" => "headerMobileShopMenu"
                                        ],
                                        false
                                    );
                                    ?>
                                </ul>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="mini-shedule_pzf mobile-none col-md-12 col-sm-12">
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:main.include", "detailsMainShedule", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/scheduleLabel.php",
                            "COMPONENT_TEMPLATE" => "detailsMainShedule"
                        ],
                        false
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="subheader-pzf"></div>
<div class="menu_bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mobile-none">
                <?php
                $APPLICATION->IncludeComponent("bitrix:menu", "mainMenuPreview", [
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "MainMenu",	// Тип меню для остальных уровней
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "2",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "MainMenuPreview",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "COMPONENT_TEMPLATE" => "mainMenuPreviewMobile"
                ],
                    false
                );
                ?>
            </div>
            <div class="col-md-12 mobile-display">
                <?php
                $APPLICATION->IncludeComponent("bitrix:menu", "mainMenuPreviewMobile", [
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "MainMenu",	// Тип меню для остальных уровней
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "2",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "MainMenuPreviewMobile",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "COMPONENT_TEMPLATE" => "mainMenuPreviewMobile"
                ],
                    false
                );
                ?>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="search_bar">
                <?php
                $APPLICATION->IncludeComponent("bitrix:catalog.search", "find_products", [
                        "ACTION_VARIABLE" => "action",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BASKET_URL" => "",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "N",
                        "CHECK_DATES" => "N",
                        "CONVERT_CURRENCY" => "N",
                        "DETAIL_URL" => "#SITE_DIR#",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_COMPARE" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "HIDE_NOT_AVAILABLE" => "L",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "L",
                        "IBLOCK_ID" => "13",
                        "IBLOCK_TYPE" => "Catalogs",
                        "LINE_ELEMENT_COUNT" => "3",
                        "NO_WORD_LOGIC" => "N",
                        "OFFERS_CART_PROPERTIES" => [],
                        "OFFERS_FIELD_CODE" => [
                            0 => "",
                            1 => "XML_ID",
                            2 => "",
                        ],
                        "OFFERS_LIMIT" => "5",
                        "OFFERS_PROPERTY_CODE" => [
                            0 => "article",
                            1 => "c",
                            2 => "",
                        ],
                        "OFFERS_SORT_FIELD" => "sort",
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Товары",
                        "PAGE_ELEMENT_COUNT" => "1000000",
                        "PRICE_CODE" => [],
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPERTIES" => "",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PROPERTY_CODE" => [
                            0 => "article",
                            1 => "c",
                            2 => "external_id",
                            3 => "",
                        ],
                        "RESTART" => "Y",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_URL" => "#SITE_DIR#",
                        "SHOW_PRICE_COUNT" => "1",
                        "USE_LANGUAGE_GUESS" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "COMPONENT_TEMPLATE" => "find_products",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "USE_TITLE_RANK" => "N",
                        "USE_SEARCH_RESULT_ORDER" => "N"
                    ],
                    false
                );?>
            </div>
        </div>
    </div>
</div>
<?php
$APPLICATION->IncludeComponent("bitrix:breadcrumb", "navigationChain", [
    "PATH" => "",
    "SITE_ID" => "s1",
    "START_FROM" => "0",
    "COMPONENT_TEMPLATE" => "navigationChain"
],
    false
);
?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <aside>
                <div class="slicknav_menu">
                    <div class="menu__button slicknav_btn slicknav_collapsed">
                        <span class="slicknav_menutxt">Каталог</span>
                        <span class="slicknav_icon">
                            <span class="slicknav_icon-bar"></span>
                            <span class="slicknav_icon-bar"></span>
                            <span class="slicknav_icon-bar"></span>
                        </span>
                    </div>
                </div>
                <?php
                $APPLICATION->IncludeComponent("bitrix:menu", "shopMenu", [
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "Main_shop_menu_preview",
                        "DELAY" => "Y",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => [],
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "MainShopMenu",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "shopMenu"
                    ],
                    false
                );
                $APPLICATION->IncludeComponent("bitrix:menu", "submenuShop", [
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => [],
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "MainShopSubmenu",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "submenuShop"
                    ],
                    false
                );?>
                <br>
                <div class="gifwrap_pzf">
                    <video autoplay="autoplay" loop="loop" muted="muted">
                        <source type="video/mp4" src="<?=DEFAULT_TEMPLATE_PATH . '/images/4848.mp4'?>">
                    </video>
                    <video autoplay="autoplay" loop="loop" muted="muted">
                        <source type="video/mp4" src="<?=DEFAULT_TEMPLATE_PATH . '/images/NaturesMiracles666.mp4'?>">
                    </video>
                </div>
                <br>
            </aside>
        </div>
        <div class="col-md-9">