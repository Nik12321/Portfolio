<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
use \Bitrix\Catalog\CatalogViewedProductTable as CatalogViewedProductTable;
use itc\CUncachedArea;

global $APPLICATION;
global $USER;
CModule::IncludeModule("iblock");
$code = $APPLICATION->CaptchaGetCode();
$user_id = CSaleBasket::GetBasketUserID();
if($user_id  > 0) {
    $product_id = $arResult['ID'];
    CatalogViewedProductTable::refresh($product_id, $user_id);
}
$reviewList = &$GLOBALS["reviewList"];
$reviewList = Array(
    "PROPERTY_CONFIRMATION" => 24,
    "PROPERTY_PRODUCT_ID" => $arResult["ID"]
);
$this->setFrameMode(true);
?>
<? if (!count($arResult["OFFERS"])) : ?>
    <div class="big_item">
        <input class="buttonSend" onclick="window.history.back();" type="button" value="> Назад к списку">
        <div class="title">
            <h1><?=$arResult["NAME"];?></h1>
            <?if(intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 1) : ?>
                <div class="we_have">
                    <span>В наличии <i class="fa fa-check-circle" aria-hidden="true"></i></span>
                </div>
            <?elseif (intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 0) : ?>
                <div class="zakaz">
                    <span>Доставка до 3-х дней<i><img src="" alt=""></i></span>
                    <span class="sub_text">Цена товара<br>может измениться.</span>
                </div>
            <?else  : ?>
                <div class="no_zakaz">
                    <span>Нет в наличии</span>
                </div>
            <?endif;?>
        </div>
        <div class="row">
            <?ob_start(); ?>
            <div class="col-md-5">
                <div class="image image_carousel">
                    <div class="main_img">
                        #DISCOUNT#
                        <?if ($arResult["PROPERTIES"]["photoName"]["VALUE"]) : ?>
                            <div class="photoalbum rphoto">
                                <div class="owl-carousel  owl-theme">
                                    <?foreach($arResult["PROPERTIES"]["photoName"]["VALUE"] as $photo) : ?>
                                        <div>
                                            <?if ($photo["NEW"]) : ?>
                                                <a href="<?=$photo["ORIGIN"];?>"><img  src="<?=$photo["NEW"];?>" data-default="<?=$photo["ORIGIN"];?>"></a>
                                            <?else :?>
                                                <a href="<?=$photo["ORIGIN"];?>"><img src="<?=$file['SRC'];?>"  data-default="<?=$photo["ORIGIN"];?>"></a>
                                            <?endif;?>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <?else:?>
                            <a href="<?=DEFAULT_TEMPLATE_PATH."/img/no_image.png";?>">
                                <img id="image_l" src="<?=DEFAULT_TEMPLATE_PATH."/img/no_image.png";?>">
                            </a>
                        <?endif;?>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="item_info changeMarket">
                    <form method="post" action="/ajax/addToCartMarket.php" data-role="buy">
                        <input style="display: none" class="cost" value = "#PRICE#"/>
                        <input style="display: none" class="id" value = "<?=$arResult["ID"];?>"/>
                        <input style="display: none" class="current_quantity" value = "<?=$arResult["QUANTITY"];?>"/>
                        <div class="count_block">
                            <div class="spinEdit" data-available-quantity="122">
                                <input class="input-quantity spinedit noSelect quantity" value="1"><div class="spinedit"><div class="minus-btn icon-chevron-down minus_prod_det"><i class="fa fa-minus" aria-hidden="true"></i></div><div class="plus_prod_det plus-btn icon-chevron-up"><i class="fa fa-plus" aria-hidden="true"></i></div></div>
                            </div>
                        </div>
                        <div class="price_block">
                        <span class="price">
                            <span class="sum">#PRICE#</span>
                            <i class="fa fa-rub fa-3"></i>
                        </span>
                            <?if(intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 0 || intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 1) : ?>
                                <input type="submit" class="b_btn reverse addToCart" align="absmiddle" value="В корзину" data-role="buy-btn">
                            <?else:?>
                                <?$APPLICATION->IncludeComponent(
                                    "custom:catalog.product.subscribe",
                                    "",
                                    Array(
                                        "BUTTON_CLASS" => "b_btn forCorrectChangeMarket",
                                        "BUTTON_ID" => $arResult["SUBSCRIBE_ID"] + 1000,
                                        "CACHE_TIME" => "3600",
                                        "CACHE_TYPE" => "N",
                                        "PRODUCT_ID" =>  $arResult["SUBSCRIBE_ID"]
                                    )
                                );?>
                            <?endif;?>
                        </div>
                    </form>
                </div>
            </div>
            <?
            $buyBlockHTML = ob_get_clean();
            itc\CUncachedArea::show('productBuyBlock', array(
                'id' => $arResult["SUBSCRIBE_ID"],
                'iblock_id' => $arResult["IBLOCK_ID"],
                "buyBlockHTML" => $buyBlockHTML
            ));
            ?>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <ul class="menu tabs_menu">
                    <li><a href="#about" class="open">Описание</a></li>
                    <li><a href="#har">Характеристики</a></li>
                    <?if($arResult["PROPERTIES"]["file"]["VALUE"] || $arResult["PROPERTIES"]["youtube"]["VALUE"]) :?>
                        <li><a href="#instuctions">Видео и инструкции</a></li>
                    <?endif;?>
                    <li><a href="#comments">Отзывы о товаре</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="tabs" class="tabs">
                    <div class="about open">
                        <?=$arResult['DETAIL_TEXT'];?>
                        <br> <br>
                    </div>
                    <div class="har">
                        <br>
                        <table class="propertys">
                            <tbody>
                            <?unset($arResult["PROPERTIES"]["photoName"]);?>
                            <?foreach($arResult["PROPERTIES"] as $prop) : ?>
                                <?if(str_replace(' ', '', $prop["VALUE"]) && $prop["CODE"] != "file" && $prop["CODE"] != "youtube" && $prop["CODE"] != "status") : ?>
                                    <tr valign="top">
                                        <td nowrap="nowrap"><?=$prop["NAME"];?></td>
                                        <td width="100%"> <?=$prop["VALUE"];?></td>
                                    </tr>
                                <?endif;?>
                            <?endforeach;?>
                            </tbody>
                        </table>
                        <br>

                    </div>
                    <?if($arResult["PROPERTIES"]["file"]["VALUE"] || $arResult["PROPERTIES"]["youtube"]["VALUE"]) :?>
                    <div class="instuctions">
                        <?foreach($arResult["PROPERTIES"]["file"]["VALUE"] as $val) : ?>
                            <?$rsFile =  CFile::GetFileArray($arResult["PROPERTIES"]["file"]["VALUE"][0]);?>
                            <div class="pdfs">
                                <a class="download" target="blank" href="<?=$rsFile["SRC"]?>">Загрузить инструкцию</a>
                            </div>
                        <?endforeach;?>
                        <?echo htmlspecialchars_decode($arResult["PROPERTIES"]["youtube"]["VALUE"]);?>
                    </div>
                    <?endif;?>
                    <div class="comments">
                        <div class="faq_form">
                            <span class="title">Добавить отзыв о товаре:</span>
                            <form action="/ajax/sendProductReview.php" method="POST" onsubmit="Box.submit(this); return false;" class="checkProductReview" id="collapse">
                                <div class="row">
                                    <div class="col-md-6 col-md-push-6">
                                        <input type="hidden" id="productID" name="productID" value="<?=$arResult["ID"]?>">
                                        <label for="name">Ваше имя:</label>
                                        <input type="text" name="author">
                                        <label for="email">Ваш Email: (Не публикуется)</label>
                                        <input type="text" name="email">
                                        <div class="captcha">
                                            <input type="text" name="captcha" size="15" placeholder="Текст с картинки">
                                            <input type="hidden" id="captcha_sid" name="captcha_sid" value="<?=$code?>">
                                            <img id="CaptchaImage" src="/bitrix/tools/captcha.php?captcha_sid=<?=$code?>" alt="CAPTCHA">
                                            <div id="captcha_control"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-md-pull-6">
                                        <label for="name">Отзыв:</label>
                                        <textarea name="content" rows="13"></textarea><br>
                                        <input type="submit" class="s_btn type2" align="absmiddle" value="Отправить">
                                        <div class="errors"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </form>
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "productReviews",
                            array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "N",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array(
                                    0 => "",
                                    1 => "",
                                ),
                                "FILTER_NAME" => "reviewList",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "20",
                                "IBLOCK_TYPE" => "Contact_with_user",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "5",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "customPagination",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array(
                                    0 => "NAME",
                                    1 => "",
                                ),
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "ASC",
                                "STRICT_SECTION_CHECK" => "N",
                                "COMPONENT_TEMPLATE" => "productReviews"
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? else : ?>
    <div class="big_item">
        <?
        $item = reset( $arResult["OFFERS"]);
        $selected_offers = 0;
        ?>
        <input class="buttonSend" onclick="window.history.back();" type="button" value="> Назад к списку">
        <div class="title">
            <h1><?=$arResult["NAME"];?></h1>
            <?if(intval(trim($arResult['allStatusValue'][0])) === 1) : ?>
                <div class="we_have">
                    <span>В наличии <i class="fa fa-check-circle" aria-hidden="true"></i></span>
                </div>
            <?elseif (intval(trim($arResult['allStatusValue'][0])) === 0) : ?>
                <div class="zakaz">
                    <span>Доставка до 3-х дней<i><img src="" alt=""></i></span>
                    <span class="sub_text">Цена товара<br>может измениться.</span>
                </div>
            <?else  : ?>
                  <div class="no_zakaz">
                    <span>Нет в наличии</span>
                </div>
            <?endif;?>
        </div>
        <div class="row">
            <?ob_start(); ?>
            <div class="col-md-5">
                <div class="image image_carousel">
                    <div class="main_img">
                        #DISCOUNT#
                        <?if ($item["PROPERTIES"]["photoName"]["VALUE"]) : ?>
                            <div class="photoalbum rphoto">
                                <div class="owl-carousel  owl-theme">

                                    <?if ($item["PROPERTIES"]["photoName"]["VALUE"]) : ?>
                                        <?foreach($item["PROPERTIES"]["photoName"]["VALUE"] as $photo) : ?>
                                            <div>
                                                <?if ($photo["NEW"]) : ?>
                                                    <a href="<?=$photo["ORIGIN"];?>"><img  src="<?=$photo["NEW"];?>" data-default="<?=$photo["ORIGIN"];?>"></a>
                                                <?else :?>
                                                    <a href="<?=$photo["ORIGIN"];?>"><img src="<?=$photo["ORIGIN"];?>"  data-default="<?=$photo["ORIGIN"];?>"></a>
                                                <?endif;?>
                                            </div>
                                        <?endforeach;?>
                                    <?else : ?>
                                        <div>
                                            <a href="<?=$arResult["DETAIL_PICTURE"];?>">
                                                <img src="<?=$arResult["DETAIL_PICTURE"];?>">
                                            </a>
                                        </div>
                                    <?endif;?>
                                </div>
                            </div>
                        <?else:?>
                            <a href="<?=DEFAULT_TEMPLATE_PATH."/img/no_image.png";?>">
                                <img id="image_l" src="<?=DEFAULT_TEMPLATE_PATH."/img/no_image.png";?>">
                            </a>
                        <?endif;?>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="item_info changeMarket">
                    <form method="post" action="/ajax/addToCartMarket.php" data-role="buy">
                        <input class="allPrices" type="hidden" name="allPrices" value="#OFFERS_PRICES#">
                        <input class="allDiscountValue" type="hidden" name="allDiscountValue" value='#OFFERS_DISCOUNT#'>
                        <input class="allId" type="hidden" name="allId" value="<?=htmlspecialchars(json_encode($arResult['allId'], ENT_NOQUOTES ))?>">
                        <input class="allPropertiesName" type="hidden" name="allPropertiesName" value='<? echo htmlspecialchars(json_encode($arResult['allPropertiesName'], ENT_NOQUOTES ))?>'>
                        <input class="allPropertiesValue" type="hidden" name="allPropertiesValue" value='<? echo htmlspecialchars(json_encode($arResult['allPropertiesValue'], ENT_NOQUOTES ))?>'>
                        <input class="allPropertiesImages" type="hidden" name="allPropertiesImages" value='<? echo htmlspecialchars(json_encode($arResult['allPropertiesImages'], ENT_NOQUOTES ))?>'>
                        <input class="allStatusValue" type="hidden" name="allStatusValue" value='<? echo htmlspecialchars(json_encode($arResult['allStatusValue'], ENT_NOQUOTES ))?>'>
                        <table class="acrticle_select custom_chekbox">
                            <tbody>
                            <tr>
                                <th>Артикул</th>
                                <th>Наименование</th>
                            </tr>
                            <?$index = 0?>
                            <?foreach ($arResult["OFFERS"] as $offer) :?>
                                    <tr>
                                        <td>
                                            <?if ($index == $selected_offers) : ?>
                                                <input value="<?=$index?>" type="radio" name="select" id="<?=$offer["ID"]?>"  class="select" checked >
                                            <?else :?>
                                                <input value="<?=$index?>" type="radio" name="select" id="<?=$offer["ID"]?>"  class="select">
                                            <?endif;?>
                                            <label for="<?=$offer["ID"]?>" class="css-label radGroup2">
                                                <?=$offer["PROPERTIES"]["article"]["VALUE"];?>
                                            </label>
                                            <?$index += 1;?>
                                        </td>
                                        <td>
                                            <?=$offer["TRADE_NAME"];?>
                                        </td>
                                    </tr>
                            <?endforeach;?>
                            <input style="display: none" class="cost" value = "#PRICE#"/>
                            <input style="display: none" class="id" value = "<?=$item["ID"];?>"/>
                            </tbody>
                        </table>
                        <div class="count_block">
                            <div class="spinEdit" data-available-quantity="122">
                                <input class="input-quantity spinedit noSelect quantity" value="1"><div class="spinedit"><div class="minus-btn icon-chevron-down minus_prod_det"><i class="fa fa-minus" aria-hidden="true"></i></div><div class="plus_prod_det plus-btn icon-chevron-up"><i class="fa fa-plus" aria-hidden="true"></i></div></div>
                            </div>
                        </div>
                        <div class="price_block">
                            <span class="price">
                                <span class="sum">#PRICE#</span>
                                <i class="fa fa-rub fa-3"></i>
                            </span>
                            <?if(intval(trim($arResult['allStatusValue'][0])) === 1 || intval(trim($arResult['allStatusValue'][0])) === 0) : ?>
                                <input type="submit" class="b_btn reverse addToCart" align="absmiddle" value="В корзину" data-role="buy-btn">
                            <?else:?>
                                <?$APPLICATION->IncludeComponent(
                                    "custom:catalog.product.subscribe",
                                    "",
                                    Array(
                                        "BUTTON_CLASS" => "b_btn forCorrectChangeMarket",
                                        "BUTTON_ID" => $arResult["SUBSCRIBE_ID"] + 1000,
                                        "CACHE_TIME" => "3600",
                                        "CACHE_TYPE" => "N",
                                        "PRODUCT_ID" =>  $arResult["SUBSCRIBE_ID"]
                                    )
                                );?>
                            <?endif;?>
                        </div>
                    </form>
                </div>
            </div>
            <?
            $buyBlockHTML = ob_get_clean();
            itc\CUncachedArea::show('productOfferBlock', array(
                'id' => $arResult["SUBSCRIBE_ID"],
                'iblock_id' => $arResult["IBLOCK_ID"],
                "offersIds" => $arResult['allId'],
                "buyBlockHTML" => $buyBlockHTML
            ));
            ?>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <ul class="menu tabs_menu">
                    <li><a href="#about" class="open">Описание</a></li>
                    <li><a href="#har">Характеристики</a></li>
                    <?if($arResult["PROPERTIES"]["file"]["VALUE"] || $arResult["PROPERTIES"]["youtube"]["VALUE"]) :?>
                        <li><a href="#instuctions">Видео и инструкции</a></li>
                    <?endif;?>
                    <li><a href="#comments">Отзывы о товаре</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="tabs" class="tabs">
                    <div class="about open">
                        <?=$arResult['DETAIL_TEXT'];?>
                        <br> <br>
                    </div>
                    <div class="har">
                        <br>
                        <table class="propertys">
                            <tbody>
                            <?unset($item["PROPERTIES"]["photoName"])?>
                            <?foreach($item["PROPERTIES"] as $prop) : ?>
                                <?if(str_replace(' ', '', $prop["VALUE"])) : ?>
                                    <tr valign="top">
                                        <td class="propertyName" nowrap="nowrap"><?=$prop["NAME"];?></td>
                                        <td class="propertyValue" width="100%"> <?=$prop["VALUE"];?></td>
                                    </tr>
                                <?endif;?>
                            <?endforeach;?>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <?if($arResult["PROPERTIES"]["file"]["VALUE"] || $arResult["PROPERTIES"]["youtube"]["VALUE"]) :?>
                        <div class="instuctions">
                            <?foreach($arResult["PROPERTIES"]["file"]["VALUE"] as $val) : ?>
                                <?$rsFile =  CFile::GetFileArray($arResult["PROPERTIES"]["file"]["VALUE"][0]);?>
                                <div class="pdfs">
                                    <a class="download" target="blank" href="<?=$rsFile["SRC"]?>">Загрузить инструкцию</a>
                                </div>
                            <?endforeach;?>
                            <?echo htmlspecialchars_decode($arResult["PROPERTIES"]["youtube"]["VALUE"]);?>
                        </div>
                    <?endif;?>
                    <div class="comments">
                        <div class="faq_form">
                            <span class="title">Добавить отзыв о товаре:</span>
                            <form action="/ajax/sendProductReview.php" method="POST" onsubmit="Box.submit(this); return false;" class="checkProductReview" id="collapse">
                                <div class="row">
                                    <div class="col-md-6 col-md-push-6">
                                        <input type="hidden" id="productID" name="productID" value="<?=$arResult["ID"]?>">
                                        <label for="name">Ваше имя:</label>
                                        <input type="text" name="author">
                                        <label for="email">Ваш Email: (Не публикуется)</label>
                                        <input type="text" name="email">
                                        <div class="captcha">
                                            <input type="text" name="captcha" size="15" placeholder="Текст с картинки">
                                            <input type="hidden" id="captcha_sid" name="captcha_sid" value="<?=$code?>">
                                            <img id="CaptchaImage" src="/bitrix/tools/captcha.php?captcha_sid=<?=$code?>" alt="CAPTCHA">
                                            <div id="captcha_control"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-md-pull-6">
                                        <label for="name">Отзыв:</label>
                                        <textarea name="content" rows="13"></textarea><br>
                                        <input type="submit" class="s_btn type2" align="absmiddle" value="Отправить">
                                        <div class="errors"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </form>
                        </div>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "productReviews",
                            array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "N",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array(
                                    0 => "",
                                    1 => "",
                                ),
                                "FILTER_NAME" => "reviewList",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "20",
                                "IBLOCK_TYPE" => "Contact_with_user",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "5",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "customPagination",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array(
                                    0 => "NAME",
                                    1 => "",
                                ),
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "ASC",
                                "STRICT_SECTION_CHECK" => "N",
                                "COMPONENT_TEMPLATE" => "productReviews"
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?endif;?>
<!--Микроразметка-->
<script type="application/ld+json">
    {
        "@context": "https://schema.org/",
        "@type": "Product",
        "name": "<?=$arResult["NAME"]?>",
        "image": "<?=$arResult["IMAGE"]?>",
        "description": "<?=$arResult["DETAIL_TEXT"]?>",
        "brand": "<?=$arResult["PROPERTIES"]["c"]["VALUE"]?>",
        "sku": "<?=$arResult["ARTICLE"]?>",
        "offers": {
            "@type": "Offer",
            "url": "<?='https://' . $_SERVER['SERVER_NAME'] . $arResult["DETAIL_PAGE_URL"]?>",
            "priceCurrency": "RUB",
            "price": "<?=($arResult["ITEM_PRICES"][0]["PRICE"]) ? $arResult["ITEM_PRICES"][0]["PRICE"] : $arResult["OFFERS"][0]["ITEM_PRICES"][0]["PRICE"]?>",
            "priceValidUntil": "<?= date("Y-m-d", mktime(0, 0, 0, date('m'), date('d'), date('Y') + 3))?>",
            "availability": "<?=(intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 0 || intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) === 1) ? "https://schema.org/InStock" : "https://schema.org/OutOfStock"?>"
        }
    }
</script>
<!--!Микроразметка-->

<script>
   var hash = window.location.hash;
   if (hash) {
       url = hash.replace('#', '');
       $('#tabs div').removeClass('open');
       $('.tabs_menu a').removeClass('open');
       $("a[href='" + hash + "']").addClass('open');
       $('.' + url).addClass('open');
   }
   else {
       $('.tabs_menu li:first-child a').addClass('open');
       $('#tabs div:first-child').addClass('open');
   }
</script>
