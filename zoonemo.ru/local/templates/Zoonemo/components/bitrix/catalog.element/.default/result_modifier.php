<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
unset($arResult["PROPERTIES"]["STATUS_GROUP"]);
($arResult["DETAIL_PICTURE"]["SRC"])
    ? ($arResult["IMAGE"] = 'https://' . $_SERVER['SERVER_NAME'] . $arResult["DETAIL_PICTURE"]["SRC"])
    : ($arResult["IMAGE"] = "");
//TODO начальные значения
$arResult["ARTICLE"] = $arResult["PROPERTIES"]["article"]["VALUE"];
$arResult["SUBSCRIBE_ID"] = $arResult["ID"];
$arFilters = Array(
    array(
        "name"=> "watermark",
        "position"=>"bl",
        'type'=>'text',
        "coefficient" => "5",
        'color' => 'B12A0F',
		'text' => $arResult["XML_ID"],
        "font"=> $_SERVER["DOCUMENT_ROOT"]. DEFAULT_TEMPLATE_PATH ."/fonts/OpenSans-Bold.ttf",
        "use_copyright"=>"Y"
    )
);
unset($arResult["PROPERTIES"]["recommend"]);
//TODO Модификация данных
if (count($arResult["OFFERS"]) > 1) {
    unset($arResult["PROPERTIES"]["article"]);
    unset($arResult["PROPERTIES"]["photoName"]);
    if (intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) !== -1 && intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) !== -2) {
        unset($arResult["PROPERTIES"]["status"]["VALUE"]);
    }
    $allId = $allPropertiesName = $allPropertiesValue = $allPropertiesImages = $allStatusValue = $allDiscountValue = [];
    foreach ($arResult["OFFERS"] as $key => &$offer) {
        //TODO Удаляем ненужные свойства
        unset($offer["PROPERTIES"]["CML2_LINK"]);
        unset($offer["PROPERTIES"]["warranty"]);
        unset($offer["PROPERTIES"]["delivery"]);
        //Если не задано наименование, то такой товар мы не должны выводить как тп
        if (!$offer["PROPERTIES"]["name"]["VALUE"]) {
            unset($arResult["OFFERS"][$key]);
            continue;
        }
        else {
            $offer["TRADE_NAME"] = $offer["PROPERTIES"]["name"]["VALUE"];
        }
        unset($offer["PROPERTIES"]["name"]);
        if (intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) !== -1 && intval(trim($arResult["PROPERTIES"]["status"]["VALUE"])) !== -2) {
            ($offer["PROPERTIES"]["status"]["VALUE"] == 1) ? ($allStatusValue[] = 1) : ($allStatusValue[] = 0);
        }
        else {
            $allStatusValue[] = -1;
        }
        $allId[] = $offer["ID"];
        $propsName = $propsValue = $propsImage = [];
        foreach($offer["PROPERTIES"] as $propKey => $prop) {
            if ($prop["CODE"] == "photoName") {
                if ($prop["VALUE"] && $offer["DETAIL_PICTURE"]) {
                    $imgData = array_merge(array($offer["DETAIL_PICTURE"]), $prop["VALUE"]);
                }
                else if ($prop["VALUE"] && $arResult["DETAIL_PICTURE"]) {
                    $imgData = array_merge(array($arResult["DETAIL_PICTURE"]),  $prop["VALUE"]);
                }
                else if ($prop["VALUE"]) {
                    $imgData = $prop["VALUE"];
                }
                else if ($offer["DETAIL_PICTURE"]) {
                    $imgData = array($offer["DETAIL_PICTURE"]);
                }
                else {
                    $imgData = array($arResult["DETAIL_PICTURE"]);
                }

                foreach($imgData as $imageKey => $image) {
                    $arFilters[0]['text'] = $offer["XML_ID"];
                    if ($image["FILE_NAME"]) {
                        $photo = $image;
                    }
                    else {
                        $photo = CFile::GetFileArray($image);
                        $photo =  $photo["ID"];
                    }
                    $newPhoto = CFile::ResizeImageGet($photo, array("width"=> 450, "height" => 450), BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                    $originalPhoto = CFile::ResizeImageGet($photo, array("width"=> $file["WIDTH"], "height" => $file["HEIGHT"]), BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                    $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey] = array();
                    if ($newPhoto) {
                        $propsImage[] = array($newPhoto['src'], $originalPhoto['src']);
                        $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey]["NEW"] = $newPhoto['src'];
                    }
                    else {
                        $propsImage[] = $originalPhoto['src'];
                        $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey]["NEW"] = $originalPhoto['src'];
                    }
                    $arResult["OFFERS"][$key]["PROPERTIES"][$propKey]["VALUE"][$imageKey]["ORIGIN"] = $originalPhoto['src'];
                }
            }
            else if ($prop["VALUE"] && $prop["CODE"] != "status") {
                $propsName[$prop["NAME"]] = $prop["NAME"];
                $propsValue[$prop["NAME"]] = $prop["VALUE"];
            }
            else {
                unset($offer["PROPERTIES"][$propKey]);
            }
        }
        foreach($arResult["PROPERTIES"] as $prop) {
            if ($prop["CODE"] != 'photoName' && $prop["CODE"] != 'photoName' && $prop["CODE"] != 'file' && $prop["CODE"] != 'youtube' && $prop["CODE"] != 'status' && !$propsName[$prop["CODE"]] && $prop["VALUE"]) {
                $propsName[$prop["CODE"]] = $prop["NAME"];
                $propsValue[$prop["CODE"]] = $prop["VALUE"];
                $arResult["OFFERS"][$key]["PROPERTIES"][$prop["CODE"]]["VALUE"] = $prop["VALUE"];
                $arResult["OFFERS"][$key]["PROPERTIES"][$prop["CODE"]]["NAME"] = $prop["NAME"];
            }
        }
        $allPropertiesName[] = $propsName;
        $allPropertiesValue[] = $propsValue;
        $allPropertiesImages[] = $propsImage;
    }
    unset($offer);
    $arResult['allId'] = $allId;
    $arResult['allPropertiesName'] = $allPropertiesName;
    $arResult['allPropertiesValue'] = $allPropertiesValue;
    $arResult['allPropertiesImages'] = $allPropertiesImages;
    $arResult['allStatusValue'] = $allStatusValue;
    $arResult['allDiscountValue'] = $allDiscountValue;
}


//TODO Если у товара одно торговое предложение
if (count($arResult["OFFERS"]) == 1) {
    foreach ($arResult["OFFERS"] as $key=>&$offer) {
        $arResult["OFFERS"] = $offer;
        $arResult["ID"] = $arResult["OFFERS"]["ID"];
        break;
    }
    foreach ($arResult["OFFERS"]["PROPERTIES"] as $key=>$prop) {
        if ($prop["VALUE"]) {
            $arResult["PROPERTIES"][$prop["CODE"]] = $prop;
        }
    }
    if ($arResult["OFFERS"]["DETAIL_PICTURE"]) {
        $arResult["DETAIL_PICTURE"] = $arResult["OFFERS"]["DETAIL_PICTURE"];
    }
    unset($arResult["OFFERS"]);
    unset($arResult["PROPERTIES"]["CML2_LINK"]);
    unset($arResult["PROPERTIES"]["warranty"]);
    unset($arResult["PROPERTIES"]["delivery"]);
    unset($arResult["PROPERTIES"]["name"]);
}


//TODO Если остался только товар без торговых предложений
if (count($arResult["OFFERS"]) === 0) {
    (!$arResult["PROPERTIES"]["photoName"]["VALUE"])
        ? $arResult["PROPERTIES"]["photoName"]["VALUE"] = array($arResult["DETAIL_PICTURE"])
        : $arResult["PROPERTIES"]["photoName"]["VALUE"] = array_merge(array($arResult["DETAIL_PICTURE"]),  $arResult["PROPERTIES"]["photoName"]["VALUE"]);
    foreach ($arResult["PROPERTIES"]["photoName"]["VALUE"] as $key=>$val) {
        if ($val["FILE_NAME"]) {
            $photo = $val;
        }
        else {
            $photo = CFile::GetFileArray($val);
            $photo =  $photo["ID"];
        }
        $arFilters[0]['text'] = $arResult["XML_ID"];
        $newPhoto = CFile::ResizeImageGet($photo, array("width"=> 450, "height" => 450), BX_RESIZE_IMAGE_EXACT, true, $arFilters);
        $originalPhoto = CFile::ResizeImageGet($photo, array("width"=> $file["WIDTH"], "height" => $file["HEIGHT"]), BX_RESIZE_IMAGE_EXACT, true, $arFilters);
        $arResult["PROPERTIES"]["photoName"]["VALUE"][$key] = null;
        $arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["ORIGIN"] = $originalPhoto['src'];
        ($newPhoto)
            ? ($arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["NEW"] = $newPhoto['src'])
            : ($arResult["PROPERTIES"]["photoName"]["VALUE"][$key]["NEW"] = $originalPhoto['src']);
    }
}
