<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;
CJSCore::init(array('popup'));
$randomString = $this->randString();
$APPLICATION->setTitle(Loc::getMessage('CPSL_SUBSCRIBE_TITLE_NEW'));
if(!$arResult['USER_ID'] && !isset($arParams['GUEST_ACCESS'])):?>
    <?
    $contactTypeCount = count($arResult['CONTACT_TYPES']);
    $authStyle = 'display: block;';
    $identificationStyle = 'display: none;';
    if(!empty($_GET['result']))
    {
        $authStyle = 'display: none;';
        $identificationStyle = 'display: block;';
    }
    ?>

    <div class="row">
        <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
            <div class="alert alert-danger"><?=Loc::getMessage('CPSL_SUBSCRIBE_PAGE_TITLE_AUTHORIZE')?></div>
        </div>
        <? $authListGetParams = array(); ?>
        <div class="col-md-8 col-sm-7" id="catalog-subscriber-auth-form" style="<?=$authStyle?>">
            <?$APPLICATION->authForm('', false, false, 'N', false);?>
            <hr class="bxe-light">
        </div>
    </div>

    <?$APPLICATION->setTitle(Loc::getMessage('CPSL_TITLE_PAGE_WHEN_ACCESSING'));?>


    <div class="row" id="catalog-subscriber-identification-form" style="<?=$identificationStyle?>">
        <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
            <div class="row">
                <div class="col-lg-12 catalog-subscriber-identification-form">
                    <h4><?=Loc::getMessage('CPSL_HEADLINE_FORM_SEND_CODE')?></h4>
                    <hr class="bxe-light">
                    <form method="post">
                        <?=bitrix_sessid_post()?>
                        <input type="hidden" name="siteId" value="<?=SITE_ID?>">
                        <?if($contactTypeCount > 1):?>
                            <div class="form-group">
                                <label for="contactType"><?=Loc::getMessage('CPSL_CONTACT_TYPE_SELECTION')?></label>
                                <select id="contactType" class="form-control" name="contactType">
                                    <?foreach($arResult['CONTACT_TYPES'] as $contactTypeData):?>
                                        <option value="<?=intval($contactTypeData['ID'])?>">
                                            <?=htmlspecialcharsbx($contactTypeData['NAME'])?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                        <?endif;?>
                        <div class="form-group">
                            <?
                            $contactLable = Loc::getMessage('CPSL_CONTACT_TYPE_NAME');
                            $contactTypeId = 0;
                            if($contactTypeCount == 1)
                            {
                                $contactType = current($arResult['CONTACT_TYPES']);
                                $contactLable = $contactType['NAME'];
                                $contactTypeId = $contactType['ID'];
                            }
                            ?>
                            <label for="contactInputOut"><?=htmlspecialcharsbx($contactLable)?></label>
                            <input type="text" class="form-control" name="userContact" id="contactInputOut">
                            <input type="hidden" name="subscriberIdentification" value="Y">
                            <?if($contactTypeId):?>
                                <input type="hidden" name="contactType" value="<?=$contactTypeId?>">
                            <?endif;?>
                        </div>
                        <button type="submit" class="btn btn-default"><?=Loc::getMessage('CPSL_BUTTON_SUBMIT_CODE')?></button>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h4><?=Loc::getMessage('CPSL_HEADLINE_FORM_FOR_ACCESSING')?></h4>
                    <hr class="bxe-light">
                    <form method="post">
                        <?=bitrix_sessid_post()?>
                        <div class="form-group">
                            <label for="contactInputCheck"><?=htmlspecialcharsbx($contactLable)?></label>
                            <input type="text" class="form-control" name="userContact" id="contactInputCheck" value=
                            "<?=!empty($_GET['contact']) ? htmlspecialcharsbx(urldecode($_GET['contact'])): ''?>">
                        </div>
                        <div class="form-group">
                            <label for="token"><?=Loc::getMessage('CPSL_CODE_LABLE')?></label>
                            <input type="text" class="form-control" name="subscribeToken" id="token">
                            <input type="hidden" name="accessCodeVerification" value="Y">
                        </div>
                        <button type="submit" class="btn btn-default"><?=Loc::getMessage('CPSL_BUTTON_SUBMIT_ACCESS')?></button>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        BX.ready(function() {
            if(BX('cpsl-auth'))
            {
                BX.bind(BX('cpsl-auth'), 'click', BX.delegate(showAuthForm, this));
                BX.bind(BX('cpsl-identification'), 'click', BX.delegate(showAuthForm, this));
            }
            function showAuthForm()
            {
                var formType = BX.proxy_context.id.replace('cpsl-', '');
                var authForm = BX('catalog-subscriber-auth-form'),
                    codeForm = BX('catalog-subscriber-identification-form');
                if(!authForm || !codeForm || !BX('catalog-subscriber-'+formType+'-form')) return;

                BX.style(authForm, 'display', 'none');
                BX.style(codeForm, 'display', 'none');
                BX.style(BX('catalog-subscriber-'+formType+'-form'), 'display', '');
            }
        });
    </script>
<?endif;

?>
    <script type="text/javascript">
        BX.message({
            CPSL_MESS_BTN_DETAIL: '<?=('' != $arParams['MESS_BTN_DETAIL']
                ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CPSL_TPL_MESS_BTN_DETAIL'));?>',

            CPSL_MESS_NOT_AVAILABLE: '<?=('' != $arParams['MESS_BTN_DETAIL']
                ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CPSL_TPL_MESS_BTN_DETAIL'));?>',
            CPSL_BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CPSL_CATALOG_BTN_MESSAGE_BASKET_REDIRECT');?>',
            CPSL_BASKET_URL: '<?=$arParams["BASKET_URL"];?>',
            CPSL_TITLE_ERROR: '<?=GetMessageJS('CPSL_CATALOG_TITLE_ERROR') ?>',
            CPSL_TITLE_BASKET_PROPS: '<?=GetMessageJS('CPSL_CATALOG_TITLE_BASKET_PROPS') ?>',
            CPSL_BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CPSL_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
            CPSL_BTN_MESSAGE_SEND_PROPS: '<?=GetMessageJS('CPSL_CATALOG_BTN_MESSAGE_SEND_PROPS');?>',
            CPSL_BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CPSL_CATALOG_BTN_MESSAGE_CLOSE') ?>',
            CPSL_STATUS_SUCCESS: '<?=GetMessageJS('CPSL_STATUS_SUCCESS');?>',
            CPSL_STATUS_ERROR: '<?=GetMessageJS('CPSL_STATUS_ERROR') ?>'
        });
    </script>
<?

if(!empty($_GET['result']) && !empty($_GET['message']))
{
    $successNotify = strpos($_GET['result'], 'Ok') ? true : false;
    $postfix = $successNotify ? 'Ok' : 'Fail';
    $popupTitle = Loc::getMessage('CPSL_SUBSCRIBE_POPUP_TITLE_'.strtoupper(str_replace($postfix, '', $_GET['result'])));

    $arJSParams = array(
        'NOTIFY_USER' => true,
        'NOTIFY_POPUP_TITLE' => $popupTitle,
        'NOTIFY_SUCCESS' => $successNotify,
        'NOTIFY_MESSAGE' => urldecode($_GET['message']),
    );
    ?>
    <script type="text/javascript">
        var <?='jaClass_'.$randomString;?> = new JCCatalogProductSubscribeList(<?=CUtil::PhpToJSObject($arJSParams, false, true);?>);
    </script>
    <?
}

if (!empty($arResult['ITEMS']))
{
    $skuTemplate = array();
    if (!empty($arResult['SKU_PROPS']))
    {
        foreach ($arResult['SKU_PROPS'] as $itemId => $arProp)
        {
            foreach($arProp as $propId => $prop)
            {
                $propId = $prop['ID'];
                $skuTemplate[$itemId][$propId] = array(
                    'SCROLL' => array(
                        'START' => '',
                        'FINISH' => '',
                    ),
                    'FULL' => array(
                        'START' => '',
                        'FINISH' => '',
                    ),
                    'ITEMS' => array()
                );
                $templateRow = '';
                if ('TEXT' == $prop['SHOW_MODE'])
                {
                    $skuTemplate[$itemId][$propId]['SCROLL']['START'] = '<div class="bx_item_detail_size full" id="#ITEM#_prop_'.$propId.'_cont">'.
                        '<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($prop['NAME']).'</span>'.
                        '<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';;
                    $skuTemplate[$itemId][$propId]['SCROLL']['FINISH'] = '</ul></div>'.
                        '<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style=""></div>'.
                        '<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style=""></div>'.
                        '</div></div>';

                    $skuTemplate[$itemId][$propId]['FULL']['START'] = '<div class="bx_item_detail_size" id="#ITEM#_prop_'.$propId.'_cont">'.
                        '<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($prop['NAME']).'</span>'.
                        '<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';;
                    $skuTemplate[$itemId][$propId]['FULL']['FINISH'] = '</ul></div>'.
                        '<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style="display: none;"></div>'.
                        '<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style="display: none;"></div>'.
                        '</div></div>';
                    foreach ($prop['VALUES'] as $value)
                    {
                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);
                        $skuTemplate[$itemId][$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="'.$propId.'_'.$value['ID'].
                            '" data-onevalue="'.$value['ID'].'" style="width: #WIDTH#;" title="'.$value['NAME'].'"><i></i><span class="cnt">'.$value['NAME'].'</span></li>';
                    }
                    unset($value);
                }
                elseif ('PICT' == $prop['SHOW_MODE'])
                {
                    $skuTemplate[$itemId][$propId]['SCROLL']['START'] = '<div class="bx_item_detail_scu full" id="#ITEM#_prop_'.$propId.'_cont">'.
                        '<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($prop['NAME']).'</span>'.
                        '<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';
                    $skuTemplate[$itemId][$propId]['SCROLL']['FINISH'] = '</ul></div>'.
                        '<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style=""></div>'.
                        '<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style=""></div>'.
                        '</div></div>';

                    $skuTemplate[$itemId][$propId]['FULL']['START'] = '<div class="bx_item_detail_scu" id="#ITEM#_prop_'.$propId.'_cont">'.
                        '<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($prop['NAME']).'</span>'.
                        '<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';
                    $skuTemplate[$itemId][$propId]['FULL']['FINISH'] = '</ul></div>'.
                        '<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style="display: none;"></div>'.
                        '<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style="display: none;"></div>'.
                        '</div></div>';
                    foreach ($prop['VALUES'] as $value)
                    {
                        $value['NAME'] = htmlspecialcharsbx($value['NAME']);
                        $skuTemplate[$itemId][$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="'.$propId.'_'.$value['ID'].
                            '" data-onevalue="'.$value['ID'].'" style="width: #WIDTH#; padding-top: #WIDTH#;"><i title="'.$value['NAME'].'"></i>'.
                            '<span class="cnt"><span class="cnt_item" style="background-image:url(\''.$value['PICT']['SRC'].'\');" title="'.$value['NAME'].'"></span></span></li>';
                    }
                    unset($value);
                }
            }
        }
        unset($templateRow, $prop);
    }

    ?>
    <div class="bx_item_list_you_looked_horizontal col<?=$arParams['LINE_ELEMENT_COUNT'];?>">
        <div class="bx_item_list_section">
            <div class="bx_item_list_slide active">
                <? foreach ($arResult['ITEMS'] as $key => $arItem)
                {
                    $strMainID = $this->GetEditAreaId($arItem['ID']);

                    $arItemIDs = array(
                        'ID' => $strMainID,
                        'PICT' => $strMainID . '_pict',
                        'SECOND_PICT' => $strMainID . '_secondpict',
                        'MAIN_PROPS' => $strMainID . '_main_props',

                        'QUANTITY' => $strMainID . '_quantity',
                        'QUANTITY_DOWN' => $strMainID . '_quant_down',
                        'QUANTITY_UP' => $strMainID . '_quant_up',
                        'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
                        'BUY_LINK' => $strMainID . '_buy_link',
                        'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
                        'SUBSCRIBE_DELETE_LINK' => $strMainID . '_delete_subscribe',

                        'PRICE' => $strMainID . '_price',
                        'DSC_PERC' => $strMainID . '_dsc_perc',
                        'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',

                        'PROP_DIV' => $strMainID . '_sku_tree',
                        'PROP' => $strMainID . '_prop_',
                        'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
                        'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
                    );

                    $strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

                    $strTitle = (
                    isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
                    && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
                        ? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
                        : $arItem['NAME']
                    );
                    $showImgClass = $arParams['SHOW_IMAGE'] != "Y" ? "no-imgs" : "";

                    ?>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card_mini a" style="height : 300px;">
                                    <form method="POST">
                                        <div class="image">
                                            <?
                                            $str = str_replace($arItem["CODE"], '', $arItem["DETAIL_PAGE_URL"]);
                                            $arItem["DETAIL_PAGE_URL"] =$str . $arItem["ID"];
                                            ?>
                                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                                <?
                                                $file = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]["ID"]);
                                                $arFilters = Array(
                                                    array("name"=> "watermark",
                                                        "position"=>"bl",
                                                        'type'=>'text',
                                                        "coefficient" => "5",
                                                        'text' => $arItem["PROPERTIES"]["ARTNUMBER"]["VALUE"],
                                                        'color' => 'B12A0F',
                                                        "font"=> $_SERVER["DOCUMENT_ROOT"]. DEFAULT_TEMPLATE_PATH ."/fonts/OpenSans-Bold.ttf",
                                                        "use_copyright"=>"Y")
                                                );
                                                $newPhoto = CFile::ResizeImageGet(
                                                    $file["ID"],
                                                    array("width"=> 96, "height" => 96),
                                                    BX_RESIZE_IMAGE_EXACT,
                                                    true,
                                                    $arFilters
                                                );
                                                ?>
                                                <?if (!empty($newPhoto)) : ?>
                                                    <img src="<?=$newPhoto['src'];?>" alt=""/>
                                                <?else :?>
                                                    <img src="<?=$file['SRC'];?>" alt=""/>
                                                <?endif;?>
                                            </a>
                                        </div>
                                        <div class="description">
                                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                                <span><?=$arItem["NAME"];?></span>
                                            </a>
                                        </div>
                                        <div class="bx_catalog_item_controls">
                                            <div>
                                                <a class="btn b_btn" href="<?=$arItem['DETAIL_PAGE_URL'];?>">
                                                    <?=GetMessage('CPSL_TPL_MESS_BTN_DETAIL');?>
                                                </a>
                                            </div>
                                            <div>
                                                <a id="<?=$arItemIDs['SUBSCRIBE_DELETE_LINK'];?>" class="btn-link b_btn" href="javascript:void(0)">
                                                    <?=GetMessage('CPSL_TPL_MESS_BTN_UNSUBSCRIBE');?>
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?$arJSParams = array(
                    'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
                    'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                    'SHOW_ADD_BASKET_BTN' => false,
                    'SHOW_BUY_BTN' => true,
                    'SHOW_ABSENT' => true,
                    'PRODUCT' => array(
                        'ID' => $arItem['ID'],
                        'NAME' => $arItem['~NAME'],
                        'PICT' => ('Y' == $arItem['SECOND_PICT']?$arItem['PREVIEW_PICTURE_SECOND']:$arItem['PREVIEW_PICTURE']),
                        'CAN_BUY' => $arItem["CAN_BUY"],
                        'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
                        'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
                        'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
                        'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
                        'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
                        'ADD_URL' => $arItem['~ADD_URL'],
                        'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
                        'LIST_SUBSCRIBE_ID' => $arParams['LIST_SUBSCRIPTIONS'],
                    ),
                    'BASKET' => array(
                        'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
                        'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                        'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
                        'EMPTY_PROPS' => $emptyProductProperties
                    ),
                    'VISUAL' => array(
                        'ID' => $arItemIDs['ID'],
                        'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
                        'QUANTITY_ID' => $arItemIDs['QUANTITY'],
                        'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
                        'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
                        'PRICE_ID' => $arItemIDs['PRICE'],
                        'BUY_ID' => $arItemIDs['BUY_LINK'],
                        'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
                        'DELETE_SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_DELETE_LINK'],
                    ),
                    'LAST_ELEMENT' => $arItem['LAST_ELEMENT'],
                );
                ?>
                    <script type="text/javascript">
                        var <?=$strObName;?> = new JCCatalogProductSubscribeList(
                            <?=CUtil::PhpToJSObject($arJSParams, false, true);?>);
                    </script>
                    <?
                }
                ?>
            </div>
        </div>
        <div style="clear: both;"></div>

    </div>
    <?
}
else
{
    if(isset($arParams['GUEST_ACCESS'])):
        echo '<h3>'.Loc::getMessage('CPSL_SUBSCRIBE_NOT_FOUND').'</h3>';
    endif;
}
?>