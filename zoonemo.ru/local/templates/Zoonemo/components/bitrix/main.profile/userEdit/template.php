<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if($arResult["SHOW_SMS_FIELD"] == true) {
    CJSCore::Init('phone_auth');
}
?>
<div class="user_cart">
    <h1>Личный кабинет</h1>
    <div class="user_info">
        <div class="row">
            <form method="post" action="/ajax/changeUserContacts.php" enctype="multipart/form-data" class="form_acc">
                <?=$arResult["BX_SESSION_CHECK"]?>
                <input type="hidden" name="sessid" id="sessid" value="e4058e3a311ee5b000710454c67bbdf1">
                <input type="hidden" name="lang" value="<?=LANG?>" />
                <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
                <div class="col-md-4 contact_form">
                    <div class="profile-block-<?=strpos($arResult["opened"], "reg") === false ? "hidden" : "shown"?>" id="user_div_reg">
                        <span class="title">Контактные данные</span>
                        <table>
                            <tbody>
                            <tr>
                                <td><?=GetMessage('NAME')?></td>
                                <td><input type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" /></td>
                            </tr>
                            <tr>
                                <td><?=GetMessage('LAST_NAME')?></td>
                                <td><input type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" /></td>
                            </tr>
                            <tr>
                                <td><?=GetMessage('SECOND_NAME')?></td>
                                <td><input type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" /></td>
                            </tr>
                            <script>
                                jQuery(function($){
                                    $('.userPhoneEdit').mask('+7 (999) 999-99-99');

                                    $('.userPhoneEdit').mask('+7 (999) 999-99-99');
                                    $('.userPhoneEdit').on("keyup", function(event) {
                                        var value = $(event.target).val();
                                        if (value[4] === "7" && value[0] == "+") {
                                            $('.userPhoneEdit').unmask();
                                            $('.userPhoneEdit').mask('+7 (999) 999-99-99');
                                            $('.userPhoneEdit').focus();
                                        }
                                        if (value[4] === "8" && value[0] == "+") {
                                            $('.userPhoneEdit').unmask();
                                            $('.userPhoneEdit').mask('8 (999) 999-99-99');
                                            $('.userPhoneEdit').focus();
                                        }
                                        if (value[3] === "7" && value[0] == "8") {
                                            $('.userPhoneEdit').unmask();
                                            $('.userPhoneEdit').mask('+7 (999) 999-99-99');
                                            $('.userPhoneEdit').focus();
                                        }
                                        if (value[3] === "8" && value[0] == "8") {
                                            $('.userPhoneEdit').unmask();
                                            $('.userPhoneEdit').mask('8 (999) 999-99-99');
                                            $('.userPhoneEdit').focus();
                                        }
                                    });
                                });
                            </script>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="col-md-4 adress_form">
                    <div class="profile-block-<?=strpos($arResult["opened"], "reg") === false ? "hidden" : "shown"?>" id="user_div_personal ">
                        <span class="title">Адрес доставки</span>
                        <table>
                            <tbody>
                                <td>Страна</td>
                                <td><input type="text" name="UF_COUNTRY" maxlength="255" value="<?=$arResult["arUser"]["UF_COUNTRY"]?>" /></td>
                            </tr>
                            <tr>
                                <td><?=GetMessage('USER_CITY')?></td>
                                <td><input type="text" name="PERSONAL_CITY" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_CITY"]?>" /></td>
                            </tr>
                            <tr>
                                <td><?=GetMessage('USER_ZIP')?></td>
                                <td><input type="text" name="PERSONAL_ZIP" maxlength="255" value="<?=$arResult["arUser"]["PERSONAL_ZIP"]?>" /></td>
                            </tr>

                            <tr>
                                <td><?=GetMessage("USER_STREET")?></td>
                                <td><input  type="text" name="PERSONAL_STREET" value="<?=$arResult["arUser"]["PERSONAL_STREET"]?>" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-4 adress_form">
                    <table>
                        <tbody>
                        <tr>
                            <td>Корпус</td>
                            <td><input type="text" name="UF_HOUSING" maxlength="255" value="<?=$arResult["arUser"]["UF_HOUSING"]?>" /></td>
                        </tr>
                        <tr>
                            <td>№ кв.</td>
                            <td><input type="text" name="UF_NOSQUARE" maxlength="255" value="<?=$arResult["arUser"]["UF_NOSQUARE"]?>" /></td>
                        </tr>
                        <tr>
                            <td>Подъезд</td>
                            <td><input type="text" name="UF_ENTRANCE" maxlength="255" value="<?=$arResult["arUser"]["UF_ENTRANCE"]?>" /></td>
                        </tr>
                        <tr>
                            <td>Этаж</td>
                            <td><input type="text" name="UF_FLOOR" maxlength="255" value="<?=$arResult["arUser"]["UF_FLOOR"]?>" /></td>
                        </tr>
                        <tr>
                            <td>Домофон</td>
                            <td><input type="text" name="UF_HOMENUMBER" maxlength="255" value="<?=$arResult["arUser"]["UF_HOMENUMBER"]?>" /></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="errors col-md-12"> </div>
                <div class="title col-md-12">
                    <div class="col-xs-6">
                        <div class="edit_btn"><input class="s_btn reverse edit" type="button" value="Редактировать"></div>
                    </div>
                    <br>
                    <div class="col-md-12 pass">
                        <div class="error_message"></div>
                    </div>
                    <div class="save_btn edit" style="display: none;">
                        <p><input type="submit" name="save">&nbsp;&nbsp;<input type="reset" class="reset" value="<?=GetMessage('MAIN_RESET');?>"></p>
                    </div>
                </div>
                <div class="title col-md-12">
                    <h1><a class="sale-order-history-link"  href="orderList/">Просмотреть историю заказов</a></h1>
                </div>
                <div class="col-md-12 pass">
                    <div class="error_message"></div>
                </div>
            </form>
        </div>
    </div>
</div>