<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
foreach ($arResult['SECTIONS'] as $key => $arSection) {
    $arMap[$arSection['ID']] = $key;
}
$rsSections = CIBlockSection::GetList([], ['ID' => array_keys($arMap)], false, ["ID", "PICTURE"]);
while ($arSection = $rsSections->GetNext()) {
    if (!isset($arMap[$arSection['ID']])) {
        continue;
    }
    $key = $arMap[$arSection['ID']];
    if (intval($arSection['PICTURE'])) {
        $file = CFile::GetFileArray(intval($arSection['PICTURE']));
        $newPhoto = CFile::ResizeImageGet($file["ID"], ["width"=> 132, "height" => 132], BX_RESIZE_IMAGE_EXACT, true);
        ($newPhoto["src"])
            ? $arResult['SECTIONS'][$key]['PICTURE'] = $newPhoto["src"]
            : $arResult['SECTIONS'][$key]['PICTURE'] = $file["SRC"];
    }
}