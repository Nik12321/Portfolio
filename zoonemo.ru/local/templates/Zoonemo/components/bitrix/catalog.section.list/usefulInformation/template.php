<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
$curIndex = 0;
?>
<div class="container-fluid">
    <?php foreach($arResult["SECTIONS"] as &$arItem) :?>
        <?php if ($curIndex == 0) :?>
            <div class="row-flex">
        <?php endif;?>
        <div class="col-md-1-5" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <?php
            $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "SECTION_EDIT"));
            $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "SECTION_DELETE"), [
                "CONFIRM"=>GetMessage("CT_BNL_SECTION_DELETE_CONFIRM")
            ]);
            ?>
            <span class="inf-txt"><br>
                <a href="<?=$arItem["SECTION_PAGE_URL"];?>">
                    <img src="<?=$arItem["PICTURE"];?>">
                </a>
                <a href="<?=$arItem["SECTION_PAGE_URL"];?>"><?=$arItem["NAME"];?></a>
            </span>
        </div>
        <?php if ($curIndex == 4) :?>
            </div>
            <?$curIndex = 0;?>
        <?php else :?>
            <?$curIndex++;?>
        <?php endif;?>
    <?php endforeach;?>
    <?php if ($curIndex != 0) :?>
</div>
<?php endif;?>
</div>
