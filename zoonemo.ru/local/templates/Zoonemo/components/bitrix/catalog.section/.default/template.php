<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use itc\CUncachedArea;

$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');
$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>
<?if (count($arResult['ITEMS'])) : ?>
    <div>
        <?
        foreach ($arResult['ITEMS'] as $item) :
            $is_this_market = false;
            $areaIds = array();
            $uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
            $areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
            $this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
            $this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
            ?>
            <?if($item["PRODUCT_MODE"] == "SIMPLE_TP") : ?>
            <div class="col-md-4 col-xs-6 good_item">
                <div class="good_item" data-role="product" data-type="">
                    <div class="card addToCart">
                        <div class="inner2">
                            <div class="complete">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 18">
                                    <path id="shopping-cart-3" class="cls-1" d="M1065.8,10l-3.43,12h-12.59l0.84,2h13.23l3.48-12h1.93l0.74-2h-4.2Zm-4.96,10,1.97-7H1046l2.94,7h11.9Zm-1.34,5a1.5,1.5,0,1,0,1.5,1.5A1.5,1.5,0,0,0,1059.5,25Zm-3.5,1.5a1.5,1.5,0,1,1-1.5-1.5A1.5,1.5,0,0,1,1056,26.5Z" transform="translate(-1046 -10)" />
                                </svg>
                                <span>Товар добавлен в корзину</span>
                            </div>
                        </div>
                        <div class="inner0">
                            <form action="/ajax/addToCart.php" method="POST">
                                <?ob_start(); ?>
                                <div class="image">
                                    #DISCOUNT#
                                    <a href="<?=$item["DETAIL_PAGE_URL"]?>">
                                        <img class="lazy" data-original="<?=$item["DETAIL_PICTURE"];?>" alt=""/>
                                    </a>
                                </div>
                                <div class="price_line">
                                    <input style="display: none" class="cost" value = "#PRICE#"/>
                                    <input style="display: none" class="id" value = "<?=$item["ID"];?>"/>
                                    <span>#PRICE#</span><i class="fa fa-rub fa-1"></i>
                                </div>
                                <?
                                $buyBlockHTML = ob_get_clean();
                                itc\CUncachedArea::show('productBuyBlock', array(
                                    'id' => $item["ID"],
                                    'iblock_id' => $item["IBLOCK_ID"],
                                    "buyBlockHTML" => $buyBlockHTML,
                                ));
                                ?>
                                <div class="description">
                                    <a href="<?=$item["DETAIL_PAGE_URL"]?>">
                                        <span><?=$item["NAME"];?></span>
                                    </a>
                                </div>
                                <?if(intval(trim($item["PROPERTIES"]["status"]["VALUE"])) === 1 || intval(trim($item["PROPERTIES"]["status"]["VALUE"])) === 0) : ?>
                                <input class="b_btn" type="submit" value = "Добавить товар в корзину"/>
                                <?else:?>
                                <? $APPLICATION->IncludeComponent(
                                    "custom:catalog.product.subscribe",
                                    "",
                                    Array(
                                        "BUTTON_CLASS" => "b_btn",
                                        "BUTTON_ID" => $item["SUBCRIBE_ID"],
                                        "CACHE_TIME" => "3600",
                                        "CACHE_TYPE" => "N",
                                        "PRODUCT_ID" => $item["SUBCRIBE_ID"]
                                    )
                                );?>
                                <?endif;?>
                                <a href="" class="b_btn f_look" data-role="quickView">Быстрый просмотр</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?else :  ?>
            <div class="col-md-4 col-xs-6 good_item">
                <div class="good_item" data-role="product" data-type="offers">
                    <div class="card addToCartMarket">
                        <div class="inner2">
                            <div class="complete">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 18">
                                    <path id="shopping-cart-3" class="cls-1" d="M1065.8,10l-3.43,12h-12.59l0.84,2h13.23l3.48-12h1.93l0.74-2h-4.2Zm-4.96,10,1.97-7H1046l2.94,7h11.9Zm-1.34,5a1.5,1.5,0,1,0,1.5,1.5A1.5,1.5,0,0,0,1059.5,25Zm-3.5,1.5a1.5,1.5,0,1,1-1.5-1.5A1.5,1.5,0,0,1,1056,26.5Z" transform="translate(-1046 -10)"></path>
                                </svg>
                                <span>Товар добавлен в корзину</span>
                            </div>
                        </div>
                        <div class="inner1">
                            <div class="inner_wrap">
                                <div class="table_cont">
                                    <?ob_start(); ?>
                                    <table>
                                        <tbody >
                                        <?foreach($item["OFFERS"] as $offer) : ?>
                                            <tr>
                                                <td class="cursive offerName"><?=$offer["NAME"];?></td>
                                                <td class="cursive">#OFFER_PRICE_<?=$offer["ID"];?>#&nbsp;<i class="fa fa-rub fa-1"></i></td>
                                                <td class="cursive">
                                                    <div class="spinEdit" data-available-quantity="122">
                                                        <input class="noSelect quantity plus" type="text" value="0" name="count" data-id="<?=$offer["ID"];?>" data-price="#OFFER_PRICE_<?=$offer["ID"];?>#"/>
                                                        <div style="display: none;" class="totalCost">#OFFER_PRICE_<?=$offer["ID"];?>#</div>
                                                        <div class="spinedit"><div class="minus_prod minus-btn icon-chevron-down"><i class="fa fa-minus" aria-hidden="true"></i></div><div class="plus_prod plus-btn icon-chevron-up"><i class="fa fa-plus" aria-hidden="true"></i></div></div>
                                                    </div>
                                                    <input type="hidden" class="cost" value = "#OFFER_PRICE_<?=$offer["ID"];?>#"/>
                                                    <input type="hidden" class="id" value = "<?=$offer["ID"];?>"/>
                                                </td>
                                            </tr>
                                        <?endforeach;?>
                                        </tbody>
                                    </table>
                                    <?
                                    $buyOfferBlockHTML = ob_get_clean();
                                    itc\CUncachedArea::show('productBuyOfferBlock', array(
                                        'id' => $item["ID"],
                                        'iblock_id' => $item["IBLOCK_ID"],
                                        'offersIds' => $item["offersIds"],
                                        "offerPriceBlockHTML" => $buyOfferBlockHTML
                                    ));
                                    ?>
                                </div>
                                <div class="cursive priceAndControl">
                                    <div class="price">
                                        Итого: <span class="cursive sum">0</span>
                                        <i class="fa fa-rub fa-1"></i>
                                    </div>
                                    <div class="confim_controls">
                                        <a class="cancel">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inner0">
                            <?ob_start(); ?>
                            <div class="image">
                                #DISCOUNT#
                                <a href="<?=$item["DETAIL_PAGE_URL"]?>">
                                    <img class="lazy" data-original="<?=$item["DETAIL_PICTURE"];?>" alt=""/>
                                </a>
                            </div>
                            <div class="price_line">
                                <span>от #PRICE#</span>
                                <i class="fa fa-rub fa-1"></i>
                            </div>
                            <?
                            $buyBlockHTML = ob_get_clean();
                            itc\CUncachedArea::show('productBuyBlock', array(
                                'id' => $item["ID"],
                                'iblock_id' => $item["IBLOCK_ID"],
                                'offersIds' => $item["offersIds"],
                                "buyBlockHTML" => $buyBlockHTML
                            ));
                            ?>
                            <div class="description">
                                <a href="<?=$item["DETAIL_PAGE_URL"]?>">
                                    <span><?=$item["NAME"];?></span>
                                </a>
                            </div>
                            <?if(intval(trim($item["PROPERTIES"]["status"]["VALUE"])) === 1 || intval(trim($item["PROPERTIES"]["status"]["VALUE"])) === 0) : ?>
                            <input class="b_btn" type="submit" value="Добавить товар в корзину" onclick="javascript://" data-role="to-cart" data-zakaz="0">
                            <?else:?>
                            <? $APPLICATION->IncludeComponent(
                                "custom:catalog.product.subscribe",
                                "",
                                Array(
                                    "BUTTON_CLASS" => "b_btn",
                                    "BUTTON_ID" => $item["SUBCRIBE_ID"],
                                    "CACHE_TIME" => "3600",
                                    "CACHE_TYPE" => "N",
                                    "PRODUCT_ID" => $item["SUBCRIBE_ID"]
                                )
                            );?>
                            <?endif;?>
                            <a href="" class="b_btn f_look" data-role="quickView">Быстрый просмотр</a>
                        </div>
                    </div>
                </div>
            </div>
        <?endif;?>
        <?endforeach;?>
    </div>
<?else :?>
    <div class="disableTextSelect">
        <br><h2><span style="color: #ff0000;">Нет товаров, удовлетовряющих заданному фильтру.</span></h2>
        <p><br> <br></p>
    </div>
<?endif;?>
