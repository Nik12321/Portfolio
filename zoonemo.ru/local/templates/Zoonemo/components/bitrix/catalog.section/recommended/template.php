<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use itc\CUncachedArea;

$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');
$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>
<? if (!empty($arResult['ITEMS'])) { ?>
<h4>
    <?="С этим товаром рекомендуем"?>
</h4><br>
<?
}
?>
<div class="catalog-products-viewed bx-<?=$arParams['TEMPLATE_THEME']?>" data-entity="<?=$containerName?>">
    <div class="owl-carousel owl-theme">
        <?
        foreach ($arResult['ITEMS'] as $arItem) {
            $uniqueId = $arItem['ID'].'_'.md5($this->randString().$component->getAction());
            $areaIds[$arItem['ID']] = $this->GetEditAreaId($uniqueId);
            $this->AddEditAction($uniqueId, $arItem['EDIT_LINK'], $elementEdit);
            $this->AddDeleteAction($uniqueId, $arItem['DELETE_LINK'], $elementDelete, $elementDeleteParams);
            ?>
            <? if ($arItem["PRODUCT_MODE"] == "SIMPLE_TP") : ?>
                <div class="card card_mini addToCart">
                    <div class="inner2">
                        <div class="complete">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 18">
                                <path id="shopping-cart-3" class="cls-1" d="M1065.8,10l-3.43,12h-12.59l0.84,2h13.23l3.48-12h1.93l0.74-2h-4.2Zm-4.96,10,1.97-7H1046l2.94,7h11.9Zm-1.34,5a1.5,1.5,0,1,0,1.5,1.5A1.5,1.5,0,0,0,1059.5,25Zm-3.5,1.5a1.5,1.5,0,1,1-1.5-1.5A1.5,1.5,0,0,1,1056,26.5Z" transform="translate(-1046 -10)"></path>
                            </svg>
                            <span>Товар добавлен в корзину</span>
                        </div>
                    </div>
                    <div class="inner0">
                        <form>
                            <?ob_start(); ?>
                            <div class="image">
                               #DISCOUNT#
                                <a rel="salute">
                                    <img src="<?=$arItem["DETAIL_PICTURE"];?>" alt=""/>
                                </a>
                            </div>
                            <?if(intval(trim($arItem["PROPERTIES"]["status"]["VALUE"])) === 1) : ?>
                                <span>В наличии <i class="fa fa-check-circle" aria-hidden="true"></i></span>
                            <?elseif (intval(trim($arItem["PROPERTIES"]["status"]["VALUE"])) === 0) : ?>
                                <span>Доставка до 3-х дней</span>
                            <?else  : ?>
                                <span>Нет в наличии</span>
                            <?endif;?>
                            <p class="description"><?=$arItem["NAME"]?></p>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="s_btn reverse">Подробнее</a>
                            <input
                                    style="display: none"
                                    class="cost"
                                    value = "#PRICE#"
                            />
                            <input
                                    style="display: none"
                                    class="id"
                                    value = "<?=$arItem["ID"];?>"
                            />
                            <div class="price_line"><span>#PRICE#</span><i class="fa fa-rub fa-1"></i></div>
                            <?
                            $buyBlockHTML = ob_get_clean();
                            itc\CUncachedArea::show('productBuyBlock', array(
                                'id' => $arItem["ID"],
                                'iblock_id' => $arItem["IBLOCK_ID"],
                                "buyBlockHTML" => $buyBlockHTML,
                            ));
                            ?>
                            <?if (intval(trim($arItem["PROPERTIES"]["status"]["VALUE"])) === 1 || intval(trim($arItem["PROPERTIES"]["status"]["VALUE"])) === 0) : ?>
                                <div class="viewed_products">
                                    <input class="b_btn w_pzf" type="submit" value = "Добавить товар в корзину"/>
                                </div>
                            <?else:?>
                                <?$APPLICATION->IncludeComponent(
                                    "custom:catalog.product.subscribe",
                                    "",
                                    Array(
                                        "BUTTON_CLASS" => "b_btn w_pzf",
                                        "BUTTON_ID" => $arItem["SUBCRIBE_ID"],
                                        "CACHE_TIME" => "3600",
                                        "CACHE_TYPE" => "N",
                                        "PRODUCT_ID" => $arItem["SUBCRIBE_ID"]
                                    )
                                );?>
                            <?endif;?>
                        </form>
                    </div>
                </div>
            <?else:?>
                <div class="good_item" data-role="product" data-type="offers">
                    <div class="card card_mini addToCartMarket">
                        <div class="inner2">
                            <div class="complete">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 18">
                                    <path id="shopping-cart-3" class="cls-1" d="M1065.8,10l-3.43,12h-12.59l0.84,2h13.23l3.48-12h1.93l0.74-2h-4.2Zm-4.96,10,1.97-7H1046l2.94,7h11.9Zm-1.34,5a1.5,1.5,0,1,0,1.5,1.5A1.5,1.5,0,0,0,1059.5,25Zm-3.5,1.5a1.5,1.5,0,1,1-1.5-1.5A1.5,1.5,0,0,1,1056,26.5Z" transform="translate(-1046 -10)"></path>
                                </svg>
                                <span>Товар добавлен в корзину</span>
                            </div>
                        </div>
                        <div class="inner1">
                            <div class="inner_wrap">
                                <div class="table_cont">
                                    <?ob_start(); ?>
                                    <table>
                                        <tbody>
                                        <?
                                        foreach($arItem["OFFERS"] as $offer) : ?>
                                            <tr>
                                                <td class="cursive offerName"><?=$offer["NAME"];?></td>
                                                <td class="cursive">#OFFER_PRICE_<?=$offer["ID"];?>#&nbsp;<i class="fa fa-rub fa-1"></i></td>
                                                <td class="cursive">
                                                    <div class="spinEdit" data-available-quantity="122">
                                                        <input class="noSelect quantity plus" type="text" value="0" name="count" data-id="<?=$offer["ID"];?>" data-price="#OFFER_PRICE_<?=$offer["ID"];?>#"/>
                                                        <div style="display: none;" class="totalCost">#OFFER_PRICE_<?=$offer["ID"];?>#</div>
                                                        <div class="spinedit"><div class="minus_prod minus-btn icon-chevron-down"><i class="fa fa-minus" aria-hidden="true"></i></div><div class="plus_prod plus-btn icon-chevron-up"><i class="fa fa-plus" aria-hidden="true"></i></div></div>
                                                    </div>
                                                    <input type="hidden" class="cost" value = "#OFFER_PRICE_<?=$offer["ID"];?>#"/>
                                                    <input type="hidden" class="id" value = "<?=$offer["ID"];?>"/>
                                                </td>
                                            </tr>
                                        <?endforeach;?>
                                        </tbody>
                                    </table>
                                    <?
                                    $buyOfferBlockHTML = ob_get_clean();
                                    itc\CUncachedArea::show('productBuyOfferBlock', array(
                                        'id' => $arItem["ID"],
                                        'iblock_id' => $arItem["IBLOCK_ID"],
                                        'offersIds' => $arItem["offersIds"],
                                        "offerPriceBlockHTML" => $buyOfferBlockHTML
                                    ));
                                    ?>
                                </div>
                                <div class="cursive priceAndControl">
                                    <div class="price">
                                        Итого: <span class="cursive sum">0</span>
                                        <i class="fa fa-rub fa-1"></i>
                                    </div>
                                    <div class="confim_controls">
                                        <a class="cancel">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inner0">
                            <?ob_start(); ?>
                            <div class="image">
                                #DISCOUNT#
                                <a rel="salute">
                                    <img src="<?=$arItem['DETAIL_PICTURE'];?>" alt=""/>
                                    <?unset($newPhoto);?>
                                </a>
                            </div>
                            <?if(intval(trim($arItem["PROPERTIES"]["status"]["VALUE"])) === 1 || intval(trim($arItem["PROPERTIES"]["status"]["VALUE"])) === 0) : ?>
                                <span>В наличии <i class="fa fa-check-circle" aria-hidden="true"></i></span>
                            <?else  : ?>
                                <span>Нет в наличии</span>
                            <?endif;?>
                            <p class="description"><?=$arItem["NAME"]?></p>
                            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="s_btn reverse">Подробнее</a>
                            <div class="price_line"><span>От #PRICE#</span><i class="fa fa-rub fa-1"></i></div>
                            <?
                            $buyBlockHTML = ob_get_clean();
                            itc\CUncachedArea::show('productBuyBlock', array(
                                'id' => $arItem["ID"],
                                'iblock_id' => $arItem["IBLOCK_ID"],
                                'offersIds' => $arItem["offersIds"],
                                "buyBlockHTML" => $buyBlockHTML,
                            ));
                            ?>
                            <?if (intval(trim($arItem["PROPERTIES"]["status"]["VALUE"])) === 0 || intval(trim($arItem["PROPERTIES"]["status"]["VALUE"])) === 1) : ?>
                                <div class="viewed_products">
                                    <input
                                            class="b_btn w_pzf"
                                            type="submit"
                                            value = "Добавить товар в корзину"
                                            onclick="javascript://"
                                            data-role="to-cart"
                                            data-zakaz="0"
                                    />
                                </div>
                            <?else :?>
                                <?$APPLICATION->IncludeComponent(
                                    "custom:catalog.product.subscribe",
                                    "",
                                    Array(
                                        "BUTTON_CLASS" => "b_btn w_pzf",
                                        "BUTTON_ID" => $arItem["SUBCRIBE_ID"],
                                        "CACHE_TIME" => "3600",
                                        "CACHE_TYPE" => "N",
                                        "PRODUCT_ID" =>$arItem["SUBCRIBE_ID"]
                                    )
                                );?>
                            <?endif;?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?
        }
        ?>
    </div>
</div>

<!-- component-end -->