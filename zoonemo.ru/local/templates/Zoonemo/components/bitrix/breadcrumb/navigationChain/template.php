<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";
$strReturn = '';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

$strReturn .= ' <div class="container">
                     <div class="row">
                         <div class="col-xs-12">
                               <ul itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumb">';
global $USER;
$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
    if($index > 0) {
        $cutLink = explode("/", $arResult[$index]["LINK"]); //разбиваем путь на SECTION_CODE
        $cnt = count($cutLink); //считаем количество элементов массива
        $numSecNow = $cnt-2; //номер текущего (чпу со "/" на конце, поэтому -2)
        $secNow = $cutLink[$numSecNow]; //нужный нам SECTION_CODE
    }
    if ($secNow>0){
        $dbSection = CIBlockSection::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID" => 13, "ID" => $secNow), false, array('NAME'));
        if ($arSection = $dbSection->GetNext()){
            $title = $arSection['NAME'];
        }
        else {
            $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
        }
    }
    else {
        $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    }
	$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');
    if($arResult[$index]["LINK"] <> "" && $_SERVER["REQUEST_URI"] != $arResult[$index]["LINK"])
    {
		$strReturn .= ' <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                            <a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="item">
                            <span itemprop="name">
                            '.$title.'
                            </span>
                            </a>
                            <meta itemprop="position" content="'.($index + 1).'" />
                         </li>';
	}
	else
	{
		$strReturn .= '
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                      <a style="display: none" href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="item">
                            <span itemprop="name">
                            '.$title.'
                            </span>
                           </a>   
                           <meta itemprop="position" content="'.($index + 1).'" />
                    <span >
                               <span>
                        '.$title.'
                        </span>             
</span>
                    </li>';
	}
}

$strReturn .= '</ul><div style="clear:both"></div></div></div></div>';
return $strReturn;

debug(123123);