<?php
$elements = CIBlockSection::GetList(["LEFT_MARGIN" => "ASC"], ["IBLOCK_ID" => 13, "ACTIVE" => "Y", "<DEPTH_LEVEL" => 4, 'LEFT_MARGIN' => 'ASC'], false, [
    "ID",
    "IBLOCK_ID",
    "DEPTH_LEVEL",
    "IBLOCK_SECTION_ID",
    "PICTURE",
    "NAME",
    "SECTION_PAGE_URL",
    "DETAIL_PICTURE"
]);
$allSections = [];
$isLevelOne = $isLevelTwo = $isLevelThree = false;
while ($element = $elements->GetNext()) {
    if ($element["DETAIL_PICTURE"]) {
        $file = CFile::GetFileArray($element["DETAIL_PICTURE"]);
        $newPhoto = CFile::ResizeImageGet($file["ID"], ["width"=> 35, "height" => 35], BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ($newPhoto["src"])
            ? $element["DETAIL_PICTURE"] = $newPhoto["src"]
            : $element["DETAIL_PICTURE"] = $file["SRC"];
    }
    if ($element["PICTURE"]) {
        $file = CFile::GetFileArray($element["PICTURE"]);
        $newPhoto = CFile::ResizeImageGet($file["ID"], ["width"=> 35, "height" => 35], BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ($newPhoto["src"])
            ? $element["PICTURE"] = $newPhoto["src"]
            : $element["PICTURE"] = $file["SRC"];
    }

    if ($element["DEPTH_LEVEL"] == 1) {
        $isLevelOne = true;
        $isLevelTwo = false;
        $isLevelThree = false;
    } else if ($element["DEPTH_LEVEL"] == 2) {
        $isLevelOne = false;
        $isLevelTwo = true;
        $isLevelThree = false;
    } else {
        $isLevelOne = false;
        $isLevelTwo = false;
        $isLevelThree = true;
    }

    if ($isLevelOne) {
        $allSections[$element["ID"]] = $element;
        $levelOneId = $element["ID"];
    }

    if ($isLevelTwo) {
        $allSections[$levelOneId]["HAS_CHILD"] = true;
        $allSections[$levelOneId]["CHILDREN"][$element["ID"]] = $element;
        $levelTwoId = $element["ID"];
    }

    if ($isLevelThree) {
        $allSections[$levelOneId]["CHILDREN"][$levelTwoId]["HAS_CHILD"] = true;
        $allSections[$levelOneId]["CHILDREN"][$levelTwoId]["CHILDREN"][$element["ID"]] = $element;
    }
}
$arResult = $allSections;