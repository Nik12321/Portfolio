<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
CModule::IncludeModule("iblock");
?>
<ul class="aside_menu m2">
    <ul class="submenu">
        <li class="has_child ico8987">
            <a href="/shop/sale/">
                Акции
            </a>
        </li>
        <?php foreach($arResult as $arItem) :?>
            <?php if (strripos($APPLICATION->GetCurPage(), $arItem["SECTION_PAGE_URL"]) !== false) :?>
                <?php if ($arItem["HAS_CHILD"] && $APPLICATION->GetCurPage() == $arItem["SECTION_PAGE_URL"]) :?>
                    <li data-type="one" data-href="<?=$arItem["SECTION_PAGE_URL"]?>" class="has_child has_child2 active">
                <?php else :?>
                    <li data-type="one" data-href="<?=$arItem["SECTION_PAGE_URL"]?>" class="has_child active">
                <?php endif;?>
                <?php if ($arItem["PICTURE"]) :?>
                    <a href="<?=$arItem["SECTION_PAGE_URL"]?>" style = "background-image: url('<?=$arItem["PICTURE"];?>'); background-repeat: no-repeat; background-position: 12px center;">
                <?php else :?>
                    <a href="<?=$arItem["SECTION_PAGE_URL"]?>">
                <?php endif;?>
            <?php else :?>
                <li data-type="one" data-href="<?=$arItem["SECTION_PAGE_URL"]?>">
                <?php if ($arItem["DETAIL_PICTURE"]) : ?>
                    <a href="<?=$arItem["SECTION_PAGE_URL"]?>" style = "background-image: url('<?=$arItem["DETAIL_PICTURE"];?>'); background-repeat: no-repeat; background-position: 18px center;">
                <?php else : ?>
                    <a href="<?=$arItem["SECTION_PAGE_URL"]?>">
                <?php endif;?>
            <?php endif;?>
            <?=$arItem["NAME"]?>
            </a>
            <ul class="submenu lev2 filtr_sort2">
                <?php foreach($arItem["CHILDREN"] as $arItemLevel2) :?>
                    <li data-type="one" data-href="<?=$arItemLevel2["SECTION_PAGE_URL"]?>" class="<?=$arItemLevel2['PARAMS']['CSS_CLASS']?>">
                        <a href="<?=$arItemLevel2["SECTION_PAGE_URL"]?>" class="">
                            <?=$arItemLevel2["NAME"]?>
                        </a>
                        <ul class="submenu filtr_sort2 lev3">
                            <?php foreach($arItemLevel2["CHILDREN"] as $arItemLevel3) :?>
                                <li data-type="one" data-href="<?=$arItemLevel2["SECTION_PAGE_URL"]?>" class="<?=$arItemLevel2['PARAMS']['CSS_CLASS']?>">
                                    <a href="<?=$arItemLevel3["SECTION_PAGE_URL"]?>" class="">
                                        <?=$arItemLevel3["NAME"]?>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </li>
                <?php endforeach;?>
            </ul>
            </li>
        <?php endforeach;?>
    </ul>
</ul>

