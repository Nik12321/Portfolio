<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="news-list">
    <div class="news_list">
        <?php foreach ($arResult["ITEMS"] as $arItem) : ?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="news_item"><span class="time"><?=$arItem["TIMESTAMP_X"];?></span>
                <?php
                $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), [
                    "CONFIRM"=>GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")
                ]);
                ?>
                <span class="title">Имя: <?=$arItem["PROPERTIES"]["NAME"]["VALUE"];?></span>
                <div class="content">
                    <p><?=$arItem["PROPERTIES"]["REVIEW"]["VALUE"];?></p>
                </div>
            </div>
        <div class="pagination">
            <?=$arResult["NAV_STRING"]?><br />
        </div>
        <?php endforeach;?>
        <?php if(!count($arResult["ITEMS"])) :?>
            <h2 class="page_title">
                Отзывов на данный товар нет. Напишите нам, и будьте первыми!
            </h2>
        <?php endif;?>
    </div>
</div>
