<?php
foreach ($arResult["ITEMS"] as $arItem) {
    if ($arItem["PROPERTIES"]["IMAGE"]["VALUE"]) {
        $file = CFile::GetFileArray($arItem["PROPERTIES"]["IMAGE"]["VALUE"]);
        $newPhoto = CFile::ResizeImageGet($file, ["height" => 800, "width" => 100000], BX_RESIZE_IMAGE_PROPORTIONAL, false, 90);
        ($newPhoto["src"])
            ? $arItem["PROPERTIES"]["IMAGE"]["SRC"] = $newPhoto["src"]
            : $arItem["PROPERTIES"]["IMAGE"]["SRC"] = $file["SRC"];
    }
}