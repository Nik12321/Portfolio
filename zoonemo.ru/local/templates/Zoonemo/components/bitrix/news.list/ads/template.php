<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="news-list">
    <div class="news_list">
        <?php foreach ($arResult["ITEMS"] as $arItem) :?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="news_item">
                <span class="time">
                    <?=$arItem["TIMESTAMP_X"];?> <?=$arItem["PROPERTIES"]["NAME"]["VALUE"];?>
                </span>
                <?php
                $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), [
                    "CONFIRM"=>GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")
                ]);
                ?>
                <span class="title">Имя: <?=$arItem["PROPERTIES"]["NAME"]["VALUE"];?></span>
                <div class="content">
                    <p><span>Описание:</span> <?=$arItem["PROPERTIES"]["ADSTEXT"]["VALUE"];?></p>
                </div>
                <?php if ($arItem["PROPERTIES"]["IMAGE"]["SRC"]) : ?>
                    <a href="<?=$arItem["PROPERTIES"]["IMAGE"]["SRC"];?>">
                        <img src="<?=$arItem["PROPERTIES"]["IMAGE"]["SRC"];?>" alt="">
                    </a>
                <?endif;?>
            </div>
        <?php endforeach;?>
        <?php if(!count($arResult["ITEMS"])) :?>
            <h2 class="page_title">Пока здесь нет объявлений. Напишите нам, и будьте первыми!</h2>
        <?php endif;?>
    </div>
</div>
