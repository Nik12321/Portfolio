<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? if($arResult["FILE"] <> '') : ?>
    <? $file = file_get_contents($arResult["FILE"]);
    $piece = strip_tags($file);
    $numberOfWords = preg_split("/\s|&nbsp;/", $piece);
    $numberOfWords = array_diff($numberOfWords, array(''));
    if (count($numberOfWords) != 1) : ?>
        <?debug("Ошибка, номер телефона некорректного формата. Необходимо ввести одну строку с номером телефона");?>
    <?else :?>
    <a  href="/" class="tel"><?=$numberOfWords[0];?></a>
    <?endif;?>
<? endif;?>