<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? if($arResult["FILE"] <> '') : ?>
    <? $file = file_get_contents($arResult["FILE"]);
    $piece = strip_tags($file);
    $numberOfWords = preg_split("/\s|&nbsp;/", $piece);
    $numberOfWords = array_diff($numberOfWords, array(''));
    if (count($numberOfWords) != 1) : ?>
    <?debug("Ошибка, номер телефона некорректного формата. Необходимо ввести одну строку с номером телефона");?>
    <?else :?>
    <a  href="/" class="tel"><?=$numberOfWords[0];?></a>
    <i class="phone">
        <svg xmlns="http://www.w3.org/2000/svg" width="24.031" height="24" viewBox="0 0 24.031 24">
            <path id="phone-9" class="cls-1" d="M522.647,14.591L520.428,17.7s0.882,0.656.921,0.687c0.994,0.747-1.927,4.8-2.948,4.1l-0.932-.692L515.233,24.9l0.943,0.7c3.314,2.344,10.591-7.8,7.412-10.309C523.536,15.252,522.65,14.594,522.647,14.591ZM509.3,26.481a11.995,11.995,0,1,0,5.2-16.133A12,12,0,0,0,509.3,26.481Zm1.779-.912a10,10,0,1,1,13.46,4.327A9.994,9.994,0,0,1,511.081,25.57Z" transform="translate(-507.969 -9)" />
        </svg>
    </i>
    <?endif;?>
<? endif;?>