<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? if($arResult["FILE"] <> '') : ?>
    <? $file = file_get_contents($arResult["FILE"]);
    $piece = strip_tags($file);
    $numberOfWords = preg_split("/\s|&nbsp;/", $piece);
    $numberOfWords = array_diff($numberOfWords, array(''));
    $numberOfWords = implode ($numberOfWords);
    $number = str_replace("(", "", $numberOfWords);
    $number = str_replace(")", "", $number);
    $number = str_replace("-", "", $number);
    $number = str_replace(" ", "", $number);
    $number = str_replace("+7", "8", $number);
    ?>
    <p class="tel"> <span class="i_wrap"><em class="fa fa-phone"></em></span> <a href="tel:<?=$number;?>"><?=$piece;?> </a> </p>
<? endif;?>