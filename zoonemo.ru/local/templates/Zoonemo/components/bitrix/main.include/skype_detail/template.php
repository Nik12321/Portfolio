<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? if($arResult["FILE"] <> '') : ?>
    <? $file = file_get_contents($arResult["FILE"]);
    $piece = strip_tags($file);
    $numberOfWords = preg_split("/\s|&nbsp;/", $piece);
    $numberOfWords = array_diff($numberOfWords, array(''));
    $numberOfWords = implode ($numberOfWords);
    ?>
    <li class="skype"><em class="fa fa-skype"></em><a href="skype:<?=$numberOfWords;?>"><?=$numberOfWords;?></a></li>
<? endif;?>