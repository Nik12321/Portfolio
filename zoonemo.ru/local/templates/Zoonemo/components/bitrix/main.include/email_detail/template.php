<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? if($arResult["FILE"] <> '') : ?>
    <? $file = file_get_contents($arResult["FILE"]);
    $piece = strip_tags($file);
    $numberOfWords = preg_split("/\s|&nbsp;/", $piece);
    $numberOfWords = array_diff($numberOfWords, array(''));
    $numberOfWords = implode ($numberOfWords);
    ?>
    <li class="mail"><em class="fa fa-envelope-o"></em><a href="mailto:<?=$numberOfWords;?>"><?=$numberOfWords;?></a></li>
<? endif;?>