<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

if (!empty($arResult['ERRORS']['FATAL'])) {
    foreach ($arResult['ERRORS']['FATAL'] as $error) {
        ShowError($error);
    }

    $component = $this->__component;
    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }
} else {
    if (!empty($arResult['ERRORS']['NONFATAL'])) {
        foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
            ShowError($error);
        }
    }
    ?>
    <div class="container-fluid sale-order-detail">
        <div class="sale-order-detail-title-container">
            <h1 class="sale-order-detail-title-element">
                <?= Loc::getMessage('SPOD_LIST_MY_ORDER', [
                    '#ACCOUNT_NUMBER#' => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
                    '#DATE_ORDER_CREATE#' => $arResult["DATE_INSERT_FORMATED"]
                ])?>
            </h1>
        </div>
        <a class="sale-order-detail-back-to-list-link-up" href="<?= htmlspecialcharsbx($arResult["URL_TO_LIST"]) ?>">
            &larr; <?= Loc::getMessage('SPOD_RETURN_LIST_ORDERS') ?>
        </a>
        <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-general">
            <div class="row">
                <div class="col-md-12 cols-sm-12 col-xs-12 sale-order-detail-general-head">
					<span class="sale-order-detail-general-item">
						<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', [
                            "#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
                            "#DATE_ORDER_CREATE#"=> $arResult["DATE_INSERT_FORMATED"]
                        ])?>
                        <?= count($arResult['BASKET']);?>
                        <?php
                        $count = count($arResult['BASKET']) % 10;
                        if ($count == '1') {
                            echo Loc::getMessage('SPOD_TPL_GOOD');
                        } elseif ($count >= '2' && $count <= '4') {
                            echo Loc::getMessage('SPOD_TPL_TWO_GOODS');
                        } else {
                            echo Loc::getMessage('SPOD_TPL_GOODS');
                        }
                        ?>
                        <?=Loc::getMessage('SPOD_TPL_SUMOF')?>
                        <?=$arResult["PRICE_FORMATED"]?>
					</span>
                </div>
            </div>
            <div class="row sale-order-detail-about-order">
                <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-title">
                            <h3 class="sale-order-detail-about-order-title-element">
                                <?= Loc::getMessage('SPOD_LIST_ORDER_INFO') ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-inner-container">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 sale-order-detail-about-order-inner-container-name">
                                    <div class="sale-order-detail-about-order-inner-container-name-title">
                                        <?php
                                        $userName = $arResult["USER_NAME"];
                                        if (strlen($userName) || strlen($arResult['FIO'])) {
                                            echo Loc::getMessage('SPOD_LIST_FIO').':';
                                        } else {
                                            echo Loc::getMessage('SPOD_LOGIN').':';
                                        }
                                        ?>
                                    </div>
                                    <div class="sale-order-detail-about-order-inner-container-name-detail">
                                        <?php
                                        if (strlen($userName)) {
                                            echo htmlspecialcharsbx($userName);
                                        } elseif (strlen($arResult['FIO'])) {
                                            echo htmlspecialcharsbx($arResult['FIO']);
                                        } else {
                                            echo htmlspecialcharsbx($arResult["USER"]['LOGIN']);
                                        }
                                        ?>
                                    </div>
                                    <a class="sale-order-detail-about-order-inner-container-name-read-less">
                                        <?= Loc::getMessage('SPOD_LIST_LESS') ?>
                                    </a>
                                </div>

                                <div class="col-md-4 col-sm-6 sale-order-detail-about-order-inner-container-status">
                                    <div class="sale-order-detail-about-order-inner-container-status-title">
                                        <?= Loc::getMessage('SPOD_LIST_CURRENT_STATUS_DATE', array(
                                            '#DATE_STATUS#' => $arResult["DATE_STATUS_FORMATED"]
                                        )) ?>
                                    </div>
                                    <div class="sale-order-detail-about-order-inner-container-status-detail">
                                        <?php
                                        if ($arResult['CANCELED'] !== 'Y') {
                                            echo htmlspecialcharsbx($arResult["STATUS"]["NAME"]);
                                        } else {
                                            echo Loc::getMessage('SPOD_ORDER_CANCELED');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-<?=($arParams['GUEST_MODE'] !== 'Y') ? 2 : 4?> col-sm-6 sale-order-detail-about-order-inner-container-price">
                                    <div class="sale-order-detail-about-order-inner-container-price-title">
                                        <?= Loc::getMessage('SPOD_ORDER_PRICE')?>:
                                    </div>
                                    <div class="sale-order-detail-about-order-inner-container-price-detail">
                                        <?= $arResult["PRICE_FORMATED"]?>
                                    </div>
                                </div>
                                <?php if ($arParams['GUEST_MODE'] !== 'Y') : ?>
                                    <div class="col-md-2 col-sm-6 sale-order-detail-about-order-inner-container-repeat">
                                        <a href="<?=$arResult["URL_TO_COPY"]?>" class="sale-order-detail-about-order-inner-container-repeat-button">
                                            <?= Loc::getMessage('SPOD_ORDER_REPEAT') ?>
                                        </a>
                                        <?php if ($arResult["CAN_CANCEL"] === "Y") : ?>
                                            <a href="<?=$arResult["URL_TO_CANCEL"]?>" class="sale-order-detail-about-order-inner-container-repeat-cancel">
                                                <?= Loc::getMessage('SPOD_ORDER_CANCEL') ?>
                                            </a>
                                        <?php endif;?>
                                    </div>
                                <?php endif;?>
                            </div>
                            <?php
                            $trackValue = Bitrix\Sale\Internals\OrderPropsValueTable::getList(
                                [
                                    'filter' => ['ORDER_ID' => $arResult["ID"], "ORDER_PROPS_ID" => 13],
                                    "select" => ["VALUE", "NAME"]
                                ]
                            )->Fetch();
                            ?>
                            <?php if ($trackValue["VALUE"]) :?>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h4 class="sale-order-detail-about-order-inner-container-details-title">
                                    </h4>
                                    <ul class="sale-order-detail-about-order-inner-container-details-list">
                                        <li class="sale-order-detail-about-order-inner-container-list-item">
                                            <?=htmlspecialcharsbx($trackValue['NAME']);?>:
                                            <div class="sale-order-detail-about-order-inner-container-list-item-element">
                                                <?=$trackValue["VALUE"];?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h1 class="page_title test" style="padding-top:30px">
        Состав заказа:</h1>
    <?
    $intSectionID =  $APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        ".default",
        array(
            "ACTION_VARIABLE" => "action",
            "ADD_PICT_PROP" => "-",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/personal/basket.php",
            "BROWSER_TITLE" => "-",
            "BY_LINK" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "N",
            "COMPATIBLE_MODE" => "Y",
            "CONVERT_CURRENCY" => "N",
            "CUSTOM_FILTER" => "",
            "DETAIL_URL" => "#SITE_DIR#/shop/products/#SECTION_ID#/#ELEMENT_ID#",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_COMPARE" => "N",
            "DISPLAY_TOP_PAGER" => "Y",
            "ELEMENT_SORT_FIELD" => "property_status",
            "ELEMENT_SORT_ORDER" => "DESC",
            "ENLARGE_PRODUCT" => "STRICT",
            "FILTER_NAME" => "order_filter",
            "HIDE_NOT_AVAILABLE" => "L",
            "HIDE_NOT_AVAILABLE_OFFERS" => "L",
            "IBLOCK_ID" => "13",
            "IBLOCK_TYPE" => "Catalogs",
            "INCLUDE_SUBSECTIONS" => "Y",
            "LABEL_PROP" => array(
            ),
            "LAZY_LOAD" => "N",
            "LINE_ELEMENT_COUNT" => "3",
            "LOAD_ON_SCROLL" => "N",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "OFFERS_LIMIT" => "15",
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER" => "asc",
            "OFFERS_SORT_ORDER2" => "desc",
            "OFFER_ADD_PICT_PROP" => "-",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "customPagination",
            "PAGER_TITLE" => "Товары",
            "PAGE_ELEMENT_COUNT" => "1000",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array(
                0 => "cost",
            ),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
            "PRODUCT_DISPLAY_MODE" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
            "RCM_TYPE" => "personal",
            "SECTION_ID" => "1",
            "SECTION_URL" => "",
            "SEF_MODE" => "N",
            "SEF_RULE" => "",
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "Y",
            "SET_META_KEYWORDS" => "Y",
            "SET_STATUS_404" => "Y",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SHOW_CLOSE_POPUP" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_FROM_SECTION" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "SHOW_SLIDER" => "Y",
            "SLIDER_INTERVAL" => "3000",
            "SLIDER_PROGRESS" => "Y",
            "TEMPLATE_THEME" => "blue",
            "USE_ENHANCED_ECOMMERCE" => "N",
            "USE_MAIN_ELEMENT_SECTION" => "Y",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "COMPONENT_TEMPLATE" => ".default",
            "SECTION_CODE" => "",
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SHOW_ALL_WO_SECTION" => "N",
            "SECTION_ID_VARIABLE" => "SECTION_ID"
        ),
        false
    );

    $javascriptParams = array(
        "url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
        "templateFolder" => CUtil::JSEscape($templateFolder),
        "templateName" => $this->__component->GetTemplateName(),
        "paymentList" => $paymentData
    );
    $javascriptParams = CUtil::PhpToJSObject($javascriptParams);
    ?>
    <script>
        BX.Sale.PersonalOrderComponent.PersonalOrderDetail.init(<?=$javascriptParams?>);
    </script>
    <?
}
?>

