<?php
$productIds = array();
foreach ($arResult['BASKET'] as $basketItem) {
    $productID = $basketItem["PRODUCT_ID"]; // ID предложения
    $offerID = CCatalogSku::GetProductInfo($productID);
    if (is_array($offerID)) {
        $productIds[] = $offerID["ID"];
    }
    else {
        $productIds[] = $basketItem["PRODUCT_ID"];
    }
}

$GLOBALS["order_filter"]["=ID"] = $productIds;