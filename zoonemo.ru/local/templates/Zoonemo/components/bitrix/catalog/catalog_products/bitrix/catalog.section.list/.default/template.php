<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>
<?php if (0 < $arResult['SECTION']['ID']) :?>
    <?php
    $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
    $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
    ?>
    <h1 class="page_title test" id="<? echo $this->GetEditAreaId($arResult['SECTION']['ID']); ?>">
        <?php if ($arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) :?>
            <?=$arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"];?>
        <?php else :?>
            <?=$arResult['SECTION']['NAME']; ?>
        <?php endif;?>
    </h1>
<?php endif;?>
<?php if (0 < $arResult["SECTIONS_COUNT"]) :?>
    <div class="disableTextSelect">
        <div class="top_category_menu">
            <ul class="submenu filtr_sort2 lev3">
                <?php foreach ($arResult['SECTIONS'] as &$arSection) :?>
                    <?php
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                    ?>
                    <li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                        <a href="<?=$arSection['SECTION_PAGE_URL'];?>"><?=$arSection['NAME'];?></a>
                    </li>
                <?php endforeach;?>
                <?php unset($arSection); ?>
            </ul>
        </div>
        <br>
    </div>
    <?php echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');?>
<?php endif;?>
