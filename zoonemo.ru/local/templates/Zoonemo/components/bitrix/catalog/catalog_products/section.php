<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Kombox\Filter\Seo;

$this->setFrameMode(true);
CModule::IncludeModule("highloadblock");

if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '') {
    $arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
}
$arParams['USE_FILTER'] = "Y";
$isFilter = ($arParams['USE_FILTER'] == 'Y');
$arFilter = ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y",];
if (0 < intval($arResult["VARIABLES"]["SECTION_ID"])) {
    $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
} elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"]) {
    $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
}

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")) {
    $arCurSection = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
    $arCurSection = [];
    if (Loader::includeModule("iblock")) {
        $dbRes = CIBlockSection::GetList([], $arFilter, false, ["ID"]);
        if(defined("BX_COMP_MANAGED_CACHE")) {
            global $CACHE_MANAGER;
            $CACHE_MANAGER->StartTagCache("/iblock/catalog");
            if ($arCurSection = $dbRes->Fetch()) {
                $CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
            }
            $CACHE_MANAGER->EndTagCache();
        } else {
            if(!$arCurSection = $dbRes->Fetch()) {
                $arCurSection = [];
            }
        }
    }
    $obCache->EndDataCache($arCurSection);
}
if (!isset($arCurSection)) {
    $arCurSection = [];
}

$GLOBALS["arrFilter"][">PROPERTY_status"] = "-2";
$hlblock = HL\HighloadBlockTable::getById(2)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class_category = $entity->getDataClass();
session_start();
if (empty($_GET["sort"]) && empty($_SESSION["sort"])) {
    $sort = "name";
} else if (empty($_GET["sort"])) {
    $sort = $_SESSION["sort"];
} else {
    $sort = $_GET["sort"];
    $_SESSION["sort"] = $sort;
}

if (empty($_GET["order"]) && empty($_SESSION["order"])) {
    $order = "asc";
} else if (empty($_GET["order"])) {
    $order = $_SESSION["order"];
} else {
    $order = $_GET["order"];
    $_SESSION["order"] = $order;
}

if ($sort == "price") {
    $arParams["ELEMENT_SORT_FIELD"] = CATALOG_PRICE_SCALE_1;
} else if ($sort == "name") {
    $arParams["ELEMENT_SORT_FIELD"] = "name";
}
$arParams["ELEMENT_SORT_ORDER"] = $order;

$sectionData = [];
if ($arResult["VARIABLES"]["SECTION_ID"]) {
    $res = CIBlockSection::GetList(
        false,
        ["ID" => $arResult["VARIABLES"]["SECTION_ID"], "IBLOCK_ID" => $arParams["IBLOCK_ID"]],
        false,
        ["IBLOCK_ID", "ID", "NAME", "UF_UPPER_DESCRIPTION", "DESCRIPTION"]
    );
    if($ar_res = $res->GetNext()) {
        $sectionData = $ar_res;
    }
}
?>
<?php if(!empty($arResult["VARIABLES"]["SECTION_ID"])) : ?>
    <?php
    $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "", [
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
        "TOP_DEPTH" => 1,
        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
        "SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
        "HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
        "ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
    ],
        $component,
        array("HIDE_ICONS" => "Y")
    );
    if ($sectionData["~UF_UPPER_DESCRIPTION"]) {
        echo $sectionData["~UF_UPPER_DESCRIPTION"];
    }
    ?>
    <div id="catalog_ajax_container">
        <div class="<?=($isSidebar ? "col-md-9 col-sm-8" : "col-xs-12")?>">
            <?php
            $APPLICATION->IncludeComponent("kombox:filter", "filterBrand", [
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_TYPE" => "N",	// Тип кеширования
                "CLOSED_OFFERS_PROPERTY_CODE" => [
                    0 => "",
                    1 => "",
                ],
                "CLOSED_PROPERTY_CODE" => [
                    0 => "",
                    1 => "",
                ],
                "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
                "FIELDS" => "",	// Показывать дополнительные поля в фильтре
                "FILTER_NAME" => "arrFilter",	// Имя выходящего массива для фильтрации
                "HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
                "IBLOCK_ID" => "13",	// Инфоблок
                "IBLOCK_TYPE" => "Catalogs",	// Тип инфоблока
                "INCLUDE_JQUERY" => "N",	// Подключить библиотеку jQuery
                "IS_SEF" => "N",	// Включить
                "MESSAGE_ALIGN" => "LEFT",	// Выводить сообщение с количеством найденных элементов
                "MESSAGE_TIME" => "5",	// Через сколько секунд скрывать сообщение (0 - не скрывать)
                "PAGER_PARAMS_NAME" => "arrPager",	// Имя массива с переменными для построения ссылок в постраничной навигации
                "PAGE_URL" => "",	// Путь к разделу (если фильтр располагается на другой странице)
                "PRICE_CODE" => "",	// Тип цены
                "SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
                "SECTION_CODE" => "",	// Символьный код раздела инфоблока
                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],	// ID раздела инфоблока
                "SORT" => "Y",	// Сортировать поля фильтра
                "STORES_ID" => "",	// Выводить товары со складов (по умолчанию - все склады)
                "XML_EXPORT" => "N",	// Включить поддержку Яндекс Островов
                "COMPONENT_TEMPLATE" => ".default"
            ],
                false
            );
            global $arrFilter;
            $filter = $GLOBALS["arrFilter"];
            $rsSection = CIBlockElement::GetList([], $filter, [], false, ['ID', 'NAME']);
            $navPageCount = $rsSection / $arParams["PAGE_ELEMENT_COUNT"];
            if (!is_integer($navPageCount)) {
                $navPageCount = intval($navPageCount) + 1;
            }
            $navResult = new CDBResult();
            $navResult->NavPageCount = $navPageCount;  // Общее количество страниц
            if ($_GET["PAGEN_1"]) {
                $navResult->NavPageNomer = $_GET["PAGEN_1"];
            } else {
                $navResult->NavPageNomer = 1;
            }
            $navResult->NavNum = 1; // Номер пагинатора. Используется для формирования ссылок на страницы
            $navResult->NavPageSize = $arParams["PAGE_ELEMENT_COUNT"];   // Количество записей выводимых на одной странице
            $navResult->NavRecordCount = $rsSection;  // Общее количество записей  PAGE_ELEMENT_COUNT
            ?>
            <div class="sort_block">
                <form class="sort_form">
                    <span class="title">Сортировать по</span>
                    <?php
                    if($sort == "name") {
                        $title = "Названию";
                    } else if ($order == "asc") {
                        $title = "Цене ↑";
                    } else {
                        $title = "Цене ↓";
                    }
                    ?>
                    <select class="sortby" name="sort" onChange="window.location.href=this.value" style="margin: 0px; padding: 0px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0;">
                        <?php if( $title == "Названию") : ?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=name&order=asc'?>" selected="true">Названию</option>
                        <?php else:?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=name&order=asc'?>">Названию</option>
                        <?php endif;?>
                        <?php if( $title == "Цене ↑") : ?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=price&order=asc'?>" selected="true">Цене ↑</option>
                        <?php else:?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=price&order=asc'?>">Цене ↑</option>
                        <?php endif;?>
                        <?php if( $title == "Цене ↓") : ?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=price&order=desc'?>" selected="true">Цене ↓</option>
                        <?php else:?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=price&order=desc'?>">Цене ↓</option>
                        <?php endif;?>
                    </select>
                </form>
            </div>
            <div class="pagination">
                <?php
                if ($navPageCount > 1) {
                    $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', $arParams["PAGER_TEMPLATE"], ['NAV_RESULT' => $navResult]);
                }
                ?>
            </div>
        </div>
        <?php
        $intSectionID = $APPLICATION->IncludeComponent("custom:catalog.section", "", [
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ELEMENT_SORT_FIELD" => "property_STATUS_GROUP",
            "ELEMENT_SORT_ORDER" => "desc",
            "ELEMENT_SORT_FIELD2" =>  $arParams["ELEMENT_SORT_FIELD"],
            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER"],
            "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
            "PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
            "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
            "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
            "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
            "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
            "INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
            "BASKET_URL" => $arParams["BASKET_URL"],
            "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
            "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
            "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
            "FILTER_NAME" => $arParams["FILTER_NAME"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_FILTER" => $arParams["CACHE_FILTER"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "SET_TITLE" => $arParams["SET_TITLE"],
            "MESSAGE_404" => $arParams["~MESSAGE_404"],
            "SET_STATUS_404" => $arParams["SET_STATUS_404"],
            "SHOW_404" => $arParams["SHOW_404"],
            "FILE_404" => $arParams["FILE_404"],
            "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
            "PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
            "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
            "PRICE_CODE" => $arParams["~PRICE_CODE"],
            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
            "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
            "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
            "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
            "PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),
            "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
            "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
            "PAGER_TITLE" => $arParams["PAGER_TITLE"],
            "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
            "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
            "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
            "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
            "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
            "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
            "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
            "LAZY_LOAD" => $arParams["LAZY_LOAD"],
            "MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
            "LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],
            "OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
            "OFFERS_PROPERTY_CODE" => (isset($arParams["LIST_OFFERS_PROPERTY_CODE"]) ? $arParams["LIST_OFFERS_PROPERTY_CODE"] : []),
            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
            "OFFERS_LIMIT" => (isset($arParams["LIST_OFFERS_LIMIT"]) ? $arParams["LIST_OFFERS_LIMIT"] : 0),
            "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
            "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
            "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
            "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
            "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
            'HIDE_NOT_AVAILABLE' => "N",
            'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
            'LABEL_PROP' => $arParams['LABEL_PROP'],
            'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
            'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
            'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
            'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
            'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
            'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
            'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
            'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
            'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
            'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
            'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',
            'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
            'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
            'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
            'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
            'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
            'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
            'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
            'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
            'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
            'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
            'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
            'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
            'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
            'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),
            'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
            'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
            'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),
            'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
            "ADD_SECTIONS_CHAIN" => "N",
            'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
            'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
            'COMPARE_NAME' => $arParams['COMPARE_NAME'],
            'USE_COMPARE_LIST' => 'Y',
            'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
            'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
            'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
        ],
            $component
        );
        ?>
        <div style="clear: both; height: 0;"></div>
        <div class="pagination">
            <?php
            if ($navPageCount > 1) {
                $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', $arParams["PAGER_TEMPLATE"], ['NAV_RESULT' => $navResult]);
            }
            ?>
        </div>
    </div>
    <br><br>
    <?php $text = Kombox\Filter\Seo::GetText($default_text = "");?>
    <?php if ($text) :?>
        <div class="text_block">
            <?=$text?>
        </div>
    <?elseif ($sectionData["~DESCRIPTION"]) :?>
        <div class="text_block">
            <?=$sectionData["~DESCRIPTION"]?>
        </div>
    <?endif;?>
    <?php $GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;?>
<?endif?>