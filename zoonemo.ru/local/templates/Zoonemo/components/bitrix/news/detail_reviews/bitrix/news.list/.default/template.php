<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>

<div class="news_list">
    <?php foreach ($arResult["ITEMS"] as $arItem) : ?>
        <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="news_item"><span class="time"><?=$arItem["DATE_CREATE"];?> <?=$arItem["PROPERTIES"]["AUTHOR"]["VALUE"];?></span>
            <?php
            $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), [
                "CONFIRM"=>GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")
            ]);
            ?>
            <div class="content">
                <p><span>Отзыв:</span> <?=$arItem["PROPERTIES"]["REVIEW"]["VALUE"];?></p>
            </div>
            <?php if ($arItem["PROPERTIES"]["ANSWER"]["VALUE"]) :?>
                <div class="content">
                    <p><span>Ответ:</span> <?=$arItem["PROPERTIES"]["ANSWER"]["VALUE"]["TEXT"];?></p>
                </div>
            <?php endif;?>
        </div>
    <?endforeach;?>
    <?if(!count($arResult["ITEMS"])) : ?>
        <h2 class="page_title">Пока здесь нет отзывов. Напишите свой и мы обязательно опубликуем его!</h2>
    <?endif;?>
</div>