<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="spiski_pzf">
    <p><a href="<?=$arResult["LIST_PAGE_URL"]?>">< Вернуться назад</a></p>
    <br><br>
    <div class="voprosy">
        <p><?=$arResult["TIMESTAMP_X"]?> <span style="font-weight: bold;"><?=$arResult["PROPERTIES"]["NAME"]["VALUE"];?></span></p>
        <p>Тема: <?=$arResult["PROPERTIES"]["THEME"]["VALUE"];?></p>
        <p>Вопрос: <?=$arResult["PREVIEW_TEXT"];?></p>
        <p>Ответ: <?=$arResult["DETAIL_TEXT"];?></p>
    </div>
</div>