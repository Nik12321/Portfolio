<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="spiski_pzf" id="<?=$this->GetEditAreaId($arResult['ID']);?>">
    <?php
    $this->AddEditAction($arResult["ID"], $arResult["EDIT_LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arResult["ID"], $arResult["DELETE_LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_DELETE"), [
        "CONFIRM"=>GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")
    ]);
    ?>
    <p><a href="<?=$arResult["LIST_PAGE_URL"]?>">< Вернуться назад</a></p>
    <br>
    <div class="voprosy">
        <p><?=$arResult["DATE_CREATE"]?> <span style="font-weight: bold;"><?=$arResult["PROPERTIES"]["NAME"]["VALUE"];?></span></p>
        <p>Тема: <?=$arResult["PROPERTIES"]["THEME"]["VALUE"];?></p>
        <p>Вопрос: <?=$arResult["PROPERTIES"]["QUESTION"]["VALUE"];?></p>
        <p>Ответ: <?=$arResult["PROPERTIES"]["ANSWER"]["VALUE"];?></p>
    </div>
</div>