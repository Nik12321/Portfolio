<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="news_list">
    <?php foreach ($arResult["ITEMS"] as $arItem) : ?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="news_item">
                <span class="time"><?=$arItem["DATE_CREATE"];?>
                    <?=$arItem["PROPERTIES"]["NAME"]["VALUE"];?>
                </span>
                <?php
                $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), [
                    "CONFIRM"=>GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")
                ]);
                ?>
                <span class="title">Тема: <?=$arItem["PROPERTIES"]["THEME"]["VALUE"];?></span>
                <div class="content">
                    <p><span>Вопрос:</span> <?=$arItem["PROPERTIES"]["QUESTION"]["VALUE"];?></p>
                </div>
                <a class="s_btn reverse" href="<?=$arItem['DETAIL_PAGE_URL'];?>">Подробнее ></a>
            </div>
    <?php endforeach;?>
    <?php if(!count($arResult["ITEMS"])) :?>
        <h2 class="page_title">Пока здесь нет ответов на вопросы. Задайте свой, и мы обязательно ответим!</h2>
    <?php endif;?>
</div>