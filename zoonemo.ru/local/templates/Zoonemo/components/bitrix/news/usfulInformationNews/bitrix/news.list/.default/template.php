<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
CModule::IncludeModule("iblock");
$filter = ['IBLOCK_CODE' => 'Useful_information', "ACTIVE" => "Y", "ACTIVE_DATE"=>"Y", "SECTION_ID" => $arResult["ITEMS"][0]["IBLOCK_SECTION_ID"]];
$cnt = CIBlockElement::GetList([], $filter, [],false, ['ID', 'NAME']);
$navPageCount = $cnt / 10;
if (!is_integer($navPageCount)) {
    $navPageCount = intval($navPageCount) + 1;
}
$navResult = new CDBResult();
$navResult->NavPageCount = $navPageCount;  // Общее количество страниц
if ($_GET["PAGEN_1"])  {
    $navResult->NavPageNomer = $_GET["PAGEN_1"];
} else {
    $navResult->NavPageNomer = 1;
}
$navResult->NavNum = 1; //Номер пагинатора. Используется для формирования ссылок на страницы
$navResult->NavPageSize = 10; //Количество записей выводимых на одной странице
$navResult->NavRecordCount = $cnt; //Общее количество записей  PAGE_ELEMENT_COUNT
?>
<div class="news_list">
    <?php foreach ($arResult["ITEMS"] as $arItem) :?>
        <div class="news_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <?php
            $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), [
                "CONFIRM"=>GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")
            ]);
            ?>
            <span class="time"><?=$arItem["TIMESTAMP_X"];?></span>
            <span class="title"><?=$arItem["NAME"];?></span>
            <a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="s_btn reverse">Подробнее</a>
        </div>
    <?php endforeach;?>
</div>
<div class="pagination">
    <?php
    if ($navPageCount > 1) {
        $APPLICATION->IncludeComponent("bitrix:system.pagenavigation", "customPagination", [
            "NAV_RESULT" => $navResult,
            "COMPONENT_TEMPLATE" => "customPagination",
            "COMPOSITE_FRAME_MODE" => "A",
            "COMPOSITE_FRAME_TYPE" => "AUTO"
        ],
            false
        );
    }
    ?>
</div>