<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="spiski_pzf">
    <h1 class="page_title"><?=$arResult["NAME"];?></h1>
    <div style="padding-top: 0.2em;">
        <small><?=$arResult["TIMESTAMP_X"]?> </small>
    </div>
    <br>
    <div class="content">
        <?echo($arResult["DETAIL_TEXT"]);?>
    </div>
</div>