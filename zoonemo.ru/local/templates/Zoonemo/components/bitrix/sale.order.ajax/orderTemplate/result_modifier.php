<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */
$currentUser = CUser::GetByID($USER->GetParam('USER_ID'));
$arUser = $currentUser->Fetch();

foreach ($arResult["JS_DATA"]["ORDER_PROP"]['properties'] as $propKey => $propItem) {
    if (($propItem["CODE"] == "fio")) {
        if ($propItem["VALUE"][0] == "<Без имени>") {
            unset($arResult["JS_DATA"]["ORDER_PROP"]['properties'][$propKey]["VALUE"]);
        }
    }
    if (($propItem["CODE"] == "email")) {
       unset($arResult["JS_DATA"]["ORDER_PROP"]['properties'][$propKey]["VALUE"]);
    }
    if (array_key_exists($propItem["CODE"], $arUser)
        && !empty($arUser[$propItem["CODE"]]) ) {
        $arResult["JS_DATA"]["ORDER_PROP"]['properties'][$propKey]["VALUE"][0] = $arUser[$propItem["CODE"]];
    }
}
$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);