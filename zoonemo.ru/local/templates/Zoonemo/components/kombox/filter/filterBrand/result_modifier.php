<?php
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
$allBrands = [];
$brandKey = "";
foreach ($arResult["ITEMS"] as $key => $arItem) {
    if ($arItem["CODE"] == "c") {
        $brandKey = $key;
        foreach ($arItem["VALUES"] as $value) {
            $allBrands[] = $value["VALUE"];
        }
    }
}
if (count($allBrands)) {
    $hlblock = HL\HighloadBlockTable::getById(1)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class_category = $entity->getDataClass();

    $rsData  = $entity_data_class_category::getList([
        'select' => ['UF_FILE', "UF_XML_ID", "UF_SORT"],
        'filter' => ["=UF_NAME" => $allBrands],
        "order" => ["UF_NAME" => "ASC"]
    ]);
    while( $elData = $rsData->fetch()) {
        $arResult["ITEMS"][$brandKey]["VALUES"][$elData["UF_XML_ID"]]["IMAGE"] = $elData["UF_FILE"];
        if ($elData["UF_SORT"]) {
            $arResult["ITEMS"][$brandKey]["VALUES"][$elData["UF_XML_ID"]]["SORT"] = $elData["UF_SORT"];
        }
    }
}
$arResult["ANOTHER_PROP"] = [];
foreach ($arResult["ITEMS"] as $item) {
    if ($item["CODE"] == "c") {
        $sortArray = [];
        foreach ($item["VALUES"] as $key => $value) {
            $sortArray[$key] = $value["SORT"];
        }
        arsort($sortArray);
        $newArray = [];
        foreach ($sortArray as $code => $value) {
            $newArray[$code] = $item["VALUES"][$code];
        }
        $item["VALUES"] = $newArray;
        $arResult["BRANDS"] = $item;
    } else {
        $arResult["ANOTHER_PROP"][] = $item;
    }
}