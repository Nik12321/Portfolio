<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
if(method_exists($this, 'setFrameMode')) {
    $this->setFrameMode(true);
}
?>
<?php if(count($arResult['ELEMENTS']) > 1 && $arResult["ITEMS_COUNT_SHOW"]) :?>
    <?php
    $arParams['MESSAGE_ALIGN'] = isset($arParams['MESSAGE_ALIGN']) ? $arParams['MESSAGE_ALIGN'] : 'LEFT';
    $arParams['MESSAGE_TIME'] = intval($arParams['MESSAGE_TIME']) >= 0 ? intval($arParams['MESSAGE_TIME']) : 5;
    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/functions.php");
    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/choice.php");
    CJSCore::Init(array("ajax", "popup"));
    $APPLICATION->AddHeadScript("/bitrix/js/kombox/filter/jquery.filter.js");
    $APPLICATION->AddHeadScript("/bitrix/js/kombox/filter/ion.rangeSlider.js");
    $APPLICATION->AddHeadScript("/bitrix/js/kombox/filter/jquery.cookie.js");
    ?>
    <div class="kombox-filter" id="kombox-filter">
        <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get"<?if($arResult['IS_SEF']):?> data-sef="yes"<?endif;?>>
            <?php foreach($arResult["HIDDEN"] as $arItem):?>
                <input
                        type="hidden"
                        name="<?=$arItem["CONTROL_NAME"]?>"
                        id="<?=$arItem["CONTROL_ID"]?>"
                        value="<?=$arItem["HTML_VALUE"]?>"
                />
            <?php endforeach;?>
            <?php
            $showsProp = false;
            $createBlock = true;
            ?>
            <?php if ($arResult["ANOTHER_PROP"]) :?>
                <?php foreach($arResult["ANOTHER_PROP"] as $arItem) :?>
                    <?php
                    $showProperty = false;
                    if($arItem["SETTINGS"]["VIEW"] == "SLIDER") {
                        if(
                            isset($arItem["VALUES"]["MIN"]["VALUE"])
                            && isset($arItem["VALUES"]["MAX"]["VALUE"])
                            && $arItem["VALUES"]["MAX"]["VALUE"] > $arItem["VALUES"]["MIN"]["VALUE"]
                        ) {
                            $showProperty = true;
                        }
                    } elseif (!empty($arItem["VALUES"]) && !isset($arItem["PRICE"])) {
                        $showProperty = true;
                    }
                    ?>
                    <?php if($showProperty) :?>
                        <?php $showsProp = true;?>
                        <?php if ($createBlock) :?>
                            <div id="anotherProperty" class="container-fluid">
                            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                            <?php $createBlock=false;?>
                        <?php endif;?>
                        <div class="lvl1<?php if($arItem["CLOSED"]):?> kombox-closed<?php endif;?>" data-id="<?=$arItem["CODE_ALT"].'-'.$arItem["ID"]?>">
                            <div class=" col-md-12 kombox-filter-property-head kombox-middle">
                                <i class="kombox-filter-property-i"></i>
                                <span class="kombox-filter-property-name"><?=$arItem["NAME"]?></span>
                                <?php if(strlen($arItem['HINT'])) :?>
                                    <span class="kombox-filter-property-hint"></span>
                                    <div class="kombox-filter-property-hint-text"><?=$arItem['HINT']?></div>
                                <?php endif;?>
                            </div>
                            <div class=" col-md-12 kombox-num kombox-filter-property-body center-kombox" style="display: flex" data-name="<?=$arItem["CODE_ALT"]?>">
                                <?php
                                $minValue = !empty($arItem["VALUES"]["MIN"]["HTML_VALUE"])
                                    ? $arItem["VALUES"]["MIN"]["HTML_VALUE"]
                                    : $arItem["VALUES"]["MIN"]["VALUE"];
                                $maxValue = !empty($arItem["VALUES"]["MAX"]["HTML_VALUE"])
                                    ? $arItem["VALUES"]["MAX"]["HTML_VALUE"]
                                    : $arItem["VALUES"]["MAX"]["VALUE"];
                                ?>
                                <p style="padding: 5px"><?=GetMessage("KOMBOX_CMP_FILTER_FROM")?></p>
                                <input
                                        class="kombox-input kombox-num-from"
                                        type="text"
                                        name="<?=$arItem["CODE_ALT"]?>_from"
                                        id="<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                        value="<?=$arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                        size="5"
                                />
                                <p style="padding: 5px"><?=GetMessage("KOMBOX_CMP_FILTER_TO")?></p>
                                <input
                                        class="kombox-input kombox-num-to"
                                        type="text"
                                        name="<?=$arItem["CODE_ALT"]?>_to"
                                        id="<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                        value="<?=$arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                        size="5"
                                />
                            </div>
                        </div>
                    <?php endif;?>
                <?php endforeach;?>
                <?php if ($showsProp) :?>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" id="filterButtoms">
                    <div class="filterGroupButtons">
                    <input type="submit" class="b_btn" id="set_filter" value="<?=GetMessage("KOMBOX_CMP_FILTER_SET_FILTER")?>" />
                    <?php if($arResult['SET_FILTER']):?>
                        <a href="<?=$arResult["DELETE_URL"]?>" class="b_btn"><?="Сбросить фильтр"?></a>
                    <?php endif;?>
                    </div>
                </div>
                <?php else :?>
                <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12" id="filterButtoms">
                    <div class="filterGroupButtons">
                        <input type="submit" id="set_filter" value="<?=GetMessage("KOMBOX_CMP_FILTER_SET_FILTER")?>" style="display: none"/>
                    </div>
                </div>
                <?php endif;?>
                <?php if (!$createBlock) :?>
                    </div>
                <?php endif;?>
            <?php else :?>
                <input type="submit" id="set_filter" value="<?=GetMessage("KOMBOX_CMP_FILTER_SET_FILTER")?>" style="display: none"/>
             <?php endif;?>
            <div class="brend__block">
                <p>Бренды:</p>
                <div class="brends">
                    <div class="lvl1<?php if($arResult["BRANDS"]["CLOSED"]):?> kombox-closed<?endif;?>" data-id="<?= $arResult["BRANDS"]["CODE_ALT"].'-'.$arResult["BRANDS"]["ID"]?>">
                        <div class="kombox-combo kombox-filter-property-body" data-name="<?=$arResult["BRANDS"]["CODE_ALT"]?>"<?php if($arResult["BRANDS"]["CLOSED"]):?> style="display:none;"<?php endif;?>>
                            <?foreach($arResult["BRANDS"]["VALUES"] as $val => $ar):?>
                                <?php komboxOtherValues($arResult["BRANDS"], 'start');?>
                                <div class="br">
                                    <?php
                                    $IMG = CFile::GetFileArray($ar["IMAGE"]);
                                    $IMG = $IMG["ID"];
                                    $img = CFile::ResizeImageGet(
                                        $IMG,
                                        ["width"=> 300, "height" => 51],
                                        BX_RESIZE_IMAGE_PROPORTIONAL,
                                        true
                                    );
                                    if ($img["src"]) {
                                        $IMG = $img["src"];
                                    } else {
                                        $IMG = $IMG["SRC"];
                                    }
                                    ?>
                                    <div class="lvl2<?=$ar["DISABLED"] ? ' kombox-disabled' : ''?><?=$ar["CHECKED"] ? ' kombox-checked' : ''?>">
                                        <input
                                                style="display: none"
                                                type="checkbox"
                                                value="<?=$ar["HTML_VALUE_ALT"]?>"
                                                name="<?=$arResult["BRANDS"]["CODE_ALT"]?>"
                                                id="<?=$ar["CONTROL_ID"]?>"
                                            <?=$ar["CHECKED"] ? 'checked="checked"' : ''?>
                                        />
                                        <a href="#">
                                            <img src="<?=$IMG;?>" alt="" title="<?=$ar["VALUE"];?>">
                                        </a>
                                    </div>
                                </div>
                            <?php endforeach;?>
                            <?php komboxOtherValues($arResult["BRANDS"]);?>
                        </div>
                    </div>
                </div>
                <a href="#" class="more s_btn">Еще</a>
                <a href="#" class="svernut s_btn">Свернуть</a>
            </div>
            <div class="modef" id="modef" style="display:none">
                <div class="modef-wrap">
                    <?=GetMessage("KOMBOX_CMP_FILTER_FILTER_COUNT", ["#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>']);?>
                    <a href="<?=$arResult["FILTER_URL"]?>"><?=GetMessage("KOMBOX_CMP_FILTER_FILTER_SHOW")?></a>
                    <span class="ecke"></span>
                </div>
            </div>
        </form>
        <div class="kombox-loading"></div>
    </div>
    <script>
        $(function(){
            komboxFilterJsInit();
            $('#kombox-filter').komboxSmartFilter({
                ajaxURL: '<?=CUtil::JSEscape($arResult["FORM_ACTION"])?>',
                urlDelete: '<?=CUtil::JSEscape($arResult["DELETE_URL"])?>',
                align: '<?=$arParams['MESSAGE_ALIGN']?>',
                modeftimeout: <?=$arParams['MESSAGE_TIME']?>
            });
        });
    </script>
    <script>
        var height = $("#anotherProperty").height();
        console.log($("#filterButtoms").height(height));
        $('.lvl2').on('click', function(){
            $("body").find(".kombox-checked").removeClass("kombox-checked");
            $(".brend__block").find('input[name*="<?echo $arResult["BRANDS"]["CODE_ALT"]?>"]').each(function() {
                $(this).prop("checked", false);
            });
            $(this).addClass("kombox-checked");
            $(this).find("input").prop('checked', true);
            $("#set_filter").click();
        });
    </script>
<?endif;?>