<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
use Bitrix\Main\Page\Asset;
?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="/" class="logo">
                    <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/logo_extend.png" alt="logo">
                </a>
                <a href="/" class="logo mobile-display">
                    <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/logo.png" alt="logo">
                </a>
            </div>
            <!--            Контактные данные для мобильной версии-->
            <div class="mobile-display footer-conts col-xs-12">
                <div class="row">
                    <div class="col-xs-6">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "phoneNumberDetail",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/phoneNumberMobile1.php",
                                "COMPONENT_TEMPLATE" => "phoneNumberDetail"
                            ),
                            false
                        );?>
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "emailDetail", Array(
                            "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",	// Шаблон области по умолчанию
                            "PATH" => "/include/emailDetail.php",	// Путь к файлу области
                            "COMPONENT_TEMPLATE" => ".default"
                        ),
                            false
                        );?>
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "skypeDetail", Array(
                            "AREA_FILE_SHOW" => "file",	// Показывать включаемую область
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",	// Шаблон области по умолчанию
                            "PATH" => "/include/skypeDetail.php",	// Путь к файлу области
                            "COMPONENT_TEMPLATE" => ".default"
                        ),
                            false
                        );?>
                    </div>
                    <div class="col-xs-6">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "phoneNumberDetail",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/phoneNumberMobile2.php",
                                "COMPONENT_TEMPLATE" => "phoneNumberDetail"
                            ),
                            false
                        );?>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "detailsMainShedule",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "standard.php",
                                "PATH" => "/include/sheduleMobile.php",
                                "COMPONENT_TEMPLATE" => "detailsMainShedule"
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>
            <!--            Меню футера-->
            <div class="col-md-2 col-sm-3 col-xs-4">
                <ul>
                    <h4>Покупателям</h4>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "subFooterMenu",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "Footer_menu_left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "FooterMenuSections",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "subFooterMenu"
                        ),
                        false
                    );?>
                </ul>
            </div>
            <div class="col-md-2 col-md-offset-1 col-sm-3 col-xs-4">
                <ul>
                    <h4>О компании</h4>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "subFooterMenu",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "FooterMenuCompanyInfo",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "subFooterMenu"
                        ),
                        false
                    );?>
                </ul>
            </div>
            <div class="col-md-2 col-xs-4 col-sm-3">
                <ul>
                    <h4>Каталог товаров</h4>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "subFooterMenu",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "FooterMenuShop",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "subFooterMenu"
                        ),
                        false
                    );?>
                </ul>
            </div>
            <!--            Контакты-->
            <div class="col-md-2 col-xs-12">
                <div class="social">
                    <a href="https://ok.ru/group/54293196374271" rel="nofollow" class="fb" target="_blank"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                    <a href="https://www.instagram.com/zoosalonnemo/" class="gplus" rel="nofollow" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="https://vk.com/zoonemoru" class="vk" rel="nofollow" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="hidden"></div>
<div class="wind_bg"></div>
<!--Всплывающий диалог регистрации-->
<div class="wind_register wind_sputnik">
    <a href="#" class="close forma-close">
        <svg id="SvgjsSvg1072" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25">
            <defs id="SvgjsDefs1073"></defs>
            <path id="SvgjsPath1074" d="M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z " fill="#7e7e7e" fill-opacity="1" transform="matrix(1,0,0,1,-320,-200)"></path>
        </svg>
    </a>
    <div class="page_title">Регистрация</div>
    <div class="errors"> </div>
    <? $APPLICATION->IncludeComponent(
        "bitrix:main.register",
        "registerForm",
        array(
            "AUTH" => "Y",
            "REQUIRED_FIELDS" => array(
                0 => "EMAIL",
            ),
            "SET_TITLE" => "N",
            "SHOW_FIELDS" => array(
                0 => "EMAIL",
            ),
            "SUCCESS_PAGE" => "/account/",
            "USER_PROPERTY" => array(
            ),
            "USER_PROPERTY_NAME" => "",
            "USE_BACKURL" => "Y",
            "COMPONENT_TEMPLATE" => "registerForm"
        ),
        false
    );?>
</div>
<!--Вслывающий диалог авторизации-->
<div class="wind_enter wind_sputnik">
    <a href="#" class="close forma-close">
        <svg id="SvgjsSvg1072" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25">
            <defs id="SvgjsDefs1073"></defs>
            <path id="SvgjsPath1074" d="M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z " fill="#7e7e7e" fill-opacity="1" transform="matrix(1,0,0,1,-320,-200)"></path>
        </svg>
    </a>
    <div class="page_title">Войти в личный кабинет</div>
    <div class="errors"> </div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:system.auth.form",
        "authorizationForm",
        array(
            "FORGOT_PASSWORD_URL" => "forgotPassword",
            "PROFILE_URL" => "/profile/",
            "REGISTER_URL" => "",
            "SHOW_ERRORS" => "Y",
            "COMPONENT_TEMPLATE" => "authorizationForm"
        ),
        false
    );?>
</div>
<!--Всплывающий диалог заказа звонка-->
<div class="wind_zakaz_1cl wind_sputnik">
    <a href="#" class="close forma-close">
        <svg id="SvgjsSvg1072" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25">
            <defs id="SvgjsDefs1073"></defs>
            <path id="SvgjsPath1074" d="M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z " fill="#7e7e7e" fill-opacity="1" transform="matrix(1,0,0,1,-320,-200)"></path>
        </svg>
    </a>
    <div class="page_title">Заказать</div>
    <div class="form-info_pzf">Оставьте Ваши контакты и наш менеджер свяжется с Вами в рабочее время с 10-00 до 20-00 с понедельника по пятницу.</div>
    <div class="form_res1">
        <form class="form_1clk" method="post">
            <input name="name" type="text" placeholder="Имя" />
            <input name="phone" class="phone" type="text" placeholder="Телефон*" />
            <input type="hidden" name="p_href" class="p_href">
            <input type="hidden" name="p_name" class="p_name">
            <div class="err"></div>
            <button class="s_btn type2">Отправить</button>
        </form>
    </div>
</div>

<div class="wind_zakaz2 wind_sputnik">
    <a href="#" class="close forma-close">
        <svg id="SvgjsSvg1072" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25">
            <defs id="SvgjsDefs1073"></defs>
            <path id="SvgjsPath1074" d="M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z " fill="#7e7e7e" fill-opacity="1" transform="matrix(1,0,0,1,-320,-200)"></path>
        </svg>
    </a>
    <div class="page_title">Обратный звонок</div>
    <div class="form-info_pzf">Оставьте Ваши контакты и наш менеджер свяжется с Вами в рабочее время с 10-00 до 20-00 с понедельника по пятницу.</div>
    <div class="form_res1">
        <form class="form_1clk2" method="post">
            <input name="name" type="text" placeholder="Имя" />
            <input name="phone" class="phone" type="text" placeholder="Телефон*" />
            <div class="err"></div>
            <button class="s_btn type2">Отправить</button>
        </form>
    </div>
</div>
<div class="wind_zakaz wind_sputnik">
    <a href="#" class="close forma-close">
        <svg id="SvgjsSvg1072" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25">
            <defs id="SvgjsDefs1073"></defs>
            <path id="SvgjsPath1074" d="M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z " fill="#7e7e7e" fill-opacity="1" transform="matrix(1,0,0,1,-320,-200)"></path>
        </svg>
    </a>
    <div class="page_title">Заказать</div>
    <form action="" method="post" onsubmit="Box.submit(this, '');return false;">
        <input name="imya" type="text" placeholder="Имя" />
        <input name="phone" type="text" placeholder="Телефон" />
        <div class="err"></div>
        <button class="s_btn type2">Отправить</button>
    </form>
</div>
<meta name="cmsmagazine" content="8b8f98ba6e95d71cf42e9faa2011c4a5" />
<a href="javascript://" class="fix-form_pzf oneclick_btn2 b_btn mobile-none" style="background-image: url('<?=DEFAULT_TEMPLATE_PATH?>/img/fix-form_pzf.jpg');"></a>
<!--Вслывающий диалог сообщения о поступлении-->
<div style="display: none;">
    <div class="box-modal" style="background:#fff;width:300px;padding:20px;" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <div class="postupl_success">
            <h2>Сообщить о поступлении</h2><br>
            <form class="postuplen_form">
                <input type="text" placeholder="Имя" name="name" />
                <input type="hidden" id="postupl_good_id" placeholder="Имя" name="good_id" />
                <input type="text" placeholder="Email*" name="email" />
                <input type="submit" class="postupleniye_submit" value="Отправить" />
                <div class="error"></div>
            </form>
        </div>
    </div>
</div>
<!--Вслывающий диалог сообщения о поступлении-->
<meta name="cmsmagazine" content="8b8f98ba6e95d71cf42e9faa2011c4a5">
<a href="javascript://" class="fix-form_pzf oneclick_btn2 b_btn mobile-none"></a>
<div style="display: none;">
    <div class="box-modal" style="background:#fff;width:300px;padding:20px;" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <div class="postupl_success">
            <h2>Сообщить о поступлении</h2><br>
            <form class="postuplen_form">
                <input type="text" placeholder="Имя" name="name">
                <input type="hidden" id="postupl_good_id" placeholder="Имя" name="good_id">
                <input type="text" placeholder="Email*" name="email">
                <input type="submit" class="postupleniye_submit" value="Отправить">
                <div class="error"></div>
            </form>
        </div>
    </div>
</div>
<?php
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/html5shiv/es5-shim.min.js');
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/html5shiv/html5shiv.min.js');
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/html5shiv/html5shiv-printshiv.min.js');
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/jquery3-1-1.min.js');
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/jquery.maskedinput.min.js');
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/notify.min.js');
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/owlCaorusel2-2-0.js');
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/scripts.js');
Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH. '/js/openapi.js');
?>
</body>
</html>