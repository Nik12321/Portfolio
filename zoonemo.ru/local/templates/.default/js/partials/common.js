
window.log = function(param) {
    console.log(param);
};



$(document).ready(function() {
 if ($(window).width() > 1024) {
 	$('.has_child').addClass('open');
 }	
	$('#menu').slicknav({
		prependTo: '.menu_bar',
		label: 'Меню'
	});
	//слайдер на главной
	$('#slider').owlCarousel({
		items:1,
	    loop:true,
	    dots: true,
	});
	//история покупок
	$('#cart_carousel').owlCarousel({
	    loop: false,
	    margin: 10,
	    nav: true,
	    dots: false,
	    items: 3,
	    navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
	    responsive : {
		    // breakpoint from 0 up
		    0 : {
		    	items:1,
		    },
		    // breakpoint from 768 up
		    768 : {
		    	items: 2,
		    },
		    1200 : {
		    	items: 3,
		    }
		}
	})
	// $('.f_look').lightcase({
	// 	maxWidth: 1024,
	// 	maxHeight: 900,
	// 	href: $(this).parents('.card').children('.big_item'),
	// 	type: 'inline'
	// });	
	$('.f_look').click(function() {
		$(this).parents('.good_item').find('.big_item').arcticmodal({
		    overlay: {
				tpl: '<div class="lol"></div>',
				css: {
					background: 'rgb(0, 0, 0)',
				    opacity: 0.6,
				    width: '100%',
				    height: '100%',
				    position: 'absolute',
				    top: 0
				}
			}			
		});
		return false;
	});
	$('.brows_models').click(function() {
		$('.models_list').arcticmodal();
		return false;
	});

	$('.models_list li').click(function() {
		var val = $(this).attr('class');
		$('input[name="model"]').val(val);
		$.arcticmodal('close');
	});
	$('.close').click(function() {
		$.arcticmodal('close');
		return false;
	});
	$('.complite_order').lightcase({
		maxWidth: 700,
		maxHeight: 250,
		href: '.popup_item',
		type: 'inline'
	});

	$('.main_img a').lightcase();
	$('.image .main_img img').after('<i class="lupa"><svg id="SvgjsSvg1075" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" width="30" height="30" viewBox="0 0 30 30"><title>magnifier-7</title><desc>Created with Avocode.</desc><defs id="SvgjsDefs1076"></defs><path id="SvgjsPath1077" d="M1000.25 1608C1005.07 1608 1009 1604.07 1009 1599.25C1009 1594.43 1005.07 1590.5 1000.25 1590.5C995.426 1590.5 991.5 1594.43 991.5 1599.25C991.5 1604.07 995.426 1608 1000.25 1608ZM1015.46 1618L1006.23 1608.77C1004.5 1609.86 1002.45 1610.5 1000.25 1610.5C994.037 1610.5 989 1605.46 989 1599.25C989 1593.04 994.037 1588 1000.25 1588C1006.46 1588 1011.5 1593.04 1011.5 1599.25C1011.5 1601.45 1010.86 1603.5 1009.77 1605.23L1019 1614.46ZM1005.25 1598H1001.5V1594.25H999V1598H995.25V1600.5H999V1604.25H1001.5V1600.5H1005.25Z " fill="#ffffff" fill-opacity="1" transform="matrix(1,0,0,1,-989,-1588)"></path></svg></i>');

	$('.main_img img').attr('src', $('.sub_img a:first-child').attr('href'));
    $('.main_img a').attr('href', $('.sub_img a:first-child').attr('href'));
	$('.sub_img a').click(function() {
        way = $(this).children().attr("src");
    	$('.main_img img').attr('src', way);
    	$('.main_img a').attr('href', way);
    	return false;
    });
	$('select').styler();
	$('.b_btn, .s_btn').hover(function() {
		$(this).toggleClass('reverse');
	});
	
	$('.cancel').click(function() {
		$(this).parents('.card').removeClass('f_step');
		return false;
	});	
	$('.f_step').click(function() {
		$(this).parents('.card').addClass('f_step');
		return false;
	});
	$('.s_step').click(function() {
		$(this).parents('.card').addClass('s_step');
		return false;
	});
	$('.filtr_sort li').click(function() {
		//$('.filtr_sort li [name="filtr_type"]').val('');
		$(this).toggleClass('active');
		//$(this).addClass('active');
		val = $(this).children('a').attr('data-type');
		x = $(this).children('[name="filtr_type"]').val().length;
		if (x > 0) {
			$(this).children('[name="filtr_type"]').val('');
			
		} else {
			$(this).children('[name="filtr_type"]').val(val);
			
		}
		
		return false;
	});
	$('.has_child').click(function() {
		$(this).toggleClass('open');
		return false;
	});
	$('.collapse.type2').click(function() {
		$(this).toggleClass('open');
		$(this).parents('.to_road').siblings('.road').toggle(400);		
		return false;
	});
	$('.collapse').click(function() {
		$(this).toggleClass('open');
		$('#collapse').toggleClass('collapsed');
		return false;
	});

	
	
	$('.input-quantity').spinedit({
	    minimum: 0,
	    maximum: 100,
	    step: 1,
	    value: 0,
	    numberOfDecimals: 0
	});
    
    
	$('.tabs_menu a').click(function() {
		url = $(this).attr('href');
		$('#tabs div').removeClass('open');
		$('.tabs_menu a').removeClass('open');
		$(this).addClass('open');
		$('.' + url).addClass('open');
		return false;

	});
	$('.tabs_menu label').click(function() {
		url = $(this).attr('for');
		$('#tabs div').removeClass('open');
		$('.tabs_menu label').removeClass('open');		
		$(this).addClass('open');
		$('.' + url).addClass('open');	

	});
	$('.tabs_menu li:first-child a').addClass('open');
	$('#tabs div:first-child').addClass('open');

// $('.slider-for').slick({
//   slidesToShow: 1,
//   slidesToScroll: 1,
//   arrows: false,
//   fade: true,
//   asNavFor: '.slider-nav'
// });
// $('.slider-nav').slick({
//   slidesToShow: 3,
//   slidesToScroll: 1,
//   asNavFor: '.slider-for',
//   dots: true,
//   centerMode: true,
//   focusOnSelect: true
// });

});




$(window).resize(function() {
 if ($(window).width() < 768) {
 	$('.has_child').removeClass('open');
 }
});