// BX.ready(function () {
//     //Скрипт маски для номера телефона по всем разделам сайта
//     var phoneInputNames = [
//         "previewGetCallInput",
//         "getCallInput"
//     ];
//     for (let i in phoneInputNames) {
//         var phoneInput = BX(phoneInputNames[i]);
//         if (phoneInput) {
//             var mask = new BX.MaskedInput({mask: '', input: phoneInput, placeholder: '_'});
//             mask.setValue('');
//             BX.bind(phoneInput, 'keyup', function (event) {
//                 let value = event.target.value;
//                 if (value[4] === "7" && value[0] == "+") {
//                     event.target.blur();
//                     mask.setMask('+7 (999) 999-99-99');
//                     mask.setValue('');
//                     event.target.focus();
//                 }
//                 if (value[4] === "8" && value[0] == "+") {
//                     event.target.blur();
//                     mask.setMask('8 (999) 999-99-99');
//                     mask.setValue('');
//                     event.target.focus();
//                 }
//                 if (value[3] === "7" && value[0] == "8") {
//                     event.target.blur();
//                     mask.setMask('+7 (999) 999-99-99');
//                     mask.setValue('');
//                     event.target.focus();
//                 }
//                 if (value[3] === "8" && value[0] == "8") {
//                     event.target.blur();
//                     mask.setMask('8 (999) 999-99-99');
//                     mask.setValue('');
//                     event.target.focus();
//                 }
//             });
//
//             BX.bind(phoneInput, 'focus', function () {
//                 if (!mask.getMask()) {
//                     mask.setMask('+7 (999) 999-99-99');
//                     mask.setValue('');
//                 }
//             });
//
//             BX.bind(phoneInput, 'blur', function () {
//                 if (!mask.checkValue()) {
//                     mask.setMask('');
//                     mask.setValue('');
//                 }
//             });
//         }
//     }
// });

jQuery(document).ready(function () {
    $(function () {
        $(".lazy").lazyload();
    });
    $("body").on("submit", ".regform", function (e) {
        data1 = $(".regform").serializeArray();
        $(".errors").empty();
        $("input").removeClass('error');
        $("body").append("<div class='modal'></div>");
        $.ajax({
            type: 'POST',
            url: '/ajax/register.php',
            data: data1,
            success: function (data) {
                $(".modal").remove();
                data2 = JSON.parse(data);
                if (data2['SUCCESS'] == "Y") {
                    $(".bx-auth-reg").replaceWith("На указанную почту отправлено письмо с подтверждением регистрации.");

                    function handler() {
                        location.reload();
                    }

                    setTimeout(() => handler(), 2000);
                } else {
                    if (data2['loginLengthError'] == 1) {
                        $(".errors").append("<p class='timeError' style='color:red'>Длина логина меньше 6 символов</p>");
                        $("input[name='REGISTER[LOGIN]']").addClass('error');
                    }
                    if (data2['loginFormatError'] == 1) {
                        $(".errors").append("<p class='timeError' style='color:red'>Логин содержит недопустимые символы</p>");
                        $("input[name='REGISTER[LOGIN]']").addClass('error');
                    }
                    if (data2['passwordLengthError'] == 1) {
                        $(".errors").append("<p class='timeError' style='color:red'>Длина пароля меньше 6 символов</p>");
                        $("input[name='password']").addClass('error');
                    }
                    if (data2['confirmError'] == 1) {
                        $(".errors").append("<p class='timeError' style='color:red'>Пароли в полях разные</p>");
                        $("input[name='REGISTER[CONFIRM_PASSWORD]']").addClass('error');
                    }
                    if (data2['emailError'] == 1) {
                        $(".errors").append("<p class='timeError' style='color:red'>Неверный формат почты</p>");
                        $("input[name='REGISTER[EMAIL]']").addClass('error');
                    }
                    if (data2['alreadyExist'] != 0)
                        $(".errors").append("<p class='timeError' style='color:red'>" + data2['alreadyExist'] + "</p>");
                    if (data2['captchaError'] == "R") {
                        $("input[name='captcha_word']").addClass('error');
                        $(".errors").append("<p class='timeError' style='color:red'>Введите текст с картинки</p>");
                    } else if (data2['captchaError'] == "N") {
                        $("input[name='captcha_word").addClass('error');
                        $(".errors").append("<p class='timeError' style='color:red'>Неверный ввод текста с картинки</p>");
                    }
                    $('#captcha_image_register').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + data2["captcha_code"]);
                    $('#captcha_sid_register').val(data2["captcha_code"]);
                    return false;
                }
            }
        });
        return false;
    });


    $("body").on("submit", ".system_auth_form", function (e) {
        data1 = $(".system_auth_form").serializeArray();
        $(".errors").empty();
        $("input").removeClass('error');
        $("body").append("<div class='modal'></div>");
        $.ajax({
            type: 'POST',
            url: '/ajax/auth.php',
            data: data1,
            success: function (data) {
                $(".modal").remove();
                data2 = JSON.parse(data);
                if (data2['SUCCESS'] == "Y")
                    location.reload();
                else {
                    if (data2['loginLengthError'] || data2['passwordLengthError']) {
                        if (data2['loginLengthError'] == 1) {
                            $("input[name='USER_LOGIN").addClass('error');
                        }
                        if (data2['passwordLengthError'] == 1) {
                            $("input[name='USER_PASSWORD").addClass('error');
                        }
                    } else {
                        if (data2['alreadyExist'] != 0)
                            $(".errors").append("<p class='timeError' style='color:red'>" + data2['alreadyExist'] + "</p>");
                    }
                    return false;
                }
            }
        });
        return false;
    });


    $("body").on("submit", ".contact1", function (e) {
            data1 = $(".contact1").serializeArray();
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/feedback.php',
                data: data1,
                success: function (data) {
                    $(".modal").remove();
                    data2 = JSON.parse(data);
                    if (data2['SUCCESS'] == "Y") {
                        $(".contact1 input").removeClass('error');
                        $(".res1").html("Сообщение отправлено");
                    } else {
                        $.each(data2, function (key, value) {
                                if (key == "captcha") {
                                    if (value != "R") {
                                        if (value == "Y") {
                                            $(".contact1 input[name=captcha_contact]").removeClass('error');
                                        } else {
                                            $(".contact1 input[name=captcha_contact]").addClass('error');
                                        }
                                    } else if (value == "R") {
                                        $(".contact1 input[name=captcha_contact]").addClass('error');
                                        return;
                                    }
                                } else if (key == "captcha_code") {
                                    $('#CaptchaImageContact').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + value);
                                    $('#captcha_sid_contact').val(value);
                                } else if (value == '') {
                                    if (key == "comment") {
                                        $(".contact1 textarea[name=" + key + "]").addClass('error');
                                    } else {
                                        $(".contact1 input[name=" + key + "]").addClass('error');
                                    }
                                } else {
                                    if (key == "comment") {
                                        $(".contact1 textarea[name=" + key + "]").removeClass('error');
                                    } else {
                                        $(".contact1 input[name=" + key + "]").removeClass('error');
                                    }
                                }
                            }
                        );
                    }
                }
            });
            return false;
        }
    );

    $('body').on('submit', '.changeMarket', function (e) {
            $wrapper = $(this);
            var cost = $(this).find("input.cost").val();
            var id = $(this).find("input.id").val();
            var quantity = $(this).find("input.quantity").val();
            // $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/addToCartMarket.php',
                data: {quantity, cost, id},
                success: function (data) {
                    // $(".modal").remove();
                    if (data != "") {
                        updateCart($wrapper.find("input.quantity").val());
                        changeText = "Добавлено";
                        text = $($wrapper).find("input.addToCart").val();
                        elem = $($wrapper).find("input.addToCart");
                        elem[0].value = "Добавлено";
                    }
                }
            });

            return false;
        }
    );


    $('body').on('change', '.changeMarket', function (e) {
            if (e.target.className != "select")
                return;
            $wrapper = $(this);
            var message_pri = $(this).find(".select:checked").val();
            var allId = $(this).find(".allId").val();
            var allPrices = $(this).find(".allPrices").val();
            var allPropertiesName = $(this).find(".allPropertiesName").val();
            var allPropertiesValue = $(this).find(".allPropertiesValue").val();
            var allPropertiesImages = $(this).find(".allPropertiesImages").val();
            var allStatusValue = $(this).find(".allStatusValue").val();
            var allDiscountValue = $(this).find(".allDiscountValue").val();
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/changeMarket.php',
                data: {
                    message_pri,
                    allId,
                    allPrices,
                    allPropertiesName,
                    allPropertiesValue,
                    allPropertiesImages,
                    allStatusValue,
                    allDiscountValue
                },
                success: function (data) {
                    $(".modal").remove();
                    data2 = JSON.parse(data);
                    if (data2['SUCCESS'] == "Y") {

                        inputCost = $($wrapper).find(".cost")[0].value = data2["PRICE"];
                        inputId = $(".id")[0].value = data2["ID"];
                        $('.item_info').find('.sum').html(getOfferDetail($(".item_info")));
                        propertyName = $('.propertyName');
                        propertyValue = $('.propertyValue');
                        propertyImages = $('.photoalbum');
                        propertyImages.empty();
                        discount = $(".main_img").find(".skidka_div");
                        discount.remove();
                        if (data2["STATUS"] === 1) {
                            $("div.title").find("div").replaceWith("<div class=\"we_have\">\n" +
                                "<span>В наличии <i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i></span>\n" +
                                "</div>");
                        } else if (data2["STATUS"] === 0) {
                            $("div.title").find("div").replaceWith(" <div class=\"zakaz\">\n" +
                                "<span>Доставка до 3-х дней<i><img src=\"\" alt=\"\"></i></span>\n" +
                                "<span class=\"sub_text\">Цена товара<br>может измениться.</span>\n" +
                                "</div>");
                        } else {
                            $("div.title").find("div").replaceWith(" <div class=\"no_zakaz\">\n" +
                                "<span>Нет в наличии</span>\n" +
                                "</div>");
                        }
                        if (data2["DISCOUNT"] > 0) {
                            $(".main_img").append(" <div class=\"skidka_div\"><div class=\"skidka\">-" + data2["DISCOUNT"] + "%</div></div>\n" +
                                "                            <div class=\"skidka_div\"></div>");
                        }
                        for (var i = 0; i < propertyName.length; i++) {
                            if (data2["PROPERTIES_NAME"].length <= i) {
                                $(propertyName[i]).hide();
                                $(propertyValue[i]).hide();
                            } else {
                                $(propertyName[i]).show();
                                $(propertyValue[i]).show();
                                $(propertyName[i]).html(data2["PROPERTIES_NAME"][i]);
                                $(propertyValue[i]).html(data2["PROPERTIES_VALUE"][i]);
                            }
                        }
                        propertyImages.append("<div class=\"owl-carousel  owl-theme\"></div>");
                        propertyImages = $(".photoalbum .owl-carousel");
                        for (var i = 0; i < data2["PROPERTIES_IMAGES"].length; i++) {
                            if (data2["PROPERTIES_IMAGES"][i].length == 2)
                                propertyImages.append("<div><a href=" + data2["PROPERTIES_IMAGES"][i][1] + ">" +
                                    "<img src=" + data2["PROPERTIES_IMAGES"][i][0] + " data-default=" + data2["PROPERTIES_IMAGES"][i][1] + ">" +
                                    "</a></div>");
                            else
                                propertyImages.append("<div><a href=" + data2["PROPERTIES_IMAGES"][i] + ">" +
                                    "<img src=" + data2["PROPERTIES_IMAGES"][i] + ">" +
                                    "</a></div>");
                        }
                        var owl2 = $(".photoalbum .owl-carousel");
                        owl2.owlCarousel({
                            items: 1,
                            margin: 1,
                            smartSpeed: 700,

                            loop: true,
                            nav: true,
                            dots: true,
                            navText: ['<i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>',
                                '<i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>'],
                            responsive: {
                                0: {
                                    items: 1,
                                    slideBy: 1
                                },
                                768: {
                                    items: 1,
                                    slideBy: 1,
                                },
                                1180: {
                                    items: 1,
                                    slideBy: 1,
                                }
                            }
                        });
                        $('.main_img a').lightcase();

                        elem = $($wrapper).find("input.addToCart");
                        elem[0].value = "В корзину";
                    } else
                        return false;
                }
            });

            return false;
        }
    );


    $('body').on('change', '.changeMarketQuick', function (e) {
            if (e.target.className != "select")
                return;
            $wrapper = $(".arcticmodal-container .changeMarketQuick ");
            var message_pri = $(this).find(".select:checked").val();
            var allId = $(this).find(".allId").val();
            var allPrices = $(this).find(".allPrices").val();
            var allPropertiesName = $(this).find(".allPropertiesName").val();
            var allPropertiesValue = $(this).find(".allPropertiesValue").val();
            var allPropertiesImages = $(this).find(".allPropertiesImages").val();
            var allStatusValue = $(this).find(".allStatusValue").val();
            var allDiscountValue = $(this).find(".allDiscountValue").val();
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/changeMarket.php',
                data: {
                    message_pri,
                    allId,
                    allPrices,
                    allPropertiesName,
                    allPropertiesValue,
                    allPropertiesImages,
                    allStatusValue,
                    allDiscountValue
                },
                success: function (data) {
                    $(".modal").remove();
                    data2 = JSON.parse(data);
                    if (data2['SUCCESS'] == "Y") {
                        inputCost = $($wrapper).find(".cost")[0].value = data2["PRICE"];
                        inputId = $($wrapper).find(".id")[0].value = data2["ID"];
                        $('.item_info').find('.sum').html(getOfferDetail($(".item_info")));
                        propertyName = $('.propertyName');
                        propertyValue = $('.propertyValue');
                        propertyMainImage = $('.rphoto');
                        discount = $(".main_img").find(".skidka_div");
                        discount.remove();
                        for (var i = 0; i < propertyName.length; i++) {
                            if (data2["PROPERTIES_NAME"].length <= i) {
                                $(propertyName[i]).hide();
                                $(propertyValue[i]).hide();
                            } else {
                                $(propertyName[i]).show();
                                $(propertyValue[i]).show();
                                $(propertyName[i]).html(data2["PROPERTIES_NAME"][i]);
                                $(propertyValue[i]).html(data2["PROPERTIES_VALUE"][i]);
                            }
                        }
                        propertyMainImage.empty();
                        if (data2["STATUS"] === 1) {
                            $("div.title").find("div").replaceWith("<div class=\"we_have\">\n" +
                                "<span>В наличии <i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i></span>\n" +
                                "</div>");
                        } else if (data2["STATUS"] === 0) {
                            $("div.title").find("div").replaceWith(" <div class=\"zakaz\">\n" +
                                "<span>Доставка до 3-х дней<i><img src=\"\" alt=\"\"></i></span>\n" +
                                "<span class=\"sub_text\">Цена товара<br>может измениться.</span>\n" +
                                "</div>");
                        } else {
                            $("div.title").find("div").replaceWith(" <div class=\"no_zakaz\">\n" +
                                "<span>Нет в наличии</span>\n" +
                                "</div>");
                        }
                        if (data2["DISCOUNT"] > 0) {
                            $(".main_img").append(" <div class=\"skidka_div\"><div class=\"skidka\">-" + data2["DISCOUNT"] + "%</div></div>\n" +
                                "<div class=\"skidka_div\"></div>");
                        }
                        if (data2["PROPERTIES_IMAGES"][0].length == 2)
                            propertyMainImage.append("" +
                                "<a data-default=" + data2["PROPERTIES_IMAGES"][0][1] + " id='main-good-photo' data-role='main-photo'><img id='image_l' src=" + data2["PROPERTIES_IMAGES"][0][0] + " data-default=" + data2["PROPERTIES_IMAGES"][0][0] + ">\n" + "</a>");
                        else
                            propertyMainImage.append("" +
                                "<a data-default=" + data2["PROPERTIES_IMAGES"][0] + " id='main-good-photo' data-role='main-photo'><img id='image_l' src=" + data2["PROPERTIES_IMAGES"][0] + " data-default=" + data2["PROPERTIES_IMAGES"][0] + ">\n" + "</a>");
                        elem = $($wrapper).find("input.addToCart");
                        elem[0].value = "В корзину";
                    } else
                        return false;
                }
            });

            return false;
        }
    );

    $('body').on('submit', '.changeMarketQuick', function (e) {
            $wrapper = $(this);
            var cost = $(this).find("input.cost").val();
            var id = $(this).find("input.id").val();
            var quantity = $(this).find("input.quantity").val()
            // $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/addToCartMarket.php',
                data: {quantity, cost, id},
                success: function (data) {
                    // $(".modal").remove();
                    if (data != "") {
                        updateCart($wrapper.find("input.quantity").val());
                        changeText = "Добавлено";
                        text = $($wrapper).find("input.addToCart").val();
                        elem = $($wrapper).find("input.addToCart");
                        elem[0].value = "Добавлено";
                    }
                }
            });

            return false;
        }
    );


    var getOfferDetail = function (e) {
        cost = $(e).find(".cost").val();
        quantity = $(e).find(".quantity").val();
        price = cost * quantity;
        return (price);
    }

    $('body').on('click', '.minus_prod_det', function () {
        var inForm = $(this).closest('form').find(".quantity").val();
        if (parseInt(inForm) == 1) {
            this.value = 1;
            return false;
        }
        input = $(this.parentElement.parentElement).find("input");
        val = $(this.parentElement.parentElement).find("input").val();
        if (val == 1)
            return;
        val = val - 1;
        input[0].value = val;
        $(this).closest(".changeMarket").find(".sum").html(getOfferDetail($(input.closest("form"))));
        $(this).closest(".changeMarketQuick").find(".sum").html(getOfferDetail($(input.closest("form"))));
    });


    $('body').on('click', '.plus_prod_det', function () {
        var total = $(this).closest('form').find(".current_quantity").val();
        var inForm = $(this).closest('form').find(".quantity").val();
        if (parseInt(total) == parseInt(inForm)) {
            this.value = total;
            return false;
        }
        input = $(this.parentElement.parentElement).find("input");
        val = $(this.parentElement.parentElement).find("input").val();
        val++;
        input[0].value = val;
        $(this).closest(".changeMarket").find(".sum").html(getOfferDetail($(input.closest("form"))));
        $(this).closest(".changeMarketQuick").find(".sum").html(getOfferDetail($(input.closest("form"))));
    });

    $('body').on('submit', '.addToCart', function (e) {
            var cost = $(this).find("input.cost").val();
            var id = $(this).find("input.id").val();
            var $wrapper = $(this);
            // $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/addToCart.php',
                data: {cost, id},
                success: function (data) {
                    // $('.modal').remove();
                    if (data == 1) {
                        updateCart(1);
                        $wrapper.find(".inner1").hide();
                        $wrapper.addClass("f_step s_step");
                        setTimeout(function () {
                            $wrapper.removeClass("f_step s_step");
                        }, 3000);
                    }
                }
            });

            return false;
        }
    );

    $('body').on('submit', '.addToCartMarket', function (e) {
            var $wrapper = $(this);
            $quantityes = $(this).find(".quantity");
            $costs = $(this).find(".cost");
            $id_s = $(this).find(".id");
            var quantity = [];
            var id = [];
            var cost = [];
            $quantityCount = 0;
            $("body").append("<div class='modal'></div>");
            for (var i = 0; i < $costs.length; i++) {
                quantity.push($quantityes[i].value);
                $quantityCount = $quantityCount + +$quantityes[i].value;
                cost.push(parseInt($costs[i].value));
                id.push($id_s[i].value);
            }
            if ($quantityCount == 0)
                return false;
            var $wrapper = $(this);
            $.ajax({
                type: 'POST',
                url: '/ajax/addToCartMarket.php',
                data: {quantity, cost, id},
                success: function (data) {
                    $(".modal").remove();
                    if (data != "") {
                        var count = 0;
                        for (var i = 0; i < $quantityes.length; i++)
                            count = count + +$($quantityes[i]).val();
                        updateCart(count);
                        $wrapper.find(".inner1").hide();
                        $wrapper.addClass("f_step s_step");
                        setTimeout(function () {
                            $wrapper.removeClass("f_step s_step");
                        }, 3000);
                    }
                }
            });

            return false;
        }
    );


    $('body').on('click', '.icon_add_to_cart', function (e) {
            var wrapper = $(this);
            var cost = $(this).find(".cost").val();
            var id = $(this).find(".id").val();
            // $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/addToCart.php',
                data: {cost, id},
                success: function (data) {
                    var tr = wrapper.closest("tr");
                    tr.children().not(".offerName").remove();
                    tr.append("<td colspan=\"2\">\n" +
                        " <div class=\"spinEdit\" data-available-quantity=\"122\">\n" +
                        "<input class=\"noSelect quantity plus\" type=\"text\" value=\"1\" name=\"count\" data-id=\"" + id + "\" data-price=\"" + cost + "\"/>\n" +
                        "<div style=\"display: none;\" class=\"totalCost\">" + cost + "</div>" +
                        "<div class=\"spinedit\"><div class=\"minus_prod minus-btn icon-chevron-down\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i></div><div class=\"plus_prod plus-btn icon-chevron-up\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i></div></div>\n" +
                        "</div>\n" +
                        "</td>");
                    var totalPrice = tr.closest('.inner_wrap').find(".sum").html();
                    tr.closest('.inner_wrap').find(".sum").html((+totalPrice) + (+cost));
                    // $(".modal").remove();
                    if (data == 1) {
                        updateCart(1);
                    }
                }
            });

            return false;
        }
    );


    $(".cancel").click(function (e) {
        var $wrapper = $(this);
        $iner1 = $wrapper.closest(".inner1");
        $wrapper = $wrapper.closest(".card");
        $wrapper.removeClass("f_step s_step");
        setTimeout(function () {
            $iner1.hide();
        }, 1000);
    })


    $("body").on('click', '.f_look', function () {
        $wrapper = $(this).closest(".card");
        var id = $wrapper.find("input.id");
        var cost = $wrapper.find("input.cost");
        var subcribe_id = $wrapper.find("input.subcribe_id").val();
        var ids = [];
        var costs = [];
        for (var i = 0; i < id.length; i++) {
            costs.push(parseInt(cost[i].value));
            ids.push(id[i].value);
        }
        var index = -1;
        var allBtn = $('body').find("[data-role=quickView]");
        for (var i = 0; i < allBtn.length; i++) {
            if ($(this).is($(allBtn[i])))
                index = i;
        }
        $("body").append("<div class='modal'></div>");
        $.ajax({
            type: 'POST',
            url: '/ajax/quickView.php',
            data: {costs, ids, subcribe_id},
            success: function (data) {
                $(".modal").remove();
                $("body").append(data);
                if (index == 0)
                    $(".prev").remove();
                else if (index == allBtn.length - 1)
                    $(".next").remove();
                $(".arcticmodal-container").on('click', '.close', function (e) {
                    $("body .lol").remove();
                    $("body .arcticmodal-container").remove();
                    return false;
                });
                $(".prev").on('click', function () {
                    $("body .lol").remove();
                    $("body .arcticmodal-container").remove();
                    allBtn[index - 1].click();
                });

                $(".next").on('click', function () {
                    $("body .lol").remove();
                    $("body .arcticmodal-container").remove();
                    allBtn[index + 1].click();
                });

                $(document).mouseup(function (e) { // событие клика по веб-документу
                    var div = $(".big_item"); // тут указываем ID элемента
                    if (!div.is(e.target) // если клик был не по нашему блоку
                        && div.has(e.target).length === 0) { // и не по его дочерним элементам
                        $("body .lol").remove();
                        $("body .arcticmodal-container").remove();
                    }
                });
            }
        });
        return false;
    });


    $('body').on('keyup', '.quantity', function (e) {
        var classList = this.className.split(/\s+/);
        for (var i = 0; i < classList.length; i++) {
            input = $(this.parentElement.parentElement).find("input");
            if (classList[i] == 'plus') {
                input = $(this.parentElement.parentElement).find("input");
                $('.sum').html(getOffers(input.closest("tbody")));
            } else if (classList[i] == 'input-quantity') {
                input = $(this.parentElement.parentElement).find("input");
                $('.sum').html(getOfferDetail(input.closest("form")));
            }
        }
    });

    $('body').on('paste', '.quantity', function (e) {
        return false;
    });

    $('body').on('keypress', '.quantity', function (evt) {
        return false;
        // input = $(".quantity").find("input");
        // var theEvent = evt || window.event;
        // var key = theEvent.keyCode || theEvent.which;
        // key = String.fromCharCode( key );
        // var regex = /[0-9]|\./;
        // if(!regex.test(key) ) {
        //     theEvent.returnValue = false;
        //     if(theEvent.preventDefault) theEvent.preventDefault();
        // }
        // return 0;
    });

    var updateCart = function (quantity) {
        var $old = $(".count").text();
        $(".count").html(+$old + +quantity);
    }


    var getOffers = function (e) {
        allOffers = $(e).find(".quantity");
        allCost = $(e).find(".totalCost");
        price = 0;
        for (var i = 0; i < allOffers.length; i++) {
            price += allOffers[i].value * parseInt(allCost[i].textContent);
        }
        return (price);
    }


    $('body').on('click', '.minus_prod', function () {
        var wrapper = $(this);
        var input = $(this).closest(".spinEdit").find(".quantity");
        var id = input.attr("data-id")
        var quantity = input.val();
        if (quantity == 0)
            return;
        var operation = "minus";
        // $("body").append("<div class='modal'></div>");
        $.ajax({
            type: 'POST',
            url: '/ajax/changeQuantityOfProductInBasket.php',
            data: {id, operation},
            success: function (data) {
                data2 = JSON.parse(data);
                // $(".modal").remove();
                if (data2["SUCCESS"] == "Y") {
                    input[0].value = (+quantity) - 1;
                    wrapper.closest(".inner1").find(".sum").html(getOffers(input.closest("tbody")));
                    updateCart(-1);
                } else {
                    console.log(data2);
                }
            }
        });
    });


    $('body').on('click', '.plus_prod', function () {
        var wrapper = $(this);
        var input = $(this).closest(".spinEdit").find(".quantity");
        var id = input.attr("data-id");
        var quantity = input.val();
        var operation = "plus";
        // $("body").append("<div class='modal'></div>");
        $.ajax({
            type: 'POST',
            url: '/ajax/changeQuantityOfProductInBasket.php',
            data: {id, operation},
            success: function (data) {
                data2 = JSON.parse(data);
                // $(".modal").remove();
                console.log(data2);
                if (data2["SUCCESS"] == "Y") {
                    input[0].value = (+quantity) + 1;
                    wrapper.closest(".inner1").find(".sum").html(getOffers(input.closest("tbody")));
                    updateCart(1);
                } else {
                    console.log(data2);
                }
            }
        });
    });


    $("body").on("submit", ".faq", function (e) {
            data1 = $(".faq").serializeArray();
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/sendFAQ.php',
                data: data1,
                success: function (data) {
                    $(".modal").remove();
                    data2 = JSON.parse(data);
                    if (data2['SUCCESS'] == "Y") {
                        $(".faq input").removeClass('error');
                        $(".faq textarea").removeClass('error');
                        $(".res1").html("Вопрос отправлен на сайт. Вы получите уведомление о его публикации после проверки!");
                    } else {
                        $.each(data2, function (key, value) {
                                if (key == "captcha") {
                                    if (value != "R") {
                                        if (value == "Y")
                                            $(".faq input[name=" + key + "]").removeClass('error');
                                        else
                                            $(".faq input[name=" + key + "]").addClass('error');
                                    } else if (value == "R") {
                                        $(".faq input[name=" + key + "]").addClass('error');
                                        return;
                                    }
                                } else if (key == "captcha_code") {
                                    $('#CaptchaImage').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + value);
                                    $('#captcha_sid').val(value);
                                } else if (value == '') {
                                    if (key == "text")
                                        $(".faq textarea[name=" + key + "]").addClass('error');
                                    else
                                        $(".faq input[name=" + key + "]").addClass('error');
                                    return;
                                } else {
                                    if (key == "text")
                                        $(".faq textarea[name=" + key + "]").removeClass('error');
                                    else
                                        $(".faq input[name=" + key + "]").removeClass('error');
                                    return;
                                }
                            }
                        );
                    }
                }
            });

            return false;
        }
    );

    $("body").on("submit", ".ads", function (e) {
            e.preventDefault();
            var $that = $(this);
            formData = new FormData($that.get(0));
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/sendAds.php',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () { // Функция вызывается перед отправкой запроса
                    console.debug('Запрос отправлен. Ждите ответа.');
                    // тут можно, к примеру, начинать показ прелоадера, в общем, на ваше усмотрение
                },
                error: function (req, text, error) { // отслеживание ошибок во время выполнения ajax-запроса
                    console.error('Упс! Ошибочка: ' + text + ' | ' + error);
                },
                success: function (data) {
                    $(".modal").remove();
                    data2 = JSON.parse(data);
                    if (data2['SUCCESS'] == "Y") {
                        $(".ads input").removeClass('error');
                        $(".ads textarea").removeClass('error');
                        $(".res1").html("Объявление отправлено на сайт. Вы получите уведомление о его публикации после проверки!");
                    } else {
                        $.each(data2, function (key, value) {
                                if (key == "captcha") {
                                    if (value != "R") {
                                        if (value == "Y")
                                            $(".ads input[name=" + key + "]").removeClass('error');
                                        else
                                            $(".ads input[name=" + key + "]").addClass('error');
                                    } else if (value == "R") {
                                        $(".ads input[name=" + key + "]").addClass('error');
                                        return;
                                    }
                                } else if (key == "captcha_code") {
                                    $('#CaptchaImage').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + value);
                                    $('#captcha_sid').val(value);
                                } else if (value == '') {
                                    if (key == "text")
                                        $(".ads textarea[name=" + key + "]").addClass('error');
                                    else
                                        $(".ads input[name=" + key + "]").addClass('error');
                                    return;
                                } else {
                                    if (key == "text")
                                        $(".ads textarea[name=" + key + "]").removeClass('error');
                                    else
                                        $(".ads input[name=" + key + "]").removeClass('error');
                                    return;
                                }
                            }
                        );
                    }
                }
            });

            return false;
        }
    );

    $("body").on("submit", ".check_review", function (e) {
            data1 = $(".check_review").serializeArray();
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/sendReview.php',
                data: data1,
                success: function (data) {
                    $(".modal").remove();
                    data2 = JSON.parse(data);
                    if (data2['SUCCESS'] == "Y") {
                        $(".check_review input").removeClass('error');
                        $(".check_review textarea").removeClass('error');
                        $(".res1").html("Отзыв отправлен. Вы получите уведомление о его публикации после проверки!");
                    } else {
                        $.each(data2, function (key, value) {
                                if (key == "captcha") {
                                    if (value != "R") {
                                        if (value == "Y")
                                            $(".check_review input[name=" + key + "]").removeClass('error');
                                        else
                                            $(".check_review input[name=" + key + "]").addClass('error');
                                        return;
                                    } else if (value == "R") {
                                        $(".check_review input[name=" + key + "]").addClass('error');
                                        return;
                                    }
                                }
                                if (key == "captcha_code") {
                                    $('#CaptchaImage').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + value);
                                    $('#captcha_sid').val(value);
                                    return;
                                }
                                if (value == '') {
                                    if (key == "content")
                                        $(".check_review textarea[name=" + key + "]").addClass('error');
                                    else
                                        $(".check_review input[name=" + key + "]").addClass('error');
                                } else {
                                    if (key == "content")
                                        $(".check_review textarea[name=" + key + "]").removeClass('error');
                                    else
                                        $(".check_review input[name=" + key + "]").removeClass('error');
                                }
                            }
                        );
                    }
                }
            });

            return false;
        }
    );


    $("body").on("submit", ".checkProductReview", function (e) {
            $(".errors").empty();
            data1 = $(".checkProductReview").serializeArray();
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/sendProductReview.php',
                data: data1,
                success: function (data) {
                    $(".modal").remove();
                    data2 = JSON.parse(data);
                    if (data2['SUCCESS'] == "Y") {
                        $(".checkProductReview input").removeClass('error');
                        $(".checkProductReview textarea").removeClass('error');
                        $(".faq_form").html("Отзыв на товар отправлен. Отзыв будет опубликован после проверки администрации!");
                    } else {
                        $.each(data2, function (key, value) {
                                if (key == "captcha") {
                                    if (value != "R") {
                                        if (value == "Y")
                                            $(".checkProductReview input[name=" + key + "]").removeClass('error');
                                        else
                                            $(".checkProductReview input[name=" + key + "]").addClass('error');
                                        return;
                                    } else if (value == "R") {
                                        $(".checkProductReview input[name=" + key + "]").addClass('error');
                                        return;
                                    }
                                }

                                if (key == "captcha_code") {
                                    $('#CaptchaImage').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + value);
                                    $('#captcha_sid').val(value);
                                    return;
                                }
                                if (value == '') {
                                    if (key == "content")
                                        $(".checkProductReview textarea[name=" + key + "]").addClass('error');
                                    else
                                        $(".checkProductReview input[name=" + key + "]").addClass('error');
                                } else {
                                    if (key == "content")
                                        $(".checkProductReview textarea[name=" + key + "]").removeClass('error');
                                    else
                                        $(".checkProductReview input[name=" + key + "]").removeClass('error');
                                }
                            }
                        );
                    }
                }
            });

            return false;
        }
    );


    jQuery(".tabs .tab-content:not(.tab-content-comments)").disableTextSelect();

    jQuery('dl.tabs dt').click(function () {
        jQuery(this)
            .siblings().removeClass('selected').end()
            .next('dd').andSelf().addClass('selected');
    });


    $("body").on("click", ".view_more", function (e) {
        jQuery("#catalog_ajax_container").animate({opacity: 0.3});
        jQuery("#catalog_ajax_container").load(this.href + "&ajax_call=true", function () {
            jQuery("#catalog_ajax_container").animate({opacity: 1});
        });
        return false;
    });


    $("body").on("click", ".oneclick_btn2", function (e) {
            open_dialog('wind_zakaz2');
            return false;
        }
    );


    $("body").on("click", ".oneclick_btn", function (e) {
            open_dialog('wind_zakaz2');
            return false;
        }
    );

    $("body").on("submit", ".form_1clk", function (e) {
            if ($(".form_1clk .phoneCall").val() == '')
                $(".phoneCall").addClass('error');
            else {
                data1 = $(".form_1clk").serialize();
                $("body").append("<div class='modal'></div>");
                $.ajax({
                    type: 'POST',
                    url: '/ajax/getCall.php',
                    data: data1,
                    success: function (data) {
                        data2 = JSON.parse(data);
                        $(".modal").remove();
                        if (data2['SUCCESS'] == "Y") {
                            $('.fix-form_pzf').remove();
                            $(".mobile-display").has(".fix-form_pzf").remove();
                            $('.wind_sputnik').html('Спасибо, заявка отправлена! Наш менеджер свяжется с вами в ближайшее время.');
                            setTimeout(function () {
                                $('.wind_sputnik').fadeOut("hide");
                            }, 1000);
                            setTimeout(function () {
                                $('.wind_sputnik').remove();
                                $('.wind_bg').remove();
                            }, 1500);
                        } else {
                            if (data2['captcha'] == "Y")
                                $(".form_res1 input").removeClass('error');
                            else if (data2['captcha'] == "N") {
                                $('#CaptchaImage').attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + data2['captcha_code']);
                                $('#captcha_sid').val(data2['captcha_code']);
                                $(".form_res1 input[name=captchaCheck]").addClass('error');
                            } else {
                                $(".form_res1 input[name=captchaCheck]").addClass('error');
                            }
                        }
                    }
                });

            }
            return false;
        }
    );


    $("body").on("submit", ".form_acc", function (e) {
            data1 = $(".form_acc").serialize();
            $(".errors").empty();
            $(".passwordError").empty();
            $("input").removeClass('error');
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/changeUserContacts.php',
                data: data1,
                success: function (data) {
                    $(".modal").remove();
                    $('.edit_btn, .save_btn').toggle();
                    $('.form_acc td').attr('disabled', 'true');
                    $(".passwordError").append("<p class='timeError' style='color:green'>Изменения успешно сохранены</p>");
                }
            });
            return false;
        }
    );


    $("body").on("click", ".sendForgottenCodeForUser", function (e) {
        $(".sendPhoneError").empty();
        $(".sendPhoneError").hide();
        if (!$(".forgottenPasswordPhone").val()) {
            $(".sendPhoneError").append("Введите номер телефона");
            $(".sendPhoneError").show();
        } else {
            var phoneNumber = $(".forgottenPasswordPhone").val();
            $("body").append("<div class='modal'></div>");
            $.ajax({
                type: 'POST',
                url: '/ajax/forgottenPasswordSms.php',
                data: {phone: phoneNumber},
                success: function (data) {
                    $(".modal").remove();
                    data2 = JSON.parse(data);
                    if (data2["SUCCESS"] == "Y") {
                        $(".changePasswordWithSms").html("<div class=\"alert alert-success\">\n" +
                            "Новый пароль был выслан по смс. Сменить пароль в дальнейшем можно в личном кабинете.\n" +
                            "\t</div>");

                    } else {
                        $(".sendPhoneError").append(data2["ERROR"]);
                        $(".sendPhoneError").show();
                    }
                }
            });
        }
        return false;
    });

    jQuery('.forma-close').click(function () {
        jQuery('.wind_bg, .wind_sputnik').hide();
        $(".errors").empty();
        $(".error").empty();
        $("input").removeClass('error');
    });

    jQuery('.vkladki a').click(function (e) {
        e.preventDefault();

        var ind = jQuery('.vkladki a').index(jQuery(this));
        jQuery('.vkladki a, .vkl_item').removeClass('active');
        jQuery('.vkladki a').eq(ind).addClass('active');
        jQuery('.vkl_item').eq(ind).addClass('active');
        window.location.hash = jQuery(this).attr("href");
    });

    if (window.location.hash.length) {
        jQuery('.vkladki a[href=' + window.location.hash + ']').trigger("click");
    }
    $("body").on("click", ".reset", function () {
        $(".errors").empty();
        $(".passwordError").empty();
        $("input").removeClass('error');
        $('.edit_btn, .save_btn').toggle();
        $('.form_acc input:not(.userPhoneEdit, .sendCodeForUser, .edit)').attr('disabled', 'true');
        $('.form_acc td').attr('disabled', 'true');
        $(".form_acc").reset();
    });
    jQuery('.edit_btn input').click(function () {
        jQuery('.edit_btn, .save_btn').toggle();
        jQuery('.form_acc input:not(.userPhoneEdit, .sendCodeForUser,  .edit)').removeAttr('disabled');
        jQuery('.form_acc td').removeAttr('disabled');
    });
    jQuery('.form_acc input:not(.userPhoneEdit, .sendCodeForUser, .edit)').attr('disabled', 'true');
    jQuery('.form_acc td').attr('disabled', 'true');
    jQuery(function ($) {
        jQuery(".phone-mask").mask("9 (999) 999-99-99");
        jQuery("#field_8").mask("9 (999) 999-99-99");
    });

    jQuery("body").on("click", ".n_minus", function () {
        var $input = jQuery(this).parent().find("input[type=text]");
        var v = parseInt($input.val());
        v = v || 1;
        if (v > 1)
            v--;
        $input.val(v);
    });
    jQuery("body").on("click", ".n_plus", function () {
        var $input = jQuery(this).parent().find("input[type=text]");
        var v = parseInt($input.val());
        v = v || 1;
        v++;
        $input.val(v);
    });

    jQuery(".disableTextSelect").disableTextSelect();
});

jQuery.fn.disableTextSelect = function () {
    return this.each(function () {
        jQuery(this).bind('mousedown.disableTextSelect', function () {
            return false;
        });
    });
};

function open_dialog(el) {
    var errors = $(".errors");
    var errorInput = $("input");
    for (var i = 0; i < errors.length; i++) {
        if ($(errors[i]).parents('.form_acc').length > 0)
            continue;
        else
            $(errors[i]).empty();
    }
    for (var i = 0; i < errorInput.length; i++) {
        if ($(errorInput[i]).parents('.form_acc').length > 0 || !$(errorInput[i]).hasClass("error"))
            continue;
        else
            $(errorInput[i]).removeClass('error');
    }
    $('.wind_sputnik').hide();
    var wind = jQuery('.' + el);
    var bg = jQuery('.wind_bg');

    wind.css({'margin-left': '-' + (wind.width() / 2) + 'px', 'margin-top': '-' + (wind.height() / 2) + 'px'}).show();
    bg.show().click(function () {
        jQuery('.wind_bg, .wind_sputnik').hide();
        var errors = $(".errors");
        var errorInput = $("input");
        for (var i = 0; i < errors.length; i++) {
            if ($(errors[i]).parents('.form_acc').length > 0)
                continue;
            else
                $(errors[i]).empty();
        }
        for (var i = 0; i < errorInput.length; i++) {
            if ($(errorInput[i]).parents('.form_acc').length > 0 || !$(errorInput[i]).hasClass("error"))
                continue;
            else
                $(errorInput[i]).removeClass('error');
        }
    });
}


function open_dialog2(el) {
    var errors = $(".errors");
    var errorInput = $("input");
    for (var i = 0; i < errors.length; i++) {
        if ($(errors[i]).parents('.form_acc').length > 0)
            continue;
        else
            $(errors[i]).empty();
    }
    for (var i = 0; i < errorInput.length; i++) {
        if ($(errorInput[i]).parents('.form_acc').length > 0 || !$(errorInput[i]).hasClass("error"))
            continue;
        else
            $(errorInput[i]).removeClass('error');
    }
    $('.wind_sputnik').hide();
    var wind = jQuery('.' + el);
    var bg = jQuery('.wind_bg');

    wind.css({'margin-left': '-' + (wind.width() / 2) + 'px', 'margin-top': '-' + (wind.height() / 2) + 'px'}).show();
    bg.show().click(function () {
        jQuery('.wind_bg, .wind_sputnik').hide();
        var errors = $(".errors");
        var errorInput = $("input");
        for (var i = 0; i < errors.length; i++) {
            if ($(errors[i]).parents('.form_acc').length > 0)
                continue;
            else
                $(errors[i]).empty();
        }
        for (var i = 0; i < errorInput.length; i++) {
            if ($(errorInput[i]).parents('.form_acc').length > 0 || !$(errorInput[i]).hasClass("error"))
                continue;
            else
                $(errorInput[i]).removeClass('error');
        }
    });
}
