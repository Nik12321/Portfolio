<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true);?>
<div class="search-page">
<script>
    function emptyForm () {
        var txt = document.getElementById('q').value;
        if(txt.trim() == '') {
            return false;
        }
        return true;
    }
</script>
<form action="/shop/search<?echo $arResult["FORM_ACTION"]?>" method="get" onsubmit="return emptyForm()">
    <svg id="SvgjsSvg1078" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16">
        <path id="SvgjsPath1080" d="M63.6249 285.999C66.0369 285.999 67.9999 284.03700000000003 67.9999 281.624C67.9999 279.21200000000005 66.0369 277.249 63.6249 277.249C61.2127 277.249 59.2499 279.21200000000005 59.2499 281.624C59.2499 284.03700000000003 61.2127 285.999 63.6249 285.999ZM71.2322 290.999L66.6156 286.38300000000004C65.7482 286.92900000000003 64.7253 287.249 63.624900000000004 287.249C60.518100000000004 287.249 57.99980000000001 284.73100000000005 57.99980000000001 281.624C57.99980000000001 278.51800000000003 60.518100000000004 275.999 63.62490000000001 275.999C66.73150000000001 275.999 69.24980000000001 278.51800000000003 69.24980000000001 281.624C69.24980000000001 282.725 68.92880000000001 283.74800000000005 68.38310000000001 284.615L72.99990000000001 289.232ZM64.2498 283.499H61.12479999999999V282.874H64.2498ZM66.1248 282.25H61.12479999999999V281.624H66.1248ZM66.1248 280.374H61.12479999999999V280.999H66.1248Z " fill="#7b7bc6" fill-opacity="1" transform="matrix(1,0,0,1,-57,-275)"></path>
    </svg>
<?if($arParams["USE_SUGGEST"] === "Y"):
	if(strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"]))
	{
		$arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
            $obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
		$obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
	}
    if (!empty($_GET["search_filter_29"])) {
        $_REQUEST['q'] = $_SESSION["SEARCH_INPUT"];
        $_REQUEST['how'] = $_SESSION["SEARCH_HOW"];
        $searchFilter = $_SESSION["SEARCH_RESULT"];
    }
	?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:search.suggest.input",
		"",
		array(
			"NAME" => "q",
			"VALUE" => $arResult["REQUEST"]["~QUERY"],
			"INPUT_SIZE" => 40,
			"DROPDOWN_SIZE" => 10,
			"FILTER_MD5" => $arResult["FILTER_MD5"],
		),
		$component, array("HIDE_ICONS" => "Y")
	);?>
<?else:?>
	<input id="q" type="text" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" class="input" />
<?endif;?>
    <button class="s_btn reverse">Найти</button>
	<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
<?if($arParams["SHOW_WHEN"]):?>
	<script>
	var switch_search_params = function()
	{
		var sp = document.getElementById('search_params');
		var flag;

		if(sp.style.display == 'none')
		{
			flag = false;
			sp.style.display = 'block'
		}
		else
		{
			flag = true;
			sp.style.display = 'none';
		}

		var from = document.getElementsByName('from');
		for(var i = 0; i < from.length; i++)
			if(from[i].type.toLowerCase() == 'text')
				from[i].disabled = flag

		var to = document.getElementsByName('to');
		for(var i = 0; i < to.length; i++)
			if(to[i].type.toLowerCase() == 'text')
				to[i].disabled = flag
		return false;
	}
	</script>
	<br /><a class="search-page-params" href="#" onclick="return switch_search_params()"><?echo GetMessage('CT_BSP_ADDITIONAL_PARAMS')?></a>
	<div id="search_params" class="search-page-params" style="display:<?echo $arResult["REQUEST"]["FROM"] || $arResult["REQUEST"]["TO"]? 'block': 'none'?>">
		<?$APPLICATION->IncludeComponent(
			'bitrix:main.calendar',
			'',
			array(
				'SHOW_INPUT' => 'Y',
				'INPUT_NAME' => 'from',
				'INPUT_VALUE' => $arResult["REQUEST"]["~FROM"],
				'INPUT_NAME_FINISH' => 'to',
				'INPUT_VALUE_FINISH' =>$arResult["REQUEST"]["~TO"],
				'INPUT_ADDITIONAL_ATTR' => 'size="10"',
			),
			null,
			array('HIDE_ICONS' => 'Y')
		);?>
	</div>
<?endif?>
</form>
</div>