<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
session_start(); //Начинаем сессию
$this->setFrameMode(true);
global $searchFilter; //Наш фильтр для поиска
global $iblockFilter;
$totalId = array();
if (isset($arResult["ORLND"]["SKU_IBLOCK_ID"]) && isset($arResult["ORLND"]["SKU_IBLOCK_TYPE"]) && ($arResult["ORLND"]["SKU_IBLOCK_TYPE"] == $arParams["IBLOCK_TYPE"]))
    $arIBlockList = array($arParams["IBLOCK_ID"], $arResult["ORLND"]["SKU_IBLOCK_ID"]);
else
    $arIBlockList = array($arParams["IBLOCK_ID"]);

if (Loader::includeModule('search')) { //Если удалось загрузить модуль, то подключаем компонент:
    //arElements хранит id всех товаров, которые удовлетворяют результату поиска
    $frame = new \Bitrix\Main\Page\FrameHelper("search");
    $frame->begin();//вызов компонента
    $arElements = $APPLICATION->IncludeComponent(
        "bitrix:search.page",
        ".default",
        Array(
            "RESTART" => $arParams["RESTART"],
            "NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
            "USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
            "CHECK_DATES" => $arParams["CHECK_DATES"],
            "arrFILTER" => array("iblock_".$arParams["IBLOCK_TYPE"]),
            "arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => $arIBlockList,
            "USE_TITLE_RANK" => "N",
            "DEFAULT_SORT" => "rank",
            "FILTER_NAME" => "",
            "SHOW_WHERE" => "N",
            "arrWHERE" => array(),
            "SHOW_WHEN" => "N",
            "PAGE_RESULT_COUNT" => (isset($arParams["PAGE_RESULT_COUNT"]) ? $arParams["PAGE_RESULT_COUNT"] : 1000),
            "DISPLAY_TOP_PAGER" => "Y",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "N",
        ),
        $component,
        array('HIDE_ICONS' => 'Y')
    );
    ?>
    <?php if (isset($arResult["ORLND"]["SKU_IBLOCK_TYPE"]) && ($arResult["ORLND"]["SKU_IBLOCK_TYPE"] != $arParams["IBLOCK_TYPE"])):?>
        <div style="display: none;">
            <?php
            $arOffers = $APPLICATION->IncludeComponent(
                "bitrix:search.page",
                ".default",
                Array(
                    "RESTART" => $arParams["RESTART"],
                    "NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
                    "USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
                    "CHECK_DATES" => $arParams["CHECK_DATES"],
                    "arrFILTER" => array("iblock_" . $arResult["ORLND"]["SKU_IBLOCK_TYPE"]),
                    "arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => array($arResult["ORLND"]["SKU_IBLOCK_ID"]),
                    "USE_TITLE_RANK" => "N",
                    "DEFAULT_SORT" => "rank",
                    "FILTER_NAME" => "",
                    "SHOW_WHERE" => "N",
                    "arrWHERE" => array(),
                    "SHOW_WHEN" => "N",
                    "PAGE_RESULT_COUNT" => (isset($arParams["PAGE_RESULT_COUNT"]) ? $arParams["PAGE_RESULT_COUNT"] : 50),
                    "DISPLAY_TOP_PAGER" => "Y",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "N",
                ),
                $component,
                array('HIDE_ICONS' => 'Y')
            );
            ?>
        </div>
    <?php endif;?>
    <?
    $frame->beginStub();?>
    <div>Загрузка компонента</div>
    <?
    $frame->end();
    if ((is_array($arElements) && !empty($arElements)) || (isset($arOffers) && is_array($arOffers) && !empty($arOffers))) {
        if (count($arOffers) > 0) {
            $resElem = CIBlockElement::GetList(
                array(),
                array("=ID" => CIBlockElement::SubQuery("PROPERTY_" . $arResult["ORLND"]["SKU_PROPERTY_SID"], array("IBLOCK_ID" => $arResult["ORLND"]["SKU_IBLOCK_ID"], "=ID" => $arOffers))),
                false,
                false,
                array('ID')
            );
            while($ob = $resElem->Fetch()) {
                $totalId[] = $ob["ID"];
            };
            $arElements = array_unique(array_merge($totalId, $arElements));
        }
        $filteredIds = array();
        $res = CIBlockElement::GetList(false, array("=ID" => $arElements, ">PROPERTY_status" => -2), false, false, array("ID"));
        while ($ob = $res->Fetch()) {
            $filteredIds[] = $ob["ID"];
        }
        if (!count($filteredIds)) {
            $_SESSION["SEARCH_RESULT"] =  "false";
            return;
        }
        else {
            $searchFilter = array("=ID" => $filteredIds);
        }

    }
    else {
        if (is_array($arElements)) { //Иначе записываем в переменную сессии со значение false, что вывести отрицательные результат поиска
            $_SESSION["SEARCH_RESULT"] =  "false";
            return;
        }
    }
}
else {//Если не удалось подключить модуль:
    $searchQuery = '';
    if (isset($_REQUEST['q']) && is_string($_REQUEST['q'])) //q - это строка, введеная в поисковик
        $searchQuery = trim($_REQUEST['q']); //Убираем для нее лидирующие пробелы
    if ($searchQuery !== '') //Если строка поиска пуста:
        $searchFilter = array('*SEARCHABLE_CONTENT' => $searchQuery);
    unset($searchQuery);
}
if (!empty($searchFilter) && is_array($searchFilter)) { //Если фильтр не пуст и является массивом, то записываем его в глобальную переменную сессии
    $_SESSION["SEARCH_RESULT"] = $searchFilter;
    $_SESSION["SEARCH_INPUT"] =  $_REQUEST['q'];
    $_SESSION["SEARCH_HOW"] =  $_REQUEST['how'];
}
?>

