<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
if (!empty($_SESSION["SEARCH_RESULT"])) {
    $search_filter = $_SESSION["SEARCH_RESULT"];
    $filter_name = "search_filter";
}
else {
    $search_filter = "";
    $filter_name = "";
}

$GLOBALS["search_filter"][">PROPERTY_status"] = "-2";
session_start();
if (empty($_GET["sort"]) && empty($_SESSION["sort"])) {
    $sort = "name";
} elseif (empty($_GET["sort"])) {
    $sort = $_SESSION["sort"];
} else {
    $sort = $_GET["sort"];
    $_SESSION["sort"] = $sort;
}
if (empty($_GET["order"]) && empty($_SESSION["order"])) {
    $order = "asc";
} elseif (empty($_GET["order"])) {
    $order = $_SESSION["order"];
} else {
    $order = $_GET["order"];
    $_SESSION["order"] = $order;
}
if ($sort == "price") {
    $arParams["ELEMENT_SORT_FIELD"] = CATALOG_PRICE_SCALE_1;
}
elseif ($sort == "name") {
    $arParams["ELEMENT_SORT_FIELD"] = "name";
}
$arParams["ELEMENT_SORT_ORDER"] =$order;
?>


<div id="catalog_ajax_container">
    <div class="<?=($isSidebar ? "col-md-9 col-sm-8" : "col-xs-12")?>">
        <?php
        $APPLICATION->IncludeComponent("kombox:filter", "filterBrand", [
            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "CACHE_TYPE" => "N",	// Тип кеширования
            "CLOSED_OFFERS_PROPERTY_CODE" => [
                0 => "",
                1 => "",
            ],
            "CLOSED_PROPERTY_CODE" => [
                0 => "",
                1 => "",
            ],
            "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
            "FIELDS" => "",	// Показывать дополнительные поля в фильтре
            "FILTER_NAME" => "search_filter",	// Имя выходящего массива для фильтрации
            "HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
            "IBLOCK_ID" => "13",	// Инфоблок
            "IBLOCK_TYPE" => "Catalogs",	// Тип инфоблока
            "INCLUDE_JQUERY" => "N",	// Подключить библиотеку jQuery
            "IS_SEF" => "N",	// Включить
            "MESSAGE_ALIGN" => "LEFT",	// Выводить сообщение с количеством найденных элементов
            "MESSAGE_TIME" => "5",	// Через сколько секунд скрывать сообщение (0 - не скрывать)
            "PAGER_PARAMS_NAME" => "arrPager",	// Имя массива с переменными для построения ссылок в постраничной навигации
            "PAGE_URL" => "/shop/search/?q=".htmlspecialcharsex($_GET["q"])."&how=r",
            "PRICE_CODE" => "",	// Тип цены
            "SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
            "SECTION_CODE" => "",	// Символьный код раздела инфоблока
            "SORT" => "N",	// Сортировать поля фильтра
            "STORES_ID" => "",	// Выводить товары со складов (по умолчанию - все склады)
            "XML_EXPORT" => "N",	// Включить поддержку Яндекс Островов
            "COMPONENT_TEMPLATE" => ".default"
        ],
            false
        );
        global $arrFilter;
        $filter = $GLOBALS[$filter_name];
        $cnt = CIBlockElement::GetList([], [$filter], [], false, ['ID', 'NAME']);
        $navPageCount = $cnt / $arParams["PAGE_ELEMENT_COUNT"];
        if (!is_integer($navPageCount)) {
            $navPageCount = intval($navPageCount) + 1;
        }
        $navResult = new CDBResult();
        $navResult->NavPageCount = $navPageCount;  // Общее количество страниц
        if ($_GET["PAGEN_3"])  {
            $navResult->NavPageNomer = $_GET["PAGEN_3"];
        } else {
            $navResult->NavPageNomer = 1;
        }
        $navResult->NavNum = 3; // Номер пагинатора. Используется для формирования ссылок на страницы
        $navResult->NavPageSize = $arParams["PAGE_ELEMENT_COUNT"];   // Количество записей выводимых на одной странице
        $navResult->NavRecordCount = $cnt; // Общее количество записей  PAGE_ELEMENT_COUNT
        ?>

        <div class="sort_block">
            <form class="sort_form">
                <span class="title">Сортировать по</span>

                <?php
                if($sort == "name") {
                    $title = "Названию";
                } elseif ($order == "asc") {
                    $title = "Цене ↑";
                } else {
                    $title = "Цене ↓";
                }
                ?>
                <select class="sortby" name="sort" onChange="window.location.href=this.value" style="margin: 0px; padding: 0px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0;">
                    <?php if( $title == "Названию") : ?>
                        <option value="<?=$APPLICATION->GetCurPageParam ('sort=name&order=asc', array('sort', 'order'))?>"  selected="true">Названию</option>
                    <?php else:?>
                        <option value="<?=$APPLICATION->GetCurPageParam ('sort=name&order=asc', array('sort', 'order'))?>">Названию</option>
                    <?php endif;?>
                    <?php if($title == "Цене ↑") : ?>
                        <option value="<?=$APPLICATION->GetCurPageParam ('sort=price&order=asc', array('sort', 'order'))?>"  selected="true">Цене ↑</option>
                    <?php else:?>
                        <option value="<?=$APPLICATION->GetCurPageParam ('sort=price&order=asc', array('sort', 'order'))?>">Цене ↑</option>
                    <?php endif;?>
                    <?php if( $title == "Цене ↓") : ?>
                        <option value="<?=$APPLICATION->GetCurPageParam ('sort=price&order=desc', array('sort', 'order'))?>"  selected="true">Цене ↓</option>
                    <?php else:?>
                        <option value="<?=$APPLICATION->GetCurPageParam ('sort=price&order=desc', array('sort', 'order'))?>">Цене ↓</option>
                    <?php endif;?>
                </select>
            </form>
        </div>
        <div class="pagination">
            <?php
            if ($navPageCount > 1) {
                $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', $arParams["PAGER_TEMPLATE"], ['NAV_RESULT' => $navResult]);
            }
            ?>
        </div>
    </div>
    <?php
    $intSectionID = $APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        "searchSection",
        array(
            "ACTION_VARIABLE" => "action",
            "ADD_PICT_PROP" => "-",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/personal/basket.php",
            "BROWSER_TITLE" => "-",
            "BY_LINK" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "N",
            "COMPATIBLE_MODE" => "Y",
            "CONVERT_CURRENCY" => "N",
            "CUSTOM_FILTER" => "",
            "DETAIL_URL" => "#SITE_DIR#/shop/products/#SECTION_ID#/#ELEMENT_ID#",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_COMPARE" => "N",
            "DISPLAY_TOP_PAGER" => "Y",
            "ELEMENT_SORT_FIELD" => "property_STATUS_GROUP",
            "ELEMENT_SORT_ORDER" => "DESC",
            "ELEMENT_SORT_FIELD2" =>  $arParams["ELEMENT_SORT_FIELD"],
            "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER"],
            "ENLARGE_PRODUCT" => "STRICT",
            "FILTER_NAME" => $filter_name,
            "HIDE_NOT_AVAILABLE" => "L",
            "HIDE_NOT_AVAILABLE_OFFERS" => "L",
            "IBLOCK_ID" => "13",
            "IBLOCK_TYPE" => "Catalogs",
            "INCLUDE_SUBSECTIONS" => "Y",
            "LABEL_PROP" => array(
            ),
            "LAZY_LOAD" => "N",
            "LINE_ELEMENT_COUNT" => "3",
            "LOAD_ON_SCROLL" => "N",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "OFFERS_LIMIT" => "15",
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER" => "asc",
            "OFFERS_SORT_ORDER2" => "desc",
            "OFFER_ADD_PICT_PROP" => "-",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "customPagination",
            "PAGER_TITLE" => "Товары",
            "PAGE_ELEMENT_COUNT" => "18",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array(
                0 => "cost",
            ),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
            "PRODUCT_DISPLAY_MODE" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
            "RCM_TYPE" => "personal",
            "SECTION_ID" => "1",
            "SECTION_URL" => "",
            "SEF_MODE" => "N",
            "SEF_RULE" => "",
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "Y",
            "SET_META_KEYWORDS" => "Y",
            "SET_STATUS_404" => "Y",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SHOW_CLOSE_POPUP" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_FROM_SECTION" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "SHOW_SLIDER" => "Y",
            "SLIDER_INTERVAL" => "3000",
            "SLIDER_PROGRESS" => "Y",
            "TEMPLATE_THEME" => "blue",
            "USE_ENHANCED_ECOMMERCE" => "N",
            "USE_MAIN_ELEMENT_SECTION" => "Y",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "COMPONENT_TEMPLATE" => ".default",
            "SECTION_CODE" => "",
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SHOW_ALL_WO_SECTION" => "N",
            "SECTION_ID_VARIABLE" => "SECTION_ID"
        ),
        false
    );
    ?>
    <div style="clear: both; height: 0;"></div>
    <div class="pagination">
        <?php
        if ($navPageCount > 1) {
            $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', $arParams["PAGER_TEMPLATE"], ['NAV_RESULT' => $navResult]);
        }
        ?>
    </div>
</div>