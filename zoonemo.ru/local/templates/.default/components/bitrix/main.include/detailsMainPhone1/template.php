<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? if($arResult["FILE"] <> '') : ?>
    <? $file = file_get_contents($arResult["FILE"]);
    $piece = strip_tags($file);
    $numberOfWords = preg_split("/\s|&nbsp;/", $piece);
    $numberOfWords = array_diff($numberOfWords, array(''));
    $number = str_replace(array("+7", " ", "(", ")","-"), array("8", "", "", "", ""), $numberOfWords[0]);
    $pattern = '/8[0-9]{10}/';
    preg_match($pattern, $number, $matches);
    ?>
    <a  href="tel:<?=$matches[0]?>" class="tel"><?=$numberOfWords[0];?></a>
<? endif;?>