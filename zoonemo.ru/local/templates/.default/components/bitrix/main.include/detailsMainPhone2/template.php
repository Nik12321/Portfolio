<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? if($arResult["FILE"] <> '') : ?>
    <? $file = file_get_contents($arResult["FILE"]);
    $piece = strip_tags($file);
    $numberOfWords = preg_split("/\s|&nbsp;/", $piece);
    $numberOfWords = array_diff($numberOfWords, array(''));
    $number = str_replace(array("", "(", ")", "+", "-"), array("", "", "", "", ""), $numberOfWords[0]);
    $pattern = '/(8|7)[0-9]{10}/';
    preg_match($pattern, $number, $matches);
    $matches = $matches[0];
    if ($matches[0] == 8)
        $matches[0] = 7;
    ?>
    <a href="https://wa.me/<?=$matches?>" class="tel"><?=$numberOfWords[0];?></a>
<? endif;?>