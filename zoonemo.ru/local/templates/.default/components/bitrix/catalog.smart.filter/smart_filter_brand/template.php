<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
    'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);

if (isset($templateData['TEMPLATE_THEME']))
{
    $this->addExternalCss($templateData['TEMPLATE_THEME']);
}
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
$this->addExternalCss("/bitrix/css/main/font-awesome.css");
CModule::IncludeModule("iblock");
?>
<?if (count($arResult["BRANDS"]) > 0) : ?>
    <script>
        function submitform() {
            $('#set_filter').click();
        }
    </script>
    <div class="bx-filter <?=$templateData["TEMPLATE_CLASS"]?> <?if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL") echo "bx-filter-horizontal"?>">
        <form id="reload" name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
            <div class="brend__block">
                <p>Бренды:</p>
                <div class="brends">
                    <?foreach($arResult["BRANDS"] as $key=>$ar) : ?>
                        <div class="br">
                            <?
                            $IMG = CFile::GetFileArray($ar["FILE"]["ID"]);
                            $IMG = $IMG["ID"]
                            ;?>
                            <a name="set_filter">
                                <label  for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
                                    <input
                                            style="display: none"
                                            type="radio"
                                            name="<?=$ar["CONTROL_NAME_ALT"]?>"
                                            id="<?=$ar["CONTROL_ID"]?>"
                                            value="<?=$ar["HTML_VALUE_ALT"]?>"
                                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                    />
                                    <?
                                    $img = CFile::ResizeImageGet(
                                        $IMG,
                                        array("width"=> 300,
                                            "height" => 51),
                                        BX_RESIZE_IMAGE_PROPORTIONAL,
                                        true);
                                    if ($img["src"]) {
                                        $IMG = $img["src"];
                                    }
                                    else {
                                        $IMG = $IMG["SRC"];
                                    }
                                    ?>
                                    <a href="<?=$APPLICATION->GetCurPageParam($ar["CONTROL_NAME_ALT"] . "=".$ar["HTML_VALUE_ALT"] . "&set_filter=444",array($ar["CONTROL_NAME_ALT"], "set_filter"));?>">
                                        <img src="<?=$IMG;?>" alt="" title="<?=$ar["VALUE"];?>">
                                    </a>
                                </label>
                            </a>
                        </div>
                    <?endforeach;?>
                </div>
                <a href="#" class="more s_btn">Еще</a>
                <a href="#" class="svernut s_btn">Свернуть</a>
            </div>
            <input
                    style="display: none"
                    class="btn btn-themes"
                    type="submit"
                    id="set_filter"
                    name="set_filter"
                    value = "444"
            />
        </form>
    </div>
    <script type="text/javascript">
        var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
        console.log(smartFilter);
    </script>
<?endif;?>