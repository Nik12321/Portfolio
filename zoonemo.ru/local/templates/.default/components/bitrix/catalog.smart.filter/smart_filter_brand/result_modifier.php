<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Entity\Base;

$all_breands = [];
$path = $APPLICATION->GetCurPage(false);
if ($path[strlen($path) - 1] == "/") {
    $path = substr($path, 0, -1);
}
$addresses = explode("/", $path);
$elem = $_SESSION["SEARCH_RESULT"]["=ID"];
$CODE = $addresses[count($addresses) - 1];
if ($CODE == "search") {
    $sub_list = CIBlockElement::GetList(["PROPERTY_c"=>"ASC"], $_SESSION["SEARCH_RESULT"], ["PROPERTY_c"]);
    while($arItem = $sub_list->Fetch()) {
        if (!in_array($arItem["PROPERTY_C_VALUE"], $all_breands) && $arItem["PROPERTY_C_VALUE"]) {
            $all_breands[] = $arItem["PROPERTY_C_VALUE"];
        }
    }
}
else {
    if ($GLOBALS['arrFilter']) {
        $sub_list = CIBlockElement::GetList(["PROPERTY_c"=>"ASC"],  $GLOBALS['arrFilter'], ["PROPERTY_c"]);
        while($arItem = $sub_list->Fetch()) {
            if (!in_array($arItem["PROPERTY_C_VALUE"], $all_breands) && $arItem["PROPERTY_C_VALUE"]) {
                $all_breands[] = $arItem["PROPERTY_C_VALUE"];
            }
        }
    }
}
if (count($all_breands) == 0) {
    return;
}
$arResult["BRANDS"] = [];
foreach($arResult["ITEMS"] as $key=>$arItem) {
    foreach ($all_breands as $val => $ar) {
        if($arItem["VALUES"][mb_strtolower($ar)]) {
            $arResult["BRANDS"][] = $arItem["VALUES"][mb_strtolower($ar)];
        }
    }
}
if (count($arResult["BRANDS"]) == 0) {
    return;
}
