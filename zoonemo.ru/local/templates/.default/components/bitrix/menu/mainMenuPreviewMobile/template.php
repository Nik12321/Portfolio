<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
?>
<?php if (count($arResult)) :?>
<?php
for($i = 0; $i < count($arResult); $i++) {
    if ($arResult[$i]["DEPTH_LEVEL"] == 2 &&  $arResult[$i - 1]["DEPTH_LEVEL"] == 1) {
        $arResult[$i - 1]["DEPTH_LEVEL"] = 2;
    }
}
$depthSecond = false;
?>
<ul class="menu" id="menu">
    <?php foreach($arResult as $arItem) :?>
    <?php if ($arItem["DEPTH_LEVEL"] == 1) :?>
    <?php if ($depthSecond) :?>
</ul>
    </li>
<?php $depthSecond = false;?>
<?php endif?>
<?php if($arItem["SELECTED"]) : ?>
    <li><a href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
<?php else:?>
    <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
<?php endif?>
<?php else :?>
<?php if (!$depthSecond) :?>
<?php $depthSecond = true;?>
<li class="slicknav_parent">

    <a href="<?=$arItem["LINK"]?>"  tabindex="0">
        <?=$arItem["TEXT"]?>
    </a>

    <ul class="submenu" role="menu" aria-hidden="true" style="display: none;">
        <?php continue;?>
        <?php endif?>
        <?php if($arItem["SELECTED"]) : ?>
            <li tabindex = "0" role="menuitem"><a href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
        <?php else:?>
            <li tabindex = "0" role="menuitem"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
        <?php endif?>
        <?php endif;?>
        <?php endforeach?>
    </ul>
    <?php endif;?>
