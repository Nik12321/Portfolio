<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
CModule::IncludeModule("iblock");

foreach ($arResult['ITEMS'] as $key => $arItem) {
    $arFilters = [
        [
            "name" => "watermark",
            "position" => "bl",
            'type' => 'text',
            "coefficient" => "5",
            'text' => $arItem["XML_ID"],
            'color' => 'B12A0F',
            "font" => $_SERVER["DOCUMENT_ROOT"] . DEFAULT_TEMPLATE_PATH . "/fonts/OpenSans-Bold.ttf",
            "use_copyright" => "Y"
        ]
    ];
    $arItem["SUBSCRIBE_ID"] = $arItem["ID"];
    $resizedPhoto = CFile::ResizeImageGet(
        $arItem["DETAIL_PICTURE"],
        ["width" => 100000, "height" => 200],
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true,
        $arFilters
    );
    ($resizedPhoto)
        ? $arItem["DETAIL_PICTURE"] = $resizedPhoto["src"]
        : $arItem["DETAIL_PICTURE"] = $arItem["DETAIL_PICTURE"]["SRC"];

    if (count($arItem["OFFERS"]) > 1) {
        $offersIds = [];
        $offers = $arItem["OFFERS"];
        unset($arItem["OFFERS"]);
        foreach ($offers as &$offer) {
            if (!$offer["PROPERTIES"]["name"]["VALUE"]) {
                continue;
            }
            $offersIds[] = $offer["ID"];
            $arItem["OFFERS"][$offer["ID"]]["NAME"] = $offer["PROPERTIES"]["name"]["VALUE"];
            $arItem["OFFERS"][$offer["ID"]]["ID"] = $offer["ID"];
        }
        unset($offer);
        $arItem["offersIds"] = $offersIds;
        $arItem["PRODUCT_MODE"] = "SOME_TP";
    }
    if (count($arItem["OFFERS"]) < 2) {
        if (count($arItem["OFFERS"])) {
            foreach ($arItem["OFFERS"] as &$offer) {
                $arItem["ID"] = $offer["ID"];
                break;
            }
        }
        $arItem["PRODUCT_MODE"] = "SIMPLE_TP";
    }
    $arResult['ITEMS'][$key] = $arItem;
}