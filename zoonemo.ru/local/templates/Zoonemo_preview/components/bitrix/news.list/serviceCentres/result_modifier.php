<?php
foreach ($arResult["ITEMS"] as &$arItem) {
    if ($arItem["PREVIEW_PICTURE"]["ID"]) {
        $file = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]["ID"]);
        $newPhoto = CFile::ResizeImageGet(
            $file,
            ["width"=> 100, "height" => 1000000],
            BX_RESIZE_IMAGE_PROPORTIONAL ,
            false,
            100
        );
        ($newPhoto["src"])
            ? $arItem["SRC"] = $newPhoto["src"]
            : $arItem["SRC"] = $file["SRC"];
    }
}
unset($arItem);