<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="small_grid">
    <div class="container">
        <h2>Сервисные центры</h2>
        <div class="row flex">
            <?php foreach($arResult["ITEMS"] as $arItem) :?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <?php if($arItem["PROPERTIES"]["LINK_TO_ANOTHER_SITE"]) :?>
                    <a href="<?=$arItem["PROPERTIES"]["LINK_TO_ANOTHER_SITE"]["VALUE"];?>" class="item">
                        <?php else : ?>
                        <a href="/service/" class="item">
                            <?php endif;?>
                            <?php
                            $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM"=>GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")));
                            ?>
                            <img class="lazy" data-original="<?=$arItem["SRC"]?>" alt="">
                        </a>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
