<?php
foreach ($arResult["ITEMS"] as &$arItem) {
    if ($arItem["PROPERTIES"]["BACKGROUND_IMG"]["VALUE"]) {
        $file = CFile::GetFileArray($arItem["PROPERTIES"]["BACKGROUND_IMG"]["VALUE"]);
        $newPhoto = CFile::ResizeImageGet(
            $file,
            ["width"=> 9999999, "height" => 600],
            BX_RESIZE_IMAGE_EXACT ,
            false,
            100
        );
        ($newPhoto["src"])
            ? $arItem["SRC"] = $newPhoto["src"]
            : $arItem["SRC"] = $file["SRC"];
    }

    if ($arItem["PROPERTIES"]["IMG"]["VALUE"]) {
        $file = CFile::GetFileArray($arItem["PROPERTIES"]["IMG"]["VALUE"]);
        $newPhoto = CFile::ResizeImageGet(
            $file,
            ["width"=> 999999999, "height" => 350],
            BX_RESIZE_IMAGE_EXACT,
            false,
            90
        );
        ($newPhoto["src"])
            ? $arItem["IMG"]["SRC"] = $newPhoto["src"]
            : $arItem["IMG"]["SRC"] = $file["SRC"];
    }

    if (!$arItem["PROPERTIES"]["LINK"]["VALUE"]) {
        $arItem["PROPERTIES"]["LINK"]["VALUE"] = "#";
    }
}
unset($arItem);