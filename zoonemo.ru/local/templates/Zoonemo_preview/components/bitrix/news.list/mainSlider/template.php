<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div id="slider" class="owl-theme owl-carousel owl-loaded owl-drag">
    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <div class="" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <!--скрипт для редактирования элементов инфоблока и ресайз изображений-->
            <?php
            $this->AddEditAction($arItem["ID"], $arItem["EDIT_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem["ID"], $arItem["DELETE_LINK"], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM"=>GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")));
            ?>
            <div class="owl-lazy"
                <?=($arItem["SRC"]) ? "data-src='" . $arItem["SRC"] ."'" : ""?>
                <?=($arItem["SRC"]) ? 'style="background: center; background-size: auto; background-repeat: no-repeat"' : 'style="background: center; background-size: cover;"'?>
            >
                <?php if ($arItem["SRC"]) : ?>
                <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="item">
                    <?php else :?>
                    <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="item">
                        <?php endif;?>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <!--Аналогично и для изображения на фоне-->
                                    <?php if ($arItem["IMG"]["SRC"]) : ?>
                                        <img src="<?=$arItem["IMG"]["SRC"]?>" alt="">
                                    <?php endif;?>
                                </div>
                                <div class="col-sm-6">
                                    <!--если есть заголовок - выводим-->
                                    <?php if ($arItem["PROPERTIES"]["TITLE"]["VALUE"]["TEXT"]) : ?>
                                        <div class="title"><?=htmlspecialcharsback($arItem["PROPERTIES"]["TITLE"]["VALUE"]["TEXT"])?></div>
                                    <?php endif;?>
                                    <!--если есть подзаголовок - выводим-->
                                    <?php if ($arItem["PROPERTIES"]["SUB_TITLE"]["VALUE"]["TEXT"]) : ?>
                                        <div class="sub_title"><?=htmlspecialcharsback($arItem["PROPERTIES"]["SUB_TITLE"]["VALUE"]["TEXT"])?></div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </a>
            </div>
        </div>
    <?php endforeach; ?>
</div>