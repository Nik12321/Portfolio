<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<ul>
    <?php foreach ($arResult["ITEMS"] as $arItem) : ?>
        <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?php $date = new DateTime($arItem["TIMESTAMP_X"]); ?>
            <span><?= $date->Format('Y-m-d');?></span>
            <a href="news/?ELEMENT_ID=<?=$arItem["ID"];?>"><?=$arItem["NAME"];?></a>
        </li>
    <?php endforeach;?>
</ul>