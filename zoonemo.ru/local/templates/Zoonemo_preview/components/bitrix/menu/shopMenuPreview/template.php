<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
\Bitrix\Main\UI\Extension::load("ui.vue");
\Bitrix\Main\UI\Extension::load("ui.vue.directives.lazyload");
?>
<?php $this->setFrameMode(true); ?>
<?php if (!empty($arResult)) : ?>
    <div class="category_grid mobile-none">
        <div class="container">
            <div class="row">
                <div class="grid_container" id="shopMenuPreviewPC">
                    <?php foreach ($arResult as $arItem) : ?>
                        <div class="item">
                            <a href="<?= $arItem["LINK"] ?>">
                                <div class="svg_cont" style="height : 132px">
                                    <?php if ($arItem['PARAMS']['SVG']) : ?>
                                        <?= $arItem['PARAMS']['SVG'] ?>
                                    <?php elseif ($arItem['PARAMS']['IMG']) : ?>
                                        <div style="display:flex; justify-content: center; align-items: center; height : 132px;  overflow: hidden;">
                                            <img
                                                    v-bx-lazyload
                                                    data-lazyload-src="<?= DEFAULT_TEMPLATE_PATH . $arItem['PARAMS']['IMG']; ?>"
                                                    src="<?= DEFAULT_TEMPLATE_PATH . $arItem['PARAMS']['IMG']; ?>"
                                                    alt=""
                                                    data-lazyload-dont-hide
                                            />
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <span><?= $arItem["TEXT"] ?></span>
                            </a>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
    <div class="category_grid mobile-display">
        <div class="container">
            <div class="row">
                <div class="grid_container">
                    <?php foreach ($arResult as $arItem): ?>
                        <div class="item">
                            <a href="<?= $arItem["LINK"] ?>">
                                <div class="svg_cont mobile_menu" style="height : 132px">
                                    <?php if ($arItem['PARAMS']['SVG']) : ?>
                                        <?= $arItem['PARAMS']['SVG'] ?>
                                    <?php elseif ($arItem['PARAMS']['IMG']) : ?>
                                        <div class="mobile_menu_img lazy"
                                             data-original="<?= DEFAULT_TEMPLATE_PATH . $arItem['PARAMS']['IMG']; ?>"
                                             style="background: center;  background-size: contain; background-repeat: no-repeat"
                                        >
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <span><?= $arItem["TEXT"] ?></span>
                            </a>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<script>
    BX.Vue.create({
        el: '#shopMenuPreviewPC'
    });
</script>
