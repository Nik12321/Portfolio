<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if (!empty($arResult)):?>
    <div class="small_grid">
        <div class="container">
            <div class="row">
                <?php foreach($arResult as $arItem) :?>
                    <div class="col-md-3 col-sm-6 adv_wrap">
                        <a href="<?=$arItem["LINK"]?>" class="item">
                            <?php if (!empty($arItem['PARAMS']['SVG'])) :?>
                                <?=$arItem['PARAMS']['SVG']?>
                            <?php elseif (!empty($arItem['PARAMS']['IMG'])) :?>
                                <div style="display:flex; justify-content: center; align-items: center; height : 132px;  overflow: hidden;">
                                    <img
                                            class="lazy"
                                            data-src="<?=DEFAULT_TEMPLATE_PATH . $arItem['PARAMS']['IMG'];?>"
                                            data-original="<?=DEFAULT_TEMPLATE_PATH . $arItem['PARAMS']['IMG'];?>"
                                            alt=""
                                    >
                                </div>
                            <?php endif;?>
                            <?php $words = explode(" ", $arItem["TEXT"]);?>
                            <?php if(count($words) == 1) :?>
                                <span  style="line-height: 40px;"><?=$arItem["TEXT"]?></span>
                            <?php else :?>
                                <?
                                $index = 0;
                                $result_line = $words[$index];
                                while($index != count($words) - 1) {
                                    $index+=1;
                                    $result_line .= "\n".$words[$index];
                                }
                                ?>
                                <span><pre><?=$result_line?></pre></span>
                            <?php endif;?>
                        </a>
                    </div>
                <?php endforeach?>
            </div>
        </div>
    </div>
<?php endif?>