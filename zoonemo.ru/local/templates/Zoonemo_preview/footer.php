<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Page\Asset;

$code = $APPLICATION->CaptchaGetCode();
?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="/" class="logo">
                    <img src="<?= DEFAULT_TEMPLATE_PATH ?>/img/logo_extend.png" alt="logo">
                </a>
                <a href="/" class="logo mobile-display">
                    <img src="<?= DEFAULT_TEMPLATE_PATH ?>/img/logo.png" alt="logo">
                </a>
            </div>

            <!--Контактные данные для мобильной версии-->
            <div class="mobile-display footer-conts col-xs-12">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="phone" style="margin: 0px">Подтверждение заказов:</p>
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:main.include", "phoneNumberDetail", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/phoneNumberMobile1.php",
                            "COMPONENT_TEMPLATE" => "phoneNumberDetail"
                        ],
                            false
                        );
                        $APPLICATION->IncludeComponent("bitrix:main.include", "emailDetail", [
                            "AREA_FILE_SHOW" => "file",    // Показывать включаемую область
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",    // Шаблон области по умолчанию
                            "PATH" => "/include/emailDetail.php",    // Путь к файлу области
                            "COMPONENT_TEMPLATE" => ".default"
                        ],
                            false
                        );
                        $APPLICATION->IncludeComponent("bitrix:main.include", "skypeDetail", [
                            "AREA_FILE_SHOW" => "file",    // Показывать включаемую область
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",    // Шаблон области по умолчанию
                            "PATH" => "/include/skypeDetail.php",    // Путь к файлу области
                            "COMPONENT_TEMPLATE" => ".default"
                        ],
                            false
                        );
                        $APPLICATION->IncludeComponent("bitrix:main.include", "whatsAppMobile", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/whatsappMobile.php",
                            "COMPONENT_TEMPLATE" => "whatsAppDetail"
                        ],
                            false
                        );
                        ?>
                    </div>
                    <div class="col-xs-6">
                        <p class="phone" style="margin: 0px">Розничные магазины:</p>
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/phoneNumberMobile2.php",
                        ],
                            false
                        );
                        $APPLICATION->IncludeComponent("bitrix:main.include", "detailsMainShedule", [
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "standard.php",
                            "PATH" => "/include/sheduleMobile.php",
                            "COMPONENT_TEMPLATE" => "detailsMainShedule"
                        ],
                            false
                        );
                        ?>
                    </div>
                </div>
            </div>
            <!--Контактные данные для мобильной версии-->

            <!--Меню футера-->
            <div class="col-md-2 col-sm-3 col-xs-4">
                <ul>
                    <h4>Покупателям</h4>
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:menu", "subFooterMenu", [
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "Footer_menu_left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => [],
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "FooterMenuSections",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "subFooterMenu",
                        "COMPOSITE_FRAME_MODE" => "Y",
                        "COMPOSITE_FRAME_TYPE" => "STATIC",
                        "MENU_THEME" => "site"
                    ],
                        false
                    );
                    ?>
                </ul>
            </div>
            <!--Меню футера-->

            <div class="col-md-2 col-md-offset-1 col-sm-3 col-xs-4">
                <ul>
                    <h4>О компании</h4>
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:menu", "subFooterMenu", [
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => [],
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "FooterMenuCompanyInfo",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "subFooterMenu",
                        "COMPOSITE_FRAME_MODE" => "Y",
                        "COMPOSITE_FRAME_TYPE" => "STATIC"
                    ],
                        false
                    );
                    ?>
                </ul>
            </div>
            <div class="col-md-2 col-xs-4 col-sm-3">
                <ul>
                    <h4>Каталог товаров</h4>
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:menu", "subFooterMenu", [
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => [],
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "FooterMenuShop",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "subFooterMenu",
                        "COMPOSITE_FRAME_MODE" => "Y",
                        "COMPOSITE_FRAME_TYPE" => "STATIC"
                    ],
                        false
                    );
                    ?>
                </ul>
            </div>

            <!--Контакты-->
            <div class="col-md-2 col-xs-12">
                <div class="social">
                    <a href="https://ok.ru/group/54293196374271" rel="nofollow" class="fb" target="_blank">
                        <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.instagram.com/zoosalonnemo/" class="gplus" rel="nofollow" target="_blank">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                    <a href="https://vk.com/zoonemoru" class="vk" rel="nofollow" target="_blank">
                        <i class="fa fa-vk" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <!--Контакты-->

        </div>
    </div>
</footer>

<div class="hidden"></div>
<div class="wind_bg"></div>

<!--Вслывающий диалог авторизации-->
<div class="wind_enter wind_sputnik">
    <a href="#" class="close forma-close">
        <svg id="SvgjsSvg1072" xmlns="http://www.w3.org/2000/svg" version="1.1"
             xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25">
            <defs id="SvgjsDefs1073"></defs>
            <path id="SvgjsPath1074"
                  d="M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z "
                  fill="#7e7e7e" fill-opacity="1" transform="matrix(1,0,0,1,-320,-200)"></path>
        </svg>
    </a>
    <div class="page_title">Войти в личный кабинет</div>
    <div class="errors"></div>
    <?php
    $APPLICATION->IncludeComponent("custom:auth.by.phone", ".default", [
        "FORGOT_PASSWORD_URL" => "/forgotPassword/",
        "PROFILE_URL" => "/profile/",
        "COMPONENT_TEMPLATE" => ".default",
        "SHOW_ERRORS" => "N",
        "SERVICE_CODE" => "",
        "CODE_LENGTH" => "8"
    ],
        false
    );
    ?>
</div>
<!--Вслывающий диалог авторизации-->

<!--Всплывающий диалог заказа звонка-->
<div class="wind_zakaz2 wind_sputnik">
    <a href="#" class="close forma-close">
        <svg id="SvgjsSvg1072" xmlns="http://www.w3.org/2000/svg" version="1.1"
             xmlns:xlink="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25">
            <defs id="SvgjsDefs1073"></defs>
            <path id="SvgjsPath1074"
                  d="M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z "
                  fill="#7e7e7e" fill-opacity="1" transform="matrix(1,0,0,1,-320,-200)"></path>
        </svg>
    </a>
    <div class="page_title">Обратный звонок</div>
    <div class="form-info_pzf">Оставьте Ваши контакты и наш менеджер свяжется с Вами в рабочее время с 10-00 до 20-00 с
        понедельника по пятницу.
    </div>
    <div class="form_res1">
        <form class="form_1clk" method="post">
            <input name="name" type="text" placeholder="Имя"/>
            <input name="phoneForm" id="previewGetCallInput" class="phoneCall" type="tel" placeholder="Телефон*"/>
            <div class="captcha">
                <input type="hidden" id="captcha_sid" name="captcha_sid" value="<?= $code; ?>"/>
                <img style="width: 100%" id="CaptchaImage" src="/bitrix/tools/captcha.php?captcha_sid=<?= $code; ?>" alt="CAPTCHA"/>
                <div id="captcha_control"></div>
            </div>
            <input type="text" name="captchaCheck" placeholder="Текст с картинки"/>
            <div class="err"></div>
            <button type="submit" class="s_btn type2">Отправить</button>
        </form>
    </div>
</div>
<!--Всплывающий диалог заказа звонка-->

<a href="javascript://" class="fix-form_pzf oneclick_btn2 b_btn mobile-none"></a>
<div class="overlayWhatsAppVidjet">
    <div class="whatsAppVidjet">
        <a href="https://wa.me/79262166680">
            <svg class="whatsAppVidjetIcon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" class="wh-messenger-svg-whatsapp wh-svg-icon">
                <path d=" M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z" fill-rule="evenodd"></path>
            </svg>
        </a>
    </div>
</div>


<!-- /Yandex.Metrika counter -->
<noscript>
    <div>
        <img src="https://mc.yandex.ru/watch/21199375" style="position:absolute; left:-9999px;" alt=""/>
    </div>
</noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>