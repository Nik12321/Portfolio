<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
if (!CModule::IncludeModule("iblock")) {
    throw new Exception('Не удалось подключить iblock.');
}

if (count($arParams["SHOW_ORDER"])) {
    $newItems = [];
    foreach($arParams["SHOW_ORDER"] as $itemId) {
        $newItems[$itemId] = [];
    }
    foreach ($arResult['ITEMS'] as $key => $arItem) {
        $newItems[$arItem["ID"]] = $arItem;
    }
    $arResult['ITEMS'] = $newItems;
}

foreach ($arResult['ITEMS'] as $key => $arItem) {
    $arItem["DETAIL_PAGE_URL"] = str_replace(["#SECTION_ID#", "#ELEMENT_ID#"], [$arItem["IBLOCK_SECTION_ID"], $arItem["ID"]], $arParams["DETAIL_URL"]);
    $arFilters = [
        [
            "name"=> "watermark",
            "position"=>"bl",
            'type'=>'text',
            "coefficient" => "5",
            'text' => $arItem["XML_ID"],
            'color' => 'B12A0F',
            "font"=> $_SERVER["DOCUMENT_ROOT"]. DEFAULT_TEMPLATE_PATH ."/fonts/OpenSans-Bold.ttf",
            "use_copyright"=>"Y"
        ]
    ];
    $arItem["SUBCRIBE_ID"] =  $arItem["ID"];
    $newPhoto = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], ["width"=> 300, "height" => 300], BX_RESIZE_IMAGE_EXACT, true, $arFilters);
    ($newPhoto) ? $arItem["DETAIL_PICTURE"] = $newPhoto["src"] : $arItem["DETAIL_PICTURE"] = $file["SRC"];
    if (count($arItem["OFFERS"]) > 1) {
        $offersIds = [];
        $offers = $arItem["OFFERS"];
        unset($arItem["OFFERS"]);
        foreach($offers as &$offer) {
            if (!$offer["PROPERTIES"]["name"]["VALUE"])
                continue;
            $offersIds[] = $offer["ID"];
            $arItem["OFFERS"][$offer["ID"]]["NAME"] = $offer["PROPERTIES"]["name"]["VALUE"];
            $arItem["OFFERS"][$offer["ID"]]["ID"] = $offer["ID"];
        }
        $arItem["offersIds"] = $offersIds;
        $arItem["PRODUCT_MODE"] = "SOME_TP";
    }
    if (count($arItem["OFFERS"]) < 2) {
        if (count($arItem["OFFERS"])){
            foreach($arItem["OFFERS"] as &$offer) {
                $arItem["ID"] = $offer["ID"];
                break;
            }
        }
        $arItem["PRODUCT_MODE"] = "SIMPLE_TP";
    }
    $arResult['ITEMS'][$key] = $arItem;
}