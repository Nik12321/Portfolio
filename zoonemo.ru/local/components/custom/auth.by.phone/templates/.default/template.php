<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array('jquery'));

if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
    ShowMessage($arResult['ERROR_MESSAGE']);

?>
<?if($arResult["FORM_TYPE"] == "login"):?>
<form method="post" target="_top" class="auth_by_phone">
    <?if($arResult["BACKURL"] <> ''):?>
        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
    <?endif?>
    <table width="95%">
        <tr>
            <td id="authPhoneContainer" colspan="2">
                <?=GetMessage("AUTH_BY_PHONE_PHONE_NUMBER")?>:<br />
                <input type="tel" id="authPhoneField" maxlength="50" value="" size="20" />
            </td>
        </tr>
        <tr>
            <td id="authCodeContainer" class="not-active" colspan="2">
                <?=GetMessage("AUTH_BY_PHONE_CODE")?>:<br />
                <input type="text" id="authCodeField" maxlength="50" size="17" autocomplete="off" />
            </td>
        </tr>
        <?if ($arResult["CAPTCHA_CODE"]):?>
            <tr>
                <td colspan="2">
                    <?echo GetMessage("AUTH_BY_PHONE_CAPTCHA_PROMT")?>:<br />
                    <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA"/><br/><br/>
                    <input type="text" name="captcha_word" maxlength="50" value="" /></td>
            </tr>
        <?endif?>
        <tr>
            <td colspan="2">
                <input type="hidden" id="authCodeLength" maxlength="50" value="<?=$arResult["CODE_LENGTH"]?>" size="20"/>
                <input type="submit" id="sendCodeForAuth" class="s_btn type2 button-auth-by-phone" name="sendCode" value="<?=GetMessage("AUTH_BY_PHONE_SEND_CODE")?>" />
                <input type="submit" id="confirmCodeForAuth" class="s_btn type2 button-auth-by-phone not-active" name="sendCode" value="<?=GetMessage("AUTH_BY_PHONE_CONFIRM_CODE")?>"/>
                <a type="button" id="returnToStart" class="type2 not-active" href="#"><?=GetMessage("AUTH_BY_PHONE_ANOTHER_PHONE")?></a>
            </td>
        </tr>
        <tr>
            <div class="errors">
        </tr>
    </table>
</form>
<?else:?>
    <form action="<?=$arResult["AUTH_URL"]?>">
        <table width="95%">
            <tr>
                <td align="center">
                    <?=$arResult["USER_NAME"]?><br />
                    [<?=$arResult["USER_LOGIN"]?>]<br />
                    <a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_BY_PHONE_PROFILE")?>"><?=GetMessage("AUTH_BY_PHONE_PROFILE")?></a><br />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="hidden" name="logout" value="yes" />
                    <input type="submit" name="logout_butt" value="<?=GetMessage("AUTH_BY_PHONE_LOGOUT_BUTTON")?>" />
                </td>
            </tr>
        </table>
    </form>
<?endif;?>
<script>
    jQuery(function($){
        $('#authPhoneField').mask('+7 (999) 999-99-99');
        $('#authPhoneField').mask('+7 (999) 999-99-99');
        $('#authPhoneField').on("keyup", function(event) {
            var value = $(event.target).val();
            if (value[4] === "7" && value[0] == "+") {
                $('#authPhoneField').unmask();
                $('#authPhoneField').mask('+7 (999) 999-99-99');
                $('#authPhoneField').focus();
            }
            if (value[4] === "8" && value[0] == "+") {
                $('#authPhoneField').unmask();
                $('#authPhoneField').mask('8 (999) 999-99-99');
                $('#authPhoneField').focus();
            }
            if (value[3] === "7" && value[0] == "8") {
                $('#authPhoneField').unmask();
                $('#authPhoneField').mask('+7 (999) 999-99-99');
                $('#authPhoneField').focus();
            }
            if (value[3] === "8" && value[0] == "8") {
                $('#authPhoneField').unmask();
                $('#authPhoneField').mask('8 (999) 999-99-99');
                $('#authPhoneField').focus();
            }
        });
    });
</script>