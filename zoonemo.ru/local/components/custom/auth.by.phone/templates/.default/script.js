(function() {
    'use strict';

    function сaesar(text, key, decode) {
        var textLetter, keyLetter, result = "", conv = decode ? -1 : 1;
        key = key ? key : " ";
        for (textLetter = keyLetter = 0; textLetter < text.length; textLetter++, keyLetter++) {
            if (keyLetter >= key.length) keyLetter = 0;
            result += String.fromCharCode( text.charCodeAt(textLetter) + conv * key.charCodeAt(keyLetter) );

        }
        return result
    }

    $(document).ready(function(){
        $("body").on("click", "#returnToStart", function(e) {
            $("#returnToStart").addClass("not-active");
            $("#authCodeContainer").addClass("not-active");
            $("#confirmCodeForAuth").addClass("not-active");
            $("#authPhoneContainer").removeClass("not-active");
            $("#sendCodeForAuth").removeClass("not-active");
            $(".auth_by_phone .errors").empty();
        });

        $("body").on("submit", ".auth_by_phone", function(e) {
            $(".auth_by_phone .errors").empty();
            if ($(e.target).find('#sendCodeForAuth').is(":visible")) {
                $("#authPhoneField").blur();
                if (!$("#authPhoneField").val()) {
                    $(".auth_by_phone .errors").append("<p class='timeError' style='color:red'>Введите номер телефона</p>");
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/sendCode.php',
                        data: {phone: $("#authPhoneField").val(), length: $("#authCodeLength").val()},
                        success: function(result)
                        {
                            if (result == "failure") {
                                $(".auth_by_phone .errors").append("<p class='timeError' style='color:red'>Произошла непредвиденная ошибка. Попробуйте перезагрузить страницу или сообщите нам о проблеме!</p>");
                            }
                            else {
                                $("#returnToStart").removeClass("not-active");
                                $("#authCodeContainer").removeClass("not-active");
                                $("#authPhoneContainer").addClass("not-active");
                                $(".auth_by_phone .errors").append("<p class='timeError' style='color:green'>Введите код, высланный по указанному номеру</p>");
                                $("#confirmCodeForAuth").attr("code", сaesar(result.toString(), "125"));
                                $("#confirmCodeForAuth").removeClass("not-active");
                                $("#sendCodeForAuth").addClass("not-active");
                            }
                        }
                    });
                }
            }
            else if ($(e.target).find('#confirmCodeForAuth').is(":visible")) {
                if (!$("#authCodeField").val()) {
                    $(".auth_by_phone .errors").append("<p class='timeError' style='color:red'>Введите высланный код</p>");
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/confirmCode.php',
                        data: {phone: $("#authPhoneField").val(), code: $("#authCodeField").val(), userCode:  сaesar($("#confirmCodeForAuth").attr("code"), "125", true)},
                        success: function(result) {
                            let data = JSON.parse(result);
                            if (data["SUCCESS"] == "Y") {
                                window.location.reload(false);
                            }
                            else {
                                $(".auth_by_phone .errors").append("<p class='timeError' style='color:red'>" + data["MESSAGE"] + "</p>");
                            }
                        }
                    });
                }
            }
            return false;
        });
    });
})();
