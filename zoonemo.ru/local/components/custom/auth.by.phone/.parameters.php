<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

$arComponentParameters = array(
	"PARAMETERS" => array(
		"PROFILE_URL" => array(
			"NAME" => GetMessage("COMP_AUTH_FORM_PROFILE_URL"), 
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		
		"SHOW_ERRORS" => array(
			"NAME" => GetMessage("COMP_AUTH_FORM_SHOW_ERRORS"), 
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),

        "SERVICE_CODE" => array(
            "NAME" => GetMessage("COMP_AUTH_FORM_SERVICE_CODE"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "CODE_LENGTH" => array(
            "NAME" => GetMessage("COMP_AUTH_FORM_CODE_LENGTH"),
            "TYPE" => "NUMBER",
            "DEFAULT" => 4,
        ),
	),
);
?>