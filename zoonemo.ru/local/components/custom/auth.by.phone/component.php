<?php
/**
 * Bitrix Framework
 * @package custom
 * @subpackage main
 * @copyright 2020 Nikita Timofeev
 */

/**
 * Bitrix vars
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParamsToDelete = array(
	"phone",
	"confirm_code",
	"service_code",
);
$currentUrl = $APPLICATION->GetCurPageParam("", $arParamsToDelete);

$arResult["BACKURL"] = $currentUrl;
$arResult['ERROR'] = false;
//Вывод ошибок
$arResult['SHOW_ERRORS'] = (array_key_exists('SHOW_ERRORS', $arParams) && $arParams['SHOW_ERRORS'] == 'Y' ? 'Y' : 'N');
//Длина отправленного кода
$arResult['CODE_LENGTH'] = (array_key_exists('CODE_LENGTH', $arParams) ? $arParams['CODE_LENGTH'] : 4);

//Если пользователь не авторизован, то:
if(!$USER->IsAuthorized()) {
    $arResult["AUTH_URL"] = $APPLICATION->GetCurPageParam("login=yes", array_merge($arParamsToDelete, array("logout_butt", "backurl")));
    $arResult["FORM_TYPE"] = "login";
    $arRes = array();
    foreach($arResult as $key=>$value)
    {
        $arRes[$key] = htmlspecialcharsbx($value);
        $arRes['~'.$key] = $value;
    }
    $arResult = $arRes;
    $loginCookieName = COption::GetOptionString("main", "cookie_name", "BITRIX_SM")."_LOGIN";
    $arResult["~LOGIN_COOKIE_NAME"] = $loginCookieName;
    $arResult["~USER_LOGIN"] = $_COOKIE[$loginCookieName];
    $arResult["USER_LOGIN"] = $arResult["LAST_LOGIN"] = htmlspecialcharsbx($arResult["~USER_LOGIN"]);
    $arResult["~LAST_LOGIN"] = $arResult["~USER_LOGIN"];

    if($APPLICATION->NeedCAPTHAForLogin($arResult["USER_LOGIN"])) {
        $arResult["CAPTCHA_CODE"] = $APPLICATION->CaptchaGetCode();
    }
    else {
        $arResult["CAPTCHA_CODE"] = false;
    }

    if(isset($APPLICATION->arAuthResult) && $APPLICATION->arAuthResult !== true) {
        $arResult['ERROR_MESSAGE'] = $APPLICATION->arAuthResult;
    }
    if($arResult['ERROR_MESSAGE'] <> '') {
        $arResult['ERROR'] = true;
    }
}
else {
    $arResult["FORM_TYPE"] = "logout";
    $arResult["PROFILE_URL"] = $arParams["PROFILE_URL"].(strpos($arParams["PROFILE_URL"], "?") !== false? "&" : "?")."backurl=".urlencode($currentUrl);
    $arRes = array();
    foreach($arResult as $key=>$value)
    {
        $arRes[$key] = htmlspecialcharsbx($value);
        $arRes['~'.$key] = $value;
    }
    $arResult = $arRes;
    $arResult["USER_NAME"] = htmlspecialcharsEx($USER->GetFormattedName(false, false));
    $arResult["USER_LOGIN"] = htmlspecialcharsEx($USER->GetLogin());
}

$this->IncludeComponentTemplate();
