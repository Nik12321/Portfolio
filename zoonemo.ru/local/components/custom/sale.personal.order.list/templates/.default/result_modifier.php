<?
$allOrdersInRightOrder = array();
$allProductsIds = array();
foreach ($arResult["ORDERS"] as $keyOrder => $order) {
    $allOrdersInRightOrder[$order["ORDER"]["ID"]] = $keyOrder;
}
krsort($allOrdersInRightOrder);
$arResult["RIGHT_ORDER"] = $allOrdersInRightOrder;






$arBasketItems = array();
$dbBasketItems = CSaleBasket::GetList(
    array("ORDER_ID" => "DESC"),
    array(
        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID,
        "!ORDER_ID" => "NULL"
    ), false, false, array("PRODUCT_ID")
);
while ($arItems = $dbBasketItems->Fetch())
{

    $productID = $arItems["PRODUCT_ID"]; // ID предложения
    $offerID = CCatalogSku::GetProductInfo($productID);
    if (is_array($offerID)) {
        $allProductsIds[] = $offerID["ID"];
    }
    else {
        $allProductsIds[] = $arItems["PRODUCT_ID"];
    }
}
$allProductsIds = array_unique($allProductsIds);
if (count($allProductsIds)) {
    $filter = array("=ID" => $allProductsIds);
    $rsSection = CIBlockElement::GetList(array(), $filter, array(), false, array('ID', 'NAME'));
    $navPageCount = $rsSection / 18;
    if (!is_integer($navPageCount))
        $navPageCount = intval($navPageCount) + 1;
    $navResult = new CDBResult();
    $navResult->NavPageCount = $navPageCount;  // Общее количество страниц
    if ($_GET["PAGEN_2"])  // Номер текущей страницы
        $navResult->NavPageNomer = $_GET["PAGEN_2"];
    else
        $navResult->NavPageNomer = 1;
    $navResult->NavNum = 2;
    $navResult->NavPageSize = $arParams["PAGE_ELEMENT_COUNT"];
    $navResult->NavRecordCount = $rsSection;

    $correctOrder = array_slice($allProductsIds, ($navResult->NavPageNomer - 1) * 18, 18);
    $arResult["SHOW_ORDER"] = $GLOBALS["order_filter"]["=ID"]  = $correctOrder;
    $arResult["NAV_RESULT_PRODUCT"] = $navResult;
    $arResult["NAV_PAGE_COUNT"] = $navPageCount;
}
