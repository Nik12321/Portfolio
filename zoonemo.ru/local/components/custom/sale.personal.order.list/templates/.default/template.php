<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL']))
{
    foreach($arResult['ERRORS']['FATAL'] as $error)
    {
        ShowError($error);
    }
    $component = $this->__component;
    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
    {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }

}
else
{
    if (!empty($arResult['ERRORS']['NONFATAL']))
    {
        foreach($arResult['ERRORS']['NONFATAL'] as $error)
        {
            ShowError($error);
        }
    }
    if (!count($arResult['ORDERS']))
    {
        if ($_REQUEST["filter_history"] == 'Y')
        {
            if ($_REQUEST["show_canceled"] == 'Y')
            {
                ?>
                <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER')?></h3>
                <?
            }
            else
            {
                ?>
                <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST')?></h3>
                <?
            }
        }
        else
        {
            ?>
            <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST')?></h3>
            <?
        }
    }
    else {
        ?>
        <div class="row">
            <div class="col-md-12">
                <ul class="menu tabs_menu">
                    <li><a href="#buyBefore" class="open">Покупали ранее</a></li>
                    <li><a href="#orderList">Список заказов</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="tabs" class="tabs">
                    <div class="orderList">
                        <div class="row col-md-12 col-sm-12">
                            <?
                            $nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
                            $clearFromLink = array("filter_history","filter_status","show_all", "show_canceled");
                            ?>
                        </div>
                        <?
                        if (!count($arResult['ORDERS']))
                        {
                            ?>
                            <div class="row col-md-12 col-sm-12">
                                <a href="<?=htmlspecialcharsbx($arParams['PATH_TO_CATALOG'])?>" class="sale-order-history-link">
                                    <?=Loc::getMessage('SPOL_TPL_LINK_TO_CATALOG')?>
                                </a>
                            </div>
                            <?
                        }

                        if ($_REQUEST["filter_history"] !== 'Y')
                        {
                            $paymentChangeData = array();
                            $orderHeaderStatus = null;

                            foreach ($arResult["RIGHT_ORDER"]  as $key) {
                                $order = $arResult['ORDERS'][$key];
                                $orderHeaderStatus = $order['ORDER']['STATUS_ID'];
                                if ($order["ORDER"]["CANCELED"] == "N" && $arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'] != "Выполнен") {
                                    ?>
                                    <div class="col-md-12 col-sm-12 sale-order-list-container">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 sale-order-list-title-container">
                                                <h2 class="sale-order-list-title">
                                                    <?=Loc::getMessage('SPOL_TPL_ORDER')?>
                                                    <?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
                                                    <?=Loc::getMessage('SPOL_TPL_FROM_DATE')?>
                                                    <?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?>,
                                                    <?=count($order['BASKET_ITEMS']);?>
                                                    <?
                                                    $count = count($order['BASKET_ITEMS']) % 10;
                                                    if ($count == '1')
                                                    {
                                                        echo Loc::getMessage('SPOL_TPL_GOOD');
                                                    }
                                                    elseif ($count >= '2' && $count <= '4')
                                                    {
                                                        echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
                                                    }
                                                    else
                                                    {
                                                        echo Loc::getMessage('SPOL_TPL_GOODS');
                                                    }
                                                    ?>
                                                    <?=Loc::getMessage('SPOL_TPL_SUMOF')?>
                                                    <?=$order['ORDER']['FORMATED_PRICE']?>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 sale-order-list-inner-container" style="padding-top: 15px;">
                                                <div class="sale-order-list-shipment-status">
                                                    <span class="sale-order-list-shipment-status-item">Статус заказа:</span>
                                                    <span class="sale-order-list-shipment-status-block"><?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'])?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 sale-order-list-inner-container">
                                                <?
                                                $showDelimeter = false;
                                                foreach ($order['PAYMENT'] as $payment)
                                                {
                                                    if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
                                                    {
                                                        $paymentChangeData[$payment['ACCOUNT_NUMBER']] = array(
                                                            "order" => htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']),
                                                            "payment" => htmlspecialcharsbx($payment['ACCOUNT_NUMBER']),
                                                            "allow_inner" => $arParams['ALLOW_INNER'],
                                                            "refresh_prices" => $arParams['REFRESH_PRICES'],
                                                            "path_to_payment" => $arParams['PATH_TO_PAYMENT'],
                                                            "only_inner_full" => $arParams['ONLY_INNER_FULL']
                                                        );
                                                    }
                                                    ?>
                                                    <div class="row sale-order-list-inner-row">
                                                        <?
                                                        if ($showDelimeter)
                                                        {
                                                            ?>
                                                            <div class="sale-order-list-top-border"></div>
                                                            <?
                                                        }
                                                        else
                                                        {
                                                            $showDelimeter = true;
                                                        }
                                                        ?>

                                                        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 sale-order-list-inner-row-template">
                                                            <a class="sale-order-list-cancel-payment">
                                                                <i class="fa fa-long-arrow-left"></i> <?=Loc::getMessage('SPOL_CANCEL_PAYMENT')?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <?
                                                }
                                                ?>
                                                <div class="row sale-order-list-inner-row">
                                                    <div class="col-md-<?=($order['ORDER']['CAN_CANCEL'] !== 'N') ? 8 : 10?>  col-sm-12 sale-order-list-about-container">
                                                        <a class="sale-order-list-about-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>"><?=Loc::getMessage('SPOL_TPL_MORE_ON_ORDER')?></a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-12 sale-order-list-repeat-container">
                                                        <a class="sale-order-list-repeat-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>"><?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?></a>
                                                    </div>
                                                    <?
                                                    if ($order['ORDER']['CAN_CANCEL'] !== 'N')
                                                    {
                                                        ?>
                                                        <div class="col-md-2 col-sm-12 sale-order-list-cancel-container">
                                                            <a class="sale-order-list-cancel-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>"><?=Loc::getMessage('SPOL_TPL_CANCEL_ORDER')?></a>
                                                        </div>
                                                        <?
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                }
                                else {
                                    ?>
                                    <div class="col-md-12 col-sm-12 sale-order-list-container">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 sale-order-list-accomplished-title-container">
                                                <div class="row">
                                                    <div class="col-md-8 col-sm-12 sale-order-list-accomplished-title-container">
                                                        <h2 class="sale-order-list-accomplished-title">
                                                            <?= Loc::getMessage('SPOL_TPL_ORDER') ?>
                                                            <?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') ?>
                                                            <?= htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER'])?>
                                                            <?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
                                                            <?= $order['ORDER']['DATE_INSERT'] ?>,
                                                            <?= count($order['BASKET_ITEMS']); ?>
                                                            <?
                                                            $count = substr(count($order['BASKET_ITEMS']), -1);
                                                            if ($count == '1')
                                                            {
                                                                echo Loc::getMessage('SPOL_TPL_GOOD');
                                                            }
                                                            elseif ($count >= '2' || $count <= '4')
                                                            {
                                                                echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
                                                            }
                                                            else
                                                            {
                                                                echo Loc::getMessage('SPOL_TPL_GOODS');
                                                            }
                                                            ?>
                                                            <?= Loc::getMessage('SPOL_TPL_SUMOF') ?>
                                                            <?= $order['ORDER']['FORMATED_PRICE'] ?>
                                                        </h2>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 sale-order-list-accomplished-date-container">
                                                        <?
                                                        if ($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'] == "Выполнен")
                                                        {
                                                            ?>
                                                            <span class="sale-order-list-accomplished-date">
										<?= Loc::getMessage('SPOL_TPL_ORDER_FINISHED')?>
									</span>
                                                            <?
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                            <span class="sale-order-list-accomplished-date canceled-order">
										<?= Loc::getMessage('SPOL_TPL_ORDER_CANCELED')?>
									</span>
                                                            <?
                                                        }
                                                        ?>
                                                        <span class="sale-order-list-accomplished-date-number"><?= $order['ORDER']['DATE_STATUS_FORMATED'] ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 sale-order-list-inner-accomplished">
                                                <div class="row sale-order-list-inner-row">
                                                    <div class="col-md-3 col-sm-12 sale-order-list-about-accomplished">
                                                        <a class="sale-order-list-about-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>">
                                                            <?=Loc::getMessage('SPOL_TPL_MORE_ON_ORDER')?>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-3 col-md-offset-6 col-sm-12 sale-order-list-repeat-accomplished">
                                                        <a class="sale-order-list-repeat-link sale-order-link-accomplished" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>">
                                                            <?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                }
                                ?>
                                <?
                            }
                        }
                        ?>
                        <div class="pagination">
                            <? echo $arResult["NAV_STRING"];?>
                        </div>
                    </div>
                    <div class="buyBefore open">
                        <h1 class="page_title test" style="padding-top:30px">
                            Товары из предыдущих заказов:</h1>
                        <?
                        $intSectionID = $APPLICATION->IncludeComponent(
                            "custom:catalog.section",
                            ".default",
                            array(
                                "ACTION_VARIABLE" => "action",
                                "ADD_PICT_PROP" => "-",
                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "ADD_TO_BASKET_ACTION" => "ADD",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "BACKGROUND_IMAGE" => "-",
                                "BASKET_URL" => "/personal/basket.php",
                                "BROWSER_TITLE" => "-",
                                "BY_LINK" => "Y",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "N",
                                "COMPATIBLE_MODE" => "Y",
                                "CONVERT_CURRENCY" => "N",
                                "CUSTOM_FILTER" => "",
                                "DETAIL_URL" => "#SITE_DIR#/shop/products/#SECTION_ID#/#ELEMENT_ID#",
                                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "DISPLAY_COMPARE" => "N",
                                "DISPLAY_TOP_PAGER" => "Y",
                                "ELEMENT_SORT_FIELD" => "property_status",
                                "ELEMENT_SORT_ORDER" => "DESC",
                                "ENLARGE_PRODUCT" => "STRICT",
                                "FILTER_NAME" => "order_filter",
                                "HIDE_NOT_AVAILABLE" => "L",
                                "HIDE_NOT_AVAILABLE_OFFERS" => "L",
                                "IBLOCK_ID" => "13",
                                "IBLOCK_TYPE" => "Catalogs",
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "LABEL_PROP" => array(
                                ),
                                "LAZY_LOAD" => "N",
                                "LINE_ELEMENT_COUNT" => "3",
                                "LOAD_ON_SCROLL" => "N",
                                "MESSAGE_404" => "",
                                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                "MESS_BTN_BUY" => "Купить",
                                "MESS_BTN_DETAIL" => "Подробнее",
                                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                "META_DESCRIPTION" => "-",
                                "META_KEYWORDS" => "-",
                                "OFFERS_FIELD_CODE" => array(
                                    0 => "",
                                    1 => "",
                                ),
                                "OFFERS_LIMIT" => "15",
                                "OFFERS_SORT_FIELD" => "sort",
                                "OFFERS_SORT_FIELD2" => "id",
                                "OFFERS_SORT_ORDER" => "asc",
                                "OFFERS_SORT_ORDER2" => "desc",
                                "OFFER_ADD_PICT_PROP" => "-",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => "customPagination",
                                "PAGER_TITLE" => "Товары",
                                "PAGE_ELEMENT_COUNT" => "18",
                                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                "PRICE_CODE" => array(
                                    0 => "cost",
                                ),
                                "PRICE_VAT_INCLUDE" => "Y",
                                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                                "PRODUCT_DISPLAY_MODE" => "Y",
                                "PRODUCT_ID_VARIABLE" => "id",
                                "PRODUCT_PROPS_VARIABLE" => "prop",
                                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                                "PRODUCT_SUBSCRIPTION" => "Y",
                                "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                                "RCM_TYPE" => "personal",
                                "SECTION_ID" => "1",
                                "SECTION_URL" => "",
                                "SEF_MODE" => "N",
                                "SEF_RULE" => "",
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "Y",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "Y",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SHOW_CLOSE_POPUP" => "N",
                                "SHOW_DISCOUNT_PERCENT" => "N",
                                "SHOW_FROM_SECTION" => "N",
                                "SHOW_MAX_QUANTITY" => "N",
                                "SHOW_OLD_PRICE" => "N",
                                "SHOW_PRICE_COUNT" => "1",
                                "SHOW_SLIDER" => "Y",
                                "SLIDER_INTERVAL" => "3000",
                                "SLIDER_PROGRESS" => "Y",
                                "TEMPLATE_THEME" => "blue",
                                "USE_ENHANCED_ECOMMERCE" => "N",
                                "USE_MAIN_ELEMENT_SECTION" => "Y",
                                "USE_PRICE_COUNT" => "N",
                                "USE_PRODUCT_QUANTITY" => "N",
                                "COMPONENT_TEMPLATE" => ".default",
                                "SHOW_ORDER" => $arResult["SHOW_ORDER"]
                            ),
                            false
                        );
                        ?>
                        <div style="clear: both; height: 0;"></div>
                        <div class="pagination">
                            <?
                            if ($arResult["NAV_PAGE_COUNT"] > 1)
                                $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', "customPagination", array(
                                    'NAV_RESULT' => $arResult["NAV_RESULT_PRODUCT"],
                                ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?
        if ($_REQUEST["filter_history"] !== 'Y')
        {
            $javascriptParams = array(
                "url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
                "templateFolder" => CUtil::JSEscape($templateFolder),
                "templateName" => $this->__component->GetTemplateName(),
                "paymentList" => $paymentChangeData
            );
            $javascriptParams = CUtil::PhpToJSObject($javascriptParams);
            ?>
            <script>
                BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
            </script>
            <?
        }
    }
}
?>
