<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Полезная информация");
$APPLICATION->SetTitle("Аквариумы");
$APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "inc",
    "EDIT_TEMPLATE" => "standard.php",
    "PATH" => "/include/aquariums.php",
    "COMPONENT_TEMPLATE" => ".default"
],
    false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
