<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Скидки");
$APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "standard.php",
		"PATH" => "/include/actions.php",
		"COMPONENT_TEMPLATE" => ".default"
	],
	false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");