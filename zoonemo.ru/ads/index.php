<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Объявления");
CModule::IncludeModule("iblock");
$code = $APPLICATION->CaptchaGetCode();
$filter = ['IBLOCK_CODE' => 'ads', "ACTIVE" => "Y", "ACTIVE_DATE"=>"Y", "PROPERTY_CONFIRMATION" => "23"];
$cnt = CIBlockElement::GetList([], $filter, [], false, ['ID', 'NAME']);
$navPageCount = $cnt / 10;
if (!is_integer($navPageCount)) {
    $navPageCount = intval($navPageCount) + 1;
}
$navResult = new CDBResult();
$navResult->NavPageCount = $navPageCount;  // Общее количество страниц
if ($_GET["PAGEN_1"]) {
    $navResult->NavPageNomer = $_GET["PAGEN_1"];
} else {
    $navResult->NavPageNomer = 1;
}
$navResult->NavNum = 1; // Номер пагинатора. Используется для формирования ссылок на страницы
$navResult->NavPageSize = 10;   // Количество записей выводимых на одной странице
$navResult->NavRecordCount = $cnt; // Общее количество записей  PAGE_ELEMENT_COUNT
?>
    <h1 class="page_title">Объявления</h1>
    <div class="faq_form res1">
        <div class="">
            <a href="#" class="b_btn collapse">Развернуть форму</a>
            <p>Вы можете разместить свое объявление. Мы постараемся рассмотреть его как можно быстрее и опубликовать.</p>
        </div>
        <form   action="/sendAds.php" enctype='multipart/form-data'  method="POST" class="collapsed ads" id="collapse">
            <div class="row">
                <div class="col-md-6 col-md-push-6">
                    <label for="name">Ваше имя</label>
                    <input type="text" name="name" value="" />
                    <label for="email">Ваш Email: (Не публикуется)</label>
                    <input type="text" name="email" value="" />
                    <input id="file" type="file" name="file">
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <label for="text">Объявление</label>
                    <textarea name="text" rows="13"></textarea>
                    <input type="text" name="captcha" placeholder="Текст с картинки" />
                    <div class="captcha">
                        <input type="hidden" id="captcha_sid" name="captcha_sid" value="<?=$code;?>" />
                        <img id="CaptchaImage" src="/bitrix/tools/captcha.php?captcha_sid=<?=$code;?>" alt="CAPTCHA" />
                        <div id="captcha_control"></div>
                    </div>
                    <input type="submit" value="Добавить" class="s_btn type2" />
                </div>
                <div class="clear"></div>
            </div>
        </form>
    </div>
    <h2 class="page_title">Объявления</h2>
<?php
$GLOBALS["arrFilter"] = ["PROPERTY_CONFIRMATION" => "23"];
$APPLICATION->IncludeComponent("bitrix:news.list", "ads", [
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "ADD_SECTIONS_CHAIN" => "Y",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "CACHE_TIME" => "36000000",
    "CACHE_TYPE" => "N",
    "CHECK_DATES" => "Y",
    "DETAIL_URL" => "",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "DISPLAY_TOP_PAGER" => "N",
    "FIELD_CODE" => [
        0 => "",
        1 => "",
    ],
    "FILTER_NAME" => "arrFilter",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "IBLOCK_ID" => "16",
    "IBLOCK_TYPE" => "Contact_with_user",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
    "INCLUDE_SUBSECTIONS" => "Y",
    "MESSAGE_404" => "",
    "NEWS_COUNT" => "10",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_TEMPLATE" => ".default",
    "PAGER_TITLE" => "Новости",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "PREVIEW_TRUNCATE_LEN" => "",
    "PROPERTY_CODE" => [
        0 => "NAME",
        1 => "",
    ],
    "SET_BROWSER_TITLE" => "Y",
    "SET_LAST_MODIFIED" => "N",
    "SET_META_DESCRIPTION" => "Y",
    "SET_META_KEYWORDS" => "Y",
    "SET_STATUS_404" => "N",
    "SET_TITLE" => "Y",
    "SHOW_404" => "N",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_BY2" => "SORT",
    "SORT_ORDER1" => "DESC",
    "SORT_ORDER2" => "ASC",
    "STRICT_SECTION_CHECK" => "N",
    "COMPONENT_TEMPLATE" => "ads"
],
    false
);
?>
    <br>
    <div class="pagination">
        <?php
        if ($navPageCount > 1) {
            $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', $arParams["PAGER_TEMPLATE"], ['NAV_RESULT' => $navResult]);
        }
        ?>
    </div>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>