<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Акции");
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Entity;
$GLOBALS["arrFilter"][">PROPERTY_status"] = "-2";
$sale_id = array();
$res = AllProductDiscount::getFull(
    array("ACTIVE" => "Y", "SITE_ID" => SITE_ID),
    array()
);
foreach($res['IDS'] as $ID) {
    $sale_id[] = $ID;
}
$GLOBALS['arrFilter'] = array("ID"=>$sale_id);
if (count($sale_id) > 0) {
    session_start();
    if (empty($_GET["sort"]) && empty($_SESSION["sort"])) {
        $sort = "name";
    }
    else if (empty($_GET["sort"])) {
        $sort = $_SESSION["sort"];
    }
    else {
        $sort = $_GET["sort"];
        $_SESSION["sort"] = $sort;
    }
    if (empty($_GET["order"]) && empty($_SESSION["order"])) {
        $order = "asc";
    }
    else if (empty($_GET["order"])) {
        $order = $_SESSION["order"];
    }
    else {
        $order = $_GET["order"];
        $_SESSION["order"] = $order;
    }
    if ($sort == "price")
        $arParams["ELEMENT_SORT_FIELD"] = CATALOG_PRICE_SCALE_1;
    else if ($sort == "name")
        $arParams["ELEMENT_SORT_FIELD"] = "name";
    $arParams["ELEMENT_SORT_ORDER"] = $order;
    ?>
    <h1 class="page_title">Каталог - Скидки</h1>
    <div class="disableTextSelect">
        <br><h2><span style="color: #ff0000;">Скидка распространяется на товары со статусом "В НАЛИЧИИ". Если товар со статусом "ПОД ЗАКАЗ", то окончательная цена требует подтверждения у оператора интернет-магазина.</span></h2>
        <p><br> <br></p>
    </div>
    <div id="catalog_ajax_container">
        <div class="<?=($isSidebar ? "col-md-9 col-sm-8" : "col-xs-12")?>">
            <?
            $APPLICATION->IncludeComponent("kombox:filter", "filterBrand", Array(
                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                "CACHE_TYPE" => "N",	// Тип кеширования
                "CLOSED_OFFERS_PROPERTY_CODE" => array(	// Свойства предложений, которые будут свернуты
                    0 => "",
                    1 => "",
                ),
                "CLOSED_PROPERTY_CODE" => array(	// Свойства, которые будут свернуты
                    0 => "",
                    1 => "",
                ),
                "CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
                "FIELDS" => "",	// Показывать дополнительные поля в фильтре
                "FILTER_NAME" => "arrFilter",	// Имя выходящего массива для фильтрации
                "HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
                "IBLOCK_ID" => "13",	// Инфоблок
                "IBLOCK_TYPE" => "Catalogs",	// Тип инфоблока
                "INCLUDE_JQUERY" => "N",	// Подключить библиотеку jQuery
                "IS_SEF" => "N",	// Включить
                "MESSAGE_ALIGN" => "LEFT",	// Выводить сообщение с количеством найденных элементов
                "MESSAGE_TIME" => "5",	// Через сколько секунд скрывать сообщение (0 - не скрывать)
                "PAGER_PARAMS_NAME" => "arrPager",	// Имя массива с переменными для построения ссылок в постраничной навигации
                "PAGE_URL" => "",	// Путь к разделу (если фильтр располагается на другой странице)
                "PRICE_CODE" => "",	// Тип цены
                "SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
                "SECTION_CODE" => "",	// Символьный код раздела инфоблока
                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],	// ID раздела инфоблока
                "SORT" => "N",	// Сортировать поля фильтра
                "STORES_ID" => "",	// Выводить товары со складов (по умолчанию - все склады)
                "XML_EXPORT" => "N",	// Включить поддержку Яндекс Островов
                "COMPONENT_TEMPLATE" => ".default"
            ),
                false
            );
            global $arrFilter;
            $filter = $GLOBALS["arrFilter"];


            $totalElement = CIBlockElement::GetList(
                array(),
                array($filter),
                array(),
                false,
                array('ID', 'NAME')
            );
            $navPageCount = $totalElement / 18;
            if (!is_integer($navPageCount))
                $navPageCount = intval($navPageCount) + 1;
            $navResult = new CDBResult();
            $navResult->NavPageCount = $navPageCount;  // Общее количество страниц
            if ($_GET["PAGEN_1"])  // Номер текущей страницы
                $navResult->NavPageNomer = $_GET["PAGEN_1"];
            else
                $navResult->NavPageNomer = 1;
            $navResult->NavNum = 1; // Номер пагинатора. Используется для формирования ссылок на страницы
            $navResult->NavPageSize = $arParams["PAGE_ELEMENT_COUNT"];   // Количество записей выводимых на одной странице
            // Общее количество записей  PAGE_ELEMENT_COUNT
            $navResult->NavRecordCount = $totalElement;
            ?>
            <div class="sort_block">
                <form class="sort_form">
                    <span class="title">Сортировать по</span>

                    <?if($sort == "name")
                        $title = "Названию";
                    else if ($order == "asc")
                        $title = "Цене ↑";
                    else
                        $title = "Цене ↓";
                    ?>
                    <select class="sortby" name="sort" onChange="window.location.href=this.value" style="margin: 0px; padding: 0px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0;">
                        <?if( $title == "Названию") : ?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=name&order=asc'?>"  selected="true">Названию</option>
                        <?else:?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=name&order=asc'?>">Названию</option>
                        <?endif;?>
                        <?if( $title == "Цене ↑") : ?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=price&order=asc'?>"  selected="true">Цене ↑</option>
                        <?else:?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=price&order=asc'?>">Цене ↑</option>
                        <?endif;?>
                        <?if( $title == "Цене ↓") : ?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=price&order=desc'?>"  selected="true">Цене ↓</option>
                        <?else:?>
                            <option value="<?=$_SERVER["SCRIPT_URL"] . '?sort=price&order=desc'?>">Цене ↓</option>
                        <?endif;?>
                    </select>
                </form>
            </div>
            <div class="pagination">
                <?
                if ($navPageCount > 1)
                    $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', "customPagination", array(
                        'NAV_RESULT' => $navResult,
                    ));
                ?>
            </div>
        </div>
        <?  $intSectionID = $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            ".default",
            array(
                "ACTION_VARIABLE" => "action",
                "ADD_PICT_PROP" => "-",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "ADD_TO_BASKET_ACTION" => "ADD",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BACKGROUND_IMAGE" => "-",
                "BASKET_URL" => "/personal/basket.php",
                "BROWSER_TITLE" => "-",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "N",
                "COMPATIBLE_MODE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "CUSTOM_FILTER" => "",
                "DETAIL_URL" => "",
                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_COMPARE" => "N",
                "DISPLAY_TOP_PAGER" => "Y",
                "ELEMENT_SORT_FIELD" => "property_STATUS_GROUP",
                "ELEMENT_SORT_ORDER" => "DESC",
                "ELEMENT_SORT_FIELD2" =>  $arParams["ELEMENT_SORT_FIELD"],
                "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER"],
                "ENLARGE_PRODUCT" => "STRICT",
                "FILTER_NAME" => "arrFilter",
                "HIDE_NOT_AVAILABLE" => "N",
                "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                "IBLOCK_ID" => "13",
                "IBLOCK_TYPE" => "Catalogs",
                "INCLUDE_SUBSECTIONS" => "A",
                "LABEL_PROP" => array(
                ),
                "LAZY_LOAD" => "N",
                "LINE_ELEMENT_COUNT" => "3",
                "LOAD_ON_SCROLL" => "N",
                "MESSAGE_404" => "",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "META_DESCRIPTION" => "-",
                "META_KEYWORDS" => "-",
                "OFFERS_FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "OFFERS_LIMIT" => "5",
                "OFFERS_SORT_FIELD" => "sort",
                "OFFERS_SORT_FIELD2" => "id",
                "OFFERS_SORT_ORDER" => "asc",
                "OFFERS_SORT_ORDER2" => "desc",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Товары",
                "PAGE_ELEMENT_COUNT" => "18",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRICE_CODE" => array(
                    0 => "cost",
                ),
                "PRICE_VAT_INCLUDE" => "Y",
                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                "PRODUCT_DISPLAY_MODE" => "N",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                "PRODUCT_SUBSCRIPTION" => "Y",
                "PROPERTY_CODE_MOBILE" => array(
                ),
                "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                "RCM_TYPE" => "personal",
                "SECTION_CODE" => "",
                "SECTION_ID" => $_REQUEST["SECTION_ID"],
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SEF_MODE" => "N",
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "Y",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SHOW_ALL_WO_SECTION" => "Y",
                "SHOW_CLOSE_POPUP" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_FROM_SECTION" => "N",
                "SHOW_MAX_QUANTITY" => "N",
                "SHOW_OLD_PRICE" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "SHOW_SLIDER" => "Y",
                "SLIDER_INTERVAL" => "3000",
                "SLIDER_PROGRESS" => "N",
                "TEMPLATE_THEME" => "blue",
                "USE_ENHANCED_ECOMMERCE" => "N",
                "USE_MAIN_ELEMENT_SECTION" => "N",
                "USE_PRICE_COUNT" => "N",
                "USE_PRODUCT_QUANTITY" => "N",
                "COMPONENT_TEMPLATE" => ".default"
            ),
            false
        );
        ?>
        <div style="clear: both; height: 0;"></div>
        <div class="pagination">
            <?
            if ($navPageCount > 1)
                $APPLICATION->IncludeComponent('bitrix:system.pagenavigation', "customPagination", array(
                    'NAV_RESULT' => $navResult,
                ));
            ?>
        </div>
    </div>
    <?
}
else {
    ?>
    <div class="disableTextSelect">
        <br><h2><span style="color: #ff0000;">Акционных товаров временно нет.</span></h2>
        <p><br> <br></p>
    </div>
    <?
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>