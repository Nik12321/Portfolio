<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>

    <form method="post" class="basket_form form">
        <input type="hidden" id="cart-delete-product">
        <div class="user_cart">
            <h1 class="page_title" style="margin-top: 0px;">Корзина</h1>
            <?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	"cart", 
	array(
		"ACTION_VARIABLE" => "basketAction",
		"ADDITIONAL_PICT_PROP_11" => "-",
		"ADDITIONAL_PICT_PROP_12" => "-",
		"ADDITIONAL_PICT_PROP_13" => "-",
		"ADDITIONAL_PICT_PROP_14" => "-",
		"AUTO_CALCULATION" => "Y",
		"BASKET_IMAGES_SCALING" => "adaptive",
		"COLUMNS_LIST_EXT" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DELETE",
			2 => "SUM",
			3 => "PROPERTY_ARTNUMBER",
		),
		"COLUMNS_LIST_MOBILE" => array(
		),
		"COMPATIBLE_MODE" => "Y",
		"CORRECT_RATIO" => "Y",
		"DEFERRED_REFRESH" => "N",
		"DISCOUNT_PERCENT_POSITION" => "bottom-right",
		"DISPLAY_MODE" => "extended",
		"EMPTY_BASKET_HINT_PATH" => "/shop/",
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_CONVERT_CURRENCY" => "N",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_PLACE" => "BOTTOM",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "N",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"HIDE_COUPON" => "Y",
		"LABEL_PROP" => array(
		),
		"OFFERS_PROPS" => array(
		),
		"PATH_TO_ORDER" => "/shop/cart/order",
		"PRICE_DISPLAY_MODE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_BLOCKS_ORDER" => "props,sku,columns",
		"QUANTITY_FLOAT" => "Y",
		"SET_TITLE" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_FILTER" => "Y",
		"SHOW_RESTORE" => "Y",
		"TEMPLATE_THEME" => "blue",
		"TOTAL_BLOCK_DISPLAY" => array(
			0 => "bottom",
		),
		"USE_DYNAMIC_SCROLL" => "Y",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_GIFTS" => "Y",
		"USE_PREPAYMENT" => "N",
		"USE_PRICE_ANIMATION" => "Y",
		"COMPONENT_TEMPLATE" => "cart"
	),
	false
);?><br>

            <!---->
            <!--            <div class="delivery">-->
            <!--                <h3>Выберите способ доставки</h3>-->
            <!--                <ul class="tabs_menu custom_chekbox menu">-->
            <!--                    <li>-->
            <!---->
            <!--                        <input name="delivery" id="courier" type="radio" value="1">-->
            <!--                        <label for="courier">-->
            <!--                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAABECAYAAADX0fiMAAAOFElEQVR4Xu2ce3RUxR3Hd+4mIUSrWKza2mpAWl+tlarHF5AXRUr1ILZI7bEaEfFZpYe8EK2LiiSbpPI4VNCq2NbT+kcraq2iJAsqRa1VPK3WijbRtuf4wAIKeZm9089v772bu6/svZuEgHTOWXYz987Mb77zm99rfoMKDHGZO/el/IMP3l0cDPZ+XevAt7RWZyqlD+L3gUqpkQxfnEwCz/5GXb5SgQ8DAb2dNluoazGMgraGhgnvDjHJATVUA8yfv/6EYDB4HgBM1VqPAYCjB2GsN+jjNYBaF43mrW9qKmkbhD5TuhhUUIQrRo36+DsQfRkgnJyOCwZrEgD990BAPR8MBlYvWVL+wmD1K/0MGig1NZHz4YprYfPTYfvPZSDyVSaz1npmbDGMwA75VV9ftkG+582LjCosDAiYFLPYNAPF9DUKMkup+Gb6PvX71L+olLGcftYPBjgDBqWubuN40+xthvhTIP6gRKL0Tuo2ANbari5j7dKlZTEQcil1dRFAMgE+BtD05D5YjE+oe9Q0gzcPdFvlDEoo9FpBZ+d7N0PIFUz8cDeRELiRuqXhcJnNFbnAkLmNxVHm+XBSKFlWybZige5qaKhYkeuoOYFSU9N6LAP/gkEnJIKh32FLhOrrK9bkSpDfdjU1LQATmMciHNzXVn/M7z/29ho/bm4u2+a3T9+gVFe3TmPiKxmo2EXETiFsT4LhnqhwTkGBOS8ZHDj2eYT+DeFwxYt+gPEFChxyPfLhtiTZ8Uh3t6ociLzwQ3B/74rcsQV5XCgDTJth6DksWKvXcTyDgna5HNTvTNQsehl7F9bde4pwzYgRWrZvXBgDzNvInsqGhrLnvFDqCRQ4pJrObnYDAsdcNlzbxcvEamtblsLRN7jebef3VQ0N5euytc8KSm1tBNtDL+kDRO80TaO0sbFsS7bOh/t5XV1LJS7C/Q4dspW0Ni5tbCx9tj/a+gUFDqkAjF/RwRetTvYdQJxJ20blwy4QXmMrnYuhJ5yTtmQEZeHCliN7ewOPwYLj+5BWM4bK9hhKrkJto5nUnX3z0JFwuLyCuel042YEhY4eE0RdHf0E1cY+3TcLXL8Grr+0j3pdj5JY4BkUOphLB6tde/EBkK3cN+Hoo7q2tlXkoK2u9fv4Sxen85dSOMXW9U/Q+LiYFMFkB5DSfR0Qod828rY4roHEaIqKDp8WCp3Y455fCihwyUq45BrnJToY059QygZWVdX6k4JBYyHwimzqor/4PkarMb4yqe/mI9/pikHlCNobyW3lbybWxbN8+omibk/LRk+y4DVNfUVjY4W4LPGSAEp1deRkpcx1DHaYzSUD2jb0d4RhmDhm6vv018snysc9pvN3AfUy+UzlUxs0d1sBUfos5JMnDQElq4kh77HweO6BEnuOz5vmiOlNTWd/4Aye0AkGD+pXXWw91Du7u43iXM13G5C76O98OlvNkt6HjdATDEZZVav09uZvy8szL+RZFX8emgYR4aoIvlYzIYF/KfWpABBAFhham4QqjfFwyu1UHSvVXkFBRJQyZqRvPIVRVxaXoXFQFiyIHEe8YoMTBqDRIrRNKNPS9VdfU/PUl1i8e1mNqbyHKowuamj4NrGVxAJxkwG/mYmdlAEQMbKuYbKEIFPa4ucE7qb9N3iyiU8J733BK71J3LKpp0ed6zBAHBT22kK2qKBOyZ1L5s9vOTovTwnqU/g0jhypbguFynYlEqsVgFzApH5G/VGZANE6eFU4XELYMbFYCxj4JXSeCvBzolFzK9zzIAohXV9pcbIVSjzGCy3Taf+ovBwDJRSKHNjZaeJFKltQ5eboWQPFVg8OUA1a598RDk+QiJireAJks2mqa9O5EnV168eybdiKgUnIvmqUQLNwHJz9EJwy2iunyHuoaAmC2Y6j/i12y0VxUJAlZ6EInnT8Gwga79e3EWKRGQ/Q6dkAsnjkyEBDOg7Bl5rJhJoY6yvpOIRnf1IqeHVDQ8lfk5/bwa1fU38K7Wt37+5avmLFtG58nCnQ/xu/oCRZuu2AXCaaNsYpPKynotbZOiBGsNhr0aq6+plxhhEVtXYWgFR1dHSuGj36RdEYCaWzs+SHcFE9lUdm6L2VLXNdODzpjVBoUYIS6O6eMC4aNe4DDMYI1G3fftCdd999amyMXEFJs4W+xxb6vQqFtNHR0bpeUJIBWClfapiO56DrKyEWDtHsbfWE2A58F8HSoj1E1e7m1yjeEbcBIZypKDEaX+WDltHBGCsrSNKqh3+m8vt4PgsKC7ctC4UujBtcuYIi/butXMZYhXK5mlVuOYaBxM853gLFn9NHp2/R7Bg+Ehf9Lx8MLctusIvYIqKGhftiE01TxN7YBnCfQou0d3OJPPu81a+qj0Z7bm9qOme3u4+BgZIQd3mup6dwCqBsmIiB9YwzCKHFQ/zYJoCylbbjmNCNSkUfNM2CEXwbwaCpYXeVn6938f1VVqGR907NAEobmq+WOM1m7JiipHc+ikaDV/F8PgtXw55PsD7l3YGAkmiz6NfRYtMVqpjBdJPFJfod2Kc4A+FpqxHSb7KCTFpVElYQQZtS7L0rzyZl6PsNDLTKTCd9yDw5cWwE+FvZ88uT+xgIKNIXC+tyPTjrZkA0gZpvgeLf+XNAge2vrK8vRx2nBWUcckdcd+ROuqK3YqNeumTJ5M0Z2s9hwcJQeEu685zBBIXxZwFK6+MQO23/BqWFk0vr3EgseQEl7hxR5Ts6/1nglESTXzcLKKjjAKE5CyW//s5nDRRguEdAEUv2nP0clHioErm6TiF55RxEnLf9mVPc8dunBJQW8Cjfz0FxyVVr+/xf0CZE4ixBO+QqWVwJw1DEPwJnyi7lI/6QfEtIkd/6LTzsSzD+yBJIa+cMqZ0CBtuRqzEn2FbJQ2+81dZuJDoWXcOQ/8RQ3Iow47Be/CQtQSbkmSJYpC/PlBkgTuceNd6Ib1wJcavs9WknJjEmvdWZvtaLSq6tffoo3IBjg8GCLUuWTPoQC3QmFu62cHhyxMoSMCfy/E1M+H8MB6e4zXzcjTMQtBGykXT8wNm/Q2j5Pv2Z+X5A3tOgpDqE+jxipbGI2eMQEzv88pti4YVThh6UWDhSIm+eA9cOTe6UDbb1pqIiNZm4hRZukSyfUkvQ+Asy7Q2gQAO5u7FwpI+IoQULQvYVuNxOU5UEwvJrUsKRgLKDvX2I19UdblDq6p49ROuenwPKLCbU34FaypSSw5HskgsQ9g/boETOoOJpWh0oLf0ErocTlAUL1o8mgLWClZYo/P2AMtvrYlpc0peiYaWajpgquf/2EcdLRR0dO/GB1ESrU+/e8nCBUlW17jDDyF8OILMgeCUK4iY/EUOZpfuIQ2QSzjCBdVcslL21gAHusOXKDk7MxngZZDhAEUCCwXwJaE1Hla8yjLwb6+snbvfDJXJuTmDrFbvNLnbHRRzr/CEBFFjxa7CieMwxO8VrGGHPgqKu7+kZ8buCgq41kDgTzl6pde9N6Y5kswEEl8hZcqk9181FRcYU55wq6YC9VeKolzjcgmk+Plsaxp4AhTEk1f0m6FrNop2AMhA2lzPqW3MBJPWAXc8lzHmPA2QSKDFzXLzmmL73op77Atd6dlvbR+LfDHoZO/bQWdDCgV3sVFF8pgxn1N6GTlLDL3Ca8V3iwx+lBUUqkchIc3Vd/IUsSTuw4du8OxaCN0L4y3xztqN8qcZMU5FTMD67OHYgK0HLYR23xXR9YaFRn3ok6w2Q1DRSPRsBG08rTZApTpcceXwZAiREKTkfwi1bsFviGZLJQwPKk9SJ95t0rcUbkT7e6gIjuKV3aS5bRsax0rs0Z0xxjzhSVFQ+ORSKZVPFS9rMn7q61tmAcW/fa3oNe+4yHxPY6161AYk41isc+AG/ZzKv+EFgxu3jPEBWkKuhznP+ppN9OmWU+bBFVKVrte7A2CMXL7VkzBGrqtqELdD9FE1cNyL8nTPvLexi3QlSt7gWmOTiilgI1hco8jJ3e0pgMUnTkgN0kS872NdlfnNXhhOcZMHKLF4nKnAOUb5/5wSKNLKDUHLWHPOLBBjslxnOZcjhnHC2sVMBCbQTRPpBttupHlMsY44Tl58sYKT4jbtkm8BgP0+VIfpdOF6u42S9DOUJFCGYffkjgMFF7wMGvtnrtJJ9CUpubZQ6QMPdbSQLntvcPPl1L+B7BsXaSi1XI1N+CuJHuDrfgIc6w4vz6IWggbxjJUfr+11BI+munbyXS7Ld8XGP6wsUaWinPSzjZyx8KUXkDBtKrtwuGsikcm1r59zfAicnXd3Tf1YqjxSREscb9jSEb1Ck16qqjWPIOLrPzaL2aO1wUihT8o4niny8ZIER4Eqclpum8VAki/QJHPNIR8cB165YcYaknfkqOYHijIBLwOroSv4uThq1nb/nYRw94osajy/bYExn7FDy2ADCMYlejB0iN9pyKgMCRUbE9zkRQpaxUqfzZ1w7OdRYV2KNtajCjdnCEP3NwAoKySUDLbn+panvyv95oNbm5enbFi+u+E9OaNiNBgyKM7h9ibsaEE5jbx+QjihxLpnUBkDaAYg7OBCLXc4MBtU7ApgEkqNRHfsvRbCFTpY0UyLGpJrGLj0kc2NsCNuHaSUtvZErKi8PBAyn7aCB4nQoxw3IlTmQS1b0oPyfKWnnaQEc+IthBFf6FaTZgBt0UJwBWXVJI61gJWfIKjuhiGwEZXpOP7sBmYN4TYqq8VB+vrl5oNsk01hDBop7QLnZwXYoRiaUA5QcPMnFAu73aJOJyj0f98leO/UdvCcJyeKfvAegL5NLuwnB2obf9V6uwHpt9z8I5TOzHFCxgQAAAABJRU5ErkJggg==" alt="">-->
            <!--                            <span>Доставка по Москве курьером</span>-->
            <!--                        </label>-->
            <!---->
            <!--                    </li>-->
            <!--                    <li>-->
            <!---->
            <!--                        <input name="delivery" id="pickup" type="radio" value="2">-->
            <!--                        <label for="pickup">-->
            <!---->
            <!--                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAABECAYAAADX0fiMAAARR0lEQVR4XtWcCXgU5RnH55vdxE2QQ7xaewiiPaz1tlqrkkAsamnBI61WVLCaUuoRINlNoNXVouSQw1IPVJRWpRZqW4oXmmQTwXq0FOlTz6qgtVZFC4hJSMjO19+7O7uZ3U2yR2Y95nnmSTLzHe/3/977eyfKyPMVDGqzvb1ltFJqlFK6xDDUCYahC/m5H1OHub/WBwnP8cyjtd5Kvy6tjSdNU7XwbEtdXemWPJNsqHxN4Pe3fJkFlRmG9R0WBSDqqy7M9SJjPc9YjyplNAPQKy6MmTKEq6AIV3R2hk6H8Ish+mhmG5UPomVMuOh1uK3VNPWKurrxj7o5j2ugVFeHJkLgTIj7Bvee/RC5icX8KfrOfNY0je3yGzveKj8rK0MjfD7jyOh7a5RlCYcZI1h8CQ+O6GfMD3n+DHdDff24tW6AM2hQqqpCh3k8uo69OxnihyUSpXfIbqJLAMJsHYw+qKkJwXVWidZqMnNMSl483LkTAFtM0/LPn1/28mDAGRQofn/oasC4CGJGO4mIsrZZ2dBQanPFYEjsu29NTfNUOCmIfjkwaSOeB6DbGxrGL8511pxAqapqG22aPcsgqDQZDEQiiIwvz5WgbPsJOIAAAGp4EjhrtN5jekPDSW9lO2bWoEDEt9mhxYnWRO8QwtidYLYEuNFedFFhoVUJx1YmgqM3WpbnysbGknXZzJMVKH5/83QGnwUgh8QmAYy27m41efHi0ojS/DgvAWePPXQrNMSVMvS9yn1FY+O4hzKlLWNQ0B/noz9uYTeGOgD5dUPDuKmZTvZRtItyjYaTjYscdL6NZZyBWP8xExoyAgVH7AomuY4B46YWizLto9QdmSzG2QauFiWMIYhfYrp/jNlekW6stKAEAqFz4ZDbewHRO5QyJ8d8i3QTfJzvk5WwiBLhwnRobxqIrgFBqa5uPVkp6y64ZEx0EL3DssySxsbSZz/OxWYzN/4Nvo0O9fbRr+HrnIHYv9TfOP2CUlvbtHc4bD4AIARw0euTLjL9LTLKMeoux/v13d2+by9adGJnX336BQWZXIFMnterrPQ1H5fJzYYz+muLXlzuVL60uwX9MiNjULA0aG69JGZpkMVPnJXJBSiAIeQwxtp9RfGeCzAPJo+VwikEdp9BTB6msx2YGZu6ulTJJ8EPyQUIZ59UP0b/taNjSNmSJSd84GyXAkog0Dwf7VETa2RZ6qhPk2JNB1yy4kUJp6iFBFBqa0NfsSz9MAOPitiaDMWGiR5n8A7AFLl9yufb+lYwWL6bv3U6IlPfazVz5tq9TLNg6NCh5jvBYOmu7McYuEeSGL1oGIUT6utPeiNuUJzd4ZJGFlIVf6nU6EzC/UCg5TH6HMq9P/c27icAqQmAH+/q6n5pyZIzutIvTKva2uaRmHzJ1FVIHoXgkjRA5u55+jmiLVARR+Lhbuxtr+fV14//eQooEvl6POE1vLBzpvpGGhJgpb+CwZWFnZ17FZEuOBFQT6fHOG6Jj4RbFhUVfTg/GPwunJR8aVVevso8+OB9DwGISQB5Ji086LS1IrYA82pHx67qzEBNT6ezRZI12sB8E1ETb0ubuPjQaAZE3BTtqHd0dZmjclWuweCa4s7O4iPxDRoY8yCtrfMbGsqcDpTy+58gZOg+lfdnM6GkID7L/YxSHjzOsRvxLc6j/49wC+rSeaDZwRFtLUkrNmFzrC+/X4zLEfFlIqAEgyFvZ6fVxp/sdASUjLlkIIIkzcDCbmHclfX1pbXSVpxCwnkJHS6ws/pCgyS19+DeCnGzIe5uv//RAwzDuxDQnsdsXpvLwtP1SeKWB5lnYhyU6urmo5FfQmslOgEFq850I2sWDD40rLPTtwLROJDFiU/wXE1Ny9nkYxYx35quLl9VQUHX1xCXW5lWEt2SfkC+SxeJkkbHod/MU6K6pRSF6O6V6Onqd0zTLJF5IpzC5JhghSmOQLIDXUKy2J2LiacD8lWMfw2LXcoGjGGRNzD6BuaZ5/evH6pU9+X8vY/Ho+4sLGx7PhgMWjJ7TU3TcShe4ZI1xCo3u0NR7yi23yKGwb50BTTdboPS8gBPv2O/Wc2OSnLYlWvOnNBh4bC+D27YjG6ZUl9/6g6s1VVwz/GAMFt2BhDMGBDOSUWsOzqMJWzU8N27uy9btOi0/7lClGMQp3lGdH+L6P5QzZ3b/LmeHgOro46StvkI+kg/LGLcMy3LuhSF+1htbcu3EKF5ALMqHQcQckzGfFbS/peItJ0kysX/6RtOYjzSmApxjVxPK1VwuqqpaT2OHZRzk8hFg4x8k2x2zO9vwsqY6A31B0SoWvqi/Zcw7wilCq+oqzvZwcKJIwcCj5GQ9kiC62A4ppWfj8Pif8lm/oHaOn0WOOVdxHW8QsbLSbysjHVEdNImnrIlqKZm3V6Y399obe5vmuEpci4DB5yD8p0BByzEPxDxTbq0qqxsHU5C+kzaccimPiN7hn76fXFxZyAYPCMhXsmWJmd7xDnuecsJBf5Cy/VMGjGXXJsAJRYIDmaelL6I0I/Z6V9wzxVlNmdO0/7ka37JOt+IcQ/vVEXFBu/ee+8cgx4SMM5GxMRUr1QqTFvzFECZzvOb+4pucyXYCQrjVyke/I7Bvi8DSmYeGS/JdfCB+sUsHAu61+drrxAPFxGaTShQhsPm37ZtyIvDh++cgP4Qr/YMbuEMObPBIkTD+6qqtft5PAVirt+qry/x5xZbpVKZFAtdL5zSBKHj8wkKeuGLOGLoFH0a87yAwp3e2Fi2TiwToFzH7mCW9U425STej3SQjZgbV6Ng58UAQDFeBotPBMha8Xzd2MAkUFYIKPHES19htBuT2ofvklmX+OcAAIBbzBswt7P5/WKevcm9VGpRAG4OQBzumHcT58M/QQ89Kc9EMdJnPnqwCfd/gRv0IS1yvBs7n24S8ZEIlzqSSHlDHlKOmjlaFzI6CWSjCq4kppE4R92FE3cFFujvscATv8TX2anlDLjCXixOnGqh/Ry45W8xAKAZ7jLGwGGVsSBuMOAkHYe0CCjNDChRbV5AQW8cDNj3srj3i4raJu7aNfZCROZauKKeKT/P7X3tta01q1Z9X6qajKhVsvBiTdx6vbSoSD1BTkVSh/ELmidIU+5lmZzjpAPMCYpk/gUUiV5L8gdKyzHs6n2AshHl+ANyJodgde6HY56EgHWIwTnomF+JUyc02DUq1Kl8+G7f6QYBLhIaiMPVDWizBpuIcooPND2Wd0WLxRiCxbiNXT8KP+VCEQOIWAhQkjbARBsiVv91KtN0OyvvsWaXwm3nAygi70xLZNI7sU1fivZOiJsW5ZT8mGSCwnLkX/TKTeiPuurqppPhkKVMSQSt/ofiLNXae1VDw9gXMl1SdXXbIfguiyWDVlc3jqxZ7q5/XyY57rwByrP4KZEYyM1L4itE5rewJnojPJks1+GE6auY4ykAuZm/p6J0qXQaB0dlfqF/5rKhx+CaBxobx/4r856JLVOct6hi00Jg5MqHmx9l95ZZ/LiSewu3lIvKUeybiADetPVVOAZfJjxTouj0i9MqGLxGdXSM/Z6tlJfiFdtZw/S9k1s4QSHsKEGnhI6FKHwVNUQa5yMglHFratrQKeE7+PVoAKAiwAqhY+rYkH9wYwHVFAhaBqf+uf9lCRitnvZ24zA4S0pDJsLdgKyv3rz5/Q3SL2bFMoUmMSA0NofD1kRAWU/ar/vB2OFXPlIHQmB5+UrP6NF738o83wL4CxCVDewQ8YwhKUCpqjyRuXen6ocoV2zfPnaYz2ehe5SkMr8OiC/Q/h5MN16tJu1pnMXY79L/wkwBkXbO1AHi/Ux3tzkhEhHzYo24zvZgriaZnARKboSFUPijl1Hb8jMsyCm8/xULXG+3Ow4OuPqVV95bu2pVuSVgtLeXjvF4rLPsTL+Ptms8HuN3cNUuLA/uvrqUvocCyj8Z928o8ojRyPRCyW6MMQRj3AunTrFBCQUYkLLP6JUvvSJHsliLu5m8yOu1yOJ79iUaXs6Ux3CLLqE83eDwS68m//IIQEje9nvcb8Ih93i9nkctq+cAFPMl0CtBLCWqigSZdRenD+tGjGj7oK8MXn8AJacjcSovbWwcf0cElKi893Ay6G7iui9iSFzPlBo03knkK2dMh7H4ZdyIsOrilqDxR9woXoOTRxXYvbvr5YKCwhLeXQZgwl3v8HMVVmc5RX6bcjXHSSUaW4ixJkiuxz7i0GZHR8t6Jv2mLCTT49JMWdTZTiLjnh4d0S3MhF4wybyVNs2c+chIn6/YM3/+KVurqpoO93hMOYPpscGTYE245u+AB6eZ97kR8yR5sg+Qn/2u0BrPstlJIDlqEFC2U/E4OtfDsHRgocNuYAOuFH0AIMul9ANxkDRlEWDdyrMb4ChRqnfzTPIqf5KNKi5WLclxULq5+nuffBjGvOegpO9PAGX27OYDUWCUYES/tsgntwCCVFmiR3rOwXoQFCqsiBGr2t7J3xXbtg29f+TID34PHT50yEVucIYToKSDsPWm6T1LuDQBFPmDHcRvUIFY53z4LNFgbjdRsy4qKmqftGvXkCksXDjUmRteiDNWBXgiZl9B3C5csGD867lyRXK/1Do441qMS7ySMiFJHY0netD8+eMW8VcOOmifO8XX8Hq9k3p6LGrrIxboIJt4SUjPIPr9Q2ensRIdUoClmnr99WUoV3cuZ2aAuV/2eo1x1103/j9xZkieBm6ZByhzHdxS6nZ5qH2YT4SsrhL3nDmnwRGcEqpi5r2TXWuwfZrb0TVLxadxB47I0UpCtaQkqonQEzJ4KccZs2eHOL60mgEmkhKUIBGlW+qm0hX95fWKddF8PabFuw0FAk3DiJy9mMT3A4E2PNawKFkfYJ0v3q8boNh+ieR1R9njrS8qGjYhGDw2oUykzzMe2EtMoCi/SIW1fLiEuZIsu2sXp4THh8PGHSxaSiJII4if4tnNJowFqJ8yEd8QGpwoDhQLZUdOoveq29n4s/v6cGqAktGWe2SXeqd1pzzDuQw7m+9n8XJ2Hav5l6onqqCsesn4Z7fs/lsTUsCZaqqjBbmdcTiDqVe/oFx++VPDiovbKddWx/Xql092PX5/kKQWF+u/bNs2vOS2246l0ioLUKQpyu4EQnxJOscsg/Fpq5ZMVazGZtz5srq6stf6AzHtuTFB3GkMsrw3LpLCGjXNjaIet0RjIA5hE6l4kI8zI0ZjM/pqKpH04wPNnRYU6Yw8/oQBG2OJqOiAqhJzemO+F5br+Jh5wFCOQkbdAUDlmXwMlREoQphdnYDT5fy8Vi/PNn+R6yIz7Wd/OseXJ5GvUu1Li6hMS8chcd2Z6WRRjgmdi/nEuYt96pIfPyYbmpxtJcgjJ/LHWNIoJjL4PxXZVFhmzCmxyUVxMbHIabxkQ6JqxGlxd7dxo5tOXqbgRLlDkuJaPq6M1+uxgf+ghKyCAuWnMx0rohiyaRxrGy0Ji5zbxOrkIq9scOR75F/nMm4ufexvo5PAiH64DY2XLFhQ+l624+YESmwSSfryu9SMJX0wbWyRjD2cszofnGNzxiQsSZD5RyUtmn8SoW/CAyf3m9s1KFBkSoqFv8RBF5Ypckif8j8OJEQguyblHqszqfPvbxnRpJCUS2hRoCV9tPuQ96vRHz8bzDw5i09fhMcqAQDh+ETT3dtagksW1UpiaTsgbUc3Rb5FRO5fl4XIwklkR7iOxVHGLvpBigUjliSZI2yR1e/yXv6fgkTT8j9WBn0NmlOSKbArIadIPQoqS5LPebr0RkQUUNXNztoVNyZzHZQYUdH/hxCmskCfBvGHwhlfHizBwmmM8yr3Q1oXNjm/0Rns2M7+eQPFOcmsWW1f4N+JjJKYg+dHsLg9AesL/AzD+vvwbF9H+y1wWQfvCnj3b55TC6fgCk1tXuHmXP6hQ7aA/R9IkTR/9IkNLwAAAABJRU5ErkJggg==" alt="">-->
            <!--                            <span>Самовызов</span>-->
            <!--                        </label>-->
            <!---->
            <!--                    </li>-->
            <!--                    <li>-->
            <!---->
            <!--                        <input name="delivery" id="dpost" type="radio" value="3">-->
            <!--                        <label for="dpost">-->
            <!--                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAABECAYAAADX0fiMAAAQHElEQVR4XtWcCXhU1RXH332TZRJQWVwrVUSsn9oWt35VEUgAQcWFarVVpICtuCsRsgCfZXDLJEGBWupWK0vVT6kb7pUkBBBXKm6tooVo+XAHEck6825/5817yWQymXmTCTHc73uZt9x377n/e865555zXpTRDWX27MqDm5rUQNM0hmptDFXKyOX3x3TdbBj6MKVUrxgyarlfR53vefY95+9qba41DPP93FxrcyCQ37A7yVa7q/GiouoBWlsnA8CFhqEG8zuYvnqn2R9gGQJYJW0+XVY28u0024v7epeDUlhYfaxS1tWAcAKEH7c7iJY2tdZf0v67HMtyc43H4B44qmtKl4FSUlJznGWFCiFrFCy/fzzyGMgngPUkYvEtosSMmzLzRkODsWHBgvxv5bykpDpPfi3L6MPfYyPtqDzeG9HxkPUb9Hm3368e6grRShuUWbNWHxQKNU+H8AkQfmAs4QBQw7wChLmqoiJ/Q2fnctq06j5ZWQaA6fFK6fH0t0+cttby7OZgcNQ/O9uPPQXpvFxcXHUxgy4EDGdG3db0Du4HmprMxS4HpNNPvHeFoyxLB2I5iH4/595ypTLnBIPDtnem306BEghU++vqrPl0fjG47t3asQ3GAsBYsLvAiB2kgINYLuD+kJhnr1mWWVhRkbcmVWBSBqW4eO0hWjctjTNDS1h2p3UXGO3BqZyMHoJz1KFRk7QJvXVHWVn+olSASQmUmTOrfhkO67/Q8fEtgoLyRF+MT0dfpEJwsrpFRVWLmbBJUcB8p7UKlpePLE32rvvcMyglJSsHMXjsA2NgVONvNzaqvB+KOzoaZFFR5TQmbn5bYIyl5eWjrvUCjCdQiosrT6Gxh9Efh7RyiLEE9Cd76eSHqIPxyCplLY5ZpRZh8F2TjJ6koMyYUXOYaYaqo2UVxVYA6qLcenRxDMknW2nX3wHSrQBTnojwhKAUFKzrl5XV8BQNnNoib0pPwQ5gBvaMIvZNdrZV63IMq+NOzq8sL89/sKMRJAQFsRGR+e2eIjIdDVI4xjStVVGiVKu178zy8hH/ifdOh6AUFlYVmKYOuHaIWKbokLw9gz/aU1lSUjmZVeiB1gnWa5qbc8bOn39KfWztuKBMn77y6IwM81kqD3Re6JGrTKoTxKokdsyc1vd0sKxs1ExPoCA2K+CQsyOV9Q4sw7yeYoekCkRs/Wg7RnbabBVOmzdv9DvR9dpxCi+dw6ZqWavY7BkrjVew4ijeF1ELp3cISmRPo1diEQ61eQRrlaXXFSGv/fb4ejFi9D3bg99UVIx8ziW8Daew6z0XhboMUPaSCnBMh8svdXHyGPtxiKJKau90EilpNxsF+YpS5uVlZSNwKqVfIm4Ia0OU7VWF7TIqDihaYQU+DyBjXeVKxRiXQCtBgBLiypc+iclbYKLe8/mMK0pLR76cvLa3GtGrUcR20aORitdtZnCbKClZ9Qutw09z64AIl6j8YDB/VUddAEodz3K8kZB2rbcx2S8LBke/kXZLUQ0gRrUut6AqlgHK79qAwiD/zI2rvXCJ1OlOUODedyD6ypycNa8axgn+CI29Tf6EAoG8RqZQdwasGG7BK5g1rrz81K02p8ycubK/ZSlZhmXjR9ELWb+nJeqoO0GBjjqA+Vh8u5xnOXQBjvoMsG7HZK/qDDA4qAby/uaocU5CZSy1QYnsgtWLAr9cA9BxyeySbgblK8h6GmA+gLZcJq2Jcz/HeIBiUdCXMInrOsMtjEP8xq7X7iFAmWCDgm0ykw5uc7hkBx3gSU9cuhcU9QZbjmvDYZtTjoTWRp8v4x3LasrBx/MIq1Ozzxc+u7R09DfJ6I59Hr08wzWvm2bW6S4oz9LRmbbgaG9+ku4EBWW4hsAam1PjMjeWJCsSNF/CRi8Xi1vCJvh38otTFaPIZlG/FRm7/tI0M05XM2a8vL9pNq6mgyPlQSLbJBrl7gfFYKuvAUWCbJECzSsAZWooZE7l/CrAuygYzCOkkpriRX3AgZGQiYxfxW7+ki3FLkHdCQp9robcR9F2l/P7M5lUFxdRtKZpysq5FG7aF3G6NDe35vVAYA51vIGD+ljlOuJpr0yB0nBeBt1Iwefa14vPtZtBQYmqu8DiNEg8kYMQKcMwrN78/a/WGbO0DuVwvpj7g3j+Cr/3EIXcUFqa/0E0h8c7bwuKsQIrtvIauONOtzLa15PJ3p2giJ0i4pGdvfqVxsaR/Rsa6sJ+f4Zqbg77srJ8KiurfmcgcHbdzJmr9yN0K8D9noPJNj5lPIcnB6XVpYBueg5Qqm6iwxt7OijhsJpEvGmrz5dVAL1fE3++vyOOLih4ATdq1n2M6VeAIkZewtJ2g0hcGvEhHqJK5K1UvGvdzSnEm6b06mVurK83zoLSnVpnrcb6ZM/SvhQVrd1LqSYJcVwIKFERzPjYRIdE0CkfCSgVgDKjE6Ds4h2xLiUFIulsJJutBM8xKPX7KNCpLLmY+clLIPDc3vX1fhcUe8efqDih12oHgw9Fp8xDp5A1kDKnfA5nLWL3KhH+/sk6TvU5Po4wRwPK8gIoG8MMXr1587aqAQMGuGZ+3Ca3bNnSNHhwvz6AeDv0neeFU6JBoa+N6YhPA51+BNGb+GU2ZTWILtriHocNtzyDm2LrJIKKVg0V5l2JDcuKArcYnzGB2dxz2nXfV7StecQarA3XpSEJQwpQ4qVstOk4JqL4cRqKtlKsy0tpfQiErICgrZjbLIu6mesGKIRAqw8/MrP13PuOOlBu9Obc0woXoVyxC9ZNnOCmsIHm2ugVaccGDv2iZcPol/5NUzVxDXDGaI59vXBKO0XLjSkQ/jcXOq9LstSH7YohYDyETHHtgQsueNQ3aFB/3JlKgtxHO+3KoF7F+ryf/cnGVEXJrR9ZcsOkf2iUrbE3IIQx0b8CmsdQvE+4ipcgXk5mZuMdTMuEVEGxl+TCwlXDIBaLMVK8Gm8y+MMP738jjZzD3uNS2VU7RM+C6ImAsgmi2NobPoczDmE2+3HM8/tXLwwEAjEikFCUoLN6OKI6j1oH0d67gBFCb4hlK4r4KPrDVakLAOH94uKXEBmTsK463xso0RatbbzV0GAIN2Qkr8OrmR/hiH3/yCtjeGei31+zqa5uWDnnU7j3GC2x09binxGXpaxUuBKViIBw0W05OTV3iSkeCMxNKkq0eyLceJ+jL8T6zuc4mIOUU+NTgP4Xz6Sv7+GeizDzt9bXDxfTXzjFg05pBQX6gvaG0OdreAFC7UxGrxtCPP8ZdXXGbOqzHzEWQBga37iIRt+klX4ce3F9G+y+KSNDncL5VO5to65s/+lLY1xJIqCFTWHPeJuCPgIs1Sg6iXdJO9XiJBfLVhICn8JuWQLniP+HSVCncv8VRPmnnG/l3kMcBRxDACXhaiWdYsBuF/oj4zd+7TiZqp7hepzc9Oo6kLrFxdWX8MatnEqD+Gw1doTK5LwvIokiNgeyrJ6Ff/UBUYICngABkCJSMtti4yTiFAFL3I8ocXEV6J9w/rhpZt4D2NdxvQ1L92GfT18HIPRj1vAr42BS7FXof4CSMG012nVA/a9wYo1x/CnVxXQQdED5luBQ35iJi3s5deqbmX36bNsPInMbGxu3+f1ZRwHCLVR+iRleB4FPci7su55ZvZ7rM9ADp/h8vgnsWzSOodxQKLND/2pGRjP7GxXOztYN4bB5g7MhvIY2JkLvFc7M3kyU7xn6W8QsP7prV8Of/P7sAUxKs2lm15eWDhevXYcF41V0z/WRChrHuHWaC8pJXBAEi6SDe9UrsT2R/jUUUILM6nKebYHIfzic8CGg3IDnbAj3zsWRg5csMbHRbUfA3yHbkZMAA0Vu4j+WtFF7IEv4g75R5Jyo57F6hXM9F0TnLWhyQzmt7kgnOPQcYJzsdJTUcR2vVycF7F7ZmwAynJGBm1OLPqjw+cxn0AMLAOxLOJFlNbUC8TiRDFwE6ho47AtWvMW0BRfoG6BbUtxv4XoabeOA91ZiHdfQeh65N0+0yDP2yp007qY+1SKLh3lrum0twgZXQHgZonKfz5dZ1txcrzIzfX1gf9mNSzb2ROJJKPbUCsssqWUZdwPCERzYQNb7Pl824heS/P+Fos9ycsyLyLgWXeKptN0IGhug+wxMi8+jgmFVJyCbwi12CnlnRSgQeDq3oaH3tYDCzlszk+oTmjuaWQxDfAlgy8rQqVJcXIPXzRIAZPldzyDEbv455y/gZLquvHyMrDyeCzt9CW8MlBeg926CYVfaY49ugUoS5hjj3Os0t0igvr5ej5UlOgKy3ohOWdYVYc/p0ysPZYlne6HgEEsCYVWkwT84b95YiW17Lm2TePQX0DkuGBy5vh0oLE9nYSmK19yO/3i1WTxT0kMqRnSo3uzaJpC1Eg4Wj51d2nCKaPm+fb8TeR+ZLrf0kPHHJSMm1rMLLpHtgEhJe1DkDrluZ8Lqj7jcgqzNRdYCPXmQqdAWyyWI+OOsWOdHtxHXmkS3iNF1rlTkpW9RaPnJwqipEPZD1sVYIxlQTXYlIRSyxt1+++h/JwWlsLDmCBKKAUa5W/9ads/HeQl9/JADTtZ3nETAWwgRtzjtOxQf9wH7GkxfLSa7rXThmA2w2W77/C3ZgNJ9HpsyyojWbd++T969954oO+02JeG2HSvy72joCa1v6MUgK66BPapE0tE1qfSRnbDk8mFtn1ZRMeKjeANJAspaPOFNkvJlJwY6De4RefkuvXGWXxL/1B/QkbKYxC1JHTzyKS1GkiT0tIgOijfAxmtuT2cXxy3wBHQOdFTATiY4wPJ7RyLak4IiL0s+nGVZj9PggGhRamw0C3qq8hUdAkfIJ33RuTa3AcjsZJPpCRRpJGJe29H/6G9+NvCJXH5PAwbunoMOCbSKvP3lxq1wd1kyQOS5Z1AiHGN/e4wzx3Ux9Cw7xsmmJjLYYofIqonI6JtYIMTp7amkBIq0iJweiI7hw0rVsldwVDDf7plzcQvUeuq5iyvBHZMc7rD1h1OgRfFRQv49qXSXMijSuOPJlxy5qzja/P8CUcJNTcbC7hKpyGdxWuLG0WDIssunt2aR1/hzNGidAsVtAJMZn6sRhGvEp9FSZGvADC1gD7Vkd3GOpMzToaS15rXtW3+Jcn04HA7Nxp0goZWUS1qgSG+Sg4tHXf7HgUQa2/2PA7GEAUgS9Z5KZ//kfNYPEJZ80i//+6BdBifcUU2q102JMsW9IJQ2KK1cU3UMahdvm5LcfonRxCvIuF6FN55f27JcFamkdghgMvDMTG3ntALwQH7kkJqISOy/DmhpngCY8Zo4sHNzRz4YCLhBfS/Dj1+ny0Bxm5fEQp9PXSbuTO7FfmrfeUrbvykA889m1F9zcw94IRA4RuLVXVK6HBSXqkjksZHogJas6OOZaTzuki2dTtGf0sYHAEHaubmiow8j0+nB5tJ0G/DyvgTeCYOSB28No77kwf4I0ZFge4YDlCMmdmtkEeivxSXN+dcckgfD9z7Gehzr79XWflO7fPmF5K3svvJ/WWeExIITn4sAAAAASUVORK5CYII=" alt="">-->
            <!--                            <span>Доставка по России-->
            <!--    			</span></label>-->
            <!---->
            <!--                    </li>-->
            <!--                </ul>-->
            <!---->
            <!--            </div>-->
            <!---->
            <!--            <div id="tabs">-->
            <!--                <div class="courier open">-->
            <!---->
            <!--                    <strong>Доставка по Москве:</strong><p></p>-->
            <!--                    <p></p>-->
            <!--                    <p><br>-->
            <!---->
            <!--                    </p><li>Заказанный товар будет доставлен на следующий или на 2 день после поступления заявки.</li>-->
            <!--                    <li>Заказы, поступившие в субботние и воскресные дни, обрабатываются в понедельник, а товар будет доставлен во вторник или среду.</li>-->
            <!--                    <li>Поступившие в праздники заказы обрабатываются в первый рабочий день, а доставляются на следующий или второй день после этого рабочего дня.</li>-->
            <!--                    <li>Доставка товара в офис и ТЦ производится до центрального входа.</li>-->
            <!--                    <li>Крупногабаритный товар доставляется до подъезда.</li>-->
            <!--                    <br>-->
            <!--                    <h2>Расценки на доставку</h2>-->
            <!--                    <div class="scroll-600_pzf">-->
            <!--                        <table class="tbl" style="width: 100%; height: 195px;" border="1" cellspacing="2" cellpadding="5" bordercolor="black">-->
            <!--                            <tbody>-->
            <!--                            <tr align="center" valign="middle">-->
            <!--                                <td>&nbsp;</td>-->
            <!--                                <td>-->
            <!--                                    <p style="text-align: center;">В пределах МКАД</p>-->
            <!--                                </td>-->
            <!--                                <td>-->
            <!--                                    <p style="text-align: center;">За пределами МКАД</p>-->
            <!--                                </td>-->
            <!--                            </tr>-->
            <!--                            <tr align="center" valign="middle">-->
            <!--                                <td style="vertical-align:middle">-->
            <!--                                    <p style="text-align: center;"><strong>Легкий товар</strong></p>-->
            <!--                                </td>-->
            <!--                                <td style="vertical-align:middle" align="center" valign="middle">-->
            <!--                                    <p style="text-align: center;">до 3000 руб. – 300 руб.</p>-->
            <!--                                    <p style="text-align: center;">до 5000 руб. – 150 руб.</p>-->
            <!--                                    <p style="text-align: center;">от 5000 руб. - бесплатно</p>-->
            <!--                                </td>-->
            <!--                                <td style="vertical-align:middle">-->
            <!--                                    <p style="text-align: center;">до 3000 руб. – 300 руб.</p>-->
            <!--                                    <p style="text-align: center;">+30 руб. 1км</p>-->
            <!--                                    <p style="text-align: center;">до 5000 руб. – 150 руб. +30 руб. 1км</p>-->
            <!--                                    <p style="text-align: center;">от 5000 руб. - 30 руб. 1км</p>-->
            <!--                                </td>-->
            <!--                            </tr>-->
            <!--                            <tr>-->
            <!--                                <td style="vertical-align:middle; text-align:center;">-->
            <!--                                    <p><strong>Крупногабаритный&nbsp;</strong></p>-->
            <!--                                </td>-->
            <!--                                <td style="vertical-align:middle">-->
            <!--                                    <p style="text-align: center;">500 руб.</p>-->
            <!--                                </td>-->
            <!--                                <td style="vertical-align:middle">-->
            <!--                                    <p style="text-align: center;">500 руб. + 30 руб. 1км</p>-->
            <!--                                </td>-->
            <!--                            </tr>-->
            <!--                            </tbody>-->
            <!--                        </table>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--                <div class="pickup">-->
            <!---->
            <!--                    <strong>Забрать заказ можно самостоятельно. Условия самовывоза:</strong><p></p>-->
            <!--                    <p></p>-->
            <!--                    <p><br>-->
            <!--                    </p><li>Оформить заказ по на сайте zoonemo.ru. Минимальная сумма заказа – 1000 рублей;</li>-->
            <!--                    <li>Дождаться звонка оператора, для подтверждения заказа;</li>-->
            <!--                    <li>Забрать заказ можно в течении 3-х дней, с момента подтверждения.</li>-->
            <!--                    <br>-->
            <!--                    <p></p>-->
            <!---->
            <!--                </div>-->
            <!--                <div class="dpost">-->
            <!---->
            <!--                    <p><b style="font-size: 20px; color: Red">✔</b>Почтой России, ЕМС - посылкой с наложенным платежом (предоплата 30%).</p>-->
            <!--                    <p><b style="font-size: 20px; color: Red">✔</b>DPD, Hermes, Boxberry, Сдэк (100% оплата).</p>-->
            <!--                    <p><b style="font-size: 20px; color: Red">✔</b>Транспортной компанией на ваш выбор (ПЭК, Желдор, Деловые линии и т.д., 100% оплата).</p>-->
            <!--                    <p><b style="font-size: 20px; color: Red">✔</b>Готовы рассмотреть от вас другие предложения по доставке в регионы.</p>-->
            <!--                    <br>-->
            <!--                    <h2>Расценки на доставку</h2>-->
            <!---->
            <!--                    <div class="scroll-600_pzf">-->
            <!--                        <table class="tbl" style="width: 100%; height: 195px; vertical-align:middle;" border="1" cellspacing="2" cellpadding="5" bordercolor="black">-->
            <!--                            <tbody>-->
            <!--                            <tr align="center" valign="middle">-->
            <!--                                <td>&nbsp;</td>-->
            <!--                                <td>-->
            <!--                                    <p style="text-align: center;">до транспортной компании</p>-->
            <!--                                    <p style="text-align: center;"><strong>(не входят услуги ТК)</strong></p>-->
            <!--                                </td>-->
            <!--                                <td>-->
            <!--                                    <p style="text-align: center;">Почтой России, ЕМС, DPD, Hermes, Сдэк</p>-->
            <!--                                </td>-->
            <!--                            </tr>-->
            <!--                            <tr align="center" valign="middle">-->
            <!--                                <td style="vertical-align:middle;">-->
            <!--                                    <p style="text-align: center;"><strong>Легкий товар</strong></p>-->
            <!--                                </td>-->
            <!--                                <td style="vertical-align:middle;">-->
            <!--                                    <p style="text-align: center;">до 3000 руб. – 300 руб.</p>-->
            <!--                                    <p style="text-align: center;">до 5000 руб. – 150 руб.</p>-->
            <!--                                    <p style="text-align: center;">от 5000 руб. - бесплатно</p>-->
            <!--                                </td>-->
            <!--                                <td style="vertical-align:middle;">-->
            <!--                                    <p style="text-align: center;"><strong>ОПЛАТА услуг Почты или DPD,&nbsp;Hermes и т.д.</strong></p>-->
            <!--                                </td>-->
            <!--                            </tr>-->
            <!--                            <tr>-->
            <!--                                <td style="vertical-align:middle; text-align: center;">-->
            <!--                                    <p><strong>Крупногабаритный&nbsp;</strong></p>-->
            <!--                                </td>-->
            <!--                                <td style="vertical-align:middle;">-->
            <!--                                    <p style="text-align: center;">500 руб.</p>-->
            <!--                                </td>-->
            <!--                                <td style="vertical-align:middle;">-->
            <!--                                    <p style="text-align: center;">-</p>-->
            <!--                                </td>-->
            <!--                            </tr>-->
            <!--                            </tbody>-->
            <!--                        </table>-->
            <!--                    </div>-->
            <!---->
            <!---->
            <!--                </div>-->
            <!--            </div>-->
            <!--        </div>-->
            <!---->
            <!--        <div class="total_block">-->
            <!--            <p>Заказ на сумму: <span>265401</span><i class="fa fa-rub fa-3"></i></p><br>-->
            <!--            <input class="b_btn" type="submit" name="recount" value="Пересчитать">-->
            <!--            <input class="b_btn complite_order" type="submit" name="order" value="Оформить заказ">-->
            <!--        </div>-->
            <!---->
        <span class="clear"></span>
        <p class="ordertotal"></p>
        </div>
    </form>
    <br>
    <br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>