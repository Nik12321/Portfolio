<h1 class="page_title">Скидки</h1>
<p class="spiski_pzf">
</p>
<h2 style="text-align: center;">Уважаемые покупатели!&nbsp;</h2>
 <br>
<table border="1" cellpadding="1" cellspacing="1">
<tbody>
<tr>
	<td colspan="6">
 <b>В розничных магазинах действуют скидки</b>
	</td>
</tr>
<tr>
	<td colspan="6">
		 &nbsp;
	</td>
</tr>
<tr>
	<td colspan="6">
		 • по социальной карте ежедневно с 10-00 до 15-00
	</td>
</tr>
<tr>
	<td colspan="6">
		 &nbsp;
	</td>
</tr>
<tr>
	<td colspan="6">
		 • для&nbsp;постоянных клиентов существует система дисконтных карт&nbsp;номиналом:
	</td>
</tr>
<tr>
	<td colspan="6">
		 &nbsp;
	</td>
</tr>
<tr>
	<td colspan="2">
		<b>
		<p style="text-align: center;">
			5%
		</p>
 </b>
	</td>
	<td rowspan="1" colspan="2">
		<p style="text-align: center;">
			 и
		</p>
	</td>
	<td rowspan="1" colspan="2">
 <b>
		<p style="text-align: center;">
			 7%
		</p>
 </b>
	</td>
</tr>
<tr>
	<td colspan="1">
		 &nbsp;
	</td>
	<td rowspan="1">
		 &nbsp;
	</td>
	<td rowspan="1">
		 &nbsp;
	</td>
	<td rowspan="1">
		 &nbsp;
	</td>
	<td rowspan="1">
		 &nbsp;
	</td>
	<td rowspan="1">
		 &nbsp;
	</td>
</tr>
<tr>
	<td colspan="3">
		 &nbsp;<img src="/local/templates/.default/img/36.jpg">
	</td>
	<td colspan="3">
		 &nbsp;<img src="/local/templates/.default/img/37.jpg">
	</td>
</tr>
</tbody>
</table>
 <strong><br>
 </strong><strong><br>
 Система накопительных скидок для покупателей интернет-магазина:<br>
 </strong>
<div>
	<table class="tbl" style="width: 100%; height: 195px;" border="1" cellspacing="2" cellpadding="5" bordercolor="black">
	<tbody>
	<tr>
		<td style="text-align: center;">
			 Сумма заказов за пол года*
		</td>
		<td style="text-align: center;">
			 Скидка
		</td>
	</tr>
	<tr>
		<td style="text-align: center;">
			 от 10 тысяч рублей
		</td>
		<td style="text-align: center;">
			 1%
		</td>
	</tr>
	<tr>
		<td style="text-align: center;">
			 от 20 тысяч рублей
		</td>
		<td style="text-align: center;">
			 2%
		</td>
	</tr>
	<tr>
		<td style="text-align: center;">
			 от 40 тысяч рублей&nbsp;
		</td>
		<td style="text-align: center;">
			 3%
		</td>
	</tr>
	<tr>
		<td style="text-align: center;">
			 от 50 тысяч рублей&nbsp;
		</td>
		<td style="text-align: center;">
			 4%
		</td>
	</tr>
	</tbody>
	</table>
</div>
<div>
 <br>
	 Скидка отслеживается по ФИО или номеру телефона. <br>
 <br>
	 Во избежание недоразумений, при смене телефона или фамилии, сообщить оператору.
</div>
<p>
</p>
 <br>
 <br>