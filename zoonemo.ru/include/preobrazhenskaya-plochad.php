 <h1 class="page_title" style="margin-top: 0px;">Зоомагазин м. Преображенская Площадь</h1>
    <div class="spiski_pzf">
        <div>
            <div><strong>Адрес: &nbsp;</strong>Краснобогатырская ул., д. 31, корпус 2, метро Преображенская Площадь.</div>
            <div><!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (начало) -->
                <div></div>
                <script type="text/javascript">// <![CDATA[
                    function fid_136196170356254182279(ymaps) {var map = new ymaps.Map("ymaps-map-id_136196170356254182279", {center: [37.70545059259028, 55.809670772581086], zoom: 15, type: "yandex#map"});map.controls.add("zoomControl").add("mapTools").add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));map.geoObjects.add(new ymaps.Placemark([37.70540767724604, 55.80952576830527], {balloonContent: ""}, {preset: "twirl#redDotIcon"}));};
                    // ]]></script>
                <script src="http://api-maps.yandex.ru/2.0-stable/?lang=ru-RU&amp;coordorder=longlat&amp;load=package.full&amp;wizard=constructor&amp;onload=fid_136196170356254182279" type="text/javascript"></script>
                <!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (конец) --></div>
            <div></div>
            <span style="font-size: 11px;"><br></span>
            <div>Первый вагон, по переходу налево. Далее следуйте на трамвайную остановку. Номера трамваев: 2, 7, 11, 33, 46. Остановка «1-я Прогонная улица», четвертая по счёту.&nbsp;</div>
            <div></div>
            <div><strong>Режим работы:&nbsp; </strong><em>пн-пт</em>&nbsp; с 10.00 до 21.00</div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <em>сб-вс</em>&nbsp; с 10.00 до 20.00&nbsp;</div>
            <div>без выходных и перерыва на обед.</div>
            <div><strong>Телефон: </strong>&nbsp;<strong>8(499) 168-63-24</strong></div>
            <div></div>
            <div></div>
            <div><strong>Дополнительные услуги в магазине по этому адресу: </strong>&nbsp;<a href="../../../../../servic/">Сервисный центр</a>, <a href="../../../../../obslug/">обслуживание аквариумов</a>.</div>
            <div></div>
            <div>
                <div></div>
                <div><strong>Наш ассортимент</strong> (пожалуйста, для уточнения наличия в магазине, звоните):</div>
                <div></div>
                <div></div>
                <div><strong>Аквариумистика. Полный цикл работ: от установки тумбы до заселения рыбками.</strong></div>
                <div><em><br></em></div>
                <div><em style="font-weight: bold;">Бренды:</em></div>
                <div></div>
                <div><em>Аквариумы, выставленные в магазине:</em> «Аква Плюс», «Аква Эль», «Аква Тек» и, &nbsp;на заказ, аквариумы фирмы «Джебо» и «Ювель».&nbsp;</div>
                <div></div>
                <div><em>Объёмные фоны:</em> «Аква Фон» по выгодным ценам.</div>
                <div></div>
                <div><em>Флора и фауна:</em> аквариумные рыбки, живые растения, декорации, корма для рыб, средства для воды, лекарства для рыб, предметы по уходу.</div>
                <div></div>
                <div><strong>Террариумистика. Полный цикл работ: от установки тумбы до заселения.</strong></div>
                <div><strong><br></strong></div>
                <div><em>Террариумы:</em> &nbsp;любое оборудование для обустройства и дизайна террариума.</div>
                <div></div>
                <div><em>Рептилии:&nbsp; </em>ящерицы, хамелеоны, пауки птицееды.&nbsp;</div>
                <div></div>
                <div><em>Живой корм для рептилий:</em> сверчок, зоофобас.</div>
                <div></div>
                <div><strong>Птицы, животные, грызуны.</strong></div>
                <div></div>
                <br>
                <div><em style="font-weight: bold;">Корма:</em></div>
                <div></div>
                <div>Самые разнообразные корма для птиц и грызунов - как импортные, так и от отечественного производителя.</div>
                <div></div>
                <div>Аксессуары, амуниция, предметы по уходу, игрушки, переноски, мягкие места, дома для кошек, выставочные клетки для собак и кошек, клетки для птиц и грызунов.</div>
                <div><em><br></em></div>
                <div><em>Бренды: </em>«Ферпласт», «Трикси», «Хаген», «Пет Продукт», «Зоолюкс», «Дези», «Зооник», «Зоофортуна», «Немс», «Савик», «Ноби», «Карли».</div>
                <div></div>
                <div><em>Амуниция:</em> «Аркон», «Зоомастер»; рулетки «Флекси», «Нитейз», «Патрик» и т.д.&nbsp;</div>
                <div></div>
                <div><strong><em>Животные:</em></strong> птицы, грызуны.&nbsp;</div>
            </div>
            <div></div>
            <div></div>
        </div>
    </div>
    <br>
    <br>
