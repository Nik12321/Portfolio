    <h1 class="page_title">Интернет-магазин</h1>
    <div class="spiski_pzf">
        <p><span style="color: #0000ff;"><span style="font-size: medium;">Режим работы интернет-магазина НЕМО.</span></span></p>
        <p>&nbsp;</p>
        <p><span style="font-size: small;">Подтверждение заказов: <strong>пн-пт с 10-00 до 19-00&nbsp;&nbsp;</strong></span></p>
        <p><span style="font-size: small;"><strong><br></strong></span></p>
        <p><span style="font-size: small;"><span style="color: #0000ff;">☏ <span style="color: #ff6600;"><strong>+7(495)795-80-09&nbsp;&nbsp; </strong></span></span><img src="../../../../../custom/themes/default/img/mail.png" alt="" align="absmiddle">&nbsp;   <a rel="nofollow" href="mailto:info@zoonemo.ru">info@zoonemo.ru</a>&nbsp;   <img src="../../../../../custom/themes/default/img/skype.png" alt="" align="absmiddle">&nbsp;   <span style="color: #ff6600;"><a href="skype:zoonemo">zoonemo</a></span></span></p>
        <p>&nbsp;</p>
        <p><span style="color: #0000ff;"><span style="font-size: small;"><span style="font-size: x-small;"><span style="color: #ff6600;"><strong>&nbsp;</strong></span></span></span></span></p>
        <p>Доставка заказа осуществляется в течение 2-3 дней с момента оформления, при условии наличия товара в магазине.</p>
        <p>&nbsp;</p>
        <p><span style="color: #000000;"><strong>Заказы, поступающие в субботу или воскресенье, а также в праздничные дни, будут комплектоваться в понедельник на вторник или на любой последующий день.</strong></span></p>
        <p><span style="color: #000000;"> </span></p>
        <p><a href="../../../../../dostavka/"><br></a><span style="color: #ff0000;"><strong><a href="../../../../../dostavka/"><span style="color: #000000;">Подробнее об условиях доставки и оплаты.</span></a><br></strong></span></p>
    </div>
    <br>
    <br>
