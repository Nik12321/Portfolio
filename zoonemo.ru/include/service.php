<h1 class="page_title">Сервисный центр</h1>
<div class="spiski_pzf">
    <h2 style="text-align: center;"><strong><span style="color: #cc99ff;">Уважаемые владельцы аквариумов!</span></strong></h2>
    <p>В нашем магазине по адресу Москва, Проспект Мира 182 работает сервисный центр по гарантийному (бесплатно) и послегарантийному (платно) ремонту аквариумного оборудования.</p>
    <p><br />У нас вы также можете приобрести или заказать запасные части к фильтрам, компрессорам и другой технике фирм <strong>Aquael, AquaPlus, Tetra.</strong></p>
    <p><strong><br /> </strong></p>
    <h2 style="text-align: center;"><strong><span style="color: #cc99ff;">Ремонт по гарантии</span></strong></h2>
    <p style="text-align: justify;"><em><span style="text-decoration: underline;">По гарантии мы принимаем аквариумное оборудование:</span></em></p>
    <ul style="text-align: justify;">
        <li><strong>AQUAEL</strong></li>
        <li><strong>Tetra</strong></li>
        <li><strong>Aqua Plus (Аква Плюс)</strong></li>
    </ul>
    <p style="text-align: justify;">Обратите внимание: для ремонта по гарантии необходимо предъявить заполненный гарантийный талон или кассовый чек. Этот талон заполняет продавец, когда вы приобретаете товар.</p>
    <p>При наличии запчастей ремонт займет не более двух недель. На отремонтированное оборудование мы тоже даем гарантию.</p>
    <p> </p>
    <p style="text-align: justify;"><strong><br /> </strong></p>
    <h2 style="text-align: center;"><strong><span style="color: #cc99ff;">Послегарантийный ремонт</span></strong></h2>
    <p>В послегарантийный ремонт мы принимаем любую аквариумную технику любых фирм <strong>Tetra, Aquael, JBL, Dennerle, Hagen, Sera, Jebo, Aquatlantis, Eheim, Juwel, Hydor </strong>и других.<br /><br />Стоимость и скорость ремонта зависит от характера повреждений. Небольшие поломки и легкие неполадки наши специалисты устранят сразу. На выполненный нами ремонт мы даем гарантию.<br /><br />В нашем центре работают опытные специалисты, которые отлично разбираются в тонкостях ремонта любой аквариумной техники отечественных и зарубежных производителей. Они обязательно помогут вам, если в вашем аквариуме сломался и не работает фильтр, компрессор, освещение, обогреватель или другое оборудование.</p>
    <p> </p>
    <h2 style="text-align: center;"><strong><span style="color: #cc99ff;">Время работы и как добраться</span></strong></h2>
    <p style="text-align: justify;">Мы рады видеть вас <strong>с 11:00 до 19:00 со вторника по субботу</strong>. Воскресенье и понедельник &ndash; выходные дни.</p>
    <p style="text-align: justify;"><br />Пункт приема в ремонт расположен в нашем зоомагазине «Немо» по адресу <strong>г. Москва, Проспект Мира, д. 182</strong> (здание стоит на перекрестке Проспекта Мира с улицей Бориса Галушкина), в пяти минутах ходьбы от станции метро ВДНХ.</p>
    <p><span style="font-size: 10px;"><br /></span></p>
    <p><strong>Телефон:<span style="color: #ff6600;"> </span></strong><strong><span style="color: #ff6600;">8-495-971-29-87</span></strong></p>
    <p> </p>
    <p><a style="font-size: 10px;" href="uploads/filemanager/8/32.pdf" target="blank"><span style="color: #0000ff;"><strong><span style="color: #ff00ff;">КАТАЛОГ ЗАПЧАСТЕЙ AQUAEL</span></strong></span></a></p>
</div>
<br />
<br />