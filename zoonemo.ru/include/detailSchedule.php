<p>Доставка осуществляется в течение 2-3 дней с момента оформления, при условии наличия товара в магазине</p>

<p>Заказы, поступающие в субботу или воскресенье, а также в праздничные дни, будут комплектоваться в понедельник на вторник или любой последующий день.</p>