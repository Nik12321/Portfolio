  <h1 class="page_title"></h1>
    <div class="spiski_pzf">
        <h1 class="zag">Уважаемые гости!<br> <br> Рады приветствовать вас на сайте Зоонемо!</h1>
        <p>&nbsp;</p>
        <p>Наш интернет-магазин представляет торговую сеть "ЗооНемо". Мы начали свою работу в 2000 году и, в настоящее время, в рамках нашей сети действуют два оффлайн-зоомагазина. Мы рады видеть Вас в наших магазинах у метро ВДНХ и метро Преображенская площадь, где найдутся разнообразные товары для Ваших домашних питомцев. В разделе «Наши магазины» Вы сможете узнать адрес каждой нашей торговой точки и подробное разъяснение, как до них добраться.</p>
        <p>&nbsp;</p>
        <p>Мы не одиноки на этой планете и живем в окружении растений и животных. Нет ничего удивительного в том, что многие люди открывают двери своего дома для того, чтобы впустить в него частичку живой природы, жить под одной крышей с кошкой или собакой, украсить интерьер квартиры или офиса аквариумом. При этом не стоит забывать, что все, кого мы приручили, нуждаются в особом уходе. Помочь Вам в этом смогут продавцы - консультанты наших магазинов.</p>
        <p>&nbsp;</p>
        <p>Мы предлагаем все, что необходимо для питания, лечения и ухода за домашними животными, обитателями аквариумов, террариумов, а также за птицами и грызунами. Для тех, кто занимается облагораживанием загородного участка, мы приготовили все, что нужно для обустройства мини-водоема и поддержания его в образцовом состоянии. Также мы предлагаем различный декор для оформления домашних прудов и фонтанов.</p>
        <p>&nbsp;</p>
        <p>В залах наших магазинов вас ждет большое разнообразие товаров, способных доставить море радости Вашим любимцам, а значит, и Вам самим. Будьте уверены - на каждый потраченный рубль они ответят любовью и благодарностью. А мы позаботимся о том, чтобы Ваше приобретение имело надлежащее качество. В наших магазинах представлены только высококачественные товары от разных производителей. Вы можете быть абсолютно уверены в надежности и безупречном качестве всей нашей продукции.</p>
        <p>&nbsp;</p>
        <p class="zag">С радостью ждем Вас в наших магазинах.</p>
        <p>&nbsp;</p>
        <p></p><div class="photoalbum">
            <a href="/custom/uploads/photoalbums/116/full/487.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/487.jpg" alt="103" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/486.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/486.jpg" alt="104" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/485.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/485.jpg" alt="105" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/484.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/484.jpg" alt="106" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/483.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/483.jpg" alt="107" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/482.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/482.jpg" alt="109" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/481.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/481.jpg" alt="319" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/480.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/480.jpg" alt="113" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/479.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/479.jpg" alt="114" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/128.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/128.jpg" alt="Зал с кормами (2)" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/127.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/127.jpg" alt="Зал с кормами (3)" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/126.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/126.jpg" alt="Зал с кормами" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/125.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/125.jpg" alt="Зал с лежаками" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/124.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/124.jpg" alt="Кошки + собаки игрушки" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/123.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/123.jpg" alt="кошки + собаки кожа" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/122.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/122.jpg" alt="Кошки + собаки корма (2)" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/121.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/121.jpg" alt="Кошки + собаки корма" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/120.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/120.jpg" alt="Кошки + собаки лакомства" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/119.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/119.jpg" alt="Кошки + собаки лежаки (2)" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/118.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/118.jpg" alt="Кошки + собаки лежаки" border="0"></a>
            <a href="/custom/uploads/photoalbums/116/full/117.jpg" rel="salute" target="_blank">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/117.jpg" alt="Кошки + собаки одежда" border="0"></a> </div>
        <p></p>
    </div>
    <br>
    <br>
