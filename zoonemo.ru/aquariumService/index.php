<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CModule::IncludeModule("fileman");
CMedialib::Init();
$APPLICATION->SetTitle("Обслуживание аквариумов");
?>
    <h1 class="page_title">Обслуживание аквариумов</h1>
<?php
$APPLICATION->IncludeComponent("bitrix:main.include", ".default", [
    "AREA_FILE_SHOW" => "file",
    "EDIT_TEMPLATE" => "",
    "COMPONENT_TEMPLATE" => ".default",
    "PATH" => "/include/aquariumService.php"
],
    false
);
$allCollections = CMedialibCollection::GetList(['arOrder' => ['NAME'=>'ASC'],'arFilter' => ['ACTIVE' => 'Y', "NAME" => "Наши аквариумы"]]);
$collectionIds = [];
foreach ($allCollections as $collection) {
    $collectionIds[] = $collection["ID"];
}
$arItems = CMedialibItem::GetList(['arCollections' => $collectionIds]);
foreach($arItems as &$arItem) {
    $newPhoto = CFile::ResizeImageGet(
        $arItem,
        ["width"=> 100, "height" => 100],
        BX_RESIZE_IMAGE_EXACT ,
        false,
        100
    );
    ($newPhoto["src"])
        ? $arItem["SRC"] = $newPhoto["src"]
        : $arItem["SRC"] = $arItem["PATH"];
}
unset($arItem);
?>
    <strong>
        <span style="color: #008000">Наши акваримы</span>
    </strong>
    <br><br>
    <div>
        <div class="photoalbum">
            <?php foreach($arItems as $arItem) :?>
                <a href="<?=$arItem["PATH"]?>">
                    <img src="<?=$arItem["SRC"]?>" alt="">
                </a>
            <?php endforeach;?>
        </div>
    </div>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>