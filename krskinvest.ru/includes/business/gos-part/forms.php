<h4>Формы ГЧП</h4>
<div class="gchp__form">
    <div class="form">
        <a href="http://old.economy.gov.ru/wps/wcm/connect/65795d06-38ab-4f9f-b945-a2dde3857c16/115-fz.pdf?MOD=AJPERES" target="_blank">115-ФЗ</a>
        <p>
            Концессионное соглашение (КС)
        </p>
    </div>
    <div class="form">
        <a href="http://static.kremlin.ru/media/acts/files/0001201507140006.pdf" target="_blank">224-ФЗ</a>
        <p>
            Соглашение о государственно-частном (муниципально-частном) партерстве (СГЧП)
        </p>
    </div>
    <div class="form">
        <a href="http://static.kremlin.ru/media/acts/files/0001201304080023.pdf" target="_blank">44-ФЗ</a>
        <p>
            “Классический” государственный контракт
        </p>
    </div>
    <div class="form">
        <a href="http://static.kremlin.ru/media/acts/files/0001201304080023.pdf" target="_blank">44-ФЗ</a>
        <p>
            Контракт жизненного цикла
        </p>
    </div>
    <div class="form">
        <a href="http://www.kremlin.ru/acts/bank/24149" target="_blank">135-ФЗ</a>
        <p>
            Договор аренды государственного имущества с инвестиционными обязательствами и др.
        </p>
    </div>
</div>
<br>