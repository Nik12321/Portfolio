<p>
	 С 28 августа 2020 года вступил в силу&nbsp;пакет федеральных законов о государственной поддержке предпринимательской деятельности в Арктической зоне Российской Федерации. Благодаря принятию пакета законов российская Арктика становится крупнейшей в России и мире экономической зоной с единым набором налоговых и административных преференций.&nbsp;
</p>
<p>
	 Получение статуса резидента Арктической зоны Российской Федерации установлено Федеральным законом от 13 июля 2020 года №193-ФЗ
</p>
<p>
	 «О государственной поддержке предпринимательской деятельности в Арктической зоне Российской Федерации».
</p>
<p>
	 В рамках подачи заявок на получение статуса резидента Арктической зоны Российской Федерации разработан&nbsp;<a href="https://arctic-russia.ru/" target="_blank">Инвестиционный портал Арктической зоны России</a><a href="https://arctic-russia.ru/" target="_blank">,</a> а также&nbsp;<a href="https://azrf.investvostok.ru/" target="_blank">кабинет инвестора</a>&nbsp;через который заявителем будет подаваться заявка на получения статуса.
</p>