<div class="arctic__img">
    <img src="/local/templates/.default/assets/img/arctic/white-bear.jpg" alt="">
</div>
<div class="arctic__blockquote--wrap">
    <div class="statistics__stats">
        <div class="statistics__stats--icon">
            <svg width="347" height="347">
                <use xlink:href="#coins" />
            </svg>
        </div>
        <div class="statistics__stats--content">
            <div class="statistics__stats--numb-box">
                <span class="statistics__stats--numb numb-line-second integer red">
                    12 123
                </span>
            </div>
            <div class="statistics__stats--numb-text">
                млрд руб.
            </div>
            <div class="statistics__stats--desc">
                инвестиционный потенциал
            </div>
            <!-- <div class="statistics__stats--desc-full"></div> -->
        </div>
    </div>

</div>