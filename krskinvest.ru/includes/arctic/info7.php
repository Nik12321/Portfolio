<div class="arctic__img">
    <img src="/local/templates/.default/assets/img/arctic/arctic-woman.jpg" alt="">
</div>
<div class="arctic__blockquote--wrap">
    <div class="arctic__blockquote md">
        <h4>
            с 1930 –х годов
        </h4>
        <p>
            Северные и арктические территории
            Красноярского края являются
            пионерами в промышленном и
            транспортном освоении региона
        </p>
    </div>
    <div class="arctic__blockquote md">
        <h4>
            с 1960 –х годов
        </h4>
        <p>
            Северные и арктические территории Красноярского края – форпост в освоении приполярной зоны
            Северного
            ледовитого океана и база развития восточного сектора Северного морского пути.
        </p>
    </div>
</div>