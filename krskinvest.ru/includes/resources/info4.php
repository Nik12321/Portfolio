<div class="statistics__stats--numb-box">
	<span class="statistics__stats--numb numb-line-first decimal1">
		158.7
	</span>
</div>
<div class="statistics__stats--numb-text">
	 млн. гектар
</div>
<div class="statistics__stats--desc">
	 площадь лесного фонда края
</div>
<div class="statistics__stats--desc-full">
	 более 40 % от общей площади лесного массива СФО
</div>
 <br>