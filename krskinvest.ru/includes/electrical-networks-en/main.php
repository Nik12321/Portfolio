<div class = "el-networks--text">
    <p>
        <span> Technological connection </span> is a complex service provided by network organizations to all
        interested parties to create the technical possibility of electricity consumption. It
        provides for the actual connection of consumer power receivers to facilities
        power grid facilities of grid organizations.
    </p>
    <p>
        The procedure for technological connection to electrical networks is established by the Rules for technological
        connection of power receivers of consumers of electrical energy, production facilities
        electrical energy, as well as power grid facilities belonging to grid organizations and
        to other persons approved by the Decree of the Government of the Russian Federation of December 27, 2004 No. 861.
    </p>
    <p>
        The procedure for determining the payment for technological connection is established by the Fundamentals of Pricing in the
        of regulated prices (tariffs) in the electric power industry, approved by the decree of the Government of the Russian
        Federation of December 29, 2011 No. 1178 "On pricing in the field of regulated prices (tariffs) in
        electric power industry ".
    </p>

    <h4> Technological connection is carried out in relation to: </h4>

    <ul>
        <li>
            objects commissioned for the first time;
        </li>
        <li>
            objects previously connected, the maximum capacity of which is increasing;
        </li>
        <li>
            objects for which the category of power supply reliability is changed;
        </li>
        <li>
            objects whose attachment point changes;
        </li>
        <li>
            objects for which the type of production activity changes without entailing a revision of the value
            maximum power, but changing the external power supply scheme of such power-receiving
            devices.
        </li>
    </ul>
</div>
<h3>
    The technological connection procedure consists of the following three stages:
</h3>