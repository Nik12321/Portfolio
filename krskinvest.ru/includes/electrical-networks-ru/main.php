<div class="el-networks--text">
    <p>
        <span>Технологическое присоединение</span> – комплексная услуга, оказываемая сетевыми организациями всем
        заинтересованным лицам для создания технической возможности потребления электрической энергии. Она
        предусматривает фактическое присоединение энергопринимающих устройств потребителя к объектам
        электросетевого хозяйства сетевых организаций.
    </p>
    <p>
        Порядок технологического присоединения к электрическим сетям установлен Правилами технологического
        присоедине­ния энергопринимающих устройств потребителей электрической энергии, объектов по производству
        электрической энергии, а также объектов электросетевого хозяйства, принадлежащих сетевым организациям и
        иным лицам, утвержденными постановлением Правительства Российской Федерации от 27 декабря 2004 г. № 861.
    </p>
    <p>
        Порядок определения платы за технологическое присоединение установлен Основами ценообразования в области
        регулируемых цен (тарифов) в электроэнергетике, утвержденными постановлением Правительства Российской
        Федерации от 29 декабря 2011 г. № 1178 "О ценообразовании в области регулируемых цен (тарифов) в
        электроэнергетике".
    </p>

    <h4>Технологическое присоединение осуществляется в отношении:</h4>

    <ul>
        <li>
            объектов, впервые вводимых в эксплуатацию;
        </li>
        <li>
            объектов, ранее присоединенных, максимальная мощность которых увеличивается;
        </li>
        <li>
            объектов, у которых изменяется категория надежности электроснабжения;
        </li>
        <li>
            объектов, у которых изменяется точка присоединения;
        </li>
        <li>
            объектов, у которых изменяется вид производственной деятельности, не влекущий пересмотр величины
            максимальной мощности, но изменяющий схему внешнего электроснабжения таких энергопринимающих
            устройств.
        </li>
    </ul>
</div>
<h3>
    Порядок технологического присоединения состоит из следующих трех этапов:
</h3>