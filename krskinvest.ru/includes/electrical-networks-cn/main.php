<div class ="el-networks--text">
    <p>
        <span>技術連接</span>是網絡組織向所有人提供的一項複雜服務
        有意者創造用電的技術可能性。它
        提供消費電力接收器與設施的實際連接
        電網組織的電網設施。
    </p>
    <p>
        電網技術連接的程序由技術規則規定
        電能消費者，生產設施的受電者的連接
        電力，以及屬於電網組織的電網設施以及
        2004年12月27日第861號俄羅斯聯邦政府法令核准的其他人士。
    </p>
    <p>
        確定技術連接費用的程序由定價基礎中的
        俄羅斯政府批准的電力行業的管制價格（電價）
        聯邦，2011年12月29日第1178號"關於規範價格（關稅）領域的定價
        電力工業"。
    </p>

    <h4>有關以下方面的技術連接：</h4>

    <ul>
        <li>
            首次調試的對象；
        </li>
        <li>
            先前連接的對象，其最大容量正在增加；
        </li>
        <li>
            電源可靠性類別已更改的對象；
        </li>
        <li>
            附著點發生變化的對象；
        </li>
        <li>
            更改生產活動類型的對象，這些生產活動不需要修改值
            最大功率，但更改了此類受電設備的外部電源方案
            設備。
        </li>
    </ul>
</div>
<h3>
    技術連接過程包括以下三個階段：
</h3>