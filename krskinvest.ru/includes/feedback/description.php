<div class="feedback__description">
	 Воспользовавшись данным сервисом, вы можете направить свои мнения, предложения, пожелания по вопросам развития инвестиционной деятельности в Красноярском крае и обеспечению благоприятного инвестиционного климата в регионе в адрес министра экономики и регионального развития Красноярского края Егора Васильева.
</div>
<div class="feedback__tel">
	<h4>
	Телефоны для справок: </h4>
	<p>
 <a href="tel:+73912493491">+7 (391) 249-34-91</a>, приемная министра экономики и регионального развития Красноярского края Егора Васильева
	</p>
</div>
<div class="feedback__social">
 <a target="_blank" href="https://www.instagram.com/krskinvest/"> <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/social/instagram.svg" alt=""> </a> <a target="_blank" href="https://www.facebook.com/groups/775818849230960/"> <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/social/facebook.svg" alt=""> </a>
</div>
<div class="feedback__order">
 <a href="http://krskinvest.ru/public/uploads/aaf69e2ce553922b5b743934da67223b.pdf" target="_blank">Порядок рассмотрения обращений</a>
</div>
 <br>