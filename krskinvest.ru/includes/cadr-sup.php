<!-- секция основной информации страницы --> <section class="cadr-sup">
<div class="container">
	<div class="cadr-sup__container">
		<h2>Кадровое обеспечение </h2>
		<div class="cadr-sup__content">
			<div class="cadr-sup__content--first">
				<div class="left--block">
					<div class="left-line-text">
						<p>
							 В целях создания благоприятного инвестиционного климата в Красноярском крае отработан механизм заполнения кадровой потребности инвестиционных проектов на всех этапах его реализации (строительство, ввод в эксплуатацию, инфраструктурное обеспечение) по двум направлениям:
						</p>
					</div>
					<div class="dots-text">
						<p>
 <b>Заполнение текущей кадровой потребности</b> <br>
 <br>
							 (<a href="http://" target="_blank">заявить о текущей кадровой потребности*</a><a target="_blank" href="http://">)</a> <br>
 <br>
						</p>
						<p class="small-text">
							 *услуга предоставляется в электронном виде через личный кабинет на Интерактивном портале агентства труда и занятости населения Красноярского края (для входа в личный кабинет необходимо зарегистрироваться в службе занятости населения или войти на Единый портал государственных услуг), а также при личном обращении в центр занятости населения (<a href="https://trud.krskstate.ru/czn/index" target="_blank">контактная информация</a>).
						</p>
					</div>
					<div class="dots-text">
						<p>
 <b>Перспективное взаимодействие в целях организации опережающего кадрового обеспечения</b> <br>
 <br>
							 (<a href="https://trud.krskstate.ru/content/опрос%20работодателей%20о%20кадровой%20потребности" target="_blank">заявить о перспективной кадровой потребности*</a>)
						</p>
					</div>
				</div>
				<div class="right--block">
					<div class="right-btn-block">
 <a class="btn-1" target="_blank" href="https://trud.krskstate.ru/employee/index"> Ознакомиться с банком соискателей в Красноярском крае </a> <a class="btn-2" target="_blank" href="https://trudvsem.ru/cv/search">
						Ознакомиться с банком соискателей на портале «Работа в России»</a>
					</div>
					<div class="right-text-block">
						<div class="content-text-block">
							<p>
								 В работе по обеспечению кадрами инвестиционного проекта службой занятости применяется индивидуальный подход по принципу «одного окна»: для взаимодействия с работодателем закрепляется ответственный центр занятости населения, который организует подбор кадров из всех территорий Красноярского края.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="cadr-sup__content--second">
				<h4>Алгоритм взаимодействия при обращении субъектов предпринимательства за поддержкой по вопросам кадрового обеспечения </h4>
 <a class="big-img" data-fancybox="gallery" href="<?=DEFAULT_TEMPLATE_PATH?>/img/structure.svg"> <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/structure.svg"> </a>
				<div class="second-text-block">
 <b>По всем вопросам кадрового обеспечения инвестиционных проектов необходимо обращаться:</b>
					<div class="dots-text">
						в центр занятости населения (<a href="https://trud.krskstate.ru/czn/index" target="_blank">контактная информация</a>)
					</div>
					<div class="dots-text">
						 в отдел по информационному сопровождению инвестиционных проектов агентства труда и занятости населения Красноярского края: <br>
						 телефон <a href="tel:+73912219890">(8 391) 221-98-90</a><br>
						 e-mail: <a href="mailto:invest@azn24.ru">invest@azn24.ru</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section>