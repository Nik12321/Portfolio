<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$ch = curl_init();
curl_setopt_array($ch, [
    CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => [
        'secret' => "6LceadkZAAAAADIhdF1Ty69ywmHKufSEuSs5BT17",
        'response' => $_POST["g-recaptcha-response"],
        'remoteip' => $_SERVER['REMOTE_ADDR']
    ],
    CURLOPT_RETURNTRANSFER => true
]);

$output = curl_exec($ch);
curl_close($ch);

$captcha_success = json_decode($output);
if ($captcha_success->success == false) {
    echo 0;
} else if ($captcha_success->success==true) {
    $email =  $_POST["email"];
    if(CModule::IncludeModule('subscribe')) {
        global $USER;
        $arFields = [
            "USER_ID" => (false),
            "FORMAT" => ("html"),
            "EMAIL" => $email,
            "ACTIVE" => "Y",
        ];
        $subscribeClass = new CSubscription;
        $ID = $subscribeClass->Add($arFields);
        if ($ID) {
            echo $ID;
        } else {
            echo "R";
        }
    } else {
        echo 0;
    }
}