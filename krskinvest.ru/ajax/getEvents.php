<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$mouths = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря'
];


global $DB;
$date = $_POST["date"];
$dateBefore = DateTime::createFromFormat('Y-m', $_POST["date"]);
$years = $dateBefore->format('Y');
$mounths = $dateBefore->format('m');

$arFilter = [
    "IBLOCK_ID" => 26,
    "ACTIVE" =>"Y",
    ">=PROPERTY_DATE_BEFORE" => date("Y-m-d", mktime(0,0,0, $mounths,1, $years)),
    "<PROPERTY_DATE_BEFORE" => date("Y-m-d", mktime(0,0,0,$mounths + 1,1, $years))
];

$colors = [];
$elements = CIBlockElement::GetList(
    false,
    ["IBLOCK_ID" => 26, "ACTIVE" =>"Y"],
    ["PROPERTY_TYPE"],
    false,
    ["PROPERTY_TYPE"]
);
while ($element = $elements->GetNext()) {
    if ($element["PROPERTY_TYPE_VALUE"]) {
        $colors[] = $element["PROPERTY_TYPE_VALUE"];
    }
}
$colors = array_unique($colors);
$colorsData = getColorsData($colors);

$allElements = [];
$elements = CIBlockElement::GetList(
    false,
    $arFilter,
    false,
    false,
    [
        "NAME",
        "ID",
        "DETAIL_PAGE_URL",
        "PREVIEW_PICTURE",
        "PROPERTY_DATE_BEFORE",
        "PROPERTY_DATE_AFTER",
        "PROPERTY_TYPE"
    ]
);
while($element = $elements->GetNext()) {
    if ($element["PROPERTY_DATE_BEFORE_VALUE"]) {
        $dateBefore = DateTime::createFromFormat('d.m.Y', $element["PROPERTY_DATE_BEFORE_VALUE"]);
        $array = [
            "title" => $element["NAME"],
            "date" => $element["PROPERTY_DATE_BEFORE_VALUE"],
            "url" => $element["DETAIL_PAGE_URL"],
            'start' => $dateBefore->format('Y-m-d'),
        ];
        if ($element["PREVIEW_PICTURE"]) {
            $previewPicture = CFile::GetFileArray($element["PREVIEW_PICTURE"]);
            $previewPicture = CFile::ResizeImageGet($previewPicture, ["width"=> 40, "height" => 40], BX_RESIZE_IMAGE_PROPORTIONAL, true, false);
            $array["logo"] = $previewPicture["src"];
        }
        if ($element["PROPERTY_TYPE_VALUE"]) {
            $array['color'] = $colorsData[$element["PROPERTY_TYPE_VALUE"]]["COLOR"]["UF_COLOR_CODE"];
        } else {
            $array['color'] = '#B5001F';
        }
        if ($element["PROPERTY_DATE_AFTER_VALUE"]) {
            $dateAfter = DateTime::createFromFormat('d.m.Y', $element["PROPERTY_DATE_AFTER_VALUE"]);
            $array['end'] =  $dateAfter->format('Y-m-d');
        }
        if ($element["PROPERTY_DATE_BEFORE_VALUE"]) {
            $date = $firstPart = $secondPart = "";
            $yearNumberOne = date("Y", strtotime($element["PROPERTY_DATE_BEFORE_VALUE"]));
            $monthNumberOne = date('n', strtotime($element["PROPERTY_DATE_BEFORE_VALUE"]))-1;
            $dayNumberOne = date("j", strtotime($element["PROPERTY_DATE_BEFORE_VALUE"]));
            $mouthNameOne = $mouths[$monthNumberOne];
            if ($element["PROPERTY_DATE_AFTER_VALUE"]) {
                $yearNumberTwo = date("Y", strtotime($element["PROPERTY_DATE_AFTER_VALUE"]));
                $monthNumberTwo = date('n', strtotime($element["PROPERTY_DATE_AFTER_VALUE"]))-1;
                $dayNumberTwo = date("j", strtotime($element["PROPERTY_DATE_AFTER_VALUE"]));
                $mouthNameTwo = $mouths[$monthNumberTwo];
                if ($yearNumberOne == $yearNumberTwo) {
                    if ($monthNumberOne == $monthNumberTwo) {
                        $date = $dayNumberOne . "-" . $dayNumberTwo . " " . $mouthNameOne;
                    } else {
                        $date = $dayNumberOne . " " . $mouthNameOne . " - " . $dayNumberTwo . " " . $mouthNameTwo;
                    }
                } else {
                    $date =  $dayNumberOne . " " . $mouthNameOne . " " . $yearNumberOne . " " . "года";
                    $date .=  " - " . $dayNumberTwo . " " . $mouthNameTwo . " " . $yearNumberTwo . " " . "года";
                }
            } else {
                $date =  $dayNumberOne . " " . $mouthNameOne;
            }
            $array["date"] = $date;
        }
        $allElements[] = $array;
    }
}

echo json_encode($allElements);