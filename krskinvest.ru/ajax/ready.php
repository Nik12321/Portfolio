<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$ch = curl_init();
curl_setopt_array($ch, [
    CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => [
        'secret' => "6LceadkZAAAAADIhdF1Ty69ywmHKufSEuSs5BT17",
        'response' => $_POST["g-recaptcha-response"],
        'remoteip' => $_SERVER['REMOTE_ADDR']
    ],
    CURLOPT_RETURNTRANSFER => true
]);

$output = curl_exec($ch);
curl_close($ch);

$captcha_success = json_decode($output);
if ($captcha_success->success == false) {
    echo 0;
} else if ($captcha_success->success==true) {
    if(CModule::IncludeModule("iblock")) {
        $files = [];
        $class = new \CIBlockElement;
        $fields = [
            "FIO" => $_POST["name"],
            "THEME" => $_POST["theme"],
            "ORGANIZATION" => $_POST["organization"],
            "EMAIL" => $_POST["email"],
            "ADDRESS" => $_POST["address"]
        ];
        if (count($_FILES)) {
            $fields["FILES"] = $_FILES;
            foreach ($_FILES as $file) {
                if (!empty($file['tmp_name'])) {
                    $files[] = CFile::SaveFile($file, 'form');
                }
            }
        }
        if ($class->Add(
            [
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID" =>  getIblockIdByCode("feedback"),
                "PROPERTY_VALUES"=> $fields,
                "NAME" => $_POST["phone"],
                "ACTIVE" => "Y",
                "PREVIEW_TEXT" => $_POST["description"]
            ]
        )) {
            $rsSites = CSite::GetByID("s1");
            $arSite = $rsSites->Fetch();
            $arEventFieldsSend = array(
                "PHONE" => ($_POST["phone"]) ? "Телефон: " . $_POST["phone"]: "",
                "FIO" => $_POST["name"],
                "THEME" => $_POST["theme"],
                "ORGANIZATION" => ($_POST["organization"]) ? "Организация (для юридических лиц): " . $_POST["organization"] : "",
                "EMAIL" => $_POST["email"],
                "ADDRESS" => ($_POST["phone"]) ? "Почтовый адрес: " . $_POST["address"]: "",
                "MESSAGE" => $_POST["description"],
                "DEFAULT_EMAIL_FROM" =>  $arSite["EMAIL"],
                "SITE_NAME" => "",
            );
            CEvent::SendImmediate("FEEDBACK_FORM_REQUEST", "s1", $arEventFieldsSend, "Y", "", $files);
            foreach ($files as $file) {
                CFile::Delete($file);
            }
            echo 1;
        } else {
            echo 0;
        };
    } else {
        echo 0;
    }
}