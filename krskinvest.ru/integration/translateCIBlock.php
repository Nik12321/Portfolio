<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
if (!$haveTranslation) {
    throw new Exception("Не подключен модуль перевода\nПроверьте его наличие в папке /bitrix/modules/tna.yandex.cloud.translate/");
}
if (!$_REQUEST["key"] || $_REQUEST["key"] != "AQVNyIV1o1Td5fsTAVbCr4MRH0pGrNY6uSz75LlS") {
    throw new Exception("Неверный ключ для перевода");
} else {
    CModule::IncludeModule('iblock');
    $globalStartTime = microtime(true);

    //Подключаем класс-переводчик
    $translateClass = YCTranslate::getInstance($_REQUEST["key"]);

    //Исходный язык
    $originalLang = "ru";

    //Язык, на который переводим:
    $translateLang = "zh";

    //ID инфоблока
    $iblockTr = 30;

    //Сюда вбить поля, которые нужно перевести
    $arFields = [
        'NAME',
    ];

    //Сюда вбить свойства, которые нужно перевести
    $arProperties = [
//        'LINK',
    ];

    $step = 1; //Начинаем перевод всегда с первой страницы
    $pageSize = 20; //Размер страницы
    while (1) {
        $arNavParams = [
            "nPageSize" => $pageSize,
            "iNumPage" => $step,
            'checkOutOfRange' => true
        ];
        $isComplete = false;
        $elements = CIBlockElement::GetList([], ["IBLOCK_ID" => $iblockTr], false, $arNavParams);
        $CIBlockElementClass = new CIBlockElement;
        while ($element = $elements->GetNextElement()) {
            $isComplete = true;
            $fields = $element->GetFields();
            $properties = $element->GetProperties();
            $curElementID = $fields['ID'];

            //Обновляем поля элемента
            $arLoadProductArray = [];
            foreach ($arFields as $field) {
                if ($fields[$field]) {
                    $translate = $translateClass->translate($fields[$field], $translateLang, $originalLang);
                    if ($translate) {
                        $arLoadProductArray[$field] = $translate[0]["text"];
                    }
                }
            }
            $CIBlockElementClass->Update($curElementID, $arLoadProductArray);

            //Обновляем свойства элемента
            foreach ($arProperties as $property) {
                if ($properties[$property]) {
                    switch ($properties[$property]["PROPERTY_TYPE"]) {
                        case "S":
                            $translate = $translateClass->translate($properties[$property]["VALUE"], $translateLang, $originalLang);
                            if ($translate) {
                                CIBlockElement::SetPropertyValuesEx($curElementID, false, [$properties[$property]["CODE"] =>  $translate[0]["text"]]);
                            }
                            break;
                        case "L":
                            break;
                    }
                }
            }
        }

        if (!$isComplete) {
            PR("COMPLETE!");
            break;
        } else {
            $step++;
        }
    }
    PR("Time taken for all steps: ". (microtime(true) - $globalStartTime) ."\n");
}