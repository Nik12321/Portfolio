<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Главная');
CModule::IncludeModule('highloadblock');
?>
    <main class="main">

        <!-- блок Навгитаров поддрежки -->
        <section class="navigator-support">
            <div class="container">
                <h2>Навигатор по мерам поддержки </h2>
                <form action="#" id="search-support">
                    <div class="navigator-support__main">
                        <div class="navigator-support__container">
                            <div class="navigator-support__main--item">
                                <?php
                                $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_kategoriipoluchatelya']])->fetch();
                                if (isset($hlblock['ID'])) {
                                    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                                    $entity_data_class = $entity->getDataClass();
                                    $elements = $entity_data_class::getList(['order' => ["UF_SORT" => "ASC"], 'select' => ['UF_XML_ID', 'UF_NAME']])->fetchAll();
                                    ?>
                                    <?php if (count($elements)) :?>
                                        <h3>Категория получателя</h3>
                                        <div id="category" class="btn-block">
                                            <?php foreach ($elements as $key => $element) :?>
                                                <input type="radio" class="radio-btn" id="<?=$element["UF_XML_ID"]?>" <?=(!$key) ? "hidden checked" : "hidden"?> name="category" value="<?=$element["UF_XML_ID"]?>">
                                                <label for="<?=$element["UF_XML_ID"]?>">
                                                    <span class="item-btn"><?=$element["UF_NAME"]?></span>
                                                </label>
                                            <?php endforeach;?>
                                        </div>
                                    <?php endif;?>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="navigator-support__main--item">
                                <?php
                                $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_razmerbiznesa']])->fetch();
                                if (isset($hlblock['ID'])) {
                                    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                                    $entity_data_class = $entity->getDataClass();
                                    $elements = $entity_data_class::getList(['order' => ["UF_SORT" => "ASC"], 'select' => ['UF_XML_ID', 'UF_NAME']])->fetchAll();
                                    ?>
                                    <?php if (count($elements)) :?>
                                        <h3>Размер бизнеса</h3>
                                        <div id="business" class="btn-block">
                                            <?php foreach ($elements as $key => $element) :?>
                                                <input type="radio" class="radio-btn" id="<?=$element["UF_XML_ID"]?>" <?=(!$key) ? "hidden checked" : "hidden"?> name="size" value="<?=$element["UF_XML_ID"]?>">
                                                <label for="<?=$element["UF_XML_ID"]?>">
                                                    <span class="item-btn"><?=$element["UF_NAME"]?></span>
                                                </label>
                                            <?php endforeach;?>
                                        </div>
                                    <?php endif;?>
                                    <?
                                }
                                ?>
                            </div>
                            <div class="navigator-support__main--item">
                                <div class="btn-block">
                                    <h3>Отрасль</h3>
                                    <div class="item-btn-choose" id="OpenModal">Выбрать</div>
                                </div>
                                <div class="xl-visible">
                                    <div style="height: 100%;" class="btn-block">
                                        <input type="submit" class="item-btn-last" value="Перейти к результатам">
                                    </div>
                                </div>
                            </div>
                            <div class="navigator-support__main--item xl-hidden">
                                <div style="height: 100%;" class="btn-block">
                                    <input type="submit" class="item-btn-last" value="Перейти к результатам">
                                </div>
                            </div>
                            <div class="business-support--modal modal" id="modalWindow">
                                <div class="modal--overlay">
                                    <div class="modal--content">
                                        <div class="modal__header">
                                            Выбрать отрасль
                                            <button class="modal__header--close" id="modalClose">
                                                <svg>
                                                    <use xlink:href="#close-btn">
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="modal__body">
                                            <div id="area_prop" class="business-support__branch-list">
                                                <?php
                                                $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_otrasl']])->fetch();
                                                if (isset($hlblock['ID'])) {
                                                    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                                                    $entity_data_class = $entity->getDataClass();
                                                    $elements = $entity_data_class::getList(['order' => ["UF_SORT" => "ASC"], 'select' => ['UF_XML_ID', 'UF_NAME']])->fetchAll();
                                                }
                                                ?>
                                                <?php foreach ($elements as $key => $element) :?>
                                                    <div class="check-item">
                                                        <input type="checkbox" id="<?=$element["UF_XML_ID"]?>"
                                                               value="<?=$element["UF_XML_ID"]?>">
                                                        <label for="<?=$element["UF_XML_ID"]?>"><?=$element["UF_NAME"]?></label>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <!-- блок о проекте -->
        <section class="ivents-main">
            <div class="container">
                <h2>Мероприятия</h2>

                <?php
                $APPLICATION->IncludeComponent(
                    "bitrix:news",
                    "events",
                    [
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "BROWSER_TITLE" => "-",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_ACTIVE_DATE_FORMAT" => "j M Y",
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",
                        "DETAIL_FIELD_CODE" => [
                            0 => "PREVIEW_TEXT",
                            1 => "PREVIEW_PICTURE",
                            2 => "DATE_ACTIVE_FROM",
                            3 => "",
                        ],
                        "DETAIL_PAGER_SHOW_ALL" => "N",
                        "DETAIL_PAGER_TEMPLATE" => "",
                        "DETAIL_PAGER_TITLE" => "Страница",
                        "DETAIL_PROPERTY_CODE" => [
                            0 => "",
                            1 => "SOURCE",
                            2 => "PHOTOS",
                            3 => "FILES",
                            4 => "TITLE",
                            5 => "FILES",
                            6 => "",
                        ],
                        "DETAIL_SET_CANONICAL_URL" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => getIblockIdByCode("events"),
                        "IBLOCK_TYPE" => "press_center",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "LIST_ACTIVE_DATE_FORMAT" => "j M Y",
                        "LIST_FIELD_CODE" => [
                            0 => "",
                            1 => "",
                        ],
                        "LIST_PROPERTY_CODE" => [
                            0 => "",
                            1 => "TITLE",
                            2 => "",
                        ],
                        "MESSAGE_404" => "",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "NEWS_COUNT" => "3",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "SEF_MODE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "USE_CATEGORIES" => "N",
                        "USE_FILTER" => "Y",
                        "USE_PERMISSIONS" => "N",
                        "USE_RATING" => "N",
                        "USE_REVIEW" => "N",
                        "USE_RSS" => "N",
                        "USE_SEARCH" => "N",
                        "USE_SHARE" => "N",
                        "COMPONENT_TEMPLATE" => "events",
                        "FILTER_NAME" => "",
                        "FILTER_FIELD_CODE" => [
                            0 => "",
                            1 => "",
                        ],
                        "FILTER_PROPERTY_CODE" => [
                            0 => "",
                            1 => "YEAR",
                            2 => "MOUNTH",
                            3 => "",
                        ],
                        "SEF_FOLDER" => "/press-center/events/",
                        "SEF_URL_TEMPLATES" => [
                            "news" => "",
                            "section" => "",
                            "detail" => "#ELEMENT_ID#/",
                        ]
                    ],
                    false
                );
                ?>
            </div>
            <div class="section--link"><a href="/press-center/events/">Все мероприятия</a></div>
        </section>

        <!-- блок новостей -->
        <section class="news__main">
            <div class="container">
                <h2>Новости</h2>
                <?php
                $APPLICATION->IncludeComponent(
                    "bitrix:news",
                    "news",
                    [
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "BROWSER_TITLE" => "-",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_ACTIVE_DATE_FORMAT" => "j M Y",
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",
                        "DETAIL_FIELD_CODE" => [
                            0 => "PREVIEW_TEXT",
                            1 => "PREVIEW_PICTURE",
                            2 => "DATE_ACTIVE_FROM",
                            3 => "",
                        ],
                        "DETAIL_PAGER_SHOW_ALL" => "N",
                        "DETAIL_PAGER_TEMPLATE" => "",
                        "DETAIL_PAGER_TITLE" => "Страница",
                        "DETAIL_PROPERTY_CODE" => [
                            0 => "",
                            1 => "SOURCE",
                            2 => "PHOTOS",
                            3 => "FILES",
                            4 => "TITLE",
                            5 => "FILES",
                            6 => "",
                        ],
                        "DETAIL_SET_CANONICAL_URL" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => getIblockIdByCode("news"),
                        "IBLOCK_TYPE" => "press_center",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "LIST_ACTIVE_DATE_FORMAT" => "j M Y",
                        "LIST_FIELD_CODE" => [
                            0 => "",
                            1 => "",
                        ],
                        "LIST_PROPERTY_CODE" => [
                            0 => "",
                            1 => "TITLE",
                            2 => "",
                        ],
                        "MESSAGE_404" => "",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "NEWS_COUNT" => "10",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "SEF_MODE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "USE_CATEGORIES" => "N",
                        "USE_FILTER" => "Y",
                        "USE_PERMISSIONS" => "N",
                        "USE_RATING" => "N",
                        "USE_REVIEW" => "N",
                        "USE_RSS" => "N",
                        "USE_SEARCH" => "N",
                        "USE_SHARE" => "N",
                        "COMPONENT_TEMPLATE" => "news",
                        "FILTER_NAME" => "",
                        "FILTER_FIELD_CODE" => [
                            0 => "",
                            1 => "",
                        ],
                        "FILTER_PROPERTY_CODE" => [
                            0 => "",
                            1 => "YEAR",
                            2 => "MOUNTH",
                            3 => "",
                        ],
                        "SEF_FOLDER" => "/press-center/news/",
                        "SEF_URL_TEMPLATES" => [
                            "news" => "",
                            "section" => "",
                            "detail" => "#ELEMENT_ID#/",
                        ]
                    ],
                    false
                );
                ?>
            </div>
            <div class="section--link"><a href="/press-center/news/">Все новости</a></div>
        </section>
        <!-- блок инвестиционных проектов -->
        <section class="invest-main">
            <div class="container">
                <h2>
                    Инвестиционные проекты
                </h2>
            </div>
            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:news",
                "investProjec",
                [
                    "ADD_ELEMENT_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_ACTIVE_DATE_FORMAT" => "j M Y",
                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                    "DETAIL_FIELD_CODE" => [
                        0 => "PREVIEW_TEXT",
                        1 => "PREVIEW_PICTURE",
                        2 => "DATE_ACTIVE_FROM",
                        3 => "",
                    ],
                    "DETAIL_PAGER_SHOW_ALL" => "N",
                    "DETAIL_PAGER_TEMPLATE" => "",
                    "DETAIL_PAGER_TITLE" => "Страница",
                    "DETAIL_PROPERTY_CODE" => [
                        0 => "",
                        1 => "SOURCE",
                        2 => "PHOTOS",
                        3 => "FILES",
                        4 => "TITLE",
                        5 => "FILES",
                        6 => "",
                    ],
                    "DETAIL_SET_CANONICAL_URL" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => getIblockIdByCode("investProjec"),
                    "IBLOCK_TYPE" => "press_center",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "LIST_ACTIVE_DATE_FORMAT" => "j M Y",
                    "LIST_FIELD_CODE" => [
                        0 => "",
                        1 => "",
                    ],
                    "LIST_PROPERTY_CODE" => [
                        0 => "",
                        1 => "TITLE",
                        2 => "",
                    ],
                    "MESSAGE_404" => "",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "NEWS_COUNT" => "10",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "SEF_MODE" => "Y",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "USE_CATEGORIES" => "N",
                    "USE_FILTER" => "Y",
                    "USE_PERMISSIONS" => "N",
                    "USE_RATING" => "N",
                    "USE_REVIEW" => "N",
                    "USE_RSS" => "N",
                    "USE_SEARCH" => "N",
                    "USE_SHARE" => "N",
                    "COMPONENT_TEMPLATE" => "investProjec",
                    "FILTER_NAME" => "",
                    "FILTER_FIELD_CODE" => [
                        0 => "",
                        1 => "",
                    ],
                    "FILTER_PROPERTY_CODE" => [
                        0 => "",
                        1 => "YEAR",
                        2 => "MOUNTH",
                        3 => "",
                    ],
                    "SEF_FOLDER" => "/krsk/invest-project/",
                    "SEF_URL_TEMPLATES" => [
                        "news" => "",
                        "section" => "",
                        "detail" => "#ELEMENT_ID#/",
                    ]
                ],
                false
            );
            ?>
            <div class="section--link"><a href="/krsk/invest-project/">Все проекты</a></div>
        </section>
        <!-- блок с слайдером брэндов -->
    </main>


    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var submitBtn = document.getElementById('search-support');
            submitBtn.addEventListener('submit', (e) => {
                e.preventDefault();
                let category = document.getElementById("category").querySelector("input:checked").getAttribute("value");
                let business = document.getElementById("business").querySelector("input:checked").getAttribute("value");
                console.log( document.getElementById("area_prop"));
                let area = document.getElementById("area_prop").querySelectorAll("input:checked");
                let areaValues = [];
                area.forEach(element => {
                    areaValues.push(element.getAttribute("value"));
                });
                let query = "?category=" + category + "&business=" + business;
                if (areaValues.length > 0) {
                    query += "&area=";
                    for (let i in areaValues) {
                        if (i == 0) {
                            query += areaValues[i];
                        } else {
                            query += "-" + areaValues[i];
                        }
                    }
                }
                window.location.href = "/business/business-support/search/" + query;
            });
        }); // end ready
    </script>


<?php require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>