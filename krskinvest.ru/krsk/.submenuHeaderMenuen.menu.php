<?php
$aMenuLinks = [
    [
        "About the region",
        "/krsk/about/",
        [],
        ["CLASS" => "region"],
        ""
    ],
    [
        "Investment climate",
        "/krsk/invest-climate/",
        [],
        ["CLASS" => "invest"],
        ""
    ],
    [
        "Investment projects",
        "/krsk/invest-project/",
        [],
        ["SVG" => "porfolio-coins"],
        ""
    ],
    [
        "Success stories",
        "/krsk/history-success/",
        [],
        ["SVG" => "darts"],
        ""
    ]
];
?>