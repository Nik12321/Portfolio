<?php
$aMenuLinks = [
    [
        "Natural resource potential",
        "/krsk/about/resources/",
        [],
        ["SVG" => "elka"],
        ""
    ],
    [
        "Economy",
        "/krsk/about/economic/",
        [],
        ["SVG" => "ruble"],
        ""
    ],
    [
        "Scientific and human resources",
        "/krsk/about/science/",
        [],
        ["SVG" => "universe"],
        ""
    ],
    [
        "Region for life",
        "/krsk/about/region-for-life/",
        [],
        ["SVG" => "family"],
        ""
    ],
    [
        "Local government",
        "/krsk/about/local-governments/",
        [],
        ["SVG" => "personal-house"],
        ""
    ],
];
?>