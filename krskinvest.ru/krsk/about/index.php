<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О регионе");
?>

    <!-- SVG Icons -->
    <svg style="display: none;" x="0px" y="0px" xmlns="http://www.w3.org/2000/svg"
         xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="personal-house" viewBox="0 0 57 62">
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M37.319 52.4046C36.7226 52.9886 35.9187 53.3227 35.0769 53.3382V59.1962C35.0769 59.7777 35.3079 60.3353 35.719 60.7464C36.1302 61.1576 36.6878 61.3885 37.2692 61.3885H54.8077C55.3891 61.3885 55.9468 61.1576 56.3579 60.7464C56.769 60.3353 57 59.7777 57 59.1962V28.5039C57.0005 28.2158 56.9442 27.9305 56.8344 27.6641C56.7246 27.3978 56.5633 27.1557 56.3598 26.9518L30.0522 0.644073C29.8485 0.439911 29.6066 0.277931 29.3402 0.16741C29.0739 0.0568895 28.7884 0 28.5 0C28.2116 0 27.9261 0.0568895 27.6598 0.16741C27.3934 0.277931 27.1515 0.439911 26.9478 0.644073L0.640157 26.9518C0.436703 27.1557 0.275445 27.3978 0.165603 27.6641C0.0557612 27.9305 -0.000511025 28.2158 3.49671e-06 28.5039V59.1962C3.49671e-06 59.7777 0.230978 60.3353 0.642115 60.7464C1.05325 61.1576 1.61087 61.3885 2.19231 61.3885H19.7308C20.3122 61.3885 20.8698 61.1576 21.281 60.7464C21.6921 60.3353 21.9231 59.7777 21.9231 59.1962V53.3381C21.0825 53.3219 20.2798 52.9879 19.6841 52.4046C19.0733 51.8065 18.7301 50.9953 18.7301 50.1495V39.7205C18.7301 38.2572 19.3238 36.8538 20.3805 35.8191C21.4373 34.7843 22.8705 34.203 24.365 34.203H32.6381C34.1326 34.203 35.5658 34.7843 36.6226 35.8191C37.6793 36.8538 38.273 38.2572 38.273 39.7205V50.1495C38.273 50.9953 37.9298 51.8065 37.319 52.4046ZM34.5896 29.5233C34.8881 28.7666 35.0324 27.9578 35.014 27.1445C34.9779 25.5489 34.3186 24.0307 33.1771 22.9151C32.0357 21.7995 30.5029 21.1751 28.9069 21.1755C27.3108 21.1759 25.7783 21.8011 24.6374 22.9173C23.4966 24.0334 22.838 25.5519 22.8027 27.1476C22.7847 27.9609 22.9294 28.7696 23.2283 29.5262C23.5271 30.2828 23.9741 30.9721 24.543 31.5535C25.1119 32.135 25.7913 32.597 26.5412 32.9123C27.2911 33.2276 28.0964 33.3899 28.9099 33.3897C29.7234 33.3895 30.5287 33.2268 31.2784 32.9111C32.0281 32.5954 32.7072 32.1331 33.2759 31.5514C33.8445 30.9696 34.2911 30.2801 34.5896 29.5233Z" />
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M50.4235 6.57512V21.9213L41.6543 13.152V6.57512C41.6543 5.99368 41.8853 5.43606 42.2964 5.02492C42.7075 4.61379 43.2652 4.38281 43.8466 4.38281H48.2312C48.8127 4.38281 49.3703 4.61379 49.7814 5.02492C50.1926 5.43606 50.4235 5.99368 50.4235 6.57512Z" />
            </symbol>
        </defs>
    </svg>

    <!-- секция основной информации страницы -->
    <section class="about">
        <div class="container">
            <h2>Красноярский край</h2>
            <div class="about--wrapper">
                <div class="about--col">
                    <div class="box xl-hidden">
                        <div class="box--icon">
                            <svg>
                                <use xlink:href="#area" />
                            </svg>
                        </div>
                        <div class="box--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/about/info1.php"]);?>
                        </div>
                    </div>

                    <div class="box sm-xl-hidden">
                        <div class="box--icon">
                            <svg>
                                <use xlink:href="#flag" />
                            </svg>
                        </div>
                        <div class="box--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/about/info2.php"]);?>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box--icon">
                            <svg>
                                <use xlink:href="#mankind" />
                            </svg>
                        </div>
                        <div class="box--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/about/info4.php"]);?>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box--icon">
                            <svg>
                                <use xlink:href="#clock" />
                            </svg>
                        </div>
                        <div class="box--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/about/info3.php"]);?>
                        </div>
                    </div>
                </div>

                <div class="about--col">
                    <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/about/krai.svg" alt="">
                    <div class="box sm-hidden">
                        <div class="box--icon">
                            <svg>
                                <use xlink:href="#flag" />
                            </svg>
                        </div>
                        <div class="box--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/about/info2.php"]);?>
                        </div>
                    </div>
                </div>

                <div class="about--col xl-visible">
                    <div class="box">
                        <div class="box--icon">
                            <svg>
                                <use xlink:href="#area" />
                            </svg>
                        </div>
                        <div class="box--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/about/info1.php"]);?>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box--icon">
                            <svg>
                                <use xlink:href="#flag" />
                            </svg>
                        </div>
                        <div class="box--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/about/info2.php"]);?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "aboutSection",
                [
                    "ALLOW_MULTI_SELECT" => "N",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => [],
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "submenuHeaderMenu" . LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "COMPONENT_TEMPLATE" => "headerMenu"
                ],
                false
            );
            ?>
        </div>
    </section>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>