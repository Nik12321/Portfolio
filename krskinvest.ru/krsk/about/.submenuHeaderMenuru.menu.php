<?php
$aMenuLinks = [
    [
        "Природно-ресурсный потенциал",
        "/krsk/about/resources/",
        [],
        ["SVG" => "elka"],
        ""
    ],
    [
        "Экономика",
        "/krsk/about/economic/",
        [],
        ["SVG" => "ruble"],
        ""
    ],
    [
        "Научный и кадровый потенциал",
        "/krsk/about/science/",
        [],
        ["SVG" => "universe"],
        ""
    ],
    [
        "Край для жизни",
        "/krsk/about/region-for-life/",
        [],
        ["SVG" => "family"],
        ""
    ],
    [
        "Местное самоуправление",
        "/krsk/about/local-governments/",
        [],
        ["SVG" => "personal-house"],
        ""
    ],
];
?>