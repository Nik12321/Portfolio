<?php
$aMenuLinks = [
    [
        "自然資源潛力",
        "/krsk/about/resources/",
        [],
        ["SVG" => "elka"],
        ""
    ],
    [
        "經濟",
        "/krsk/about/economic/",
        [],
        ["SVG" => "ruble"],
        ""
    ],
    [
        "科學和人力資源",
        "/krsk/about/science/",
        [],
        ["SVG" => "universe"],
        ""
    ],
    [
        "生命的邊緣",
        "/krsk/about/region-for-life/",
        [],
        ["SVG" => "family"],
        ""
    ],
    [
        "地方政府",
        "/krsk/about/local-governments/",
        [],
        ["SVG" => "personal-house"],
        ""
    ],
];
?>