<?php
$aMenuLinks = [
    [
        "О регионе",
        "/krsk/about/",
        [],
        ["CLASS" => "region"],
        ""
    ],
    [
        "Инвестиционный климат",
        "/krsk/invest-climate/",
        [],
        ["CLASS" => "invest"],
        ""
    ],
    [
        "Инвестиционные проекты",
        "/krsk/invest-project/",
        [],
        ["SVG" => "porfolio-coins"],
        ""
    ],
    [
        "Истории успеха",
        "/krsk/history-success/",
        [],
        ["SVG" => "darts"],
        ""
    ]
];
?>