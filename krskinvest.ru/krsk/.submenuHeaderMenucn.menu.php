<?php
$aMenuLinks = [
    [
        "關於該地區",
        "/krsk/about/",
        [],
        ["CLASS" => "region"],
        ""
    ],
    [
        "投資環境",
        "/krsk/invest-climate/",
        [],
        ["CLASS" => "invest"],
        ""
    ],
    [
        "投資項目",
        "/krsk/invest-project/",
        [],
        ["SVG" => "porfolio-coins"],
        ""
    ],
    [
        "成功的故事",
        "/krsk/history-success/",
        [],
        ["SVG" => "darts"],
        ""
    ]
];
?>