<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?>
    <!-- блок новостей -->
    <section class="news">
        <!-- Контейнер для новостей -->
        <div class="container">
            <h2>
                Новости
                <button class="news__subscribe" id="OpenModalSubscribe">

                    <svg width="53" height="53" viewBox="0 0 53 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6.625 0C4.86794 0 3.18285 0.697989 1.94042 1.94042C0.697989 3.18285 0 4.86794 0 6.625L0 46.375C0 48.1321 0.697989 49.8172 1.94042 51.0596C3.18285 52.302 4.86794 53 6.625 53H46.375C48.1321 53 49.8172 52.302 51.0596 51.0596C52.302 49.8172 53 48.1321 53 46.375V6.625C53 4.86794 52.302 3.18285 51.0596 1.94042C49.8172 0.697989 48.1321 0 46.375 0L6.625 0ZM11.5938 8.28125C10.7152 8.28125 9.87267 8.63024 9.25146 9.25146C8.63024 9.87267 8.28125 10.7152 8.28125 11.5938C8.28125 12.4723 8.63024 13.3148 9.25146 13.936C9.87267 14.5573 10.7152 14.9062 11.5938 14.9062C18.622 14.9063 25.3624 17.6982 30.3321 22.6679C35.3018 27.6376 38.0937 34.378 38.0938 41.4062C38.0938 42.2848 38.4427 43.1273 39.064 43.7485C39.6852 44.3698 40.5277 44.7188 41.4062 44.7188C42.2848 44.7188 43.1273 44.3698 43.7485 43.7485C44.3698 43.1273 44.7188 42.2848 44.7188 41.4062C44.7188 23.1113 29.8887 8.28125 11.5938 8.28125ZM11.5938 21.5312C10.7152 21.5312 9.87267 21.8802 9.25146 22.5015C8.63024 23.1227 8.28125 23.9652 8.28125 24.8438C8.28125 25.7223 8.63024 26.5648 9.25146 27.186C9.87267 27.8073 10.7152 28.1562 11.5938 28.1562C15.1079 28.1563 18.4781 29.5522 20.9629 32.0371C23.4478 34.5219 24.8437 37.8921 24.8438 41.4062C24.8438 42.2848 25.1927 43.1273 25.814 43.7485C26.4352 44.3698 27.2777 44.7188 28.1562 44.7188C29.0348 44.7188 29.8773 44.3698 30.4985 43.7485C31.1198 43.1273 31.4688 42.2848 31.4688 41.4062C31.4687 36.1351 29.3748 31.0798 25.6475 27.3525C21.9202 23.6252 16.8649 21.5313 11.5938 21.5312ZM13.25 44.7188C14.5678 44.7187 15.8316 44.1953 16.7634 43.2634C17.6953 42.3316 18.2188 41.0678 18.2188 39.75C18.2188 38.4322 17.6953 37.1684 16.7634 36.2366C15.8316 35.3047 14.5678 34.7813 13.25 34.7812C11.9322 34.7813 10.6684 35.3047 9.73656 36.2366C8.80474 37.1684 8.28125 38.4322 8.28125 39.75C8.28125 41.0678 8.80474 42.3316 9.73656 43.2634C10.6684 44.1953 11.9322 44.7188 13.25 44.7188Z" fill="#C80022"/>
                    </svg>

                </button>
            </h2>
            <?php
            $APPLICATION->IncludeComponent(
	"bitrix:news", 
	"news", 
	array(
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j M Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "PREVIEW_PICTURE",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"DETAIL_PAGER_SHOW_ALL" => "N",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "SOURCE",
			2 => "PHOTOS",
			3 => "FILES",
			4 => "TITLE",
			5 => "FILES",
			6 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => getIblockIdByCode("news"),
		"IBLOCK_TYPE" => "press_center",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LIST_ACTIVE_DATE_FORMAT" => "j M Y",
		"LIST_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "TITLE",
			2 => "",
		),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "Y",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_REVIEW" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "news",
		"FILTER_NAME" => "",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "YEAR",
			2 => "MOUNTH",
			3 => "",
		),
		"SEF_FOLDER" => "/press-center/news/",
		"SEF_URL_TEMPLATES" => array(
			"news" => "/press-center/news/",
			"section" => "",
			"detail" => "#ELEMENT_ID#/",
		)
	),
	false
            );
            ?>
        </div>
    </section>
    <div class="modal news__subscribe--modal" id="modalWindowSubscribe">
        <div class="modal--overlay">
            <div class="modal--content">
                <div class="modal__header">
                    Подписаться на новости
                    <button class="modal__header--close" id="modalCloseSubscribe">
                        <svg>
                            <use xlink:href="#close-btn">
                        </svg>
                    </button>
                </div>
                <div class="modal__body">
                    <div id="formError" class="form--error hide">
                        Возникла ошибка во время обработки запроса
                    </div>
                    <div id="formSuccess" class="form--success hide">
                        Вы успешно подписались на новости!
                    </div>
                    <form action="#" id="form">
                        <label>
                            <span>Введите ваш е-mail:</span>
                            <input id="email" placeholder="e-mail..." type="email" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                   required>
                            <div id="captcha" style="text-align: center !important;"></div>
                        </label>
                        <!-- capch -->
                        <input type="submit" value="Подписаться">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var onloadCallback = function() {
            grecaptcha.render('captcha', {
                'sitekey' : '6LceadkZAAAAAKnHZpiIsqrai5B4L_XcAWHFBy6c'
            });
            // document.getElementById("captcha").firstChild.style.width = "100%";
        };
    </script>
    <script>
        const form = document.getElementById("form"),
            msgError = document.getElementById("formError"),
            msgSuccess = document.getElementById("formSuccess"),
            email = document.getElementById("email"),
            openModalBtn = document.getElementById("OpenModalSubscribe");
        form.addEventListener("submit", (e) => {
            if (!grecaptcha.getResponse()) {
                alert('Вы не заполнили поле "Я не робот"!');
                e.preventDefault();
            } else if (email.value) {
                e.preventDefault();
                var formData = new FormData();
                formData.append("g-recaptcha-response",  grecaptcha.getResponse());
                formData.append("email",  email.value);
                var xmlHttp = new XMLHttpRequest();
                xmlHttp.onreadystatechange = function() {
                    if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                        if (xmlHttp.responseText == "R" || xmlHttp.responseText) {
                            form.classList.add('hide');
                            msgError.classList.add('hide');
                            setTimeout(
                                function () {
                                    form.parentNode.removeChild(form);
                                    openModalBtn.remove();
                                },
                                250
                            );
                            msgSuccess.classList.remove("hide")
                        } else {
                            msgError.classList.remove('hide');
                        }
                    }
                }
                xmlHttp.open("post", "/ajax/addSubscriber.php");
                xmlHttp.send(formData);
            }
        });
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>