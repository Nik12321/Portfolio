<?php
$aMenuLinks = [
    [
        "新聞",
        "/press-center/news/",
        [],
        ["SVG" => "megafon"],
        ""
    ],
    [
        "活動",
        "/press-center/events/",
        [],
        ["SVG" => "calendar"],
        ""
    ],
    [
        "媒體資料",
        "/press-center/media-materials/",
        [],
        ["SVG" => "images"],
        ""
    ],
    [
        "有用的鏈接",
        "/press-center/useful-links/",
        [],
        ["SVG" => "link"],
        ""
    ]
];
?>