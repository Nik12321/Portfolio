<?php
$aMenuLinks = [
    [
        "Новости",
        "/press-center/news/",
        [],
        ["SVG" => "megafon"],
        ""
    ],
    [
        "Мероприятия",
        "/press-center/events/",
        [],
        ["SVG" => "calendar"],
        ""
    ],
    [
        "Медиаматериалы",
        "/press-center/media-materials/",
        [],
        ["SVG" => "images"],
        ""
    ],
    [
        "Полезные ссылки",
        "/press-center/useful-links/",
        [],
        ["SVG" => "link"],
        ""
    ]
];
?>