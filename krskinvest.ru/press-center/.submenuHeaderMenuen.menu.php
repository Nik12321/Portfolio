<?php
$aMenuLinks = [
    [
        "News",
        "/press-center/news/",
        [],
        ["SVG" => "megafon"],
        ""
    ],
    [
        "Activity",
        "/press-center/events/",
        [],
        ["SVG" => "calendar"],
        ""
    ],
    [
        "Media materials",
        "/press-center/media-materials/",
        [],
        ["SVG" => "images"],
        ""
    ],
    [
        "Useful links",
        "/press-center/useful-links/",
        [],
        ["SVG" => "link"],
        ""
    ]
];
?>