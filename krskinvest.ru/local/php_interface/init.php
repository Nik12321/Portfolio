<?php
$haveTranslation = false;
if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/tna.yandex.cloud.translate/classes/general/YCTranslate.php')) {
    require_once($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/tna.yandex.cloud.translate/classes/general/YCTranslate.php');
    $haveTranslation = true;
}
define("DEFAULT_TEMPLATE_PATH", "/local/templates/.default/assets/");
CModule::IncludeModule("iblock");
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');

function PR($o, $toString = false, $ltf = false)
{
    $bt_src = debug_backtrace();
    if ($ltf) {
        $bt = $bt_src[1];
    } else {
        $bt = $bt_src[0];
    }
    $dRoot = $_SERVER["DOCUMENT_ROOT"];
    $dRoot = str_replace("/", "\\", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    $dRoot = str_replace("\\", "/", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    $output = '<div style="font-size:9pt; color:#000; background:#fff; border:1px dashed #000;">';
    $output .= '<div style="padding:3px 5px; background:#99CCFF; font-weight:bold;">File: ' . $bt["file"] . ' [' . $bt["line"] . '] ' . date("d.m.Y H:i:s") . '</div>';
    $output .= '<pre style="padding:10px;">' . var_export($o, true) . '</pre>';
    $output .= '</div>';
    if ($toString) {
        return $output;
    }
    echo $output;
}

function logToFile($filepath, $var, $unlink = false)
{
    $logFile = $_SERVER["DOCUMENT_ROOT"].$filepath;
    $dirpath = dirname($logFile);
    if(!file_exists($dirpath)) {
        mkdir($dirpath);
    }
    if($unlink) {
        unlink($logFile);
    }
    file_put_contents($logFile, print_r($var,true) . "\n\n", FILE_APPEND);
}

function getIblockIdByCode($code)
{
    $id = CIBlock::GetList([], ['CODE' => $code], false)->Fetch()["ID"];
    if ($id) {
        return $id;
    } else {
        return false;
    }
}

function getColorsData($colorsCode)
{
    if (is_array($colorsCode) && count($colorsCode)) {
        $colorsIds = $colorsData = $result = [];
        $typesHLBlockId = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_tipmeropriyatiya']])->fetch();
        $colorsHLBlockId = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'colors']])->fetch();

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($typesHLBlockId)->getDataClass();
        $types = $entity::getList(['filter' => ["UF_XML_ID" => $colorsCode], 'select' => ['UF_XML_ID', 'UF_NAME', "UF_COLOR"]])->fetchAll();
        foreach ($types as $type) {
            if ($type["UF_COLOR"]) {
                $colorsIds[] = $type["UF_COLOR"];
            }
        }

        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($colorsHLBlockId)->getDataClass();
        $colors = $entity::getList(['filter' => ["ID" => $colorsIds], 'select' => ['UF_NAME', 'UF_COLOR_CODE', "ID"]])->fetchAll();
        foreach ($colors as $color) {
            $colorsData[$color["ID"]] = $color;
        }
        foreach ($types as &$type) {
            if ($type["UF_COLOR"] && $colorsData[$type["UF_COLOR"]]) {
                $result[$type["UF_XML_ID"]] = [
                    "COLOR" => $colorsData[$type["UF_COLOR"]],
                    "NAME" => $type["UF_NAME"]
                ];
            }
        }
        return $result;
    } else {
        return false;
    }
}

