document.addEventListener("DOMContentLoaded", ready);
BX.addCustomEvent('onAjaxSuccess', ready);
function ready() {
    const years = document.querySelectorAll(".checkYear"),
        yearFilter = document.querySelector("#changeYear").firstElementChild.children,
        mouths = document.querySelectorAll(".checkMounth"),
        mouthFilter = document.querySelector("#changeMounth").firstElementChild.children;
    selectValue(years, yearFilter, "select");
    selectValue(mouths, mouthFilter, "select");
    filterElements(years, yearFilter,true);
    filterElements(mouths, mouthFilter);
}

function filterElements(items, options, removeAnotherValue = false) {
    items.forEach((item) => {
        let itemValue = item.getAttribute("value");
        item.addEventListener("click", () => {
            for (let j = 0; j < options.length; j++) {
                let optionValue = options[j].getAttribute("value");
                if (optionValue != itemValue) {
                    options[j].selected = false;
                }
                else {
                    if(removeAnotherValue) {
                        removeSelectValue("#changeMounth");
                    }
                    options[j].selected = true;
                    let btnName = options[j].innerHTML;
                    document.querySelector('[name="' + btnName + '"]').click();
                }
            }
        });
    });
}

    function selectValue(items, options, selector) {
    let isHaveSelectedItems = false;
    for (let i = 0; i < options.length; i++) {
        if (options[i].selected) {
            if (options[i].innerHTML){
                let item = document.querySelector('[name="' +  options[i].innerHTML + '"]');
                if (item) {
                    item.parentElement.querySelector("a[value='" +  options[i].getAttribute("value") + "'").classList.add(selector);
                }
            }
        }
    }
}

function removeSelectValue(optionsSelector) {
    let options = document.querySelector(optionsSelector).firstElementChild.children;
    for (let i = 0; i < options.length; i++) {
        options[i].selected = false;
    }
}