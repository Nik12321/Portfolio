<?php
global $arrFilter;
$arResult["INDUSTRY"] = $arResult["STATUS"] = $arResult["COMPLEX_PROJECT"] = [];
$properties = CIBlockElement::GetList(
    ["PROPERTY_INDUSTRY" => "ASC"],
    ['IBLOCK_ID' => $arParams["IBLOCK_ID"]],
    ["PROPERTY_INDUSTRY"]
);
while($property = $properties->Fetch()) {
    $arResult["INDUSTRY"][$property['PROPERTY_INDUSTRY_ENUM_ID']] = $property["PROPERTY_INDUSTRY_VALUE"];
}

$properties = CIBlockElement::GetList(
    ["PROPERTY_STATUS" => "ASC"],
    ['IBLOCK_ID' => $arParams["IBLOCK_ID"]],
    ["PROPERTY_STATUS"]
);
while($property = $properties->Fetch()) {
    $arResult["STATUS"][$property['PROPERTY_STATUS_ENUM_ID']] = $property["PROPERTY_STATUS_VALUE"];
}

$properties = CIBlockElement::GetList(
    ["PROPERTY_COMPLEX_PROJECT" => "ASC"],
    ['IBLOCK_ID' => $arParams["IBLOCK_ID"]],
    ["PROPERTY_COMPLEX_PROJECT"]
);
while($property = $properties->Fetch()) {
    $arResult["COMPLEX_PROJECT"][$property['PROPERTY_COMPLEX_PROJECT_ENUM_ID']] = $property["PROPERTY_COMPLEX_PROJECT_VALUE"];
}
