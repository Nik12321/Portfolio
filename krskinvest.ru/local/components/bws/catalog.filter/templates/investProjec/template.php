<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
CAjax::GetForm('name="ajaxform" action="formtest.php" method="POST"', 'comp_'.$arParams['AJAX_ID'], $arParams['AJAX_ID'])
?>

<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
    <?foreach($arResult["ITEMS"] as $arItem):
        if(array_key_exists("HIDDEN", $arItem)):
            echo $arItem["INPUT"];
        endif;
    endforeach;?>
    <div class="filter-body">
        <div class="filter-body-col-3">
            <div id="changeStatus" style="display:none">
                <?=$arResult["ITEMS"]["PROPERTY_59"]["INPUT"]?>
            </div>
            <div class="filter-body-head">
                Статус
            </div>
            <select id="status" class="filter-body-select">
                <option class="checkStatus" name="(все)" disabled hidden>Выберите</option>
                <?foreach($arResult["STATUS"] as $key => $propertyValue) :?>
                    <option class="checkStatus"  name="<?=$propertyValue?>" value="<?=$key?>"><?=$propertyValue?></option>
                <?endforeach;?>
            </select>
        </div>
        <div class="filter-body-col-3">
            <div id="changeComplexProject" style="display:none">
                <?=$arResult["ITEMS"]["PROPERTY_60"]["INPUT"]?>
            </div>
            <div class="filter-body-head">
                Комплексный инвестиционный проект
            </div>
            <select id="complexProject" class="filter-body-select">
                <option class="checkComplexProject" name="(все)" disabled hidden>Выберите</option>
                <?foreach($arResult["COMPLEX_PROJECT"] as $key => $propertyValue) :?>
                    <option class="checkComplexProject"  name="<?=$propertyValue?>" value="<?=$key?>"><?=$propertyValue?></option>
                <?endforeach;?>
            </select>
        </div>
        <div class="filter-body-col-3">
            <div id="changeIndustry" style="display:none">
                <?=$arResult["ITEMS"]["PROPERTY_52"]["INPUT"]?>
            </div>
            <div class="filter-body-head">
                Отрасль
            </div>
            <select id="industry" class="filter-body-select">
                <option class="checkIndustry" name="(все)" disabled hidden>Выберите</option>
                <?foreach($arResult["INDUSTRY"] as $key => $propertyValue) :?>
                    <option class="checkIndustry"  name="<?=$propertyValue?>" value="<?=$key?>"><?=$propertyValue?></option>
                <?endforeach;?>
            </select>
        </div>
    </div>
    <input type="hidden" name="set_filter" value="Y" />&nbsp;
    <div class="filter-body">
        <div class="filter-body-col-2">
            <div class="filter-body-submit">
                <input name="set_filter" type="submit" value="Применить">
            </div>
        </div>
        <div class="filter-body-col-2">
            <div class="filter-body-reset">
                <input type="reset" value="Сбросить" onclick="document.location='<?=str_replace("index.php", "", $_SERVER["SCRIPT_NAME"])?>'">
            </div>
        </div>
    </div>
</form>