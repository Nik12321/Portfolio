<?php
global $arrFilter;
$arResult["MOUNTHS"] = $arResult["YEARS"] = [];
$properties = CIBlockElement::GetList(
    ["PROPERTY_YEAR" => "ASC"],
    ['IBLOCK_ID' => $arParams["IBLOCK_ID"], "!ID" => $arParams["LAST_ITEM"], "ACTIVE" => "Y"],
    ["PROPERTY_YEAR"]
);
while($property = $properties->Fetch()) {
    $arResult["YEARS"][$property['PROPERTY_YEAR_ENUM_ID']] = $property["PROPERTY_YEAR_VALUE"];
}
if (count($arrFilter)) {
    $years = $arrFilter["PROPERTY"]["YEAR"];
    $properties = CIBlockElement::GetList(
        false,
        ['IBLOCK_ID'=>$arParams["IBLOCK_ID"], "PROPERTY" => ["YEAR" => $years], "ACTIVE" => "Y", "!ID" => $arParams["LAST_ITEM"],],
        ["PROPERTY_MOUNTH"]
    );
    while($arItem = $properties->Fetch()) {
        $arResult["MOUNTHS"][] = $arItem["PROPERTY_MOUNTH_VALUE"];
    }
} else {
    $properties = CIBlockElement::GetList(
        false,
        ['IBLOCK_ID' => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "!ID" => $arParams["LAST_ITEM"],],
        ["PROPERTY_MOUNTH"]
    );
    while($arItem = $properties->Fetch()) {
        $arResult["MOUNTHS"][] = $arItem["PROPERTY_MOUNTH_VALUE"];
    }
}