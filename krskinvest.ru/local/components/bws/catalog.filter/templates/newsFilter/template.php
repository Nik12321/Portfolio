<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
CAjax::GetForm('name="ajaxform" action="formtest.php" method="POST"', 'comp_'.$arParams['AJAX_ID'], $arParams['AJAX_ID'])
?>
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
    <!-- Выбор года -->
    <div class="news--archive">
        <div id="changeYear" style="display:none">
            <?=$arResult["ITEMS"]["PROPERTY_1"]["INPUT"]?>
        </div>
        <?foreach($arResult["YEARS"] as $key => $propertyValue) :?>
            <?if ($propertyValue != "(все)"):?>
                <a class="checkYear" value="<?=$key?>"><?=$propertyValue?></a>
                <button style="display:none" name="<?=$propertyValue?>" value="<?=$key?>"><?=$propertyValue?></button>
            <?endif;?>
        <?endforeach;?>
    </div>
    <!-- Выбор месяца -->
    <div class="news--archive container-mouth">
        <?foreach($arResult["ITEMS"]["PROPERTY_2"]["LIST"] as $key => $propertyValue) :?>
            <?if (in_array($propertyValue, $arResult["MOUNTHS"])):?>
                <a class="checkMounth" href="#" value="<?=$key?>"><?=$propertyValue?></a>
                <button style="display:none" name="<?=$propertyValue?>" value="<?=$key?>"><?=$propertyValue?></button>
            <?endif;?>
        <?endforeach;?>
        <div id="changeMounth" style="display:none">
            <?=$arResult["ITEMS"]["PROPERTY_2"]["INPUT"]?>
        </div>
    </div>
    <input type="hidden" name="set_filter" value="Y"/>&nbsp;
</form>
