<?php
global $arrFilter;
$arResult["INDUSTRY"] = $arResult["STATUS"] = $arResult["COMPLEX_PROJECT"] = [];
$properties = CIBlockElement::GetList(
    ["PROPERTY_INDUSTRY" => "ASC"],
    ['IBLOCK_ID' => $arParams["IBLOCK_ID"]],
    ["PROPERTY_INDUSTRY"]
);
while($property = $properties->Fetch()) {
    $arResult["INDUSTRY"][$property['PROPERTY_INDUSTRY_ENUM_ID']] = $property["PROPERTY_INDUSTRY_VALUE"];
}

$properties = CIBlockElement::GetList(
    ["PROPERTY_STATUS" => "ASC"],
    ['IBLOCK_ID' => $arParams["IBLOCK_ID"]],
    ["PROPERTY_STATUS"]
);
while($property = $properties->Fetch()) {
    $arResult["STATUS"][$property['PROPERTY_STATUS_ENUM_ID']] = $property["PROPERTY_STATUS_VALUE"];
}

$properties = CIBlockElement::GetList(
    ["PROPERTY_MUNICIPALITY" => "ASC"],
    ['IBLOCK_ID' => $arParams["IBLOCK_ID"]],
    ["PROPERTY_MUNICIPALITY"]
);
while($property = $properties->Fetch()) {
    $arResult["MUNICIPALITY"][$property['PROPERTY_MUNICIPALITY_ENUM_ID']] = $property["PROPERTY_MUNICIPALITY_VALUE"];
}
