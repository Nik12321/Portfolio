document.addEventListener("DOMContentLoaded", ready);
BX.addCustomEvent('onAjaxSuccess', ready);
function ready() {
    const status = document.querySelectorAll(".checkStatus"),
        statusSelect = document.querySelector("#status"),
        statusFilter = document.querySelector("#changeStatus").firstElementChild.children,
        complexProject = document.querySelectorAll(".checkComplexProject"),
        complexProjectSelect = document.querySelector("#complexProject"),
        complexProjectFilter = document.querySelector("#changeComplexProject").firstElementChild.children,
        industry = document.querySelectorAll(".checkIndustry"),
        industrySelect = document.querySelector("#industry"),
        industryFilter = document.querySelector("#changeIndustry").firstElementChild.children;

    filterElements(status, statusFilter, statusSelect);
    filterElements(complexProject, complexProjectFilter, complexProjectSelect);
    filterElements(industry, industryFilter, industrySelect);
    selectValue(statusSelect, statusFilter, "select");
    selectValue(complexProjectSelect, complexProjectFilter, "select");
    selectValue(industrySelect, industryFilter, "select");
}

function filterElements(items, options, select) {
    select.addEventListener("change", (e) => {
        let itemValue = e.target.value;
        for (let j = 0; j < options.length; j++) {
            let optionValue = options[j].getAttribute("value");
            if (optionValue != itemValue) {
                options[j].selected = false;
            }
            else {
                options[j].selected = true;
            }
        }
    });
}

function selectValue(select, options, selector) {
    let isHaveSelectedItems = false;
    for (let i = 0; i < options.length; i++) {
        if (options[i].selected) {
            console.log(options[i]);
            if (options[i].innerHTML){
                let item = select.querySelector('[name="' +  options[i].innerHTML + '"]');
                if (item) {
                    item.selected = true;
                }
            }
        }
    }
}