<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);?>
<div class="search">
    <form action="<?=$arResult["FORM_ACTION"]?>">
        <input class="<?=($arParams["MOBILE"] == "N") ? "input-submit" : "input-submit-mobile" ?>" type="submit">
        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:search.suggest.input",
            "",
            [
                "NAME" => "q",
                "VALUE" => "",
                "INPUT_SIZE" => 15,
                "DROPDOWN_SIZE" => 10,
            ],
            $component,
            ["HIDE_ICONS" => "Y"]
        );
        ?>
    </form>
</div>