<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
?>
<?if(count($arResult)):?>
    <div class="col">
        <?php foreach($arResult as $key => $arItemLevelOne) :?>
            <h3>
                <a href="<?=$arItemLevelOne["LINK"]?>">
                    <?=mb_strtoupper($arItemLevelOne["TEXT"])?>
                </a>
            </h3>
            <?php if ($arItemLevelOne["IS_PARENT"]) :?>
                <?php foreach($arItemLevelOne["CHILD_ITEMS"] as $arItemLevelTwo) :?>
                    <h4>
                        <a class="link-menu-1" href="<?=$arItemLevelTwo["LINK"]?>">
                            <?=$arItemLevelTwo["TEXT"]?>
                        </a>
                    </h4>
                    <?if($arItemLevelTwo["IS_PARENT"]):?>
                        <ul>
                            <?foreach($arItemLevelTwo["CHILD_ITEMS"] as $arItemLevelThree):?>
                                <li><a href="<?=$arItemLevelThree["LINK"]?>"><?=$arItemLevelThree["TEXT"]?></a></li>
                            <?endforeach;?>
                        </ul>
                    <?endif;?>
                <?php endforeach;?>
            <?php endif;?>
        <?php unset($arResult[$key])?>
        <?php break;?>
        <?php endforeach;?>
    </div>
    <div class="col">
        <?php foreach($arResult as $key => $arItemLevelOne) :?>
            <h3>
                <a href="<?=$arItemLevelOne["LINK"]?>">
                    <?=mb_strtoupper($arItemLevelOne["TEXT"])?>
                </a>
            </h3>
            <?php if ($arItemLevelOne["IS_PARENT"]) :?>
                <?php foreach($arItemLevelOne["CHILD_ITEMS"] as $arItemLevelTwo) :?>
                    <h4>
                        <a class="link-menu-1" href="<?=$arItemLevelTwo["LINK"]?>">
                            <?=$arItemLevelTwo["TEXT"]?>
                        </a>
                    </h4>
                    <?if($arItemLevelTwo["IS_PARENT"]):?>
                        <ul>
                            <?foreach($arItemLevelTwo["CHILD_ITEMS"] as $arItemLevelThree):?>
                                <li><a href="<?=$arItemLevelThree["LINK"]?>"><?=$arItemLevelThree["TEXT"]?></a></li>
                            <?endforeach;?>
                        </ul>
                    <?endif;?>
                <?php endforeach;?>
            <?php endif;?>
            <?php unset($arResult[$key])?>
            <?php break;?>
        <?php endforeach;?>
    </div>
    <div class="col">
        <?php foreach($arResult as $key => $arItemLevelOne) :?>
            <h3>
                <a href="<?=$arItemLevelOne["LINK"]?>">
                    <?=mb_strtoupper($arItemLevelOne["TEXT"])?>
                </a>
            </h3>
            <?php if ($arItemLevelOne["IS_PARENT"]) :?>
                <?php foreach($arItemLevelOne["CHILD_ITEMS"] as $arItemLevelTwo) :?>
                    <h4>
                        <a class="link-menu-1" href="<?=$arItemLevelTwo["LINK"]?>">
                            <?=$arItemLevelTwo["TEXT"]?>
                        </a>
                    </h4>
                    <?if($arItemLevelTwo["IS_PARENT"]):?>
                        <ul>
                            <?foreach($arItemLevelTwo["CHILD_ITEMS"] as $arItemLevelThree):?>
                                <li><a href="<?=$arItemLevelThree["LINK"]?>"><?=$arItemLevelThree["TEXT"]?></a></li>
                            <?endforeach;?>
                        </ul>
                    <?endif;?>
                <?php endforeach;?>
            <?php endif;?>
        <?php endforeach;?>
    </div>
<?endif;?>