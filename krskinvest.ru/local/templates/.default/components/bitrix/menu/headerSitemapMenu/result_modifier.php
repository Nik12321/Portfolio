<?php
$currentParentLevelOne = "";
$currentParentLevelTwo = "";
foreach($arResult as $key => $arItem) {
    $arResult[$key] = $arItem;
    if ($arItem["IS_PARENT"]) {
        if ($arItem["DEPTH_LEVEL"] == 1) {
            $currentParentLevelOne = $key;
        } elseif ($arItem["DEPTH_LEVEL"] == 2) {
            $currentParentLevelTwo = $key;
            if ($arResult[$currentParentLevelOne]["CHILD_ITEMS"]) {
                $arResult[$currentParentLevelOne]["CHILD_ITEMS"][$key] = $arItem;
            } else {
                $arResult[$currentParentLevelOne]["CHILD_ITEMS"] = array($key => $arItem);
            }
            unset($arResult[$key]);
        }
    } elseif ($arItem["DEPTH_LEVEL"] == 2) {
        if ($arResult[$currentParentLevelOne]["CHILD_ITEMS"]) {
            $arResult[$currentParentLevelOne]["CHILD_ITEMS"][$key] = $arItem;
        } else {
            $arResult[$currentParentLevelOne]["CHILD_ITEMS"] = array($key => $arItem);
        }
        unset($arResult[$key]);
    } elseif ($arItem["DEPTH_LEVEL"] == 3) {
        if ($arResult[$currentParentLevelOne]["CHILD_ITEMS"][$currentParentLevelTwo]["CHILD_ITEMS"]) {
            $arResult[$currentParentLevelOne]["CHILD_ITEMS"][$currentParentLevelTwo]["CHILD_ITEMS"][] = $arItem;
        } else {
            $arResult[$currentParentLevelOne]["CHILD_ITEMS"][$currentParentLevelTwo]["CHILD_ITEMS"] = array($arItem);
        }
        unset($arResult[$key]);
    } elseif ($arItem["DEPTH_LEVEL"] > 3) {
        unset($arResult[$key]);
    } else {
        continue;
    }
}