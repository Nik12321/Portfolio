<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
?>
<?php if(count($arResult)):?>
    <ul class="navigation_menu">
        <?php foreach($arResult as $arItemLevelOne) :?>
            <li class="menu-1">
                <div class="flex-contauner">
                    <a class="menu" href="<?=$arItemLevelOne["LINK"]?>"><?=$arItemLevelOne["TEXT"]?></a>
                    <?php if ($arItemLevelOne["IS_PARENT"]) :?>
                        <div class="arrow-right">
                            <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 10.5V0L5.5 5L0 10.5Z" fill="#4D5F76"/>
                            </svg>
                        </div>
                    <?php endif;?>
                </div>
                <?php if ($arItemLevelOne["IS_PARENT"]) :?>
                    <div class="drop-down-list-1 drop-down-list">
                        <div class="background-dropdown">
                            <ul class="first-list">
                                <?php foreach($arItemLevelOne["CHILD_ITEMS"] as $arItemLevelTwo) :?>
                                    <li class="<?=($arItemLevelTwo["IS_PARENT"]) ? "menu-in-1 menu-in" : ""?>">
                                        <div class="flex-contauner">
                                            <a class="link-menu-1" href="<?=$arItemLevelTwo["LINK"]?>">
                                                <?=$arItemLevelTwo["TEXT"]?>
                                                <?php if($arItemLevelTwo["IS_PARENT"]):?>
                                                    <svg width="8" height="8">
                                                        <use xlink:href="#plus"/>
                                                    </svg>
                                                <?php endif?>
                                            </a>
                                            <?php if($arItemLevelTwo["IS_PARENT"]):?>
                                                <div class="arrow-right-1">
                                                    <svg width="6" height="11">
                                                        <use xlink:href="#arrow-right"/>
                                                    </svg>
                                                </div>
                                            <?php endif;?>
                                        </div>
                                        <?php if($arItemLevelTwo["IS_PARENT"]):?>
                                            <div class="drop-right-list-1 drop-right-list">
                                                <ul class="second-list">
                                                    <?php foreach($arItemLevelTwo["CHILD_ITEMS"] as $arItemLevelThree):?>
                                                        <li><a href="<?=$arItemLevelThree["LINK"]?>"><?=$arItemLevelThree["TEXT"]?></a></li>
                                                    <?php endforeach;?>
                                                </ul>
                                            </div>
                                        <?php endif;?>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                <?endif;?>
            </li>
        <?php endforeach;?>
    </ul>
<?php endif;?>