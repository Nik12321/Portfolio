<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true) {
    die();
}
if(method_exists ($this , 'setFrameMode')) {
    $this->setFrameMode(true);
}
global $USER;
?>
<div id="accorsys-switch-lang"  class="lang">
    <?php
    if(method_exists ( $this , 'setFrameMode'))
        $frame = $this->createFrame()->begin();
    ?>
    <a data-lang="<?=$arResult['CURRENT']['CODE']?>"><?=strtoupper($arResult['CURRENT']['CODE'])?></a>
    <div class="change-lang">
        <?php foreach($arResult['SITES'] as $site):?>
            <a href="<?=$site['LINK']?>" data-lang="<?=$site['CODE']?>"><?=strtoupper($site['CODE'])?></a>
        <?php endforeach;?>
    </div>
    <?php
    if(method_exists($this , 'setFrameMode')) {
        $frame->end();
    }
    ?>
</div>