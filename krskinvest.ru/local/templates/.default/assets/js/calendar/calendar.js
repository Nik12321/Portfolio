document.addEventListener('DOMContentLoaded', function() {
  const calendar = new Calendar('', {
    // пример данных мероприятий
    events: [
        {
            date: '10 октября 2020',
            start: '2020-10-10',
            title: 'Форум предпринимательства Сибири 1',
            logo: '/media/image/event-40x40.png',
            url: 'http://event/1',
            type: 'bisness',
            color: '#B5001F'
        },
        {
            date: '25 октября 2020',
            start: '2020-10-25',
            title: 'Форум предпринимательства Сибири 232',
            logo: '/media/image/event-40x40.png',
            url: 'http://event/2',
            type: 'bisness',
            color: '#B5001F'
        },
        {
            date: '15 октября 2020',
            start: '2020-10-15',
            title: 'Форум предпринимательства Сибири 232',
            logo: '/media/image/event-40x40.png',
            url: 'http://event/2',
            type: 'bisness',
            color: '#B5001F'
        },
        {
            date: '26 - 30 октября 2020',
            start: '2020-10-26',
            end: '2020-10-30',
            title: 'Форум предпринимательства Сибири 1412',
            logo: '/media/image/event-40x40.png',
            url: 'http://event/3',
            type: 'forum',
            color: '#1C3351'
        }
    ],
    // пример данных для генерации легенды под календарем
    legends: [
        {
            color: '#B5001F',
            title: 'Бизнес-миссия'
        },
        {
            color: '#1C3351',
            title: 'Форум'
        }
    ]
  });

  const exampleObjEvents = {
      '2020-09': [
          {
              date: '10 сентября 2020',
              start: '2020-09-10',
              title: 'Бизнес-миссия предпринимательства Сибири 1',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/1',
              type: 'bisness',
              color: '#B5001F'
          },
          {
              date: '25 сентября 2020',
              start: '2020-09-25',
              title: 'Бизнес-миссия предпринимательства Сибири 232',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/2',
              type: 'bisness',
              color: '#B5001F'
          },
          {
              date: '21 сентября 2020',
              start: '2020-09-21',
              title: 'Бизнес-миссия предпринимательства Сибири 232',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/2',
              type: 'bisness',
              color: '#B5001F'
          },
          {
              date: '29 сентября 2020',
              start: '2020-09-29',
              title: 'Бизнес-миссия предпринимательства Сибири 232',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/2',
              type: 'forum',
              color: '#1C3351'
          },
          {
              date: '5 - 13 сентября 2020',
              start: '2020-09-5',
              end: '2020-09-13',
              title: 'Бизнес-миссия предпринимательства Сибири 1412',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/3',
              type: 'forum',
              color: '#1C3351'
          }
      ],
      '2020-10': [
          {
              date: '10 октября 2020',
              start: '2020-10-10',
              title: 'Форум предпринимательства Сибири 1',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/1',
              type: 'bisness',
              color: '#B5001F'
          },
          {
              date: '25 октября 2020',
              start: '2020-10-25',
              title: 'Форум предпринимательства Сибири 232',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/2',
              type: 'bisness',
              color: '#B5001F'
          },
          {
              date: '26 - 30 октября 2020',
              start: '2020-10-26',
              end: '2020-10-30',
              title: 'Форум предпринимательства Сибири 1412',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/3',
              type: 'forum',
              color: '#1C3351'
          }
      ],
      '2020-11': [
          {
              date: '10 ноября 2020',
              start: '2020-11-10',
              title: 'Форум предпринимательства Сибири 1',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/1',
              type: 'bisness',
              color: '#B5001F'
          },
          {
              date: '25 ноября 2020',
              start: '2020-11-25',
              title: 'Форум предпринимательства Сибири 232',
              logo: '/media/image/event-40x40.png',
              url: 'http://event/2',
              type: 'bisness',
              color: '#B5001F'
          }
      ]
  }

  /**
   * Пользовательский слушатель внутреннего особытия у календаря
   * для случаев, где нужно без перезагрузки страницы отобразить
   * дни мероприятий текущего месяца
   *
   * @event [name] - имя внутреннего события
   * @callback [Function] - возвращает событие, где
   * - @property {type} - prev-month/next-month
   * - @property {handler} - e.type
   * - @property {date} - Текущая установленая дата в календаре
   */
  calendar.elCalendar.addEventListener('on.change-date', (e) => {
      const calendarEvents = calendar.instanceEvents;
      const date = e.detail.data.date;

      console.log('Установленная дата', date);
      console.log('Установленный Год', calendar.yearNumber);
      console.log('Установленный месяц - номер', calendar.monthNumber);

      calendarEvents.events = exampleObjEvents[date];

      calendarEvents.renderEvents();
  });
});
