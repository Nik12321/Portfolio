$(document).ready(function () {
  function smoothCount(selection, option) {
    if (!document.querySelector(selection)) return;

    var show = true;
    var selector = selection;
    $(window).on("scroll load resize", function () {
      if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
      var w_top = $(window)
        .scrollTop(); // Количество пикселей на которое была прокручена страница
      var e_top = $(selector).offset().top; // Расстояние от блока со счетчиками до верха всего документа
      var w_height = $(window).height(); // Высота окна браузера
      var d_height = $(document).height(); // Высота всего документа
      var e_height = $(selector).outerHeight(); // Полная высота блока со счетчиками
      if (w_top + w_height >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
        for (let i = 0; i < option.length; i++) {
          $(option[i].selector).css('opacity', '1');
          $(option[i].selector).spincrement(option[i].parametr);
        };
        show = false;
      }
    });
  };

  smoothCount('.box--main', [{
    selector: '.box--main.number-decimal',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);
  smoothCount('.box--main', [{
    selector: '.box--main.number',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: "",
      duration: 2000
    }
  }]);

  smoothCount('.stats__number', [{
    selector: '.stats__number',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);
  smoothCount('.local-gover__img--number', [{
    selector: '.local-gover__img--number',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: "",
      duration: 2000
    }
  }]);


  smoothCount('.rating__stats--numb', [{
    selector: '.rating__stats--numb',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  smoothCount('.investments__stats--numb', [{
    selector: '.investments__stats--numb',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: "",
      duration: 2000
    }
  }]);

  // Страница с природно-ресурсным потенциалом
  // Первая линия чисел
  smoothCount('.numb-line-first.integer', [{
    selector: '.numb-line-first.integer',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);
  smoothCount('.numb-line-first.decimal1', [{
    selector: '.numb-line-first.decimal1',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: "&#46;",
      duration: 2000
    }
  }]);

  smoothCount('.numb-line-first.decimal2', [{
    selector: '.numb-line-first.decimal2',
    parametr: {
      decimalPlaces: 2,
      decimalPoint: ",",
      thousandSeparator: "",
      duration: 2000
    }
  }]);

  smoothCount('.numb-line-first.decimal3', [{
    selector: '.numb-line-first.decimal3',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  // Вторая линия

  smoothCount('.numb-line-second.integer', [{
    selector: '.numb-line-second.integer',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  smoothCount('.numb-line-second.integer2', [{
    selector: '.numb-line-second.integer2',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: "",
      duration: 2000
    }
  }]);

  smoothCount('.numb-line-second.decimal', [{
    selector: '.numb-line-second.decimal',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  // Третья линия

  smoothCount('.numb-line-third.integer', [{
    selector: '.numb-line-third.integer',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  smoothCount('.numb-line-third.decimal', [{
    selector: '.numb-line-third.decimal',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  // Четвертая линия
  smoothCount('.numb-line-four.integer', [{
    selector: '.numb-line-four.integer',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);
  smoothCount('.numb-line-four.decimal', [{
    selector: '.numb-line-four.decimal',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  // Пятая линия

  smoothCount('.numb-line-five.decimal', [{
    selector: '.numb-line-five.decimal',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  smoothCount('.numb-line-five.integer', [{
    selector: '.numb-line-five.integer',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  // Шестая линия

  smoothCount('.numb-line-six.decimal', [{
    selector: '.numb-line-six.decimal',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  smoothCount('.numb-line-six.integer', [{
    selector: '.numb-line-six.integer',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  // Седьмая линия

  smoothCount('.numb-line-seven.decimal', [{
    selector: '.numb-line-seven.decimal',
    parametr: {
      decimalPlaces: 1,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);

  smoothCount('.numb-line-seven.integer', [{
    selector: '.numb-line-seven.integer',
    parametr: {
      decimalPlaces: 0,
      thousandSeparator: " ",
      duration: 2000
    }
  }]);
});
