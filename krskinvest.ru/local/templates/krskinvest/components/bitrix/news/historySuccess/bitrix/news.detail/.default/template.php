<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<!-- секция основной информации страницы -->
<div class="container--centered">
    <div class="container-right">
        <div class="history-success-detail--col">
            <div class="history-success-detail__info">
                <h3><?=$arResult["NAME"]?></h3>

                <?php if ($arResult["PROPERTIES"]["TERM"]["VALUE"]) :?>
                    <p><span>Сроки реализации:</span> <?=$arResult["PROPERTIES"]["TERM"]["VALUE"]?></p>
                <?php endif;?>


                <?php if ($arResult["PROPERTIES"]["INVESTMENT"]["VALUE"]) :?>
                    <p><span>Объем инвестиций:</span> <?=$arResult["PROPERTIES"]["INVESTMENT"]["VALUE"]?></p>
                <?php endif;?>

                <?php if ($arResult["PROPERTIES"]["INVESTORS"]["VALUE"]) :?>
                    <p><span>Инвесторы, участники проекта:</span> <?=$arResult["PROPERTIES"]["INVESTORS"]["VALUE"]?></p>
                <?php endif;?>

                <?php if ($arResult["PREVIEW_TEXT"]) :?>
                    <p><span>Информация о проекте:</span> <?=$arResult["PREVIEW_TEXT"]?></p>
                <?php endif;?>
            </div>
        </div>
        <div class="history-success-detail--col">
            <div class="history-success-detail__img">
                <a class="big-img" data-fancybox="gallery" href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>">
                    <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>">
                </a>
            </div>
        </div>
    </div>
</div>