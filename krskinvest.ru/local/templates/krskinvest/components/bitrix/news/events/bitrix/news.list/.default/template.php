<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<!-- Пагинация -->
<?php if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?php endif;?>
<!-- Пагинация -->

<?php foreach($arResult["ITEMS"] as $arItem) :?>
    <?php
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="news__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
            <div class="news__item--date adaptive-date"><?=$arItem["DATE"]?></div>
            <div class="news__item--img" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div>
            <div class="news__item--column">
                <div class="news__item--date"><?=$arItem["DATE"]?></div>
                <div class="news__item--title"><?=$arItem["NAME"]?></div>
                <div class="news__item--text"><?=strip_tags(htmlspecialcharsBack($arItem["PROPERTIES"]["TITLE"]["VALUE"]["TEXT"], '<br>'))?></div>
                <div class="news__item--detail">
                    <span class="btn--link"><?=$arItem["DETAIL_PAGE_URL"]?></span>
                </div>
            </div>
        </a>
    </div>
<?php endforeach;?>
</div>
</div>
<!-- Пагинация -->
<?php if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?php endif;?>
<!-- Пагинация -->