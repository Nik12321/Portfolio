<?php
$mouths = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря'
];

foreach($arResult["ITEMS"] as $key=>$arItem) {
    if ($arItem["PREVIEW_PICTURE"]) {
        $resizedPreview = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            array("height" => 310, "width" => 1200000),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true,
            false
        );
        if ($resizedPreview) {
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["SRC"] = $resizedPreview["src"];
        }
    } else {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["SRC"] = DEFAULT_TEMPLATE_PATH . "/img/plug-img.svg";
    }

    if ($arItem["PROPERTIES"]["DATE_BEFORE"]["VALUE"]) {
        $date = $firstPart = $secondPart = "";
        $yearNumberOne = date("Y", strtotime($arItem["PROPERTIES"]["DATE_BEFORE"]["VALUE"]));
        $monthNumberOne = date('n', strtotime($arItem["PROPERTIES"]["DATE_BEFORE"]["VALUE"]))-1;
        $dayNumberOne = date("j", strtotime($arItem["PROPERTIES"]["DATE_BEFORE"]["VALUE"]));
        $mouthNameOne = $mouths[$monthNumberOne];
        if ($arItem["PROPERTIES"]["DATE_AFTER"]["VALUE"]) {
            $yearNumberTwo = date("Y", strtotime($arItem["PROPERTIES"]["DATE_AFTER"]["VALUE"]));
            $monthNumberTwo = date('n', strtotime($arItem["PROPERTIES"]["DATE_AFTER"]["VALUE"]))-1;
            $dayNumberTwo = date("j", strtotime($arItem["PROPERTIES"]["DATE_AFTER"]["VALUE"]));
            $mouthNameTwo = $mouths[$monthNumberTwo];
            if ($yearNumberOne == $yearNumberTwo) {
                if ($monthNumberOne == $monthNumberTwo) {
                    $date = $dayNumberOne . "-" . $dayNumberTwo . " " . $mouthNameOne;
                } else {
                    $date = $dayNumberOne . " " . $mouthNameOne . " - " . $dayNumberTwo . " " . $mouthNameTwo;
                }
            } else {
                $date =  $dayNumberOne . " " . $mouthNameOne . " " . $yearNumberOne . " " . "года";
                $date .=  " - " . $dayNumberTwo . " " . $mouthNameTwo . " " . $yearNumberTwo . " " . "года";
            }
        } else {
            $date =  $dayNumberOne . " " . $mouthNameOne;
        }
        $arResult["ITEMS"][$key]["DATE"] = $date;
    }
}