<?php
foreach($arResult["ITEMS"] as $key=>$arItem) {
    if ($arItem["PREVIEW_PICTURE"]) {
        $resizedPreview = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            array("width"=> 150000, "height" => 400),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true,
            false
        );
        if ($resizedPreview) {
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["SRC"] = $resizedPreview["src"];
        }
    } else {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["SRC"] = DEFAULT_TEMPLATE_PATH . "/img/plug-img.svg";
    }
}