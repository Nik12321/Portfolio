<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>

<?php if (count($arResult["ITEMS"])) :?>
    <div class="invest-project__content--list">
        <?php foreach($arResult["ITEMS"] as $key => $arItem) :?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="link-img" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="link-img--inner">
                    <div class="link-img--image" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);">
                    </div>
                    <div class="link-img--text">
                        <?=$arItem["NAME"]?>
                    </div>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
                </div>
            </div>
        <?php endforeach;?>
    </div>
<?php endif;?>

<!-- Пагинация -->
<?php if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?php endif;?>
<!-- Пагинация -->
