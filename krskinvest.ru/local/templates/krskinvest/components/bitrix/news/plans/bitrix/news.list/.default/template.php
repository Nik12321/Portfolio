<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>

<?php if (count($arResult["ITEMS"])) :?>
    <div class="plan-object__content--list">
        <?php foreach($arResult["ITEMS"] as $key => $arItem) :?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <div class="item--row">
                        <div class="item--icon">
                            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                        </div>
                        <div class="item--title">
                            <h5>Территория реализации</h5>
                            <p><?=$arItem["PROPERTIES"]["TERRITORY"]["VALUE"]?></p>
                        </div>
                    </div>
                    <div class="item--text">
                        <?=$arItem["NAME"]?>
                    </div>
                </a>
            </div>
        <?php endforeach;?>
    </div>
<?php endif;?>

<!-- Пагинация -->
<?php if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
<?php endif;?>
<!-- Пагинация -->
