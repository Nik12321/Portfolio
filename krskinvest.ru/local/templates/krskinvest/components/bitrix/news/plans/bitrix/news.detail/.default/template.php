<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?><script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=9ce47341-3963-4c13-8f98-7307288807e5" type="text/javascript"></script>
<div class="container-right">
    <div class="invest-project-detail__col">
        <div class="invest-project-detail__info">
            <h2><?=$arResult["NAME"]?></h2>

            <h4>Наименование проекта</h4>
            <p><?=$arResult["NAME"]?></p>

            <?php if ($arResult["PROPERTIES"]["COMPLEX_PROJECT"]["VALUE"]) :?>
                <h4>Комплексный проект</h4>
                <p><?=$arResult["PROPERTIES"]["COMPLEX_PROJECT"]["VALUE"]?></p>
            <?php endif;?>

            <?php if ($arResult["PROPERTIES"]["INDUSTRY"]["VALUE"]) :?>
                <h4>Отрасль</h4>
                <p><?=$arResult["PROPERTIES"]["INDUSTRY"]["VALUE"]?></p>
            <?php endif;?>

            <?php if ($arResult["PROPERTIES"]["INVESTOR"]["VALUE"]) :?>
                <h4>Инвестор, участники проекта</h4>
                <p><?=$arResult["PROPERTIES"]["INVESTOR"]["VALUE"]?></p>
            <?php endif;?>

            <?php if ($arResult["PROPERTIES"]["TERRITORY"]["VALUE"]) :?>
                <h4>Территория реализации</h4>
                <p><?=$arResult["PROPERTIES"]["TERRITORY"]["VALUE"]?></p>
            <?php endif;?>

            <?php if ($arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"]) :?>
                <h4>Описание проекта</h4>
                <p><?=htmlspecialcharsback($arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"]["TEXT"])?></p>
            <?php endif;?>

            <?php if ($arResult["PROPERTIES"]["VOLUME"]["VALUE"]) :?>
                <h4>Объем инвестиций</h4>
                <p><?=$arResult["PROPERTIES"]["VOLUME"]["VALUE"]?></p>
            <?php endif;?>

            <?php if ($arResult["PROPERTIES"]["TERM"]["VALUE"]) :?>
                <h4>Срок реализации</h4>
                <p><?=$arResult["PROPERTIES"]["TERM"]["VALUE"]?></p>
            <?php endif;?>

            <?php if ($arResult["PROPERTIES"]["GOAL_STATS"]["VALUE"]) :?>
                <h4>Целевые показатели</h4>
                <p><?=$arResult["PROPERTIES"]["GOAL_STATS"]["VALUE"]?></p>
            <?php endif;?>

            <?php if ($arResult["PROPERTIES"]["CURRENT_STATUS"]["VALUE"]) :?>
                <h4>Текущее состояние</h4>
                <p><?=$arResult["PROPERTIES"]["CURRENT_STATUS"]["VALUE"]?></p>
            <?php endif;?>
        </div>
    </div>
    <div class="invest-project-detail__col">
		<!--<div class="invest-project-detail__img">-->
		  <!--<a class="big-img" data-fancybox="gallery" href="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>">-->
		      <!--<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>">-->
		  <!--</a>-->
		<!--</div> -->
        <?php if ($arResult["PROPERTIES"]["COORDINATES"]["VALUE"]) :?>
            <div class="invest-project-detail__map">
                <div class="grey-map-btn">
                    <button id="fullscreenMap">Смотреть обьект на карте</button>
                </div>
                <div class="map" id="mapID"></div>
            </div>
            <script>
                ymaps.ready(function () {
                    <?php
                    $coordinate = explode(",", $arResult["PROPERTIES"]["COORDINATES"]["VALUE"][0]);
                    $coordinate = array_map('trim', $coordinate);
                    ?>
                    var myMap = new ymaps.Map('mapID', {
                        center: [<?=$coordinate[0]?>, <?=$coordinate[1]?>],
                        zoom: 15,
                        controls: []
                    }, {
                        searchControlProvider: 'yandex#search'
                    });

                    // Кнопка fullscreen
                    let btn = document.getElementById('fullscreenMap')
                    var fullscreenControl = new ymaps.control.FullscreenControl();
                    myMap.controls.add(fullscreenControl);
                    btn.onclick = function () {
                        fullscreenControl.enterFullscreen();
                    }
                    var zoomControl = new ymaps.control.ZoomControl({
                        options: {
                            position: {
                                top: 10,
                                left: 10
                            }
                        }
                    });
                    myMap.controls.add(zoomControl);

                    // Создаём макет содержимого.
                    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                    );
                    <?php foreach($arResult["PROPERTIES"]["COORDINATES"]["VALUE"] as $key => $value) :?>
                    <?php
                    $coordinate = explode(",", $value);
                    $coordinate = array_map('trim', $coordinate);
                    ?>
                    myPlacemarkWithContent = new ymaps.Placemark([<?=$coordinate[0]?>, <?=$coordinate[1]?>], {
                        hintContent: '<?=$arResult["NAME"]?>'
                    }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#imageWithContent',
                        // Своё изображение иконки метки.
                        iconImageHref: '<?=DEFAULT_TEMPLATE_PATH?>/img/marker-map.svg',
                        // Размеры метки.
                        iconImageSize: [48, 48],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [-24, -24],
                        // Смещение слоя с содержимым относительно слоя с картинкой.
                        iconContentOffset: [15, 15],
                        // Макет содержимого.
                        iconContentLayout: MyIconContentLayout
                    });
                    myMap.geoObjects.add(myPlacemarkWithContent);
                    <?php endforeach;?>
                });
            </script>
        <?php endif;?>
    </div>
</div>