<?php
if (count($arResult["PROPERTIES"]["PHOTOS"]["VALUE"])) {
    foreach ($arResult["PROPERTIES"]["PHOTOS"]["VALUE"] as $key => $value) {
        $file = CFile::GetFileArray($value);
        $arResult["PROPERTIES"]["PHOTOS"]["VALUE"][$key] = $file["SRC"];
    }
}
if (!$arResult["PREVIEW_PICTURE"]["SRC"]) {
    $arResult["PREVIEW_PICTURE"]["SRC"] = DEFAULT_TEMPLATE_PATH . "/img/plug-img.svg";
}