<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="local-gover-detail__title">
    <h2><?=$arResult["NAME"]?></h2>
    <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="">
</div>

<div class="local-gover-detail--wrap">
    <div class="local-gover-detail__about">
        <?php if ($arResult["PROPERTIES"]["STATUS"]["VALUE"]) :?>
        <p>
            <b>Статус:</b>
            <?=$arResult["PROPERTIES"]["STATUS"]["VALUE"]?>
        </p>
        <?php endif;?>

        <?php if ($arResult["PROPERTIES"]["DATE_CREATE"]["VALUE"]) :?>
            <p>
                <b>Дата образования:</b>
                <?=$arResult["PROPERTIES"]["DATE_CREATE"]["VALUE"]?>
            </p>
        <?php endif;?>

        <?php if ($arResult["PROPERTIES"]["POPULATION"]["VALUE"]) :?>
            <p>
                <b>Численность населения</b>
                <?=$arResult["PROPERTIES"]["POPULATION"]["VALUE"]?>
            </p>
        <?php endif;?>

        <?php if ($arResult["PROPERTIES"]["CENTER_DISTANCY"]["VALUE"]) :?>
            <p>
                <b>Удаленность от центра городского округа до Красноярска:</b>
                <?=$arResult["PROPERTIES"]["CENTER_DISTANCY"]["VALUE"]?>
            </p>
        <?php endif;?>

        <?php if ($arResult["PROPERTIES"]["NUMBER_OF_POINTS"]["VALUE"]) :?>
            <p>
                <b>Количество населенных пунктов в составе городского округа:</b>
                <?=$arResult["PROPERTIES"]["NUMBER_OF_POINTS"]["VALUE"]?>
            </p>
        <?php endif;?>

        <?php if ($arResult["PROPERTIES"]["ADM_ADDRESS"]["VALUE"]["TEXT"]) :?>
            <p>
                <b>Адрес администрации:</b>
                <?=htmlspecialcharsBack($arResult["PROPERTIES"]["ADM_ADDRESS"]["VALUE"]["TEXT"])?>
            </p>
        <?php endif;?>

        <?php if ($arResult["PROPERTIES"]["INT_ADDRESS"]["VALUE"]) :?>
            <p>
                <b>Адрес в сети Интернет:</b>
                <a href="<?=$arResult["PROPERTIES"]["INT_ADDRESS"]["VALUE"]?>">
                    <?=$arResult["PROPERTIES"]["INT_ADDRESS"]["VALUE"]?>
                </a>
            </p>
        <?php endif;?>
        <div class="local-gover-detail__about--extra">
            <?=$arResult["PREVIEW_TEXT"]?>
        </div>
    </div>
    <?php if ($arResult["DETAIL_PICTURE"]) : ?>
        <div class="local-gover-detail__img">
            <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="">
        </div>
    <?php endif;?>
</div>