<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="container">
    <?php
    $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "cubeMenu",
                [
                    "ALLOW_MULTI_SELECT" => "N",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => [],
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "submenuHeaderMenu" . LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "COMPONENT_TEMPLATE" => "headerMenu"
                ],
                false
            );
    ?>
</div>
<!-- секция основной информации страницы -->
<section class="local-gover">
    <div class="container">
        <h2>Местное самоуправление</h2>
    </div>
    <div class="container--centered">
        <div class="container-left">
            <div class="local-gover__img">
                <div class="local-gover__img--number">
                    <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW"=>"file", "PATH"=>"/includes/local-goverments/municipalityNumber.php"]); ?>
                </div>
                <div class="local-gover__img--text">
                    муниципальных образований
                </div>
            </div>
            <div class="local-gover__stats">
                <div class="local-gover__stats--col">
                    <div class="stats">
                        <div class="stats__icon">

                            <svg width="93" height="93" viewBox="0 0 93 93" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M46.5002 0L6.2002 21.359V24.8H46.5002H86.8002V21.359L46.5002 0ZM12.4002 31V62L6.2002 71.641V80.6H86.8002V71.641L80.6002 62V31H12.4002ZM24.8002 37.2H31.0002V71.3H24.8002V37.2ZM43.4002 37.2H49.6002V71.3H43.4002V37.2ZM62.0002 37.2H68.2002V71.3H62.0002V37.2Z"
                                    fill="#C4C4C4" />
                                <path d="M6.2002 21.359L46.5002 0L86.8002 21.359V24.8H46.5002H6.2002V21.359Z"
                                      fill="#C80022" />
                            </svg>

                        </div>
                        <div class="stats--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW"=>"file", "PATH"=>"/includes/local-goverments/municipalityAreas.php"]); ?>
                        </div>
                    </div>
                    <div class="stats">
                        <div class="stats__icon">
                            <svg>
                                <use xlink:href="#map">
                            </svg>
                        </div>
                        <div class="stats--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW"=>"file", "PATH"=>"/includes/local-goverments/cityAreas.php"]); ?>
                        </div>
                    </div>
                    <div class="stats">
                        <div class="stats__icon">
                            <svg>
                                <use xlink:href="#puzzle">
                            </svg>
                        </div>
                        <div class="stats--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW"=>"file", "PATH"=>"/includes/local-goverments/municipalitySubAreas.php"]); ?>
                        </div>
                    </div>
                </div>
                <div class="local-gover__stats--col">
                    <div class="stats">
                        <div class="stats__icon">
                            <svg width="78" height="79" viewBox="0 0 78 79" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M73.6665 0H38.9998C36.6165 0 34.6665 1.95 34.6665 4.33333V11.31L34.7098 11.3533L56.3765 30.8533C59.1065 33.28 60.6665 36.8333 60.6665 40.5167V43.3333H69.3332V52H60.6665V60.6667H69.3332V69.3333H60.6665V78H73.6665C76.0498 78 77.9998 76.05 77.9998 73.6667V4.33333C77.9998 1.95 76.0498 0 73.6665 0ZM51.9998 17.3333H43.3332V8.66667H51.9998V17.3333ZM69.3332 34.6667H60.6665V26H69.3332V34.6667ZM69.3332 17.3333H60.6665V8.66667H69.3332V17.3333Z"
                                    fill="#C80022" />
                                <path
                                    d="M52.0003 73.6725V40.5225C51.9988 39.9178 51.8709 39.3201 51.6246 38.7678C51.3783 38.2156 51.0192 37.721 50.5703 37.3158L28.9036 17.8158C28.1182 17.0785 27.0774 16.6746 26.0003 16.6891C24.9603 16.6891 23.9203 17.0791 23.0969 17.8158L1.43027 37.3158C0.974839 37.7156 0.611247 38.2092 0.364422 38.7627C0.117596 39.3162 -0.00662609 39.9165 0.000272359 40.5225V73.6725C0.000272359 76.0558 1.95027 78.0058 4.33361 78.0058H13.0003C15.3836 78.0058 17.3336 76.0558 17.3336 73.6725V56.3391H34.6669V73.6725C34.6669 76.0558 36.6169 78.0058 39.0003 78.0058H47.6669C50.0503 78.0058 52.0003 76.0558 52.0003 73.6725Z"
                                    fill="#C4C4C4" />
                            </svg>
                        </div>
                        <div class="stats--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW"=>"file", "PATH"=>"/includes/local-goverments/cityPoints.php"]); ?>
                        </div>
                    </div>
                    <div class="stats">
                        <div class="stats__icon">

                            <svg width="94" height="94" viewBox="0 0 94 94" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M51.6396 66.2881L30.2646 42.5381C29.3645 41.5358 27.6355 41.5358 26.7353 42.5381L5.36034 66.2881C5.05267 66.6291 4.85072 67.0522 4.77904 67.5059C4.70736 67.9596 4.76904 68.4243 4.95659 68.8436C5.33659 69.701 6.18684 70.252 7.12496 70.252H11.875V86.877C11.875 87.5069 12.1252 88.1109 12.5706 88.5563C13.016 89.0017 13.6201 89.252 14.25 89.252H21.375C22.0049 89.252 22.6089 89.0017 23.0543 88.5563C23.4997 88.1109 23.75 87.5069 23.75 86.877V77.377H33.25V86.877C33.25 87.5069 33.5002 88.1109 33.9456 88.5563C34.391 89.0017 34.9951 89.252 35.625 89.252H42.75C43.3799 89.252 43.9839 89.0017 44.4293 88.5563C44.8747 88.1109 45.125 87.5069 45.125 86.877V70.252H49.875C50.3349 70.2539 50.7855 70.122 51.1718 69.8723C51.5581 69.6226 51.8634 69.2659 52.0504 68.8457C52.2375 68.4255 52.2983 67.96 52.2253 67.5058C52.1524 67.0517 51.9488 66.6286 51.6396 66.2881Z"
                                    fill="#C4C4C4" />
                                <path
                                    d="M89.6742 47.6359L72.4242 28.4692C71.6978 27.6604 70.3025 27.6604 69.576 28.4692L52.326 47.6359C52.0778 47.9111 51.9148 48.2525 51.8569 48.6187C51.7991 48.9848 51.8489 49.3598 52.0002 49.6982C52.3069 50.3901 52.993 50.8348 53.7501 50.8348H57.5835V64.2515C57.5835 64.7598 57.7854 65.2473 58.1448 65.6068C58.5043 65.9662 58.9918 66.1681 59.5001 66.1681H65.2501C65.7585 66.1681 66.246 65.9662 66.6054 65.6068C66.9649 65.2473 67.1668 64.7598 67.1668 64.2515V56.5848H74.8335V64.2515C74.8335 64.7598 75.0354 65.2473 75.3948 65.6068C75.7543 65.9662 76.2418 66.1681 76.7501 66.1681H82.5001C83.0085 66.1681 83.496 65.9662 83.8554 65.6068C84.2149 65.2473 84.4168 64.7598 84.4168 64.2515V50.8348H88.2501C88.6213 50.8364 88.985 50.7299 89.2967 50.5284C89.6084 50.3269 89.8548 50.039 90.0058 49.6999C90.1567 49.3608 90.2058 48.9851 90.1469 48.6186C90.088 48.2521 89.9238 47.9107 89.6742 47.6359Z"
                                    fill="#C4C4C4" />
                                <path
                                    d="M56.1441 21.0667L40.7691 3.98335C40.1216 3.26244 38.878 3.26244 38.2305 3.98335L22.8555 21.0667C22.6342 21.312 22.489 21.6163 22.4374 21.9426C22.3858 22.269 22.4302 22.6033 22.5651 22.9049C22.8384 23.5216 23.45 23.9179 24.1248 23.9179H27.5415V35.8762C27.5415 36.3293 27.7215 36.7638 28.0418 37.0842C28.3622 37.4046 28.7967 37.5846 29.2498 37.5846H34.3748C34.8279 37.5846 35.2624 37.4046 35.5828 37.0842C35.9032 36.7638 36.0831 36.3293 36.0831 35.8762V29.0429H42.9165V35.8762C42.9165 36.3293 43.0965 36.7638 43.4168 37.0842C43.7372 37.4046 44.1717 37.5846 44.6248 37.5846H49.7498C50.2029 37.5846 50.6374 37.4046 50.9578 37.0842C51.2782 36.7638 51.4581 36.3293 51.4581 35.8762V23.9179H54.8748C55.2057 23.9193 55.5298 23.8244 55.8076 23.6448C56.0855 23.4652 56.3051 23.2086 56.4396 22.9064C56.5742 22.6041 56.6179 22.2693 56.5654 21.9426C56.5129 21.6159 56.3666 21.3116 56.1441 21.0667Z"
                                    fill="#C80022" />
                            </svg>
                        </div>
                        <div class="stats--col">
                            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW"=>"file", "PATH"=>"/includes/local-goverments/villagePoints.php"]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h3>
            Муниципальные образования
        </h3>
        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "",
            Array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "DATE_ACTIVE_FROM",
                "SORT_ORDER2" => "DESC",
                "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
                "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                "SET_TITLE" => $arParams["SET_TITLE"],
                "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                "MESSAGE_404" => $arParams["MESSAGE_404"],
                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                "SHOW_404" => $arParams["SHOW_404"],
                "FILE_404" => $arParams["FILE_404"],
                "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                "CHECK_DATES" => $arParams["CHECK_DATES"],
            ),
            $component
        );
        ?>
    </div>
</section>