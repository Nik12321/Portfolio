<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="news-detail">
    <div class="wrap">
        <div class="news-detail__item--img" style="background-image: url(<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>);"></div>

        <div class="column">
            <div class="news-detail__item--date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
            <div class="news__item--title"><?=$arResult["NAME"]?></div>
            <div class="news-detail__item--text">
                <?=htmlspecialcharsBack($arResult["PROPERTIES"]["TITLE"]["VALUE"]["TEXT"])?>
            </div>
        </div>
    </div>
    <?=$arResult["PREVIEW_TEXT"];?>
    <br><br>
    <?php if ($arResult["PROPERTIES"]["SOURCE"]["VALUE"]) :?>
        <p>
            <strong>
                <a target="_blank" href="<?=$arResult["PROPERTIES"]["SOURCE"]["VALUE"]?>">Источник</a>
            </strong>
        </p>
    <?php endif;?>
    <?php if (count($arResult["PROPERTIES"]["FILES"]["VALUE"])) :?>
        <?php foreach ($arResult["PROPERTIES"]["FILES"]["VALUE"] as $key => $value) : ?>
            <strong>
                <p><a href="<?=$value["FILE_PATH"]?>"><?=$value["FILE_NAME"]?></a></p>
                <p><?=$value["DATE"]?>  <?=$value["EXPANSION"]?>,  <?=$value["SIZE"]?></p>
            </strong>
        <?php endforeach;?>
    <?php endif;?>
    <br><br>
    <?php if (count($arResult["PROPERTIES"]["PHOTOS"]["VALUE"])) :?>
        <div class="news-detail__item--gallery">
            <?php foreach ($arResult["PROPERTIES"]["PHOTOS"]["VALUE"] as $key => $value) : ?>
                <a class="big-img" data-fancybox="gallery" href="<?=$value?>">
                    <img src="<?=$value?>">
                </a>
            <?endforeach;?>
        </div>
    <?php endif;?>
</div>

<!-- Пагинация -->
<div class="pagination">
    <?php if ($arResult["PREV_NEW"]) :?>
    <a href="<?=$arResult["PREV_NEW"]?>">
        <svg>
            <use xlink:href="#pagination-arrow-prev" />
        </svg>
        <span>Назад</span>
    </a>
    <?php endif;?>
    <?php if ($arResult["NEXT_NEW"]) :?>
        <a href="<?=$arResult["NEXT_NEW"]?>">
            <span>Далее</span>
            <svg>
                <use xlink:href="#pagination-arrow-next" />
            </svg>
        </a>
    <?php endif;?>
</div>
<!-- Пагинация -->