<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
//Ишем первую новость
if ($lastNew = CIBlockElement::GetList(
    ["DATE_ACTIVE_FROM" => "DESC", "SORT" => "ASC"],
    ['IBLOCK_ID'=>$arParams["IBLOCK_ID"], "ACTIVE" => "Y"],
    false,
    ["nTopCount" => 1],
    ["ID"])
    ->Fetch()) {
    $lastItemID = [intval($lastNew["ID"])];
}
$GLOBALS["LAST_NEW"]["ID"] = $lastItemID;
?>
    <!-- Последняя новость -->
    <div class="news--last">
        <div class="news__item">
            <?php //Выводим первую новость
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "lastNew",
                [
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "NEWS_COUNT" => 1,
                    "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                    "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                    "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                    "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
                    "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                    "SET_TITLE" => $arParams["SET_TITLE"],
                    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                    "MESSAGE_404" => $arParams["MESSAGE_404"],
                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                    "SHOW_404" => $arParams["SHOW_404"],
                    "FILE_404" => $arParams["FILE_404"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                    "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                    "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                    "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                    "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                    "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                    "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                    "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                    "FILTER_NAME" => "LAST_NEW",
                    "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                    "CHECK_DATES" => $arParams["CHECK_DATES"],
                ],
                $component
            );
            ?>
        </div>
    </div>
    <!-- Список новостей -->
    <div class="news--list">
<?php
//Выполняем преобразования для фильтр, его мы выведем в конце
//ob_start();
//$preFilter = [];
//if (!$_GET["set_filter"]) {
//    $years = $mounth = [];
//    $properties = CIBlockElement::GetList(
//        false,
//        ['IBLOCK_ID' => $arParams["IBLOCK_ID"], "!ID" => $lastItemID, "ACTIVE" => "Y"],
//        ["PROPERTY_YEAR"]
//    );
//    while ($property = $properties->Fetch()) {
//        $years[$property["PROPERTY_YEAR_VALUE"]] = true;
//    }
//    $yearValues = CIBlockPropertyEnum::GetList(
//        ["value"=>"DESC", "SORT"=>"ASC"],
//        ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE"=>"YEAR"]
//    );
//    while ($yearValue = $yearValues->Fetch()) {
//        if ($years[$yearValue["VALUE"]]) {
//            $selectedYear[] = $yearValue["VALUE"];
//            $preFilter["YEAR"] = $yearValue["ID"];
//            break;
//        }
//    }
//    $properties = CIBlockElement::GetList(
//        ["ACTIVE_FROM" => "DESC"],
//        ['IBLOCK_ID' => $arParams["IBLOCK_ID"], "PROPERTY" => ["YEAR" => $preFilter["YEAR"]], "!ID" => $lastItemID, "ACTIVE" => "Y"],
//        ["PROPERTY_MOUNTH"]
//    );
//    while ($property = $properties->Fetch()) {
//        $mounth[$property["PROPERTY_MOUNTH_VALUE"]] = true;
//        break;
//    }
//    $mounthValues = CIBlockPropertyEnum::GetList(
//        ["value" => "ASC", "SORT"=>"DESC"],
//        ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE"=>"MOUNTH"]
//    );
//    while ($mounthValue = $mounthValues->Fetch()) {
//        if ($mounth[$mounthValue["VALUE"]]) {
//            $preFilter["MOUNTH"] = $mounthValue["ID"];
//            break;
//        }
//    }
//}
//$APPLICATION->IncludeComponent(
//    "bws:catalog.filter",
//    "newsFilter",
//    Array(
//        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
//        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
//        "FILTER_NAME" => $arParams["FILTER_NAME"],
//        "FIELD_CODE" => $arParams["FILTER_FIELD_CODE"],
//        "PROPERTY_CODE" => $arParams["FILTER_PROPERTY_CODE"],
//        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
//        "CACHE_TIME" => $arParams["CACHE_TIME"],
//        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
//        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
//        "PRE_FILTER" => $preFilter,
//        "LAST_ITEM" => $lastItemID
//    ),
//    $component
//);
$GLOBALS[$arParams["FILTER_NAME"]]["!ID"] = $lastItemID;
//$APPLICATION->AddViewContent("filter", ob_get_clean());
//Выводим список новостей
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "",
    Array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "NEWS_COUNT" => $arParams["NEWS_COUNT"],
        "SORT_BY1" => "DATE_ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
        "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
        "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
        "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
        "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
        "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        "MESSAGE_404" => $arParams["MESSAGE_404"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "SHOW_404" => $arParams["SHOW_404"],
        "FILE_404" => $arParams["FILE_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_FILTER" => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["PAGER_TITLE"],
        "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
        "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
        "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
        "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
        "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
        "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
        "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
        "FILTER_NAME" => $arParams["FILTER_NAME"],
        "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
    ),
    $component
);
//Выводим фильтр
//$APPLICATION->ShowViewContent("filter");
?>