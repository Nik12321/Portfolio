<?php
if (count($arResult["PROPERTIES"]["PHOTOS"]["VALUE"])) {
    foreach ($arResult["PROPERTIES"]["PHOTOS"]["VALUE"] as $key => $value) {
        $file = CFile::GetFileArray($value);
        $arResult["PROPERTIES"]["PHOTOS"]["VALUE"][$key] = $file["SRC"];
    }
}
if (!$arResult["PREVIEW_PICTURE"]["SRC"]) {
    $arResult["PREVIEW_PICTURE"]["SRC"] = DEFAULT_TEMPLATE_PATH . "/img/plug-img.svg";
}
$nextDay = date("d.m.Y", strtotime($arResult["DATE_ACTIVE_FROM"] . " + 1 day"));
$prevDay = date("d.m.Y", strtotime($arResult["DATE_ACTIVE_FROM"] . " - 1 day"));
$prev = $next = "";
$elements = CIBlockElement::GetList(
    ["DATE_ACTIVE_FROM"=>"DESC", "SORT" => "DESC"],
    ["IBLOCK_ID" => $arResult["IBLOCK_ID"], "ACTIVE" => "Y", ["AND",  ">DATE_ACTIVE_FROM" => $prevDay,  "<DATE_ACTIVE_FROM" => $nextDay]],
    false,
    false,
    ["DETAIL_PAGE_URL", "NAME"]
);
while ($element = $elements->GetNext()) {
    if ($element["NAME"] == $arResult["NAME"]) {
        $nextElem = $elements->GetNext();
        if ($nextElem) {
            $next = $nextElem;
        }
        break;
    }
    $prev = $element;
}
if (!$prev) {
    $prev = CIBlockElement::GetList(
        ["DATE_ACTIVE_FROM"=>"DESC", "SORT" => "ASC"],
        ["IBLOCK_ID" => $arResult["IBLOCK_ID"], "ACTIVE" => "Y", "<DATE_ACTIVE_FROM" => $arResult["DATE_ACTIVE_FROM"], "!ID" => $arResult["ID"]],
        false,
        false,
        ["DETAIL_PAGE_URL"]
    )->GetNext();
}
$prev = $prev["DETAIL_PAGE_URL"];
if (!$next) {
    $next = CIBlockElement::GetList(
        ["DATE_ACTIVE_FROM"=>"ASC", "SORT" => "DESC"],
        ["IBLOCK_ID" => $arResult["IBLOCK_ID"], "ACTIVE" => "Y", ">DATE_ACTIVE_FROM" => $arResult["DATE_ACTIVE_FROM"], "!ID" => $arResult["ID"]],
        false,
        false,
        ["DETAIL_PAGE_URL"]
    )->GetNext();
}
$next = $next["DETAIL_PAGE_URL"];
$arResult["PREV_NEW"] = $prev;
$arResult["NEXT_NEW"] = $next;

foreach ($arResult["PROPERTIES"]["FILES"]["VALUE"] as $key => $value) {
    $fileArray = [];
    $arFile = CFile::GetFileArray($value);
    $parseTime = date_parse_from_format("j.n.Y H:i", $arResult["TIMESTAMP_X"]);
    $fileArray['FILE_PATH'] = ($arFile["SRC"]) ? $arFile["SRC"] : false;
    $fileArray["FILE_NAME"] = $arFile["DESCRIPTION"];
    $fileArray["SIZE"] = CFile::FormatSize($arFile["FILE_SIZE"] ?? 0);
    $fileArray["EXPANSION"] = GetFileExtension($arFile["FILE_NAME"]);
    $fileArray["DATE"] = (($parseTime["day"] < 10)
            ? "0" . $parseTime["day"]
            : $parseTime["day"])
        . "."
        . (($parseTime["month"] < 10)
            ? "0" . $parseTime["month"]
            : $parseTime["month"])
        . "."
        . $parseTime["year"];
    $arResult["PROPERTIES"]["FILES"]["VALUE"][$key] = $fileArray;
}