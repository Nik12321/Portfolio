<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php foreach($arResult["ITEMS"] as $arItem) :?>
    <?php
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="news__item--date adaptive-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
        <div class="news__item--img" style="background-image: url(<?=
        ($arItem["PREVIEW_PICTURE"]["SRC"]
            ? $arItem["PREVIEW_PICTURE"]["SRC"]
            :  DEFAULT_TEMPLATE_PATH . "/img/plug-img.svg")
        ?>);"></div>
        <div class="news__item--column">
            <div class="news__item--date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
            <div class="news__item--title"><?=$arItem["NAME"]?></div>
            <div class="news__item--text"><?=strip_tags(htmlspecialcharsBack($arItem["PROPERTIES"]["TITLE"]["VALUE"]["TEXT"], '<br>'))?></div>
            <div class="news__item--detail">
                <span class="btn--link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Читать подробнее</span>
            </div>
        </div>
    </a>
<?php endforeach;?>