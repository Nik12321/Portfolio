<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if (count($arResult['ITEMS'])) :?>
<div class="infa-sup__content">
    <?php foreach ($arResult["ITEMS"] as $arItem) : ?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="infa-sup__content--item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <div class="item--img">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="logo6">
                </div>
                <div class="item--title"><?=$arItem["NAME"]?></div>
                <div class="item--text">
                    <h4>Меры поддержки</h4>
                    <p><?=$arItem["PROPERTIES"]["SUP_MEASURES"]["VALUE"]?></p>
                </div>
                <div class="item--link">
                    Подробнее...
                </div>
            </a>
        </div>
    <?php endforeach;?>
</div>
<?php endif;?>
