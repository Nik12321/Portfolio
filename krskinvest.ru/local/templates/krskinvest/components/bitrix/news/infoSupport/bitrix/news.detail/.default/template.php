<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=9ce47341-3963-4c13-8f98-7307288807e5" type="text/javascript"></script>
<!-- секция основной информации страницы -->
<section class="support-page">
    <div class="container">
        <h2><?=$arResult["NAME"]?></h2>
        <div class="support-page__content">
            <div class="support-page__content--img">
                <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="arb">
            </div>
            <div class="support-page__content--text">
                <?php if ($arResult["PROPERTIES"]["PHONE"]["VALUE"]) :?>
                    <h4>Телефон </h4>
                    <a href="tel:<?=str_replace([" ", "(", ")", "-"], "", $arResult["PROPERTIES"]["PHONE"]["VALUE"])?>">
                        <?=$arResult["PROPERTIES"]["PHONE"]["VALUE"]?>
                    </a>
                <?php endif;?>
                <?php if ($arResult["PROPERTIES"]["EMAIL"]["VALUE"]) :?>
                    <h4>E-mail</h4>
                    <a href="mailto:<?=$arResult["PROPERTIES"]["EMAIL"]["VALUE"]?>">
                        <?=$arResult["PROPERTIES"]["EMAIL"]["VALUE"]?>
                    </a>
                <?php endif;?>
                <?php if ($arResult["PROPERTIES"]["SITE_ADDR"]["VALUE"]) :?>
                    <h4>Адрес сайта</h4>
                    <a target="_blank" href="<?=$arResult["PROPERTIES"]["SITE_ADDR"]["VALUE"]?>">
                        <?=$arResult["PROPERTIES"]["SITE_ADDR"]["VALUE"]?>
                    </a>
                <?php endif;?>
                <h4>Наименование организации</h4>
                <p><?=$arResult["NAME"]?></p>
                <?php if ($arResult["PROPERTIES"]["GOAL"]["VALUE"]) :?>
                    <h4>Цель</h4>
                    <p><?=$arResult["PROPERTIES"]["GOAL"]["VALUE"]?></p>
                <?php endif;?>
                <?php if ($arResult["PROPERTIES"]["TASKS"]["VALUE"]) :?>
                    <h4>Задачи</h4>
                    <p><?=$arResult["PROPERTIES"]["TASKS"]["VALUE"]?></p>
                <?php endif;?>
                <?php if ($arResult["PROPERTIES"]["SUP_MEASURES"]["VALUE"]) :?>
                    <h4>Меры поддержки</h4>
                    <p><?=$arResult["PROPERTIES"]["SUP_MEASURES"]["VALUE"]?></p>
                <?php endif;?>
                <?php if ($arResult["PROPERTIES"]["MUNICIPALITY"]["VALUE"]) :?>
                    <h4>Муниципальное образование</h4>
                    <p><?=$arResult["PROPERTIES"]["MUNICIPALITY"]["VALUE"]?></p>
                <?php endif;?>
                <?php if ($arResult["PROPERTIES"]["LEADER"]["VALUE"]) :?>
                    <h4>Руководитель</h4>
                    <p><?=$arResult["PROPERTIES"]["LEADER"]["VALUE"]?></p>
                <?php endif;?>
                <?php if ($arResult["PROPERTIES"]["ADDRESS"]["VALUE"]) :?>
                    <h4>Адрес</h4>
                    <p><?=$arResult["PROPERTIES"]["ADDRESS"]["VALUE"]?></p>
                <?php endif;?>
                <?php if (count($arResult["PROPERTIES"]["FILIALS"]["VALUE"]) && $arResult["PROPERTIES"]["FILIALS"]["VALUE"]) :?>
                    <h4>Филиалы и представительства </h4>
                    <p>Представительства в Красноярском крае:</p>
                    <div class="support-page__content--extra">
                        <div class="support-page__content--branches">
                            <?php foreach($arResult["PROPERTIES"]["FILIALS"]["VALUE"] as $filial) :?>
                                <div>
                                    <?=htmlspecialcharsBack($filial["TEXT"])?>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif;?>
                <?php if (count($arResult["PROPERTIES"]["PHOTOS"]["VALUE"]) && $arResult["PROPERTIES"]["PHOTOS"]["VALUE"]) :?>
                    <h4>Презентация</h4>
                    <div class="news-detail__item--gallery">
                        <?php foreach ($arResult["PROPERTIES"]["PHOTOS"]["VALUE"] as $key => $value) : ?>
                            <a class="big-img" data-fancybox="gallery" href="<?=$value?>">
                                <img src="<?=$value?>">
                            </a>
                        <?endforeach;?>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
    <?php if ($arResult["PROPERTIES"]["COORDINATES"]["VALUE"]) :?>
        <div class="support-page__container--map">
            <div class="grey-map-btn">
                <button id="fullscreenMap">Смотреть обьект на карте</button>
            </div>
            <div class="map-page" id="mapID"></div>
        </div>
        <script>
            ymaps.ready(function () {
                <?php
                $coordinate = explode(",", $arResult["PROPERTIES"]["COORDINATES"]["VALUE"][0]);
                $coordinate = array_map('trim', $coordinate);
                ?>
                var myMap = new ymaps.Map('mapID', {
                    center: [<?=$coordinate[0]?>, <?=$coordinate[1]?>],
                    zoom: 15,
                    controls: ['zoomControl']
                }, {
                    searchControlProvider: 'yandex#search'
                });

                // Кнопка fullscreen
                let btn = document.getElementById('fullscreenMap')
                var fullscreenControl = new ymaps.control.FullscreenControl();
                myMap.controls.add(fullscreenControl);
                btn.onclick = function () {
                    fullscreenControl.enterFullscreen();
                }

                // Создаём макет содержимого.
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                );
                <?php foreach($arResult["PROPERTIES"]["COORDINATES"]["VALUE"] as $key => $value) :?>
                <?php
                $coordinate = explode(",", $value);
                $coordinate = array_map('trim', $coordinate);
                ?>
                myPlacemarkWithContent = new ymaps.Placemark([<?=$coordinate[0]?>, <?=$coordinate[1]?>], {
                    hintContent: '<?=$arResult["NAME"]?>',
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#imageWithContent',
                    // Своё изображение иконки метки.
                    iconImageHref: '<?=DEFAULT_TEMPLATE_PATH?>/img/marker-map.svg',
                    // Размеры метки.
                    iconImageSize: [48, 48],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-24, -24],
                    // Смещение слоя с содержимым относительно слоя с картинкой.
                    iconContentOffset: [15, 15],
                    // Макет содержимого.
                    iconContentLayout: MyIconContentLayout
                });
                myMap.geoObjects.add(myPlacemarkWithContent);
                <?php endforeach;?>
            });
        </script>
    <?php endif;?>
</section>