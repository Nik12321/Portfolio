<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
$arUniqueId = array();
$counter = 0;
?>
<div class="btn-style-2--wrap">
    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $uniqueId = randString(4);
        $arUniqueId[$counter] = $uniqueId;
        ?>
        <button class="btn-style-3 tab" id="<?php if($counter == 0) echo "defaultOpen"?>" onclick="openTab(event, '<?=$arUniqueId[$counter]?>')">
                <span id="<?=$this->GetEditAreaId($arItem['ID']);?>"><?=$arItem['NAME']?></span>
            <?=$arItem['PROPERTIES']['SVG']['~VALUE']['TEXT']?>
        </button>
    <?php
    $counter++;
    endforeach; ?>
</div>
<?php
$counter = 0;
foreach ($arResult["ITEMS"] as $arItem): ?>
    <div class="tabcontent" id="<?=$arUniqueId[$counter]?>" style="display: none;">
        <div class="gray-tab--inner">
            <?=$arItem["DETAIL_TEXT"]?>
        </div>
    </div>
<?php
$counter++;
endforeach; ?>


