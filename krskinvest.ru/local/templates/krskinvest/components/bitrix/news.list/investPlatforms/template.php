<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if(count($arResult["ITEMS"])):?>
    <div class="platform__slider--nav-container">
        <div class="swiper-container platform__slider--nav">
            <div class="swiper-wrapper">
                <?php foreach ($arResult["ITEMS"] as $arItem) :?>
                    <div class="swiper-slide">
                        <div class="slide">
                            <div class="slide-text">
                                «<?=$arItem["NAME"]?>»
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
        <div class="swiper-button-next swiper-button-white"></div>
        <div class="swiper-button-prev swiper-button-white"></div>
    </div>
    <div class="swiper-container platform__slider">
        <div class="swiper-wrapper">
            <?php foreach($arResult["ITEMS"] as $arItem) :?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="swiper-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="platform__content">
                        <div class="platform__content--col">
                            <?php if ($arItem["PROPERTIES"]["LOCATION"]["VALUE"]) :?>
                                <h5>Месторасположение</h5>
                                <p><?=$arItem["PROPERTIES"]["LOCATION"]["VALUE"]?></p>
                            <?php endif;?>
                            <?php if ($arItem["PROPERTIES"]["TYPE"]["VALUE"]) :?>
                                <h5>Форма собственности</h5>
                                <p><?=$arItem["PROPERTIES"]["TYPE"]["VALUE"]?></p>
                            <?php endif;?>

                            <?php if ($arItem["PROPERTIES"]["MANAGEMENT_COMPANY"]["VALUE"]) :?>
                                <h5>Управляющая компания</h5>
                                <p><?=$arItem["PROPERTIES"]["MANAGEMENT_COMPANY"]["VALUE"]?></p>
                            <?php endif;?>

                            <?php if ($arItem["PROPERTIES"]["SQUARE"]["VALUE"]) :?>
                                <h5>Площадь (кв. м)</h5>
                                <p><?=$arItem["PROPERTIES"]["SQUARE"]["VALUE"]?></p>
                            <?php endif;?>
                            <?php if ($arItem["PROPERTIES"]["CONTACTS"]["VALUE"]["TEXT"]) :?>
                                <div class="platform__content--contact">
                                    <h5>Контактные данные</h5>
                                    <?=htmlspecialchars_decode($arItem["PROPERTIES"]["CONTACTS"]["VALUE"]["TEXT"])?>
                                </div>
                            <?php endif;?>
                        </div>
                        <div class="platform__content--col">
                            <?php if ($arItem["PROPERTIES"]["IS_AVAILABILITY"]["VALUE"] == "Да") :?>
                                <h5 class="underline">Наличие подведённой к объекту инфраструктуры</h5>
                            <?php endif;?>
                            <?php if ($arItem["PROPERTIES"]["ELECTRICITY"]["VALUE"]) :?>
                                <h5>Электроэнергия</h5>
                                <p><?=$arItem["PROPERTIES"]["ELECTRICITY"]["VALUE"]?></p>
                            <?php endif;?>
                            <?php if ($arItem["PROPERTIES"]["HEAT_SUPPLY"]["VALUE"]) :?>
                                <h5>Управляющая компания</h5>
                                <p><?=$arItem["PROPERTIES"]["HEAT_SUPPLY"]["VALUE"]?></p>
                            <?php endif;?>
                            <?php if ($arItem["PROPERTIES"]["WATER_SUPPLY"]["VALUE"]) :?>
                                <h5>Водообеспечение</h5>
                                <p><?=$arItem["PROPERTIES"]["WATER_SUPPLY"]["VALUE"]?></p>
                            <?php endif;?>
                            <?php if ($arItem["PROPERTIES"]["TRANSPORTY_INFRASTRUCTURE"]["VALUE"]) :?>
                                <h5>Транспортная инфраструктура</h5>
                                <p><?=$arItem["PROPERTIES"]["TRANSPORTY_INFRASTRUCTURE"]["VALUE"]?></p>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
<?php endif;?>
<script>
    var galleryThumbs = new Swiper('.platform__slider--nav', {
        spaceBetween: 10,
        slidesPerView: 1,
        freeMode: false,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1100: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
            },
            545: {
                slidesPerView: 2,
            }
        },
        on: {
            init: function() {
                checkArrow(<?=count($arResult["ITEMS"])?>);
            },
            resize: function () {
                checkArrow(<?=count($arResult["ITEMS"])?>);
            }
        }
    });
    var galleryTop = new Swiper('.platform__slider', {
        spaceBetween: 10,
        autoHeight: true,
        coverflowEffect: {
            rotate: 30,
            slideShadows: false,
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });

    function checkArrow(col) {
        var swiperPrev = document.querySelector('.swiper-button-prev');
        var swiperNext = document.querySelector('.swiper-button-next');
        if (col == 4) {
            if (window.innerWidth < 1100) {
                swiperPrev.style.display = 'block';
                swiperNext.style.display = 'block';
            } else {
                swiperPrev.style.display = 'none';
                swiperNext.style.display = 'none';
            }
        } else if (col == 3) {
            if (window.innerWidth < 768) {
                swiperPrev.style.display = 'block';
                swiperNext.style.display = 'block';
            } else {
                swiperPrev.style.display = 'none';
                swiperNext.style.display = 'none';
            }
        } else if (col == 2) {
            if (window.innerWidth < 545) {
                swiperPrev.style.display = 'block';
                swiperNext.style.display = 'block';
            } else {
                swiperPrev.style.display = 'none';
                swiperNext.style.display = 'none';
            }
        }
    }
</script>
