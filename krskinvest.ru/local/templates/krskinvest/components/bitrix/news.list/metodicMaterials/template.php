<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])):?>
<div class="metodic-material__content">
    <div class="investpaper__content--list item-2">
        <div class="list-flex">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="list--element" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <a download="<?=$arItem["NAME"] . "." . $arItem["EXPANSION"]?>" href="<?=($arItem['FILE_PATH']) ? $arItem['FILE_PATH'] : "#"?>">
                        <div class="item__img">
                            <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="investpaperEnRU">
                        </div>
                        <div class="item__title"><?=$arItem["NAME"]?></div>
                        <div class="item__subtitle"><?=strtoupper($arItem["EXPANSION"])?>, <?=$arItem["SIZE"]?></div>
                    </a>
                </div>
            <?endforeach;?>
        </div>
    </div>
</div>
<?endif;?>