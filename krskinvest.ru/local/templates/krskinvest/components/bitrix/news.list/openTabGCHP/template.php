<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<?php foreach($arResult["ITEMS"] as $arItem): ?>
    <?php
    $uniqueId = randString(4);
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <input type="checkbox" id="uniqueID<?=$uniqueId?>" hidden>
        <label for="uniqueID<?=$uniqueId?>">
            <div class="item--row">
                <div class="item--icon">
                    <?php
                    if ($arItem["PROPERTIES"]["SVG_IMAGE"]["VALUE"] != "") //если картинка задана в свойстве
                    {
                        $image = CFile::GetPath($arItem["PROPERTIES"]["SVG_IMAGE"]["VALUE"]); //то берем ее из свойства SVG_IMAGE
                    } else $image = $arItem["PREVIEW_PICTURE"]["SRC"]; //иначе берем ее из картинки для анонса
                    ?>
                    <img src="<?= $image ?>" alt="">
                </div>
                <div class="item--title">
                    <h5>Территория реализации</h5>
                    <p><?=$arItem["PROPERTIES"]["TERRITORY_OF_REALIZATION"]["VALUE"]?></p>
                </div>
            </div>
            <div class="item--text">
                <?=$arItem["PREVIEW_TEXT"]?>
            </div>
        </label>
        <div class="item--body gray-tab--inner">
            <?=$arItem["DETAIL_TEXT"]?>
        </div>
    </div>
<?php endforeach ?>
