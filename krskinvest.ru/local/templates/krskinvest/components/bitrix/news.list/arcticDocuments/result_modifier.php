<?php
$arFiles = $arTexts = [];
foreach($arResult["ITEMS"] as &$arItem) {
    if ($arItem["PROPERTIES"]["FILE"]["VALUE"]) {
        $arFile = CFile::GetFileArray($arItem["PROPERTIES"]["FILE"]["VALUE"]);
        $arItem['FILE_PATH'] = ($arFile["SRC"]) ? $arFile["SRC"] : false;
        $arItem["EXPANSION"] = GetFileExtension($arFile["FILE_NAME"]);
        $arItem["SIZE"] = CFile::FormatSize($arFile["FILE_SIZE"] ?? 0);
        $arFiles[] = $arItem;
    } else {
        $arTexts[] = $arItem;
    }
}
unset($arItem);
$arResult["FILES"] = $arFiles;
$arResult["TEXTS"] = $arTexts;