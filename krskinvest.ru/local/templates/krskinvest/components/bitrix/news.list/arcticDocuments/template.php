<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])):?>
    <?php if (count($arResult["FILES"])) :?>
        <div class="arctic-doc__file-list">
            <?php foreach($arResult["FILES"] as $arItem):?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <a download="<?=$arItem["NAME"] . "." . $arItem["EXPANSION"]?>" href="<?=$arItem["FILE_PATH"]?>" class="file-btn" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="file-btn__icon"><svg><use xlink:href="#<?=$arItem["EXPANSION"]?>"></svg></div>
                    <div class="file-btn__info">
                        <div class="file-btn__info--name">
                            <?=$arItem["PROPERTIES"]["FULL_NAME"]["VALUE"]?>
                        </div>
                        <div class="file-btn__info--meta">
                            <?=strtoupper($arItem["EXPANSION"])?>, <?=$arItem["SIZE"]?>
                        </div>
                    </div>
                </a>
            <?php endforeach;?>
        </div>
    <?php endif;?>
    <?php if (count($arResult["TEXTS"])) :?>
        <div class="arctic-doc__text">
            <?php foreach($arResult["TEXTS"] as $arItem):?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <p id="<?=$this->GetEditAreaId($arItem['ID']);?>"><?=$arItem["PROPERTIES"]["FULL_NAME"]["VALUE"]?></p>
            <?php endforeach;?>
        </div>
    <?php endif;?>
<?endif;?>