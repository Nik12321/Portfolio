<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if (count($arResult["ITEMS"])) :?>
<div class="commessioners__authorized-krsk--wrap">
    <?foreach($arResult["ITEMS"] as $key => $arItem):?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <input id="item-<?=$key?>" type="checkbox" hidden>
            <label for="item-<?=$key?>">
                <div class="item--icon">
                   <?=htmlspecialcharsback($arItem["PROPERTIES"]["SVG"]["VALUE"]);?>
                </div>
                <div class="item--title"><?=$arItem["NAME"]?></div>
            </label>
            <?php foreach ($arItem["PROPERTIES"]["FIO"]["VALUE"] as $key => $value) :?>
                <div class="item--more-info">
                    <h6><?=$arItem["PROPERTIES"]["UNIT"]["VALUE"][$key]?></h6>
                    <p><?=$arItem["PROPERTIES"]["FIO"]["VALUE"][$key]?></p>
                    <p><?=$arItem["PROPERTIES"]["POSITION"]["VALUE"][$key]?></p>
                    <?php
                    $phones = $arItem["PROPERTIES"]["CONTACTS"]["VALUE"][$key];
                    $phones = explode(";", $phones);
                    ?>
                    <?php foreach ($phones as $phone) :?>
                        <a href="tel:<?=str_replace([" ", ")", "(", "-"],"", $phone)?>">
                            <?=$phone?>
                        </a>
                    <?php endforeach;?>
                </div>
            <?php endforeach;?>
        </div>
    <?endforeach;?>
</div>
<?php endif;?>