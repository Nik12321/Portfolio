<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])):?>
    <?php foreach($arResult["ITEMS"] as $key => $arItem):?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="arctic-pref__dropdown" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <h4><?=$arItem["NAME"]?></h4>
            <input type="checkbox" id="<?="supSection-" . $arItem["IBLOCK_SECTION_ID"]?><?=$key?>" hidden="">
            <label for="<?="supSection-" . $arItem["IBLOCK_SECTION_ID"]?><?=$key?>" class="arctic-pref__dropdown--head">
                <?php if ($arItem["PROPERTIES"]["TITLE"]["VALUE"]) :?>
                    <?=$arItem["PROPERTIES"]["TITLE"]["VALUE"]?>
                <?php else :?>
                    <?=htmlspecialcharsback($arItem["PROPERTIES"]["SVG"]["VALUE"])?>
                <?php endif;?>
            </label>
            <div class="arctic-pref__dropdown--body">
                <div class="inner">
                    <?=$arItem["PREVIEW_TEXT"]?>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php endif;?>