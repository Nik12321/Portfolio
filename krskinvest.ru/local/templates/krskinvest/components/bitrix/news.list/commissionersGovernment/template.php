<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if (count($arResult["ITEMS"])) :?>
    <div class="commessioners__authorized-other--wrap">
        <?php foreach($arResult["ITEMS"] as $key => $arItem):?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="item--inner">
                    <div class="item--title"><?=$arItem["NAME"]?></div>
                    <div class="item--gerb">
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                    </div>
                    <div class="item--text">
                        <p><?=$arItem["PROPERTIES"]["FIO"]["VALUE"]?></p>
                        <p><?=$arItem["PROPERTIES"]["POSITION"]["VALUE"]?></p>
                        <?php foreach($arItem["PROPERTIES"]["CONTACTS"]["VALUE"] as $number) :?>
                            <p>
                                <a href="tel:<?=str_replace([" ", ")", "(", "-"],"", $number)?>">
                                    <?=$number?>
                                </a>
                            </p>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
<?php endif;?>