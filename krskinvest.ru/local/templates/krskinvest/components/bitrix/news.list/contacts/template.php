<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if (count($arResult["ITEMS"])) :?>
    <div class="feedback__contact">
        <?php foreach($arResult["ITEMS"] as $key => $arItem):?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="contact-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="contact-item--title">
                    <?=$arItem["NAME"]?>
                </div>
                <div class="contact-item--icon">
                    <?=htmlspecialcharsBack($arItem["PROPERTIES"]["SVG"]["VALUE"]["TEXT"])?>
                </div>
                <div class="contact-item--link-list">
                    <?php if ($arItem["PROPERTIES"]["PHONE"]["VALUE"]) :?>
                        <div class="link-item">
                            Телефон:
                            <a href="tel:<?=str_replace([" ", ")", "(", "-"], "", $arItem["PROPERTIES"]["PHONE"]["VALUE"])?>">
                                <?=$arItem["PROPERTIES"]["PHONE"]["VALUE"]?>
                            </a>
                        </div>
                    <?php endif;?>
                    <?php if ($arItem["PROPERTIES"]["FAX"]["VALUE"]) :?>
                        <div class="link-item">
                            Факс:
                            <a href="fax:<?=str_replace([" ", ")", "(", "-"], "", $arItem["PROPERTIES"]["FAX"]["VALUE"])?>">
                                <?=$arItem["PROPERTIES"]["FAX"]["VALUE"]?>
                            </a>
                        </div>
                    <?php endif;?>
                    <?php if ($arItem["PROPERTIES"]["SITE"]["VALUE"]) :?>
                        <div class="link-item">
                            Веб-сайт:
                            <a target="_blank" href="<?=$arItem["PROPERTIES"]["SITE"]["VALUE"]?>" class="red-link">
                                <?=$arItem["PROPERTIES"]["SITE"]["VALUE"]?>
                            </a>
                        </div>
                    <?php endif;?>
                    <?php if ($arItem["PROPERTIES"]["EMAIL"]["VALUE"]) :?>
                        <div class="link-item">
                            e-mail:
                            <a href="mailto:<?=$arItem["PROPERTIES"]["EMAIL"]["VALUE"]?>" class="red-link">
                                <?=$arItem["PROPERTIES"]["EMAIL"]["VALUE"]?>
                            </a>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        <?php endforeach;?>
    </div>
<?php endif;?>