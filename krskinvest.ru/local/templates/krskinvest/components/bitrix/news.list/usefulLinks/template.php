<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="usefullink__content">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="usefullink__content--item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <a target="_blank" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">
                <div class="item--img" style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);"></div>
                <div class="item--text"><?=$arItem["NAME"]?></div>
            </a>
        </div>
    <?endforeach;?>
</div>
