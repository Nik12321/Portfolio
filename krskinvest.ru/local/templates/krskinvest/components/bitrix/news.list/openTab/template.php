<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<?php foreach ($arResult["ITEMS"] as $arItem): ?>
    <?php
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    $uniqueId = randString(4);
    ?>
    <div class="dropdown__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <input type="checkbox" hidden id="item-<?=$uniqueId ?>">
        <label class="item--head" for="item-<?=$uniqueId ?>">
            <span>
                <?php
                if ($arItem["PREVIEW_TEXT"] != "") { //если есть описание для анонса
                    echo $arItem["PREVIEW_TEXT"]; //выводим описание для анонса
                } else {  //иначе выводим название самого элемента
                    echo $arItem["NAME"];
                }
                ?>
            </span>
        </label>
        <div class="item--body">
            <div class="item--body-inner">
                <!-- Дополнительная информация в выпадающем поле настраивается в детальном описании.
                     Для правильной отработки стилей заголовки - <H6>, абзац - <p>.-->
                <?= $arItem["DETAIL_TEXT"] ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>

