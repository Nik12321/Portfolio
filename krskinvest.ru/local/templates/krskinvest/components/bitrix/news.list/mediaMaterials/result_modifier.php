<?php
foreach($arResult["ITEMS"] as &$arItem) {
    $arFile = CFile::GetFileArray($arItem["PROPERTIES"]["FILE"]["VALUE"]);
    $arItem['FILE_PATH'] = ($arFile["SRC"]) ? $arFile["SRC"] : false;
    $arItem["SIZE"] = CFile::FormatSize($arFile["FILE_SIZE"] ?? 0);
    $arItem["EXPANSION"] = GetFileExtension($arFile["FILE_NAME"]);
    $parseTime =  date_parse_from_format("j.n.Y H:i", $arItem["DATE_CREATE"]);
    $arItem["DATE"] = (($parseTime["day"] < 10)
            ? "0" . $parseTime["day"]
            : $parseTime["day"])
        . "."
        . (($parseTime["month"] < 10)
            ? "0" . $parseTime["month"]
            : $parseTime["month"])
        . "."
        . $parseTime["year"];
}

$firstItem = array_shift($arResult["ITEMS"]);
if (is_array($firstItem)) {
    $arResult["FIRST_ITEM"] = $firstItem;
}