<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="investpaper__content">
    <div class="investpaper__content--item item-1">
        <?php
        $this->AddEditAction($arResult["FIRST_ITEM"]['ID'], $arResult["FIRST_ITEM"]['EDIT_LINK'], CIBlock::GetArrayByID($arResult["FIRST_ITEM"]["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arResult["FIRST_ITEM"]['ID'], $arResult["FIRST_ITEM"]['DELETE_LINK'], CIBlock::GetArrayByID($arResult["FIRST_ITEM"]["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <a target="_blank" download="<?=$arResult["FIRST_ITEM"]["NAME"] . "." . $arResult["FIRST_ITEM"]["EXPANSION"]?>" href="<?=($arResult["FIRST_ITEM"]['FILE_PATH']) ? $arResult["FIRST_ITEM"]['FILE_PATH'] : "#"?>" id="<?=$this->GetEditAreaId($arResult["FIRST_ITEM"]['ID']);?>">
            <div class="item__img">
                <img src="<?=$arResult["FIRST_ITEM"]["DETAIL_PICTURE"]["SRC"]?>" alt="investpaperEnRU">
            </div>
            <div class="item__title"><?=$arResult["FIRST_ITEM"]["NAME"]?></div>
            <div class="item__subtitle"><?=strtoupper($arResult["FIRST_ITEM"]["EXPANSION"])?>, <?=$arResult["FIRST_ITEM"]["SIZE"]?></div>
        </a>
    </div>
    <div class="investpaper__content--list item-2">
        <div class="list-flex">
            <?php foreach($arResult["ITEMS"] as $arItem):?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="list--element" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <a download="<?=$arItem["NAME"] . "." . $arItem["EXPANSION"]?>" href="<?=($arItem['FILE_PATH']) ? $arItem['FILE_PATH'] : "#"?>">
                        <div class="item__img">
                            <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="investpaperEnRU">
                        </div>
                        <div class="item__title"><?=$arItem["NAME"]?></div>
                        <div class="item__subtitle"><?=strtoupper($arItem["EXPANSION"])?>, <?=$arItem["SIZE"]?></div>
                    </a>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>