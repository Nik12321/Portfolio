<?php
foreach($arResult["ITEMS"] as $itemKey => &$arItem) {
    foreach($arItem["PROPERTIES"]["FILES"]["VALUE"] as $key => $value) {
        $fileArray = [];
        $arFile = CFile::GetFileArray($value);
        $parseTime =  date_parse_from_format("j.n.Y H:i", $arItem["TIMESTAMP_X"]);
        $fileArray['FILE_PATH'] = ($arFile["SRC"]) ? $arFile["SRC"] : false;
        $fileArray["FILE_NAME"] = $arFile["ORIGINAL_NAME"];
        $fileArray["SIZE"] = CFile::FormatSize($arFile["FILE_SIZE"] ?? 0);
        $fileArray["EXPANSION"] = GetFileExtension($arFile["FILE_NAME"]);
        $fileArray["DATE"] = (($parseTime["day"] < 10)
                ? "0" . $parseTime["day"]
                : $parseTime["day"])
            . "."
            . (($parseTime["month"] < 10)
                ? "0" . $parseTime["month"]
                : $parseTime["month"])
            . "."
            . $parseTime["year"];
        $arItem["PROPERTIES"]["FILES"]["VALUE"][$key] = $fileArray;
    }
    foreach($arItem["PROPERTIES"]["PHOTOS"]["VALUE"] as $key => $value) {
        $arFile = CFile::GetFileArray($value);
        $src = ($arFile["SRC"]) ? $arFile["SRC"] : false;
        $arItem["PROPERTIES"]["PHOTOS"]["VALUE"][$key] = $src;
    }
}