<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if (count($arResult["ITEMS"])) :?>
    <?php foreach($arResult["ITEMS"] as $key => $arItem):?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="dropdown__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <input type="checkbox" hidden id="item-<?=$arItem['ID']?>">
            <label class="item--head" for="item-<?=$arItem['ID']?>">
                <span><?=$arItem["NAME"]?></span>
            </label>
            <div class="item--body">
                <div class="item--body-inner">
                    <?=$arItem["DETAIL_TEXT"]?>
                    <?php if ($arItem["PROPERTIES"]["FILES"]["VALUE"] && count($arItem["PROPERTIES"]["FILES"]["VALUE"])) :?>
                        <div class="meeting--file">
                            <?php foreach ($arItem["PROPERTIES"]["FILES"]["VALUE"] as $value) :?>
                            <a href="<?=$value["FILE_PATH"]?>" class="file-btn" download="<?=$value["FILE_NAME"]?>">
                                <div class="file-btn__icon">
                                    <svg><use xlink:href="#<?=$value["EXPANSION"]?>"></svg>
                                </div>
                                <div class="file-btn__info">
                                    <div class="file-btn__info--name">
                                        <?=$value["FILE_NAME"]?>
                                    </div>
                                    <div class="file-btn__info--meta">
                                        <?=strtoupper($value["EXPANSION"])?>, <?=$value["SIZE"]?>
                                    </div>
                                </div>
                            </a>
                            <?php endforeach;?>
                        </div>
                    <?php endif;?>
                    <?php if ($arItem["PROPERTIES"]["PHOTOS"]["VALUE"] && count($arItem["PROPERTIES"]["PHOTOS"]["VALUE"])) :?>
                        <div class="meeting--gallery">
                            <?php foreach ($arItem["PROPERTIES"]["PHOTOS"]["VALUE"] as $value) :?>
                                <a class="big-img" data-fancybox="gallery" href="<?=$value?>">
                                    <img src="<?=$value?>">
                                </a>
                            <?php endforeach;?>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    <?php endforeach;?>

<?php endif;?>
