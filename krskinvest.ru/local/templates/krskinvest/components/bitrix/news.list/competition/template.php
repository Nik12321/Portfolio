<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if (count($arResult["ITEMS"])) :?>
    <?php foreach($arResult["ITEMS"] as $key => $arItem):?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $value = $arItem["PROPERTIES"]["FILE"]["VALUE"];
        ?>
        <a download="<?=$value["FILE_NAME"]?>" href="<?=$value["FILE_PATH"]?>" class="file-btn" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="file-btn__icon">
                <svg><use xlink:href="#<?=$value["EXPANSION"]?>"></svg>
            </div>
            <div class="file-btn__info">
                <div class="file-btn__info--name">
                    <?=$value["FILE_NAME"]?>
                </div>
                <div class="file-btn__info--meta">
                    <?=strtoupper($value["EXPANSION"])?>, <?=$value["SIZE"]?>
                </div>
            </div>
        </a>
    <?php endforeach;?>
<?php endif;?>
