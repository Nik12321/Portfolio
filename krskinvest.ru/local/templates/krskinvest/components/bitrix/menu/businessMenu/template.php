<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="business-support__links">
    <?php
    $iBlockName = "";
    $arIBlockDescriptions = array();
    $arKeys = array();
    $sections = CIBlock::GetList(array("SORT" => "ASC"), array());
    $counter = 0;
    while($arSections = $sections -> GetNext()) {  //перебираем массив инфоблоков
        $iBlockName = $arSections['NAME'];
        $arIBlockDescriptions[$iBlockName] = $arSections['DESCRIPTION']; //формируем массив вида [Название инфоблока]->[Описание инфоблока]
        $arKeys[$iBlockName] = array_keys($arIBlockDescriptions)[$counter]; //записываем наименование ключей в массив
        $counter++;
    }
    ?>
    <?php foreach ($arResult as $arItem): ?>
        <a class="link" href="<?= $arItem['LINK'] ?>">
            <div class="link--title">
                <?= $arItem['TEXT'] ?>
            </div>
            <div class="link--text">
                <h5>Подробнее</h5>
                <p>
                    <?php
                    foreach ($arKeys as $key)
                    {
                        //если в названии инфоблока есть подстрока *название пункта меню* (задаем в .submenu-headerMenu.menu.php)
                        if(false !== strpos($key, $arItem['TEXT'])){
                            echo $arIBlockDescriptions[$key]; //выводим описание инфоблока
                        }
                    }
                    ?>
                </p>
            </div>
            <div class="link--icon">
                <?= $arItem['PARAMS']['SVG'] ?>
            </div>
        </a>
    <?php endforeach ?>
</div>