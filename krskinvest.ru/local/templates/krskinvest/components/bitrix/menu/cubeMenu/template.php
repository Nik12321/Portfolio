<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
?>
<?php if(count($arResult)):?>
    <div class="btn-style-1--wrap">
    <?php foreach($arResult as $arItem) :?>
        <a class="btn-style-1 <?=($arItem["SELECTED"]) ? "active" : ""?>" href="<?=$arItem["LINK"]?>">
            <?=$arItem["TEXT"]?>
        </a>
    <?php endforeach;?>
    </div>
<?php endif;?>