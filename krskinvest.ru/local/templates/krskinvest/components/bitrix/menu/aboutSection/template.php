<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
?>
<?php if(count($arResult)):?>
    <div class="about__btns">
        <?php foreach($arResult as $arItem) :?>
            <a class="btn" href="<?=$arItem["LINK"]?>">
                <div class="btn--text">
                    <?=$arItem["TEXT"]?>
                </div>
                <div class="btn--icon">
                    <svg>
                        <use xlink:href="#<?=$arItem["PARAMS"]["SVG"]?>" />
                    </svg>
                </div>
            </a>
        <?php endforeach;?>
    </div>
<?php endif;?>