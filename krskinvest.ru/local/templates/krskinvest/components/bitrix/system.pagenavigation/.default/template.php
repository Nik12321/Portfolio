<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
$nPageWindow = 5; //количество отображаемых страниц
if ($arResult["NavPageNomer"] > floor($nPageWindow/2) + 1 && $arResult["NavPageCount"] > $nPageWindow)  {
    $nStartPage = $arResult["NavPageNomer"] - floor($nPageWindow/2);
} else {
    $nStartPage = 1;
}

if ($arResult["NavPageNomer"] <= $arResult["NavPageCount"] - floor($nPageWindow/2) && $nStartPage + $nPageWindow-1 <= $arResult["NavPageCount"]) {
    $nEndPage = $nStartPage + $nPageWindow - 1;
} else {
    $nEndPage = $arResult["NavPageCount"];
    if($nEndPage - $nPageWindow + 1 >= 1) {
        $nStartPage = $nEndPage - $nPageWindow + 1;
    }
}
$arResult["nStartPage"] = $nStartPage;
$arResult["nEndPage"]  = $nEndPage;
$leftArrowLink = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".((($arResult["NavPageNomer"] - 1) == 0) ? $arResult["nEndPage"] : $arResult["NavPageNomer"] - 1);
$rightArrowLink = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"] + 1);
?>
<div class="pagination">
    <a href="<?=$leftArrowLink?>">
        <svg>
            <use xlink:href="#pagination-arrow-prev" />
        </svg>
    </a>
    <?php if ($arResult["nStartPage"] > 1) :?>
        <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a> ...
    <?php endif;?>
    <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
        <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <a href="#" class="active"><?=$arResult["nStartPage"]?></a>
        <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
            <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
        <?else:?>
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
        <?endif?>
        <?$arResult["nStartPage"]++?>
    <?endwhile?>
    <?php if ($arResult["NavPageCount"] != $arResult["nEndPage"]) :?>
        ...<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a>
    <?php endif;?>
    <a href="<?=$rightArrowLink?>">
        <svg>
            <use xlink:href="#pagination-arrow-next" />
        </svg>
    </a>
</div>