<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
$nPageWindow = 10; //количество отображаемых страниц
if ($arResult["NavPageNomer"] > floor($nPageWindow/2) + 1 && $arResult["NavPageCount"] > $nPageWindow)  {
    $nStartPage = $arResult["NavPageNomer"] - floor($nPageWindow/2);
} else {
    $nStartPage = 1;
}

if ($arResult["NavPageNomer"] <= $arResult["NavPageCount"] - floor($nPageWindow/2) && $nStartPage + $nPageWindow-1 <= $arResult["NavPageCount"]) {
    $nEndPage = $nStartPage + $nPageWindow - 1;
} else {
    $nEndPage = $arResult["NavPageCount"];
    if($nEndPage - $nPageWindow + 1 >= 1) {
        $nStartPage = $nEndPage - $nPageWindow + 1;
    }
}
$arResult["nStartPage"] = $nStartPage;
$arResult["nEndPage"]  = $nEndPage;
$leftArrowLink = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".((($arResult["NavPageNomer"] - 1) == 0) ? $arResult["nEndPage"] : $arResult["NavPageNomer"] - 1);
$rightArrowLink = $arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"] + 1);
?>
<div class="pagination--red">
    <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
        <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <a href="#" class="active"><?=$arResult["nStartPage"]?></a>
        <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
            <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
        <?else:?>
            <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
        <?endif?>
        <?$arResult["nStartPage"]++?>
    <?endwhile?>
</div>