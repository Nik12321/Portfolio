<?php

$newResult = [];
foreach ($arResult["SEARCH"] as $arItem) {
    preg_match_all("/<a\s.*?href=\"(.+?)\".*?>(.+?)<\/a>/", $arItem["CHAIN_PATH"], $matches);
    if ($matches[0][1]) {
        $trueSectionName = $matches[0][1];
        $trueSectionName = preg_replace(['/<a\s.*?href="(.+?)".*?>/', '/<\/a>/'], "", $trueSectionName);
        if ($trueSectionName) {
            ($newResult[$trueSectionName])
                ? $newResult[$trueSectionName][] = $arItem
                : $newResult[$trueSectionName] = [$arItem];
        }
    }
}
$arResult["SEARCH"] = $newResult;
