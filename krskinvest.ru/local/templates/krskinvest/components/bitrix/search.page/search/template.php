<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

?>
<div class="search-page">
    <form action="" method="get">
        <div class="search-page__input">
            <form action="#">
                <button class="search-page__input--btn">
                    <svg>
                        <use xlink:href="#search">
                    </svg>
                </button>
                <input type="text" placeholder="Поиск..." name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" size="40" />
            </form>
        </div>
        <input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
    </form>
    <?php if($arResult["ERROR_CODE"]!=0):?>
        <p><?=GetMessage("SEARCH_ERROR")?></p>
        <?php ShowError($arResult["ERROR_TEXT"]);?>
    <?php elseif(count($arResult["SEARCH"])>0):?>
        <?php foreach($arResult["SEARCH"] as $key => $sections):?>
            <div class="search-page__list">
                <h3><?=$key?></h3>
                <ul>
                    <?php foreach ($sections as $arItem) :?>
                        <li>
                            <a target="_blank" href="<?echo $arItem["URL"]?>">
                                <h4><?echo $arItem["TITLE_FORMATED"]?></h4>
                                <span><?=$arItem["CHAIN_PATH"]?></span>
                            </a>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        <?endforeach;?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
        <br />
    <?php else:?>
        <?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
    <?php endif;?>
</div>