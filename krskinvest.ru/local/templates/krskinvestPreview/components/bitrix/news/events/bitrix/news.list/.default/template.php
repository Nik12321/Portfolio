<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<script src="<?=DEFAULT_TEMPLATE_PATH?>/js/calendar/calendar.min.js" type="text/javascript"></script>
<div class="ivents-main--container">
    <!-- Календарь -->
    <div class="col">
        <h4>Календарь мероприятий</h4>
        <div class="calendar">
            <div id="ippk-calendar"></div>
        </div>
    </div>
    <!-- Календарь -->
    <!-- Список Мероприятий -->
    <div class="col">
        <h4>Ближайшие мероприятия</h4>
        <div class="news--list">
            <?php foreach($arResult["ITEMS"] as $arItem) :?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="news__item"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                        <div class="adaptive">
                            <div class="news__item--img" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"></div>
                            <div class="news__item--date adaptive-date"><?=$arItem["DATE"]?></div>
                        </div>
                        <div class="news__item--column">
                            <div class="news__item--date"><?=$arItem["DATE"]?></div>
                            <div class="news__item--title">
                                <?=$arItem["NAME"]?>
                            </div>
                            <div class="news__item--text">
                                <?=strip_tags(htmlspecialcharsBack($arItem["PROPERTIES"]["TITLE"]["VALUE"]["TEXT"], '<br>'))?>
                            </div>
                        </div>
                    </a>
                </div>

            <?php endforeach;?>
        </div>
    </div>
    <!-- Список Мероприятий -->
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var events = [
            <?php foreach($arResult["EVENTS"] as $event) :?>
            {
                date: '<?=$event["DATE"]?>',
                title: '<?=$event["NAME"]?>',
                url: '<?=$event["DETAIL_PAGE_URL"]?>',
                <?php if ($event["TYPE"]) :?>
                color: '<?=$event["TYPE"]["COLOR"]["UF_COLOR_CODE"]?>',
                <?php else :?>
                color: '#B5001F',
                <?php endif;?>
                <?php if ($event["SRC"]) :?>
                logo: '<?=$event["SRC"]?>',
                <?php endif;?>
                start: '<?=$event["DATE_BEFORE"]?>',
                <?php if ($event["DATE_AFTER"]) :?>
                end: '<?=$event["DATE_AFTER"]?>'
                <?php endif;?>
            },
            <?php endforeach;?>
        ];
        var legends = [
            <?php foreach($arResult["COLORS"] as $colorData) :?>
            {
                color: '<?=$colorData["COLOR"]["UF_COLOR_CODE"]?>',
                title: '<?=$colorData["NAME"]?>'
            },
            <?php endforeach;?>
        ];
        const calendar = new Calendar('', {
            // пример данных мероприятий
            events: events,
            // пример данных для генерации легенды под календарем
            legends: legends
        });

        calendar.elCalendar.addEventListener('on.change-date', (e) => {
            const calendarEvents = calendar.instanceEvents;
            const date = e.detail.data.date;
            var formData = new FormData();
            formData.append("date",  date);
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    let array = JSON.parse(xmlHttp.responseText);
                    calendarEvents.events = array;
                    calendarEvents.renderEvents();
                }
            };
            xmlHttp.open("post", "/ajax/getEvents.php");
            xmlHttp.send(formData);
        });
    });
</script>