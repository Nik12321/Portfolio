<?php
$mouths = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря'
];

foreach($arResult["ITEMS"] as $key=>$arItem) {
    if ($arItem["PREVIEW_PICTURE"]) {
        $resizedPreview = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            array("width"=> 200, "height" => 200),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true,
            false
        );
        if ($resizedPreview) {
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["SRC"] = $resizedPreview["src"];
        }
    } else {
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["SRC"] = DEFAULT_TEMPLATE_PATH . "/img/plug-img.svg";
    }
    if ($arItem["PROPERTIES"]["DATE_BEFORE"]["VALUE"]) {
        $date = $firstPart = $secondPart = "";
        $yearNumberOne = date("Y", strtotime($arItem["PROPERTIES"]["DATE_BEFORE"]["VALUE"]));
        $monthNumberOne = date('n', strtotime($arItem["PROPERTIES"]["DATE_BEFORE"]["VALUE"]))-1;
        $dayNumberOne = date("j", strtotime($arItem["PROPERTIES"]["DATE_BEFORE"]["VALUE"]));
        $mouthNameOne = $mouths[$monthNumberOne];
        if ($arItem["PROPERTIES"]["DATE_AFTER"]["VALUE"]) {
            $yearNumberTwo = date("Y", strtotime($arItem["PROPERTIES"]["DATE_AFTER"]["VALUE"]));
            $monthNumberTwo = date('n', strtotime($arItem["PROPERTIES"]["DATE_AFTER"]["VALUE"]))-1;
            $dayNumberTwo = date("j", strtotime($arItem["PROPERTIES"]["DATE_AFTER"]["VALUE"]));
            $mouthNameTwo = $mouths[$monthNumberTwo];
            if ($yearNumberOne == $yearNumberTwo) {
                if ($monthNumberOne == $monthNumberTwo) {
                    $date = $dayNumberOne . "-" . $dayNumberTwo . " " . $mouthNameOne;
                } else {
                    $date = $dayNumberOne . " " . $mouthNameOne . " - " . $dayNumberTwo . " " . $mouthNameTwo;
                }
            } else {
                $date =  $dayNumberOne . " " . $mouthNameOne . " " . $yearNumberOne . " " . "года";
                $date .=  " - " . $dayNumberTwo . " " . $mouthNameTwo . " " . $yearNumberTwo . " " . "года";
            }
        } else {
            $date =  $dayNumberOne . " " . $mouthNameOne;
        }
        $arResult["ITEMS"][$key]["DATE"] = $date;
    }
}

//Извлекаем мероприятия за последний месяц и данные для календаря
$events = [];
$elements = CIBlockElement::GetList(
    ["DATE_ACTIVE_FROM" => "DESC", "SORT" => "ASC"],
    [
        "IBLOCK_ID" => 26,
        "ACTIVE" =>"Y",
        ">=PROPERTY_DATE_BEFORE" => date("Y-m-01"),
    ],
    false,
    false,
    ["NAME", "ID", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "PROPERTY_DATE_BEFORE", "PROPERTY_DATE_AFTER", "PROPERTY_TYPE"]
);
while ($element = $elements->GetNext()) {
    if ($element["PROPERTY_DATE_BEFORE_VALUE"]) {
        $dateBefore = DateTime::createFromFormat('d.m.Y', $element["PROPERTY_DATE_BEFORE_VALUE"]);
        $start = $dateBefore->format('Y-m-d');
        $element["DATE_BEFORE"] = $start;
    }
    if ($element["PROPERTY_DATE_AFTER_VALUE"]) {
        $dateBefore = DateTime::createFromFormat('d.m.Y', $element["PROPERTY_DATE_AFTER_VALUE"]);
        $start = $dateBefore->format('Y-m-d');
        $element["DATE_AFTER"] = $start;
    }
    if ($element["DATE_BEFORE"]) {
        $date = $firstPart = $secondPart = "";
        $yearNumberOne = date("Y", strtotime($element["DATE_BEFORE"]));
        $monthNumberOne = date('n', strtotime($element["DATE_BEFORE"]))-1;
        $dayNumberOne = date("j", strtotime($element["DATE_BEFORE"]));
        $mouthNameOne = $mouths[$monthNumberOne];
        if ($element["DATE_AFTER"]) {
            $yearNumberTwo = date("Y", strtotime($element["DATE_AFTER"]));
            $monthNumberTwo = date('n', strtotime($element["DATE_AFTER"]))-1;
            $dayNumberTwo = date("j", strtotime($element["DATE_AFTER"]));
            $mouthNameTwo = $mouths[$monthNumberTwo];
            if ($yearNumberOne == $yearNumberTwo) {
                if ($monthNumberOne == $monthNumberTwo) {
                    $date = $dayNumberOne . "-" . $dayNumberTwo . " " . $mouthNameOne;
                } else {
                    $date = $dayNumberOne . " " . $mouthNameOne . " - " . $dayNumberTwo . " " . $mouthNameTwo;
                }
            } else {
                $date =  $dayNumberOne . " " . $mouthNameOne . " " . $yearNumberOne . " " . "года";
                $date .=  " - " . $dayNumberTwo . " " . $mouthNameTwo . " " . $yearNumberTwo . " " . "года";
            }
        } else {
            $date =  $dayNumberOne . " " . $mouthNameOne;
        }
        $element["DATE"] = $date;
    }
    $events[] = $element;
}

$colors = [];
$elements = CIBlockElement::GetList(
    false,
    ["IBLOCK_ID" => 26, "ACTIVE" =>"Y"],
    ["PROPERTY_TYPE"],
    false,
    ["PROPERTY_TYPE"]
);
while ($element = $elements->GetNext()) {
    if ($element["PROPERTY_TYPE_VALUE"]) {
        $colors[] = $element["PROPERTY_TYPE_VALUE"];
    }
}
$colors = array_unique($colors);
$colorsData = getColorsData($colors);
foreach ($events as &$event) {
    if ($event["PROPERTY_TYPE_VALUE"]) {
        $event["TYPE"] = $colorsData[$event["PROPERTY_TYPE_VALUE"]];
    }

    if ($event["PREVIEW_PICTURE"]) {
        $previewPicture = CFile::GetFileArray($event["PREVIEW_PICTURE"]);
        $previewPicture = CFile::ResizeImageGet($previewPicture, ["width"=> 40, "height" => 40], BX_RESIZE_IMAGE_PROPORTIONAL, true, false);
        $event["SRC"] = $previewPicture["src"];
    }
}
unset($event);
$arResult["EVENTS"] = $events;
$arResult["COLORS"] = $colorsData;