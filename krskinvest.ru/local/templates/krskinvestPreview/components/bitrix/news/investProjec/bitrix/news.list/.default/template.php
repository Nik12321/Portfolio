<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="invest-swiper">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <?php foreach($arResult["ITEMS"] as $arItem) :?>
            <?php
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <!-- Slides -->
            <div class="swiper-slide slick__slider--item"
                 style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);">
                <div class="slick__slider--container">
                    <div class="slider--item__title">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <?=$arItem["NAME"]?>
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <div class="swiper-pagination"></div>
</div>
