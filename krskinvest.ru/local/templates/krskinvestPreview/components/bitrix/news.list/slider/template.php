<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
?>

<div class="swiper-wrapper">
    <?php foreach($arResult["ITEMS"] as $arItem) :?>
        <?php
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div
                class="swiper-slide"
                style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"
                id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <?php if ($arItem["PROPERTIES"]["SLIDE_H1"]["VALUE"]) :?>
                <video src="<?=$arItem["FILE_PATH"]?>" style="width: 100%; height: 100%; object-fit: cover;"
                       autoplay muted loop></video>
            <?php else :?>
                <div class="slider--item__container">
                    <div class="slider--item__title">
                        <h1><?=$arItem["PROPERTIES"]["SLIDE_H1"]["VALUE"]?></h1>
                    </div>
                    <div class="slider--item__text">
                        <?=$arItem["PREVIEW_TEXT"]?>
                    </div>
                </div>
            <?php endif;?>
        </div>
    <?php endforeach;?>
</div>
<div class="swiper-pagination"></div>