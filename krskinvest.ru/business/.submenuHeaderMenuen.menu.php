<?php
$aMenuLinks = [
    [
        "Business support measures",
        "/business/business-support/",
        [],
        ["SVG" => "handshake"],
        ""
    ],
    [
        "Investment sites",
        "/business/platform/",
        [],
        ["SVG" => "cirqle-1"],
        ""
    ],
    [
        "Technological connection to electrical networks",
        "/business/electrical-networks/",
        [],
        ["SVG" => "electric"],
        ""
    ],
    [
        "Arctic",
        "/business/arctic/",
        [],
        ["CLASS" => "arctic"],
        ""
    ],
    [
        "Staffing",
        "/business/cadr-sup/",
        [],
        ["SVG" => "people-group"],
        ""
    ],
    [
        "Feedback line",
        "/business/feedback/",
        [],
        ["SVG" => "chat"],
        ""
    ],
    [
        "Support infrastructure",
        "/business/infrastructure-sup/",
        [],
        ["SVG" => "people-web"],
        ""
    ],
    [
        "Public private partnership",
        "/business/gos-part/",
        [],
        ["CLASS" => "gchp-nav"],
        ""
    ]
];
?>