<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Финансовая поддержка");
?>
    <section class="business-sup">
        <div class="finance-sup container">
            <h2>Финансовая поддержка</h2>
            <?php
            $iblockId = getIblockIdByCode("financeSupport");
            $entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock($iblockId);
            $sections_1lvl = $entity::getList([
                "order" => ["SORT" => "ASC"],
                "select" => ["NAME", "ID", "CODE"],
                "filter" => ["IBLOCK_ID" => $iblockId, "ACTIVE" => "Y"]
            ])->fetchAll();
            ?>
            <div class="btn-style-2--wrap">
                <?php foreach ($sections_1lvl as $key => $section) : ?>
                    <a class="btn-style-2" href="#<?=$section['CODE']?>">
                        <span>
                        <?=$section["NAME"] ?>
                        </span>
                    </a>
                <?php endforeach ?>
            </div>
            <?php foreach($sections_1lvl as $key => $section):?>
                <div id="<?=$section['CODE']?>" class="anchors-link-to">
                    <h3><?=$section["NAME"]?></h3>
                    <div class="finance-sup__column">
                        <?php
                        $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"openTab", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => getIblockIdByCode("financeSupport"),
		"IBLOCK_TYPE" => "-",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "150",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => $section["ID"],
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "NAME",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "openTab"
	),
	false
);
                        ?>
                    </div>
                </div>
            <?php endforeach?>
        </div>
    </section>

    <!-- Для переключения вкладок -->
    <script>
        function openTab(evt, financeTab) {
            // Declare all variables
            var i, tabcontent, tablinks;
            tabsGroup = document.getElementById(tabsGroupId)
            // Get all elements with class="tabcontent" and hide them
            tabcontent = tabsGroup.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                if (document.getElementById(financeTab).style.display == "none") {
                    tabcontent[i].style.display = "none";
                }
            }
            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = tabsGroup.getElementsByClassName("tab");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            // Show the current tab, and add an "active" class to the button that opened the tab
            if (document.getElementById(financeTab).style.display == "none") {
                document.getElementById(financeTab).style.display = "block";
                evt.currentTarget.className += " active";
            } else {
                document.getElementById(financeTab).style.display = "none";
                evt.currentTarget.className - "active";
            }
        }
    </script>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>