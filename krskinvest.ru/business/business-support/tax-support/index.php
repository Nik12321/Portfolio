<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Налоговая поддержка");
?>

    <section class="business-sup">
        <div class="container">
            <h2>Налоговая поддержка</h2>
            <?php
            $iblockId = getIblockIdByCode("taxSupport");
            $entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock($iblockId);
            $sections_1lvl = $entity::getList([
                "order" => ["SORT" => "ASC"],
                "select" => ["NAME", "ID", "UF_HEADLINE_LVL1", "UF_HEADLINE_LVL2", "CODE"],
                "filter" => ["IBLOCK_ID" => $iblockId, "ACTIVE" => "Y", "DEPTH_LEVEL" => 1]
            ])->fetchAll();

            $sections_2lvl_arrays = [];
            $sections_2lvl_result = $entity::getList([
                "order" => ["IBLOCK_SECTION_ID" => "ASC", "SORT" => "ASC"],
                "select" => ["NAME", "ID", "IBLOCK_SECTION_ID", "DESCRIPTION"],
                "filter" => ["IBLOCK_ID" => $iblockId, "ACTIVE" => "Y"]
            ])->fetchAll();
            foreach ($sections_2lvl_result as $array) {
                if ($sections_2lvl_arrays[$array["IBLOCK_SECTION_ID"]]) {
                    $sections_2lvl_arrays[$array["IBLOCK_SECTION_ID"]][] = $array;
                } else {
                    $sections_2lvl_arrays[$array["IBLOCK_SECTION_ID"]] = [$array];
                }
            }
            ?>
            <div class="btn-style-2--wrap">
                <?php foreach ($sections_1lvl as $key => $section) : ?>
                    <a class="btn-style-2" href="#<?= $section['CODE'] ?>">
                        <span>
                        <?=$section["NAME"] ?>
                        </span>
                    </a>
                <?php endforeach ?>
            </div>
            <?php foreach ($sections_1lvl as $key => $section) : ?>
                <div id="<?=$section['CODE'] ?>" class="tax-sup__property anchors-link-to">
                    <h3><?=$section["NAME"]; ?></h3>
                    <div class="tax-sup__law">
                        <div class="tax-sup__law--main">
                            <?= $section["UF_HEADLINE_LVL1"]; ?>
                        </div>
                        <div class="tax-sup__law--submain">
                            <?= $section["UF_HEADLINE_LVL2"]; ?>
                        </div>
                    </div>
                    <?php
                    $sections_2lvl = $sections_2lvl_arrays[$section["ID"]];
                    foreach ($sections_2lvl as $key3 => $section3) {
                        $sectionArr[$key3] = $section3["ID"];
                        $sectionCount = count($sectionArr);
                    }
                    $styleClass = "four-tab";
                    switch ($sectionCount) { //определяем класс стилей для div`а с кнопками
                        case 1:
                            $styleClass = "one-tab";
                            break;
                        case 3:
                            $styleClass = "three-tab";
                            break;
                        case 4:
                            $styleClass = "four-tab";
                            break;
                    }
                    ?>
                    <div class="tax-sup__tabs <?= $styleClass ?>">
                        <h5>Льготные ставки</h5>
                        <div class="tab--row">
                            <?php foreach ($sections_2lvl as $key1 => $section1) :?>
                                <button class="tab" onclick="openTab(event, '<?= $section['CODE'], $key1 ?>', '<?= $section['CODE'] ?>')">
                                    <?= $section1["NAME"] ?>
                                </button>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <?php
                    unset($sectionArr);
                    $sections_2lvl = $sections_2lvl_arrays[$section["ID"]];
                    ?>
                    <?php foreach ($sections_2lvl as $key2 => $section2) :?>
                        <div id="<?= $section['CODE'] ?><?= $key2 ?>" class="tabcontent <?= $section['CODE'] ?>" style="display: none;">
                            <?php if($section2["DESCRIPTION"] != "") :?>
                                <div class="blue-tab--inner">
                                    <?=$section2["DESCRIPTION"]?>
                                </div>
                            <?php endif ?>
                            <?php
                            $APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "openTab",
                            array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "DISPLAY_DATE" => "Y",
                                "DISPLAY_NAME" => "Y",
                                "DISPLAY_PICTURE" => "Y",
                                "DISPLAY_PREVIEW_TEXT" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array(
                                    0 => "",
                                    1 => "",
                                ),
                                "FILTER_NAME" => "",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => getIblockIdByCode("taxSupport"),
                                "IBLOCK_TYPE" => "-",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "20",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => $section2["ID"],
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array(
                                    0 => "",
                                    1 => "NAME",
                                    2 => "",
                                ),
                                "SET_BROWSER_TITLE" => "Y",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "SORT",
                                "SORT_BY2" => "ACTIVE_FROM",
                                "SORT_ORDER1" => "ASC",
                                "SORT_ORDER2" => "ASC",
                                "STRICT_SECTION_CHECK" => "N",
                                "COMPONENT_TEMPLATE" => "openTab"
                            ),
                            false
                        );
                            ?>
                        </div>
                    <?php endforeach;?>
                </div>
            <?php endforeach ?>
        </div>
    </section>

    <!-- Для переключения вкладок -->
    <script>
        function openTab(evt, taxTab, tabsGroupId) {
            // Declare all variables
            var i, tabcontent, tablinks;
            tabsGroup = document.getElementById(tabsGroupId)

            // Get all elements with class="tabcontent" and hide them
            tabcontent = tabsGroup.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                if (document.getElementById(taxTab).style.display == "none") {
                    tabcontent[i].style.display = "none";
                }
            }
            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = tabsGroup.getElementsByClassName("tab");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            // Show the current tab, and add an "active" class to the button that opened the tab
            if (document.getElementById(taxTab).style.display == "none") {

                document.getElementById(taxTab).style.display = "block";
                evt.currentTarget.className += " active";
            } else {
                document.getElementById(taxTab).style.display = "none";
                evt.currentTarget.className - "active";
            }
        }
        const anchors = document.querySelectorAll('a[href*="#"]')
        for (let anchor of anchors) {
            anchor.addEventListener('click', function (e) {
                e.preventDefault()
                const blockID = anchor.getAttribute('href').substr(1)
                document.getElementById(blockID).scrollIntoView({
                    behavior: 'smooth',
                    block: 'start'
                })
            })
        }
    </script>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>