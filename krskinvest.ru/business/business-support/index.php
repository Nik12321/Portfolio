<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Меры поддержки бизнеса");
CModule::IncludeModule('highloadblock');
?>

<section class="business-support">
    <div class="container">
        <h2>Меры поддержки бизнеса</h2>
        <div class="business-support--wrapper">
            <div class="business-support--col">
                <div class="business-support__links">
                    <?php
                    $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "businessMenu",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "submenuHeaderMenu" . LANGUAGE_ID,
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "businessMenu"
                        ),
                        false
                    );
                    ?>
                </div>
            </div>
            <div class="business-support--col">
                <form action="#" id="search-support">
                    <div class="navigator-support__main--item">
                        <?php
                        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_kategoriipoluchatelya']])->fetch();
                        if (isset($hlblock['ID'])) {
                            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                            $entity_data_class = $entity->getDataClass();
                            $elements = $entity_data_class::getList(['order' => ["UF_SORT" => "ASC"], 'select' => ['UF_XML_ID', 'UF_NAME']])->fetchAll();
                            ?>
                            <?php if (count($elements)) :?>
                                <h3>Категория получателя</h3>
                                <div id="category" class="btn-block">
                                    <?php foreach ($elements as $key => $element) :?>
                                        <input type="radio" class="radio-btn" id="<?=$element["UF_XML_ID"]?>" <?=(!$key) ? "hidden checked" : "hidden"?> name="category" value="<?=$element["UF_XML_ID"]?>">
                                        <label for="<?=$element["UF_XML_ID"]?>">
                                            <span class="item-btn"><?=$element["UF_NAME"]?></span>
                                        </label>
                                    <?php endforeach;?>
                                </div>
                            <?php endif;?>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="navigator-support__main--item">
                        <h3>Отрасль</h3>
                        <div class="btn-block">
                            <button class="item-btn-choose" id="OpenModal">Выбрать</button>
                        </div>
                        <div class="main-btn__container"></div>
                    </div>

                    <div class="navigator-support__main--item">
                        <?php
                        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_razmerbiznesa']])->fetch();
                        if (isset($hlblock['ID'])) {
                            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                            $entity_data_class = $entity->getDataClass();
                            $elements = $entity_data_class::getList(['order' => ["UF_SORT" => "ASC"], 'select' => ['UF_XML_ID', 'UF_NAME']])->fetchAll();
                            ?>
                            <?php if (count($elements)) :?>
                                <h3>Размер бизнеса</h3>
                                <div id="business" class="btn-block">
                                    <?php foreach ($elements as $key => $element) :?>
                                        <input type="radio" class="radio-btn" id="<?=$element["UF_XML_ID"]?>" <?=(!$key) ? "hidden checked" : "hidden"?> name="size" value="<?=$element["UF_XML_ID"]?>">
                                        <label for="<?=$element["UF_XML_ID"]?>">
                                            <span class="item-btn"><?=$element["UF_NAME"]?></span>
                                        </label>
                                    <?php endforeach;?>
                                </div>
                            <?php endif;?>
                            <?
                        }
                        ?>
                    </div>
                    <div style="max-height: 300px;" class="navigator-support__main--item">
                        <div style="height: 100%;" class="btn-block">
                            <input type="submit" class="item-btn-last" value="Перейти к результатам">
                        </div>
                    </div>
                    <div class="business-support--modal modal" id="modalWindow">
                        <div class="modal--overlay">
                            <div class="modal--content">
                                <div class="modal__header">
                                    Выбрать отрасль
                                    <button class="modal__header--close" id="modalClose">
                                        <svg id="close-btn" viewBox="0 0 24 24">
                                            <path d="M20,0H4C1.794,0,0,1.794,0,4v16c0,2.206,1.794,4,4,4h16c2.206,0,4-1.794,4-4V4C24,1.794,22.206,0,20,0z M22,20 c0,1.104-0.896,2-2,2H4c-1.103,0-2-0.896-2-2V4c0-1.103,0.897-2,2-2h16c1.104,0,2,0.897,2,2V20z" />
                                            <path d="M17.423,6.779H13.9l-0.918,1.687c-0.278,0.513-0.535,1.046-0.812,1.602h-0.042c-0.277-0.491-0.555-1.025-0.854-1.56 l-1.025-1.729h-3.63l3.459,5.125L6.534,17.22H10.1l0.981-1.879c0.256-0.512,0.535-1.024,0.79-1.58h0.064 c0.256,0.534,0.512,1.067,0.811,1.58l1.046,1.879h3.673l-3.48-5.466L17.423,6.779z" />
                                        </svg>
                                    </button>
                                </div>
                                <div class="modal__body">
                                    <div id="area_prop" class="business-support__branch-list">
                                        <?php
                                        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_otrasl']])->fetch();
                                        if (isset($hlblock['ID'])) {
                                            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                                            $entity_data_class = $entity->getDataClass();
                                            $elements = $entity_data_class::getList(['order' => ["UF_SORT" => "ASC"], 'select' => ['UF_XML_ID', 'UF_NAME']])->fetchAll();
                                            ?>
                                            <?php foreach ($elements as $key => $element) :?>
                                                <div class="check-item">
                                                    <input type="checkbox" id="<?=$element["UF_XML_ID"]?>" value="<?=$element["UF_XML_ID"]?>">
                                                    <label for="<?=$element["UF_XML_ID"]?>">
                                                        <?=$element["UF_NAME"]?>
                                                    </label>
                                                </div>
                                            <?php endforeach;?>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var submitBtn = document.getElementById('search-support');
        submitBtn.addEventListener('submit', (e) => {
            e.preventDefault();
            let category = document.getElementById("category").querySelector("input:checked").getAttribute("value");
            let business = document.getElementById("business").querySelector("input:checked").getAttribute("value");
            let area = document.getElementById("area_prop").querySelectorAll("input:checked");
            let areaValues = [];
            area.forEach(element => {
                areaValues.push(element.getAttribute("value"));
            });
            let query = "?category=" + category + "&business=" + business;
            if (areaValues.length > 0) {
                query += "&area=";
                for (let i in areaValues) {
                    if (i == 0) {
                        query += areaValues[i];
                    } else {
                        query += "-" + areaValues[i];
                    }
                }
            }
            window.location.href = "/business/business-support/search/" + query;
        });
    }); // end ready
</script>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
