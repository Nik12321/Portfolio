<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Навигатор по мерам поддержки");

CModule::IncludeModule('highloadblock');

$propertyIblockId = getIblockIdByCode("propertySupport");
$financeIblockId = getIblockIdByCode("financeSupport");
$taxIblockId = getIblockIdByCode("taxSupport");
$anotherIblockId = getIblockIdByCode("anotherSupportMeasure");

$categotyHLBlockId = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_kategoriipoluchatelya']])->fetch();
$businessHLBlockId = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_razmerbiznesa']])->fetch();
$areaHLBlockId = \Bitrix\Highloadblock\HighloadBlockTable::getList(["filter" => ['TABLE_NAME' => 'b_hlbd_otrasl']])->fetch();

$category = $business = $area = "";

if ($_GET["category"]) {
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($categotyHLBlockId)->getDataClass();
    $element = $entity::getList(['filter' => ["UF_XML_ID" => $_GET["category"]], 'select' => ['UF_XML_ID', 'UF_NAME']])->fetch();
    if ($element["UF_NAME"]) {
        $filter["PROPERTY_CATEGORY"] = [$element["UF_XML_ID"], false];
        $category = $element["UF_NAME"];
    }
}

if ($_GET["business"]) {
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($businessHLBlockId)->getDataClass();
    $element = $entity::getList(['filter' => ["UF_XML_ID" => $_GET["business"]], 'select' => ['UF_XML_ID', 'UF_NAME']])->fetch();
    if ($element["UF_NAME"]) {
        $filter["PROPERTY_BUSINESS_SIZE"] = [$element["UF_XML_ID"], false];
        $business = $element["UF_NAME"];
    }
}

if ($_GET["area"]) {
    $areas = explode("-", $_GET["area"]);
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($areaHLBlockId)->getDataClass();
    $elements = $entity::getList(['filter' => ["UF_XML_ID" => $areas], 'select' => ['UF_XML_ID', 'UF_NAME']]);
    while ($element = $elements->fetch()){
        if ($element["UF_NAME"]) {
            ($filter["PROPERTY_BUSINESS_SIZE"])
                ? $filter["PROPERTY_AREA"][] = $element["UF_XML_ID"]
                : $filter["PROPERTY_AREA"] = [$element["UF_XML_ID"]];
            ($area)
                ? $area[] = $element["UF_NAME"]
                : $area = [$element["UF_NAME"]];
        }
    }
    ($filter["PROPERTY_BUSINESS_SIZE"])
        ? $filter["PROPERTY_AREA"][] = false
        : $filter["PROPERTY_AREA"] = [false];
}

$elementsByGroup = [];
$allElements = [];
$elements = CIBlockElement::GetList(false, $filter, false, false, ["ID", "IBLOCK_ID", "IBLOCK_SECTION_ID"]);
while ($element = $elements->Fetch()) {
    ($elementsByGroup[$element["IBLOCK_ID"]])
        ? $elementsByGroup[$element["IBLOCK_ID"]]["ID"][] = $element["ID"]
        : $elementsByGroup[$element["IBLOCK_ID"]]["ID"] = [$element["ID"]];
    ($allElements[$element["IBLOCK_ID"]])
        ? $allElements[$element["IBLOCK_ID"]][] = $element
        : $allElements[$element["IBLOCK_ID"]] = [$element];
}
?>
<!-- секция основной информации страницы -->
<section class="business-search">
    <div class="container">
        <h2>Навигатор по мерам поддержки</h2>
        <div class="business-search__result">
            <?php if ($category) :?>
                <div class="business-search__result--block">
                    <?=$category?>
                </div>
            <?php endif;?>
            <?php if ($business) :?>
                <div class="business-search__result--block">
                    <?=$business?>
                </div>
            <?php endif;?>
            <?php foreach ($area as $areaValue) :?>
                <div class="business-search__result--block">
                    <?=$areaValue?>
                </div>
            <?php endforeach;?>
        </div>

        <?php if (count($elementsByGroup[$financeIblockId])) :?>
            <div class="business-search__list">
                <h3>Финансовая поддержка</h3>
                <?php
                $sectionIds = [];
                foreach ($allElements[$financeIblockId] as $element) {
                    $sectionIds[] = $element["IBLOCK_SECTION_ID"];
                }
                $entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock($financeIblockId);
                $sections = $entity::getList([
                    "order" => ["IBLOCK_SECTION_ID" => "ASC", "SORT" => "ASC"],
                    "select" => ["NAME", "ID"],
                    "filter" => ["IBLOCK_ID" => $financeIblockId, "ACTIVE" => "Y", "ID" => $sectionIds]
                ])->fetchAll();
                ?>

                <?php foreach ($sections as $section) :?>
                    <h4><?=$section["NAME"]?></h4>
                    <?php
                    global $arFilter;
                    $arFilter = $elementsByGroup[$financeIblockId];
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "openTab",
                        [
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => [
                                0 => "",
                                1 => "",
                            ],
                            "FILTER_NAME" => "arFilter",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => $financeIblockId,
                            "IBLOCK_TYPE" => "content",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "150",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => $section["ID"],
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => [
                                0 => "NAME",
                                1 => ""
                            ],
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "DESC",
                            "SORT_ORDER2" => "ASC",
                            "STRICT_SECTION_CHECK" => "N",
                            "COMPONENT_TEMPLATE" => "openTab"
                        ],
                        false
                    );
                    ?>
                <?php endforeach;?>
            </div>
        <?php endif;?>


        <?php if (count($elementsByGroup[$taxIblockId])) :?>
            <div class="business-search__list">
                <h3>Налоговая поддержка</h3>
                <?php
                $sectionIds = [];
                foreach ($allElements[$taxIblockId] as $element) {
                    $sectionIds[] = $element["IBLOCK_SECTION_ID"];
                }
                $entity = \Bitrix\Iblock\Model\Section::compileEntityByIblock($taxIblockId);
                $sections = $entity::getList([
                    "order" => ["IBLOCK_SECTION_ID" => "ASC", "SORT" => "ASC"],
                    "select" => ["IBLOCK_SECTION_ID"],
                    "filter" => ["IBLOCK_ID" => $taxIblockId, "ACTIVE" => "Y", "ID" => $sectionIds]
                ])->fetchAll();
                $newSectionsIds = [];
                foreach ($sections as $section) {
                    $newSectionsIds[] = $section["IBLOCK_SECTION_ID"];
                }
                $sections = $entity::getList([
                    "order" => ["IBLOCK_SECTION_ID" => "ASC", "SORT" => "ASC"],
                    "select" => ["NAME", "ID"],
                    "filter" => ["IBLOCK_ID" => $taxIblockId, "ACTIVE" => "Y", "ID" => $newSectionsIds]
                ])->fetchAll();
                ?>
                <?php foreach ($sections as $section) :?>
                    <h4><?=$section["NAME"]?></h4>
                    <?php
                    global $arFilter;
                    $arFilter = $elementsByGroup[$taxIblockId];
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "openTab",
                        [
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => [
                                0 => "",
                                1 => "",
                            ],
                            "FILTER_NAME" => "arFilter",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => $taxIblockId,
                            "IBLOCK_TYPE" => "content",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "150",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => $section["ID"],
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => [
                                0 => "NAME",
                                1 => ""
                            ],
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "DESC",
                            "SORT_ORDER2" => "ASC",
                            "STRICT_SECTION_CHECK" => "N",
                            "COMPONENT_TEMPLATE" => "openTab"
                        ],
                        false
                    );
                    ?>
                <?php endforeach;?>
            </div>
        <?php endif;?>


        <?php if (count($elementsByGroup[$propertyIblockId])) :?>
            <div class="business-search__list">
                <h3>Имущественная поддержка</h3>
                <?php
                global $arFilter;
                $arFilter = $elementsByGroup[$propertyIblockId];
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "openTab",
                    [
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => [
                            0 => "",
                            1 => "",
                        ],
                        "FILTER_NAME" => "arFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => $propertyIblockId,
                        "IBLOCK_TYPE" => "content",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "150",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => [
                            0 => "NAME",
                            1 => ""
                        ],
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "COMPONENT_TEMPLATE" => "openTab"
                    ],
                    false
                );
                ?>
            </div>
        <?php endif;?>

        <?php if (count($elementsByGroup[$anotherIblockId])) :?>
            <div class="business-search__list">
                <h3>Прочие меры поддержки</h3>
                <?php
                global $arFilter;
                $arFilter = $elementsByGroup[$anotherIblockId];
                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "openTab",
                    [
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => [
                            0 => "",
                            1 => "",
                        ],
                        "FILTER_NAME" => "arFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => $anotherIblockId,
                        "IBLOCK_TYPE" => "content",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "150",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => [
                            0 => "NAME",
                            1 => ""
                        ],
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N",
                        "COMPONENT_TEMPLATE" => "openTab"
                    ],
                    false
                );
                ?>
            </div>
        <?php endif;?>
    </div>
</section>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>