<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Бизнесу");
?>
    <section class="business">
        <div class="container">
            <h2><?=GetMessage("H1")?></h2>
            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "sectionMenu",
                array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "3",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "submenuHeaderMenu" . LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "COMPONENT_TEMPLATE" => "sectionMenu",
                    "CHILD_MENU_TYPE" => "submenuHeaderMenu" . LANGUAGE_ID
                ),
                false
            );
            ?>
        </div>
    </section>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>