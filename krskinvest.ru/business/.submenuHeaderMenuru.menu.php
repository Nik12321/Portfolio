<?php
$aMenuLinks = [
    [
        "Меры поддержки бизнеса",
        "/business/business-support/",
        [],
        ["SVG" => "handshake"],
        ""
    ],
    [
        "Инвестиционные площадки",
        "/business/platform/",
        [],
        ["SVG" => "cirqle-1"],
        ""
    ],
    [
        "Технологическое присоединение к электрическим сетям",
        "/business/electrical-networks/",
        [],
        ["SVG" => "electric"],
        ""
    ],
    [
        "Арктика",
        "/business/arctic/",
        [],
        ["CLASS" => "arctic"],
        ""
    ],
    [
        "Кадровое обеспечение",
        "/business/cadr-sup/",
        [],
        ["SVG" => "people-group"],
        ""
    ],
    [
        "Линия обратной связи",
        "/business/feedback/",
        [],
        ["SVG" => "chat"],
        ""
    ],
    [
        "Инфраструктура поддержки",
        "/business/infrastructure-sup/",
        [],
        ["SVG" => "people-web"],
        ""
    ],
    [
        "Государственно-частное партнерство",
        "/business/gos-part/",
        [],
        ["CLASS" => "gchp-nav"],
        ""
    ]
];
?>