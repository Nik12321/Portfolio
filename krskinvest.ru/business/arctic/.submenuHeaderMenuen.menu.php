<?php
$aMenuLinks = [
    [
        "General information",
        "/business/arctic/",
        [],
        [],
        ""
    ],
    [
        "How to become a resident",
        "/business/arctic/resident/",
        [],
        [],
        ""
    ],
    [
        "Preferences for residents of<br>the Arctic zone of the Russian Federation",
        "/business/arctic/preference/",
        [],
        [],
        ""
    ],
    [
        "Regulations",
        "/business/arctic/documents/",
        [],
        [],
        ""
    ]
];
?>