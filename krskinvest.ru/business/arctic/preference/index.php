<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Преференции для резидентов Арктической зоны РФ");
?>    <!-- секция основной информации страницы -->
    <section class="arctic-pref">
        <div class="container">
            <h2>Арктика</h2>
            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "cubeMenu",
                array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "submenuHeaderMenu" . LANGUAGE_ID,
                    "USE_EXT" => "N",
                    "COMPONENT_TEMPLATE" => "headerMenu"
                ),
                false
            );
            $iblockId = getIblockIdByCode("arcticPreference");
            $sections = \Bitrix\Iblock\SectionTable::getList([
                'order' => ["DATE_CREATE" => "ASC"],
                'select' => ["NAME", "ID", "CNT"],
                "filter" => ["IBLOCK_ID" => $iblockId],
                "runtime" => [
                    'elements' => [
                        'data_type' =>"Bitrix\Iblock\ElementTable",
                        'reference' => [
                            '=this.IBLOCK_ID' => 'ref.IBLOCK_ID',
                            '=this.ID' => 'ref.IBLOCK_SECTION_ID',
                            '=this.ACTIVE' => 'ref.ACTIVE',
                        ],
                    ],
                    'CNT' => [
                        'data_type' => 'integer',
                        'expression' => ['count(%s)', 'elements.ID']
                    ]
                ]
            ])->fetchAll();
            ?>
            <?php if (count($sections)) :?>
                <div class="grey-tab--wrap">
                    <?php foreach ($sections as $section) :?>
                        <button class="grey-tab tab"  onclick="openTab(event, '<?="supSection-" . $section["ID"]?>')">
                            <?=$section["NAME"]?>
                        </button>
                    <?php endforeach;?>
                </div>
                <?php foreach ($sections as $section) :?>
                    <div class="tabcontent" style="display: none;" id="<?="supSection-" . $section["ID"]?>">
                        <div class="arctic-pref__dropdown--wrap <?=($section["CNT"] == 3) ? "three-el" : "four-el" ?>">
                            <?php
                            global $subFilter;
                            $subFilter = ["IBLOCK_SECTION_ID" => $section["ID"]];
                            $APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "arcticPreference",
                                array(
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "ADD_SECTIONS_CHAIN" => "Y",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "N",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "N",
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "FILTER_NAME" => "subFilter",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "IBLOCK_ID" => $iblockId,
                                    "IBLOCK_TYPE" => "content",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "MESSAGE_404" => "",
                                    "NEWS_COUNT" => "21",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => ".default",
                                    "PAGER_TITLE" => "Новости",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_LAST_MODIFIED" => "N",
                                    "SET_META_DESCRIPTION" => "N",
                                    "SET_META_KEYWORDS" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "N",
                                    "SHOW_404" => "N",
                                    "SORT_BY1" => "SORT",
                                    "SORT_ORDER1" => "ASC",
                                    "STRICT_SECTION_CHECK" => "N",
                                    "COMPONENT_TEMPLATE" => "arcticPreference",
                                    "FIELD_CODE" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "PROPERTY_CODE" => array(
                                        0 => "SVG",
                                        1 => ""
                                    )
                                ),
                                false
                            );
                            ?>
                        </div>
                        <?php if ($section["CNT"] != 3) :?>
                            <div class="arctic-pref__support">
                                Ознакомиться с мерами поддержки инвесторов в Арктике можно <a target="_blank" class="btn--link" href="https://investarctic.com/">«ЗДЕСЬ»</a>
                            </div>
                        <?php endif;?>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </section>
    <script>
        function openTab(evt, openTab) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tab");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(openTab).style.display = "block";
            evt.currentTarget.className += " active";
        }
        document.getElementById("defaultOpen").click();
    </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>