<?php
$aMenuLinks = [
    [
        "Общие сведения",
        "/business/arctic/",
        [],
        [],
        ""
    ],
    [
        "Как стать резидентом",
        "/business/arctic/resident/",
        [],
        [],
        ""
    ],
    [
        "Преференции для резидентов<br>Арктической зоны РФ",
        "/business/arctic/preference/",
        [],
        [],
        ""
    ],
    [
        "Нормативные документы",
        "/business/arctic/documents/",
        [],
        [],
        ""
    ]
];
?>