<?php
$aMenuLinks = [
    [
        "業務支持措施",
        "/business/business-support/",
        [],
        ["SVG" => "handshake"],
        ""
    ],
    [
        "投資地點",
        "/business/platform/",
        [],
        ["SVG" => "cirqle-1"],
        ""
    ],
    [
        "與電網的技術連接",
        "/business/electrical-networks/",
        [],
        ["SVG" => "electric"],
        ""
    ],
    [
        "北極",
        "/business/arctic/",
        [],
        ["CLASS" => "arctic"],
        ""
    ],
    [
        "人員配備",
        "/business/cadr-sup/",
        [],
        ["SVG" => "people-group"],
        ""
    ],
    [
        "反饋專線",
        "/business/feedback/",
        [],
        ["SVG" => "chat"],
        ""
    ],
    [
        "支持基礎設施",
        "/business/infrastructure-sup/",
        [],
        ["SVG" => "people-web"],
        ""
    ],
    [
        "公私合營",
        "/business/gos-part/",
        [],
        ["CLASS" => "gchp-nav"],
        ""
    ]
];
?>