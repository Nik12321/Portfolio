<? $_SERVER["DOCUMENT_ROOT"] = __DIR__; ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<? $query = new \Bitrix\Main\Entity\Query(Bitrix\Iblock\ElementTable::getEntity()); 
$query
->setSelect(array('ID','DETAIL_PICTURE', 'IBLOCK_ID'))
->setFilter(array( "ACTIVE" => "Y", "IBLOCK_ID"=> array(16, 7)))
->setCacheTtl(360000);
$obResult = $query->exec();
$elements = $obResult->fetchall();
$iblock16counter = 0;
foreach ($elements as $element) {
	if ($element["IBLOCK_ID"]==16) {
		$iblock16counter++;	
	}
}
echo "В 16 - ом инфоблоке  $iblock16counter элемента(ов)";?><br><br><?
$iblock16picturecounter = 0;
foreach ($elements as $element) {
	if ($element["DETAIL_PICTURE"]==NULL && $element["IBLOCK_ID"]==16) {
		$iblock16picturecounter++;	
	}
}
echo "В 16 - ом инфоблоке $iblock16picturecounter  элемента(ов) без фотографии"; ?><br><br><?
$persent16=($iblock16picturecounter/$iblock16counter)*100; 
echo "В 16 - ом инфоблоке $persent16 %  элемента(ов) без фотографии"; ?><br><br><?
$iblock7counter = 0;
foreach ($elements as $element) {
	if ($element["IBLOCK_ID"]==7) {
		$iblock7counter++;
	}
}
echo "В 7 - ом инфоблоке  $iblock7counter элемента(ов)"; ?><br><br><?

$iblock7picturecounter = 0;
foreach ($elements as $element) {
	if ($element["DETAIL_PICTURE"]==NULL && $element["IBLOCK_ID"]==7) {
		$iblock7picturecounter++;	
	}
}
echo "В 7 - ом инфоблоке $iblock7picturecounter  элемента(ов) без фотографии"; ?><br><br><?
$persent7=($iblock7picturecounter/$iblock7counter)*100; 
echo "В 7 - ом инфоблоке $persent7 %  элемента(ов) без фотографии"; ?><br><br><?
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>7,"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$article7arr = array($arProps['ARTICLE']['VALUE']);
$iblock7articlecounter = 0;                     		                			
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arProps = $ob->GetProperties();
	$article7 = $arProps['ARTICLE']['VALUE']; 
	array_push($article7arr,$arProps['ARTICLE']['VALUE']); 
	if (isset($article7)) {
		$iblock7articlecounter++;
	}	
}
echo "$iblock7articlecounter товаров имеют артикул в инфоблоке 7";?><br><br><?
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>16,"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$iblock16articlecounter = 0;
$article16arr = array($arProps['CML2_ARTICLE']['VALUE']);
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arProps = $ob->GetProperties();
	$article16 = $arProps['CML2_ARTICLE']['VALUE'];
	array_push($article16arr,$arProps['CML2_ARTICLE']['VALUE']); 
	if (isset($article16)) {
		$iblock16articlecounter++;
	}

}
echo "$iblock16articlecounter товаров имеют артикул в инфоблоке 16";?><br><br><?
$diff = array_intersect($article7arr, $article16arr); 
$iblockСoincidenceСounter = count($diff);
echo "$iblockСoincidenceСounter совпадений по артикулам между инфоблоками 7 и 16";?><br><br><?
$arSelect = Array("ID", "IBLOCK_ID", "NAME","DETAIL_PICTURE","DATE_ACTIVE_FROM","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>7,"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$iblock7propsarr = array();              		                			
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arProps = $ob->GetProperties(); 
	$iblock7propsarr[] = array('Article' => $arProps['ARTICLE']['VALUE'],'Picture' => $arFields['DETAIL_PICTURE']['VALUE']);
}
$arSelect = Array("ID", "IBLOCK_ID", "NAME","DETAIL_PICTURE","DATE_ACTIVE_FROM","PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>16,"ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$iblock16propsarr = array();              		                			
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();  
	$arProps = $ob->GetProperties(); 
	$iblock16propsarr[] = array('Article' => $arProps['CML2_ARTICLE']['VALUE'],'Picture' => $arFields['DETAIL_PICTURE']['VALUE']);
}
$iblock167dif = array();
foreach ($iblock16propsarr as $pos => $value)
{
	foreach ($iblock7propsarr as $article)
	{
		if ($article['Article'] == $value['Article'])
		{
			array_push($iblock167dif, $article);
		}
	}
}
$iblockallpicturecounter = 0;
foreach ($iblock167dif as $key) {
	if ($key['Picture']!=NULL) {
		$iblockallpicturecounter++;
	}
}
$iblockallpicturecounterfinal = $iblockСoincidenceСounter-$iblockallpicturecounter;
echo "$iblockallpicturecounterfinal экспортируемых товара(ов) не имеют картинки";?><br><br><?
$exportСoincidence=$iblock7counter-$iblockСoincidenceСounter;
echo "$exportСoincidence товара(ов) не экспортировались";?><br><br><?

$iblock7pictureexportcounter = 0;
foreach ($elements as $element) {
	if ($element["DETAIL_PICTURE"]!=NULL && $element["IBLOCK_ID"]==7) {
		$iblock7pictureexportcounter++;	
	}
}
$iblock7pictureexportcounterfinal = $iblock7pictureexportcounter - $iblockallpicturecounter;
echo "$iblock7pictureexportcounterfinal фотографий товаров не экспортировались"; ?><br><br><?
