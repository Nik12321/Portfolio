<?
// Подключаем API Bitriks
$_SERVER["DOCUMENT_ROOT"] = __DIR__;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);


$catalogFirstID = 7;
$catalogNewID = 16;
$arSelect1 = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_ARTICLE");
$elemen1 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $catalogFirstID), false, false, $arSelect1);

$arSelect2 = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_CML2_ARTICLE");
$elemen2 = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => $catalogNewID), false, false, $arSelect2);

//$maxHandledCount = isset($_GET["max"]) && $_GET["max"] > 0 ? $_GET["max"] : 500;

$countElementsIB = CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => 7),
    array(),
    false,
    array('ID', 'NAME')
); 

$arNoArticle1 = array();
$arNoArticle2 = array();
$array2 = array();
while($ob = $elemen2->GetNextElement()) {
	$elem = $ob->fields;
	
	if (!$elem['PROPERTY_CML2_ARTICLE_VALUE']) {
		//$arNoArticle1[] = $elem["ID"];
	} else {
		$array2[$elem['PROPERTY_CML2_ARTICLE_VALUE']] = $elem;
	}
	
	//break;
}

//echo '<pre>';
//print_r($arNoArticle1);
//echo '</pre>';
//exit;

$i = 0;
while($ob = $elemen1->GetNextElement()) {
	$el = $ob->fields;
	
	if (!$el['PROPERTY_ARTICLE_VALUE']) {
		//$arNoArticle2[] = $el['ID'];
		continue;
	}

	if($array2[$el['PROPERTY_ARTICLE_VALUE']] && 1) {
		$i++;
		setImage($el, $array2[$el['PROPERTY_ARTICLE_VALUE']]);
		
		if(0) {
			echo '<pre>';
			print_r(array(
				$el,
				$array2[$el['PROPERTY_ARTICLE_VALUE']]
			));
			echo '</pre>';
		}
	}
	
	//if($i >= $maxHandledCount) {
	//	break;
	//}
}

//echo '<pre>';
//print_r($arNoArticle2);
//echo '</pre>';
//exit;

function setImage($elem, $newElement) {
	$CIblock = new CIBlockElement;
	
	$ID = $newElement["ID"];

	$arLoadProductArray = Array();
	
	if(isset($elem["PREVIEW_PICTURE"]) && $elem["PREVIEW_PICTURE"]) {
		if($newElement["PREVIEW_PICTURE"]) {
			echo "New product with ID ".$ID. " have PREVIEW_PICTURE yet. skiping.<br/>";
		} else {
			$arLoadProductArray["PREVIEW_PICTURE"] = CFile::MakeFileArray(CFile::GetPath($elem["PREVIEW_PICTURE"]));
		}
	}
	
	if(isset($elem["DETAIL_PICTURE"]) && $elem["DETAIL_PICTURE"]) {
		if($newElement["DETAIL_PICTURE"]) {
			echo "New product with ID ".$ID. " have DETAIL_PICTURE yet. skiping.<br/>";
		} else {
			$arLoadProductArray["DETAIL_PICTURE"] = CFile::MakeFileArray(CFile::GetPath($elem["DETAIL_PICTURE"]));
		}
	}
	
	if(count($arLoadProductArray) > 0) {
		$res = $CIblock->Update($ID, $arLoadProductArray);
		
		if ($res) {
			print_r($ID." set <br>");
		}
	} else {
		echo "New product with ID ".$ID. " doesn't have changes in images.<br/>";
	}
	echo "<br/>";
}

//print_r('noArticle');
//print_r($arNoArticle1);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>