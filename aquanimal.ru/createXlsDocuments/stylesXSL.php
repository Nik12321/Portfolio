<?php
$allStyles = array();
//Заголовок
$allStyles["title"]  = new PHPExcel_Style();
$allStyles["title"] ->applyFromArray(
    array(
        'font' => array(
            'bold' => true,
            'size' => 20,
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'wrap'       => TRUE
        )
    )
);
//Обычное поле вне таблицы
$allStyles["normalWithoutTable"] = new PHPExcel_Style();
$allStyles["normalWithoutTable"]->applyFromArray(
    array(
        'font' => array(
            'bold' => false,
            'size' => 10,
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'wrap'       => TRUE
        )
    )
);
//Обычное поле внутри таблицы (для элементов)
$allStyles["elementCommonSell"] = new PHPExcel_Style();
$allStyles["elementCommonSell"]->applyFromArray(
    array(
        'font' => array(
            'bold' => false,
            'size' => 10,
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'wrap'       => TRUE
        ),
        'borders' => array(
            'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
    )
);
//Обычное поле внутри таблицы (для элементов)
$allStyles["price"] = new PHPExcel_Style();
$allStyles["price"]->applyFromArray(
    array(
        'font' => array(
            'bold' => false,
            'size' => 10,
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'wrap'       => TRUE
        ),
        'borders' => array(
            'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffd700')
        ),
    )
);
//Обычное поле внутри таблицы (для разделов)
$allStyles["section"] = new PHPExcel_Style();
$allStyles["section"]->applyFromArray(
    array(
        'font' => array(
            'bold' => false,
            'size' => 10,
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'wrap'       => TRUE
        ),
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'd8bfd8')
        ),
        'borders' => array(
            'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
    )
);
//Названия колонок в таблице
$allStyles["h1"] = new PHPExcel_Style();
$allStyles["h1"]->applyFromArray(
    array(
        'font' => array(
            'bold' => true,
            'size' => 12,
        ),
        'alignment' => array(
            'horizontal'    => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'wrap'       => TRUE
        ),
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'fffff4')
        ),
        'borders' => array(
            'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
    )
);

//Красная ячейка с черной границей
$allStyles["error"] = new PHPExcel_Style();
$allStyles["error"]->applyFromArray(
    array(
        'font' => array(
            'bold' => false,
            'size' => 10,
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            'wrap'       => TRUE
        ),
        'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ff0000')
        ),
        'borders' => array(
            'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
            'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
        ),
    )
);