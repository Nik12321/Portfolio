<?php
//Функция, которая будет заполнять xls файл данными на основе массива с иерархией разделов
function fillPrices($array, $groupLevel = 1) {
    global $index, $allStyles, $objPHPExcel, $allElements;
    if ($array["CHILD"]) {
        foreach ($array["CHILD"] as $child) {
            if ($child["CHILD"]) {
                $objPHPExcel->getActiveSheet()->getRowDimension($index)->setOutlineLevel($groupLevel);
                $objPHPExcel->getActiveSheet()->getRowDimension($index)->setVisible(true);
                //Тут добавляем название раздела
                $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $child["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('C' . $index)->getHyperlink()->setUrl($child["SECTION_PAGE_URL"]);
                $objPHPExcel->getActiveSheet()->getCell('C' . $index)->getHyperlink()->setTooltip('Navigate to website');
                $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["section"], 'A' . $index . ':G' . $index);
                $index++;
                //Если есть элементы в разделе - заполняем ими xls файл
                if (count($allElements[$child["ID"]])) {
                    foreach ($allElements[$child["ID"]] as $element) {
                        $objPHPExcel->getActiveSheet()->getRowDimension($index)->setOutlineLevel($groupLevel + 1);
                        $objPHPExcel->getActiveSheet()->getRowDimension($index)->setVisible(true);
                        if ($element["PROPERTY_CML2_ARTICLE_VALUE"]) {
                            $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('B' . $index, $element["PROPERTY_CML2_ARTICLE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                        $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $element["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->getCell('C' . $index)->getHyperlink()->setUrl($element["DETAIL_PAGE_URL"]);
                        $objPHPExcel->getActiveSheet()->getCell('C' . $index)->getHyperlink()->setTooltip('Navigate to website');
                        if ($element["PRICE"]) {
                            $objPHPExcel->setActiveSheetIndex()->setCellValue('F' . $index, ic($element["PRICE"]));
                        }
                        if ($element["PROPERTY_CML2_BAR_CODE_VALUE"]) {
                            $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('G' . $index, $element["PROPERTY_CML2_BAR_CODE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                        }
                        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["elementCommonSell"], 'A' . $index . ':G' . $index);
                        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["price"], 'F' . $index);
                        $index++;
                    }
                }
                fillPrices($child, $groupLevel + 1);
            }
            else {
                fillPrices($child, $groupLevel);
            }
        }
    }
    else {
        $objPHPExcel->getActiveSheet()->getRowDimension($index)->setOutlineLevel($groupLevel);
        $objPHPExcel->getActiveSheet()->getRowDimension($index)->setVisible(true);
        $groupLevel++;
        $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $array["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->getCell('C' . $index)->getHyperlink()->setUrl($array["SECTION_PAGE_URL"]);
        $objPHPExcel->getActiveSheet()->getCell('C' . $index)->getHyperlink()->setTooltip('Navigate to website');
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["section"], 'A' . $index . ':G' . $index);
        $index++;
        //Если есть элементы в разделе - заполняем ими xls файл
        if (count($allElements[$array["ID"]])) {
            foreach ($allElements[$array["ID"]] as $element) {
                $objPHPExcel->getActiveSheet()->getRowDimension($index)->setOutlineLevel($groupLevel);
                $objPHPExcel->getActiveSheet()->getRowDimension($index)->setVisible(true);
                if ($element["PROPERTY_CML2_ARTICLE_VALUE"]) {
                    $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('B' . $index, $element["PROPERTY_CML2_ARTICLE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                }
                $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $element["NAME"], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->getCell('C' . $index)->getHyperlink()->setUrl($element["DETAIL_PAGE_URL"]);
                $objPHPExcel->getActiveSheet()->getCell('C' . $index)->getHyperlink()->setTooltip('Navigate to website');
                if ($element["PRICE"]) {
                    $objPHPExcel->setActiveSheetIndex()->setCellValue('F' . $index, ic($element["PRICE"]));
                }
                if ($element["PROPERTY_CML2_BAR_CODE_VALUE"]) {
                    $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('G' . $index, $element["PROPERTY_CML2_BAR_CODE_VALUE"], PHPExcel_Cell_DataType::TYPE_STRING);
                }
                $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["elementCommonSell"], 'A' . $index . ':G' . $index);
                $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["price"], 'F' . $index);
                $index++;
            }
        }
    }
}

//Функция, которая будет заполнять xls файл данными на основе массива с иерархией разделов
function insertBooklets($booklets, $bookletSectionArticle, $title) {
    global $index, $allStyles, $objPHPExcel;
    $groupLevel = 1;
    if (count($booklets)) {
        if ($title) {
            $objPHPExcel->getActiveSheet()->getRowDimension($index)->setOutlineLevel($groupLevel);
            $objPHPExcel->getActiveSheet()->getRowDimension($index)->setVisible(true);
            $groupLevel++;
            if (is_array($title)) {
                foreach ($title as $name => $article) {
                    $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('B' . $index, $article, PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $name, PHPExcel_Cell_DataType::TYPE_STRING);
                    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["section"], 'A' . $index . ':G' . $index);
                    $index++;
                    break;
                }
            }
            else {
                $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $title, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["section"], 'A' . $index . ':G' . $index);
                $index++;
            }
        }
        foreach ($booklets as $key => $booklet) {
            $objPHPExcel->getActiveSheet()->getRowDimension($index)->setOutlineLevel($groupLevel);
            $objPHPExcel->getActiveSheet()->getRowDimension($index)->setVisible(true);
            if ($bookletSectionArticle[$key]) {
                $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('B' . $index, $bookletSectionArticle[$key], PHPExcel_Cell_DataType::TYPE_STRING);
                unset($bookletSectionArticle[$key]);
            }
            $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $key, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["section"], 'A' . $index . ':G' . $index);
            $index++;
            foreach($booklet as $name => $article) {
                $objPHPExcel->getActiveSheet()->getRowDimension($index)->setOutlineLevel($groupLevel + 1);
                $objPHPExcel->getActiveSheet()->getRowDimension($index)->setVisible(true);
                $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('B' . $index, $article, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $name, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["elementCommonSell"], 'A' . $index . ':G' . $index);
                $index++;
            }
        }
        if (count($bookletSectionArticle)) {
            foreach ($bookletSectionArticle as $name => $article) {
                $objPHPExcel->getActiveSheet()->getRowDimension($index)->setOutlineLevel($groupLevel);
                $objPHPExcel->getActiveSheet()->getRowDimension($index)->setVisible(true);
                $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('B' . $index, $article, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('C' . $index, $name, PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["section"], 'A' . $index . ':G' . $index);
                $index++;
            }
        }
    }
}