<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"infootercatalog", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"BLOCK_TITLE" => "Бренды",
		"CHILD_MENU_TYPE" => "footercatalog",
		"COMPONENT_TEMPLATE" => "infootercatalog",
		"COMPOSITE_FRAME_MODE" => "Y",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DELAY" => "N",
		"ELLIPSIS_NAMES" => "Y",
		"LVL1_COUNT" => "0",
		"LVL2_COUNT" => "0",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footercatalog",
		"USE_EXT" => "Y"
	),
	false
);?>