<div class="footer-social-networks" style="margin-bottom:25px;">
	<div style="margin-bottom:6px;text-transform:uppercase;">Напишите нам</div>
	<table class="icons-social">
		<tbody>
			<tr>
				<td><a href="https://api.whatsapp.com/send?phone=79255061114" class="whatsapp"><img src="/include/whatsapp.svg" width="30"></a></td>
				<td><a href="https://www.youtube.com/channel/UCLUSVePwn4tB6CZnHTTL0RA?view_as=subscriber" class="viber"><img src="/include/youtube.svg" width="30"></a></td>
				<td><a href="https://tglink.ru/@AquanimalBot" class="telegramm" target="_blank"><img src="/include/telegram.svg" width="30"></a></td>
			</tr>
			<tr>
				<td><a href="https://www.instagram.com/aquanimal_b2b/" class="instagram" target="_blank"><img src="/include/instagram.svg" width="30"></a></td>
				<td><a href="https://vk.com/aquanimal" class="vk" target="_blank"><img src="/include/vk.svg" width="30"></a></td>
				<td><a href="" class="facebook" target="_blank"><img src="/include/facebook.svg" width="30"></a></td>
			</tr>
		</tbody>
	</table>
</div>
