<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
$productID = $_POST["productID"];
if ($productID) {
    $dbBasketItems = \Bitrix\Sale\Basket::getList([
        'select' => ['ID'],
        'filter' => [
            'PRODUCT_ID' => $productID,
            '=FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
            '=ORDER_ID' => null
        ]
    ])->Fetch();
    if ($dbBasketItems) {
        echo $dbBasketItems["ID"];
    } else {
        echo 0;
    }
} else {
    echo 0;
}