<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
$productID = $_POST["id"];
$elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "ID" => $productID), false, false, array("ID", "PROPERTY_CML2_ARTICLE", "NAME", "QUANTITY", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PREVIEW_TEXT"));
if ($element = $elements->GetNext()) {
    $photo = CFile::GetFileArray($element["DETAIL_PICTURE"]);
    $photo =  $photo["ID"];
    $photo = CFile::ResizeImageGet(
        $photo,
        array("width"=> 196, "height" => 196),
        BX_RESIZE_IMAGE_EXACT,
        true
    );
    $customPrices = getCustomPricesForProducts(array($productID));
    $customPriceForProduct = $customPrices[$productID]["RESULT_PRICE"]["DISCOUNT_PRICE"];
    $priceF = priceFormat($customPriceForProduct);
    $priceF = ''. $priceF .' <i class="units">&#x20bd;</i>';

    if ($element["QUANTITY"] <= 0) {
        $quantityInfo = '<span class="b-stores__genamount" data-href="#stocks" title="" data-es-offset="-135"><span class="js-stores__value"><span style="color:red; font-wieght: bold">Нет в наличии</span><svg class="svg-icon empty"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-4x-stores-not-available"></use></svg></span></span>';
    }
    else if ($element["QUANTITY"] < 10) {
        $quantityInfo = '<span class="b-stores__genamount" data-href="#stocks" title="" data-es-offset="-135"><span class="js-stores__value">Мало<svg class="svg-icon isset"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-4x-stores-available"></use></svg></span></span>';
    }
    else {
        $quantityInfo = '<span class="b-stores__genamount" data-href="#stocks" title="" data-es-offset="-135"><span class="js-stores__value">В наличии<svg class="svg-icon isset"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-4x-stores-available"></use></svg></span></span>';
    }

    $html = "<div id='addProductFromParseArticle'>
<div class=\"popup-background\" style='display:block'></div>
<div class=\"popup-notice-add-to-basket\" style='display:block'>
    <div class=\"popup-main-block-add-to-basket\">
        <a href=\"#\" class=\"popup-close-add-to-basket\">
            <svg id=\"SvgjsSvg1072\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"25\" height=\"25\" viewBox=\"0 0 25 25\">
                <defs id=\"SvgjsDefs1073\"></defs>
                <path id=\"SvgjsPath1074\" d=\"M336.824 218.69L332.509 214.417L328.219 218.75L326.31 216.841L330.584 212.511L326.25 208.219L328.159 206.309L332.488 210.582L336.764 206.25L338.69 208.175L334.419 212.489L338.75 216.76500000000001ZM332.5 200C325.597 200 320 205.596 320 212.5C320 219.403 325.597 225 332.5 225C339.403 225 345 219.403 345 212.5C345 205.596 339.403 200 332.5 200Z \" fill=\"#7e7e7e\" fill-opacity=\"1\" transform=\"matrix(1,0,0,1,-320,-200)\"></path>
            </svg>
        </a>
        <div class=\"popup-info\">
        <h1>По введенному артикулу найден следующий товар:</h1>
                <div class=\"list-showcase__picture\">
                    <a href=\"" . $element['DETAIL_PAGE_URL'] . "\"><img class=\"js-list-picture js-lazy\" src=\"". $photo['src'] . "\" alt=\"" . $element['NAME'] . "\" title=\"" . $element['NAME'] . "\" style=\"\"></a>
                </div>
                <div class=\"list-showcase__name-rating\">
                    <div class=\"list-showcase__name\"><a href=\"" . $element['DETAIL_PAGE_URL'] . "\">" . $element['NAME'] . "</a></div>
                    <div class=\"list-showcase__rating\">
                    </div>
                </div>
                <div class=\"list-showcase__article\">Артикул: " . $element['PROPERTY_CML2_ARTICLE_VALUE'] . "</div>
                <div>
                    <div class=\"c-prices js-prices view-list page-list product-alone-popup multyprice-no\" data-page=\"list\" data-view=\"list\" data-maxshow=\"2\" data-showmore=\"Y\" data-usealone=\"Y\" data-multiprice=\"N\" data-productmultiprice=\"N\">
                    <span class=\"c-prices__price js-prices__price js-prices__price-code_BASE\" data-pricecode=\"BASE\">
                        <span class=\"c-prices__value js-prices_pdv_BASE\">" . $priceF . "</span>
                    </span>
                    </div>
                </div>
                <div class=\"stores-popup\">
                <div class=\"b-stores js-stores\" data-firstelement=\"" . $element['ID'] . "\" data-page=\"list\">
                <div class=\"b-stores__inner\">". $quantityInfo . "</div>
                </div>
                </div>
                <div class=\"list-showcase__pay hidden-print\">
                    <span class=\"b-pay__quantity\">
                     <input type=\"hidden\" name=\"id\" class=\"js-add2basketpid\" value=\"\">
                     <span class=\"c-quantity\">
                     <span class=\"c-quantity__inner\">
                     <a class=\"c-quantity__minus js-minus\"></a>
                     <input type=\"text\" class=\"c-quantity__value js-quantity ajax-quantity-popup\" name=\"quantity\" value=\"1\" data-ratio=\"1\">
                     <span class=\"c-quantity__measure js-measurename\">шт</span>
                     <a class=\"c-quantity__plus js-plus\"></a>
                     </span>
                     </span>
                    </span>
                </div>";
                if ($element["PREVIEW_TEXT"]) {
                    $html .= "<div class=\"list-showcase__preview-text\">
                <div class=\"list-showcase__preview-text__text\">" . $element['PREVIEW_TEXT'] . "</div>
                <div class=\"list-showcase__preview-text__more\"><a href=\"" . $element['DETAIL_PAGE_URL'] . "\" title=\"\">Подробнее</a></div>
                </div>";
                }
    $html .= "<button class=\"btn btn-popup popup-add\" data-id='" . $element['ID'] . "' data-entity='addToBasketPopup'>Добавить товар в корзину</button>
                </div></div></div>";
    echo $html;
}

?>