<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
$id = $_POST["id"];
$quantity = $_POST["quantity"];
$product = array();
if ($id && $quantity) {
    $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "ID" => $id), false, false, array("ID", "NAME"));
    if($element = $elements->Fetch()) {
        $product['quantity'] = $quantity;
        $product['id'] = $element['ID'];
        $product['name'] = $element['NAME'];
        $price = getCustomPricesForProducts(array($element["ID"]));
        $product["price"] =  $price[$element["ID"]]["RESULT_PRICE"]["DISCOUNT_PRICE"];
        $dbBasketItems = \Bitrix\Sale\Basket::getList([
            'select' => ["QUANTITY", "ID"],
            'filter' => [
                "PRODUCT_ID" => $element["ID"],
                '=FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
                '=ORDER_ID' => null,
                '=LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
            ]
        ]);
        if ($dbBasketItem = $dbBasketItems->Fetch()) {
            $updateFields = array("QUANTITY" => $dbBasketItem["QUANTITY"] + $quantity);
            CSaleBasket::Update($dbBasketItem["ID"], $updateFields);
        }
        else {
            $params = array(
                'IBLOCK_TYPE' => "1c_catalog_new",
                'IBLOCK_ID' => "16",
                'ACTION_VARIABLE' => "action",
                'PRODUCT_QUANTITY_VARIABLE' => "quantity",
                'PRODUCT_PROPS_VARIABLE' => "prop",
                'USE_PRICE_COUNT' => "N",
                'USE_PRODUCT_QUANTITY' => "Y",
                'PRODUCT_PROPERTIES' => Array(),
                'ADD_PROPERTIES_TO_BASKET' => "Y",
                'PARTIAL_PRODUCT_PROPERTIES' => "N",
                'OFFERS_CART_PROPERTIES' => array(),
                "PRICE" => 777,
                "CUSTOM_PRICE" => "Y",
            );
            $restat = Custom_EasyAdd2Basket($element["ID"], $quantity, $params);
        }
        $successFullAdded = "Y";
    }
    else {
        $successFullAdded = "N";
    }
}
else {
    $successFullAdded = "N";
}

$arEventFields = array ("successfullAdded" => $successFullAdded, "yandexMetricData" => $product);
echo json_encode($arEventFields);

?>