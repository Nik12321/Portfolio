<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
$basketIDs = $_POST["basketIDs"];
$arEventField = array();
$arEventField["products"] = array();
if (count($basketIDs)) {
    $dbBasketItems = \Bitrix\Sale\Basket::getList([
        'select' => ['QUANTITY', 'PRODUCT_ID'],
        'filter' => [
            '=ID' => $basketIDs,
            '=LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
        ]
    ]);
    $allID = array();
    while ($dbBasketItem = $dbBasketItems->fetch()) {
        $arEventField["products"][$dbBasketItem["PRODUCT_ID"]]["id"] = $dbBasketItem["PRODUCT_ID"];
        $arEventField["products"][$dbBasketItem["PRODUCT_ID"]]["quantity"] = (int) $dbBasketItem["QUANTITY"];
        $allID[] = $dbBasketItem["PRODUCT_ID"];
    }
    if (count($allID)) {
        $elements = CIBlockElement::GetList(false, array("IBLOCK_ID"=>16,"ID"=>$allID), false, false, array("ID", "NAME"));
        while ($element = $elements->Fetch()) {
            if ($arEventField["products"][$element["ID"]]) {
                $arEventField["products"][$element["ID"]]["name"] = $element["NAME"];
            }
        }
        $arEventField["SUCCESS"] = "Y";
    }
    else {
        $arEventField["SUCCESS"] = "N";
        $arEventField["MESSAGE"] = "ERROR, cannot get basket items";
    }
}
else {
    $arEventField["SUCCESS"] = "N";
    $arEventField["MESSAGE"] = "ERROR, cannot get basketIDs";
}
echo json_encode($arEventField);
?>