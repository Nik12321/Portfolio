<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
function BasketNumberWordEndings($num, $lang = false, $arEnds = false)
{
    if ($lang===false)
        $lang = LANGUAGE_ID;

    if ($arEnds===false)
        $arEnds = array( "ов", "ов",  "", "а");

    if ($lang=="ru")
    {
        if (strlen($num)>1 && substr($num, strlen($num)-2, 1)=="1")
        {
            return $arEnds[0];
        }
        else
        {
            $c = IntVal(substr($num, strlen($num)-1, 1));
            if ($c==0 || ($c>=5 && $c<=9))
                return $arEnds[1];
            elseif ($c==1)
                return $arEnds[2];
            else
                return $arEnds[3];
        }
    }
    elseif ($lang=="en")
    {
        if (IntVal($num)>1)
        {
            return "s";
        }
        return "";
    }
    else
    {
        return "";
    }
}


$operation = $_POST["operation"];
$productId = $_POST["id"];
$content = "";
$productData = "";
$quantity = $_POST["quantity"];
$item = "";
if ($productId) {
    $dbBasketItems = \Bitrix\Sale\Basket::getList([
        'select' => ['QUANTITY', 'ID'],
        'filter' => [
            'PRODUCT_ID' => $productId,
            '=FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
            '=ORDER_ID' => null
        ]
    ]);
    if ($arItems = $dbBasketItems->Fetch()) {
        $item = ["QUANTITY" => $quantity, "ID" => $arItems["ID"]];
        $arFields = ["QUANTITY" => $quantity];
        if (!$arFields["QUANTITY"]) {
          $content = " <div class=\"b-pay\">
            <div class=\"b-pay-fix\">
                <form class=\"b-pay__form js-pay__form js-buyform" . $productId .  " js-synchro clearfix\" name=\"add2basketform\">
                    <input type=\"hidden\" name=\"action\" value=\"ADD2BASKET\">
                    <input type=\"hidden\" name=\"id\" class=\"js-add2basketpid\"
                           value=\"". $productId ."\">
                    <span class=\"b-pay__inner\">       
                    <span class=\"b-pay__buttons\">
                        <a rel=\"nofollow\" class=\"b-pay__button b-pay__add2basket js-add2basket js-submit btn-primary\"
                           href=\"#\"
                           title=\"В корзину\">В корзину</a>
                    
                        <a rel=\"nofollow\" class=\"b-pay__button b-pay__inbasket js-inbasket btn-primary-darken\"
                           href=\"/personal/cart/\"
                           title=\"В корзине\">В корзинe</a>
                    </span>
                </span>
                    <input type=\"submit\" name=\"submit\" class=\"nonep\" value=\"\"/>
                </form>
            </div>
        </div>";
        }
        if (CSaleBasket::Update($arItems["ID"], $arFields)) {
            \Bitrix\Sale\BasketComponentHelper::updateFUserBasketPrice(\Bitrix\Sale\Fuser::getId(), SITE_ID);
            \Bitrix\Sale\BasketComponentHelper::updateFUserBasketQuantity(\Bitrix\Sale\Fuser::getId(), SITE_ID);
            $success = "Y";
            $itemsInBasket = 0;
            $totalPrice = 0;
            $dbBasketItems = \Bitrix\Sale\Basket::getList([
                'select' => ['*'],
                'filter' => [
                    '=FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
                    '=ORDER_ID' => null,
                    "=DELAY" => "N"
                ]
            ]);
            while ($arItems = $dbBasketItems->Fetch()) {
                $itemsInBasket++;
                $totalPrice += $arItems["PRICE"] * $arItems["QUANTITY"];
            }
            if (!$totalPrice) {
                $smallBasket = "Ваша корзина пуста";
            }
            else {
                $totalPrice .= " руб.";
                $smallBasket = $itemsInBasket . " товар" . BasketNumberWordEndings($itemsInBasket) . " на " . $totalPrice;
            }
        } else {
            $success = "N";
            $message = "Cannot update quantity of element";
        }
    }
    else {
        $success = "N";
        $message = "Cannot find element with id " .$productId ;
    }
}
else {
    $success = "N";
    $message = "Uncorrect id";
}

$arEventFields = array (
    "item" => $item,
    "success"=> $success,
    "message"=> $message,
    "content" => $content,
    "itemsInBasket" => $itemsInBasket,
    "totalPrice" => $totalPrice,
    "smallBasket" => $smallBasket
);
echo json_encode($arEventFields);