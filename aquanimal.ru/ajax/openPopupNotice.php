<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;
//Если пользователь не авторизован:
if (!$USER->IsAuthorized()) {
    //Если пользователь не получил уведомление:
    if ($_SESSION["GET_NOTICE"] != "Y") {
        //Отмечаем, что пользователь получил уведомление
        $_SESSION["GET_NOTICE"] = "Y";
        echo false;
    }
    //Иначе - возвращаем true
    else {
      echo true;
    }
}
else {
    //Получаем ID пользователя
    $userID = $USER->GetID();
    if ($userID) {
        //Возвращаем его
        echo $userID;
    }
    else {
        echo "Authorized, but not have ID. WTF.";
    }
}
?>