<?php

use Bitrix\Highloadblock\HighloadBlockTable;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;
if ($USER->IsAuthorized()) {
    $clientID = $_POST["clientID"];
    if ($clientID) {
        echo $clientID;
        $HighloadBlockTableID = 6;
        $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById($HighloadBlockTableID)->fetch();
        $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();
        $userID = $USER->GetID();
        $reference = $strEntityDataClass::getList(array(
            'select' => array('UF_CLIENT_ID', 'UF_USER_ID'),
            'filter' => array(
                'UF_CLIENT_ID' => $clientID,
                'UF_USER_ID' => $userID
            )
        ));
        if ($readyReference = $reference->Fetch()) {
            if ($readyReference['UF_CLIENT_ID'] == $clientID && $readyReference['UF_USER_ID'] == $userID) {
                echo true;
            }
            else {
                echo false;
            }
        }
        else {
            $result = $strEntityDataClass::add(array(
                'UF_USER_ID' => $userID,
                'UF_CLIENT_ID' => $clientID,
            ));
            if ($result) {
                echo true;
            }
            else {
                echo false;
            }
        }
    }
    else {
        echo false;
    }
}
?>