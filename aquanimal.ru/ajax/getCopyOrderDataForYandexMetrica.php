<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
$orderID = $_POST["orderID"];
$arEventField = array();
$arEventField["products"] = array();
if ($orderID) {
    $arEventField["ORDER_ID"] = $orderID;
    $dbBasketItems = \Bitrix\Sale\Basket::getList([
        'select' => ['QUANTITY', 'PRODUCT_ID'],
        'filter' => [
            '=ORDER_ID' => $orderID,
            '=LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
        ]
    ]);
    $allID = array();
    while ($dbBasketItem = $dbBasketItems->fetch()) {
        $arEventField["products"][$dbBasketItem["PRODUCT_ID"]]["id"] = $dbBasketItem["PRODUCT_ID"];
        $arEventField["products"][$dbBasketItem["PRODUCT_ID"]]["quantity"] = (int) $dbBasketItem["QUANTITY"];
        $allID[] = $dbBasketItem["PRODUCT_ID"];
    }
    if (count($allID)) {
        $elements = CIBlockElement::GetList(false, array("IBLOCK_ID"=>16,"ID"=>$allID), false, false, array("ID", "NAME"));
        $price = getCustomPricesForProducts($allID);
        while ($element = $elements->Fetch()) {
            if ($arEventField["products"][$element["ID"]]) {
                $arEventField["products"][$element["ID"]]["name"] = $element["NAME"];
                $arEventField["products"][$element["ID"]]["price"] = $price[$element["ID"]]["RESULT_PRICE"]["DISCOUNT_PRICE"];
            }
        }
        $arEventField["SUCCESS"] = "Y";
    }
    else {
        $arEventField["SUCCESS"] = "N";
        $arEventField["MESSAGE"] = "ERROR, cannot get basket items from order with ID: " . $orderID;
    }
}
else {
    $arEventField["SUCCESS"] = "N";
    $arEventField["MESSAGE"] = "ERROR, cannot get orderID";
}
echo json_encode($arEventField);
?>