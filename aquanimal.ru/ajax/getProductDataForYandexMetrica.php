<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
$productID = $_POST["productID"];
$arEventField = array();
if ($productID) {
    $element = CIBlockElement::GetList(false, array("IBLOCK_ID"=>16,"ID"=>$productID), false, false, array("ID", "NAME"))->Fetch();
    if ($element) {
        $price = getCustomPricesForProducts(array($element["ID"]));
        $price = $price[$element["ID"]]["RESULT_PRICE"]["DISCOUNT_PRICE"];
        $arEventField["PRICE"] = $price;
        $arEventField["NAME"] = $element["NAME"];
        $arEventField["ID"] = $element["ID"];
        $arEventField["SUCCESS"] = "Y";
    }
    else {
        $arEventField["SUCCESS"] = "N";
        $arEventField["MESSAGE"] = "ERROR, cannot find product with ID: " . $productID;
    }
}
else {
    $arEventField["SUCCESS"] = "N";
    $arEventField["MESSAGE"] = "ERROR, cannot get productID";
}
echo json_encode($arEventField);
?>