<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
$line = trim($_POST["lineValue"]);
$successFullAdded = "N";
$params = array();
$added = array();
if ($line) {
    $articles = explode(" ", $line);
    $articles = array_unique($articles);
    if (count($articles) === 1) {
        $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "PROPERTY_CML2_ARTICLE" => $articles), false, false, array("ID"));
        if ($element = $elements->Fetch()) {
            $productID = $element["ID"];
            $successFullAdded = "Y";
            $openPopup = "Y";
        }
        else {
            $openPopup = "N";
            $successFullAdded = "N";
            $productID = 0;
        }
    }
    else {
        $products = array();
        $allIDs = array();
        $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "PROPERTY_CML2_ARTICLE" => $articles), false, false, array("ID", "NAME", "PROPERTY_CML2_ARTICLE"));
        while($element = $elements->Fetch()) {
            if (($key = array_search($element["PROPERTY_CML2_ARTICLE_VALUE"], $articles)) !== FALSE) {
                unset($articles[$key]);
                $products["products"][$element["ID"]]["name"] = $element["NAME"];
                $products["products"][$element["ID"]]["id"] = $element["ID"];
                $products["products"][$element["ID"]]["quantity"] = 1;
                $allIDs[] = $element["ID"];
                $dbBasketItems = \Bitrix\Sale\Basket::getList([
                    'select' => ["QUANTITY", "ID"],
                    'filter' => [
                        "PRODUCT_ID" => $element["ID"],
                        '=FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
                        '=ORDER_ID' => null,
                        '=LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                    ]
                ]);
                if ($dbBasketItem = $dbBasketItems->Fetch()) {
                    $updateFields = array("QUANTITY" => $dbBasketItem["QUANTITY"] + 1);
                    CSaleBasket::Update($dbBasketItem["ID"], $updateFields);
                }
                else {
                    $params = array(
                        'IBLOCK_TYPE' => "1c_catalog_new",
                        'IBLOCK_ID' => "16",
                        'ACTION_VARIABLE' => "action",
                        'PRODUCT_QUANTITY_VARIABLE' => "quantity",
                        'PRODUCT_PROPS_VARIABLE' => "prop",
                        'USE_PRICE_COUNT' => "N",
                        'USE_PRODUCT_QUANTITY' => "Y",
                        'PRODUCT_PROPERTIES' => Array(),
                        'ADD_PROPERTIES_TO_BASKET' => "Y",
                        'PARTIAL_PRODUCT_PROPERTIES' => "N",
                        'OFFERS_CART_PROPERTIES' => array(),
                        "PRICE" => 777,
                        "CUSTOM_PRICE" => "Y",
                    );
                    $restat = Custom_EasyAdd2Basket($element["ID"], 1, $params);
                }
                $successFullAdded = "Y";
                $openPopup = "N";
            }
        }
        if ($allIDs) {
            $prices = getCustomPricesForProducts($allIDs);
            foreach ($products["products"] as $key => $elem) {
                $products["products"][$key]["price"] =  $prices[$key]["RESULT_PRICE"]["DISCOUNT_PRICE"];
            }
        }

    }
}
$arEventFields = array ("notAdded"=> $articles, "successfullAdded" => $successFullAdded, "openPopupAddOneProduct" => $openPopup, "productID" => $productID, "yandexMetricData" => $products);
echo json_encode($arEventFields);

?>