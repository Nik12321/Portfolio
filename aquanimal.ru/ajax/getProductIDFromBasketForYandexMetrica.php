<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
CModule::IncludeModule("iblock");
$productID = $_POST["basketID"];
$arEventField = array();
if ($productID) {
    $element = \Bitrix\Sale\Basket::getList([
        'select' => ['PRODUCT_ID', "QUANTITY"],
        'filter' => [
            'ID' => $productID,
            '=FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
        ]
    ])->fetch();
    if ($element) {
        $arEventField["PRODUCT_ID"] = $element["PRODUCT_ID"];
        $arEventField["QUANTITY"] = (int) $element["QUANTITY"];
        $arEventField["SUCCESS"] = "Y";
    }
    else {
        $arEventField["SUCCESS"] = "N";
        $arEventField["MESSAGE"] = "ERROR, cannot find basket item with ID: " . $productID;
    }
}
else {
    $arEventField["SUCCESS"] = "N";
    $arEventField["MESSAGE"] = "ERROR, cannot get basketID";
}
echo json_encode($arEventField);
?>