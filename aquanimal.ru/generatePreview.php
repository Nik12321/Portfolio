<?php
//echo 1;
$_SERVER["DOCUMENT_ROOT"] = __DIR__;
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
//if ($_SERVER['REMOTE_ADDR']!='127.0.0.1') die(); // защита от постороннего запуска
error_reporting(E_ERROR);
//display_errors('on');
//echo $DOCUMENT_ROOT."\n";

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//set_time_limit(0);

$globalStartTime = microtime(true);

$limit = 1000000;

function printProgressBar($current,$total,&$fw = NULL, $result = NULL)
{
    $currentPercent = round(100*$current/$total);

    $result = false;

    $appendStr = $result && is_array($result) ? json_encode($result) : '';

    if($fw)
    {
        //fprintf($fw, "YES");
        fprintf($fw,"\rProgress: %3d%% Images: %6d. %s", $currentPercent,$current, $appendStr);
    }
    else{
        printf("\rProgress: %3d%% Images: %6d. %s", $currentPercent,$current, $appendStr);
    }

    //echo '*** Progress complete thumbs ***<br/>'.$currentPercent."% <br/>";
    //echo 'Complete images: '.$current ."<br/><br/>";
}

function dump($var,$exit = false)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';

    if($exit)
    {
        exit;
    }
}

$thumbConfig = [
    'project_detail_picture'=>[
        'iblock_id'=>16,
        'thumbAttribute' => "DETAIL_PICTURE",
        //'noindexAttribute' => "PROPERTY_ATT_IMAGE_NOINDEX",
        'presets'=> [
            'inmenu'=>[
                'width'=>210,
                'height'=>140,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL, //BX_RESIZE_IMAGE_EXACT, //default
            ],
            // bitrix/templates/proopt_default/components/bitrix/catalog.element/popup/result_modifier.php
            'popup'=>[
                'width'=>210,
                'height'=>170,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/components/bitrix/catalog.section/light/component_epilog.php
            // bitrix/templates/proopt_default/components/bitrix/catalog.section/light/result_modifier.php
            'light'=>[
                'width'=>220,
                'height'=>220,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/extended/templates/catalog.element/gopro/result_modifier.php
            'gopro_element'=>[
                'width'=>90,
                'height'=>90,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/extended/templates/catalog.section/gopro/result_modifier.php
            'gopro_section_showcase'=>[
                'width'=>198,
                'height'=>208,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            'gopro_section_gallery'=>[
                'width'=>100,
                'height'=>100,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/template_ext/catalog.section/gopro/result_modifier.php
            'gopro_section_showcase_2'=>[
                'width'=>210,
                'height'=>170,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            'gopro_section_gallery_2'=>[
                'width'=>55,
                'height'=>55,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],

            // bitrix/templates/proopt_default/components/bitrix/catalog.section/searchpage/result_modifier.php
            'searchpage'=>[
                'width'=>225,
                'height'=>150,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/components/bitrix/catalog.section/searchtitle/result_modifier.php
            'searchtitle'=>[
                'width'=>22,
                'height'=>14,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],

        ],

    ],

    'project_preview_picture'=>[
        'iblock_id'=>16,
        'thumbAttribute' => "PREVIEW_PICTURE",
        //'noindexAttribute' => "PROPERTY_ATT_IMAGE_NOINDEX",
        'presets'=> [
            'inmenu'=>[
                'width'=>210,
                'height'=>140,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL, //BX_RESIZE_IMAGE_EXACT, //default
            ],
            // bitrix/templates/proopt_default/components/bitrix/catalog.element/popup/result_modifier.php
            'popup'=>[
                'width'=>210,
                'height'=>170,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/components/bitrix/catalog.section/light/component_epilog.php
            // bitrix/templates/proopt_default/components/bitrix/catalog.section/light/result_modifier.php
            'light'=>[
                'width'=>220,
                'height'=>220,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/extended/templates/catalog.element/gopro/result_modifier.php
            'gopro_element'=>[
                'width'=>90,
                'height'=>90,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/extended/templates/catalog.section/gopro/result_modifier.php
            'gopro_section_showcase'=>[
                'width'=>198,
                'height'=>208,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            'gopro_section_gallery'=>[
                'width'=>100,
                'height'=>100,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/template_ext/catalog.section/gopro/result_modifier.php
            'gopro_section_showcase_2'=>[
                'width'=>210,
                'height'=>170,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            'gopro_section_gallery_2'=>[
                'width'=>55,
                'height'=>55,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],

            // bitrix/templates/proopt_default/components/bitrix/catalog.section/searchpage/result_modifier.php
            'searchpage'=>[
                'width'=>225,
                'height'=>150,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/components/bitrix/catalog.section/searchtitle/result_modifier.php
            'searchtitle'=>[
                'width'=>22,
                'height'=>14,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],

        ],

    ],


    'project'=>[
        'iblock_id'=>16,
        'thumbAttribute' => "PROPERTY_MORE_PHOTO",
        //'noindexAttribute' => "PROPERTY_ATT_IMAGE_NOINDEX",
        'presets'=> [
            // bitrix/templates/proopt_default/components/bitrix/catalog.element/inmenu/result_modifier.php
            'inmenu'=>[
                'width'=>210,
                'height'=>140,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL, //BX_RESIZE_IMAGE_EXACT, //default
            ],
            // bitrix/templates/proopt_default/components/bitrix/catalog.element/popup/result_modifier.php
            'popup'=>[
                'width'=>210,
                'height'=>170,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/components/bitrix/catalog.section/light/component_epilog.php
            // bitrix/templates/proopt_default/components/bitrix/catalog.section/light/result_modifier.php
            'light'=>[
                'width'=>220,
                'height'=>220,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/extended/templates/catalog.element/gopro/result_modifier.php
            'gopro_element'=>[
                'width'=>90,
                'height'=>90,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/extended/templates/catalog.section/gopro/result_modifier.php
            'gopro_section_showcase'=>[
                'width'=>198,
                'height'=>208,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            'gopro_section_gallery'=>[
                'width'=>100,
                'height'=>100,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            // bitrix/templates/proopt_default/template_ext/catalog.section/gopro/result_modifier.php
            'gopro_section_showcase_2'=>[
                'width'=>210,
                'height'=>170,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],
            'gopro_section_gallery_2'=>[
                'width'=>55,
                'height'=>55,
                'watermark'=>false,
                'resizeType' => BX_RESIZE_IMAGE_PROPORTIONAL,
            ],

        ],
    ],


];



$arWatermark = Array(
    Array(
        'name' => 'watermark',
        'position' => 'center',
        "size" => "real",
        "fill" => "repeat",
        'file' => $_SERVER['DOCUMENT_ROOT'] . '/upload/watermark/watermark_mid_texture.png',
    )
);


//global $USER;

// data cache
$cacheLifetime = 86400;
$cacheID = 'GeneratePreviewThumbs';
$cachePath = '/'.$cacheID;
// state cache
$stateCacheLifetime = 86400;
$stateCacheID = 'StateGeneratePreviewThumbs';
$stateCachePath = '/'.$stateCacheID;

function clearCache($path)
{
    $obCache = new CPHPCache;
    $obCache->CleanDir("", "cache".$path);

    // удалим весь кэш для каталога
    BXClearCache(false, $path);
}
// ----------------------------------------------------------- //
// функция создания превьюшек.
// ----------------------------------------------------------- //
function generateThumbsForItem($current,&$dataQueue,&$dataConfig,&$dataSeries,&$fileSrc = [])
{
    //echo 123;
    global $arWatermark;
    if(!isset($dataSeries[$current]))
    {
        return ['msg'=>'Data Series not contain element with $current['.$current.'] index'];
    }
    $startTime = microtime(true);
    $iblockName = $dataSeries[$current][0];

    $item = $dataQueue[$iblockName][$dataSeries[$current][1]];
    //return 1;
    if($item)
    {
        if($item[$dataConfig[$iblockName]['thumbAttribute'].'_VALUE'] || $item[$dataConfig[$iblockName]['thumbAttribute']])// && count($arFields["PHOTO_GALLERY"])))
        {
            $renderSrc = [];
            $image = $item[$dataConfig[$iblockName]['thumbAttribute'].'_VALUE'] ?: $item[$dataConfig[$iblockName]['thumbAttribute']];

            $closeIndex = false;

            if(isset($dataConfig[$iblockName]['noindexAttribute']) && $dataConfig[$iblockName]['noindexAttribute']
                && $item[$dataConfig[$iblockName]['noindexAttribute'].'_VALUE'] == "Да")
            {
                $closeIndex = true;
            }

            if(count($fileSrc)>0 && isset($fileSrc[$image]) && $fileSrc[$image])
            {
                $image = $fileSrc[$image];
            }

            foreach($dataConfig[$iblockName]['presets'] as $preset)
            {
                $resizeType = $preset['resizeType'] ?: BX_RESIZE_IMAGE_EXACT;
                $waterMark = $preset['watermark'] === false ? array() : $arWatermark;

                //dump([$resizeType,$waterMark,$arWatermark]);
                //continue;
                if(1):
                    $renderSrc[] = CFile::ResizeImageGet(
                        $image
                        , Array("width" => $preset['width'],'height'=>$preset['height'])
                        , $resizeType
                        , true
                        , $waterMark
                    );
                else:
                    $renderSrc[] = FTResizeImageGet(
                        $image
                        , Array("width" => $preset['width'],'height'=>$preset['height'])
                        , $resizeType
                        , false
                        , $waterMark
                        , false
                        , false
                        , $closeIndex
                    );
                endif;

            }
            $msg = 'Thumb generate success.';
            //dump($renderSrc);
        }
        else
        {
            $msg = 'thumbAttribute not value.<br/>';
        }
    }
    else {
        $msg = 'ITEM NOT EXISTS.<br/>';
    }
    //echo 'iteration time: '.(microtime(true)-$startTime).'<br/>';

    return [
        'time'=>(microtime(true)-$startTime),
        'msg'=>$msg,
        'renderSrc'=>$renderSrc,
    ];
}
// ----------------------------------------------------------- //

if(CModule::IncludeModule("iblock"))
{
    $totalCount = 0;

    $fw = fopen("php://stdout", "w");

    //ваш код...
    echo "*** Start generate preview *** \n\n";

    // state cache
    $cacheLifetime = 1;//86400;
    $cacheID = 'consoleGenerateScript_'.md5(serialize($thumbConfig));
    $cachePath = '/'.$stateCacheID;

    // кэширование всех файлов подходящих для генерации thumb'ов.
    $obCache = new CPHPCache();

    if($obCache->InitCache($cacheLifetime, $cacheID, $cachePath) && 0)
    {
        // либо из кэша достаем их.
        $vars = $obCache->GetVars();
        $dataForGenerate = $vars['dataForGenerate'];
        $totalCount = $vars['totalCount'];
        $dataSeries = $vars['dataSeries'];
        $fileSrc = $vars['fileSrc'];
    }
    elseif($obCache->StartDataCache())
    {
        // либо вытаскиваем из БД и кладем в кэш.
        $dataForGenerate = [];
        $dataSeries = [];

        $fileId = [];
        $fileSrc = [];
        foreach($thumbConfig as $keyName=>$config)
        {
            $arSelect = Array("ID", "NAME", $config['thumbAttribute']);

            if(isset($config['noindexAttribute']) && $config['noindexAttribute']) {
                $arSelect[] = $config['noindexAttribute'];
            }

            $arFilter = Array(
                "IBLOCK_ID"=>$config['iblock_id'],
                //"ACTIVE_DATE"=>"Y",
                //"ACTIVE"=>"Y"
            );
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

            $result = array();
            $indexInSeries = 0;
            while($ob = $res->GetNextElement())
            {
                $curItem = $ob->GetFields();
                $result[] = $curItem;
                $dataSeries[$totalCount] = [$keyName,$indexInSeries++];
                $totalCount++;

                if($curItem[$config['thumbAttribute'].'_VALUE'])
                {
                    $fileId[] = $curItem[$config['thumbAttribute'].'_VALUE'];
                } elseif($curItem[$config['thumbAttribute']]) {
                    $fileId[] = $curItem[$config['thumbAttribute']];
                }

                if($totalCount > $limit) {
                    break;
                }
            }
            //dump($fileId,1);
            $dataForGenerate[$keyName] = $result;
        }
        if(count($fileId)>0)
        {
            //dump(implode(', ',$fileId),1);

            $arFilter = Array("@ID" => implode(',',$fileId));
            //найдем самые большие файлы ядра
            $res = CFile::GetList(array(), $arFilter);
            while($res_arr = $res->GetNext())
            {
                //dump($res_arr,1);

                $fileSrc[$res_arr['ID']] = $res_arr;
            }
            //echo $res_arr["SUBDIR"]."/".$res_arr["FILE_NAME"]." = ".$res_arr["FILE_SIZE"]."<br>";
        }


        //dump($dataForGenerate); exit;

        $obCache->EndDataCache(array(
            'dataForGenerate' => $dataForGenerate,
            'totalCount'=>$totalCount,
            'dataSeries'=>$dataSeries,
            'fileSrc'=>$fileSrc,
        ));
    }



    echo "Data select.\n";
    echo 'Count for generate: '.$totalCount."\n\n";
    //exit(0);
    // ----------------------------------------------------------- //

    // ----------------------------------------------------------- //
    // сохраняем состояние текущей обработки.
    // в данный момент сохраняем в сессии пользователя. надо что-нибудь получше придумать. в кэше хранить и обновлять не получилось почему-то... :)
    // $totalCount

    if ($argc != 2 || in_array($argv[1], array('-startFrom')))
    {
        if($argv[1] == '-startFrom')
        {
            $startFrom = (int)$argv[2];
        }
    }

    //dump([$startFrom,$argv],1);
    if(0) {
        print_r([
            'dataForGenerate' => $dataForGenerate,
            //'totalCount'=>$totalCount,
            //'dataSeries'=>$dataSeries,
            'fileSrc'=>$fileSrc
        ]);
    }



    for($i = $startFrom ?: 0;$i<$totalCount ;$i++)
    {
        //echo $i;
        $result = generateThumbsForItem($i,$dataForGenerate,$thumbConfig,$dataSeries,$fileSrc);
        //echo $i."\n";
        //sleep(1);
        printProgressBar($i,$totalCount,$fw, $result);
        usleep(50000);

        //break;
    }

    fclose($fw);
    echo "\n\nThat\'s all. Thanks for waiting.\n\n";
}
else{
    echo "\nCModule cannot include module 'iblock'. Error!\n";
}

echo "Total time : ".(microtime(true)-$globalStartTime)."\n\n";

fclose($fw);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>