<?php
/**
 * User: shakinm@gmail.com
 * Date: 07.03.2017
 * Time: 10:01
 */

define('BX_SESSION_ID_CHANGE', false);
define('BX_SKIP_POST_UNQUOTE', true);
define('NO_AGENT_CHECK', true);
define("STATISTIC_SKIP_ACTIVITY_CHECK", true);

if (isset($_REQUEST["type"]) && $_REQUEST["type"] == "crm")
{
   define("ADMIN_SECTION", true);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
/**
 * Class ImportPrices
 */
class ImportPrices
{
    public function parseFile($file)
    {
        return true;
    }
}

try {
  //   if (($_GET["mode"] != "run") && ($_GET["mode"] != "extract")) {
		
		// if($_GET["mode"] == "file" && $_SERVER['REQUEST_METHOD'] !== 'POST') {
  //   		echo "Failure. Mode 'file' allow only POST requests.\n";
  //   		 exit;
		// }

  //       $filename = preg_replace("#^(/tmp/|upload/1c/webdata)#", "", $_GET["filename"]);
  //       $filename = trim(str_replace("\\", "/", trim($filename)), "/");
  //       $io = CBXVirtualIo::GetInstance();
  //       $bBadFile = HasScriptExtension($filename)
  //           || IsFileUnsafe($filename)
  //           || !$io->ValidatePathString("/" . $filename);

  //       if (!$bBadFile) {
  //           $DIR_NAME = $_SERVER["DOCUMENT_ROOT"] . "/" . COption::GetOptionString("main", "upload_dir", "upload") . "/1c_prices";
  //           $FILE_NAME = rel2abs($DIR_NAME, "/" . $filename);
  //           if ((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/" . $filename)) {
  //               $ABS_FILE_NAME = $DIR_NAME . $FILE_NAME;
  //           }
  //       }

  //       unlink($ABS_FILE_NAME);


  //   	$USER->Authorize(8);
  //   	COption::SetOptionString("catalog", "DEFAULT_SKIP_SOURCE_CHECK", "Y"); 
  //   	COption::SetOptionString("sale", "secure_1c_exchange", "N");
    	
  //           $APPLICATION->IncludeComponent("bitrix:clients.get.file.1c", "", Array(
  //           	"IBLOCK_TYPE" => COption::GetOptionString("catalog", "1C_IBLOCK_TYPE", "-"),
  //           	"SITE_LIST" => array(COption::GetOptionString("catalog", "1C_SITE_LIST", "-")),
  //           	"INTERVAL" => COption::GetOptionString("catalog", "1C_INTERVAL", "-"),
  //           	"GROUP_PERMISSIONS" => explode(",", COption::GetOptionString("catalog", "1C_GROUP_PERMISSIONS", "1")),
  //           	"GENERATE_PREVIEW" => COption::GetOptionString("catalog", "1C_GENERATE_PREVIEW", "Y"),
  //           	"PREVIEW_WIDTH" => COption::GetOptionString("catalog", "1C_PREVIEW_WIDTH", "100"),
  //           	"PREVIEW_HEIGHT" => COption::GetOptionString("catalog", "1C_PREVIEW_HEIGHT", "100"),
  //           	"DETAIL_RESIZE" => COption::GetOptionString("catalog", "1C_DETAIL_RESIZE", "Y"),
  //           	"DETAIL_WIDTH" => COption::GetOptionString("catalog", "1C_DETAIL_WIDTH", "300"),
  //           	"DETAIL_HEIGHT" => COption::GetOptionString("catalog", "1C_DETAIL_HEIGHT", "300"),
  //           	"ELEMENT_ACTION" => COption::GetOptionString("catalog", "1C_ELEMENT_ACTION", "D"),
  //           	"SECTION_ACTION" => COption::GetOptionString("catalog", "1C_SECTION_ACTION", "D"),
  //           	"FILE_SIZE_LIMIT" => COption::GetOptionString("catalog", "1C_FILE_SIZE_LIMIT", 200*1024),
  //           	"USE_CRC" => COption::GetOptionString("catalog", "1C_USE_CRC", "Y"),
  //           	"USE_ZIP" => COption::GetOptionString("catalog", "1C_USE_ZIP", "Y"),
  //           	"USE_OFFERS" => COption::GetOptionString("catalog", "1C_USE_OFFERS", "N"),
  //           	"FORCE_OFFERS" => COption::GetOptionString("catalog", "1C_FORCE_OFFERS", "N"),
  //           	"USE_IBLOCK_TYPE_ID" => COption::GetOptionString("catalog", "1C_USE_IBLOCK_TYPE_ID", "N"),
  //           	"USE_IBLOCK_PICTURE_SETTINGS" => COption::GetOptionString("catalog", "1C_USE_IBLOCK_PICTURE_SETTINGS", "N"),
  //           	"TRANSLIT_ON_ADD" => COption::GetOptionString("catalog", "1C_TRANSLIT_ON_ADD", "Y"),
  //           	"TRANSLIT_ON_UPDATE" => COption::GetOptionString("catalog", "1C_TRANSLIT_ON_UPDATE", "Y"),
  //           	"TRANSLIT_REPLACE_CHAR" => COption::GetOptionString("catalog", "1C_TRANSLIT_REPLACE_CHAR", "_"),
  //           	"SKIP_ROOT_SECTION" => COption::GetOptionString("catalog", "1C_SKIP_ROOT_SECTION", "N"),
  //           	"DISABLE_CHANGE_PRICE_NAME" => COption::GetOptionString("catalog", "1C_DISABLE_CHANGE_PRICE_NAME")
  //               )
  //           );
  //       }
//    if ($_GET["mode"] == "run" && $_GET["filename"] != "") {
        // $filename = preg_replace("#^(/tmp/|upload/1c/webdata)#", "", $_GET["filename"]);
        // $filename = trim(str_replace("\\", "/", trim($filename)), "/");
        // $io = CBXVirtualIo::GetInstance();
        // $bBadFile = HasScriptExtension($filename)
        //     || IsFileUnsafe($filename)
        //     || !$io->ValidatePathString("/" . $filename);

        // if (!$bBadFile) {
        //     $DIR_NAME = $_SERVER["DOCUMENT_ROOT"] . "/" . COption::GetOptionString("main", "upload_dir", "upload") . "/1c_prices";
        //     $FILE_NAME = rel2abs($DIR_NAME, "/" . $filename);
        //     if ((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/" . $filename)) {
        //         $ABS_FILE_NAME = $DIR_NAME . $FILE_NAME;
        //     }
        // }
//    }

    if (($_GET["mode"] == "extract") && ($_GET["filename"] != "")) {
        // var_dump($_GET["mode"]);
        // var_dump($_GET["filename"]);
        // var_dump($ABS_FILE_NAME || "No");

        if (!file_exists($ABS_FILE_NAME) || !is_file($ABS_FILE_NAME)) {
            echo "failure\r\nNo zip file";
        } else {
            $zip = new ZipArchive;
            $res = $zip->open($ABS_FILE_NAME);

            if ($res !== true) {
                echo "failure\r\nBad zip file";
            } else {

                $zip->extractTo($DIR_NAME);
                $zip->close();

                // CEventLog::Add(array(
                //     "SEVERITY" => "SECURITY",
                //     "AUDIT_TYPE_ID" => '1C_IMPORT_PRICE',
                //     "MODULE_ID" => "main",
                //     "ITEM_ID" => 'FILE=' . $filename,
                //     "DESCRIPTION" => 'Архив успешно распакован',
                // ));
                echo "successful\r\nАрхив получен, файл распакован. См. журнал событий интернет-магазина";
            }
        }
    }

    if (($_GET["mode"] == "run") && ($_GET["filename"] != "")) {

        //$_SESSION['__STEPS']['__step'] = '1C_EXPORT';

        if (!file_exists($ABS_FILE_NAME) || !is_file($ABS_FILE_NAME)) {
            echo "failure\r\nNo xml file";
        } else {
            $ABS_FILE_NAME = base64_encode($ABS_FILE_NAME);
            $pid = shell_exec('/usr/bin/php -f ' . __DIR__ . '/lib/initPriceImport.php ' . $ABS_FILE_NAME);

            if (!$pid) {
                echo "failure\r\nОшибка запуска процесса импорта цен. См. журнал событий веб-сервера";
                die();
            }
            CEventLog::Add(array(
                "SEVERITY" => "SECURITY",
                "AUDIT_TYPE_ID" => '1C_IMPORT_PRICE',
                "MODULE_ID" => "main",
                "ITEM_ID" => 'PID=' . $pid,
                "DESCRIPTION" => 'Успешный запуск процесса импорта',
            ));
            echo "successful\r\nФайл получен, запущен фоновый процесс импорта. См. журнал событий интернет-магазина";
        }
    }
} catch (Exception $e) {
    echo "failure\n";
}