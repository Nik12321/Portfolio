<?php
/**
 * User: shakinm@gmail.com
 * Date: 22.03.2017
 * Time: 11:03
 */

/*
 * функция для распаралелливания процесса exec
 */
function detached_exec($cmd)
{
    $pid = pcntl_fork();
    switch ($pid) {
        case -1 :
            return false;
        case 0 :
          //  posix_setsid();
            exec($cmd);
            exit();
        default:
            return $pid;
    }
}

//$pid = exec('/usr/bin/php -f ' . __DIR__ . '/priceImport.php ' . $argv[1]);

$pid = detached_exec('/usr/bin/php -f ' . __DIR__ . '/priceImport.php ' . $argv[1]);

echo $pid;