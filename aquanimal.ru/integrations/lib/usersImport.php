<?
/**
 * User: shakinm@gmail.com
 * Date: 22.03.2017
 * Time: 11:07
 */
define('BX_SESSION_ID_CHANGE', false);
define('BX_SKIP_POST_UNQUOTE', true);
define('NO_AGENT_CHECK', true);
define("STATISTIC_SKIP_ACTIVITY_CHECK", true);

$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

function ZipStatusString( $status )
{
    switch( (int) $status )
    {
        case ZipArchive::ER_OK           : return 'N No error';
        case ZipArchive::ER_MULTIDISK    : return 'N Multi-disk zip archives not supported';
        case ZipArchive::ER_RENAME       : return 'S Renaming temporary file failed';
        case ZipArchive::ER_CLOSE        : return 'S Closing zip archive failed';
        case ZipArchive::ER_SEEK         : return 'S Seek error';
        case ZipArchive::ER_READ         : return 'S Read error';
        case ZipArchive::ER_WRITE        : return 'S Write error';
        case ZipArchive::ER_CRC          : return 'N CRC error';
        case ZipArchive::ER_ZIPCLOSED    : return 'N Containing zip archive was closed';
        case ZipArchive::ER_NOENT        : return 'N No such file';
        case ZipArchive::ER_EXISTS       : return 'N File already exists';
        case ZipArchive::ER_OPEN         : return 'S Can\'t open file';
        case ZipArchive::ER_TMPOPEN      : return 'S Failure to create temporary file';
        case ZipArchive::ER_ZLIB         : return 'Z Zlib error';
        case ZipArchive::ER_MEMORY       : return 'N Malloc failure';
        case ZipArchive::ER_CHANGED      : return 'N Entry has been changed';
        case ZipArchive::ER_COMPNOTSUPP  : return 'N Compression method not supported';
        case ZipArchive::ER_EOF          : return 'N Premature EOF';
        case ZipArchive::ER_INVAL        : return 'N Invalid argument';
        case ZipArchive::ER_NOZIP        : return 'N Not a zip archive';
        case ZipArchive::ER_INTERNAL     : return 'N Internal error';
        case ZipArchive::ER_INCONS       : return 'N Zip archive inconsistent';
        case ZipArchive::ER_REMOVE       : return 'S Can\'t remove file';
        case ZipArchive::ER_DELETED      : return 'N Entry has been deleted';
        
        default: return sprintf('Unknown status %s', $status );
    }
}

//$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../..//upload/1c_custom_exchange/';
// $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'].'/upload/1c_user_old/import.xml';
// set_include_path(get_include_path() . PATH_SEPARATOR . $DOCUMENT_ROOT);
date_default_timezone_set('Europe/Moscow');

$zip_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Contragents.zip';
$file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Contragents.xml';
$dest_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange';

$logFile = "/_logs/usersImport/".date('Y-m-d').'.txt';

//exec("unzip ".$zip_file_path, $result, $returnval);
/*exec("tar xvf ".$zip_file_path." -C ".$dest_file_path, $result, $returnval);
print_r($result);
print_r($returnval);
exit;
*/
global $DB;

if(file_exists($zip_file_path)) {
	$zip = new ZipArchive;
	$res = $zip->open($zip_file_path);

	if ($res !== true) {
		echo "Ошибка открытия архива: ".$zip_file_path;
	} else {
		//var_dump($zip->numFiles);
		$extracted = $zip->extractTo($dest_file_path);
		//var_dump($extracted, $zip->status, ZipStatusString($zip->status));
		$zip->close();
		
		if($extracted) {
			logToFile($logFile, "Распаковка архива прошла успешно. Продолжаем обработку...");
		} else {
			var_dump($extracted, $zip->status, ZipStatusString($zip->status));
			logToFile($logFile, "Ошибка распаковки. Прерываем обработку...");
			throw new Exception('Ошибка распаковки.');
		}
		
	}
} else {
	throw new Exception('Архив не найден.');
}


if (isset($file_path) && file_exists($file_path)) {
	logToFile($logFile, "Файл {$file_path} в последний раз был изменен: " . date("F d Y H:i:s.", filectime($file_path)));
} else {
	logToFile($logFile, 'XML файл '.$file_path.' не найден.');
	throw new Exception('XML файл '.$file_path.' не найден.');
}
//exit;
try {
	$time_start = microtime(true);

	$feed = file_get_contents($file_path);
	// var_dump($feed);
	$xml_string = simplexml_load_string($feed);
	$errors = libxml_get_errors();

	foreach ($errors as $error) {
		throw new Exception(display_xml_error($error, $xml_string));
	}
	libxml_clear_errors();

	$startMemory = memory_get_usage();
	$contragentCount = 0;
	foreach ($xml_string->Контрагенты as $values) {

		$allContragent = 0;
		foreach ($values->Контрагент as $usersContragentCount) {
			$allContragent++;
		}
		logToFile($logFile, "Всего содержится контрагентов {$allContragent}");
		// var_dump($allContragent);

		$attributes = $values->attributes();
		if ($attributes['СодержитТолькоИзменения'] && 1) {
			logToFile($logFile, "В файле присутствуют изменения {$file_path}");
			foreach ($values->Контрагент as $value) {
				$contragentCount++;

				// если XML_ID не пуст
				if ($value->Ид !== "") {
					$cUser = new CUser;

					$xml_id = trim($value->Ид);

					$arFields = array(
						"LOGIN" => trim($value->Login)
					);
					$login = $arFields['LOGIN'];
					//logToFile($logFile, "В XML пользователь с логином: {$arFields['LOGIN']}, XML_ID: {$xml_id} найден");

					$arFilter = Array(
						  "LOGIC"=>"OR",
						  Array(
							 "EMAIL" => $login
						  ),
						  Array(
							 "LOGIN" => $login
						  ),
						  Array(
							 "XML_ID" => $xml_id
						  )
					);
					//$arUser = CUser::GetList(($by = ""), ($order = ""), $arFilter);
					$arUser = CUser::GetList(($by = ""), ($order = ""), array("XML_ID" => $xml_id));

					$arXMLFields = parse($value, $arFields);
					$arXMLFields['NAME'] = trim($value->Наименование);
					$arXMLFields['XML_ID'] = trim($xml_id);
					
					$rUser = $arUser->Fetch();
					
					if(!$rUser) {
						$arUser = CUser::GetList(($by = ""), ($order = ""), array("LOGIN" => $login));
						//$arUser = CUser::GetByLogin($login);
						
						$rUser = $arUser->Fetch();
					}
						
					// Если пользователь c XML_ID найден
					if ($rUser) {
						
						print_r([
							$login,
							
							$xml_id,
							
							$rUser
						]); //continue;
						
						// var_dump("Пользователь XML_ID: ".$rUser['XML_ID']." в базе битрикс c ID:".$rUser['ID']. " найден, идёт обновление данных в таблице битрикс");
						logToFile($logFile, "Пользователь XML_ID: {$rUser['XML_ID']} в базе битрикс c ID: {$rUser['ID']} найден, идёт обновление данных в таблице битрикс");

						

						//В случае успеха обновления, добавляем HASH в old_user
						if ($cUser->Update($rUser['ID'], $arXMLFields)) {
							// unset($arXMLFields); // очищаем массив от лишних полей
							logToFile($logFile, "Данные пользователя ID:{$rUser["ID"]} обновлены");
							
							$from_old = "FROM old_users where LOGIN='$login';";
							$rsOLDUser = $DB->Query("SELECT * $from_old");
							
							$arOLDUser = $rsOLDUser->GetNext();
							
							$arFields['HASH'] = trim($value->Hash);
							
							if(!$arOLDUser) {
								
								__afterUserAdd($arFields);
								logToFile($logFile, "HASH пользователя с ID:{$rUser["ID"]} добавлен в old_user");
							} else {
					      // var_dump("Пользователь уже существует в таблице old_users ".$arOLDUser['LOGIN']." XML_ID: ".$xml_id);
								logToFile($logFile, "Пользователь уже существует в таблице old_users {$login} XML_ID: {$xml_id}. Обновляем хэш.");
						
								$hash = $arFields['HASH'] ;
								$DB->Query("UPDATE old_users SET HASH = '$hash' where LOGIN='$login'");
							}
						} else {
							logToFile($logFile, "ОШИБКА. При обновлении профиля пользователя {$arOLDUser['ID']}. ".$cUser->LAST_ERROR);
						}
					} else {
						$password = md5(time());
						$addFields = Array(
						  "NAME"              => strip_tags($arXMLFields['NAME']),
						  //"LAST_NAME"         => "Иванов",
						  "EMAIL"             => $login,
						  "LOGIN"             => $login,
						  "ACTIVE"            => "Y",
						  "GROUP_ID"          => array(2,5),
						  "PASSWORD"          => $password,
						  "CONFIRM_PASSWORD"  => $password,
                            "XML_ID" => $xml_id
						);

						$ID = $cUser->Add($addFields);
						if (intval($ID) > 0) {
							logToFile($logFile, "Добавлен новый пользователь с логином: {$login} и ID: {$ID}. XML_ID: {$xml_id}\n");
							
							// Ищем пользователя с таким логином в "old_users"
							
							// var_dump($login);
							$from_old = "FROM old_users where LOGIN='$login';";
							$rsOLDUser = $DB->Query("SELECT * $from_old");

							if($arOLDUser = $rsOLDUser->GetNext()) {
							  // var_dump("Пользователь уже существует в таблице old_users ".$arOLDUser['LOGIN']." XML_ID: ".$xml_id);
								logToFile($logFile, "Совпадение логина {$login} в таблице old_users. XML_ID: {$xml_id}\n");
								$hash = trim($value->Hash);
								$DB->Query("UPDATE old_users SET HASH = '$hash' where LOGIN='$login'");
							} else {
							  // var_dump("Совпадений не найдено по ".$arFields['LOGIN'].". Добавляем в old_user");
									logToFile($logFile, "Совпадений не найдено по {$login}, идёт добавление в таблицу old_users\n");

									$arFields['HASH'] = trim($value->Hash);
									__afterUserAdd($arFields);
									// var_dump("Пользователь ".$arFields['LOGIN']." добавлен в old_user");
									logToFile($logFile, "Пользователь {$login} успешно добавлен в таблицу old_users");
									
							}
						} else {
							logToFile($logFile, "Произошла ошибка при добавлении нового пользователя с логином: {$login}. XML_ID: {$xml_id}\nОшибка: {$cUser->LAST_ERROR}");
							// var_dump($cUser->LAST_ERROR);
						}						
					}
					
					unset($arXMLFields);
					unset($arFields);
					unset($cUser);
				}
			}
			
			break;
		} else {
			logToFile($logFile, "XML не содержит изменений.");
			exit;
		}
	}

	logToFile($logFile, "Обработано: ".$contragentCount);

	$time_end = microtime(true);
	$execution_time = ($time_end - $time_start);
	
} catch (Exception $e) {
	echo($e);

	logToFile($logFile, $e->getMessage());
	logToFile($logFile, $e);
}

function parse ($values, $fields) {
	// print_r($values);
	// print_r($fields);
	if ($values->АдресРегистрации->АдресноеПоле) {
		foreach ($values->АдресРегистрации->АдресноеПоле as $adress) {
			switch ($adress->Тип) {
				case 'Почтовый индекс':
					logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
					$fields['PERSONAL_ZIP'] = trim($adress->Значение);
					break;
				case 'Страна':
					logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
					$fields['PERSONAL_COUNTRY'] = "1";
					break;
				case 'Регион':
					logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
					$fields['PERSONAL_STATE'] = trim($adress->Значение);
					break;
				case 'Город':
					logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
					$fields['PERSONAL_CITY'] = trim($adress->Значение);
					break;
				case 'Улица':
					logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
					$fields['PERSONAL_STREET'] = trim($adress->Значение);
					break;
				
				default:
					throw new Exception(display_xml_error($errors, $xml));
					break;
			}
		}
	}

	if ($values->Контакты->Контакт) {
		foreach ($values->Контакты->Контакт as $contact) {
			switch ($contact->Тип) {
				case 'Телефон рабочий':
					logToFile($logFile, "Обработан: '".$contact->Тип."' и добавлен в массив");
					$fields['PERSONAL_PHONE'] = trim($contact->Значение);
					break;
				case 'Электронная почта':
					logToFile($logFile, "Обработан: '".$contact->Тип."' и добавлен в массив");
					$fields['PERSONAL_MAILBOX'] = trim($contact->Значение);
					break;
				case 'Факс':
					logToFile($logFile, "Обработан: '".$contact->Тип."' и добавлен в массив");
					$fields['PERSONAL_FAX'] = trim($contact->Значение);
					break;
				default:
					throw new Exception(display_xml_error($errors, $xml));
					break;
			}
		}
	}

	return $fields;
}

// function updateHashPassword ($login, $hashXmlPassword) {
// 	global $logFile;
// 	global $rUser;

// 	if ($hashXmlPassword == "") {
// 		return logToFile($logFile, "Пустой HASH у {$login}");
// 	}

// 	logToFile($logFile, "Обновление пароля у {$rUser["ID"]}");

// 	$password = $rUser["PASSWORD"];
// 	$xmlHash = $hashXmlPassword;
// 	// print_r((string) $password. "\n");
// 	// print_r($xmlHash);

// 	// Если HASH из XML длиннее 32символов
// 	if (strlen($xmlHash) > 32) {
// 		$salt = substr($xmlHash, 0, strlen($xmlHash) - 32);
// 		$db_password = substr($xmlHash, -32);
// 		// print_r('<pre>'.$db_password.'</pre>');
// 	} else {
// 		$salt = "";
// 		$db_password = $xmlHash;
// 	}

// 	$user_password = md5($salt.$password);
// 	  // print_r($user_password. "\n");
// 	$passwordCorrect = ($db_password === $user_password) || (strtolower($db_password) === strtolower($user_password));
// 		// print_r('pwoefmpo');
// 	  print_r($passwordCorrect. "\n");

// 	// Если HASHы совпадают
// 	if ($passwordCorrect) {
// 		// Обновляем пароль пользователя в базе Битрикса
// 		logToFile($logFile, "Обновление пароля у пользователя с ИД: {$rUser["ID"]}  завершено");
// 		$bUbdate = $cUser->Update($rUser["ID"], array("PASSWORD" => $password));
// 	}
// }

/**
 * @param $error
 * @param $xml
 * @return string
 */
function display_xml_error($error, $xml) {
  $return = $xml[$error->line - 1] . "\n";
  $return .= str_repeat('-', $error->column) . "^\n";

  switch ($error->level) {
    case LIBXML_ERR_WARNING:
      $return .= "Warning $error->code: ";
      break;
    case LIBXML_ERR_ERROR:
      $return .= "Error $error->code: ";
      break;
    case LIBXML_ERR_FATAL:
      $return .= "Fatal Error $error->code: ";
      break;
  }

  $return .= trim($error->message) .
    "\n  Line: $error->line" .
    "\n  Column: $error->column";

  if ($error->file) {
    $return .= "\n  File: $error->file";
  }

  return "$return\n\n--------------------------------------------\n\n";
}

// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_after.php");
?>