<?php


$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../';
//$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../upload/1c_custom_exchange/';
//set_include_path(get_include_path() . PATH_SEPARATOR . $DOCUMENT_ROOT);
date_default_timezone_set('Europe/Moscow');

$logFile = "/_logs/priceImport/".date('Y-m-d').'.txt';

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$zip_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Vigr.zip';
//$file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Vigr.xml.new';
$file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Vigr.xml';
$dest_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange';

ini_set('memory_limit', '512M');

/*
if(file_exists($zip_file_path)) {
	$zip = new ZipArchive;
	$res = $zip->open($zip_file_path);

	if ($res !== true) {
		echo "Ошибка открытия архива: ".$zip_file_path;
	} else {
		//var_dump($zip->numFiles);
		$extracted = $zip->extractTo($dest_file_path);
		//var_dump($extracted, $zip->status, ZipStatusString($zip->status));
		$zip->close();
		
		if($extracted) {
			logToFile($logFile, "Распаковка архива прошла успешно. Продолжаем обработку...");
		} else {
			var_dump($extracted, $zip->status, ZipStatusString($zip->status));
			logToFile($logFile, "Ошибка распаковки. Прерываем обработку...");
			throw new Exception('Ошибка распаковки.');
		}
		
	}
} else {
	throw new Exception('Архив не найден.');
}

*/

if (isset($file_path) && file_exists($file_path)) {
	logToFile($logFile, "Файл {$file_path} в последний раз был изменен: " . date("F d Y H:i:s.", filectime($file_path)));
} else {
	logToFile($logFile, 'XML файл '.$file_path.' не найден.');
	throw new Exception('XML файл '.$file_path.' не найден.');
}

try {
	$time_start = microtime(true);

	if (!CModule::IncludeModule('highloadblock') || !CModule::IncludeModule('catalog') || !CModule::IncludeModule('iblock')) {
		logToFile($logFile, 'Ошибка загрузки модуля catalog');
		throw new Exception('Ошибка загрузки модуля catalog');
	}


	// if (!isset($argv[1])) {
	// 	logToFile($logFile, 'XML файл  c ценами не найден.');
	//   throw new Exception('XML файл  c ценами не найден.');
	// } else {

	//   $file_path = base64_decode($argv[1]);
	//   if (!file_exists($file_path)) {
	// 		logToFile($logFile, 'XML файл  c ценами не найден.');
	//     throw new Exception('XML файл  c ценами не найден.');
	//   }
	// }

	$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
		array(
			"filter" => array(
				'TABLE_NAME' => 'hl_client_price'
			)
		)
	)->fetch();

  // var_dump($hlblock['ID']);

	if (isset($hlblock['ID'])) {
		$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();

		$time_start = microtime(true);
		
		$startMemory = memory_get_usage();
		$iAll = $iDelete = $iAdd = $iUpd = $iNotFind = $iNotChanged = $counter = $iKontragentNotFound = 0;

		$arrElements = getElements();
		// var_dump($arrElements);
		$arrElementsById = $arrElements['byId'];
		$arrElementsByXml = $arrElements['byXML'];
		$arrUsers = getUser();

		// logToFile($logFile, $arrUsers);// exit;

		logToFile($logFile, "Список пользователей с кодами контрагентов");
		logToFile($logFile, $arrUsers);

		//Ставим у каждого договора и товара галочку Удалить. Чтобы удалить договор, если дальнейший код не изменит галочку на другую.
		global $DB;
		$DB->Query("UPDATE hl_client_price SET UF_DELETE = 1");

		//var_dump($xml->kont);
			
			
		$xml = new XMLReader();
		$xml->open($file_path);
		 
		// moving to first `kont` tag.
		while($xml->read() && $xml->name != 'kont')
		{
			;
		}
		
		while($xml->name == 'kont')
		{
			$element = new SimpleXMLElement($xml->readOuterXML());

			$user_xml_id = strval($element->attributes()->kod);
			
			logToFile($logFile, "В XML файле пользователь с кодом: ".$user_xml_id);

			$currentUserId = isset($arrUsers[$user_xml_id], $arrUsers[$user_xml_id]["ID"]) && $arrUsers[$user_xml_id]["ID"] ? $arrUsers[$user_xml_id]["ID"] : NULL;
			
			//echo "<pre>";
			print_r(compact('user_xml_id','currentUserId'));
			//print_r($user_xml_id);
			print_r("\n");
			// print_r($contract_id);
			//echo "</pre>";

			if ($currentUserId && 1) {
				logToFile($logFile, "Пользователь с ID = {$currentUserId} и XML_ID = {$user_xml_id} найден - обрабатываем");


				$rsData = $entity_data_class::getList(array(
				  'select' => array('*'),
				  'filter' => array(
					'UF_USER_ID' => $currentUserId,
					//'UF_CONTRACT_ID' => $contract_id,
				  )
				));

				$productIndex = 0;

				$arrUserPrice = array();

				while ($el = $rsData->fetch()) {
					if ($arrElementsById[$el['UF_PRODUCT_ID']]) {
						$arrUserPrice[$arrElementsById[$el['UF_PRODUCT_ID']]] = array(
							'ID' => $el['ID'],
							'UF_PRICE' => $el['UF_PRICE'],

							'UF_USER_ID' => $el['UF_USER_ID'],
							//'UF_CONTRACT_ID' => $el['UF_CONTRACT_ID']
						);
					}
				}

				logToFile($logFile, "По пользователю найдены следующие товары в БД:");
				logToFile($logFile, $arrUserPrice);
				
				
				
				foreach($element->nomenklatura as $item) {
					
					$element_code = strval($item->attributes()->kod);
					
					foreach($item->offer as $offer) {
						
						print_r(array(
							'code' => $element_code,
							'price' => strval($offer->attributes()->cena),
							'GUID' => strval($offer->attributes()->GUID),
						));
						

						$element_xml_id = trim(strval($offer->attributes()->GUID));
						$element_price = (double)preparePrice(strval($offer->attributes()->cena));

						logToFile($logFile, array($element_xml_id, $element_price));

						$result = false;

						$productIndex++;

						$iAll++;

						if (!$arrElementsByXml[$element_xml_id]) {
							logToFile($logFile, $productIndex.". Товар с XML_ID = {$element_xml_id}, код {$element_code} не найден.");
							$iNotFind++;
							continue;
						}
					
						
						/*
						 * Проверяем: есть - обновляем, нет - добавляем
						 */
						 
						if (isset($arrUserPrice[$element_xml_id])
							//&& ($arrUserPrice[$element_xml_id]['UF_CONTRACT_ID'] == $contract_id)
						) {
							// Обновляем цену.
							if($arrUserPrice[$element_xml_id]['UF_PRICE'] != $element_price) {
								logToFile($logFile, $productIndex.". Товар с ID = {$arrElementsByXml[$element_xml_id]}, XML_ID = {$element_xml_id}, код {$element_code}, цена {$element_price} - обновляем цену.");
								
								$result = $entity_data_class::update(
									$arrUserPrice[$element_xml_id]['ID'],
									array(
										'UF_PRICE' => $element_price,
										'UF_DELETE' => 0,
										//'UF_CONTRACT_ID' => $contract_id,
									)
								);
								if ($result) {
									$iUpd++;
								}
							} else {
								logToFile($logFile, $productIndex.". Товар с ID = {$arrElementsByXml[$element_xml_id]}, XML_ID = {$element_xml_id}, код {$element_code}, цена {$element_price} - цена не изменилась.");
								
								// снимаем флаг удаления.
								$result = $entity_data_class::update(
									$arrUserPrice[$element_xml_id]['ID'],
									array(
										'UF_DELETE' => 0,
										//'UF_CONTRACT_ID' => $contract_id,
									)
								);
								if ($result) {
									$iNotChanged++;
								}
							}
						} elseif ($arrElementsByXml[$element_xml_id] && !isset($arrUserPrice[$element_xml_id])) {
							logToFile($logFile, $productIndex.". Товар с ID = {$arrElementsByXml[$element_xml_id]}, XML_ID = {$element_xml_id}, код {$element_code}, цена {$element_price} - добавляем в БД.");
							
							$result = $entity_data_class::add(array(
								'UF_USER_ID' => $currentUserId,
								'UF_PRODUCT_ID' => $arrElementsByXml[$element_xml_id],
								//'UF_CONTRACT_ID' => $contract_id,
								'UF_PRICE' => $element_price,
								'UF_DELETE' => 0,
							));

							if ($result) {
								$iAdd++;
							}
						} else {
							logToFile($logFile, $productIndex.". Товар с XML_ID = {$element_xml_id}, код {$element_code} - WTF??? В БД есть, а не добавился...");
						}
					}
				}

			} else {
				$iKontragentNotFound++;
				logToFile($logFile, "Пользователь с кодом {$user_xml_id} НЕ найден - пропускаем.");
			}

			//print "\n";
			//$countIx++;

			$xml->next('kont');
			unset($element);
		}
		 
		print "Number of items=$countIx\n";
		print "memory_get_usage() =" . memory_get_usage()/1024 . "kb\n";
		print "memory_get_usage(true) =" . memory_get_usage(true)/1024 . "kb\n";
		print "memory_get_peak_usage() =" . memory_get_peak_usage()/1024 . "kb\n";
		print "memory_get_peak_usage(true) =" . memory_get_peak_usage(true)/1024 . "kb\n";
		 
		print "custom memory_get_process_usage() =" . memory_get_process_usage() . "kb\n";
		 
		 
		$xml->close();		
		
	
		//Удаляем все товары с флагом удаления.
		$deleted = $DB->Query("DELETE FROM hl_client_price WHERE UF_DELETE = 1");
		$iDelete += $deleted->AffectedRowsCount();

		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start);

		CEventLog::Add(array(
			"SEVERITY" => "SECURITY",
			"AUDIT_TYPE_ID" => '1C_IMPORT_PRICE',
			"MODULE_ID" => "main",
			"ITEM_ID" => 'integration/lib/priceImport.php',
			//"DESCRIPTION" => "Добавлено {$iAdd}, обновлено {$iUpd} из {$iAll} за {$execution_time} сек.",
			"DESCRIPTION" => "Всего в выгрузке товаров: {$iAll}, из них добавлено: {$iAdd}, обновлено {$iUpd}, без изменений {$iNotChanged}, удалено: {$iDelete}, не найдено: {$iNotFind}. Контрагентов не найдено: {$iKontragentNotFound} за {$execution_time} сек.",
		));
	
		logToFile($logFile, "Всего в выгрузке товаров: {$iAll},\n из них добавлено: {$iAdd},\n обновлено {$iUpd},\n без изменений {$iNotChanged},\n удалено: {$iDelete},\n не найдено: {$iNotFind}. \nКонтрагентов не найдено: {$iKontragentNotFound} за {$execution_time} сек.");
	} else {
		logToFile($logFile, 'Не найден HlBlock с ценами...');
		throw new Exception('Не найден HlBlock с ценами...');
	};
} catch (Exception $e) {

	echo($e);

	logToFile($logFile, $e->getMessage());
	logToFile($logFile, $e);
	
	//    TODO не забыть раскомментить перед деплоем!
	CEventLog::Add(array(
		"SEVERITY" => "SECURITY",
		"AUDIT_TYPE_ID" => '1C_IMPORT_PRICE',
		"MODULE_ID" => "main",
		"ITEM_ID" => 'integration/lib/priceImport.php',
		"DESCRIPTION" => $e->getMessage(),
	));
}

/**
 * @param $price string
 * @return float
 */
function preparePrice($price) {
	$result = str_replace(array(',',' '), array('.',''), $price);
	return $result;
}

/**
 * Выборка всех элементов для сопоставления ID и XML_ID
 * @return mixed
 */
function getElements() {
  $rows = [];

  $arFilter = array(
    'IBLOCK_ID' => [16, 17],
    // 'IBLOCK_ID' => 1,
    'ACTIVE' => 'Y',
    '!XML_ID' => false,
  );

  $rsElement = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "XML_ID"));
  while ($obElement = $rsElement->Fetch()) {
		// торговые предложения тоже нужны. 
		if (stripos($obElement['XML_ID'], '#') && 0) {
			$xmlId = substr($obElement['XML_ID'], 0, stripos($obElement['XML_ID'], '#'));
		} else {
			$xmlId=$obElement['XML_ID'];
		}

		$rows['byId'][$obElement['ID']] = $xmlId;
		$rows['byXML'][$xmlId] = $obElement['ID'];
  }
  unset($rsElement);
  unset($obElement);

  return $rows;
}

/**
 * Выборка всех пользователей для сопоставления ID и XML_ID
 * @return mixed
 */
function getUser() {
  $rows = [];
  $cUser = new CUser;
  $arFilter = array(
    '!XML_ID' => false,
    "ACTIVE" => 'Y',
  );
  $arSelect = array("SELECT" => array("XML_ID","UF_BASEPRICE"));
  $dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter, $arSelect);
  // $dbUsers = $cUser->GetList($sort_by="", $sort_ord="", $arFilter, $arSelect);
  while ($arUser = $dbUsers->Fetch()) {
    $rows[$arUser['XML_ID']] = array(
			"ID" => $arUser["ID"],
			// "UF_BASEPRICE" => $arUser["UF_BASEPRICE"],
			"NAME" => $arUser["NAME"],
			"EMAIL" => $arUser["EMAIL"],
			"XML_ID" => $arUser["XML_ID"],
			// "UF_XML_ID" => $arUser["UF_XML_ID"],
		);
  }
  //var_dump($rows);
  return $rows;
}

function memory_get_process_usage()
{
	$status = file_get_contents('/proc/' . getmypid() . '/status');
	
	$matchArr = array();
	preg_match_all('~^(VmRSS|VmSwap):\s*([0-9]+).*$~im', $status, $matchArr);
	
	if(!isset($matchArr[2][0]) || !isset($matchArr[2][1]))
	{
		return false;
	}
	
	return intval($matchArr[2][0]) + intval($matchArr[2][1]);
}