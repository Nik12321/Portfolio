<?
require_once($_SERVER['DOCUMENT_ROOT'] ."/bitrix/modules/main/include/prolog_before.php");
if(isset($_GET["kod"], $_GET["hash"], $_GET["type"])) {
    $rsUsers = CUser::GetList(
        ($by = "NAME"),
        ($order = "desc"),
        Array("UF_CONTRAGENT_CODE" => $_GET["kod"])
    );
    if ($arUser = $rsUsers->Fetch()) {
        if(isset($arUser["EMAIL"])) {
            if($_GET["hash"] == strtoupper(md5(strval($_GET["kod"])+strtoupper($arUser["EMAIL"])))) {
                $file_name;
                $file_path;
                if ($_GET["type"]=='All' || $_GET["type"]=='InStock') {                
                    $file_name = 'Price'.$_GET["kod"].'.zip';
                    $file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Prices/'.$_GET["type"].'/'.$file_name;      
                    if(isset($file_path, $file_name)) {
                        if (file_exists($file_path) && is_file($file_path)) {
                            header('Content-Description: File Transfer');
                            header('Content-type: application/zip');
                            header('Content-Disposition: attachment; filename="'.$file_name.'"');
                            header('Content-Transfer-Encoding: binary');
                            header('Expires: 0');
                            header('Cache-Control: private');
                            header('Pragma: public');
                            header('Content-Length: '.filesize($file_path));
                            ob_clean(); 
                            readfile($file_path);
                        } else{
                            echo 'Не удалось получить файл!';
                        }
                    } else {
                        echo 'Файл с прайс-листом отсутствует';
                    }
                } else {
                    echo "Неверный тип прайс-листа";
                    die();
                } 
            } else{
                echo "Указан не верный хэш";
                die();
            }
        } else {
            echo "Пользователь не найден";
            logToFileEvent('/_logs/download-prices.log', $rsUsers);
            die();
        }
    }
}    
require_once($_SERVER['DOCUMENT_ROOT'] ."/bitrix/modules/main/include/prolog_after.php");?>
