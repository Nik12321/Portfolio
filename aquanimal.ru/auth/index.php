<?
define('NEED_AUTH', true);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');

$APPLICATION->SetTitle('AQUANIMAL Авторизация');
global $USER;
if ($USER->IsAuthorized()) {
    LocalRedirect("/catalog/");
}
?>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>