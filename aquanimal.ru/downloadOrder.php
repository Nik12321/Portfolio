<?
require_once($_SERVER['DOCUMENT_ROOT'] ."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
if(CUser::IsAuthorized()) {
    $arProps = getUsers(CUser::GetID());
    if(isset($_GET["id"],$_GET["filename"])) {
        $arOrder = CSaleOrder::GetByID($_GET["id"]);
        if ($arProps['ID'] == $arOrder['USER_ID']){
            $file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/orders_docs/'.$_GET["id"].'/'.$_GET["filename"];
            if(isset($file_path)) {
                if (file_exists($file_path) && is_file($file_path)) {
                    header('Content-Description: File Transfer');
                    header('Content-type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="'.$_GET["filename"].'"');
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: private');
                    header('Pragma: public');
                    header('Content-Length: '.filesize($file_path));
                    ob_clean();
                    readfile($file_path);
                } else{
                    echo 'Не удалось получить файл!';
                }

            } else {
                echo 'Файл для этого заказа отсутствует';
            }
        }else{
            echo 'ID заказчика и ID данного пользователя не совпадают. Доступ закрыт.';
        }

    }else{
        echo "Файл не найден";
    }
} else {
    echo "Вы не авторизованы. Авторизуйтесь и повторите попытку.";
    die();
}
?>