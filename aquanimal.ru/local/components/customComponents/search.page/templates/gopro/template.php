<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

?><div class="search_page clearfix"><?
	?><form action="" method="get" class="form_search"><?
		?><input type="hidden" name="tags" value="<?echo $arResult['REQUEST']['TAGS']?>" /><?
		?><input type="text" class="q" name="q" value="<?=$arResult['REQUEST']['QUERY']?>" size="40" /><?
		?><input class="btn btn1" type="submit" class="search_94" value="<?=GetMessage('SEARCH_GO')?>" /><?
		?><input type="hidden" name="how" value="<?=$arResult['REQUEST']['HOW']=='d'? 'd': 'r'?>" /><?
	?></form><?
?></div><?

if($arResult['ERROR_CODE']!=0)
{
	ShowError($arResult['ERROR_TEXT']);
	?><p><?=GetMessage('SEARCH_CORRECT_AND_CONTINUE')?></p><br /><br /><?
} elseif(count($arResult['SEARCH'])>0)
{
	?><div class="spage"><?
		////////////////// IBLOCKS
		if(!empty($arResult["EXT_SEARCH"]["IBLOCK"]["IBLOCKS"]))
		{
			foreach($arResult["EXT_SEARCH"]["IBLOCK"]["IBLOCKS"] as $iblock_id => $arIblock)
			{
				// catalog
				if(in_array($iblock_id,$arParams['IBLOCK_ID']))
				{
					global $arrSearchFilter;
					$arIds = array();
					foreach($arResult['EXT_SEARCH']['IBLOCK']['ITEMS'][$iblock_id] as $arItem)
					{
						if( $arItem['ITEM_ID']!=$arItem['ID'] && IntVal($arItem['ITEM_ID'])>0 )
						{
							$arIds[] = $arItem['ITEM_ID'];
						}
					}
					if( is_array($arIds) && count($arIds)>0 )
					{
						?><div class="iblock"><?
							?><div class="title"><h3><?=$arIblock["NAME"]?></h3></div><?
							$arrSearchFilter = array('ID'=>$arIds);
							$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"gopro", 
	array(
		"IBLOCK_TYPE" => "1c_catalog_new",
		"IBLOCK_ID" => "16",
		"ELEMENT_SORT_FIELD" => "shows",
		"ELEMENT_SORT_ORDER" => "desc",
		"PROPERTY_CODE" => array(
			0 => "CML2_ARTICLE",
			1 => "BRAND",
			2 => "CML2_ATTRIBUTES",
			3 => $arParams["PROPCODE_MORE_PHOTO"],
			4 => "",
		),
		"META_KEYWORDS" => "",
		"META_DESCRIPTION" => "",
		"BROWSER_TITLE" => "-",
		"INCLUDE_SUBSECTIONS" => "Y",
		"BASKET_URL" => "",
		"ACTION_VARIABLE" => "",
		"PRODUCT_ID_VARIABLE" => "",
		"SECTION_ID_VARIABLE" => "",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"FILTER_NAME" => "arrSearchFilter",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "0",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"DISPLAY_COMPARE" => "N",
		"PAGE_ELEMENT_COUNT" => "10",
		"LINE_ELEMENT_COUNT" => "",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "N",
		"PRICE_VAT_INCLUDE" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "0",
		"PAGER_SHOW_ALL" => "N",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "NAME",
			1 => $arParams["OFFERS_FIELD_CODE"],
			2 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => $arParams["OFFERS_PROPERTY_CODE"],
			2 => "",
		),
		"OFFERS_SORT_FIELD" => "shows",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_LIMIT" => "0",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"CONVERT_CURRENCY" => "N",
		"CURRENCY_ID" => $arParams["CURRENCY_ID"],
		"BY_LINK" => "Y",
		"COMPONENT_TEMPLATE" => "gopro",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"BACKGROUND_IMAGE" => "-",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
			0 => "CML2_ATTRIBUTES",
		),
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"COMPATIBLE_MODE" => "Y",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"CUSTOM_FILTER" => "",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"VIEW" => "",
		"PROP_MORE_PHOTO" => "-",
		"PROP_ARTICLE" => "-",
		"PROP_ACCESSORIES" => "-",
		"USE_FAVORITE" => "Y",
		"USE_SHARE" => "Y",
		"SHOW_ERROR_EMPTY_ITEMS" => "Y",
		"EMPTY_ITEMS_HIDE_FIL_SORT" => "Y",
		"DONT_SHOW_LINKS" => "N",
		"USE_AUTO_AJAXPAGES" => "N",
		"OFF_MEASURE_RATION" => "N",
		"USE_STORE" => "Y",
		"LIST_SKU_VIEW" => "",
		"COLUMNS5" => "N",
		"COL_XS_6" => "N",
		"STICKERS_PROPS" => array(
		),
		"STICKERS_DISCOUNT_VALUE" => "N",
		"OFF_HOVER_POPUP" => "N",
		"USE_LAZYLOAD" => "Y",
		"USE_RATING" => "Y",
		"PROP_SKU_MORE_PHOTO" => "-",
		"PROP_SKU_ARTICLE" => "-",
		"PROPS_ATTRIBUTES" => array(
		),
		"PROPS_ATTRIBUTES_COLOR" => array(
		),
		"USE_MIN_AMOUNT" => "Y",
		"MIN_AMOUNT" => "",
		"MAIN_TITLE" => "",
		"HIDE_IN_LIST" => "N",
		"PROP_STORE_REPLACE_SECTION" => "0",
		"RATING_PROP_COUNT" => "-",
		"RATING_PROP_SUM" => "-",
		"HIDE_AJAXPAGES_LINK" => "N"
	),
	false
);
						?></div><?
					}
				}
			}
			foreach($arResult["EXT_SEARCH"]["IBLOCK"]["IBLOCKS"] as $iblock_id => $arIblock)
			{
				// catalog
				if(in_array($iblock_id,$arParams['IBLOCK_ID']))
				{
					$c = false;
					foreach($arResult['EXT_SEARCH']['IBLOCK']['ITEMS'][$iblock_id] as $arItem)
					{
						if( IntVal($arItem['ITEM_ID'])<1 )
						{
							$c = true;
							break;
						}
					}
					if($c)
					{
						?><div class="iblock"><?
							?><div class="title"><h3><?=$arIblock["NAME"]?></h3></div><?
							foreach($arResult['EXT_SEARCH']['IBLOCK']['ITEMS'][$iblock_id] as $arItem)
							{
								if( IntVal($arItem['ITEM_ID'])<1 )
								{
									?><div class="sitem"><?
										?><div class="name"><a href="<?=$arItem['URL']?>"><?=$arItem['TITLE_FORMATED']?></a></div><?
										?><div class="description"><?=$arItem['BODY_FORMATED']?></div><?
										?><div class="chain"><?=$arItem['CHAIN_PATH']?></div><?
									?></div><?
								}
							}
						?></div><?
					}
				}
			}
			
		}
		
	
	if($arParams['DISPLAY_BOTTOM_PAGER'] != 'N'){
		echo '<br />'.$arResult['NAV_STRING'];
	}
}
else{
	ShowNote(GetMessage('SEARCH_NOTHING_TO_FOUND'));
};