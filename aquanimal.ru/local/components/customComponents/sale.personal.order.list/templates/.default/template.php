<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

CJSCore::Init(array('clipboard', 'fx'));
$this->addExternalCss("/bitrix/css/main/bootstrap.css");

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
Loc::loadMessages(__FILE__);
if (!empty($arResult['ERRORS']['FATAL'])) {
    foreach ($arResult['ERRORS']['FATAL'] as $error) {
        ShowError($error);
    }
    $component = $this->__component;
    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }

}
else {
    if (!empty($arResult['ERRORS']['NONFATAL'])) {
        foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
            ShowError($error);
        }
    }
    if (!count($arResult['ORDERS'])) {
        if ($_REQUEST["filter_history"] == 'Y') {
            if ($_REQUEST["show_canceled"] == 'Y') {
                ?>
                <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER') ?></h3>
                <?
            }
            else {
                ?>
                <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST') ?></h3>
                <?
            }
        }
        else {
            ?>
            <h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST') ?></h3>
            <?
        }
    }
    if (!count($arResult['ORDERS'])) {
        ?>
        <div class = "row col-md-12 col-sm-12">
            <a href = "<?= htmlspecialcharsbx($arParams['PATH_TO_CATALOG']) ?>" class = "sale-order-history-link">
                <?= Loc::getMessage('SPOL_TPL_LINK_TO_CATALOG') ?>
            </a>
        </div>
        <?
    }
    ?>

    <?if (count($arResult["ORDERS"]) && $_REQUEST["filter_history"] !== 'Y') :?>
        <table class="delivery">
            <thead>
            <tr>
                <th>Дата</th>
                <th>Номер заказа</th>
                <th>Финансовый номер</th>
                <th>Статус заказа</th>
                <th>Статус оплаты</th>
                <th>Печатные формы</th>
                <th>Функции</th>
            </tr>
            </thead>

            <?
            $paymentChangeData = array();
            $orderHeaderStatus = NULL;
            $arIcons = array("pdf","docx","rar","doc","xlsx","7z");
            ?>
            <tbody>
            <?
            foreach ($arResult['ORDERS'] as $key => $order) : ?>
                <?$XLS_HTML_URL = getOrderXLS_HTML($order["ORDER"]['ID']);?>
                <tr>
                    <td aria-label="Дата">
                        <?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?>
                    </td>
                    <td aria-label="Номер заказа">
                        <?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN') . $order['ORDER']['ACCOUNT_NUMBER']?>
                    </td>
                    <td aria-label="Финансовый номер"></td>
                    <td aria-label="Статус заказа" class="<?=($order["ORDER"]["CANCELED"] == "Y") ? "cancel" : (($arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["NAME"] == "Выполнен") ? "complete" : "")?>">
                        <?if ($order["ORDER"]["CANCELED"] == "Y") :?>
                        Отменен
                        <?else:?>
                        <?=$arResult["INFO"]["STATUS"][$order["ORDER"]["STATUS_ID"]]["NAME"]?>
                        <?endif;?>
                    </td>
                    <td aria-label="Статус оплаты">
                        <?if ($order["PAYMENT"][0]["PAID"] == "Y") :?>
                            оплачено
                        <?else:?>
                            не оплачено
                        <?endif;?>
                    </td>
                    <td aria-label="Печатные формы">
                        <?
                        $dirpath = $_SERVER['DOCUMENT_ROOT'].'/upload/1c_custom_exchange/orders_docs/'.$order['ORDER']['ACCOUNT_NUMBER'].'/';
                        $scan = scandir($dirpath);
                        ?>
                        <?if(count($scan) && $scan !== false):?>
                            <ul class="sale-order-detail-order-files-ul">
                                <?foreach($scan as $key=>$val):?>
                                    <?if(!is_dir($dirpath.'/'.$val)):?>
                                        <? if (in_array(pathinfo($dirpath.'/'.$val, PATHINFO_EXTENSION),$arIcons)): ?>
                                            <li class="sale-order-icon sale-order-detail-order-files-<?=pathinfo($dirpath.'/'.$val, PATHINFO_EXTENSION);?>">
                                                <a class = "sale-order-detail-order-files-link" href="/downloadOrder.php?id=<?=$arParams['ID']?>&filename=<?=$val?>"><?=$val?></a>
                                            </li>
                                        <?endif;?>
                                    <?endif;?>
                                <?endforeach;?>
                            </ul>
                        <?else:?>
                            Отсутствуют
                        <?endif;?>
                    </td>
                    <td aria-label="Функции">
                        <div>
                            <?if($XLS_HTML_URL['xlsx']):?><a class = "sale-order-list-cancel-link" href = "<?= $XLS_HTML_URL['xlsx'] ?>">Excel</a><?endif;?>
                            <?if($XLS_HTML_URL['html']):?><a class = "sale-order-list-cancel-link" href = "<?= $XLS_HTML_URL['html'] ?>">PDF</a><?endif;?>
                        </div>
                        <div>
                            <a class = "sale-order-list-about-link" href = "<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"]) ?>"><?= Loc::getMessage('SPOL_TPL_MORE_ON_ORDER') ?></a>
                        </div>
                        <div>
                            <a attr-id="<?=$order["ORDER"]["ID"]?>" class = "sale-order-list-repeat-link" href = "<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"]) ?>"><?= Loc::getMessage('SPOL_TPL_REPEAT_ORDER') ?></a>
                        </div>
                    </td>
                </tr>
            <?endforeach;?>
            </tbody>
        </table>
    <?else :?>
        <?
        $orderHeaderStatus = NULL;
        if ($_REQUEST["show_canceled"] === 'Y' && count($arResult['ORDERS'])) {
            ?>
            <h1 class = "sale-order-title">
                <?= Loc::getMessage('SPOL_TPL_ORDERS_CANCELED_HEADER') ?>
            </h1>
            <?
        }

        foreach ($arResult['ORDERS'] as $key => $order) {
            if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $_REQUEST["show_canceled"] !== 'Y') {
                $orderHeaderStatus = $order['ORDER']['STATUS_ID'];
                ?>
                <h1 class = "sale-order-title">
                    <?= Loc::getMessage('SPOL_TPL_ORDER_IN_STATUSES') ?>
                    &laquo;<?= htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME']) ?>&raquo;
                </h1>
                <?
            }
            ?>
            <div class = "col-md-12 col-sm-12 sale-order-list-container">
                <div class = "row">
                    <div class = "col-md-12 col-sm-12 sale-order-list-accomplished-title-container">
                        <div class = "row">
                            <div class = "col-md-8 col-sm-12 sale-order-list-accomplished-title-container">
                                <h2 class = "sale-order-list-accomplished-title">
                                    <?= Loc::getMessage('SPOL_TPL_ORDER') ?>
                                    <?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') ?>
                                    <?= htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']) ?>
                                    <?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
                                    <?= $order['ORDER']['DATE_INSERT'] ?>,
                                    <?= count($order['BASKET_ITEMS']); ?>
                                    <?
                                    $count = substr(count($order['BASKET_ITEMS']), -1);
                                    if ($count == '1') {
                                        echo Loc::getMessage('SPOL_TPL_GOOD');
                                    } elseif ($count >= '2' || $count <= '4') {
                                        echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
                                    } else {
                                        echo Loc::getMessage('SPOL_TPL_GOODS');
                                    }
                                    ?>
                                    <?= Loc::getMessage('SPOL_TPL_SUMOF') ?>
                                    <?= $order['ORDER']['FORMATED_PRICE'] ?>
                                </h2>
                            </div>
                            <div class = "col-md-4 col-sm-12 sale-order-list-accomplished-date-container">
                                <?
                                if ($_REQUEST["show_canceled"] !== 'Y') {
                                    ?>
                                    <span class = "sale-order-list-accomplished-date">
										<?= Loc::getMessage('SPOL_TPL_ORDER_FINISHED') ?>
									</span>
                                    <?
                                } else {
                                    ?>
                                    <span class = "sale-order-list-accomplished-date canceled-order">
										<?= Loc::getMessage('SPOL_TPL_ORDER_CANCELED') ?>
									</span>
                                    <?
                                }
                                ?>
                                <span class = "sale-order-list-accomplished-date-number"><?= $order['ORDER']['DATE_STATUS_FORMATED'] ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class = "row">
                    <div class = "col-md-12 sale-order-list-inner-accomplished">
                        <div class = "row sale-order-list-inner-row">
                            <div class = "col-md-3 col-sm-12 sale-order-list-about-accomplished">
                                <a class = "sale-order-list-about-link" href = "<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"]) ?>">
                                    <?= Loc::getMessage('SPOL_TPL_MORE_ON_ORDER') ?>
                                </a>
                            </div>
                            <div class = "col-md-3 col-md-offset-6 col-sm-12 sale-order-list-repeat-accomplished">
                                <a attr-id="<?=$order["ORDER"]["ID"]?>" class = "sale-order-list-repeat-link sale-order-link-accomplished" href = "<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"]) ?>">
                                    <?= Loc::getMessage('SPOL_TPL_REPEAT_ORDER') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?
        }
        ?>
        <div class = "clearfix"></div>
    <?endif;?>
    <?
    echo $arResult["NAV_STRING"];

    if ($_REQUEST["filter_history"] !== 'Y') {
        $javascriptParams = array(
            "url" => CUtil::JSEscape($this->__component->GetPath() . '/ajax.php'),
            "templateFolder" => CUtil::JSEscape($templateFolder),
            "templateName" => $this->__component->GetTemplateName(),
            "paymentList" => $paymentChangeData
        );
        $javascriptParams = CUtil::PhpToJSObject($javascriptParams);
        ?>
        <script>
            BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
        </script>
        <?
    }
}
?>
