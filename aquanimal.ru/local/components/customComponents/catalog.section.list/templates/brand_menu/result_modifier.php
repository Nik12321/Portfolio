<?php
if(is_array($arResult['SECTIONS']) && count($arResult['SECTIONS'])>0) {
    foreach($arResult['SECTIONS'] as $key => $arSection) {
        if( $arParams['FILTER_CONTROL_NAME']!='') {
            $arResult['SECTIONS'][$key]['SECTION_PAGE_URL'] = $arSection['SECTION_PAGE_URL'].'?'.$arParams['FILTER_CONTROL_NAME'];
        }
    }
}