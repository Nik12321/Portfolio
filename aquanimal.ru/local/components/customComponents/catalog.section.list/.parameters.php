<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
if(!CModule::IncludeModule('iblock')) return false;
if(!CModule::IncludeModule('catalog')) return false;

$iblocks = array(""=>"");
$rs = \Bitrix\Iblock\IblockTable::getList();
while($r = $rs->fetch())
{
	$iblocks[$r["ID"].""] = "{$r["NAME"]} [{$r["ID"]}]";
}

$fields = array(
	"ID" => "ID",
	"CODE" => "CODE",
	"IBLOCK_ID" => "IBLOCK_ID",
	"IBLOCK_SECTION_ID" => "IBLOCK_SECTION_ID",
	"NAME" => "NAME",
	"ACTIVE" => "ACTIVE",
	"ACTIVE_FROM" => "ACTIVE_FROM",
	"ACTIVE_TO" => "ACTIVE_TO",
	"PREVIEW_PICTURE" => "PREVIEW_PICTURE",
	"PREVIEW_TEXT" => "PREVIEW_TEXT",
	"DETAIL_PICTURE" => "DETAIL_PICTURE",
	"DETAIL_TEXT" => "DETAIL_TEXT",
	"DATE_CREATE" => "DATE_CREATE",
	"CREATED_BY" => "CREATED_BY",
	"SORT" => "SORT",
	"SHOW_COUNTER" => "SHOW_COUNTER",
	"XML_ID" => "XML_ID",
	"DETAIL_PAGE_URL" => "DETAIL_PAGE_URL",
);

$arPrice = CCatalogIBlockParameters::getPriceTypesList();

$arComponentParameters = Array(
	"GROUPS" => array(),
	'PARAMETERS' => array(
		'IBLOCK_ID' => Array(
			'PARENT' => 'BASE',
			'NAME' => "Iblock",
			'TYPE' => 'LIST',
			'VALUES' => $iblocks,
			'DEFAULT' => '',
		),
        'ELEMENT_ID' => Array(
            'PARENT' => 'BASE',
            'NAME' => "Elements ids",
            'TYPE' => 'LIST',
            'DEFAULT' => '',
        ),
		'CACHE_TIME'	=>	array('DEFAULT'=>'36000000'),
	),
);
?>