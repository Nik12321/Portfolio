<?

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Loader;
use Bitrix\Iblock\InheritedProperty;
use Bitrix\Iblock\Template;
use Bitrix\Main\Application;

class ForvardAutoCatalogSections extends CBitrixComponent
{
    protected $cacheAddon = array();
    protected $cacheKeys = array("SECTIONS","SECTIONS_CHAIN","ALL_ITEMS_IDS","PRICES");

    public function onPrepareComponentParams($arParams)
    {
        $this->cacheAddon = array($arParams['SECTION_ID'], $arParams['IBLOCK_ID'], $arParams['FIELDS']);
        return $arParams;
    }

    protected function checkModules()
    {
        if (!Loader::includeModule("iblock")) throw new Main\LoaderException("Module iblock not installed.");
        if (!Loader::includeModule("catalog")) throw new Main\LoaderException("Module catalog not installed.");
    }

    protected function checkParams()
    {
        if ($this->arParams['IBLOCK_ID'] <= 0) throw new \Bitrix\Main\ArgumentNullException('IBLOCK_ID');
    }

    protected function readDataFromCache()
    {
        if ($this->arParams['CACHE_TYPE'] == 'N') { return false;}
        return !($this->StartResultCache(false, $this->cacheAddon,'/'.$this->getSiteId().'/fa/catalog.sections/common'));
    }

    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0)
        {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }
    protected function getResult()
    {
        $arParams = &$this->arParams; //Получаем параметры по ссылке
        $arResult = &$this->arResult; //Получаем данные по ссылке
        $arResult["SECTIONS"] = array();
        $tree = CIBlockSection::GetTreeList(
            $arFilter=array("IBLOCK_ID" => $arParams["IBLOCK_ID"],"ID" => $arParams["IDS"], "ACTIVE" => "Y"),
            $arSelect=Array("ID", "NAME", "CODE", "SECTION_PAGE_URL", "PICTURE")
        );
        while($section = $tree->GetNext()) {
            $arResult["SECTIONS"][] = $section;
        }
    }

    public function executeComponent_customCache()
    {
        global $APPLICATION;
        try
        {
            $this->checkModules();
            $this->checkParams();
            $cache_id = md5(serialize($this->cacheAddon));
            $obCache = \Bitrix\Main\Data\Cache::createInstance();
            if ($obCache->initCache(87600, $cache_id, "faSections")) {
                $this->arResult = $obCache->getVars();
            } else if ($obCache->startDataCache()) {
                $this->getResult();
                $obCache->endDataCache($this->arResult);
            }
            $this->IncludeComponentTemplate();
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
            $this->abortDataCache();
        }

        return parent::executeComponent();
    }

    public function executeComponent_internalCache()
    {
        global $APPLICATION;
        try
        {
            $this->checkModules();
            $this->checkParams();
            if (!$this->readDataFromCache())
            {
                $this->getResult();
                $this->putDataToCache();
                $this->IncludeComponentTemplate();
            }

            if($this->arResult["SECTIONS_CHAIN"])
            {
                foreach ($this->arResult["SECTIONS_CHAIN"] as &$s)
                {
                    $APPLICATION->AddChainItem($s["NAME"],$s["SECTION_PAGE_URL"]);
                }
                unset($s);
            }
        }
        catch (Exception $e)
        {
            ShowError($e->getMessage());
            $this->abortDataCache();
        }

        return parent::executeComponent();
    }

    public function executeComponent()
    {
        if(1) {
            return $this->executeComponent_internalCache();
        } else {
            return $this->executeComponent_customCache();
        }
    }
}