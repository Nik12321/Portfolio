<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
$this->setFrameMode(true);
$days = [
    'Понедельник' => "ПН",
    'Вторник' => "ВТ",
    'Среда' => "СР",
    'Четверг' => "ЧТ",
    'Пятница' => "ПТ",
    'Суббота' => "СБ",
    'Воскресенье' => "ВС",
];
?>
<div class="news-calendar">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="NewsCalMonthNav" align="left">
                <?if($arResult["SHOW_MONTH_LIST"]):?>
                    &nbsp;&nbsp;
                    <select onChange="b_result()" name="MONTH_SELECT" id="month_sel">
                        <?foreach($arResult["SHOW_MONTH_LIST"] as $month => $arOption):?>
                            <option value="<?=$arOption["VALUE"]?>" <?if($arResult["currentMonth"] == $month) echo "selected";?>><?=$arOption["DISPLAY"]?></option>
                        <?endforeach?>
                    </select>
                    <script language="JavaScript" type="text/javascript">
                        function b_result()
                        {
                            var idx=document.getElementById("month_sel").selectedIndex;
                            <?if($arParams["AJAX_ID"]):?>
                            BX.ajax.insertToNode(document.getElementById("month_sel").options[idx].value, 'comp_<?echo CUtil::JSEscape($arParams['AJAX_ID'])?>', <?echo $arParams["AJAX_OPTION_SHADOW"]=="Y"? "true": "false"?>);
                            <?else:?>
                            window.document.location.href=document.getElementById("month_sel").options[idx].value;
                            <?endif?>
                        }
                    </script>
                <?endif?>
            </td>
        </tr>
    </table>
    <br/>
    <table width='100%' border='0' cellspacing='1' cellpadding='4' class='NewsCalTable'>
        <tr>
            <?foreach($arResult["WEEK_DAYS"] as $WDay):?>
                <td class='NewsCalHeader'><?=$days[$WDay["FULL"]]?></td>
            <?endforeach?>
        </tr>
        <?foreach($arResult["MONTH"] as $arWeek):?>
            <tr>
                <?foreach($arWeek as $arDay):?>
                    <td align="left" valign="top" class='<?=(count($arDay["events"])) ? "NewsCalWeekend" : "NewsCalDefault"?>' width="14%">
                        <?if (count($arDay["events"])) : ?>
                    <?php
                    $eventsHtml = "";
                    foreach($arDay["events"] as $arEvent) {
                        if ($eventsHtml) {
                            $eventsHtml .= "<br><br><a href='" . $arEvent["url"] ."'>" . $arEvent["title"] ."</a>";
                        } else {
                            $eventsHtml = "<a href='" . $arEvent["url"] ."'>" . $arEvent["title"] ."</a>";
                        }
                    }
                    ?>
                        <div class="showEvents liteTooltip" data-tooltip-mouseover="<?=$eventsHtml?>">
                            <?endif;?>
                            <span class="<?=$arDay["day_class"]?>"><?=$arDay["day"]?></span>
                            <div data-tooltip="эта подсказка длиннее, чем элемент" data-attr="<?=$arDay["day"]?>">
                                <?foreach($arDay["events"] as $arEvent):?>
                                    <div style="display: hidden" event-url="<?=$arEvent["url"]?>" event-name="<?=$arEvent["title"]?>"></div>
                                <?endforeach?>
                            </div>
                            <?if (count($arDay["events"])) : ?>
                        </div>
                    <?endif;?>
                    </td>
                <?endforeach?>
            </tr >
        <?endforeach?>
    </table>
</div>
