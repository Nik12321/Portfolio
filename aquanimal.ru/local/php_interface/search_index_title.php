<?php
AddEventHandler("search", "BeforeIndex", Array("IndexCatalogTitle", "BeforeCatalogIndexHandler"));

class IndexCatalogTitle
{
    // создаем обработчик события "BeforeIndex"
    function BeforeCatalogIndexHandler($arFields)
    {
        if($arFields['MODULE_ID'] == 'iblock' && in_array($arFields['PARAM2'], array('16','17')))
        {
        	if(!CModule::IncludeModule("iblock")) // подключаем модуль
       			return $arFields;

        	$db_props = CIBlockElement::GetProperty(  // Запросим свойства индексируемого элемента
	          	$arFields["PARAM2"],         // BLOCK_ID индексируемого свойства
	          	$arFields["ITEM_ID"],          // ID индексируемого свойства
	          	array("sort" => "asc"),       // Сортировка (можно упустить)
	          	Array("CODE"=>"CML2_ARTICLE")
          	); // CODE свойства (в данном случае артикул)

	        if($ar_props = $db_props->Fetch())
	           $arFields["TITLE"] .= " ".$ar_props["VALUE"];   // Добавим свойство в конец заголовка индексируемого элемента
        	
        	//PR($arFields); 
        } else {
            $arFields['BODY'] = '';
            $arFields['TITLE'] = '';
            $arFields['TAGS'] = '';
        }

        return $arFields;
    }
}