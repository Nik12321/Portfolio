<?

function ZipStatusString( $status )
{
    switch( (int) $status )
    {
        case ZipArchive::ER_OK           : return 'N No error';
        case ZipArchive::ER_MULTIDISK    : return 'N Multi-disk zip archives not supported';
        case ZipArchive::ER_RENAME       : return 'S Renaming temporary file failed';
        case ZipArchive::ER_CLOSE        : return 'S Closing zip archive failed';
        case ZipArchive::ER_SEEK         : return 'S Seek error';
        case ZipArchive::ER_READ         : return 'S Read error';
        case ZipArchive::ER_WRITE        : return 'S Write error';
        case ZipArchive::ER_CRC          : return 'N CRC error';
        case ZipArchive::ER_ZIPCLOSED    : return 'N Containing zip archive was closed';
        case ZipArchive::ER_NOENT        : return 'N No such file';
        case ZipArchive::ER_EXISTS       : return 'N File already exists';
        case ZipArchive::ER_OPEN         : return 'S Can\'t open file';
        case ZipArchive::ER_TMPOPEN      : return 'S Failure to create temporary file';
        case ZipArchive::ER_ZLIB         : return 'Z Zlib error';
        case ZipArchive::ER_MEMORY       : return 'N Malloc failure';
        case ZipArchive::ER_CHANGED      : return 'N Entry has been changed';
        case ZipArchive::ER_COMPNOTSUPP  : return 'N Compression method not supported';
        case ZipArchive::ER_EOF          : return 'N Premature EOF';
        case ZipArchive::ER_INVAL        : return 'N Invalid argument';
        case ZipArchive::ER_NOZIP        : return 'N Not a zip archive';
        case ZipArchive::ER_INTERNAL     : return 'N Internal error';
        case ZipArchive::ER_INCONS       : return 'N Zip archive inconsistent';
        case ZipArchive::ER_REMOVE       : return 'S Can\'t remove file';
        case ZipArchive::ER_DELETED      : return 'N Entry has been deleted';

        default: return sprintf('Unknown status %s', $status );
    }
}

function display_xml_error($error, $xml) {
    $return = $xml[$error->line - 1] . "\n";
    $return .= str_repeat('-', $error->column) . "^\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "Warning $error->code: ";
            break;
        case LIBXML_ERR_ERROR:
            $return .= "Error $error->code: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "Fatal Error $error->code: ";
            break;
    }

    $return .= trim($error->message) .
        "\n  Line: $error->line" .
        "\n  Column: $error->column";

    if ($error->file) {
        $return .= "\n  File: $error->file";
    }

    return "$return\n\n--------------------------------------------\n\n";
}

function parseContragent($values, $fields) {
    // print_r($values);
    // print_r($fields);
    if ($values->АдресРегистрации->АдресноеПоле) {
        foreach ($values->АдресРегистрации->АдресноеПоле as $adress) {
            switch ($adress->Тип) {
                case 'Почтовый индекс':
                    logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
                    $fields['PERSONAL_ZIP'] = trim($adress->Значение);
                    break;
                case 'Страна':
                    logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
                    $fields['PERSONAL_COUNTRY'] = "1";
                    break;
                case 'Регион':
                    logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
                    $fields['PERSONAL_STATE'] = trim($adress->Значение);
                    break;
                case 'Район':
                    logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
                    $fields['PERSONAL_STATE'] = trim($adress->Значение);
                    break;
                case 'Город':
                    logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
                    $fields['PERSONAL_CITY'] = trim($adress->Значение);
                    break;
                case 'Населенный пункт':
                    logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
                    $fields['PERSONAL_CITY'] = trim($adress->Значение);
                    break;
                case 'Улица':
                    logToFile($logFile, "Обработан: '".$adress->Тип."' и добавлен в массив");
                    $fields['PERSONAL_STREET'] = trim($adress->Значение);
                    break;

                default:
                    throw new Exception(display_xml_error($errors, $xml));
                    break;
            }
        }
    }

    if ($values->Контакты->Контакт) {
        foreach ($values->Контакты->Контакт as $contact) {
            switch ($contact->Тип) {
                case 'Телефон рабочий':
                    logToFile($logFile, "Обработан: '".$contact->Тип."' и добавлен в массив");
                    $fields['PERSONAL_PHONE'] = trim($contact->Значение);
                    break;
                case 'Электронная почта':
                    logToFile($logFile, "Обработан: '".$contact->Тип."' и добавлен в массив");
                    $fields['PERSONAL_MAILBOX'] = trim($contact->Значение);
                    break;
                case 'Факс':
                    logToFile($logFile, "Обработан: '".$contact->Тип."' и добавлен в массив");
                    $fields['PERSONAL_FAX'] = trim($contact->Значение);
                    break;

                default:
                    throw new Exception(display_xml_error($errors, $xml));
                    break;
            }
        }
    }

    return $fields;
}

function agent__importUsers()
{
    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../';
    date_default_timezone_set('Europe/Moscow');

    $zip_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Contragents.zip';
    $file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Contragents.xml';
    $dest_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange';

    $logFile = "/_logs/usersImport/".date('Y-m-d').'.txt';

    global $DB;

    if(file_exists($zip_file_path)) {
        $zip = new ZipArchive;
        $res = $zip->open($zip_file_path);

        if ($res !== true) {
            echo "Ошибка открытия архива: ".$zip_file_path;
        } else {
            //var_dump($zip->numFiles);
            $extracted = $zip->extractTo($dest_file_path);
            //var_dump($extracted, $zip->status, ZipStatusString($zip->status));
            $zip->close();

            if($extracted) {
                logToFile($logFile, "Распаковка архива прошла успешно. Продолжаем обработку...");
            } else {
                //var_dump($extracted, $zip->status, ZipStatusString($zip->status));
                logToFile($logFile, "Ошибка распаковки. Прерываем обработку...");
                //throw new Exception('Ошибка распаковки.');
            }
        }
    } else {
        //throw new Exception('Архив не найден.');
        logToFile($logFile, "Архив не найден. Пропускаем обработку.");

        return __METHOD__ . "();";
    }

    if (isset($file_path) && file_exists($file_path)) {
        logToFile($logFile, "Файл {$file_path} в последний раз был изменен: " . date("F d Y H:i:s.", filectime($file_path)));
    } else {
        logToFile($logFile, 'XML файл '.$file_path.' не найден.');
        //throw new Exception('XML файл '.$file_path.' не найден.');

        return __METHOD__ . "();";
    }

    try {
        $time_start = microtime(true);

        $feed = file_get_contents($file_path);
        // var_dump($feed);
        $xml_string = simplexml_load_string($feed);
        $errors = libxml_get_errors();

        //var_dump($errors);

        if(count($errors)) {
            foreach ($errors as $error) {
                throw new Exception(display_xml_error($error, $xml_string));
            }
            libxml_clear_errors();
        }


        $startMemory = memory_get_usage();
        $contragentCount = 0;
        foreach ($xml_string->Контрагенты as $values) {

            $allContragent = 0;
            foreach ($values->Контрагент as $usersContragentCount) {
                $allContragent++;
            }
            logToFile($logFile, "Всего содержится контрагентов {$allContragent}");
            // var_dump($allContragent);

            $attributes = $values->attributes();
            if ($attributes['СодержитТолькоИзменения'] && 1) {
                logToFile($logFile, "В файле присутствуют изменения {$file_path}");
                foreach ($values->Контрагент as $value) {
                    $contragentCount++;

                    // если XML_ID не пуст
                    if ($value->Ид !== "") {
                        $cUser = new CUser;

                        $xml_id = trim($value->Ид);

                        $arFields = array(
                            "LOGIN" => trim($value->Login)
                        );
                        $login = $arFields['LOGIN'];
                        //logToFile($logFile, "В XML пользователь с логином: {$arFields['LOGIN']}, XML_ID: {$xml_id} найден");

                        $arFilter = Array(
                            "LOGIC"=>"OR",
                            Array(
                                "EMAIL" => $login
                            ),
                            Array(
                                "LOGIN" => $login
                            ),
                            Array(
                                "XML_ID" => $xml_id
                            )
                        );
                        //$arUser = CUser::GetList(($by = ""), ($order = ""), $arFilter);
                        $arUser = CUser::GetList(($by = ""), ($order = ""), array("XML_ID" => $xml_id));

                        $arXMLFields = parseContragent($value, $arFields);
                        $arXMLFields['NAME'] = trim($value->Наименование);
                        $arXMLFields['UF_CONTRAGENT_CODE'] = trim($value->Код);
                        $arXMLFields['XML_ID'] = trim($xml_id);

                        $rUser = $arUser->Fetch();

                        if(!$rUser) {
                            $arUser = CUser::GetList(($by = ""), ($order = ""), array("LOGIN" => $login));
                            //$arUser = CUser::GetByLogin($login);

                            $rUser = $arUser->Fetch();
                        }

                        // Если пользователь c XML_ID найден
                        if ($rUser) {

                            //print_r([
                            //	$login,

                            //	$xml_id,

                            //	$rUser
                            //]); //continue;

                            // var_dump("Пользователь XML_ID: ".$rUser['XML_ID']." в базе битрикс c ID:".$rUser['ID']. " найден, идёт обновление данных в таблице битрикс");
                            logToFile($logFile, "Пользователь XML_ID: {$rUser['XML_ID']} в базе битрикс c ID: {$rUser['ID']} найден, идёт обновление данных в таблице битрикс");



                            //В случае успеха обновления, добавляем HASH в old_user
                            if ($cUser->Update($rUser['ID'], $arXMLFields)) {
                                // unset($arXMLFields); // очищаем массив от лишних полей
                                logToFile($logFile, "Данные пользователя ID:{$rUser["ID"]} обновлены");

                                $from_old = "FROM old_users where LOGIN='$login';";
                                $rsOLDUser = $DB->Query("SELECT * $from_old");

                                $arOLDUser = $rsOLDUser->GetNext();

                                $arFields['HASH'] = trim($value->Hash);

                                if(!$arOLDUser) {

                                    //__afterUserAdd($arFields);
                                    logToFile($logFile, "HASH пользователя с ID:{$rUser["ID"]} добавлен в old_user");
                                } else {
                                    // var_dump("Пользователь уже существует в таблице old_users ".$arOLDUser['LOGIN']." XML_ID: ".$xml_id);
                                    logToFile($logFile, "Пользователь уже существует в таблице old_users {$login} XML_ID: {$xml_id}. Обновляем хэш.");

                                    $hash = $arFields['HASH'] ;
                                    $DB->Query("UPDATE old_users SET HASH = '$hash' where LOGIN='$login'");
                                }
                            } else {
                                logToFile($logFile, "ОШИБКА. При обновлении профиля пользователя {$arOLDUser['ID']}. ".$cUser->LAST_ERROR);
                            }
                        } else {
                            $password = md5(time());
                            $addFields = Array(
                                "NAME"              => strip_tags($arXMLFields['NAME']),
                                //"LAST_NAME"         => "Иванов",
                                "EMAIL"             => $login,
                                "LOGIN"             => $login,
                                "ACTIVE"            => "Y",
                                "GROUP_ID"          => array(2,5),
                                "PASSWORD"          => $password,
                                "CONFIRM_PASSWORD"  => $password,
                                "XML_ID" => $xml_id
                            );

                            $ID = $cUser->Add($addFields);
                            if (intval($ID) > 0) {
                                logToFile($logFile, "Добавлен новый пользователь с логином: {$login} и ID: {$ID}. XML_ID: {$xml_id}\n");

                                // Ищем пользователя с таким логином в "old_users"

                                // var_dump($login);
                                $from_old = "FROM old_users where LOGIN='$login';";
                                $rsOLDUser = $DB->Query("SELECT * $from_old");

                                if($arOLDUser = $rsOLDUser->GetNext()) {
                                    // var_dump("Пользователь уже существует в таблице old_users ".$arOLDUser['LOGIN']." XML_ID: ".$xml_id);
                                    logToFile($logFile, "Совпадение логина {$login} в таблице old_users. XML_ID: {$xml_id}\n");
                                    $hash = trim($value->Hash);
                                    $DB->Query("UPDATE old_users SET HASH = '$hash' where LOGIN='$login'");
                                } else {
                                    // var_dump("Совпадений не найдено по ".$arFields['LOGIN'].". Добавляем в old_user");
                                    logToFile($logFile, "Совпадений не найдено по {$login}, идёт добавление в таблицу old_users\n");

                                    $arFields['HASH'] = trim($value->Hash);
                                    //__afterUserAdd($arFields);
                                    // var_dump("Пользователь ".$arFields['LOGIN']." добавлен в old_user");
                                    logToFile($logFile, "Пользователь {$login} успешно добавлен в таблицу old_users");

                                }
                            } else {
                                logToFile($logFile, "Произошла ошибка при добавлении нового пользователя с логином: {$login}. XML_ID: {$xml_id}\nОшибка: {$cUser->LAST_ERROR}");
                                //var_dump($cUser->LAST_ERROR);
                            }
                        }

                        unset($arXMLFields);
                        unset($arFields);
                        unset($cUser);
                    }
                }

                break;
            } else {
                logToFile($logFile, "XML не содержит изменений.");
                //exit;
            }
        }

        logToFile($logFile, "Обработано: ".$contragentCount);

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);

        @unlink($zip_file_path);

        rename($file_path, $file_path.'-Handled-'.date('Y-m-d H:i:s'));

    } catch (Exception $e) {
        echo($e);

        logToFile($logFile, $e->getMessage());
        logToFile($logFile, $e);
    }



    return __METHOD__ . "();";
}


function agent__importFilials() {
    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../';
    date_default_timezone_set('Europe/Moscow');
    ini_set('memory_limit', '1024M');
    set_time_limit(0);
    $logFile = "/_logs/filials/Result" . date('Y-m-d') . ".txt";
    //Папка, которую выгружаются филиалы и подразделения
    $currectDir = $DOCUMENT_ROOT . "/upload/1c_custom_exchange/";
    //Путь к архиву
    $zip_file_path = $currectDir . "Filials.zip";
    //Путь к файлу с филиалами и подразделами
    $file_path = $currectDir . "Filials.csv";
    //Путь к обработанному файлу
    $newFilePath = $currectDir . "Filials.csv-HANDLED-" . date('Y-m-d H:i:s');
    //Если архив существует, то распаковываем его:
    if(file_exists($zip_file_path)) {

        $zip = new ZipArchive;
        $res = $zip->open($zip_file_path);

        if ($res !== true) {
            echo "Error archive openning: ".$zip_file_path;
        } else {
            $extracted = $zip->extractTo($currectDir);
            $zip->close();

            if($extracted) {
                logToFile($logFile, "Extracting success finished. Continue handling...");
            } else {
                logToFile($logFile, "Extract error. Stop handling...");
                throw new Exception('Ошибка распаковки.');
            }
        }
    }
    else {
        //throw new Exception('Архив не найден.');
        logToFile($logFile, "Archive not found. Skip handling.");
        throw new Exception('Архив не найден');
    }
    //Если существует файл, то продолжаем его обработку:
    if (isset($file_path) && file_exists($file_path)) {
        logToFile($logFile, "File {$file_path} was changed in last time: " . date("F d Y H:i:s.", filectime($file_path)));
    } else {
        logToFile($logFile, 'CSV file '. $file_path.' not found.');
        throw new Exception('CSV файл '.$file_path.' не найден.');
    }
    try {
        $delete = $update = $add = $notFoundFilial = $error = 0;
        //Подключаем highload-блок (теперь мы делаем ОДНО обращение к нему)
        if (!CModule::IncludeModule('highloadblock') || !CModule::IncludeModule('catalog') || !CModule::IncludeModule('iblock')) {
            logToFile($logFile, 'Ошибка загрузки модуля catalog, highloadblock, iblock');
            throw new Exception('Ошибка загрузки модуля catalog, highloadblock, iblock');
        }
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array(
                "filter" => array(
                    'TABLE_NAME' => 'filial_references'
                )
            )
        )->fetch();
        if (isset($hlblock['ID'])) {
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $time_start = microtime(true);
            $startMemory = memory_get_usage();

            //Получаем все данные
            $allData = array();
            $allDivisionXmlIds = array();
            if (($handle = fopen($file_path, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    $allData[$data[0]][$data[1]] =  iconv("CP1251", "UTF-8", $data[2]);
                    $allDivisionXmlIds[] = $data[1];
                }
                fclose($handle);
            }
            $arrUsers = getUser();
            if (count($allDivisionXmlIds)) {
                $rsData = $entity_data_class::getList(array(
                    'select' => array('*'),
                    'filter' => array(
                        'UF_DIVISION_XML_ID' => $allDivisionXmlIds,
                    )
                ));
                while ($el = $rsData->fetch()) {
                    if ($allData[$el["UF_FILIAL_XML_ID"]][$el["UF_DIVISION_XML_ID"]] != $el["UF_DIVISION_NAME"]) {
                        $entity_data_class::delete($el["ID"]);
                        $delete++;
                    }
                    else {
                        unset($allData[$el["UF_FILIAL_XML_ID"]][$el["UF_DIVISION_XML_ID"]]);
                        if (!$el["UF_DIVISION_ID"] &&  $arrUsers[$el["UF_DIVISION_XML_ID"]]["ID"]) {
                            $entity_data_class::update($el["ID"], array("UF_DIVISION_ID" =>  $arrUsers[$el["UF_DIVISION_XML_ID"]]["ID"]));
                            $update++;
                        }
                    }
                }
                foreach ($allData as $keyElem => $element) {
                    if (!count($element)) {
                        unset($allData[$keyElem]);
                    }
                }
            }
            if (count($allData)) {
                foreach ($allData as $filialXmlID => $divisions) {
                    if ($arrUsers[$filialXmlID]) {
                        $filialID = $arrUsers[$filialXmlID]["ID"];
                        foreach($divisions as $divisionXmlID => $divisonName) {
                            if ($divisonName && $divisionXmlID && $filialXmlID && $filialID) {
                                $divisionID = $arrUsers[$divisionXmlID]["ID"];
                                $result = $entity_data_class::add(array(
                                    'UF_FILIAL_ID' => $filialID,
                                    'UF_DIVISION_ID' => $divisionID,
                                    'UF_DIVISION_XML_ID' => $divisionXmlID,
                                    'UF_DIVISION_NAME' => $divisonName,
                                    "UF_FILIAL_XML_ID" => $filialXmlID,
                                    "UF_IS_SHOW" => "Y"
                                ));
                                if ($result) {
                                    logToFile($logFile, "Add new division with XML_ID: {$divisionXmlID}");
                                    $add++;
                                }
                                else {
                                    logToFile($logFile, "Error for add new division with ID: {$divisionID}");
                                    $error++;
                                }
                            }
                        }
                    }
                    else {
                        logToFile($logFile, "Not found filial with XML_ID: {$filialXmlID}");
                        $notFoundFilial++;
                        continue;
                    }
                }

                print "memory_get_usage() =" . memory_get_usage()/1024 . "kb\n";
                print "memory_get_usage(true) =" . memory_get_usage(true)/1024 . "kb\n";
                print "memory_get_peak_usage() =" . memory_get_peak_usage()/1024 . "kb\n";
                print "memory_get_peak_usage(true) =" . memory_get_peak_usage(true)/1024 . "kb\n";
                print "custom memory_get_process_usage() =" . memory_get_process_usage() . "kb\n";

                $time_end = microtime(true);
                $execution_time = ($time_end - $time_start);


                logToFile($logFile, "Total add new divisions: {$add},\n not found filials {$notFoundFilial}.\n Time taken {$execution_time} seconds.");
                logToFile($logFile, error_get_last());
                print "Total add new divisions: {$add},\n update divisions: {$update},\n delete divisions: {$delete},\n not found filials {$notFoundFilial}.\n Time taken {$execution_time} seconds.";
                print_r(error_get_last());
                @unlink($zip_file_path);
                rename($file_path, $newFilePath);
            }
            else {
                logToFile($logFile, "ERROR!!! Uncorrect data!");
            }
        }
        else {
            logToFile($logFile, 'Ошибка, не найден hl-блок с филиалами и подразделениями');
            throw new Exception('Ошибка, не найден hl-блок с филиалами и подразделениями');
        }
    } catch (Exception $e) {
        logToFile($logFile, "ERROR!!! importFilials Handled.");
        logToFile($logFile, $e->getMessage());
        logToFile($logFile, $e);
    }
    return __METHOD__ . "();";
}


function agent__importPricesCSV()
{
    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../../';
    date_default_timezone_set('Europe/Moscow');
    ini_set('memory_limit', '1024M');
    set_time_limit(0);
    //Папка, которую выгружаются цены
    $currectDir = $DOCUMENT_ROOT . "/upload/1c_custom_exchange/CustPrices/";
    $logFileSetttings = "/_logs/priceImport/commonSettings".date('Y-m-d_H_i').'.txt';
    if (file_exists($currectDir)) {
        //Получаем список файлов в ней
        $files = scandir($currectDir);
        //Подключаем highload-блок (теперь мы делаем ОДНО обращение к нему)
        if (!CModule::IncludeModule('highloadblock') || !CModule::IncludeModule('catalog') || !CModule::IncludeModule('iblock')) {
            logToFile($logFileSetttings, 'Ошибка загрузки модуля catalog, highloadblock, iblock');
            throw new Exception('Ошибка загрузки модуля catalog, highloadblock, iblock');
        }
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array(
                "filter" => array(
                    'TABLE_NAME' => 'hl_client_price'
                )
            )
        )->fetch();
        if (isset($hlblock['ID'])) {
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            //Получаем все элементы
            $arrElements = getElements();
            $arrElementsById = $arrElements['byId'];
            $arrElementsByXml = $arrElements['byXML'];

            //Получаем всех пользователей
            $arrUsers = getUser();
//            logToFile($logFile, "Users list with csv_id fields: ".date('Y-m-d H:i:s'));
//            logToFile($logFile, $arrUsers);

            foreach ($files as $filename) {
                $pos = strripos($filename, "Prices");
                //Если в начале архива стоит слово Prices - парсим
                if ($pos === 0) {
                    //Название файла для парсинга
                    $filenameСSV = str_replace("zip", "csv", $filename);
                    //Путь к zip файлу
                    $zip_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/CustPrices/' . $filename;
                    //Путь к csv файлу
                    $file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/CustPrices/' . $filenameСSV;
                    //Новое название csv файла (после парсинга)
                    $newFilenameСSV = str_replace("Prices", "HANDLED-", $filenameСSV);
                    //Новый путь к csv файлу (после парсинга)
                    $newFilePath =  $DOCUMENT_ROOT.'/upload/1c_custom_exchange/CustPrices/' . $newFilenameСSV;
                    //Сюда будем логировать парсинг файла
                    $logFile = "/_logs/priceImport/".str_replace(".csv", "",$filenameСSV) .'.txt';

                    //Если архив существует, то распаковываем его:
                    if(file_exists($zip_file_path)) {

                        $zip = new ZipArchive;
                        $res = $zip->open($zip_file_path);

                        if ($res !== true) {
                            echo "Error archive openning: ".$zip_file_path;
                        } else {
                            $extracted = $zip->extractTo($currectDir);
                            //var_dump($extracted, $zip->status, ZipStatusString($zip->status));
                            $zip->close();

                            if($extracted) {
                                logToFile($logFile, "Extracting success finished. Continue handling...");
                            } else {
                                logToFile($logFile, "Extract error. Stop handling...");
                                throw new Exception('Ошибка распаковки.');
                            }
                        }
                    }
                    else {
                        //throw new Exception('Архив не найден.');
                        logToFile($logFile, "Archive not found. Skip handling.");
                        throw new Exception('Архив не найден');
                    }
                    //Если существует файл, то продолжаем его обработку:
                    if (isset($file_path) && file_exists($file_path)) {
                        logToFile($logFile, "File {$file_path} was changed in last time: " . date("F d Y H:i:s.", filectime($file_path)));
                    } else {
                        logToFile($logFile, 'CSV file '. $file_path.' not found.');
                        throw new Exception('CSV файл '.$file_path.' не найден.');
                    }

                    try {
                        $time_start = microtime(true);
                        $startMemory = memory_get_usage();
                        $verbose = false;
                        $iAll  = $iAdd = $iUpd = $iNotFind = $iNotChanged = $counter = $iKontragentNotFound = $kontragentIndex = 0;

                        //Получаем все данные
                        $allData = $allUsersExternalIdsProducts = array();
                        if (($handle = fopen($file_path, "r")) !== FALSE) {
                            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                                $allData[$data[0]][$data[1]] = $data[2];
                                if (!$allUsersExternalIdsProducts[$data[0]]) {
                                    $allUsersExternalIdsProducts[$data[0]] = array($data[1]);
                                }
                                else {
                                    $allUsersExternalIdsProducts[$data[0]][] = $data[1];
                                }
                            }
                            fclose($handle);
                        }
                        foreach ($allData as $key => $userData) {
                            $kontragentIndex++;
                            $user_xml_id = $key;

                            //logToFile($logFile, "User in XML with XML_ID = ".$user_xml_id);

                            $currentUserId = isset($arrUsers[$user_xml_id], $arrUsers[$user_xml_id]["ID"]) && $arrUsers[$user_xml_id]["ID"] ? $arrUsers[$user_xml_id]["ID"] : NULL;
                            if ($currentUserId && $allUsersExternalIdsProducts[$user_xml_id]) {
                                $productsIds = array();
                                foreach ($allUsersExternalIdsProducts[$user_xml_id] as $id) {
                                    if ($arrElementsByXml[$id]) {
                                        $productsIds[] = $arrElementsByXml[$id];
                                    }
                                }
                                if (count($productsIds)) {
                                    logToFile($logFile, "[".$kontragentIndex."] User with ID = {$currentUserId} & XML_ID = {$user_xml_id} found - processing");
                                    $rsData = $entity_data_class::getList(array(
                                        'select' => array('*'),
                                        'filter' => array(
                                            'UF_USER_ID' => $currentUserId,
                                            "UF_PRODUCT_ID" => $productsIds
                                        )
                                    ));

                                    $productIndex = 0;

                                    $arrUserPrice = array();

                                    while ($el = $rsData->fetch()) {
                                        if ($arrElementsById[$el['UF_PRODUCT_ID']]) {
                                            $arrUserPrice[$arrElementsById[$el['UF_PRODUCT_ID']]] = array(
                                                'ID' => $el['ID'],
                                                'UF_PRICE' => $el['UF_PRICE'],
                                                'UF_USER_ID' => $el['UF_USER_ID'],
                                            );
                                        }
                                    }

                                    if(0) {
                                        logToFile($logFile, "По пользователю найдены следующие товары в БД:");
                                        logToFile($logFile, $arrUserPrice);
                                    } else {
                                        logToFile($logFile, "[".$kontragentIndex."] User get count of records in DB: ".count($arrUserPrice));
                                    }
                                    foreach($userData as $itemId => $price) {

                                        $element_code = $itemId;

                                        $messageToLog = NULL;

                                        $element_xml_id = $itemId;
                                        $element_price =  preparePrice($price);

                                        $result = false;

                                        $productIndex++;

                                        $iAll++;
                                        if (!$arrElementsByXml[$element_xml_id]) {
                                            if($verbose) {
                                                logToFile($logFile, $productIndex.". Product with XML_ID = {$element_xml_id}, code {$element_code} not found.");
                                            }

                                            $iNotFind++;
                                            continue;
                                        }


                                        /*
                                         * Проверяем: есть - обновляем, нет - добавляем
                                         */

                                        if (isset($arrUserPrice[$element_xml_id])) {
                                            // Обновляем цену.
                                            if($arrUserPrice[$element_xml_id]['UF_PRICE'] != $element_price) {
                                                $messageToLog = $productIndex.". Product with ID = {$arrElementsByXml[$element_xml_id]}, XML_ID = {$element_xml_id}, code {$element_code}, price {$element_price} - update price.";

                                                $result = $entity_data_class::update($arrUserPrice[$element_xml_id]['ID'], array('UF_PRICE' => $element_price,));
                                                if ($result) {
                                                    $iUpd++;
                                                }
                                            }
                                            else {
                                                $messageToLog = $productIndex.". Product with ID = {$arrElementsByXml[$element_xml_id]}, XML_ID = {$element_xml_id}, code {$element_code}, price {$element_price} - price doesn't changed.";
                                                $iNotChanged++;
                                            }
                                        }
                                        elseif ($arrElementsByXml[$element_xml_id] && !isset($arrUserPrice[$element_xml_id])) {
                                            $messageToLog = $productIndex.". Product with ID = {$arrElementsByXml[$element_xml_id]}, XML_ID = {$element_xml_id}, code {$element_code}, price {$element_price} - adding to DB.";

                                            $result = $entity_data_class::add(array(
                                                'UF_USER_ID' => $currentUserId,
                                                'UF_PRODUCT_ID' => $arrElementsByXml[$element_xml_id],
                                                'UF_PRICE' => $element_price
                                            ));
                                            if ($result) {
                                                $iAdd++;
                                            }
                                        }
                                        else {
                                            $messageToLog = $productIndex.". Product with XML_ID = {$element_xml_id}, code {$element_code} - WTF??? Exist in DB, but doesn't added...";
                                        }
                                        if($messageToLog && $verbose) {
                                            logToFile($logFile, "[".$kontragentIndex."] ".$messageToLog);
                                        }

                                    }
                                    unset($arrUserPrice);
                                }
                                else {
                                    logToFile($logFile, "Cannot find products for user with XML_ID {$user_xml_id}");
                                }
                            }
                            else {
                                $iKontragentNotFound++;
                                logToFile($logFile, "User with XML_ID {$user_xml_id} not found - skiping.");
                            }
                            unset($userData);
                        }
                        unset($allData);
                        unset($allUsersExternalIdsProducts);
                        print "memory_get_usage() =" . memory_get_usage()/1024 . "kb\n";
                        print "memory_get_usage(true) =" . memory_get_usage(true)/1024 . "kb\n";
                        print "memory_get_peak_usage() =" . memory_get_peak_usage()/1024 . "kb\n";
                        print "memory_get_peak_usage(true) =" . memory_get_peak_usage(true)/1024 . "kb\n";
                        print "custom memory_get_process_usage() =" . memory_get_process_usage() . "kb\n";

                        $time_end = microtime(true);
                        $execution_time = ($time_end - $time_start);

                        CEventLog::Add(array(
                            "SEVERITY" => "SECURITY",
                            "AUDIT_TYPE_ID" => '1C_IMPORT_PRICE',
                            "MODULE_ID" => "main",
                            "ITEM_ID" => __METHOD__,
                            "DESCRIPTION" => "Всего в выгрузке товаров: {$iAll}, из них добавлено: {$iAdd}, обновлено {$iUpd}, без изменений {$iNotChanged}, не найдено: {$iNotFind}. Контрагентов не найдено: {$iKontragentNotFound} за {$execution_time} сек.",
                        ));

                        logToFile($logFile, "Total product counts in XML: {$iAll},\n from these added: {$iAdd},\n updated {$iUpd},\n without changes {$iNotChanged},\n not found: {$iNotFind}. \n Users not found.: {$iKontragentNotFound}. Time taken {$execution_time} seconds.");
                        logToFile($logFile, error_get_last());
                        print "Total product counts in XML: {$iAll},\n from these added: {$iAdd},\n updated {$iUpd},\n without changes {$iNotChanged},\\n not found: {$iNotFind}. \n Users not found.: {$iKontragentNotFound}. Time taken {$execution_time} seconds.";
                        @unlink($zip_file_path);
                        rename($file_path, $newFilePath);
                        print_r(error_get_last());
                    } catch (Exception $e) {
                        logToFile($logFile, "ERROR!!! importPrice Handled.");
                        logToFile($logFile, $e->getMessage());
                        logToFile($logFile, $e);
                        //    TODO не забыть раскомментить перед деплоем!
                        CEventLog::Add(array(
                            "SEVERITY" => "SECURITY",
                            "AUDIT_TYPE_ID" => '1C_IMPORT_PRICE',
                            "MODULE_ID" => "main",
                            "ITEM_ID" => __METHOD__,
                            "DESCRIPTION" => $e->getMessage(),
                        ));
                    }
                }
            }
        }
        else {
            logToFile($logFileSetttings, 'HlBlock with prices not found...');
            throw new Exception('HlBlock with prices not found...');
        };
    }

    return __METHOD__ . "();";
}


//Импорт внутренного счета пользователей
function agent__importBalances()
{
    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../..';
    date_default_timezone_set('Europe/Moscow');

    //Пути к архиву, файлу в нем и папке, в которой все лежит
    $zip_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Balance.zip';
    $file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Balance.csv';
    $dest_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange';
    //Путь для лог файлов
    $logFile = "/_logs/balancesImport/".date('Y-m-d_H_i').'.txt';
    //Задаем начальные параметры
    \Bitrix\Main\Loader::includeModule('sale');
    ini_set('memory_limit', '1024M');
    set_time_limit(0);
    try {
        //Если существует архив - распаковываем его
        if(file_exists($zip_file_path)) {
            $zip = new ZipArchive;
            $res = $zip->open($zip_file_path);
            //Если не удается открыть - выдаем ошибку
            if ($res !== true) {
                logToFile($logFile, "Error archive openning: ".$zip_file_path);
                throw new Exception("Error archive openning: ".$zip_file_path);
            }
            else {
                $extracted = $zip->extractTo($dest_file_path);
                $zip->close();
                if($extracted) {
                    logToFile($logFile, "Extracting success finished. Continue handling...");
                }
                //Если не удалось распаковать, выдаем ошибку
                else {
                    logToFile($logFile, "Extract error. Stop handling...");
                    throw new Exception("Extract error. Stop handling...");
                }
            }
        }
        //Иначе выдаем ошибку
        else {
            logToFile($logFile, "Archive not found. Skip handling.");
            throw new Exception("Archive not found. Skip handling.");
        }

        //Проверяем существование распакованного файла
        if (isset($file_path) && file_exists($file_path)) {
            logToFile($logFile, "File {$file_path} was changed in last time: " . date("F d Y H:i:s.", filectime($file_path)));
        } else {
            logToFile($logFile, 'CSV file '. $file_path.' not found.');
            throw new Exception('CSV файл '.$file_path.' не найден.');
        }

        //Если распаковали и файл существует - задаем начальные значения
        $time_start = microtime(true);
        $allData = $allXmlIds = array();
        $newBalance = $notFoundUsers = $uncorrectBalance = 0;

        //Считываем содержимое файла
        if (($handle = fopen($file_path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $balance = floatval(str_replace(array("&nbsp;", ","), array("", "."), htmlentities(mb_convert_encoding($data[1], "utf-8", "windows-1251"))));
                ($balance) ? $allData[$data[0]] = $balance : $allData[$data[0]] = 0;
                $allXmlIds[] = $data[0];
            }
            fclose($handle);
        }
        $totalNumberOfInputs = count($allXmlIds);
        $totalExistNumberOfInputs = count($allData);
        $allXmlIds = array_unique($allXmlIds);
        //Если найдены пользователи - работаем дальше. Если не найдены - выдаем ошибку.
        if (count($allXmlIds)) {
            //Получаем всех пользователей системы
            $users = getUser();
            //Перебираем данные и обновляем счета
            foreach($allXmlIds as $xmlId) {
                $user = $users[$xmlId];
                if (count($user)) {
                    $currentBalanceID = CSaleUserAccount::GetList(array(), array("USER_ID" => $user["ID"]), false, false, array("*"))->Fetch();
                    logToFile($logFile, $currentBalanceID);
                    CSaleUserAccount::Update($currentBalanceID["ID"], array("CURRENT_BUDGET" => $allData[$xmlId]), "RUB", "Import from 1C");
                    $newBalance++;
                }
                else {
                    $notFoundUsers++;
                }
            }

            print "memory_get_usage() =" . memory_get_usage()/1024 . "kb\n";
            print "memory_get_usage(true) =" . memory_get_usage(true)/1024 . "kb\n";
            print "memory_get_peak_usage() =" . memory_get_peak_usage()/1024 . "kb\n";
            print "memory_get_peak_usage(true) =" . memory_get_peak_usage(true)/1024 . "kb\n";
            print "custom memory_get_process_usage() =" . memory_get_process_usage() . "kb\n";

            $time_end = microtime(true);
            $execution_time = ($time_end - $time_start);
            logToFile($logFile, "Total balances count: {$totalNumberOfInputs},\n theirs exists: {$totalExistNumberOfInputs},\n added balances {$newBalance},\n and not found users: {$notFoundUsers}.\n Time taken {$execution_time} seconds.");
            rename($file_path, $file_path.'-Handled-'.date('Y-m-d H:i:s'));
            @unlink($zip_file_path);
        }
        else {
            logToFile($logFile, 'Users not found...');
            throw new Exception('Users not found...');
        }
    }
    catch (Exception $e) {
        logToFile($logFile, "ERROR!!! importBalances Handled.");
        logToFile($logFile, $e->getMessage());
        logToFile($logFile, $e);
        CEventLog::Add(array(
            "SEVERITY" => "SECURITY",
            "AUDIT_TYPE_ID" => '1C_IMPORT_SIMILAR_PRODUCTS',
            "MODULE_ID" => "main",
            "ITEM_ID" => __METHOD__,
            "DESCRIPTION" => $e->getMessage(),
        ));
    }
    return __METHOD__ . "();";
}


function agent__importSimilarProducts()
{
    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__) . '/../..';
    date_default_timezone_set('Europe/Moscow');

    $zip_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Analogs.zip';
    $file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange/Analogs.csv';
    $dest_file_path = $DOCUMENT_ROOT.'/upload/1c_custom_exchange';
    $logFile = "/_logs/similarProductsImport/".date('Y-m-d_H_i').'.txt';

    ini_set('memory_limit', '1024M');
    set_time_limit(0);

    global $DB;

    if(file_exists($zip_file_path) && 1) {

        $zip = new ZipArchive;
        $res = $zip->open($zip_file_path);

        if ($res !== true) {
            echo "Error archive openning: ".$zip_file_path;
        } else {
            $extracted = $zip->extractTo($dest_file_path);
            //var_dump($extracted, $zip->status, ZipStatusString($zip->status));
            $zip->close();

            if($extracted) {
                logToFile($logFile, "Extracting success finished. Continue handling...");
            } else {
                //var_dump($extracted, $zip->status, ZipStatusString($zip->status));
                logToFile($logFile, "Extract error. Stop handling...");
                //throw new Exception('Ошибка распаковки.');
            }
        }
    }
    else {
        throw new Exception('Архив не найден.');
        logToFile($logFile, "Archive not found. Skip handling.");
//
//        return __METHOD__ . "();";
    }

    if (isset($file_path) && file_exists($file_path)) {
        logToFile($logFile, "File {$file_path} was changed in last time: " . date("F d Y H:i:s.", filectime($file_path)));
    } else {
        logToFile($logFile, 'CSV file '. $file_path.' not found.');
//    throw new Exception('CSV файл '.$file_path.' не найден.');

        return __METHOD__ . "();";
    }

    try {
        $time_start = microtime(true);
        $allData = array();
        $allXmlIds = array();
        if (($handle = fopen($file_path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if ($allData[$data[0]]) {
                    $allData[$data[0]][] = $data[1];
                }
                else {
                    $allData[$data[0]] = array($data[1]);
                }
                $allXmlIds[] = $data[0];
            }
            fclose($handle);
        }
        $iAll = count($allXmlIds);
        $iSet = $iNSet = 0;
        $allXmlIds = array_unique($allXmlIds);
        if (count($allXmlIds)) {
            $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "XML_ID" => $allXmlIds), false, false, array("ID", "XML_ID"));
            while ($element = $elements->Fetch()) {
                if ($allData[$element["XML_ID"]]) {
                    $newPropertyValue = array();
                    $subElements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "XML_ID" => $allData[$element["XML_ID"]]), false, false, array("ID"));
                    while ($subElement = $subElements->Fetch()) {
                        $newPropertyValue[] = array("VALUE" => $subElement["ID"]);
                    }
                    if (count($newPropertyValue)) {
                        logToFile($logFile, "Product with xml_id " . $element["XML_ID"] . " have new similar products:");
                        logToFile($logFile, $newPropertyValue);
                        $iSet++;
                        CIBlockElement::SetPropertyValuesEx($element["ID"], 16, array("POKHOZHIE_TOVARY" => $newPropertyValue));
                    }
                    else {
                        $iNSet++;
                        logToFile($logFile, "Product with xml_id " . $element["XML_ID"] . " not have similar products, because it cannot found products with xml_ids: ");
                        logToFile($logFile, $allData[$element["XML_ID"]]);
                    }

                }
                else {
                    logToFile($logFile, 'Error, get product with xml_id, '. $element["XML_ID"] .' that not have similar products');
                    throw new Exception('Error, get product, that not have similar products');
                }
            }

            print "memory_get_usage() =" . memory_get_usage()/1024 . "kb\n";
            print "memory_get_usage(true) =" . memory_get_usage(true)/1024 . "kb\n";
            print "memory_get_peak_usage() =" . memory_get_peak_usage()/1024 . "kb\n";
            print "memory_get_peak_usage(true) =" . memory_get_peak_usage(true)/1024 . "kb\n";

            print "custom memory_get_process_usage() =" . memory_get_process_usage() . "kb\n";

            $time_end = microtime(true);
            $execution_time = ($time_end - $time_start);
            logToFile($logFile, "Total similar products count: {$iAll},\n count of products that have new property value: {$iSet},\n that not have new property value {$iNSet}. Time taken {$execution_time} seconds.");
            rename($file_path, $file_path.'-Handled-'.date('Y-m-d H:i:s'));
            @unlink($zip_file_path);
        }
        else {
            logToFile($logFile, 'Similar products now found...');
            throw new Exception('Similar products now found...');
        }
    } catch (Exception $e) {

        //echo($e);

        logToFile($logFile, "ERROR!!! importSimilarProducts Handled.");
        logToFile($logFile, $e->getMessage());
        logToFile($logFile, $e);

        //    TODO не забыть раскомментить перед деплоем!
        CEventLog::Add(array(
            "SEVERITY" => "SECURITY",
            "AUDIT_TYPE_ID" => '1C_IMPORT_SIMILAR_PRODUCTS',
            "MODULE_ID" => "main",
            "ITEM_ID" => __METHOD__,
            "DESCRIPTION" => $e->getMessage(),
        ));
    }



    return __METHOD__ . "();";
}


//Агент проверяет изображения всех товаров и отправляет несколько писем:
//1) Список товаров, у которых нет детального изображения
//2) Список товаров, у которых нет изображения для анонса
function agent__checkProductImages()
{
    $dbE = CIBlock::GetList(Array(), Array( "XML_ID" => "85e888c7-5ec9-4b9c-a34b-6f61c7bdabba"));
    if ($arE = $dbE->GetNext()) {
        $notExistDet  = array();
        $notExistPre = array();
        $res = CIBlockElement::GetList(false, array("IBLOCK_ID" => $arE["ID"], "ACTIVE" => "Y"), false, false,
                                       array("ID",
                                           "NAME",
                                           "DETAIL_PICTURE",
                                           "PREVIEW_PICTURE",
                                           "DETAIL_PAGE_URL"));
        $index = 0;
        while($ob = $res->GetNext()) {
            $index++;
            $existDetail = ($ob["DETAIL_PICTURE"]) ? true : false;
            $existPreview = ($ob["PREVIEW_PICTURE"]) ? true : false;
            $elementDetail = array();
            $elementPreview = array();
            if (!$existDetail) {
                $elementDetail["ID"] = $ob["ID"];
                $elementDetail["NAME"] = $ob["NAME"];
                $elementDetail["DETAIL_PAGE_URL"] = "http://aquanimal.ru" . $ob["DETAIL_PAGE_URL"];
                $notExistDetail[] = $elementDetail; //элементы без детальной картинки
            }
            if (!$existPreview){
                $elementPreview["ID"] = $ob["ID"];
                $elementPreview["NAME"] = $ob["NAME"];
                $elementPreview["DETAIL_PAGE_URL"] = "http://aquanimal.ru" . $ob["DETAIL_PAGE_URL"];
                $notExistPreview[] = $elementPreview; //элементы без картинки для анонса
            }
        }
        logToFile("/_logs/checkImages.txt", "Всего проверено товаров: " . $index . "\n");
        logToFile("/_logs/checkImages.txt", "Количество элементов, у которых нет детальной картинки: " . count($notExistDetail). "\n");
        logToFile("/_logs/checkImages.txt", "Количество элементов, у которых нет картинки для анонса: " . count($notExistPreview). "\n");
        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];

        $result = "";
        foreach ($notExistDetail as $elem) {
            $result .= $elem["NAME"] . " (" . $elem["ID"] . ") : " . "<a href='" . $elem["DETAIL_PAGE_URL"] . "'>ссылка</a>" . "<br>";
        }
        if ($result == "") {
            $result = "Нет товаров без детальной картинки!";
        }
        $arEventFields = array(
            "EMAIL" => $mail_to,
            "RESULT" =>  $result,
            "DEFAULT_EMAIL_FROM" => $mail_to,
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => "",
        );
        logToFile("/_logs/mailEventDataDetail.txt", $arEventFields);
        CEvent::Send("AGENT_RESULT_WITHOUT_DET_OR_PREV", "s1", $arEventFields, "Y", ""); //отправляем письмо с товарами без детальной картинки

        $result = "";
        foreach ($notExistPreview as $elem) {
            $result .= $elem["NAME"] . " (" . $elem["ID"] . ") : " . "<a href='" . $elem["DETAIL_PAGE_URL"] . "'>ссылка</a>" . "<br>";
        }
        if ($result == "") {
            $result = "Нет товаров без картинки анонса!";
        }
        $arEventFields = array(
            "EMAIL" => $mail_to,
            "RESULT" =>  $result,
            "DEFAULT_EMAIL_FROM" => $mail_to,
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => "",
        );
        logToFile("/_logs/mailEventDataPreview.txt", $arEventFields);
//        CEvent::Send("AGENT_RESULT_WITHOUT_MAIN_IMAGE", "s1", $arEventFields, "Y", ""); //отправляем письмо с товарами без картинки для анонса
        //UPD: Не отправяляем письмо т.к. заказчик попросил присылать только те товары где нет детальной картинки.
    }
    else {
        throw new Exception('Не найден инфоблок с кодом 1c_catalog_new.');
    }

    return __METHOD__ . "();";
}


//Агент удаляет старые файлы выгрузки, которым больше 30 дней
function agent__deleteOld_1c_files()
{
    $deletedFiles = 0;
    $deletedError = 0;
    $deletedSuccess = 0;
    $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
    //Сюда записываем пути к нужным директориям
    $paths = array("/upload/1c_custom_exchange/CustPrices/", "/upload/1c_custom_exchange/");
    foreach ($paths as $path) {
        $files = scandir($DOCUMENT_ROOT . $path);
        foreach ($files as $file) {
            if (preg_match("/^(Vigr.csv-Handled-|Contragents.xml-Handled-|Analogs.csv-Handled-|Filials.csv-HANDLED-|HANDLED-)/", $file) !== 0) {
                $filePath = $DOCUMENT_ROOT . $path . "/" . $file;
                $currentDate = time(); // текущее время (метка времени)
                $fileDate = filemtime($filePath);
                if ($fileDate) {
                    $datediff = $currentDate - $fileDate; // получим разность дат (в секундах)
                    if (($datediff / (60 * 60 * 24)) > 7) {
                        $deletedFiles++;
                        if (unlink($filePath)) {
                            $deletedSuccess++;
                        }
                        else {
                            $deletedError++;
                        }
                    }
                }
            }
        }
    }
    logToFile("/_logs/deleteOldFiles.txt", "Всего должно быть удалено файлов: " . $deletedFiles . "\n");
    logToFile("/_logs/deleteOldFiles.txt", "Успешно удалено: " . $deletedSuccess . "\n");
    logToFile("/_logs/deleteOldFiles.txt", "Не удалось удалить: " . $deletedError . "\n");
    return __METHOD__ . "();";
}

//Агент, выполняющий рассылку всем пользователям с брошенными корзинами
function agent_sendNotificationAboutForgottenBasket() {
    \Bitrix\Main\Loader::includeModule('sale');
    \Bitrix\Main\Loader::includeModule('landing');
    $daysBasketForgotten = 1;
    $dateFrom = new \Bitrix\Main\Type\DateTime;
    $dateTo = new \Bitrix\Main\Type\DateTime;

    $dateFrom->add('-1 days')->add('-1 hours');
    $dateTo->add('-1 days');
    logToFile("/_logs/emptyBasket.txt", "Проверка пустых корзин с " .  $dateFrom->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT) . " по " . $dateTo->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT));
    $filter = array(
        '>MIN_DATE_INSERT' => $dateFrom->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT),
        '<MIN_DATE_INSERT' => $dateTo->format(\Bitrix\Main\UserFieldTable::MULTIPLE_DATETIME_FORMAT),
    );
    $filter = $filter + array(
            '!FUSER.USER_ID' => null,
            '=ORDER_ID' => null,
        );
    $userListDb = \Bitrix\Sale\Internals\BasketTable::getList(array(
        'select' => array('USER_ID' => 'FUSER.USER_ID', 'EMAIL' => 'FUSER.USER.EMAIL', 'FUSER_USER_NAME' => 'FUSER.USER.NAME'),
        'filter' => $filter,
        'runtime' => array(
            new \Bitrix\Main\Entity\ExpressionField('MIN_DATE_INSERT', 'MIN(%s)', 'DATE_INSERT'),
        ),
        'order' => array('USER_ID' => 'ASC')
    ));
    $ob = $userListDb->fetchAll();

    $result = "";
    foreach ($ob as $basket) {
        $result .= "Email пользоваетеля: " . $basket["EMAIL"] . "<br>" .
            "ID пользователя: " . $basket["USER_ID"] . "<br><br>";
        logToFile("/_logs/emptyBasket.txt", $basket);
    }
    if ($result != "") {
        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        $mail_to = $arSite["EMAIL"];
        $arEventFields = array(
            "EMAIL" => $mail_to,
            "RESULT" =>  $result,
            "DEFAULT_EMAIL_FROM" => $mail_to,
            "SITE_NAME" => $arSite["NAME"],
            "SERVER_NAME" => "",
        );
        logToFile("/_logs/mailEventData.txt", $arEventFields);
        CEvent::Send("AGENT_FORGOTTEN_BASKETS", "s1", $arEventFields, "Y", "");
    }
    return __METHOD__ . "();";
}


/**
 * @param $price string
 * @return float
 */
function preparePrice($price) {
    $result = str_replace(array(',',' '), array('.',''), $price);
    return $result;
}

/**
 * Выборка всех элементов для сопоставления ID и XML_ID
 * @return mixed
 */
function getElements() {
    $rows = [];

    $arFilter = array(
        'IBLOCK_ID' => [16, 17],
        // 'IBLOCK_ID' => 1,
        'ACTIVE' => 'Y',
        '!XML_ID' => false,
    );

    $rsElement = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "XML_ID"));
    while ($obElement = $rsElement->Fetch()) {
        // торговые предложения тоже нужны.
        if (stripos($obElement['XML_ID'], '#') && 0) {
            $xmlId = substr($obElement['XML_ID'], 0, stripos($obElement['XML_ID'], '#'));
        } else {
            $xmlId=$obElement['XML_ID'];
        }

        $rows['byId'][$obElement['ID']] = $xmlId;
        $rows['byXML'][$xmlId] = $obElement['ID'];
    }
    unset($rsElement);
    unset($obElement);

    return $rows;
}

/**
 * Выборка всех пользователей для сопоставления ID и XML_ID
 * @return mixed
 */
function getUser() {
    $rows = [];
    $cUser = new CUser;
    $arFilter = array(
        '!XML_ID' => false,
        "ACTIVE" => 'Y',
    );
    $arSelect = array("SELECT" => array("XML_ID","UF_BASEPRICE"));
    $dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter, $arSelect);
    // $dbUsers = $cUser->GetList($sort_by="", $sort_ord="", $arFilter, $arSelect);
    while ($arUser = $dbUsers->Fetch()) {
        $rows[$arUser['XML_ID']] = array(
            "ID" => $arUser["ID"],
            // "UF_BASEPRICE" => $arUser["UF_BASEPRICE"],
            "NAME" => $arUser["NAME"],
            "EMAIL" => $arUser["EMAIL"],
            "XML_ID" => $arUser["XML_ID"],
            // "UF_XML_ID" => $arUser["UF_XML_ID"],
        );
    }
    //var_dump($rows);
    return $rows;
}

function memory_get_process_usage()
{
    $status = file_get_contents('/proc/' . getmypid() . '/status');

    $matchArr = array();
    preg_match_all('~^(VmRSS|VmSwap):\s*([0-9]+).*$~im', $status, $matchArr);

    if(!isset($matchArr[2][0]) || !isset($matchArr[2][1]))
    {
        return false;
    }

    return intval($matchArr[2][0]) + intval($matchArr[2][1]);
}