<?
use Bitrix\Main\Entity\ExpressionField;

itc\CUncachedArea::registerCallback('productBuyBlock', function ($key, $data) {
    global $USER;
    $output = array();
    if (!CModule::IncludeModule('sale')) return array();
    if (!CModule::IncludeModule('catalog')) return array();
    if (!CModule::IncludeModule('iblock')) return array();

	$ENABLE_UNDER_THE_ORDER = FALSE;
	
	//$uGroups = $USER->GetUserGroupArray();
	
	/*
	$rs = GetBasketListSimple(false);
    $products = array();
    if ($rs) {
        while ($b = $rs->Fetch()) {
            if (!$products[$b['PRODUCT_ID']])
                $products[$b['PRODUCT_ID']] = $b['QUANTITY'];
            else
                $products[$b['PRODUCT_ID']] += $b['QUANTITY'];
        }
    }*/

    $ids = array();
    $datas = array();
    foreach ($data as $item) {
        if ($item["id"] > 0) {
            $ids[] = $item["id"];
            $datas[intval($item["id"])] = $item;
        }
    }
	
	$customPrices = getCustomPricesForProducts($ids);

	foreach ($data as $item) {
        $subkey = itc\CUncachedArea::getSubkey($item);
		
		$id = $item["id"];
		
        if ($id <= 0) {
            $output[$subkey] = "";
            continue;
        }

        $buyBlockHTML = $item["customHtml"]; #PRICE_BLOCK_HTML# #STORES_BLOCK_HTML# #PAY_BLOCK_HTML#
		
		$buyBlockHTML = str_replace(
			array('#PRICE_BLOCK_HTML#','#STORES_BLOCK_HTML#','#PAY_BLOCK_HTML#'),
			array($item['priceBlock'],$item['storesBlock'],$item['payBlock']), 
			$buyBlockHTML
		);
		
		$customPriceForProduct = $customPrices[$id]["RESULT_PRICE"]["DISCOUNT_PRICE"];
		
        $priceF = priceFormat($customPriceForProduct);
        
        $priceF = ''. $priceF .' <i class="units">&#x20bd;</i>';
		
        $buyBlockHTML = str_replace("#PRICE#", $priceF, $buyBlockHTML);
		
        $output[$subkey] = $buyBlockHTML;

    }
    return $output;
});

?>