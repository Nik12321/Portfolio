<?php

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\FileTable;
use Bitrix\Main\Loader;
use Dompdf\Dompdf;

CModule::IncludeModule('itconstruct.uncachedarea');

$incFile = __DIR__ . "/uncached_handlers.php";
if (file_exists($incFile)) require_once($incFile);
$incFile = __DIR__ . "/agents.php";
if (file_exists($incFile)) require_once($incFile);
$incFile = __DIR__ . "/search_index_title.php";
if (file_exists($incFile)) require_once($incFile);
$autoloaderPath = $_SERVER["DOCUMENT_ROOT"] . "/dompdf/vendor/autoload.php";
if (file_exists($autoloaderPath)) {
    require_once($autoloaderPath);
}

AddEventHandler('catalog', "OnSuccessCatalogImport1C", "OnSuccessCatalogImport1CHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "DoNotUpdate");
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "DoNotAdd");
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", "OnBeforeIBlockSectionUpdateHandler");
AddEventHandler("main", "OnAfterUserAuthorize", "OnAfterUserAuthorizeHandler");


function getPriceMatrix(&$arElement, $arFilterType = array(), $arCurrencyParams = array())
{
    $bCatalog = Loader::includeModule('catalog');
    if (!isset($arElement['PRICE_MATRIX'])) {
        if ($bCatalog) {
            $arElement['PRICE_MATRIX'] = CatalogGetPriceTableEx($arElement['ID'], $arFilterType, "Y", $arCurrencyParams);
            if (isset($arElement['PRICE_MATRIX']['COLS']) && is_array($arElement['PRICE_MATRIX']['COLS'])) {
                foreach ($arElement['PRICE_MATRIX']['COLS'] as $sKeyColumn => $arColumn) {
                    $arElement['PRICE_MATRIX']['COLS'][$sKeyColumn]['NAME_LANG'] = htmlspecialcharsbx($arColumn['NAME_LANG']);
                }
            }
        }
    }
    if (isset($arElement['PRICE_MATRIX'])) {
        if (!empty($arElement['PRICE_MATRIX']['ROWS'])) {
            foreach ($arElement['PRICE_MATRIX']['ROWS'] as $sKeyRow => $arRow) {
                $strMinCode = '';
                $boolStartMin = true;
                $dblMinPrice = 0;
                foreach ($arElement['PRICE_MATRIX']['CAN_BUY'] as $iPriceTypeId) {

                    if (isset($arElement['PRICE_MATRIX']['MATRIX'][$iPriceTypeId][$sKeyRow])) {
                        if ($boolStartMin) {
                            $dblMinPrice = $arElement['PRICE_MATRIX']['MATRIX'][$iPriceTypeId][$sKeyRow]['DISCOUNT_PRICE'];
                            $strMinCode = $iPriceTypeId;
                            $boolStartMin = false;
                        } else {
                            $dblComparePrice = $arElement['PRICE_MATRIX']['MATRIX'][$iPriceTypeId][$sKeyRow]['DISCOUNT_PRICE'];
                            if ($dblMinPrice > $dblComparePrice) {
                                $dblMinPrice = $dblComparePrice;
                                $strMinCode = $iPriceTypeId;
                            }
                        }
                    }
                }
                if ('' != $strMinCode) {
                    $arElement['PRICE_MATRIX']['MATRIX'][$strMinCode][$sKeyRow]['MIN_PRICE'] = 'Y';
                }
            }
        }
    }
    if (isset($arElement['PRICE_MATRIX']['COLS']) && count($arElement['PRICE_MATRIX']['COLS']) > 0) {
        foreach ($arElement['PRICE_MATRIX']['MATRIX'] as $iCol => $arRow) {
            foreach ($arRow as $iRow => $arPrice) {
                $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['DISCOUNT_DIFF'] = $arPrice['PRICE'] - $arPrice['DISCOUNT_PRICE'];
                $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['DISCOUNT_DIFF_PERCENT'] = roundEx(
                    100 * $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['DISCOUNT_DIFF'] / $arPrice['PRICE'],
                    0
                );
                $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['PRINT_VALUE'] = FormatCurrency(
                    $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['PRICE'],
                    $arPrice['CURRENCY']
                );
                $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['PRINT_DISCOUNT_VALUE'] = FormatCurrency(
                    $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['DISCOUNT_PRICE'],
                    $arPrice['CURRENCY']
                );
                $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['PRINT_DISCOUNT_DIFF'] = FormatCurrency(
                    $arElement['PRICE_MATRIX']['MATRIX'][$iCol][$iRow]['DISCOUNT_DIFF'],
                    $arPrice['CURRENCY']
                );
            }
        }
    }
}


function isAlreadyResize(&$file, $arSize, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL, $arFilters = false, $bImmediate = false)
{
    if (!is_array($file) && intval($file) > 0) {
        $file = CFile::GetFileArray($file);
    }

    //Выполняем проверки на корректность входных данных:
    if (!is_array($file) || !array_key_exists("FILE_NAME", $file) || strlen($file["FILE_NAME"]) <= 0) {
        return false;
    }
    if ($resizeType !== BX_RESIZE_IMAGE_EXACT && $resizeType !== BX_RESIZE_IMAGE_PROPORTIONAL_ALT && $resizeType !== BX_RESIZE_IMAGE_PROPORTIONAL) {
        return false;
    }
    if (!is_array($arSize) || intval($arSize["width"]) <= 0 || intval($arSize["height"]) <= 0 || !array_key_exists("width", $arSize) || !array_key_exists("height", $arSize)) {
        return false;
    }
    $arSize["width"] = intval($arSize["width"]);
    $arSize["height"] = intval($arSize["height"]);

    $bFilters = is_array($arFilters) && !empty($arFilters);
    if ($arSize["width"] >= $file["WIDTH"] && $arSize["height"] >= $file["HEIGHT"]) {
        if ($bFilters) {
            $arSize["width"] = $file["WIDTH"];
            $arSize["height"] = $file["HEIGHT"];
            $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL;
        } else {
            $file = array(
                "src" => $file["SRC"],
                "width" => intval($file["WIDTH"]),
                "height" => intval($file["HEIGHT"]),
                "size" => $file["FILE_SIZE"],
            );
            return true;
        }
    }

    if (!is_array($arFilters)) {
        $arFilters = array(array("name" => "sharpen", "precision" => 15),);
    }
    $arResizeParams = array($arSize, $resizeType, array(), false, $arFilters, $bImmediate);
    $callbackData = array();
    $callbackData["cacheID"] = $file["ID"] . "/" . md5(serialize($arResizeParams));
    $callbackData["cacheOBJ"] = new CPHPCache;
    $callbackData["fileDIR"] = "/" . "resize_cache/" . $callbackData["cacheID"] . "/" . $file["SUBDIR"];
    $callbackData["fileNAME"] = $file["FILE_NAME"];
    $callbackData["fileURL"] = $callbackData["fileDIR"] . "/" . $callbackData["fileNAME"];

    $bucket = CCloudStorage::FindBucketForFile($file, $file["FILE_NAME"]);
    if (!is_object($bucket))
        return false;
    if ($bucket->Init()) {
        $callbackData["obTargetBucket"] = $obTargetBucket = $bucket;
    }
    $url = $callbackData["obTargetBucket"]->GetFileSRC($callbackData["fileURL"]);
    $headers = @get_headers($url);
    if ($headers[0] != 'HTTP/1.0 200 OK') {
        return false;
    } else {
        $file["src"] = $url;
        return true;
    }
}

function GetDataForProduct(&$arItems, $params = array())
{
    $bCatalog = Loader::includeModule('catalog');
    if (Loader::includeModule('iblock') && is_array($arItems) && count($arItems) > 0) {
        $arElements = array();
        $arElementsIDs = array();
        foreach ($arItems as $iKeyItem => $arItem) {
            $arElementsIDs[] = $arItem['ID'];
            $arElements[$arItems[$iKeyItem]['ID']] = &$arItems[$iKeyItem];
            if (!empty($arItems[$iKeyItem]['OFFERS']) && is_array($arItems[$iKeyItem]['OFFERS'])) {
                foreach ($arItems[$iKeyItem]['OFFERS'] as $iOfferKey => $arOffer) {
                    $arElementsIDs[] = $arOffer['ID'];
                    $arElements[$arOffer['ID']] = &$arItems[$iKeyItem]['OFFERS'][$iOfferKey];
                }
            }
        }

        $iTime = ConvertTimeStamp(time(), 'FULL');
        if (Loader::includeModule('redsign.quickbuy')) {
            $arFilter = array(
                'DATE_FROM' => $iTime,
                'DATE_TO' => $iTime,
                'QUANTITY' => 0,
                'ELEMENT_ID' => $arElementsIDs,
            );
            $dbRes = CRSQUICKBUYElements::GetList(array('ID' => 'SORT'), $arFilter);
            while ($arData = $dbRes->Fetch()) {
                if (array_key_exists($arData['ELEMENT_ID'], $arElements)) {
                    $arElements[$arData['ELEMENT_ID']]['QUICKBUY'] = $arData;
                    $arElements[$arData['ELEMENT_ID']]['QUICKBUY']['TIMER'] = CRSQUICKBUYMain::GetTimeLimit($arData);
                }
            }
        }

        if (Loader::includeModule('redsign.daysarticle2')) {
            $arFilter = array(
                'DATE_FROM' => $iTime,
                'DATE_TO' => $iTime,
                'QUANTITY' => 0,
                'ELEMENT_ID' => $arElementsIDs,
            );
            $dbRes = CRSDA2Elements::GetList(array('ID' => 'SORT'), $arFilter);
            while ($arData = $dbRes->Fetch()) {
                if (array_key_exists($arData['ELEMENT_ID'], $arElements)) {
                    $arElements[$arData['ELEMENT_ID']]['DAYSARTICLE2'] = $arData;
                    $arElements[$arData['ELEMENT_ID']]['DAYSARTICLE2']['DINAMICA_EX'] = CRSDA2Elements::GetDinamica($arData);
                }
            }
        }

        $arFilesIds = array();
        foreach ($arElements as $iElementId => $arElement) {
            $CODE = false;
            if (isset($arElement['PROPERTIES'][$params['PROP_MORE_PHOTO']]['ID'])) {
                $CODE = $params['PROP_MORE_PHOTO'];
            } elseif (isset($arElement['PROPERTIES'][$params['PROP_SKU_MORE_PHOTO']]['ID'])) {
                $CODE = $params['PROP_SKU_MORE_PHOTO'];
            }

            if ($CODE && isset($arElement['PROPERTIES'][$CODE]['VALUE']) && !is_array($arElement['PROPERTIES'][$CODE]['VALUE']) && IntVal($arElement['PROPERTIES'][$CODE]['VALUE']) > 0) {
                $iFileId = $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'];
                $arFilesIds[$iFileId] = $iFileId;
            } elseif ($CODE && isset($arElement['PROPERTIES'][$CODE]['VALUE']) && is_array($arElement['PROPERTIES'][$CODE]['VALUE']) && count($arElement['PROPERTIES'][$CODE]['VALUE']) > 0) {
                foreach ($arElement['PROPERTIES'][$CODE]['VALUE'] as $iFileKey => $iFileId) {
                    $arFilesIds[$iFileId] = $iFileId;
                }
            }

            if (!is_array($arElements[$iElementId]['PREVIEW_PICTURE']) && intval($arElements[$iElementId]['PREVIEW_PICTURE']) > 0) {
                $iFileId = $arElements[$iElementId]['PREVIEW_PICTURE'];
                $arFilesIds[$iFileId] = $iFileId;
            }
            if (!is_array($arElements[$iElementId]['DETAIL_PICTURE']) && intval($arElements[$iElementId]['DETAIL_PICTURE']) > 0) {
                $iFileId = $arElements[$iElementId]['DETAIL_PICTURE'];
                $arFilesIds[$iFileId] = $iFileId;
            }
        }

        $res = Bitrix\Main\FileTable::getList(array(
            'order' => array('ID' => 'asc'),
            'filter' => array('ID' => $arFilesIds)
        ));
        while ($row = $res->fetch()) {
            $arFilesIds[$row['ID']] = $row;
            if (empty($arFilesIds[$row['ID']]['SRC'])) {
                $arFilesIds[$row['ID']]['SRC'] = CFile::GetFileSRC($row);
            }
        }

        foreach ($arElements as $iElementId => $arElement) {
            $CODE = false;
            if (isset($arElement['PROPERTIES'][$params['PROP_MORE_PHOTO']]['ID'])) {
                $CODE = $params['PROP_MORE_PHOTO'];
            } elseif (isset($arElement['PROPERTIES'][$params['PROP_SKU_MORE_PHOTO']]['ID'])) {
                $CODE = $params['PROP_SKU_MORE_PHOTO'];
            }
            // add images
            if ($CODE && isset($arElement['PROPERTIES'][$CODE]['VALUE']) && !is_array($arElement['PROPERTIES'][$CODE]['VALUE']) && IntVal($arElement['PROPERTIES'][$CODE]['VALUE']) > 0) {
                if (\Bitrix\Main\Config\Option::get("grain.customsettings", "denyCheckResizeImages")) {
                    $propValue = $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'];
                    if (isAlreadyResize($propValue, array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, array()) === FALSE) {
                        $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'] = array(0 => array('RESIZE' => CFile::ResizeImageGet($arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'], array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true, array())));
                    } else {
                        $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'] = array(
                            'src' => $propValue["src"]
                        );
                    }
                } else {
                    $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'] = array(0 => array('RESIZE' => CFile::ResizeImageGet($arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'], array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true, array())));
                }
            } elseif ($CODE && isset($arElement['PROPERTIES'][$CODE]['VALUE']) && is_array($arElement['PROPERTIES'][$CODE]['VALUE']) && count($arElement['PROPERTIES'][$CODE]['VALUE']) > 0) {
                foreach ($arElement['PROPERTIES'][$CODE]['VALUE'] as $iFileKey => $iFileId) {
                    if (\Bitrix\Main\Config\Option::get("grain.customsettings", "denyCheckResizeImages")) {
                        $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'][$iFileKey] = $arFilesIds[$iFileId];
                        $propValue = $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'][$iFileKey];
                        if (isAlreadyResize($propValue, array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, array()) === FALSE) {
                            $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'][$iFileKey]['RESIZE'] = CFile::ResizeImageGet($arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'][$iFileKey], array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true, array());
                        } else {
                            $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'][$iFileKey]['RESIZE'] = array(
                                'src' => $propValue["src"]
                            );
                        }
                    } else {
                        $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'][$iFileKey] = $arFilesIds[$iFileId];
                        $arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'][$iFileKey]['RESIZE'] = CFile::ResizeImageGet($arElements[$iElementId]['PROPERTIES'][$CODE]['VALUE'][$iFileKey], array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true, array());
                    }
                }
            }
            if (!empty($arElements[$iElementId]['PREVIEW_PICTURE'])) {
                if (\Bitrix\Main\Config\Option::get("grain.customsettings", "denyCheckResizeImages")) {
                    $arPreviewPicture = $arElements[$iElementId]['PREVIEW_PICTURE'];
                    $arElements[$iElementId]['PREVIEW_PICTURE'] = (is_array($arElements[$iElementId]['PREVIEW_PICTURE']) > 0 ? $arElements[$iElementId]['PREVIEW_PICTURE'] : $arFilesIds[$arElements[$iElementId]['PREVIEW_PICTURE']]);
                    if (isAlreadyResize($arPreviewPicture, array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, array()) === FALSE) {
                        $arElements[$iElementId]['PREVIEW_PICTURE']['RESIZE'] = CFile::ResizeImageGet($arElements[$iElementId]['PREVIEW_PICTURE'], array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, array());
                    } else {
                        $arElements[$iElementId]['PREVIEW_PICTURE']['RESIZE'] = array(
                            'src' => $arPreviewPicture["src"]
                        );
                    }
                } else {
                    $arElements[$iElementId]['PREVIEW_PICTURE'] = (is_array($arElements[$iElementId]['PREVIEW_PICTURE']) > 0 ? $arElements[$iElementId]['PREVIEW_PICTURE'] : $arFilesIds[$arElements[$iElementId]['PREVIEW_PICTURE']]);
                    $arElements[$iElementId]['PREVIEW_PICTURE']['RESIZE'] = CFile::ResizeImageGet($arElements[$iElementId]['PREVIEW_PICTURE'], array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true, array());
                }
            }
            if (!empty($arElements[$iElementId]['DETAIL_PICTURE'])) {
                if (\Bitrix\Main\Config\Option::get("grain.customsettings", "denyCheckResizeImages")) {
                    $arDetailPicture = $arElements[$iElementId]['DETAIL_PICTURE'];
                    $arElements[$iElementId]['DETAIL_PICTURE'] = (is_array($arElements[$iElementId]['DETAIL_PICTURE']) > 0 ? $arElements[$iElementId]['DETAIL_PICTURE'] : $arFilesIds[$arElements[$iElementId]['DETAIL_PICTURE']]);
                    if (isAlreadyResize($arDetailPicture, array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, array()) === FALSE) {
                        $arElements[$iElementId]['DETAIL_PICTURE']['RESIZE'] = CFile::ResizeImageGet($arElements[$iElementId]['DETAIL_PICTURE'], array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true, array());
                    } else {
                        $arElements[$iElementId]['DETAIL_PICTURE']['RESIZE'] = array(
                            'src' => $arDetailPicture["src"]
                        );
                    }
                } else {
                    $arElements[$iElementId]['DETAIL_PICTURE'] = (is_array($arElements[$iElementId]['DETAIL_PICTURE']) > 0 ? $arElements[$iElementId]['DETAIL_PICTURE'] : $arFilesIds[$arElements[$iElementId]['DETAIL_PICTURE']]);
                    $arElements[$iElementId]['DETAIL_PICTURE']['RESIZE'] = CFile::ResizeImageGet($arElements[$iElementId]['DETAIL_PICTURE'], array('width' => $params['MAX_WIDTH'], 'height' => $params['MAX_HEIGHT']), BX_RESIZE_IMAGE_PROPORTIONAL, true, array());
                }
            }
            if ($params['USE_PRICE_COUNT']) {
                getPriceMatrix($arElements[$iElementId], $params['FILTER_PRICE_TYPES'], $params['CURRENCY_PARAMS']);
            }
            CIBlockPriceTools::setRatioMinPrice($arElements[$iElementId], false);
        }
        if ($bCatalog && ($params['PAGE'] == 'detail' || $params['DETAIL_PICTURE'])) {
            $resSet = CCatalogProductSet::GetList(array(), array('ITEM_ID' => $arElementsIDs));
            while ($arFields = $resSet->fetch()) {
                $iElementId = $arFields['ITEM_ID'];
                $arElements[$iElementId]['HAVE_SET'] = true;
                if (!empty($arElements[$iElementId]['OFFERS'])) {
                    foreach ($arElements[$iElementId]['OFFERS'] as $iOfferKey => $arOffer) {
                        $arElements[$iElementId]['OFFERS'][$iOfferKey]['HAVE_SET'] = true;
                    }
                }
            }
        }
        foreach ($arItems as $iKeyItem => $arItem) {
            $CODE = $params['PROP_MORE_PHOTO'];
            $HAVE_OFFERS = (is_array($arItem['OFFERS']) && count($arItem['OFFERS']) > 0) ? true : false;
            if ($HAVE_OFFERS) {
                $PRODUCT = &$arItem['OFFERS'][0];
            } else {
                $PRODUCT = &$arItem;
            }
            // first image
            $arItems[$iKeyItem]['FIRST_PIC'] = false;
            $arItems[$iKeyItem]['FIRST_PIC_DETAIL'] = false;
            if (isset($PRODUCT['PREVIEW_PICTURE']['RESIZE']) && is_array($PRODUCT['PREVIEW_PICTURE']['RESIZE']) && $params['PAGE'] != 'detail') {
                $arItems[$iKeyItem]['FIRST_PIC'] = $PRODUCT['PREVIEW_PICTURE'];
            } elseif (isset($PRODUCT['DETAIL_PICTURE']['RESIZE']) && is_array($PRODUCT['DETAIL_PICTURE']['RESIZE'])) {
                $arItems[$iKeyItem]['FIRST_PIC'] = $PRODUCT['DETAIL_PICTURE'];
                $arItems[$iKeyItem]['FIRST_PIC_DETAIL'] = $PRODUCT['DETAIL_PICTURE'];
            } elseif ($CODE && isset($PRODUCT['PROPERTIES'][$CODE]['VALUE'][0]['RESIZE']) && is_array($PRODUCT['PROPERTIES'][$CODE]['VALUE'][0]['RESIZE'])) {
                $arItems[$iKeyItem]['FIRST_PIC'] = $PRODUCT['PROPERTIES'][$CODE]['VALUE'][0];
                $arItems[$iKeyItem]['FIRST_PIC_DETAIL'] = $PRODUCT['PROPERTIES'][$CODE]['VALUE'][0];
            } elseif (isset($arItem['PREVIEW_PICTURE']['RESIZE']) && is_array($arItem['PREVIEW_PICTURE']['RESIZE']) && $params['PAGE'] != 'detail') {
                $arItems[$iKeyItem]['FIRST_PIC'] = $arItem['PREVIEW_PICTURE'];
            } elseif (isset($arItem['DETAIL_PICTURE']['RESIZE']) && is_array($arItem['DETAIL_PICTURE']['RESIZE'])) {
                $arItems[$iKeyItem]['FIRST_PIC'] = $arItem['DETAIL_PICTURE'];
                $arItems[$iKeyItem]['FIRST_PIC_DETAIL'] = $arItem['DETAIL_PICTURE'];
            } elseif (!empty($arItem['OFFERS'])) {
                $CODE = $params['PROP_SKU_MORE_PHOTO'];
                foreach ($arItem['OFFERS'] as $arOffer) {
                    if (isset($arOffer['PROPERTIES'][$CODE]['VALUE'][0]['RESIZE']) && is_array($arOffer['PROPERTIES'][$CODE]['VALUE'][0]['RESIZE'])) {
                        $arItems[$iKeyItem]['FIRST_PIC'] = $arOffer['PROPERTIES'][$CODE]['VALUE'][0];
                        $arItems[$iKeyItem]['FIRST_PIC_DETAIL'] = $arOffer['PROPERTIES'][$CODE]['VALUE'][0];
                        break;
                    }
                }
            }
        }
    }
}

function DoNotUpdate(&$arFields)
{

    // получим поле "Запретить перемещение товаров в другие разделы"
    // из допольнитель настроек главного модуля
    $denyMovingToOtherSections = \Bitrix\Main\Config\Option::get("grain.customsettings", "denyMovingToOtherSections");

    if ($_REQUEST['mode'] == 'import') {
        unset($arFields['PREVIEW_PICTURE']);
        unset($arFields['DETAIL_PICTURE']);
        unset(
            $arFields['SORT'],
            $arFields['CODE'],
            $arFields['ACTIVE']
        );

        // запрет на перемещение в другие разделы.
        if ($denyMovingToOtherSections == "Y") {
            unset(
                $arFields['IBLOCK_SECTION_ID'],
                $arFields['IBLOCK_SECTION']
            );
        }
    }
}

//Добавляет свойство в заказ, в которых сохраняются товары, которые остались в корзине пользователя
AddEventHandler("sale", 'OnSaleComponentOrderProperties', "OnSaleComponentOrderProperties");
function OnSaleComponentOrderProperties(&$arUserResult, $request, &$arParams, &$arResult)
{
    $dbBasketItems = \Bitrix\Sale\Basket::getList([
        'select' => ['PRODUCT_ID'],
        'filter' => [
            "DELAY" => "Y",
            '=FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
            '=ORDER_ID' => null,
            '=LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
        ]
    ]);
    $allDeleyedProductIds = array();
    while ($item = $dbBasketItems->Fetch()) {
        $allDeleyedProductIds[] = $item["PRODUCT_ID"];
    }
    if (count($allDeleyedProductIds)) {
        $propertyValues = array();
        $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "ID" => $allDeleyedProductIds), false, false, array("ID", "NAME", "PROPERTY_CML2_ARTICLE"));
        while ($userBasketRecord = $elements->Fetch()) {
            $propertyValues[] = $userBasketRecord["NAME"] . "|" . $userBasketRecord["ID"] . "|" . $userBasketRecord["PROPERTY_CML2_ARTICLE_VALUE"];
        }
        $arUserResult['ORDER_PROP'][2] = $propertyValues;
    }
    if ($arUserResult['ORDER_PROP'][3]) {
        //Подключаем highload-блок (теперь мы делаем ОДНО обращение к нему)
        CModule::IncludeModule('highloadblock');
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
            array(
                "filter" => array(
                    'TABLE_NAME' => 'filial_references'
                )
            )
        )->fetch();
        if (isset($hlblock['ID'])) {
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $user = $entity_data_class::getList(array(
                'select' => array('*'),
                'filter' => array(
                    'UF_DIVISION_NAME' => $arUserResult['ORDER_PROP'][3],
                )))->fetch();
            if ($user["UF_DIVISION_XML_ID"]) {
                $arUserResult['ORDER_PROP'][3] = $user["UF_DIVISION_XML_ID"];
            }
        }
    }
}

AddEventHandler("catalog", "OnProductUpdate", "OnProductUpdate");
function OnProductUpdate($id, $arFields)
{
    CModule::IncludeModule("sale");
    if (isset($arFields["QUANTITY"])) {
        //Если товар в наличии - отправить письмо и изменить записи в корзинах пользователей
        if ($arFields["QUANTITY"] > 0) {
            $rsSites = CSite::GetByID("s1");
            $arSite = $rsSites->Fetch();

            $userBasketRecords = \Bitrix\Sale\Basket::getList([
                'select' => ['PRODUCT_ID', "ID"],
                'filter' => [
                    "PRODUCT_ID" => $id,
                    "ORDER_ID" => null,
                    //Выбираем отложенные товары
                    "DELAY" => 'Y',
                ]
            ]);
            $usersIds = "";
            while ($userBasketRecord = $userBasketRecords->Fetch()) {
                $arFields = array(
                    "DELAY" => "N"
                );
                if ($userBasketRecord["ID"]) {
                    CSaleBasket::Update($userBasketRecord["ID"], $arFields);
                    if (!$usersIds) {
                        $usersIds = $userBasketRecord["USER_ID"];
                    } else {
                        $usersIds .= "|" . $userBasketRecord["USER_ID"];
                    }
                }
            }
            if ($usersIds && $id) {
                $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "ID" => $id), false, false, array("QUANTITY", "ID", "DETAIL_PAGE_URL", "NAME"));
                if ($element = $elements->GetNext()) {
                    $users = CUser::GetList($by = "NAME", $order = "desc", $filter = array("ID" => $usersIds));
                    while ($user = $users->Fetch()) {
                        $arEventFields1 = array(
                            "USER_NAME" => $user["LAST_NAME"] . " " . $user["NAME"],
                            "EMAIL" => $user["EMAIL"],
                            "NAME" => $element["NAME"],
                            "PAGE_URL" => "http://www.aquanimal.ru/" . $element["DETAIL_PAGE_URL"],
                            "DEFAULT_EMAIN_FROM" => $arSite["EMAIL"],
                            "SITE_NAME" => $arSite["NAME"],
                            "SERVER_NAME" => "www.aquanimal.ru",
                        );
//                    CEvent::Send("SALE_SUBSCRIBE_PRODUCT", "s1", $arEventFields1, "Y");

                        $arEventFields2 = array(
                            "USER_NAME" => $user["LAST_NAME"] . " " . $user["NAME"],
                            "EMAIL" => $user["EMAIL"],
                            "NAME" => $element["NAME"],
                            "PAGE_URL" => "http://www.aquanimal.ru/" . $element["DETAIL_PAGE_URL"],
                            "DEFAULT_EMAIN_FROM" => $arSite["EMAIL"],
                            "SITE_NAME" => $arSite["NAME"],
                            "SERVER_NAME" => "www.aquanimal.ru",
                        );
//                    CEvent::Send("PRODUCT_IS_NOW_AVAILABLE", "s1", $arEventFields2, "Y");
                    }
                }
            }
        } else {
            $userBasketRecords = \Bitrix\Sale\Basket::getList([
                'select' => ['ID'],
                'filter' => [
                    "PRODUCT_ID" => $id,
                    "ORDER_ID" => "NULL",
                    "DELAY" => 'N',
                ]
            ]);
            while ($userBasketRecord = $userBasketRecords->Fetch()) {
                $arFields = array(
                    "DELAY" => "Y"
                );
                CSaleBasket::Update($userBasketRecord["ID"], $arFields);
            }
        }
    }
}

function DoNotAdd(&$arFields)
{
    if ($_REQUEST['mode'] == 'import') {
        unset($arFields['PREVIEW_PICTURE']);
        unset($arFields['DETAIL_PICTURE']);
    }
}

function OnBeforeIBlockSectionUpdateHandler(&$arFields)
{
    $denyMovingToOtherSections = \Bitrix\Main\Config\Option::get("grain.customsettings", "denyMovingToOtherSections");

    if ($_REQUEST['mode'] == 'import') {
        // запрет на перемещение в другие разделы.
        if ($denyMovingToOtherSections == "Y") {
            unset(
                $arFields['SORT'],
                $arFields['NAME'],
                $arFields['IBLOCK_SECTION_ID'],
                $arFields['CODE'],
                $arFields['DESCRIPTION'],
                $arFields['ACTIVE'],
                $arFields['PICTURE'],
                $arFields['DETAIL_PICTURE']
            );
        }
    }
}

function OnBeforeCatalogImport1CHandler($arParams, $xmlFilePath)
{
    if (COption::GetOptionString("catalog", "1c_catalog_files_log") == 'Y' || 1) {
        $dir = $_SERVER["DOCUMENT_ROOT"] . '/upload/1c_catalog_files_log/' . date('Y-m-d') . '/';
        mkdir($dir, BX_DIR_PERMISSIONS, true);
        if (file_exists($xmlFilePath)) {
            $pathInfo = pathinfo($xmlFilePath);
            list($pref, $tmp) = explode(' ', $pathInfo['basename'], 2);
            if (empty($pref)) {
                $pref = 'some';
            }
            $newFilePath = $dir . date('Ymd-His') . '__' . $pref . '.xml';
            copy($xmlFilePath, $newFilePath);
        }
    }
}

function OnSuccessCatalogImport1CHandler($arParams, $xmlFilePath)
{
    $descriptionArr = array(
        'method' => __METHOD__,
        'currentTime' => ConvertTimeStamp(time(), "FULL"),
        'currentTimestamp' => time(),
        'arParams' => $arParams,
        'xmlFilePath' => $xmlFilePath,
    );

    \CEventLog::Add(array(
        "SEVERITY" => "SECURITY",
        "AUDIT_TYPE_ID" => "1C_EXCHANGE_LOG",
        "MODULE_ID" => "catalog",
        "ITEM_ID" => "UNKNOWN",
        "DESCRIPTION" => json_encode($descriptionArr),
    ));
}

function logToFile($filepath, $var, $unlink = false)
{
    $logFile = $_SERVER["DOCUMENT_ROOT"] . $filepath;
    $dirpath = dirname($logFile);

    if (!file_exists($dirpath)) {
        mkdir($dirpath);
    }

    if ($unlink) {
        unlink($logFile);
    }
    $body =
        date('Y-m-d H:i:s') . "\n"
        . print_r($var, true) . "\n\n";
    file_put_contents($logFile, $body, FILE_APPEND);
}

function getClientId()
{
    CModule::IncludeModule("main");
    global $USER;
    if ($USER->IsAuthorized()):
        $filter = Array("ID" => $USER->GetID());
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter, array("SELECT" => array("ID", "UF_*")));
        if ($arUser = $rsUsers->Fetch()):
            if ($arUser['UF_OLD_CLIENT_ID']):
                return $arUser['UF_OLD_CLIENT_ID'];
            endif;
        endif;
    endif;

    return false;
}

function getUsers($ID)
{

    CModule::IncludeModule("main");
    $filter = Array("ID" => $ID);
    $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter, array("SELECT" => array("ID", "UF_*")));
    if ($arUser = $rsUsers->Fetch()):

        return $arUser;
    endif;

    return false;
}

//function getOldId

function getUserPrice($GID)
{

    global $DB;
    global $USER;
    if (!$USER->IsAuthorized())
        return false;

    if (!$GID)
        return false;
    if ($CID = getClientId()):
        $results = $DB->Query("SELECT `price` FROM `shop_product_price` WHERE `client_id`=" . $CID . " and `product_id`=" . $GID);
        if ($row = $results->Fetch()):
            return $row['price'];
        endif;
    endif;

    //  return $PRICE1;
}

function getOldId($GID)
{
    global $DB;
    $results = $DB->Query("SELECT `VALUE` FROM `b_iblock_element_property` where `IBLOCK_ELEMENT_ID`=" . $GID . " and `IBLOCK_PROPERTY_ID`=63");
    if ($row = $results->Fetch()):
        return $row['VALUE'];
    endif;

    return false;
}

function is_pay()
{
    global $USER;
    if ($USER->IsAuthorized()):
        return true;
    endif;

    return false;
}

function getPricelist()
{
    global $USER;
    $pricelist = array();
    if ($USER->IsAuthorized()):
        $CLIEND_OLD_ID = getClientId();
        global $DB;
        $results = $DB->Query("SELECT cp.id,cp.name,cp.type,files.guid FROM client_pricelist as cp LEFT JOIN files ON files.id=cp.file WHERE cp.client_id=" . $CLIEND_OLD_ID);
        while ($row = $results->Fetch()):
            $pricelist[] = $row;
        endwhile;

        return $pricelist;
    endif;

    return false;
}

function getBalance()
{
    global $USER;
    $filter = Array("ID" => $USER->GetID());
    $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter, array("SELECT" => array("UF_*")));
    if ($arUser = $rsUsers->Fetch()):
        if ($arUser['UF_OLD_BALANCE'] > 0)
            return $arUser['UF_OLD_BALANCE'];
    endif;

    return 0;
}

function ic($text)
{
    return $text;
}

function ic2($text)
{
    return iconv('cp1251', 'utf-8', $text);
}

function num2str($num)
{
    $nul = 'ноль';
    $ten = array(
        array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
    );
    $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
    $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
    $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
    $unit = array( // Units
        array('копейка', 'копейки', 'копеек', 1),
        array('рубль', 'рубля', 'рублей', 0),
        array('тысяча', 'тысячи', 'тысяч', 1),
        array('миллион', 'миллиона', 'миллионов', 0),
        array('миллиард', 'милиарда', 'миллиардов', 0),
    );
    //
    list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub) > 0) {
        foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
            if (!intval($v))
                continue;
            $uk = sizeof($unit) - $uk - 1; // unit key
            $gender = $unit[$uk][3];
            list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2 > 1)
                $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
            else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk > 1)
                $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
        } //foreach
    } else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
    $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
}

function morph($n, $f1, $f2, $f5)
{
    $n = abs(intval($n)) % 100;
    if ($n > 10 && $n < 20)
        return $f5;
    $n = $n % 10;
    if ($n > 1 && $n < 5)
        return $f2;
    if ($n == 1)
        return $f1;
    return $f5;
}

function createXLS_PDF_Order($orderId, $arFields)
{
    include($_SERVER['DOCUMENT_ROOT'] . '/PHPExcel/PHPExcel.php');

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $activeSheet = $objPHPExcel->getActiveSheet();
    $activeSheet->getStyle('A11')->getAlignment()->setWrapText(true);
    $h1 = new PHPExcel_Style();
    $h2 = new PHPExcel_Style();
    $h3 = new PHPExcel_Style();
    $table_top = new PHPExcel_Style();
    $table_center = new PHPExcel_Style();
    $table_bottom = new PHPExcel_Style();
    $total = $arFields['PRICE'];
    $products = $arFields['BASKET_ITEMS'];
    $ur_order = getUsers($arFields['USER_ID']);
    $prim = "";
    $h1->applyFromArray(
        array(
            'font' => array(
                'bold' => true,
                'size' => 18,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        )
    );
    $h2->applyFromArray(
        array(
            'font' => array(
                'bold' => true,
                'size' => 13,
            )
        )
    );

    $h3->applyFromArray(
        array(
            'font' => array(
                'size' => 10,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        )
    );
    $table_top->applyFromArray(
        array(
            'font' => array(
                'bold' => true,
                'size' => 9,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
        )
    );
    $table_center->applyFromArray(
        array(
            'font' => array(
                'size' => 9,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
        )
    );
    $table_bottom->applyFromArray(
        array(
            'font' => array(
                'size' => 9,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
            ),
        )
    );
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('С')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
    $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('A1', ic('Заявка № ' . $orderId . ' от ' . date("d.m.Y")));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($h1, 'A1:H1');
    $objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('A2', ic('Контрагент: ' . $ur_order['NAME']));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($h2, 'A2:H2');
    $objPHPExcel->getActiveSheet()->mergeCells('A3:H3');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('A3', ic($prim));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($h3, 'A3:H3');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('A4', ic('№'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'A4');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('B4', ic('Артикул'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'B4');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('C4', ic('Штрихкод'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'C4');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('D4', ic('Наименование товара'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'D4');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('E4', ic('Ед. измер'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'E4');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('F4', ic('Кол-во штук'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'F4');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('G4', ic('Цена'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'G4');
    $objPHPExcel->setActiveSheetIndex()->setCellValue('H4', ic('Сумма'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'H4');
    $i = 5;
    $pn = 1;
    foreach ($products as $key => $item) {
        if (!$item["ARTICLE"]) {
            $getListElements = CIBlockElement::GetList(array("NAME" => "ASC"), array("IBLOCK_ID" => 16, "ID" => $item["PRODUCT_ID"]), false, false, array("PROPERTY_CML2_ARTICLE", "PROPERTY_CML2_BAR_CODE"));
            if ($getListElement = $getListElements->GetNext()) {
                $products[$key]["ARTICLE"] = $getListElement["PROPERTY_CML2_ARTICLE_VALUE"];
                $products[$key]["BAR_CODE"] = $getListElement["PROPERTY_CML2_BAR_CODE_VALUE"];
            }
        }
    }
    foreach ($products as $item) {
        $objPHPExcel->setActiveSheetIndex()->setCellValue('A' . $i, $pn);
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_center, 'A' . $i);
        $objPHPExcel->setActiveSheetIndex()->setCellValueExplicit('B' . $i, $item['ARTICLE'], PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_center, 'B' . $i);
        $objPHPExcel->setActiveSheetIndex()->setCellValue('C' . $i, ic($item['BAR_CODE']));
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_center, 'C' . $i);
        $objPHPExcel->setActiveSheetIndex()->setCellValue('D' . $i, ic($item['NAME']));
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_center, 'D' . $i);
        $objPHPExcel->setActiveSheetIndex()->setCellValue('E' . $i, ic('шт'));
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_center, 'E' . $i);
        $objPHPExcel->setActiveSheetIndex()->setCellValue('F' . $i, $item['QUANTITY']);
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_center, 'F' . $i);
        $objPHPExcel->setActiveSheetIndex()->setCellValue('G' . $i, $item['PRICE']);
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_center, 'G' . $i);
        $objPHPExcel->setActiveSheetIndex()->setCellValue('H' . $i, $item['PRICE'] * $item['QUANTITY']);
        $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_center, 'H' . $i);
        $i++;
        $pn++;
    }

    $objPHPExcel->getActiveSheet()->mergeCells('A' . $i . ':G' . $i);
    $objPHPExcel->setActiveSheetIndex()->setCellValue('A' . $i, ic('Всего к оплате:'));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'A' . $i . ':G' . $i);
    $objPHPExcel->setActiveSheetIndex()->setCellValue('H' . $i, $total);
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_top, 'H' . $i);
    $i++;
    $objPHPExcel->getActiveSheet()->mergeCells('A' . $i . ':H' . $i);
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_bottom, 'A' . $i . ':H' . $i);
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($table_bottom, 'H' . $i);
    $i++;
    $i++;
    $objPHPExcel->getActiveSheet()->mergeCells('A' . $i . ':H' . $i);
    $objPHPExcel->setActiveSheetIndex()->setCellValue('A' . $i, ic('Всего к оплате: ' . num2str($total)));
    $objPHPExcel->setActiveSheetIndex()->setSharedStyle($h2, 'A' . $i . ':H' . $i);
    $file_name_xls = 'order' . $orderId . '-' . date("d.m.Y") . '-' . uniqid() . '.xlsx';
    $file = '/files/' . $file_name_xls;
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save($_SERVER['DOCUMENT_ROOT'] . $file);
    $html = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/pdf/tpl/neworder.html');
    $html = ic2($html);
    $html = str_replace("#ORDER_ID#", $orderId, $html);
    $html = str_replace("#ORDER_DATE#", date("d.m.Y"), $html);
    $html = str_replace("#USER_NAME#", $ur_order['NAME'], $html);
    $html = str_replace("#PRIM#", $prim, $html);
    $goods = '';
    $pn = 1;
    foreach ($products as $item) {
        $goods .= '
            <tr valign="top">
                <td>' . $pn . '</td>
                <td>' . $item['ARTICLE'] . '</td>
                <td>' . $item['BAR_CODE'] . '</td>
                <td>' . $item['NAME'] . '</td>
                <td>шт</td>
                <td>' . $item['QUANTITY'] . '</td>
                <td>' . $item['PRICE'] . '</td>
                <td>' . $item['PRICE'] * $item['QUANTITY'] . '</td>
            </tr>';
        $pn++;
    }
    $html = str_replace("#GOODS#", $goods, $html);
    $html = str_replace("#ALL_SUMM#", $total, $html);
    $html = str_replace("#ALL_SUMM_STR#", num2str($total), $html);
    $file_name_htm = 'order' . $orderId . '-' . date("d.m.Y") . '-' . uniqid() . '.pdf';
    $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
    $html = ic($html);
    $dompdf = new Dompdf([
        'fontDir' => '/dompdf/lib/fonts',//указываем путь к папке, в которой лежат скомпилированные шрифты
        'defaultFont' => 'TimesNewRoman',//делаем наш шрифт шрифтом по умолчанию
    ]);
    $dompdf->loadHtml($html, 'UTF-8');
    $dompdf->setPaper('A4', 'landscape');
    $dompdf->render(); // Создаем из HTML PDF
    $html = $dompdf->output();
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/files/' . $file_name_htm, $html);
    global $DB;
    $arFields_N = array(
        "order_id" => $orderId,
        "html" => '"' . $DB->ForSql('/files/' . $file_name_htm) . '"',
        "xlsx" => '"' . $DB->ForSql($file) . '"'
    );
    $DB->Insert("order_files", $arFields_N);
}

AddEventHandler("sale", "OnOrderAdd", "OnOrderAddHand");
function OnOrderAddHand($ID, $arFields)
{
    CModule::IncludeModule("main");
    $user = CUser::GetByID($arFields["USER_ID"])->Fetch();
    ($user["NAME"])
        ? $ORDER_USER = "$user[NAME] $user[LAST_NAME]"
        : $ORDER_USER = $user["EMAIL"];

    $itemIds = [];
    foreach ($arFields["BASKET_ITEMS"] as $key => $arItem) {
        $itemIds[] = $arFields["PRODUCT_ID"];
    }
    $elements = CIBlockElement::GetList(
        false,
        ["IBLOCK_ID" => 16, "ID" => $itemIds],
        false,
        false,
        ["ID", "PROPERTY_CML2_ARTICLE", "PROPERTY_CML2_BAR_CODE"]
    );
    $productData = [];
    while ($element = $elements->Fetch()) {
        $productData[$element["ID"]] = [
            "article" => $element["PROPERTY_CML2_ARTICLE_VALUE"],
            "barcode" => $element["PROPERTY_CML2_BAR_CODE_VALUE"]
        ];
    }
    $ORDER_LIST = "";
    foreach ($arFields["BASKET_ITEMS"] as $key => $arItem) {
        $arItem["QUANTITY"] = intval($arItem["QUANTITY"]);
        $index = $key + 1;
        $article = $productData[$arItem["PRODUCT_ID"]]['article'];
        $barCode = $productData[$arItem["PRODUCT_ID"]]['barcode'];
        $url = $_SERVER["SERVER_NAME"] . $arItem["DETAIL_PAGE_URL"];
        $ORDER_LIST .= "$index) <a href='$url'> $arItem[NAME] ($article|$barCode) - $arItem[QUANTITY] шт. x $arItem[PRICE] руб.</a><br>";
        $itemIds[] = $arFields["PRODUCT_ID"];
    }
    $detailUrlOrder = $_SERVER["SERVER_NAME"] .= "/personal/order/?ID=$ID";
    $ORDER_LIST .= "Вы можете просмотреть свой <a href='$detailUrlOrder'>заказ</a> в личном кабинете<br>";

    $arEventFields = array(
        "ORDER_ID" => $ID,
        "ORDER_DATE" => $arFields["DATE_STATUS"],
        "PRICE" => $arFields["PRICE"] . " руб",
        "ORDER_LIST" => $ORDER_LIST,
        "ORDER_USER" => $ORDER_USER,
        "EMAIL" => $user["EMAIL"],
        "SALE_EMAIL" => "info@aquanimal.ru"
    );
    CEvent::SendImmediate("SALE_NEW_ORDER_UPGRADE", "s1", $arEventFields, "Y");
    createXLS_PDF_Order($ID, $arFields);
}

//Делает цену кастомной, то есть, PRICE не перебиваеся BASE_PRICE
//Если задан id товара, то проверяет наличие товара и делает его отложенным, если его количество равно нулю
AddEventHandler("sale", "OnBeforeBasketAdd", "OnBeforeBasketAdd");
function OnBeforeBasketAdd(&$arFields)
{
    $arFields["CUSTOM_PRICE"] = "Y";
    if ($arFields["PRODUCT_ID"]) {
        $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "ID" => $arFields["PRODUCT_ID"]), false, false, array("QUANTITY"));
        if ($element = $elements->Fetch()) {
            if ((int)$element["QUANTITY"] <= 0) {
                $arFields["DELAY"] = "Y";
            }
        }
    }
}

//Добавляем DETAIL_PAGE_URL товарам в корзине
AddEventHandler("sale", "OnBasketAdd", "OnBasketAdd");
function OnBasketAdd($ID, &$arFields)
{
    CModule::IncludeModule("sale");
    $dbBasketItems = \Bitrix\Sale\Basket::getList([
        'select' => ['DETAIL_PAGE_URL', "PRODUCT_ID"],
        'filter' => [
            "ID" => $ID
        ]
    ]);
    if ($arItems = $dbBasketItems->Fetch()) {
        if (!$arItems["DETAIL_PAGE_URL"] && $arItems["PRODUCT_ID"]) {
            $elements = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "ID" => $arItems["PRODUCT_ID"]), false, false, array("DETAIL_PAGE_URL"));
            if ($element = $elements->GetNext()) {
                if ($element["DETAIL_PAGE_URL"]) {
                    CSaleBasket::Update($ID, array("DETAIL_PAGE_URL" => $element["DETAIL_PAGE_URL"]));
                }
            }
        }
    }
}


function getQuery($key = NULL, $default = NULL)
{
    if (NULL === $key) {
        return $_GET;
    }
    return (isset($_GET[$key])) ? $_GET[$key] : $default;
}

function getOrderXLS_HTML($ID)
{
    global $DB;
    $results = $DB->Query("SELECT * FROM order_files WHERE order_id=" . $ID);
    if ($row = $results->Fetch()):
        return $row;
    endif;
}

AddEventHandler("main", "OnBeforeProlog", "OnBeforePrologHandl");

function OnBeforePrologHandl()
{
    $tmp = explode("?", $_SERVER['REQUEST_URI']);
    $URI = explode("/", $tmp[0]);
    if ($URI[1] == "shop" && $URI[2] == "category" && isset($URI[3])):
        CModule::IncludeModule("iblock");
        $arFilter = array('IBLOCK_ID' => 16, "UF_OLD_ID" => $URI[3]);
        $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
        if ($arSect = $rsSect->GetNext()):
            LocalRedirect($arSect['SECTION_PAGE_URL']);
            die();
        endif;
    endif;

    if ($URI[1] == "shop" && $URI[2] == "product" && isset($URI[3])):
        $arSelect = Array("ID", "IBLOCK_ID", "DETAIL_PAGE_URL");
        $arFilter = Array("IBLOCK_ID" => IntVal(16), "PROPERTY_CML2_TRAITS" => "%" . $URI[3]);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        if ($ob = $res->GetNextElement()):
            $arFields = $ob->GetFields();
            LocalRedirect($arFields['DETAIL_PAGE_URL']);
            die();
        endif;
    endif;
}

// регистрируем обработчик
AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
function BeforeIndexHandler($arFields)
{
    if (!CModule::IncludeModule("iblock")) // подключаем модуль
        return $arFields;
    if ($arFields["MODULE_ID"] == "iblock") {
        $db_props = CIBlockElement::GetProperty(  // Запросим свойства индексируемого элемента
            $arFields["PARAM2"],         // BLOCK_ID индексируемого свойства
            $arFields["ITEM_ID"],          // ID индексируемого свойства
            array("sort" => "asc"),       // Сортировка (можно упустить)
            Array("CODE" => "CML2_ARTICLE")); // CODE свойства (в данном случае артикул)
        if ($ar_props = $db_props->Fetch())
            $arFields["TITLE"] .= " " . $ar_props["VALUE"];   // Добавим свойство в конец заголовка индексируемого элемента
    }
    return $arFields; // вернём изменения
}

function logToFileEvent($filepath, $event, $unlink = false)
{
    $logFile = $_SERVER["DOCUMENT_ROOT"] . $filepath;
    $dirpath = dirname($logFile);

    if (!file_exists($dirpath)) {
        mkdir($dirpath);
    }

    if ($unlink) {
        unlink($logFile);
    }
    file_put_contents($logFile, print_r($event, true) . "\n\n", FILE_APPEND);
}

function PR($o, $toString = false, $ltf = false)
{
    $bt_src = debug_backtrace();
    if ($ltf)
        $bt = $bt_src[1];
    else
        $bt = $bt_src[0];
    $dRoot = $_SERVER["DOCUMENT_ROOT"];
    $dRoot = str_replace("/", "\\", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    $dRoot = str_replace("\\", "/", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);

    $output = '<div style="font-size:9pt; color:#000; background:#fff; border:1px dashed #000;">';
    $output .= '<div style="padding:3px 5px; background:#99CCFF; font-weight:bold;">File: ' . $bt["file"] . ' [' . $bt["line"] . '] ' . date("d.m.Y H:i:s") . '</div>';
    $output .= '<pre style="padding:10px;">' . var_export($o, true) . '</pre>';
    $output .= '</div>';

    if ($toString) return $output;

    echo $output;
}

AddEventHandler('main', 'OnBeforeUserAdd', 'OnBeforeUserAddHandler');
function OnBeforeUserAddHandler(&$arFields)
{
    $arFields["EXTERNAL_AUTH_ID"] = false;
    logToFileEvent('/_logs/add-user.log', array(
        __METHOD__,
        $arFields
    ));
}

AddEventHandler('main', 'OnAfterUserAdd', 'OnAfterUserAddHandler');
function OnAfterUserAddHandler(&$arFields)
{
    logToFileEvent('/_logs/add-user.log', array(
        __METHOD__, $arFields
    ));
    global $DB;
    $DB->Insert("old_users", array(
        "LOGIN" => "'" . $DB->ForSQL($arFields["LOGIN"]) . "'",
        "HASH" => "'" . $DB->ForSQL($arFields["HASH"]) . "'"
    ));
}

AddEventHandler('main', 'OnBeforeUserUpdate', 'OnBeforeUserUpdateHandler');
function OnBeforeUserUpdateHandler(&$arFields)
{
    logToFileEvent('/_logs/add-user.log', array(
        __METHOD__,
        $arFields
    ));
}

AddEventHandler('main', 'OnAfterUserUpdate', 'OnAfterUserUpdateHandler');
function OnAfterUserUpdateHandler(&$arFields)
{
    logToFileEvent('/_logs/add-user.log', array(
        __METHOD__,
        $arFields
    ));
}

AddEventHandler("main", "OnBeforeUserLogin", "__beforeUserLogin");
function __beforeUserLogin(&$arFields)
{
    global $DB;

    if (check_email($arFields["LOGIN"])) {// вход по емейлу
        if ($user = \Bitrix\Main\UserTable::getList(array("filter" => array("=EMAIL" => $arFields["LOGIN"])))->Fetch()) {
            $arFields["LOGIN"] = $user["LOGIN"];
        }
    }

    // ---- Когда таблица old_users станет пустой - её можно будет удалить и отключить этот код ----
    $login = $DB->ForSQL($arFields["LOGIN"]);
    $password = $arFields["PASSWORD"];
    $from_old = "FROM old_users where LOGIN='$login';";

    // Ищем пользователя с таким логином в "old_users"
    $rsOLDUser = $DB->Query("SELECT * $from_old");
    if (($arOLDUser = $rsOLDUser->GetNext())) {
        // Ищем пользователя с таким логином в базе Битрикса
        $rsBXUser = CUser::GetByLogin($login);
        // PR($rsOLDUser);
        if (($arBXUser = $rsBXUser->GetNext())) {
            // Проверяем правильность присланного пароля
            // алгоритмом старой системы

            if (strlen($arOLDUser['HASH']) > 32) {
                $salt = substr($arOLDUser['HASH'], 0, strlen($arOLDUser['HASH']) - 32);
                $db_password = substr($arOLDUser['HASH'], -32);
            } else {
                $salt = "";
                $db_password = $arOLDUser['HASH'];
            }

            $user_password = md5($salt . $password);
            $passwordCorrect = ($db_password === $user_password) || (strtolower($db_password) === strtolower($user_password));

            //PR(array($salt, $password, $arOLDUser, $db_password, $user_password, $passwordCorrect)); exit;

            if ($passwordCorrect) {
                // Обновляем пароль пользователя в базе Битрикса
                // PR(array($arBXUser)); exit;
                $USER = new CUser;
                $bUbdate = $USER->Update($arBXUser["ID"], array("PASSWORD" => $password), false);
                unset($USER);
                if (!$bUbdate)
                    return true;

                // А затем удаляем пользователя из "old_users"
                $DB->Query("DELETE $from_old");
                //return true;
            }
        }
    }
    if (!check_email($arFields["LOGIN"])) {// вход по номеру телефона
        if ($user = \Bitrix\Main\UserTable::getList(array("filter" => array("=PERSONAL_PHONE" => $arFields["LOGIN"])))->Fetch()) {
            $arFields["LOGIN"] = $user["LOGIN"];
        }
    }

}

AddEventHandler("catalog", "OnGetOptimalPrice", 'OnGetOptimalPriceHandler2');
function OnGetOptimalPriceHandler2($productID, $quantity = 1, $arUserGroups = array(), $renewal = "N", $arPrices = array(), $siteID = "s1", $arDiscountCoupons = false)
{
    global $TYPE_PRICE;
    global $USER;
    $TYPE_PRICE = 1;
    $HighloadBlockTableID = 3;
    $HighloadBlockReferencesID = 5;

    $arHLBlock2 = Bitrix\Highloadblock\HighloadBlockTable::getById($HighloadBlockReferencesID)->fetch();
    $obEntity2 = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock2);
    $strEntityDataClass2 = $obEntity2->getDataClass();

    $userID = $USER->GetID();

    $reference = $strEntityDataClass2::getList(array(
        'select' => array('UF_FILIAL_ID'),
        'filter' => array(
            'UF_DIVISION_ID' => $userID,
        )
    ));
    if ($readyReference = $reference->Fetch()) {
        $userID = $readyReference["UF_FILIAL_ID"];
    }

    $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById($HighloadBlockTableID)->fetch();
    $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
    $strEntityDataClass = $obEntity->getDataClass();

    $rsData = $strEntityDataClass::getList(array(
        'select' => array('*'),
        'filter' => array(
            'UF_USER_ID' => $userID,
            'UF_PRODUCT_ID' => $productID,
        )
    ));

    while ($el = $rsData->Fetch()) {
        $price = round($el['UF_PRICE'], 2);

        $arResult = array(
            "PRICE" => array(
                "ID" => $el['UF_PRODUCT_ID'],
                "CATALOG_GROUP_ID" => 1,
                "PRICE" => $price,
                "CURRENCY" => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                "ELEMENT_IBLOCK_ID" => 16,
                'VAT_RATE' => 20,
                'VAT_INCLUDED' => 'Y',
            ),
            "DISCOUNT_PRICE" => 0,
            "DISCOUNT_LIST" => array(),
            //"PRODUCT_ID" => $el['UF_PRODUCT_ID']
            "RESULT_PRICE" => [
                "BASE_PRICE" => $price,
                "DISCOUNT_PRICE" => $price,
                //"DISCOUNT"  => '', //итоговая скидка (разница между BASE_PRICE и DISCOUNT_PRICE)
                //"PERCENT" => '', //итоговая скидка в процентах
                "CURRENCY" => \Bitrix\Currency\CurrencyManager::getBaseCurrency()
            ]
        );
        //PR($arResult);
        return $arResult;
    }
    return true;
}

function getCustomPricesForProducts($productsIds = array())
{
    global $USER;
    $TYPE_PRICE = 1;
    $HighloadBlockTableID = 3;
    $HighloadBlockReferencesID = 5;
    $result = array();

    if (count($productsIds)) {
        $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById($HighloadBlockTableID)->fetch();
        $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();


        $arHLBlock2 = Bitrix\Highloadblock\HighloadBlockTable::getById($HighloadBlockReferencesID)->fetch();
        $obEntity2 = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock2);
        $strEntityDataClass2 = $obEntity2->getDataClass();

        $userID = $USER->GetID();

        $reference = $strEntityDataClass2::getList(array(
            'select' => array('UF_FILIAL_ID'),
            'filter' => array(
                'UF_DIVISION_ID' => $userID,
            )
        ));
        if ($readyReference = $reference->Fetch()) {
            $userID = $readyReference["UF_FILIAL_ID"];
        }

        $rsData = $strEntityDataClass::getList(array(
            'select' => array('*'),
            'filter' => array(
                'UF_USER_ID' => $userID,
                'UF_PRODUCT_ID' => $productsIds,
            )
        ));
        while ($el = $rsData->Fetch()) {
            if ($index = array_search($el['UF_PRODUCT_ID'], $productsIds)) {
                unset($productsIds[$index]);
            }

            $price = round($el['UF_PRICE'], 2);


            $result[$el['UF_PRODUCT_ID']] = array(
                "PRICE" => array(
                    "ID" => $el['UF_PRODUCT_ID'],
                    "CATALOG_GROUP_ID" => 1,
                    "PRICE" => $price,
                    "CURRENCY" => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                    "ELEMENT_IBLOCK_ID" => 16,
                    'VAT_RATE' => 20,
                    'VAT_INCLUDED' => 'Y',
                ),
                "DISCOUNT_PRICE" => 0,
                "DISCOUNT_LIST" => array(),
                "RESULT_PRICE" => [
                    "BASE_PRICE" => $price,
                    "DISCOUNT_PRICE" => $price,
                    "CURRENCY" => \Bitrix\Currency\CurrencyManager::getBaseCurrency()
                ]
            );
        }
        $showBasePriceOption = \Bitrix\Main\Config\Option::get("grain.customsettings", "showBasePriceOption");
        if ($showBasePriceOption === "Y" && count($productsIds)) {
            foreach ($productsIds as $id) {
                $ar_res = CPrice::GetBasePrice($id);
                $price = round($ar_res["PRICE"], 2);
                $result[$id] = array(
                    "PRICE" => array(
                        "ID" => $id,
                        "CATALOG_GROUP_ID" => 1,
                        "PRICE" => $price,
                        "CURRENCY" => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                        "ELEMENT_IBLOCK_ID" => 16,
                        'VAT_RATE' => 20,
                        'VAT_INCLUDED' => 'Y',
                    ),
                    "DISCOUNT_PRICE" => 0,
                    "DISCOUNT_LIST" => array(),
                    //"PRODUCT_ID" => $el['UF_PRODUCT_ID']
                    "RESULT_PRICE" => [
                        "BASE_PRICE" => $price,
                        "DISCOUNT_PRICE" => $price,
                        //"DISCOUNT"  => '', //итоговая скидка (разница между BASE_PRICE и DISCOUNT_PRICE)
                        //"PERCENT" => '', //итоговая скидка в процентах
                        "CURRENCY" => \Bitrix\Currency\CurrencyManager::getBaseCurrency()
                    ]
                );
            }
        }
    }

    return $result;
}


function Custom_EasyAdd2Basket($productID, $quantity, $arParams)
{
    global $APPLICATION;

    $ret = false;
    $productID = IntVal($productID);

    if ($productID > 0 && \Bitrix\Main\Loader::includeModule('sale') && \Bitrix\Main\Loader::includeModule('catalog')) {
        $QUANTITY = 0;
        $product_properties = array();
        $arRewriteFields = array();
        $intProductIBlockID = IntVal(CIBlockElement::GetIBlockByID($productID));
        $successfulAdd = true;

        if ($intProductIBlockID > 0) {
            // PROPERTIES
            if ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y') {
                if ($intProductIBlockID == $arParams["IBLOCK_ID"]) {
                    if (!empty($arParams["PRODUCT_PROPERTIES"])) {
                        $product_properties = CIBlockPriceTools::CheckProductProperties(
                            $arParams["IBLOCK_ID"],
                            $productID,
                            $arParams["PRODUCT_PROPERTIES"],
                            $arParams["PRODUCT_PROPERTIES"],//$_REQUEST[$arParams["PRODUCT_PROPS_VARIABLE"]],
                            $arParams['PARTIAL_PRODUCT_PROPERTIES'] == 'Y'
                        );
                        if (!is_array($product_properties)) {
                            $product_properties = array();
                        }
                    }
                } else {
                    //$skuAddProps = (isset($_REQUEST['basket_props']) && !empty($_REQUEST['basket_props']) ? $_REQUEST['basket_props'] : '');
                    if (!empty($arParams["OFFERS_CART_PROPERTIES"])) {
                        $product_properties = CIBlockPriceTools::GetOfferProperties(
                            $productID,
                            $arParams["IBLOCK_ID"],
                            $arParams["OFFERS_CART_PROPERTIES"]//,
                        //$skuAddProps
                        );
                    }
                }
            }

            // QUANTITY
            if ($arParams["USE_PRODUCT_QUANTITY"]) {
                if (IntVal($quantity) > 0) {
                    $QUANTITY = doubleval($quantity);
                }
            }
            if (0 >= $QUANTITY) {
                $rsRatios = CCatalogMeasureRatio::getList(
                    array(),
                    array('PRODUCT_ID' => $productID),
                    false,
                    false,
                    array('PRODUCT_ID', 'RATIO')
                );
                if ($arRatio = $rsRatios->Fetch()) {
                    $intRatio = IntVal($arRatio['RATIO']);
                    $dblRatio = doubleval($arRatio['RATIO']);
                    $QUANTITY = ($dblRatio > $intRatio ? $dblRatio : $intRatio);
                }
            }
            if (0 >= $QUANTITY)
                $QUANTITY = 1;
        } else {
            $strError = 'CATALOG_ELEMENT_NOT_FOUND';
            $successfulAdd = false;
        }

        if ($successfulAdd) {
            if (!customAdd2BasketByProductId($productID, $QUANTITY, $arRewriteFields, $product_properties)) {
                if ($ex = $APPLICATION->GetException())
                    $strError = $ex->GetString();
                else
                    $strError = 'CATALOG_ERROR2BASKET';
                $successfulAdd = false;
            } else {
                $ret = true;
            }
        }
    }

    return $ret;
}

function priceFormat($priceVal, $needCeil = false)
{
    $tmp = $needCeil ? ceil($priceVal) : $priceVal;
    return number_format($tmp, 2, ",", " ");
}

function customAdd2BasketByProductId($productID, $QUANTITY, $arRewriteFields, $product_properties)
{
    $product = false;
    $skuInfo = CCatalogSKU::GetInfoByIBlock(16);
    $offersIblockID = $skuInfo["IBLOCK_ID"];
    $skuPropID = $skuInfo["SKU_PROPERTY_ID"];

    if ($productID > 0) {
        $arSelect = array("ID", "IBLOCK_ID", "NAME", "IBLOCK_EXTERNAL_ID", "XML_ID");
        //$offersIblock = CIBlockPriceTools::GetOffersIBlock(ForvardAutoTools::getIblockId("catalog"));

        $product = CIBlockElement::GetList(false, array("IBLOCK_ID" => 16, "ID" => $productID), false, false, $arSelect)->Fetch();

        if (!$product && $offersIblockID > 0) {
            $arSelectOffer = array_merge($arSelect, array("PROPERTY_" . $skuPropID));
            $product = CIBlockElement::GetList(false, array("IBLOCK_ID" => $offersIblockID, "ID" => $productID), false, false, $arSelectOffer)->Fetch();
        }
    }

    $basketID = false;
    if ($product) {

        $tmpPrice = CCatalogProduct::GetOptimalPrice($productID);
        $PRICE = NULL;

        if ($tmpPrice && isset($tmpPrice["RESULT_PRICE"]["DISCOUNT_PRICE"])) {
            $PRICE = $tmpPrice["RESULT_PRICE"]["DISCOUNT_PRICE"];
        }

        $arFields = array(
            "PRODUCT_ID" => $productID,
            "DELAY" => "N",
            "CAN_BUY" => "Y",
            "PRICE" => $PRICE,
            "QUANTITY" => $QUANTITY,
            "CURRENCY" => "RUB",
            "LID" => SITE_ID,
            "NAME" => $product["NAME"],
            "MODULE" => "catalog",
            //"PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider",
            "CUSTOM_PRICE" => "Y",
            "PRODUCT_XML_ID" => $product["XML_ID"],
            "CATALOG_XML_ID" => $product["IBLOCK_EXTERNAL_ID"]
        );
        $arFields["PROPS"] = $product_properties;

        $basketID = CSaleBasket::Add($arFields);

    }

    return $basketID;
}


// регистрируем последнюю активность пользователя, если модуль соцсетей не установлен
if (!IsModuleInstalled('socialnetwork')) {
    AddEventHandler('main', 'OnBeforeProlog', 'CustomSetLastActivityDate');
    function CustomSetLastActivityDate()
    {
        if ($GLOBALS['USER']->IsAuthorized()) {
            CUser::SetLastActivityDate($GLOBALS['USER']->GetID());
        }
    }
}


// клиент вошёл в ЛК, отправляем уведомление на почту об этом
AddEventHandler("main", "OnAfterUserAuthorize", "OnAfterUserLoginHandler");
function OnAfterUserLoginHandler($arUser)
{
    if (!CUser::IsOnLine($arUser["user_fields"]["ID"], 1500)) {
        if (!$arUser["save"]) {
            logToFileEvent('/_logs/auth-user.log', $arUser);
            $arEventsFields = array(
                "USER_ID" => $arUser["user_fields"]["ID"],
                "LOGIN" => $arUser["user_fields"]["LOGIN"],
                "NAME" => $arUser["user_fields"]["NAME"],
                "LAST_NAME" => $arUser["user_fields"]["LAST_NAME"],
                "EMAIL" => $arUser["user_fields"]["EMAIL"]
            );
            CEvent::Send("USER_LOGGED_IN", "s1", $arEventsFields, "Y", "", "");
            logToFileEvent('/_logs/auth-user.log', array(__METHOD__, $arEventsFields));
        }
    } else {
        CUser::SetLastActivityDate($arUser["user_fields"]["ID"]);
    }
    if ($_SERVER["SCRIPT_NAME"] == "/auth/index.php") {
        LocalRedirect("/catalog/");
    }
}

// клиент вышел из ЛК, отправляем уведомление на почту об этом
AddEventHandler("main", "OnAfterUserLogout", "OnAfterUserLogoutHandler");
function OnAfterUserLogoutHandler($arParams)
{
    global $USER, $DB;
    if (isset($arParams["USER_ID"])) {
        $rsUser = $USER->GetByID($arParams["USER_ID"]);
        $arUser = $rsUser->Fetch();

        $arEventsFields = array(
            "USER_ID" => $arUser["ID"],
            "LOGIN" => $arUser["LOGIN"],
            "NAME" => $arUser["NAME"],
            "LAST_NAME" => $arUser["LAST_NAME"],
            "EMAIL" => $arUser["EMAIL"]
        );

        CEvent::Send("USER_LOGGED_OUT", "s1", $arEventsFields, "Y", "", "");
        logToFileEvent('/_logs/auth-user.log', array(__METHOD__, $arEventsFields));

        $strSqlPrefix = "UPDATE b_user SET " .
            "LAST_ACTIVITY_DATE = NULL WHERE ID IN (" . $arUser["ID"] . ")";
        $DB->Query($strSqlPrefix, false, "", array("ignore_dml" => true));

    } else {
        logToFileEvent('/_logs/auth-user.log', array(
            __METHOD__, "Сообщение на почту не отправилось: " . $arParams
        ));
    }
}

// уведомление на почту после создания заказа и всех его параметров, после отправки письма, но до редиректа.
AddEventHandler("sale", "OnSaleComponentOrderOneStepComplete", "OnSaleComponentOrderOneStepCompleteHandler");

function OnSaleComponentOrderOneStepCompleteHandler($ID, $arOrder, $arParams)
{
    global $USER;

    if ($USER->IsAuthorized()) {

        $rsUser = $USER->GetByID($arOrder["USER_ID"]);;
        $arUser = $rsUser->Fetch();

        if (!isset($arUser["LAST_NAME"]) && !isset($arUser["NAME"]) && !isset($arUser["SECOND_NAME"])) {
            $nameUser = $arUser["LAST_NAME"] . " " . $arUser["NAME"] . " " . $arUser["SECOND_NAME"] . " ";

            $arEventsFields = array(
                "USER_LOGIN" => $nameUser,
                "USER_ID" => $arOrder["USER_ID"],
                "ORDER_ID" => $ID,
                "USER_NAME" => $nameUser
            );
        } else {

            $arEventsFields = array(
                "USER_LOGIN" => $USER->GetLogin(),
                "USER_ID" => $arOrder["USER_ID"],
                "ORDER_ID" => $ID,
                "USER_NAME" => $USER->GetFullName()
            );
        }

        logToFileEvent('/_logs/sale-user.log', array(
            __METHOD__, $arEventsFields
        ));

        CEvent::Send("ORDER_PLACE", "s1", $arEventsFields, "Y", "", "");
        unset($arEventsFields);
    }
    $props = CSaleOrderProps::GetList(
        array("SORT" => "ASC"),
        array(
            "CODE" => "comment"
        ),
        false,
        false,
        array()
    );
    if ($prop = $props->Fetch()) {
        $orderProps = CSaleOrderPropsValue::GetOrderProps($ID);
        while ($orderProp = $orderProps->Fetch()) {
            if ($orderProp["CODE"] == "comment") {
                CSaleOrderPropsValue::Update($orderProp['ID'], array(
                    'NAME' => $orderProp['NAME'],
                    'CODE' => $orderProp['CODE'],
                    'ORDER_PROPS_ID' => $orderProp['ORDER_PROPS_ID'],
                    'ORDER_ID' => $ID,
                    'VALUE' => $arOrder["USER_DESCRIPTION"],
                ));
                break;
            }
        }
    }
}

//Пересчет цен в корзине при обновлении страницы
AddEventHandler("main", "OnProlog", "recountUserPricesInBasket");
function recountUserPricesInBasket()
{
    if (CModule::IncludeModule('sale')) {
        $productIds = $productPrices = [];
        $dbBasketItems = \Bitrix\Sale\Basket::getList([
            'select' => ['PRODUCT_ID', "ID", "PRICE"],
            'filter' => [
                '=FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
                '=ORDER_ID' => null,
                '=LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
            ]
        ]);
        while ($item = $dbBasketItems->fetch()) {
            $productIds[$item["ID"]] = $item["PRODUCT_ID"];
            $productPrices[$item["ID"]] = $item["PRICE"];
        }
        global $USER;
        if (count($productIds)) {
            $customPrices = getCustomPricesForProducts($productIds);
            foreach ($productIds as $id => $productID) {
                if ($customPrices[$productID]["RESULT_PRICE"]["DISCOUNT_PRICE"]
                    && $productPrices[$id] != $customPrices[$productID]["RESULT_PRICE"]["DISCOUNT_PRICE"]
                    && $id
                    && $productID
                ) {
                    CSaleBasket::Update($id, ["PRICE" => $customPrices[$productID]["RESULT_PRICE"]["DISCOUNT_PRICE"]]);
                }
            }
        }
    }
}

function smartSearch($query, $oldResults)
{
    $query .= "*";
    $obSearch = new CSearch;
    $obSearch->SetOptions(array(//мы добавили еще этот параметр, чтобы не ругался на форматирование запроса
        'ERROR_ON_EMPTY_STEM' => false,
    ));
    $obSearch->Search(array(
        'QUERY' => $query,
        'SITE_ID' => SITE_ID,
        'MODULE_ID' => 'iblock',
        "PARAM1" => "1c_catalog_new",
        'PARAM2' => 16
    ));
    if (!$obSearch->selectedRowsCount()) {//и делаем резапрос, если не найдено с морфологией...
        $obSearch->Search(array(
            'QUERY' => $query,
            'SITE_ID' => SITE_ID,
            'MODULE_ID' => 'iblock',
            "PARAM1" => "1c_catalog_new",
            'PARAM2' => 16
        ), array(), array('STEMMING' => false));//... уже с отключенной морфологией
    }
    $allIdsOfProducts = array();
    while ($row = $obSearch->fetch()) {
        $key = array_search($row["ITEM_ID"], $oldResults);
        if ($key === FALSE || $key === NULL) {
            $allIdsOfProducts[] = $row["ITEM_ID"];
        }
    }
    if (count($allIdsOfProducts)) {
        return $allIdsOfProducts;
    } else {
        return false;
    }
}