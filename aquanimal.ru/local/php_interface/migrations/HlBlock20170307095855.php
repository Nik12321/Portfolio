<?php

namespace Sprint\Migration;


class HlBlock20170307095855 extends Version {

    protected $description = "Создание HlBlock для хранения индивидуальных цен для клиентов";

    public function up(){
        $helper = new HelperManager();

        $hlblockId = $helper->Hlblock()->addHlblockIfNotExists(array(
            'NAME' => 'ClientPrice',
            'TABLE_NAME' => 'hl_client_price',
        ));
        $helper->UserTypeEntity()->addUserTypeEntityIfNotExists('HLBLOCK_' . $hlblockId, 'UF_USER_ID', array(
            'USER_TYPE_ID' => 'integer'
        ));
        $helper->UserTypeEntity()->addUserTypeEntityIfNotExists('HLBLOCK_' . $hlblockId, 'UF_PRODUCT_ID', array(
            'USER_TYPE_ID' => 'integer'
        ));
        $helper->UserTypeEntity()->addUserTypeEntityIfNotExists('HLBLOCK_' . $hlblockId, 'UF_PRICE', array(
            'USER_TYPE_ID' => 'string'
        ));
		
		$helper->UserTypeEntity()->addUserTypeEntityIfNotExists('HLBLOCK_' . $hlblockId, 'UF_DELETE', array(
            'USER_TYPE_ID' => 'boolean'
        ));
    }

    public function down(){
        $helper = new HelperManager();

        $helper->Hlblock()->deleteHlblockIfExists('ClientPrice');

    }

}
