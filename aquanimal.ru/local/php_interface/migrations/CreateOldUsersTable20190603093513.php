<?php

namespace Sprint\Migration;


class CreateOldUsersTable20190603093513 extends Version
{
    protected $description = "Creating table for old users to smooth migration";

    public function up() {
      $helper = new HelperManager();

			$connection = \Bitrix\Main\Application::getConnection();
      $sqlHelper = $connection->getSqlHelper();
			
			// $sql = 'SELECT LOGIN FROM b_user WHERE ACTIVE = \''.$this->sqlHelper->forSql('Y', 1).'\' ';
			
			$sql = 'CREATE TABLE `old_users` (
				`LOGIN` varchar(60) NOT NULL,
				`HASH` varchar(64) NOT NULL,
				PRIMARY KEY (`LOGIN`)
			);';
			$connection->queryExecute($sql);
    }

    public function down() {
	    $helper = new HelperManager();

	    $connection = \Bitrix\Main\Application::getConnection();
	    $sqlHelper = $connection->getSqlHelper();
		
			$sql = 'DROP TABLE IF EXISTS `old_users`;';
			$connection->queryExecute($sql);
    }
}
