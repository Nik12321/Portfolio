<?php

namespace Sprint\Migration;


class Banner_prop20200303071113 extends Version
{

    protected $description = "";

    public function up() {
        $helper = $this->getHelperManager();

    
            $iblockId = $helper->Iblock()->getIblockIdIfExists('banners','presscenter');
    
    
                $helper->Iblock()->saveProperty($iblockId, array (
  'NAME' => 'Параметры показа баннера',
  'ACTIVE' => 'Y',
  'SORT' => '500',
  'CODE' => 'SHOW_PARAMETER',
  'DEFAULT_VALUE' => '',
  'PROPERTY_TYPE' => 'L',
  'ROW_COUNT' => '1',
  'COL_COUNT' => '30',
  'LIST_TYPE' => 'C',
  'MULTIPLE' => 'N',
  'XML_ID' => '',
  'FILE_TYPE' => '',
  'MULTIPLE_CNT' => '5',
  'LINK_IBLOCK_ID' => '0',
  'WITH_DESCRIPTION' => 'N',
  'SEARCHABLE' => 'N',
  'FILTRABLE' => 'N',
  'IS_REQUIRED' => 'Y',
  'VERSION' => '1',
  'USER_TYPE' => NULL,
  'USER_TYPE_SETTINGS' => NULL,
  'HINT' => '',
  'VALUES' => 
  array (
    0 => 
    array (
      'VALUE' => 'Показывать всем',
      'DEF' => 'Y',
      'SORT' => '500',
      'XML_ID' => 'for_all',
    ),
    1 => 
    array (
      'VALUE' => 'Показывать только авторизованным пользователям',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'only_auth',
    ),
    2 => 
    array (
      'VALUE' => 'Показывать только НЕ авторизованным пользователям (гостям)',
      'DEF' => 'N',
      'SORT' => '500',
      'XML_ID' => 'only_guests',
    ),
  ),
));
        
    
    
    
        }

    public function down()
    {
        $helper = $this->getHelperManager();

        //your code ...
    }

}
