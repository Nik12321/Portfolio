<?php

namespace Sprint\Migration;


class Version20200117105452 extends Version
{

    protected $description = "";

    public function up()
    {
        $helper = $this->getHelperManager();

        
                $helper->Event()->saveEventType('USER_LOGGED_IN', array (
  'LID' => 'ru',
  'EVENT_TYPE' => 'email',
  'NAME' => 'Клиент вошёл',
  'DESCRIPTION' => '#USER_ID# - ID пользователя
#STATUS# - Статус логина
#LOGIN# - Логин
#URL_LOGIN# - Логин, закодированный для использования в URL
#NAME# - Имя
#LAST_NAME# - Фамилия
#EMAIL# - E-Mail пользователя',
  'SORT' => '150',
));
                $helper->Event()->saveEventType('USER_LOGGED_IN', array (
  'LID' => 'en',
  'EVENT_TYPE' => 'email',
  'NAME' => ' User logged in',
  'DESCRIPTION' => '#USER_ID# - ID пользователя
#STATUS# - Статус логина
#MESSAGE# - Сообщение пользователю
#LOGIN# - Логин
#URL_LOGIN# - Логин, закодированный для использования в URL
#CHECKWORD# - Контрольная строка для смены пароля
#NAME# - Имя
#LAST_NAME# - Фамилия
#EMAIL# - E-Mail пользователя',
  'SORT' => '150',
));
        
        
            }

    public function down()
    {
        $helper = $this->getHelperManager();

        //your code ...
    }

}
