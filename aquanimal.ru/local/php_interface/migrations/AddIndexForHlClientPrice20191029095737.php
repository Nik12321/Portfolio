<?php

namespace Sprint\Migration;

use CModule;

class AddIndexForHlClientPrice20191029095737 extends Version
{

    protected $description = "";

    public function up()
    {
        CModule::IncludeModule("iblock");
        CModule::IncludeModule('highloadblock');
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList([
            'filter' => ['=NAME' => "ClientPrice"]
        ])->fetch();
        if(!$hlblock){
            throw new \Exception('HL-block with name "ClientPrice" not found');
        }
        $hlName = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDBTableName();
        global $DB;
        $strSql = '
    CREATE 
        INDEX UF_USER_ID_UF_PRODUCT_ID 
                    ON ' . $hlName . ' (UF_USER_ID, UF_PRODUCT_ID)';
        $res = $DB->Query($strSql, true);
        if(!$res){
            throw new \Exception('Error. Index already exist');
        }
    }

    public function down()
    {
        CModule::IncludeModule("iblock");
        CModule::IncludeModule('highloadblock');
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList([
            'filter' => ['=NAME' => "ClientPrice"]
        ])->fetch();
        if(!$hlblock){
            throw new \Exception('HL-block with name "ClientPrice" not found');
        }
        $hlName = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDBTableName();
        global $DB;
        $strSql = '
    DROP  
        INDEX UF_USER_ID_UF_PRODUCT_ID 
                    ON ' . $hlName;
        $res = $DB->Query($strSql, true);
        if(!$res){
            throw new \Exception('Error. Index not exist');
        }
    }

}
