<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;

logToFile("/_logs/changestatus_".date('Y-m-d').".txt",array(
    $_POST,
    $_REQUEST,
    $_GET,
));

if ($_POST['login'] && $_POST['password']) {
    $auth = $USER->Login($_POST['login'], $_POST['password'], "N");
    if ($auth == 1) {
        $strGroups = $USER->GetUserGroupString();
        $result = explode( ',', $strGroups);
        if (in_array("8", $result)) {
            if ($USER->IsAuthorized()) {
                if(CModule::IncludeModule('sale')) {
                    if ($_POST['orderID'] && $_POST['status']) {
                        $order = \Bitrix\Sale\Order::load($_POST['orderID']);
                        $finID = $_POST["finID"];
                        if (!empty($order)) {
                            $r = $order->setField('STATUS_ID', $_POST['status']);
                            $r = $order->save();
                            echo 'Статус успешно обновлен!';
                        }
                        else {
                            echo 'Заказа не существует';
                        }
                        if ($finID && $arOrderProps = CSaleOrderProps::GetByID(4)) {
                            $db_vals = CSaleOrderPropsValue::GetList(array(), array('ORDER_ID' => $_POST['orderID'], 'ORDER_PROPS_ID' => $arOrderProps['ID']));
                            if ($arVals = $db_vals->Fetch()) {
                                CSaleOrderPropsValue::Update($arVals['ID'], array(
                                    'NAME' => $arVals['NAME'],
                                    'CODE' => $arVals['CODE'],
                                    'ORDER_PROPS_ID' => $arVals['ORDER_PROPS_ID'],
                                    'ORDER_ID' => $arVals['ORDER_ID'],
                                    'VALUE' => $finID,
                                ));
                            }
                            else {
                                CSaleOrderPropsValue::Add(array(
                                    'NAME' => $arOrderProps['NAME'],
                                    'CODE' => $arOrderProps['CODE'],
                                    'ORDER_PROPS_ID' => $arOrderProps['ID'],
                                    'ORDER_ID' => $_POST['orderID'],
                                    'VALUE' => $finID,
                                ));
                            }
                        }
                    }
                    else{
                        echo'Произошла ошибка, данные для обработки заказа (orderID и/или status) не поступили';
                    }
                }
                else {
                    echo'Произошла ошибка, модуль sale не удалось подключить';
                }
            }
            else{
                echo "Авторизация не удалась, попробуйте еще раз";
            }
        }
        else{
            echo "У вас нет достаточных прав доступа.";
        }
    }
    else{
        echo 'Неверный логин или пароль';
    }
}
else{
    echo 'Данные для входа не поступили в обработчик.';
}
?>