<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle("Контакты");
?>
    <div itemscope itemtype="http://schema.org/Organization" style="line-height:18px;">
        <span itemprop="name" style="display:none">Aquanimal</span>
        <link itemprop="url" href="https://aquanimal.ru/" style="display:none">
        <div>
            <p>
                Многоканальный телефон для связи с любым сотрудником фирмы<br>
                <b itemprop="telephone">(495) 308-99-00</b>
            </p>
            <p itemprop="telephone">
                8-800-550-2156<br>
                <b>Звонок бесплатный</b>
            </p>
            <p>
                Любые вопросы отправляйте на <a href="mailto:info@aquanimal.ru"><span itemprop="email">info@aquanimal.ru</span></a><br>
                Также Вы можете связаться с <a href="/sotrudniki/">менеджерами по электронной почте, icq или skype</a>.
            </p>
            <p>
                Мы работаем по будням с <b>09:00 до 18:00</b>
            </p>
            <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                Адрес компании:
                <span itemprop="addressLocality">Россия, Москва,</span>
                <span itemprop="streetAddress">Минская улица Вл, 5,</span>
                <span itemprop="postalCode">119590</span>
            </div>
            <?
            $APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	"gopro", 
	array(
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.72756144273174;s:10:\"yandex_lon\";d:37.51645711864898;s:12:\"yandex_scale\";i:18;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.516457118649;s:3:\"LAT\";d:55.727561442745;s:4:\"TEXT\";s:55:\"Минская улица Вл, 5, Москва, 119590\";}}}",
		"MAP_WIDTH" => "750",
		"MAP_HEIGHT" => "500",
		"CONTROLS" => array(
			0 => "ZOOM",
			1 => "SMALLZOOM",
			2 => "MINIMAP",
			3 => "TYPECONTROL",
			4 => "SCALELINE",
		),
		"OPTIONS" => array(
			0 => "ENABLE_DBLCLICK_ZOOM",
			1 => "ENABLE_DRAGGING",
		),
		"MAP_ID" => "contacts",
		"COMPONENT_TEMPLATE" => "gopro",
		"API_KEY" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);
            ?>
        </div>
    </div>
    <br>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>