<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Просмотренные товары");
global $arrAlreadyViewed;
$APPLICATION->IncludeComponent("bitrix:sale.viewed.product", "filter", [
    "VIEWED_COUNT" => "10000",
    "VIEWED_NAME" => "Y",
    "VIEWED_IMAGE" => "Y",
    "VIEWED_PRICE" => "Y",
    "VIEWED_CURRENCY" => "default",
    "VIEWED_CANBUY" => "N",
    "VIEWED_CANBUSKET" => "N",
    "VIEWED_IMG_HEIGHT" => "75",
    "VIEWED_IMG_WIDTH" => "75",
    "BASKET_URL" => "/personal/cart/",
    "ACTION_VARIABLE" => "action",
    "PRODUCT_ID_VARIABLE" => "id",
    "SET_TITLE" => "N"
],
    false
);
?>
<div class="pmenu">
    <?php
    $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "personal",
        [
            "ROOT_MENU_TYPE" => "personal",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => [],
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "",
            "USE_EXT" => "N",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N",
            "SEPARATORS_PLACE" => [
                0 => "3",
                1 => "7",
                2 => ""
            ],
            "COMPONENT_TEMPLATE" => "personal"
        ],
        false
    );
    ?>
</div>
<div class="pcontent">
    <?php global $rsGoProViewedFilter;?>
    <?php
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.viewed.products",
        "gopro",
        [
            "ACTION_VARIABLE" => "action",
            "ADDITIONAL_PICT_PROP_7" => "DOCS",
            "ADDITIONAL_PICT_PROP_#IBLOCK_ID_offers#" => "MORE_PHOTO",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "BASKET_URL" => "/personal/cart/",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CART_PROPERTIES_7" => [],
            "CART_PROPERTIES_#IBLOCK_ID_offers#" => [],
            "COLUMNS5" => "N",
            "COMPONENT_TEMPLATE" => "gopro",
            "CONVERT_CURRENCY" => "N",
            "DEPTH" => "",
            "DETAIL_URL" => "",
            "DONT_SHOW_LINKS" => "N",
            "EMPTY_ITEMS_HIDE_FIL_SORT" => "N",
            "HIDE_NOT_AVAILABLE" => "N",
            "IBLOCK_ID" => "16",
            "IBLOCK_TYPE" => "1c_catalog_new",
            "LABEL_PROP_7" => "-",
            "MAIN_TITLE" => "",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MIN_AMOUNT" => "10",
            "OFFER_TREE_PROPS_8" => [
                0 => "COLOR_DIRECTORY",
                1 => "SKU_SIZE_MEMORY",
            ],
            "OFF_MEASURE_RATION" => "Y",
            "OFF_SMALLPOPUP" => "N",
            "PAGE_ELEMENT_COUNT" => "30",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => [0 => "BASE"],
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "PROPERTY_CODE_7" => [],
            "PROPERTY_CODE_#IBLOCK_ID_offers#" => [],
            "PROPS_ATTRIBUTES" => [0 => "CML2_ARTICLE"],
            "PROPS_ATTRIBUTES_COLOR" => [],
            "PROP_ACCESSORIES" => "-",
            "PROP_ARTICLE" => "CML2_ARTICLE",
            "PROP_MORE_PHOTO" => "MORE_PHOTO",
            "PROP_SKU_ARTICLE" => "CML2_ARTICLE",
            "PROP_SKU_MORE_PHOTO" => "MORE_PHOTO",
            "SECTION_CODE" => "",
            "SECTION_ELEMENT_CODE" => "",
            "SECTION_ELEMENT_ID" => "",
            "SECTION_ID" => "",
            "SHOW_DISCOUNT_PERCENT" => "Y",
            "SHOW_ERROR_EMPTY_ITEMS" => "Y",
            "SHOW_FROM_SECTION" => "N",
            "SHOW_IMAGE" => "Y",
            "SHOW_NAME" => "Y",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "SHOW_PRODUCTS_7" => "Y",
            "USE_AUTO_AJAXPAGES" => "N",
            "USE_FAVORITE" => "Y",
            "USE_MIN_AMOUNT" => "Y",
            "USE_PRODUCT_QUANTITY" => "N",
            "USE_SHADOW_ON_HOVER" => "N",
            "USE_SHARE" => "Y",
            "USE_STORE" => "Y",
            "VIEW" => "showcase",
            "SHOW_PRODUCTS_16" => "Y",
            "PROPERTY_CODE_16" => [0 => "CML2_ARTICLE"],
            "CART_PROPERTIES_16" => [0 => "CML2_MANUFACTURER"],
            "ADDITIONAL_PICT_PROP_16" => "MORE_PHOTO",
            "LABEL_PROP_16" => "-",
            "ADDITIONAL_PICT_PROP_17" => "MORE_PHOTO"
        ],
        false
    );
    ?>
    <?php $APPLICATION->ShowViewContent('paginator');?>
</div>
<br>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
