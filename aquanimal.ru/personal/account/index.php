<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
?>
<div class = "pmenu">
    <?$APPLICATION->IncludeComponent(
        "bitrix:menu",
        "personal",
        array(
            "ROOT_MENU_TYPE" => "personal",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => array(
            ),
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "",
            "USE_EXT" => "N",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N",
            "SEPARATORS_PLACE" => array(
                0 => "3",
                1 => "7",
                2 => "10",
                3 => "",
            ),
            "COMPONENT_TEMPLATE" => "personal"
        ),
        false
    );?>
</div>
<?
global $USER;
$ar = CSaleUserAccount::GetByUserID($USER->GetID(), "RUB");
?>
<div class = "pcontent">
    <div class = "account-wrapper">
        Ваш баланс: <?=($ar) ? number_format($ar["CURRENT_BUDGET"], 2, '.', '') : 0?> руб.
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
