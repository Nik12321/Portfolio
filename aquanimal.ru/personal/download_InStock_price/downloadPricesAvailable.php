<?php

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); // include  bitrix prolog (clasess)

//Подключаем модуль
include($_SERVER['DOCUMENT_ROOT'] . '/PHPExcel/PHPExcel.php');

//Подключаем стили
include($_SERVER['DOCUMENT_ROOT'] . '/createXlsDocuments/stylesXSL.php');

//Подключаем функции
include($_SERVER['DOCUMENT_ROOT'] . '/createXlsDocuments/functions.php');

//Получаем номера телефонов
$phone1 = \Bitrix\Main\Config\Option::get("grain.customsettings","phone1");
$phone2 = \Bitrix\Main\Config\Option::get("grain.customsettings","phone2");

//Получаем все переменные, необходимые для работы
global $USER;
$siteUrl = "https://" . $_SERVER['SERVER_NAME'];
$index = $startIndex = 5;
$sections = $allPrices = $allElements = $allSections['ROOT'] = array();

//Создаем файл
$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setShowSummaryBelow(false);
$activeSheet = $objPHPExcel->getActiveSheet();
$activeSheet->getStyle('A11')->getAlignment()->setWrapText(true);

//Задаем ширину колонок
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(0);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(100);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);

//Задаем заголовок
$title = "Прайс - лист ООО 'Аква Энимал'";
if ($phone1 && $phone2) {
    $title .= "  тел. " . $phone1 . ", " . $phone2;
}
else if ($phone1) {
    $title .= "  тел. " . $phone1;
}
else if ($phone2) {
    $title .= "  тел. " . $phone2;
}

$objPHPExcel->getActiveSheet()->mergeCells('B1:G1');
$objPHPExcel->setActiveSheetIndex()->setCellValue('B1', ic($title));
$objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["title"], 'A1:G1');

//Задаем данные о файле: имя клиента, дата и т.д.
$name = $USER->GetFullName();
$objPHPExcel->getActiveSheet()->mergeCells('B2:C2');
$objPHPExcel->setActiveSheetIndex()->setCellValue('B2', ic("Для " . $name . ". Цены на " . date('d.m.Y')));
$objPHPExcel->setActiveSheetIndex()->setCellValue('D2', ic("ИТОГО:"));
$objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["normalWithoutTable"], 'A2:G2');

//Задаем названия колонок
$objPHPExcel->setActiveSheetIndex()->setCellValue('B3', ic("Артикул"));
$objPHPExcel->setActiveSheetIndex()->setCellValue('C3', ic("Наименование"));
$objPHPExcel->setActiveSheetIndex()->setCellValue('E3', ic("Для заказа"));
$objPHPExcel->setActiveSheetIndex()->setCellValue('F3', ic("Цена"));
$objPHPExcel->setActiveSheetIndex()->setCellValue('G3', ic("Штрих-код"));
$objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["h1"], 'A3:G3');
$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(24);
$objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["h1"], 'A4:G4');


//Получаем полную иерархию разделов
$sections[0] = &$allSections['ROOT'];
$getListSections = CIBlockSection::GetList(array('DEPTH_LEVEL'=>'ASC','SORT'=>'ASC'), array("IBLOCK_ID" => 16, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE'=>'Y'), false, array('ID','NAME', 'SECTION_PAGE_URL', 'IBLOCK_SECTION_ID'));
while($getListSection = $getListSections->GetNext()) {
    $getListSection["SECTION_PAGE_URL"] = $siteUrl . $getListSection["SECTION_PAGE_URL"];
    $sections[intval($getListSection['IBLOCK_SECTION_ID'])]['CHILD'][$getListSection['ID']] = $getListSection;
    $sections[$getListSection['ID']] = &$sections[intval($getListSection['IBLOCK_SECTION_ID'])]['CHILD'][$getListSection['ID']];
}
unset($sections);
//$allSections - массив с разделами

//Получаем все  персональные цены
$page = $APPLICATION->GetCurPage();
$HighloadBlockTableID = 3;
$arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById($HighloadBlockTableID)->fetch();
$obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
$strEntityDataClass = $obEntity->getDataClass();
$rsData = $strEntityDataClass::getList(array(
    'select' => array('UF_PRODUCT_ID', "UF_PRICE"),
    'filter' => array(
        'UF_USER_ID' => $USER->GetID(),
    )
));
while ($el = $rsData->Fetch()) {
    $allPrices[$el["UF_PRODUCT_ID"]] = $el["UF_PRICE"];
}

//Получаем все элементы сайта:
$getListElements = CIBlockElement::GetList(array("NAME" => "ASC"), array("IBLOCK_ID" => 16, 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE'=>'Y', ">CATALOG_QUANTITY" => 0), false, false, array("IBLOCK_SECTION_ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_CML2_ARTICLE", "PROPERTY_CML2_BAR_CODE"));
while($getListElement = $getListElements->GetNext()) {
    if ($allPrices[$getListElement["ID"]]) {
        $getListElement["PRICE"] = $allPrices[$getListElement["ID"]];
    }
    $getListElement["DETAIL_PAGE_URL"] = $siteUrl . $getListElement["DETAIL_PAGE_URL"];
    if (!$allElements[$getListElement["IBLOCK_SECTION_ID"]]) {
        $allElements[$getListElement["IBLOCK_SECTION_ID"]] = array($getListElement);
    }
    else {
        $allElements[$getListElement["IBLOCK_SECTION_ID"]][] = $getListElement;
    }
}
//allElements - массив с элементами

//Заполняем xls файл ценами
fillPrices($allSections["ROOT"]);
$endIndex = $index - 1;
$objPHPExcel->getActiveSheet()->setCellValue('E2', '=SUM(E5:E'. $endIndex . ')');
$objPHPExcel->getActiveSheet()->setCellValue('F2', '=SUMPRODUCT((E5:E'. $endIndex . ')*(F5:F'. $endIndex . '))&" руб."');
//Заполняем брошюрами нашу таблицу
$bookletsFile = $_SERVER["DOCUMENT_ROOT"] . "/upload/1c_custom_exchange/Booklets.csv";
if (file_exists($bookletsFile)) {
    $bookletsData = $bookletSectionArticle = array();
    $bookletTitle = array();
    if (($handle = fopen($bookletsFile, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            $data[0] = iconv("windows-1251", "UTF-8", $data[0]);
            $data[1] = iconv("windows-1251", "UTF-8", $data[1]);
            $data[2] = iconv("windows-1251", "UTF-8", $data[2]);
            if ($bookletsData[$data[2]]) {
                $bookletsData[$data[2]][$data[1]] = $data[0];
            }
            else {
                $bookletsData[$data[2]] = array($data[1] => $data[0]);
            }
        }
        fclose($handle);
        ksort($bookletsData);
        foreach ($bookletsData as &$data) {
            ksort($data);
        }
        $bookletSectionArticle = $bookletsData["! FREE Брошюры"];
        $bookletTitle = $bookletsData[""];
        unset($bookletsData["! FREE Брошюры"]);
        unset($bookletsData[""]);
        if (count($bookletsData)) {
            insertBooklets($bookletsData,$bookletSectionArticle,$bookletTitle);
        }
    }
}

$objPHPExcel->setActiveSheetIndex()->setCellValue('D' . $index, ic("ИТОГО:"));
$objPHPExcel->getActiveSheet()->setCellValue('E' . $index, '=SUM(E5:E'. $endIndex . ')');
$objPHPExcel->getActiveSheet()->setCellValue('F' . $index, '=SUMPRODUCT((E5:E'. $endIndex . ')*(F5:F'. $endIndex . '))&" руб."');
$objPHPExcel->setActiveSheetIndex()->setSharedStyle($allStyles["normalWithoutTable"], 'A' . $index . ':G' . $index);


//Задаем название файла
$file_name_xls = '/createXlsDocuments/' . $USER->GetID(). 'PriceAvaialble.xlsx';

//Сохраняем файл
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save($_SERVER['DOCUMENT_ROOT'] . $file_name_xls);

$file = $_SERVER['DOCUMENT_ROOT'] . $file_name_xls;
if (file_exists($file)) {
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($file));
    readfile($file);
    @unlink($file);
}