<? define("NEED_AUTH", true);
  require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
  $APPLICATION->SetTitle("Персональный раздел");
?>
<div class = "pmenu">
  <?$APPLICATION->IncludeComponent(
    "bitrix:menu", 
    "personal", 
    array(
      "ROOT_MENU_TYPE" => "personal",
      "MENU_CACHE_TYPE" => "A",
      "MENU_CACHE_TIME" => "3600",
      "MENU_CACHE_USE_GROUPS" => "Y",
      "MENU_CACHE_GET_VARS" => array(
      ),
      "MAX_LEVEL" => "1",
      "CHILD_MENU_TYPE" => "",
      "USE_EXT" => "N",
      "DELAY" => "N",
      "ALLOW_MULTI_SELECT" => "N",
      "SEPARATORS_PLACE" => array(
        0 => "3",
        1 => "7",
        2 => "10",
        3 => "",
      ),
      "COMPONENT_TEMPLATE" => "personal"
    ),
    false
  );?>
</div>
<? $prices = getPricelist(); ?>
   <div class = "account-wrapper">
      <img src = "/custom/themes/default/img/new/download_started_2.png" style = "display:none"/>
   <div class = "btn_feedback"><a class="fancyajax fancybox.ajax recall" href="/include/popup/recall/?AJAX_CALL=Y" title="Заказать звонок">Заказать звонок</a></div>
     <? if ($prices): ?>

       <?
       $names = array(
         0 => array( 'Скачать прайс-лист ПО НАЛИЧИЮ', 'price_1' ),
         1 => array( 'Скачать прайс-лист ПОЛНЫЙ', 'price_2' )
       );
       foreach ($prices as $price) {
         $name = $price['name'];
         if (isset($names[$price['type']])):
           $name = $names[$price['type']][0];
         endif;
         echo '<div class="btn_price"><a href="/file/?' . $price['guid'] . '&download_price=1" class="button2  ' . $names[$price['type']][1] . '"><div><p>' . $name . '</p></div></a></div>';
         unset($name, $price);
       }
       ?>

     <? endif; ?>
      <br class = "clear"/>
   </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
