<? define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Загрузка полного прайс-листа");
global $USER;
?>
<div class="pmenu">
  <?$APPLICATION->IncludeComponent(
    "bitrix:menu", 
    "personal", 
    array(
      "ROOT_MENU_TYPE" => "personal",
      "MENU_CACHE_TYPE" => "A",
      "MENU_CACHE_TIME" => "3600",
      "MENU_CACHE_USE_GROUPS" => "Y",
      "MENU_CACHE_GET_VARS" => array(
      ),
      "MAX_LEVEL" => "1",
      "CHILD_MENU_TYPE" => "",
      "USE_EXT" => "N",
      "DELAY" => "N",
      "ALLOW_MULTI_SELECT" => "N",
      "SEPARATORS_PLACE" => array(
        0 => "3",
        1 => "7",
        2 => "10",
        3 => "",
      ),
      "COMPONENT_TEMPLATE" => "personal"
    ),
    false
  );?>
</div>
<div class="pcontent">
    <?
    //Получаем все  персональные цены
    $HighloadBlockTableID = 3;
    $arHLBlock = Bitrix\Highloadblock\HighloadBlockTable::getById($HighloadBlockTableID)->fetch();
    $obEntity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
    $strEntityDataClass = $obEntity->getDataClass();
    $rsData = $strEntityDataClass::getList(array(
        'select' => array('UF_PRODUCT_ID', "UF_PRICE"),
        'filter' => array(
            'UF_USER_ID' => $USER->GetID(),
        )
    ));
    if($rsData->fetch()) {
        ?>
        <p>Cейчас начнется скачивание прайса. Eсли в течении 5 секунд скачивание не началось, нажмите на кнопку ниже</p>
        <div class="radial-progress">
            <div class="circle">
                <div class="mask full">
                    <div class="fill"></div>
                </div>
                <div class="mask half">
                    <div class="fill"></div>
                    <div class="fill fix"></div>
                </div>
            </div>
            <span class="inset seconds">5</span>
        </div>
        <script>
            var countdown = $('.seconds'), startFrom = 5, timer;
            function startCountdown(){
                countdown.parent('span').show();
                timer = setInterval(function() {
                    if (startFrom == 5) {
                        $(".radial-progress .mask.half .fill").css({
                            transform: 'rotate(180deg)'
                        });
                        $(".radial-progress .mask.full .fill.fix").css({
                            transform: 'rotate(360deg)'
                        });

                        $(".radial-progress .full").css({
                            transform: 'rotate(180deg)'
                        });
                    }
                    countdown.text(startFrom--);
                    if (startFrom <= (5/2)) {
                        $(".radial-progress .full .fill").css({
                            transform: 'rotate(180deg)'
                        });
                    }
                    if(startFrom < 0) {
                        var btnModal = $(".pcontent");
                        var btnDownPrice = $("<a/>")
                            .addClass("btn-download-price")
                            .attr("href", "downloadPrices.php")
                            .html("Ссылка на скачивание прайса")
                            .appendTo(btnModal);
                        clearInterval(timer);
                    }
                }, 1000);
            }
            $(window).ready(function () {
                startCountdown();
                window.location.replace('downloadPrices.php');
            });
        </script>
        <?
    }
    else {
        logToFileEvent('/_logs/download-prices.log', $arProps);
        ?>
        <p>Для текущего пользователя прайс-лист отсутствует, либо отсутствуют дополнительные данные для запроса на скачивание файла.</p>
        <?
    }
    ?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>