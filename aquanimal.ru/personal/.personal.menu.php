<?
$aMenuLinks = Array(
	Array(
		"Общая информация", 
		"/personal/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Личный счет", 
		"/personal/account/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Отсутствующие товары", 
		"/personal/subscribe/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Просмотренные товары", 
		"/personal/viewed/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Избранные товары", 
		"/personal/favorite/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Подписка на рассылки", 
		"/personal/subscriptions/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Корзина", 
		"/personal/cart/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Профили доставки", 
		"/personal/profiles/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"История заказов", 
		"/personal/order/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Скачать полный прайс-лист", 
		"/personal/download_full_price/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Скачать прайс-лист по наличию", 
		"/personal/download_InStock_price/", 
		Array(), 
		Array(), 
		"" 
	)
);
?>