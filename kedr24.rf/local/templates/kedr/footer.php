<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="footer_ftr bg-green">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="ftr_block">
                    <img class="ftr_logo" src="<?= DEFAULT_TEMPLATE_PATH ?>img/logo-stm-invert.svg">
                    <div class="ftr_text ftr_contacts">
                        <?php if ($isMobile) : ?>
                            <a href="tel:<?= \COption::GetOptionString("askaron.settings", "UF_PHONE"); ?>">
                                <?= \COption::GetOptionString("askaron.settings", "UF_PHONE_FORMATED"); ?>
                            </a>
                        <?php else : ?>
                            <?= \COption::GetOptionString("askaron.settings", "UF_PHONE_FORMATED"); ?>
                        <?php endif; ?>
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/footer/contacts.php"]
                        );
                        ?>
                    </div>
                    <div class="ftr_text ftr_copy">
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/footer/description.php"]
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $.mask.definitions['c'] = "[9]";
        $(".phoneNumber").mask("+7(c99) 999-9999");
    });
</script>
</body>
</html>
