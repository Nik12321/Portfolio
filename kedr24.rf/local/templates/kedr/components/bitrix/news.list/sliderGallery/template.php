<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
global $sliNum;
$sliNum++;
?>
<?php if (count($arResult["ITEMS"]) > 1) : ?>
    <div class="tabs-slider_tas">
        <div class="js-tabs-slider">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach ($arResult['ITEMS'] as $key => $arItem) : ?>
                        <div data-tab-btn="<?= $arItem["CODE"] ?>"
                             class="tas_tab <?= ($key === 0) ? "tas_tab--active" : "" ?> swiper-slide"><?= $arItem["NAME"] ?></div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="tas_arrow tas_arrow-left swiper-button-prev"></div>
            <div class="tas_arrow tas_arrow-right swiper-button-next"></div>
        </div>
    </div>
<?php endif; ?>
<?php foreach ($arResult["ITEMS"] as $arItem) : ?>
    <?php
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="sli_body js-slides-<?= $sliNum ?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"
        <?php if (count($arResult["ITEMS"]) > 1) : ?>
            data-tab="<?= $arItem["CODE"] ?>"
        <?php endif; ?>
    >
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($arItem['VALUES'] as $value): ?>
                    <div class="swiper-slide">
                        <a href="<?= $value['LINK'] ?>" class="sli_link show-media" data-fancybox="<?= $sliNum ?>">
                            <img class="sli_img" src="<?= $value['SRC'] ?>">
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
    <?php $sliNum++; ?>
<?php endforeach; ?>