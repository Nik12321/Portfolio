<?php
$videosIds = [];
$videosIblockId = 0;
foreach ($arResult["ITEMS"] as &$arItem) {
    $values = $arItem["PROPERTIES"]["FILES"]["VALUE"];
    $resultArray = [];
    foreach ($values as $value) {
        $file = CFile::GetFileArray($value);
        $resizedFile = CFile::ResizeImageGet($file, ["width" => 280, "height" => 250], BX_RESIZE_IMAGE_EXACT, false, 100);
        ($resizedFile["src"])
            ? $resultArray[] = ["SRC" => $resizedFile["src"], "LINK" => $file["SRC"]]
            : $resultArray[] = ["SRC" => $file["SRC"], "LINK" => $file["SRC"]];
    }

    if (count($arItem["PROPERTIES"]["VIDEOS"]["VALUE"]) && $arItem["PROPERTIES"]["VIDEOS"]["VALUE"] !== false) {
        if (!$videosIblockId) {
            $videosIblockId = $arItem["PROPERTIES"]["VIDEOS"]["LINK_IBLOCK_ID"];
        }
        $videosIds = array_merge($videosIds, $arItem["PROPERTIES"]["VIDEOS"]["VALUE"]);
    }
    $arItem["VALUES"] = $resultArray;
}
unset($arItem);
if (count($videosIds)) {
    $videoEndData = [];
    $videos = CIBlockElement::GetList(false, ["IBLOCK_ID" => $videosIblockId, "ID" => $videosIds], false, false, ["ID", "PREVIEW_PICTURE", "PROPERTY_FILE"]);
    while ($videoData = $videos->Fetch()) {
        $preview = CFile::GetFileArray($videoData["PREVIEW_PICTURE"]);
        $video = CFile::GetFileArray($videoData["PROPERTY_FILE_VALUE"]);

        $resizedFile = CFile::ResizeImageGet($preview, ["width" => 280, "height" => 250], BX_RESIZE_IMAGE_EXACT, false, 100);
        ($resizedFile["src"])
            ? $videoEndData[$videoData["ID"]] = ["SRC" => $resizedFile["src"], "LINK" => $video["SRC"]]
            : $videoEndData[$videoData["ID"]] = ["SRC" => $file["SRC"], "LINK" => $video["SRC"]];
    }
}
foreach ($arResult["ITEMS"] as &$arItem) {
    if (count($arItem["PROPERTIES"]["VIDEOS"]["VALUE"]) && $arItem["PROPERTIES"]["VIDEOS"]["VALUE"] !== false) {
        $addVideos = [];
        foreach ($arItem["PROPERTIES"]["VIDEOS"]["VALUE"] as $value) {
            $addVideos[] = $videoEndData[$value];
        }
        $arItem["VALUES"] = array_merge( $arItem["VALUES"], $addVideos);
    }
}
unset($arItem);