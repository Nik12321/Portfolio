<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<?php foreach ($arResult["ITEMS"] as $arItem) : ?>
    <?php
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="nov_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="nov_title"><?=$arItem["NAME"]?></div>
        <div class="nov_date"><?=$arItem["ACTIVE_FROM"]?></div>
        <div class="nov_text">
           <?=$arItem["PREVIEW_TEXT"]?>
        </div>
    </div>
<?php endforeach; ?>