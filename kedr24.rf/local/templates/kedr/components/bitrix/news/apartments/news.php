<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
$uri = str_replace([$_SERVER["QUERY_STRING"], "?"], "", $_SERVER["REQUEST_URI"]);
?>
<?php if ($uri === "/") : ?>
    <div class="about_abt spot-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="title-lg">
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/title1.php"]
                        );
                        ?>
                    </h1>
                    <div class="text-main">
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/description1.php"]
                        );
                        ?>
                    </div>
                    <div class="abt_logos">
                        <img class="abt_logo abt_logo-stm" src="<?= DEFAULT_TEMPLATE_PATH ?>img/logo-stm.svg">
                        <div class="abt_divider"></div>
                        <img class="abt_logo abt_logo-sber" src="<?= DEFAULT_TEMPLATE_PATH ?>img/logo-sberbank.svg">
                    </div>
                    <div class="call-order_cao">
                        <?php if ($isMobile) : ?>
                            <a href="tel:<?= \COption::GetOptionString("askaron.settings", "UF_PHONE"); ?>"
                               class="cao_btn cao_call">Позвонить</a>
                        <?php else : ?>
                            <div class="cao_btn cao_call js-order-call">Позвонить</div>
                        <?php endif; ?>
                        <div class="cao_btn cao_order js-order-call">Заказать звонок</div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="promo-images_pim bg-brown-green">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="pim_block">
                        <div class="pim_cell">
                            <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/promo-01.jpg" class="pim_img">
                        </div>
                        <div class="pim_cell">
                            <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/promo-02.jpg" class="pim_img">
                        </div>
                        <div class="pim_cell">
                            <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/promo-03.jpg" class="pim_img">
                        </div>
                        <div class="pim_cell">
                            <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/promo-04.jpg" class="pim_img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grow_gro bg-brown-lite">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="gro_block">
                        <div class="title-lg">
                            <?php
                            $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/title2.php"]
                            );
                            ?>
                        </div>
                        <div class="text-main">
                            <?php
                            $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/description2.php"]
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="advantages_adv">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="adv_cells">
                        <div class="adv_cell">
                            <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/spot-discout.svg" class="adv_img">
                            <div class="adv_title">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/title3.php"]
                                );
                                ?>
                            </div>
                            <div class="adv_text">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/description3.php"]
                                );
                                ?>
                            </div>
                        </div>
                        <div class="adv_cell">
                            <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/spot-plan.svg" class="adv_img">
                            <div class="adv_title">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/title4.php"]
                                );
                                ?>
                            </div>
                            <div class="adv_text">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/description4.php"]
                                );
                                ?>
                            </div>
                        </div>
                        <div class="adv_cell">
                            <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/spot-sun.svg" class="adv_img">
                            <div class="adv_title">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/title5.php"]
                                );
                                ?>
                            </div>
                            <div class="adv_text">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/description5.php"]
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class=" btn-links_bts mt30">
                        <?php if ($isMobile) : ?>
                            <a href="tel:<?= \COption::GetOptionString("askaron.settings", "UF_PHONE"); ?>"
                               class="bts_link bts_active bts_01">Позвонить</a>
                        <?php else : ?>
                            <div class="bts_link bts_active bts_01 js-order-call">Позвонить</div>
                        <?php endif; ?>
                        <div class="bts_link bts_active bts_02 js-order-call">Заказать звонок</div>
                        <div class="bts_link bts_active bts_03 js-mortgage">Ипотека от 5,7%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="map_map">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="title-lg">
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/title6.php"]
                        );
                        ?>
                    </div>

                    <div class="text-main">
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/apartments/description6.php"]
                        );
                        ?>
                    </div>
                </div>
                <div class="map_col col-sm-12">

                    <a href="<?= DEFAULT_TEMPLATE_PATH ?>img/map-lg-squirrel-2.jpg" class="show-media">
                        <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/map-squirrel-2.jpg" class="map_img">
                    </a>

                </div>
            </div>
        </div>
    </div>

    <div class="map-btm_mab bg-green">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/bg-lines-green.png" class="mab_bg">
                    <div class=" btn-links_bts mab_btns">
                        <?php if ($isMobile) : ?>
                            <a href="tel:<?= \COption::GetOptionString("askaron.settings", "UF_PHONE"); ?>"
                               class="bts_link bts_active bts_01">Позвонить</a>
                        <?php else : ?>
                            <div class="bts_link bts_active bts_01 js-order-call">Позвонить</div>
                        <?php endif; ?>
                        <div class="bts_link bts_active bts_02 js-order-call">Заказать звонок</div>
                        <div class="bts_link bts_active bts_03 js-mortgage">Ипотека от 5,7%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $apartmentTypesInfo = [
        "1-komn-kvartiry" => ["SUB_TITLE" => " квартиры", "BACKGROUND" => "brown"],
        "2-komn-kvartiry" => ["SUB_TITLE" => " квартиры", "BACKGROUND" => "green"],
        "3-komn-kvartiry" => ["SUB_TITLE" => " квартиры", "BACKGROUND" => "brown"],
        "4-komn-kvartiry" => ["SUB_TITLE" => " квартиры", "BACKGROUND" => "green"],
        "nezhilye" => ["SUB_TITLE" => " помещения", "BACKGROUND" => "brown", "REMOVE_BTN_PERCENT" => true]
    ];
    $apartmentTypes = [
        "1-komn-kvartiry",
        "2-komn-kvartiry",
        "3-komn-kvartiry",
        "4-komn-kvartiry",
        "nezhilye"
    ];

    $results = CIBlockSection::GetList(
        false,
        ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "CODE" => $apartmentTypes],
        true,
        ["ID", "CODE", "NAME", "UF_SLOGAN", "IBLOCK_SECTION_ID"]
    );
    while ($result = $results->Fetch()) {
        if ($result["ELEMENT_CNT"]) {
            $apartmentTypesInfo[$result["CODE"]]["SECTION_ID"] = $apartmentTypesInfo["IBLOCK_SECTION_ID"];
            if (!$apartmentTypesInfo[$result["CODE"]]["NAME"]) {
                $apartmentTypesInfo[$result["CODE"]]["NAME"] = $result["NAME"];
            }
            if (!$apartmentTypesInfo[$result["CODE"]]["SLOGAN"]) {
                $apartmentTypesInfo[$result["CODE"]]["SLOGAN"] = $result["UF_SLOGAN"];
            }
            if (!$apartmentTypesInfo[$result["CODE"]]["VALUES"]) {
                $apartmentTypesInfo[$result["CODE"]]["VALUES"] = [$result["ID"]];
            } else {
                $apartmentTypesInfo[$result["CODE"]]["VALUES"][] = $result["ID"];
            }
        }
    }
    foreach ($apartmentTypesInfo as $type) {
        if (count($type["VALUES"])) {
            if (count($type["VALUES"]) === 1) {
                $titleOnSlider = CIBlockSection::GetList(false, ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $type["SECTION_ID"]], false, ["NAME"])->Fetch()["NAME"];
            }
            $GLOBALS["newsFilter"] = ["SECTION_ID" => $type["VALUES"]];
            $APPLICATION->IncludeComponent("bitrix:news.list", "slider", [
                "REMOVE_BTN_PERCENT" => $type["REMOVE_BTN_PERCENT"],
                "SECTION_NAME" => $type["NAME"],
                "SUB_TITLE" => $type["SUB_TITLE"],
                "SLOGAN" => $type["SLOGAN"],
                "SLIDER_TITLE" => $titleOnSlider,
                "BACKGROUND_COLOR" => $type["BACKGROUND"],
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                "SORT_BY1" => $arParams["SORT_BY1"],
                "SORT_ORDER1" => $arParams["SORT_ORDER1"],
                "SORT_BY2" => $arParams["SORT_BY2"],
                "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
                "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                "SET_TITLE" => $arParams["SET_TITLE"],
                "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                "MESSAGE_404" => $arParams["MESSAGE_404"],
                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                "SHOW_404" => $arParams["SHOW_404"],
                "FILE_404" => $arParams["FILE_404"],
                "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                "FILTER_NAME" => "newsFilter",
                "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                "CHECK_DATES" => $arParams["CHECK_DATES"],
            ],
                $component
            );
        }
    }
    ?>
    <div class="digits-promo_dip bg-green-green">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/digits-promo.png" class="dip_img">
                </div>
            </div>
        </div>
    </div>

<?php else : ?>
    <?php
    $APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/404.php"]
    );
    ?>
<?php endif; ?>