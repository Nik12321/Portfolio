<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="item_itm spot-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <h1 class="title-lg">
                    <?= $arResult["NAME"] ?> м<sup>2</sup>
                    <?php if ($arResult["SECTION_NAME"]) : ?>
                        <a href="<?= $arResult["SECTION_NAME"]["SECTION_PAGE_URL"] ?>"
                           class="title-lg_sup"><?= $arResult["SECTION_NAME"]["NAME"] ?></a>
                    <?php endif; ?>
                </h1>

                <div class="itm_cols">
                    <div class="itm_col itm_col--left">
                        <?php if ($arResult["PREVIEW_PICTURE"]["SRC"]) : ?>
                            <img src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>" class="itm_plan">
                        <?php endif; ?>
                        <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/compass.png" class="itm_compass">
                    </div>
                    <div class="itm_col itm_col--right">
                        <table class="itm_table">
                            <tbody>
                            <?php if (count($arResult["PROPERTIES"]["ROOM_SQUARE"]["VALUE"])) : ?>
                                <tr>
                                    <td>
                                        <?php if ($arResult["PROPERTIES"]["ROOM_SQUARE"]["DESCRIPTION"][0]) : ?>
                                            <?= $arResult["PROPERTIES"]["ROOM_SQUARE"]["DESCRIPTION"][0] ?>
                                        <?php else : ?>
                                            <?php if (count($arResult["PROPERTIES"]["ROOM_SQUARE"]["VALUE"]) === 1) : ?>
                                                комната
                                            <?php else : ?>
                                                комнаты
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php foreach ($arResult["PROPERTIES"]["ROOM_SQUARE"]["VALUE"] as $key => $value) : ?>
                                            <?= $value ?> м
                                            <sup>2</sup><?= ($key !== (count($arResult["PROPERTIES"]["ROOM_SQUARE"]["VALUE"]) - 1) ? ", " : "") ?>
                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($arResult["PROPERTIES"]["KITCHEN_SQUARE"]["VALUE"]) : ?>
                                <tr>
                                    <td>кухня</td>
                                    <td><?= $arResult["PROPERTIES"]["KITCHEN_SQUARE"]["VALUE"] ?> м<sup>2</sup></td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($arResult["PROPERTIES"]["HALL_SQUARE"]["VALUE"]) : ?>
                                <tr>
                                    <td>коридор</td>
                                    <td><?= $arResult["PROPERTIES"]["HALL_SQUARE"]["VALUE"] ?> м<sup>2</sup></td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($arResult["PROPERTIES"]["SU_SQUARE"]["VALUE"]) : ?>
                                <tr>
                                    <td>санузел</td>
                                    <td><?= $arResult["PROPERTIES"]["SU_SQUARE"]["VALUE"] ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($arResult["PROPERTIES"]["BALCON_SQUARE"]["VALUE"] || $arResult["PROPERTIES"]["BALCON_SQUARE"]["DESCRIPTION"]) : ?>
                                <tr>
                                    <td>
                                        <?= ($arResult["PROPERTIES"]["BALCON_SQUARE"]["DESCRIPTION"]) ? $arResult["PROPERTIES"]["BALCON_SQUARE"]["DESCRIPTION"] : "балкон" ?>
                                    </td>
                                    <td><?= $arResult["PROPERTIES"]["BALCON_SQUARE"]["VALUE"] ?> м<sup>2</sup></td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($arResult["PROPERTIES"]["PRICE_FOR_M"]["VALUE"]) : ?>
                                <tr class="itm_vivid-text">
                                    <td>цена за м<sup>2</sup></td>
                                    <td><?= $arResult["PROPERTIES"]["PRICE_FOR_M"]["VALUE"] ?> руб.</td>
                                </tr>
                            <?php endif; ?>
                            <?php if ($arResult["PROPERTIES"]["TOTAL_PRICE"]["VALUE"]) : ?>
                                <tr class="itm_vivid-text">
                                    <td>стоимость квартиры</td>
                                    <td><?= $arResult["PROPERTIES"]["TOTAL_PRICE"]["VALUE"] ?> руб.</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <?php if ($arResult["DETAIL_PICTURE"]["SRC"]) : ?>
                            <img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" class="itm_floor">
                        <?php endif; ?>
                    </div>
                </div>
                <?php if ($arResult["PROPERTIES"]["TOUR_SWF"]["VALUE"] && $arResult["PROPERTIES"]["TOUR_XML"]["VALUE"]) : ?>
                    <script src="<?= DEFAULT_TEMPLATE_PATH ?>tour/tour.js"></script>
                    <div id="pano" style="width:100%;">
                        <noscript>
                            <table style="width:100%;height:100%;">
                                <tr style="vertical-align:middle;">
                                    <td>
                                        <div style="text-align:center;">ОШИБКА:<br/><br/>Javascript не
                                            включен.<br/><br/></div>
                                    </td>
                                </tr>
                            </table>
                        </noscript>
                        <script>
                            embedpano({
                                swf: "<?=$arResult["PROPERTIES"]["TOUR_SWF"]["VALUE"]?>",
                                xml: "<?=$arResult["PROPERTIES"]["TOUR_XML"]["VALUE"]?>",
                                target: "pano",
                                html5: "auto",
                                mobilescale: 1.0,
                                passQueryParameters: true
                            });
                        </script>
                    </div>
                <?php endif; ?>
                <div class="call-order_cao">
                    <a href="tel:<?= \COption::GetOptionString("askaron.settings", "UF_PHONE"); ?>"
                       class="cao_btn cao_call">Позвонить</a>
                    <div class="cao_btn cao_order js-order-call">Заказать звонок</div>
                </div>
            </div>
        </div>
    </div>
</div>