<?php
if ($arResult["IBLOCK_SECTION_ID"]) {
    $result = CIBlockSection::GetList(false, ["IBLOCK_ID" => $arResult["IBLOCK_ID"], "ID" => $arResult["IBLOCK_SECTION_ID"]], false, ["IBLOCK_SECTION_ID"])->Fetch();
    if ($result["IBLOCK_SECTION_ID"]) {
        $result = CIBlockSection::GetList(
            false,
            ["IBLOCK_ID" => $arResult["IBLOCK_ID"], "ID" => $result["IBLOCK_SECTION_ID"]],
            false,
            ["NAME", "SECTION_PAGE_URL"]
        )->GetNext();
        if ($result["NAME"]) {
            $arResult["SECTION_NAME"] = $result;
        }
    }
}
$ipropValues = (new \Bitrix\Iblock\InheritedProperty\ElementValues($arParams["IBLOCK_ID"], $arResult["ID"]))->getValues();
if ($ipropValues["ELEMENT_META_TITLE"]) {
    $title = $ipropValues["ELEMENT_META_TITLE"] . " кв. м";
    if( $arResult["SECTION_NAME"]["NAME"]) {
        $title .= " - " . $arResult["SECTION_NAME"]["NAME"];
    }
    $title .= " - Жилой коммплекс «Кедр»";
    $APPLICATION->SetPageProperty("title", $title);
    $APPLICATION->SetTitle($title);
}