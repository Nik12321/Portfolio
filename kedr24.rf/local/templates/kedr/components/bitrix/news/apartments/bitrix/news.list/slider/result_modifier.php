<?php

foreach ($arResult["ITEMS"] as &$arItem) {
    if ($arItem["PROPERTIES"]["SLIDER_IMAGE"]["VALUE"]) {
        $file = CFile::GetFileArray($arItem["PROPERTIES"]["SLIDER_IMAGE"]["VALUE"]);
        $resizedFile = CFile::ResizeImageGet($file, ["width" => 9999, "height" => 250], BX_RESIZE_IMAGE_PROPORTIONAL, false, 100);
        if ($resizedFile["src"]) {
            $arItem["PREVIEW_PICTURE"]["SRC"] = $resizedFile["src"];
        } else {
            $arItem["PREVIEW_PICTURE"]["SRC"] = $file["SRC"];
        }
    } else {
        unset($arItem["PREVIEW_PICTURE"]);
    }
}
unset($arItem);