<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<div class="item_itm spot-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <h1 class="title-lg">
                    <?= $arResult["NAME"] ?> м<sup>2</sup>
                    <?php if ($arResult["SECTION_NAME"]) : ?>
                        <a href="<?= $arResult["SECTION_NAME"]["SECTION_PAGE_URL"] ?>" class="title-lg_sup"><?= $arResult["SECTION_NAME"]["NAME"] ?></a>
                    <?php endif; ?>
                </h1>

                <div class="itm_cols">
                    <div class="itm_col itm_col--left">
                        <?php if ($arResult["PREVIEW_PICTURE"]["SRC"]) : ?>
                            <img src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>" class="itm_plan">
                        <?php endif; ?>
                    </div>
                    <div class="itm_col itm_col--right">
                        <?php if ($arResult["DETAIL_PICTURE"]["SRC"]) : ?>
                            <img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" class="itm_floor">
                        <?php endif; ?>
                        <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/compass.png" class="itm_compass">
                    </div>
                </div>
                <div class="call-order_cao">
                    <div class="cao_btn cao_call js-order-call">Позвонить</div>
                    <div class="cao_btn cao_order js-order-call">Заказать звонок</div>
                </div>
            </div>
        </div>
    </div>
</div>