<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
?>
<?php if (count($arResult['ITEMS'])) : ?>
    <?php
    global $sliNum;
    $sliNum++;
    ?>
    <div class="slides_sli sli--with-btn bg-<?= $arParams["BACKGROUND_COLOR"] ?>-lite <?= ($arParams["BACKGROUND_COLOR"] == "green") ? "sli--green " : "" ?>"
         data-anchor="#<?= $sliNum ?>k">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="sli_block">
                        <div class="sli_title title-lg">
                            <?= $arParams["SECTION_NAME"] ?>
                            <span class="xlite"><?= $arParams["SUB_TITLE"] ?></span>
                            <?php if ($arParams["SLIDER_TITLE"]) : ?>
                                <div class="title-lg_sup"><?= $arParams["SLIDER_TITLE"] ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="sli_notice">
                            <?= $arParams["SLOGAN"] ?>
                        </div>
                        <div class="sli_body js-slides-<?= $sliNum ?>">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <?php foreach ($arResult['ITEMS'] as $arItem) : ?>
                                        <?php
                                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                        $sliBgClass = 'sli_slide-bg sli_slide-bg--' . mt_rand(1, 10);
                                        ?>
                                        <div class="swiper-slide <?= $sliBgClass ?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                            <div class="sli_text"><?= $arItem["PROPERTIES"]["SQUARE"]["VALUE"] ?>
                                                м<sup>2</sup></div>
                                            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="sli_link">
                                                <img class="sli_img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                                            </a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>


                        <div class=" btn-links_bts sli_btns">
                            <?php if ($isMobile) : ?>
                                <a href="tel:<?= \COption::GetOptionString("askaron.settings", "UF_PHONE"); ?>" class="bts_link bts_active bts_01">Позвонить</a>
                            <?php else : ?>
                                <div class="bts_link bts_active bts_01 js-order-call">Позвонить</div>
                            <?php endif; ?>
                            <div class="bts_link bts_active bts_02 js-order-call">Заказать звонок</div>
                            <?php if (!$arParams["REMOVE_BTN_PERCENT"]) : ?>
                                <div class="bts_link bts_active bts_03 js-mortgage">Ипотека от 5,7%</div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>