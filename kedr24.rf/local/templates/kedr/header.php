<?php

use Bitrix\Main\Page\Asset;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title><?= $APPLICATION->ShowTitle() ?></title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="<?= DEFAULT_TEMPLATE_PATH ?>favicon.ico"/>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= DEFAULT_TEMPLATE_PATH ?>icons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= DEFAULT_TEMPLATE_PATH ?>icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-P84PVQF');
    </script>
    <!-- End Google Tag Manager -->
    <style>
        .modal-logotips {
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            margin-bottom: 15px;
        }

        .modal-logo-img {
            margin: 0 auto;
            width: 30%;
        }
    </style>

    <?php
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH . 'css/main.css');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "js/main.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "js/jquery.maskedinput.js");
    $APPLICATION->ShowHead();
    ?>
</head>
<body>
<div id="panel">
    <?php $APPLICATION->ShowPanel(); ?>
</div>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P84PVQF" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->


<div class="top-nav_tav">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="tav_ctr">
                    <a class="tav_logo" href="/">
                        <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/logo.svg" class="tav_logo-img">
                        <span class="tav_logo-txt">
                             <?php
                             $APPLICATION->IncludeComponent(
                                 "bitrix:main.include",
                                 "",
                                 ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/header/logoText.php"]
                             );
                             ?>
                        </span>
                    </a>
                    <div class="tav_links">
                        <a href="/#1k" class="tav_link bold js-inner-link"
                           onclick="ym(69609880,'reachGoal','top-navs-clicked'); return true;">1 к</a>
                        <a href="/#2k" class="tav_link bold js-inner-link"
                           onclick="ym(69609880,'reachGoal','top-navs-clicked'); return true;">2 к</a>
                        <a href="/#3k" class="tav_link bold js-inner-link"
                           onclick="ym(69609880,'reachGoal','top-navs-clicked'); return true;">3 к</a>
                        <a href="/#4k" class="tav_link bold js-inner-link"
                           onclick="ym(69609880,'reachGoal','top-navs-clicked'); return true;">4 к</a>
                        <span class="popup_pop">
                            <span class="tav_link pop_btn">О проекте</span>
                            <div class="pop_win">
                                <a href="/o-proekte/#o-zastroishike" class="pop_link"
                                   onclick="ym(69609880,'reachGoal','top-navs-clicked'); return true;">О застройщике</a>
                                <a href="/o-proekte/#hod-stroitelstva" class="pop_link"
                                   onclick="ym(69609880,'reachGoal','top-navs-clicked'); return true;">Ход строительства</a>
                                <a href="/o-proekte/#dokumenty" class="pop_link"
                                   onclick="ym(69609880,'reachGoal','top-navs-clicked'); return true;">Документы</a>
                                <a href="/o-proekte/#novosti" class="pop_link"
                                   onclick="ym(69609880,'reachGoal','top-navs-clicked'); return true;">Новости</a>
                            </div>
                        </span>
                    </div>
                    <div class="tav_right">
                        <?php if ($isMobile) : ?>
                            <a href="tel:<?= \COption::GetOptionString("askaron.settings", "UF_PHONE"); ?>"
                               class="tav_phone"
                               onclick="ym(69609880,'reachGoal','phone-clicked'); return true;">
                                <?= \COption::GetOptionString("askaron.settings", "UF_PHONE_FORMATED"); ?>
                            </a>
                        <?php else : ?>
                            <div class="tav_phone js-order-call"
                                 onclick="ym(69609880,'reachGoal','phone-clicked'); return true;">
                                <svg width="31" height="36" viewBox="0 0 31 36" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.0231 14.1544L18.3793 21.7575C19.2246 22.9574 20.8827 23.2449 22.0826 22.3996C23.2076 21.6071 24.762 21.8766 25.5545 23.0015L26.5909 24.4726L27.4746 25.727C28.3514 26.9716 28.0532 28.6914 26.8086 29.5683C23.2155 32.0995 18.2508 31.2387 15.7196 27.6457L6.58319 14.6767C4.05196 11.0836 4.91273 6.11893 8.50577 3.58771C9.75042 2.71088 11.4702 3.00906 12.347 4.2537L13.2307 5.50808L14.2671 6.97918C15.0596 8.10414 14.7901 9.65855 13.6651 10.4511C12.4652 11.2964 12.1777 12.9544 13.0231 14.1544Z"
                                          stroke="white" stroke-width="2"></path>
                                </svg>
                                <span>
                                    <?= \COption::GetOptionString("askaron.settings", "UF_PHONE_FORMATED"); ?>
                                </span>
                            </div>
                            <a href="mailto:<?= \COption::GetOptionString("askaron.settings", "UF_MAIL"); ?>" class="tav_phone js-order-call">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                                     style="enable-background:new 0 0 512 512;" xml:space="preserve">
											<path d="M467,61H45C20.218,61,0,81.196,0,106v300c0,24.72,20.128,45,45,45h422c24.72,0,45-20.128,45-45V106
												C512,81.28,491.872,61,467,61z M460.786,91L256.954,294.833L51.359,91H460.786z M30,399.788V112.069l144.479,143.24L30,399.788z
												 M51.213,421l144.57-144.57l50.657,50.222c5.864,5.814,15.327,5.795,21.167-0.046L317,277.213L460.787,421H51.213z M482,399.787
												L338.213,256L482,112.212V399.787z" fill="white"></path>
									</svg>
                                <span>
                                    <?= \COption::GetOptionString("askaron.settings", "UF_MAIL"); ?>
                                </span> 
                            </a>
                        <?php endif; ?>
                        <a href="<?= \COption::GetOptionString("askaron.settings", "UF_VK_LINK"); ?>"
                           class="tav_soc tav_soc--vk" target="_blank"></a>
                        <a href="<?= \COption::GetOptionString("askaron.settings", "UF_INSTAGRAMM"); ?>"
                           class="tav_soc tav_soc--in" target="_blank"></a>
                        <span class="popup_pop pop--right tav_mobile-menu">
                            <span class="bars_brs pop_btn">
                                <span class="brs_mid"></span>
                            </span>
                            <div class="pop_win">
                                <a href="/#1k" class="pop_link bold">1 комнатные</a>
                                <a href="/#2k" class="pop_link bold">2 комнатные</a>
                                <a href="/#3k" class="pop_link bold">3 комнатные</a>
                                <a href="/#4k" class="pop_link bold">4 комнатные</a>
                                <a href="/o-proekte/#o-zastroishike" class="pop_link">О застройщике</a>
                                <a href="/o-proekte/#hod-stroitelstva" class="pop_link">Ход строительства</a>
                                <a href="/o-proekte/#dokumenty" class="pop_link">Документы</a>
                                <a href="/o-proekte/#novosti" class="pop_link">Новости</a>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="js-order-call" class="popup-win_pow">
    <div class="pow_input" id="js-order-call-input">
        <div class="pow_title">Заказать звонок</div>
        <label class="pow_label">Оставьте Ваш номер телефона, и наш сотрудник свяжется с Вами, чтобы предложить лучшие
            условия:</label>
        <input type="text" id="js-order-call-name" class="pow_input-text"
               placeholder="Ваше имя...">
        <input type="text" id="js-order-call-phone" class="pow_input-text phoneNumber"
               placeholder="Ваш номер телефона...">
        <div class="pow_notice">Вы даете согласие на обработку персональных данных в соответствии
            с <a href="http://www.kremlin.ru/acts/bank/24154" target="_blank" rel="nofollow" class="pow_link">ФЗ-152</a>.
        </div>
        <div class="pow_btns text-center">
            <button class="pow_btn" id="js-order-call-submit">Жду звонка</button>
        </div>
    </div>
    <div class="pow_success" id="js-order-call-success">
        <div class="pow_title">Звонок заказан</div>
        <p>Благодарим за проявленный интерес! В ближайшее время с Вами свяжется наш сотрудник и ответит на все Ваши
            вопросы.</p>
        <div class="pow_btns text-center">
            <button class="pow_btn js-popup-close">Закрыть</button>
        </div>
    </div>
</div>

<div id="js-mortgage" class="popup-win_pow">
    <div class="pow_input" id="js-mortgage-input">
        <div class="pow_title">Ипотека от 5,7%</div>
        <div class="modal-logotips">
            <img class="modal-logo-img" src="<?= DEFAULT_TEMPLATE_PATH ?>img/logo-sberbank.svg">
            <img class="modal-logo-img" src="<?= DEFAULT_TEMPLATE_PATH ?>img/ef81459ff65c8416584fef66236ca562.png">
            <img class="modal-logo-img" src="<?= DEFAULT_TEMPLATE_PATH ?>img/logo_banks/VTB_logo_ru.png">
        </div>

        <label class="pow_label">Оставьте Ваш номер телефона, и наш сотрудник свяжется с Вами, чтобы рассказать, как
            получить самые лучшие условия по ипотеке:</label>
        <input type="text" id="js-mortgage-name" class="pow_input-text"
               placeholder="Ваше имя...">
        <input type="text" id="js-mortgage-phone" class="pow_input-text"
               placeholder="Ваш номер телефона...">
        <div class="pow_notice">Вы даете согласие на обработку персональных данных в соответствии
            с <a href="http://www.kremlin.ru/acts/bank/24154" target="_blank" rel="nofollow" class="pow_link">ФЗ-152</a>.
        </div>
        <div class="pow_btns text-center">
            <button class="pow_btn" id="js-mortgage-submit">Узнать больше</button>
        </div>
    </div>
    <div class="pow_success" id="js-mortgage-success">
        <div class="pow_title">Запрос отправлен</div>
        <p>Благодарим за проявленный интерес! В ближайшее время с Вами свяжется наш сотрудник, чтобы проконсультировать,
            как получить лучшие условия по ипотеке.</p>
        <div class="pow_btns text-center">
            <button class="pow_btn js-popup-close">Закрыть</button>
        </div>
    </div>
</div>