!function (a, b) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function (a) {
        if (!a.document) throw new Error("jQuery requires a window with a document");
        return b(a)
    } : b(a)
}("undefined" != typeof window ? window : this, function (a, b) {
    "use strict";
    var c = [], d = a.document, e = Object.getPrototypeOf, f = c.slice, g = c.concat, h = c.push, i = c.indexOf, j = {},
        k = j.toString, l = j.hasOwnProperty, m = l.toString, n = m.call(Object), o = {};

    function p(a, b) {
        b = b || d;
        var c = b.createElement("script");
        c.text = a, b.head.appendChild(c).parentNode.removeChild(c)
    }

    var q = "3.2.1", r = function (a, b) {
        return new r.fn.init(a, b)
    }, s = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, t = /^-ms-/, u = /-([a-z])/g, v = function (a, b) {
        return b.toUpperCase()
    };
    r.fn = r.prototype = {
        jquery: q, constructor: r, length: 0, toArray: function () {
            return f.call(this)
        }, get: function (a) {
            return null == a ? f.call(this) : a < 0 ? this[a + this.length] : this[a]
        }, pushStack: function (a) {
            var b = r.merge(this.constructor(), a);
            return b.prevObject = this, b
        }, each: function (a) {
            return r.each(this, a)
        }, map: function (a) {
            return this.pushStack(r.map(this, function (b, c) {
                return a.call(b, c, b)
            }))
        }, slice: function () {
            return this.pushStack(f.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (a) {
            var b = this.length, c = +a + (a < 0 ? b : 0);
            return this.pushStack(c >= 0 && c < b ? [this[c]] : [])
        }, end: function () {
            return this.prevObject || this.constructor()
        }, push: h, sort: c.sort, splice: c.splice
    }, r.extend = r.fn.extend = function () {
        var a, b, c, d, e, f, g = arguments[0] || {}, h = 1, i = arguments.length, j = !1;
        for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || r.isFunction(g) || (g = {}), h === i && (g = this, h--); h < i; h++) if (null != (a = arguments[h])) for (b in a) c = g[b], d = a[b], g !== d && (j && d && (r.isPlainObject(d) || (e = Array.isArray(d))) ? (e ? (e = !1, f = c && Array.isArray(c) ? c : []) : f = c && r.isPlainObject(c) ? c : {}, g[b] = r.extend(j, f, d)) : void 0 !== d && (g[b] = d));
        return g
    }, r.extend({
        expando: "jQuery" + (q + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (a) {
            throw new Error(a)
        }, noop: function () {
        }, isFunction: function (a) {
            return "function" === r.type(a)
        }, isWindow: function (a) {
            return null != a && a === a.window
        }, isNumeric: function (a) {
            var b = r.type(a);
            return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a))
        }, isPlainObject: function (a) {
            var b, c;
            return !(!a || "[object Object]" !== k.call(a)) && (!(b = e(a)) || (c = l.call(b, "constructor") && b.constructor, "function" == typeof c && m.call(c) === n))
        }, isEmptyObject: function (a) {
            var b;
            for (b in a) return !1;
            return !0
        }, type: function (a) {
            return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? j[k.call(a)] || "object" : typeof a
        }, globalEval: function (a) {
            p(a)
        }, camelCase: function (a) {
            return a.replace(t, "ms-").replace(u, v)
        }, each: function (a, b) {
            var c, d = 0;
            if (w(a)) {
                for (c = a.length; d < c; d++) if (b.call(a[d], d, a[d]) === !1) break
            } else for (d in a) if (b.call(a[d], d, a[d]) === !1) break;
            return a
        }, trim: function (a) {
            return null == a ? "" : (a + "").replace(s, "")
        }, makeArray: function (a, b) {
            var c = b || [];
            return null != a && (w(Object(a)) ? r.merge(c, "string" == typeof a ? [a] : a) : h.call(c, a)), c
        }, inArray: function (a, b, c) {
            return null == b ? -1 : i.call(b, a, c)
        }, merge: function (a, b) {
            for (var c = +b.length, d = 0, e = a.length; d < c; d++) a[e++] = b[d];
            return a.length = e, a
        }, grep: function (a, b, c) {
            for (var d, e = [], f = 0, g = a.length, h = !c; f < g; f++) d = !b(a[f], f), d !== h && e.push(a[f]);
            return e
        }, map: function (a, b, c) {
            var d, e, f = 0, h = [];
            if (w(a)) for (d = a.length; f < d; f++) e = b(a[f], f, c), null != e && h.push(e); else for (f in a) e = b(a[f], f, c), null != e && h.push(e);
            return g.apply([], h)
        }, guid: 1, proxy: function (a, b) {
            var c, d, e;
            if ("string" == typeof b && (c = a[b], b = a, a = c), r.isFunction(a)) return d = f.call(arguments, 2), e = function () {
                return a.apply(b || this, d.concat(f.call(arguments)))
            }, e.guid = a.guid = a.guid || r.guid++, e
        }, now: Date.now, support: o
    }), "function" == typeof Symbol && (r.fn[Symbol.iterator] = c[Symbol.iterator]), r.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (a, b) {
        j["[object " + b + "]"] = b.toLowerCase()
    });

    function w(a) {
        var b = !!a && "length" in a && a.length, c = r.type(a);
        return "function" !== c && !r.isWindow(a) && ("array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a)
    }

    var x = function (a) {
        var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + 1 * new Date, v = a.document, w = 0,
            x = 0, y = ha(), z = ha(), A = ha(), B = function (a, b) {
                return a === b && (l = !0), 0
            }, C = {}.hasOwnProperty, D = [], E = D.pop, F = D.push, G = D.push, H = D.slice, I = function (a, b) {
                for (var c = 0, d = a.length; c < d; c++) if (a[c] === b) return c;
                return -1
            },
            J = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            K = "[\\x20\\t\\r\\n\\f]", L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            M = "\\[" + K + "*(" + L + ")(?:" + K + "*([*^$|!~]?=)" + K + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + K + "*\\]",
            N = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + M + ")*)|.*)\\)|)",
            O = new RegExp(K + "+", "g"), P = new RegExp("^" + K + "+|((?:^|[^\\\\])(?:\\\\.)*)" + K + "+$", "g"),
            Q = new RegExp("^" + K + "*," + K + "*"), R = new RegExp("^" + K + "*([>+~]|" + K + ")" + K + "*"),
            S = new RegExp("=" + K + "*([^\\]'\"]*?)" + K + "*\\]", "g"), T = new RegExp(N),
            U = new RegExp("^" + L + "$"), V = {
                ID: new RegExp("^#(" + L + ")"),
                CLASS: new RegExp("^\\.(" + L + ")"),
                TAG: new RegExp("^(" + L + "|[*])"),
                ATTR: new RegExp("^" + M),
                PSEUDO: new RegExp("^" + N),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + K + "*(even|odd|(([+-]|)(\\d*)n|)" + K + "*(?:([+-]|)" + K + "*(\\d+)|))" + K + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + J + ")$", "i"),
                needsContext: new RegExp("^" + K + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + K + "*((?:-\\d)?\\d*)" + K + "*\\)|)(?=[^-]|$)", "i")
            }, W = /^(?:input|select|textarea|button)$/i, X = /^h\d$/i, Y = /^[^{]+\{\s*\[native \w/,
            Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, $ = /[+~]/,
            _ = new RegExp("\\\\([\\da-f]{1,6}" + K + "?|(" + K + ")|.)", "ig"), aa = function (a, b, c) {
                var d = "0x" + b - 65536;
                return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
            }, ba = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, ca = function (a, b) {
                return b ? "\0" === a ? "�" : a.slice(0, -1) + "\\" + a.charCodeAt(a.length - 1).toString(16) + " " : "\\" + a
            }, da = function () {
                m()
            }, ea = ta(function (a) {
                return a.disabled === !0 && ("form" in a || "label" in a)
            }, {dir: "parentNode", next: "legend"});
        try {
            G.apply(D = H.call(v.childNodes), v.childNodes), D[v.childNodes.length].nodeType
        } catch (fa) {
            G = {
                apply: D.length ? function (a, b) {
                    F.apply(a, H.call(b))
                } : function (a, b) {
                    var c = a.length, d = 0;
                    while (a[c++] = b[d++]) ;
                    a.length = c - 1
                }
            }
        }

        function ga(a, b, d, e) {
            var f, h, j, k, l, o, r, s = b && b.ownerDocument, w = b ? b.nodeType : 9;
            if (d = d || [], "string" != typeof a || !a || 1 !== w && 9 !== w && 11 !== w) return d;
            if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {
                if (11 !== w && (l = Z.exec(a))) if (f = l[1]) {
                    if (9 === w) {
                        if (!(j = b.getElementById(f))) return d;
                        if (j.id === f) return d.push(j), d
                    } else if (s && (j = s.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d
                } else {
                    if (l[2]) return G.apply(d, b.getElementsByTagName(a)), d;
                    if ((f = l[3]) && c.getElementsByClassName && b.getElementsByClassName) return G.apply(d, b.getElementsByClassName(f)), d
                }
                if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
                    if (1 !== w) s = b, r = a; else if ("object" !== b.nodeName.toLowerCase()) {
                        (k = b.getAttribute("id")) ? k = k.replace(ba, ca) : b.setAttribute("id", k = u), o = g(a), h = o.length;
                        while (h--) o[h] = "#" + k + " " + sa(o[h]);
                        r = o.join(","), s = $.test(a) && qa(b.parentNode) || b
                    }
                    if (r) try {
                        return G.apply(d, s.querySelectorAll(r)), d
                    } catch (x) {
                    } finally {
                        k === u && b.removeAttribute("id")
                    }
                }
            }
            return i(a.replace(P, "$1"), b, d, e)
        }

        function ha() {
            var a = [];

            function b(c, e) {
                return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e
            }

            return b
        }

        function ia(a) {
            return a[u] = !0, a
        }

        function ja(a) {
            var b = n.createElement("fieldset");
            try {
                return !!a(b)
            } catch (c) {
                return !1
            } finally {
                b.parentNode && b.parentNode.removeChild(b), b = null
            }
        }

        function ka(a, b) {
            var c = a.split("|"), e = c.length;
            while (e--) d.attrHandle[c[e]] = b
        }

        function la(a, b) {
            var c = b && a, d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;
            if (d) return d;
            if (c) while (c = c.nextSibling) if (c === b) return -1;
            return a ? 1 : -1
        }

        function ma(a) {
            return function (b) {
                var c = b.nodeName.toLowerCase();
                return "input" === c && b.type === a
            }
        }

        function na(a) {
            return function (b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a
            }
        }

        function oa(a) {
            return function (b) {
                return "form" in b ? b.parentNode && b.disabled === !1 ? "label" in b ? "label" in b.parentNode ? b.parentNode.disabled === a : b.disabled === a : b.isDisabled === a || b.isDisabled !== !a && ea(b) === a : b.disabled === a : "label" in b && b.disabled === a
            }
        }

        function pa(a) {
            return ia(function (b) {
                return b = +b, ia(function (c, d) {
                    var e, f = a([], c.length, b), g = f.length;
                    while (g--) c[e = f[g]] && (c[e] = !(d[e] = c[e]))
                })
            })
        }

        function qa(a) {
            return a && "undefined" != typeof a.getElementsByTagName && a
        }

        c = ga.support = {}, f = ga.isXML = function (a) {
            var b = a && (a.ownerDocument || a).documentElement;
            return !!b && "HTML" !== b.nodeName
        }, m = ga.setDocument = function (a) {
            var b, e, g = a ? a.ownerDocument || a : v;
            return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), v !== n && (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)), c.attributes = ja(function (a) {
                return a.className = "i", !a.getAttribute("className")
            }), c.getElementsByTagName = ja(function (a) {
                return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length
            }), c.getElementsByClassName = Y.test(n.getElementsByClassName), c.getById = ja(function (a) {
                return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length
            }), c.getById ? (d.filter.ID = function (a) {
                var b = a.replace(_, aa);
                return function (a) {
                    return a.getAttribute("id") === b
                }
            }, d.find.ID = function (a, b) {
                if ("undefined" != typeof b.getElementById && p) {
                    var c = b.getElementById(a);
                    return c ? [c] : []
                }
            }) : (d.filter.ID = function (a) {
                var b = a.replace(_, aa);
                return function (a) {
                    var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
                    return c && c.value === b
                }
            }, d.find.ID = function (a, b) {
                if ("undefined" != typeof b.getElementById && p) {
                    var c, d, e, f = b.getElementById(a);
                    if (f) {
                        if (c = f.getAttributeNode("id"), c && c.value === a) return [f];
                        e = b.getElementsByName(a), d = 0;
                        while (f = e[d++]) if (c = f.getAttributeNode("id"), c && c.value === a) return [f]
                    }
                    return []
                }
            }), d.find.TAG = c.getElementsByTagName ? function (a, b) {
                return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0
            } : function (a, b) {
                var c, d = [], e = 0, f = b.getElementsByTagName(a);
                if ("*" === a) {
                    while (c = f[e++]) 1 === c.nodeType && d.push(c);
                    return d
                }
                return f
            }, d.find.CLASS = c.getElementsByClassName && function (a, b) {
                if ("undefined" != typeof b.getElementsByClassName && p) return b.getElementsByClassName(a)
            }, r = [], q = [], (c.qsa = Y.test(n.querySelectorAll)) && (ja(function (a) {
                o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + K + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + K + "*(?:value|" + J + ")"), a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]")
            }), ja(function (a) {
                a.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var b = n.createElement("input");
                b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + K + "*[*^$|!~]?="), 2 !== a.querySelectorAll(":enabled").length && q.push(":enabled", ":disabled"), o.appendChild(a).disabled = !0, 2 !== a.querySelectorAll(":disabled").length && q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:")
            })), (c.matchesSelector = Y.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ja(function (a) {
                c.disconnectedMatch = s.call(a, "*"), s.call(a, "[s!='']:x"), r.push("!=", N)
            }), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = Y.test(o.compareDocumentPosition), t = b || Y.test(o.contains) ? function (a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a, d = b && b.parentNode;
                return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
            } : function (a, b) {
                if (b) while (b = b.parentNode) if (b === a) return !0;
                return !1
            }, B = b ? function (a, b) {
                if (a === b) return l = !0, 0;
                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? I(k, a) - I(k, b) : 0 : 4 & d ? -1 : 1)
            } : function (a, b) {
                if (a === b) return l = !0, 0;
                var c, d = 0, e = a.parentNode, f = b.parentNode, g = [a], h = [b];
                if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? I(k, a) - I(k, b) : 0;
                if (e === f) return la(a, b);
                c = a;
                while (c = c.parentNode) g.unshift(c);
                c = b;
                while (c = c.parentNode) h.unshift(c);
                while (g[d] === h[d]) d++;
                return d ? la(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0
            }, n) : n
        }, ga.matches = function (a, b) {
            return ga(a, null, null, b)
        }, ga.matchesSelector = function (a, b) {
            if ((a.ownerDocument || a) !== n && m(a), b = b.replace(S, "='$1']"), c.matchesSelector && p && !A[b + " "] && (!r || !r.test(b)) && (!q || !q.test(b))) try {
                var d = s.call(a, b);
                if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d
            } catch (e) {
            }
            return ga(b, n, null, [a]).length > 0
        }, ga.contains = function (a, b) {
            return (a.ownerDocument || a) !== n && m(a), t(a, b)
        }, ga.attr = function (a, b) {
            (a.ownerDocument || a) !== n && m(a);
            var e = d.attrHandle[b.toLowerCase()],
                f = e && C.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
            return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null
        }, ga.escape = function (a) {
            return (a + "").replace(ba, ca)
        }, ga.error = function (a) {
            throw new Error("Syntax error, unrecognized expression: " + a)
        }, ga.uniqueSort = function (a) {
            var b, d = [], e = 0, f = 0;
            if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
                while (b = a[f++]) b === a[f] && (e = d.push(f));
                while (e--) a.splice(d[e], 1)
            }
            return k = null, a
        }, e = ga.getText = function (a) {
            var b, c = "", d = 0, f = a.nodeType;
            if (f) {
                if (1 === f || 9 === f || 11 === f) {
                    if ("string" == typeof a.textContent) return a.textContent;
                    for (a = a.firstChild; a; a = a.nextSibling) c += e(a)
                } else if (3 === f || 4 === f) return a.nodeValue
            } else while (b = a[d++]) c += e(b);
            return c
        }, d = ga.selectors = {
            cacheLength: 50,
            createPseudo: ia,
            match: V,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (a) {
                    return a[1] = a[1].replace(_, aa), a[3] = (a[3] || a[4] || a[5] || "").replace(_, aa), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
                }, CHILD: function (a) {
                    return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || ga.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && ga.error(a[0]), a
                }, PSEUDO: function (a) {
                    var b, c = !a[6] && a[2];
                    return V.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && T.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
                }
            },
            filter: {
                TAG: function (a) {
                    var b = a.replace(_, aa).toLowerCase();
                    return "*" === a ? function () {
                        return !0
                    } : function (a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b
                    }
                }, CLASS: function (a) {
                    var b = y[a + " "];
                    return b || (b = new RegExp("(^|" + K + ")" + a + "(" + K + "|$)")) && y(a, function (a) {
                        return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "")
                    })
                }, ATTR: function (a, b, c) {
                    return function (d) {
                        var e = ga.attr(d, a);
                        return null == e ? "!=" === b : !b || (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(O, " ") + " ").indexOf(c) > -1 : "|=" === b && (e === c || e.slice(0, c.length + 1) === c + "-"))
                    }
                }, CHILD: function (a, b, c, d, e) {
                    var f = "nth" !== a.slice(0, 3), g = "last" !== a.slice(-4), h = "of-type" === b;
                    return 1 === d && 0 === e ? function (a) {
                        return !!a.parentNode
                    } : function (b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode,
                            r = h && b.nodeName.toLowerCase(), s = !i && !h, t = !1;
                        if (q) {
                            if (f) {
                                while (p) {
                                    m = b;
                                    while (m = m[p]) if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
                                    o = p = "only" === a && !o && "nextSibling"
                                }
                                return !0
                            }
                            if (o = [g ? q.firstChild : q.lastChild], g && s) {
                                m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n];
                                while (m = ++n && m && m[p] || (t = n = 0) || o.pop()) if (1 === m.nodeType && ++t && m === b) {
                                    k[a] = [w, n, t];
                                    break
                                }
                            } else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1) while (m = ++n && m && m[p] || (t = n = 0) || o.pop()) if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m === b)) break;
                            return t -= e, t === d || t % d === 0 && t / d >= 0
                        }
                    }
                }, PSEUDO: function (a, b) {
                    var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error("unsupported pseudo: " + a);
                    return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ia(function (a, c) {
                        var d, f = e(a, b), g = f.length;
                        while (g--) d = I(a, f[g]), a[d] = !(c[d] = f[g])
                    }) : function (a) {
                        return e(a, 0, c)
                    }) : e
                }
            },
            pseudos: {
                not: ia(function (a) {
                    var b = [], c = [], d = h(a.replace(P, "$1"));
                    return d[u] ? ia(function (a, b, c, e) {
                        var f, g = d(a, null, e, []), h = a.length;
                        while (h--) (f = g[h]) && (a[h] = !(b[h] = f))
                    }) : function (a, e, f) {
                        return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop()
                    }
                }), has: ia(function (a) {
                    return function (b) {
                        return ga(a, b).length > 0
                    }
                }), contains: ia(function (a) {
                    return a = a.replace(_, aa), function (b) {
                        return (b.textContent || b.innerText || e(b)).indexOf(a) > -1
                    }
                }), lang: ia(function (a) {
                    return U.test(a || "") || ga.error("unsupported lang: " + a), a = a.replace(_, aa).toLowerCase(), function (b) {
                        var c;
                        do {
                            if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-")
                        } while ((b = b.parentNode) && 1 === b.nodeType);
                        return !1
                    }
                }), target: function (b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id
                }, root: function (a) {
                    return a === o
                }, focus: function (a) {
                    return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
                }, enabled: oa(!1), disabled: oa(!0), checked: function (a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected
                }, selected: function (a) {
                    return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
                }, empty: function (a) {
                    for (a = a.firstChild; a; a = a.nextSibling) if (a.nodeType < 6) return !1;
                    return !0
                }, parent: function (a) {
                    return !d.pseudos.empty(a)
                }, header: function (a) {
                    return X.test(a.nodeName)
                }, input: function (a) {
                    return W.test(a.nodeName)
                }, button: function (a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b
                }, text: function (a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
                }, first: pa(function () {
                    return [0]
                }), last: pa(function (a, b) {
                    return [b - 1]
                }), eq: pa(function (a, b, c) {
                    return [c < 0 ? c + b : c]
                }), even: pa(function (a, b) {
                    for (var c = 0; c < b; c += 2) a.push(c);
                    return a
                }), odd: pa(function (a, b) {
                    for (var c = 1; c < b; c += 2) a.push(c);
                    return a
                }), lt: pa(function (a, b, c) {
                    for (var d = c < 0 ? c + b : c; --d >= 0;) a.push(d);
                    return a
                }), gt: pa(function (a, b, c) {
                    for (var d = c < 0 ? c + b : c; ++d < b;) a.push(d);
                    return a
                })
            }
        }, d.pseudos.nth = d.pseudos.eq;
        for (b in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0}) d.pseudos[b] = ma(b);
        for (b in{submit: !0, reset: !0}) d.pseudos[b] = na(b);

        function ra() {
        }

        ra.prototype = d.filters = d.pseudos, d.setFilters = new ra, g = ga.tokenize = function (a, b) {
            var c, e, f, g, h, i, j, k = z[a + " "];
            if (k) return b ? 0 : k.slice(0);
            h = a, i = [], j = d.preFilter;
            while (h) {
                c && !(e = Q.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = R.exec(h)) && (c = e.shift(), f.push({
                    value: c,
                    type: e[0].replace(P, " ")
                }), h = h.slice(c.length));
                for (g in d.filter) !(e = V[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({
                    value: c,
                    type: g,
                    matches: e
                }), h = h.slice(c.length));
                if (!c) break
            }
            return b ? h.length : h ? ga.error(a) : z(a, i).slice(0)
        };

        function sa(a) {
            for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
            return d
        }

        function ta(a, b, c) {
            var d = b.dir, e = b.next, f = e || d, g = c && "parentNode" === f, h = x++;
            return b.first ? function (b, c, e) {
                while (b = b[d]) if (1 === b.nodeType || g) return a(b, c, e);
                return !1
            } : function (b, c, i) {
                var j, k, l, m = [w, h];
                if (i) {
                    while (b = b[d]) if ((1 === b.nodeType || g) && a(b, c, i)) return !0
                } else while (b = b[d]) if (1 === b.nodeType || g) if (l = b[u] || (b[u] = {}), k = l[b.uniqueID] || (l[b.uniqueID] = {}), e && e === b.nodeName.toLowerCase()) b = b[d] || b; else {
                    if ((j = k[f]) && j[0] === w && j[1] === h) return m[2] = j[2];
                    if (k[f] = m, m[2] = a(b, c, i)) return !0
                }
                return !1
            }
        }

        function ua(a) {
            return a.length > 1 ? function (b, c, d) {
                var e = a.length;
                while (e--) if (!a[e](b, c, d)) return !1;
                return !0
            } : a[0]
        }

        function va(a, b, c) {
            for (var d = 0, e = b.length; d < e; d++) ga(a, b[d], c);
            return c
        }

        function wa(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++) (f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
            return g
        }

        function xa(a, b, c, d, e, f) {
            return d && !d[u] && (d = xa(d)), e && !e[u] && (e = xa(e, f)), ia(function (f, g, h, i) {
                var j, k, l, m = [], n = [], o = g.length, p = f || va(b || "*", h.nodeType ? [h] : h, []),
                    q = !a || !f && b ? p : wa(p, m, a, h, i), r = c ? e || (f ? a : o || d) ? [] : g : q;
                if (c && c(q, r, h, i), d) {
                    j = wa(r, n), d(j, [], h, i), k = j.length;
                    while (k--) (l = j[k]) && (r[n[k]] = !(q[n[k]] = l))
                }
                if (f) {
                    if (e || a) {
                        if (e) {
                            j = [], k = r.length;
                            while (k--) (l = r[k]) && j.push(q[k] = l);
                            e(null, r = [], j, i)
                        }
                        k = r.length;
                        while (k--) (l = r[k]) && (j = e ? I(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l))
                    }
                } else r = wa(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : G.apply(g, r)
            })
        }

        function ya(a) {
            for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = ta(function (a) {
                return a === b
            }, h, !0), l = ta(function (a) {
                return I(b, a) > -1
            }, h, !0), m = [function (a, c, d) {
                var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
                return b = null, e
            }]; i < f; i++) if (c = d.relative[a[i].type]) m = [ta(ua(m), c)]; else {
                if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
                    for (e = ++i; e < f; e++) if (d.relative[a[e].type]) break;
                    return xa(i > 1 && ua(m), i > 1 && sa(a.slice(0, i - 1).concat({value: " " === a[i - 2].type ? "*" : ""})).replace(P, "$1"), c, i < e && ya(a.slice(i, e)), e < f && ya(a = a.slice(e)), e < f && sa(a))
                }
                m.push(c)
            }
            return ua(m)
        }

        function za(a, b) {
            var c = b.length > 0, e = a.length > 0, f = function (f, g, h, i, k) {
                var l, o, q, r = 0, s = "0", t = f && [], u = [], v = j, x = f || e && d.find.TAG("*", k),
                    y = w += null == v ? 1 : Math.random() || .1, z = x.length;
                for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
                    if (e && l) {
                        o = 0, g || l.ownerDocument === n || (m(l), h = !p);
                        while (q = a[o++]) if (q(l, g || n, h)) {
                            i.push(l);
                            break
                        }
                        k && (w = y)
                    }
                    c && ((l = !q && l) && r--, f && t.push(l))
                }
                if (r += s, c && s !== r) {
                    o = 0;
                    while (q = b[o++]) q(t, u, g, h);
                    if (f) {
                        if (r > 0) while (s--) t[s] || u[s] || (u[s] = E.call(i));
                        u = wa(u)
                    }
                    G.apply(i, u), k && !f && u.length > 0 && r + b.length > 1 && ga.uniqueSort(i)
                }
                return k && (w = y, j = v), t
            };
            return c ? ia(f) : f
        }

        return h = ga.compile = function (a, b) {
            var c, d = [], e = [], f = A[a + " "];
            if (!f) {
                b || (b = g(a)), c = b.length;
                while (c--) f = ya(b[c]), f[u] ? d.push(f) : e.push(f);
                f = A(a, za(e, d)), f.selector = a
            }
            return f
        }, i = ga.select = function (a, b, c, e) {
            var f, i, j, k, l, m = "function" == typeof a && a, n = !e && g(a = m.selector || a);
            if (c = c || [], 1 === n.length) {
                if (i = n[0] = n[0].slice(0), i.length > 2 && "ID" === (j = i[0]).type && 9 === b.nodeType && p && d.relative[i[1].type]) {
                    if (b = (d.find.ID(j.matches[0].replace(_, aa), b) || [])[0], !b) return c;
                    m && (b = b.parentNode), a = a.slice(i.shift().value.length)
                }
                f = V.needsContext.test(a) ? 0 : i.length;
                while (f--) {
                    if (j = i[f], d.relative[k = j.type]) break;
                    if ((l = d.find[k]) && (e = l(j.matches[0].replace(_, aa), $.test(i[0].type) && qa(b.parentNode) || b))) {
                        if (i.splice(f, 1), a = e.length && sa(i), !a) return G.apply(c, e), c;
                        break
                    }
                }
            }
            return (m || h(a, n))(e, b, !p, c, !b || $.test(a) && qa(b.parentNode) || b), c
        }, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ja(function (a) {
            return 1 & a.compareDocumentPosition(n.createElement("fieldset"))
        }), ja(function (a) {
            return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
        }) || ka("type|href|height|width", function (a, b, c) {
            if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
        }), c.attributes && ja(function (a) {
            return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
        }) || ka("value", function (a, b, c) {
            if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue
        }), ja(function (a) {
            return null == a.getAttribute("disabled")
        }) || ka(J, function (a, b, c) {
            var d;
            if (!c) return a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
        }), ga
    }(a);
    r.find = x, r.expr = x.selectors, r.expr[":"] = r.expr.pseudos, r.uniqueSort = r.unique = x.uniqueSort, r.text = x.getText, r.isXMLDoc = x.isXML, r.contains = x.contains, r.escapeSelector = x.escape;
    var y = function (a, b, c) {
        var d = [], e = void 0 !== c;
        while ((a = a[b]) && 9 !== a.nodeType) if (1 === a.nodeType) {
            if (e && r(a).is(c)) break;
            d.push(a)
        }
        return d
    }, z = function (a, b) {
        for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
        return c
    }, A = r.expr.match.needsContext;

    function B(a, b) {
        return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
    }

    var C = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i, D = /^.[^:#\[\.,]*$/;

    function E(a, b, c) {
        return r.isFunction(b) ? r.grep(a, function (a, d) {
            return !!b.call(a, d, a) !== c
        }) : b.nodeType ? r.grep(a, function (a) {
            return a === b !== c
        }) : "string" != typeof b ? r.grep(a, function (a) {
            return i.call(b, a) > -1 !== c
        }) : D.test(b) ? r.filter(b, a, c) : (b = r.filter(b, a), r.grep(a, function (a) {
            return i.call(b, a) > -1 !== c && 1 === a.nodeType
        }))
    }

    r.filter = function (a, b, c) {
        var d = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? r.find.matchesSelector(d, a) ? [d] : [] : r.find.matches(a, r.grep(b, function (a) {
            return 1 === a.nodeType
        }))
    }, r.fn.extend({
        find: function (a) {
            var b, c, d = this.length, e = this;
            if ("string" != typeof a) return this.pushStack(r(a).filter(function () {
                for (b = 0; b < d; b++) if (r.contains(e[b], this)) return !0
            }));
            for (c = this.pushStack([]), b = 0; b < d; b++) r.find(a, e[b], c);
            return d > 1 ? r.uniqueSort(c) : c
        }, filter: function (a) {
            return this.pushStack(E(this, a || [], !1))
        }, not: function (a) {
            return this.pushStack(E(this, a || [], !0))
        }, is: function (a) {
            return !!E(this, "string" == typeof a && A.test(a) ? r(a) : a || [], !1).length
        }
    });
    var F, G = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/, H = r.fn.init = function (a, b, c) {
        var e, f;
        if (!a) return this;
        if (c = c || F, "string" == typeof a) {
            if (e = "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : G.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
            if (e[1]) {
                if (b = b instanceof r ? b[0] : b, r.merge(this, r.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), C.test(e[1]) && r.isPlainObject(b)) for (e in b) r.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
                return this
            }
            return f = d.getElementById(e[2]), f && (this[0] = f, this.length = 1), this
        }
        return a.nodeType ? (this[0] = a, this.length = 1, this) : r.isFunction(a) ? void 0 !== c.ready ? c.ready(a) : a(r) : r.makeArray(a, this)
    };
    H.prototype = r.fn, F = r(d);
    var I = /^(?:parents|prev(?:Until|All))/, J = {children: !0, contents: !0, next: !0, prev: !0};
    r.fn.extend({
        has: function (a) {
            var b = r(a, this), c = b.length;
            return this.filter(function () {
                for (var a = 0; a < c; a++) if (r.contains(this, b[a])) return !0
            })
        }, closest: function (a, b) {
            var c, d = 0, e = this.length, f = [], g = "string" != typeof a && r(a);
            if (!A.test(a)) for (; d < e; d++) for (c = this[d]; c && c !== b; c = c.parentNode) if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && r.find.matchesSelector(c, a))) {
                f.push(c);
                break
            }
            return this.pushStack(f.length > 1 ? r.uniqueSort(f) : f)
        }, index: function (a) {
            return a ? "string" == typeof a ? i.call(r(a), this[0]) : i.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (a, b) {
            return this.pushStack(r.uniqueSort(r.merge(this.get(), r(a, b))))
        }, addBack: function (a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        }
    });

    function K(a, b) {
        while ((a = a[b]) && 1 !== a.nodeType) ;
        return a
    }

    r.each({
        parent: function (a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null
        }, parents: function (a) {
            return y(a, "parentNode")
        }, parentsUntil: function (a, b, c) {
            return y(a, "parentNode", c)
        }, next: function (a) {
            return K(a, "nextSibling")
        }, prev: function (a) {
            return K(a, "previousSibling")
        }, nextAll: function (a) {
            return y(a, "nextSibling")
        }, prevAll: function (a) {
            return y(a, "previousSibling")
        }, nextUntil: function (a, b, c) {
            return y(a, "nextSibling", c)
        }, prevUntil: function (a, b, c) {
            return y(a, "previousSibling", c)
        }, siblings: function (a) {
            return z((a.parentNode || {}).firstChild, a)
        }, children: function (a) {
            return z(a.firstChild)
        }, contents: function (a) {
            return B(a, "iframe") ? a.contentDocument : (B(a, "template") && (a = a.content || a), r.merge([], a.childNodes))
        }
    }, function (a, b) {
        r.fn[a] = function (c, d) {
            var e = r.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = r.filter(d, e)), this.length > 1 && (J[a] || r.uniqueSort(e), I.test(a) && e.reverse()), this.pushStack(e)
        }
    });
    var L = /[^\x20\t\r\n\f]+/g;

    function M(a) {
        var b = {};
        return r.each(a.match(L) || [], function (a, c) {
            b[c] = !0
        }), b
    }

    r.Callbacks = function (a) {
        a = "string" == typeof a ? M(a) : r.extend({}, a);
        var b, c, d, e, f = [], g = [], h = -1, i = function () {
            for (e = e || a.once, d = b = !0; g.length; h = -1) {
                c = g.shift();
                while (++h < f.length) f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1)
            }
            a.memory || (c = !1), b = !1, e && (f = c ? [] : "")
        }, j = {
            add: function () {
                return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {
                    r.each(b, function (b, c) {
                        r.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && "string" !== r.type(c) && d(c)
                    })
                }(arguments), c && !b && i()), this
            }, remove: function () {
                return r.each(arguments, function (a, b) {
                    var c;
                    while ((c = r.inArray(b, f, c)) > -1) f.splice(c, 1), c <= h && h--
                }), this
            }, has: function (a) {
                return a ? r.inArray(a, f) > -1 : f.length > 0
            }, empty: function () {
                return f && (f = []), this
            }, disable: function () {
                return e = g = [], f = c = "", this
            }, disabled: function () {
                return !f
            }, lock: function () {
                return e = g = [], c || b || (f = c = ""), this
            }, locked: function () {
                return !!e
            }, fireWith: function (a, c) {
                return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this
            }, fire: function () {
                return j.fireWith(this, arguments), this
            }, fired: function () {
                return !!d
            }
        };
        return j
    };

    function N(a) {
        return a
    }

    function O(a) {
        throw a
    }

    function P(a, b, c, d) {
        var e;
        try {
            a && r.isFunction(e = a.promise) ? e.call(a).done(b).fail(c) : a && r.isFunction(e = a.then) ? e.call(a, b, c) : b.apply(void 0, [a].slice(d))
        } catch (a) {
            c.apply(void 0, [a])
        }
    }

    r.extend({
        Deferred: function (b) {
            var c = [["notify", "progress", r.Callbacks("memory"), r.Callbacks("memory"), 2], ["resolve", "done", r.Callbacks("once memory"), r.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", r.Callbacks("once memory"), r.Callbacks("once memory"), 1, "rejected"]],
                d = "pending", e = {
                    state: function () {
                        return d
                    }, always: function () {
                        return f.done(arguments).fail(arguments), this
                    }, catch: function (a) {
                        return e.then(null, a)
                    }, pipe: function () {
                        var a = arguments;
                        return r.Deferred(function (b) {
                            r.each(c, function (c, d) {
                                var e = r.isFunction(a[d[4]]) && a[d[4]];
                                f[d[1]](function () {
                                    var a = e && e.apply(this, arguments);
                                    a && r.isFunction(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + "With"](this, e ? [a] : arguments)
                                })
                            }), a = null
                        }).promise()
                    }, then: function (b, d, e) {
                        var f = 0;

                        function g(b, c, d, e) {
                            return function () {
                                var h = this, i = arguments, j = function () {
                                    var a, j;
                                    if (!(b < f)) {
                                        if (a = d.apply(h, i), a === c.promise()) throw new TypeError("Thenable self-resolution");
                                        j = a && ("object" == typeof a || "function" == typeof a) && a.then, r.isFunction(j) ? e ? j.call(a, g(f, c, N, e), g(f, c, O, e)) : (f++, j.call(a, g(f, c, N, e), g(f, c, O, e), g(f, c, N, c.notifyWith))) : (d !== N && (h = void 0, i = [a]), (e || c.resolveWith)(h, i))
                                    }
                                }, k = e ? j : function () {
                                    try {
                                        j()
                                    } catch (a) {
                                        r.Deferred.exceptionHook && r.Deferred.exceptionHook(a, k.stackTrace), b + 1 >= f && (d !== O && (h = void 0, i = [a]), c.rejectWith(h, i))
                                    }
                                };
                                b ? k() : (r.Deferred.getStackHook && (k.stackTrace = r.Deferred.getStackHook()), a.setTimeout(k))
                            }
                        }

                        return r.Deferred(function (a) {
                            c[0][3].add(g(0, a, r.isFunction(e) ? e : N, a.notifyWith)), c[1][3].add(g(0, a, r.isFunction(b) ? b : N)), c[2][3].add(g(0, a, r.isFunction(d) ? d : O))
                        }).promise()
                    }, promise: function (a) {
                        return null != a ? r.extend(a, e) : e
                    }
                }, f = {};
            return r.each(c, function (a, b) {
                var g = b[2], h = b[5];
                e[b[1]] = g.add, h && g.add(function () {
                    d = h
                }, c[3 - a][2].disable, c[0][2].lock), g.add(b[3].fire), f[b[0]] = function () {
                    return f[b[0] + "With"](this === f ? void 0 : this, arguments), this
                }, f[b[0] + "With"] = g.fireWith
            }), e.promise(f), b && b.call(f, f), f
        }, when: function (a) {
            var b = arguments.length, c = b, d = Array(c), e = f.call(arguments), g = r.Deferred(), h = function (a) {
                return function (c) {
                    d[a] = this, e[a] = arguments.length > 1 ? f.call(arguments) : c, --b || g.resolveWith(d, e)
                }
            };
            if (b <= 1 && (P(a, g.done(h(c)).resolve, g.reject, !b), "pending" === g.state() || r.isFunction(e[c] && e[c].then))) return g.then();
            while (c--) P(e[c], h(c), g.reject);
            return g.promise()
        }
    });
    var Q = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    r.Deferred.exceptionHook = function (b, c) {
        a.console && a.console.warn && b && Q.test(b.name) && a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c)
    }, r.readyException = function (b) {
        a.setTimeout(function () {
            throw b
        })
    };
    var R = r.Deferred();
    r.fn.ready = function (a) {
        return R.then(a)["catch"](function (a) {
            r.readyException(a)
        }), this
    }, r.extend({
        isReady: !1, readyWait: 1, ready: function (a) {
            (a === !0 ? --r.readyWait : r.isReady) || (r.isReady = !0, a !== !0 && --r.readyWait > 0 || R.resolveWith(d, [r]))
        }
    }), r.ready.then = R.then;

    function S() {
        d.removeEventListener("DOMContentLoaded", S), a.removeEventListener("load", S), r.ready()
    }

    "complete" === d.readyState || "loading" !== d.readyState && !d.documentElement.doScroll ? a.setTimeout(r.ready) : (d.addEventListener("DOMContentLoaded", S), a.addEventListener("load", S));
    var T = function (a, b, c, d, e, f, g) {
        var h = 0, i = a.length, j = null == c;
        if ("object" === r.type(c)) {
            e = !0;
            for (h in c) T(a, b, h, c[h], !0, f, g)
        } else if (void 0 !== d && (e = !0, r.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function (a, b, c) {
            return j.call(r(a), c)
        })), b)) for (; h < i; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
        return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
    }, U = function (a) {
        return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType
    };

    function V() {
        this.expando = r.expando + V.uid++
    }

    V.uid = 1, V.prototype = {
        cache: function (a) {
            var b = a[this.expando];
            return b || (b = {}, U(a) && (a.nodeType ? a[this.expando] = b : Object.defineProperty(a, this.expando, {
                value: b,
                configurable: !0
            }))), b
        }, set: function (a, b, c) {
            var d, e = this.cache(a);
            if ("string" == typeof b) e[r.camelCase(b)] = c; else for (d in b) e[r.camelCase(d)] = b[d];
            return e
        }, get: function (a, b) {
            return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][r.camelCase(b)]
        }, access: function (a, b, c) {
            return void 0 === b || b && "string" == typeof b && void 0 === c ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b)
        }, remove: function (a, b) {
            var c, d = a[this.expando];
            if (void 0 !== d) {
                if (void 0 !== b) {
                    Array.isArray(b) ? b = b.map(r.camelCase) : (b = r.camelCase(b), b = b in d ? [b] : b.match(L) || []), c = b.length;
                    while (c--) delete d[b[c]]
                }
                (void 0 === b || r.isEmptyObject(d)) && (a.nodeType ? a[this.expando] = void 0 : delete a[this.expando])
            }
        }, hasData: function (a) {
            var b = a[this.expando];
            return void 0 !== b && !r.isEmptyObject(b)
        }
    };
    var W = new V, X = new V, Y = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, Z = /[A-Z]/g;

    function $(a) {
        return "true" === a || "false" !== a && ("null" === a ? null : a === +a + "" ? +a : Y.test(a) ? JSON.parse(a) : a)
    }

    function _(a, b, c) {
        var d;
        if (void 0 === c && 1 === a.nodeType) if (d = "data-" + b.replace(Z, "-$&").toLowerCase(), c = a.getAttribute(d), "string" == typeof c) {
            try {
                c = $(c)
            } catch (e) {
            }
            X.set(a, b, c)
        } else c = void 0;
        return c
    }

    r.extend({
        hasData: function (a) {
            return X.hasData(a) || W.hasData(a)
        }, data: function (a, b, c) {
            return X.access(a, b, c)
        }, removeData: function (a, b) {
            X.remove(a, b)
        }, _data: function (a, b, c) {
            return W.access(a, b, c)
        }, _removeData: function (a, b) {
            W.remove(a, b)
        }
    }), r.fn.extend({
        data: function (a, b) {
            var c, d, e, f = this[0], g = f && f.attributes;
            if (void 0 === a) {
                if (this.length && (e = X.get(f), 1 === f.nodeType && !W.get(f, "hasDataAttrs"))) {
                    c = g.length;
                    while (c--) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = r.camelCase(d.slice(5)), _(f, d, e[d])));
                    W.set(f, "hasDataAttrs", !0)
                }
                return e
            }
            return "object" == typeof a ? this.each(function () {
                X.set(this, a)
            }) : T(this, function (b) {
                var c;
                if (f && void 0 === b) {
                    if (c = X.get(f, a), void 0 !== c) return c;
                    if (c = _(f, a), void 0 !== c) return c
                } else this.each(function () {
                    X.set(this, a, b)
                })
            }, null, b, arguments.length > 1, null, !0)
        }, removeData: function (a) {
            return this.each(function () {
                X.remove(this, a)
            })
        }
    }), r.extend({
        queue: function (a, b, c) {
            var d;
            if (a) return b = (b || "fx") + "queue", d = W.get(a, b), c && (!d || Array.isArray(c) ? d = W.access(a, b, r.makeArray(c)) : d.push(c)), d || []
        }, dequeue: function (a, b) {
            b = b || "fx";
            var c = r.queue(a, b), d = c.length, e = c.shift(), f = r._queueHooks(a, b), g = function () {
                r.dequeue(a, b)
            };
            "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
        }, _queueHooks: function (a, b) {
            var c = b + "queueHooks";
            return W.get(a, c) || W.access(a, c, {
                empty: r.Callbacks("once memory").add(function () {
                    W.remove(a, [b + "queue", c])
                })
            })
        }
    }), r.fn.extend({
        queue: function (a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? r.queue(this[0], a) : void 0 === b ? this : this.each(function () {
                var c = r.queue(this, a, b);
                r._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && r.dequeue(this, a)
            })
        }, dequeue: function (a) {
            return this.each(function () {
                r.dequeue(this, a)
            })
        }, clearQueue: function (a) {
            return this.queue(a || "fx", [])
        }, promise: function (a, b) {
            var c, d = 1, e = r.Deferred(), f = this, g = this.length, h = function () {
                --d || e.resolveWith(f, [f])
            };
            "string" != typeof a && (b = a, a = void 0), a = a || "fx";
            while (g--) c = W.get(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
            return h(), e.promise(b)
        }
    });
    var aa = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, ba = new RegExp("^(?:([+-])=|)(" + aa + ")([a-z%]*)$", "i"),
        ca = ["Top", "Right", "Bottom", "Left"], da = function (a, b) {
            return a = b || a, "none" === a.style.display || "" === a.style.display && r.contains(a.ownerDocument, a) && "none" === r.css(a, "display")
        }, ea = function (a, b, c, d) {
            var e, f, g = {};
            for (f in b) g[f] = a.style[f], a.style[f] = b[f];
            e = c.apply(a, d || []);
            for (f in b) a.style[f] = g[f];
            return e
        };

    function fa(a, b, c, d) {
        var e, f = 1, g = 20, h = d ? function () {
                return d.cur()
            } : function () {
                return r.css(a, b, "")
            }, i = h(), j = c && c[3] || (r.cssNumber[b] ? "" : "px"),
            k = (r.cssNumber[b] || "px" !== j && +i) && ba.exec(r.css(a, b));
        if (k && k[3] !== j) {
            j = j || k[3], c = c || [], k = +i || 1;
            do {
                f = f || ".5", k /= f, r.style(a, b, k + j)
            } while (f !== (f = h() / i) && 1 !== f && --g)
        }
        return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e
    }

    var ga = {};

    function ha(a) {
        var b, c = a.ownerDocument, d = a.nodeName, e = ga[d];
        return e ? e : (b = c.body.appendChild(c.createElement(d)), e = r.css(b, "display"), b.parentNode.removeChild(b), "none" === e && (e = "block"), ga[d] = e, e)
    }

    function ia(a, b) {
        for (var c, d, e = [], f = 0, g = a.length; f < g; f++) d = a[f], d.style && (c = d.style.display, b ? ("none" === c && (e[f] = W.get(d, "display") || null, e[f] || (d.style.display = "")), "" === d.style.display && da(d) && (e[f] = ha(d))) : "none" !== c && (e[f] = "none", W.set(d, "display", c)));
        for (f = 0; f < g; f++) null != e[f] && (a[f].style.display = e[f]);
        return a
    }

    r.fn.extend({
        show: function () {
            return ia(this, !0)
        }, hide: function () {
            return ia(this)
        }, toggle: function (a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function () {
                da(this) ? r(this).show() : r(this).hide()
            })
        }
    });
    var ja = /^(?:checkbox|radio)$/i, ka = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i, la = /^$|\/(?:java|ecma)script/i, ma = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
    };
    ma.optgroup = ma.option, ma.tbody = ma.tfoot = ma.colgroup = ma.caption = ma.thead, ma.th = ma.td;

    function na(a, b) {
        var c;
        return c = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : [], void 0 === b || b && B(a, b) ? r.merge([a], c) : c
    }

    function oa(a, b) {
        for (var c = 0, d = a.length; c < d; c++) W.set(a[c], "globalEval", !b || W.get(b[c], "globalEval"))
    }

    var pa = /<|&#?\w+;/;

    function qa(a, b, c, d, e) {
        for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; n < o; n++) if (f = a[n], f || 0 === f) if ("object" === r.type(f)) r.merge(m, f.nodeType ? [f] : f); else if (pa.test(f)) {
            g = g || l.appendChild(b.createElement("div")), h = (ka.exec(f) || ["", ""])[1].toLowerCase(), i = ma[h] || ma._default, g.innerHTML = i[1] + r.htmlPrefilter(f) + i[2], k = i[0];
            while (k--) g = g.lastChild;
            r.merge(m, g.childNodes), g = l.firstChild, g.textContent = ""
        } else m.push(b.createTextNode(f));
        l.textContent = "", n = 0;
        while (f = m[n++]) if (d && r.inArray(f, d) > -1) e && e.push(f); else if (j = r.contains(f.ownerDocument, f), g = na(l.appendChild(f), "script"), j && oa(g), c) {
            k = 0;
            while (f = g[k++]) la.test(f.type || "") && c.push(f)
        }
        return l
    }

    !function () {
        var a = d.createDocumentFragment(), b = a.appendChild(d.createElement("div")), c = d.createElement("input");
        c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), b.appendChild(c), o.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, b.innerHTML = "<textarea>x</textarea>", o.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue
    }();
    var ra = d.documentElement, sa = /^key/, ta = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        ua = /^([^.]*)(?:\.(.+)|)/;

    function va() {
        return !0
    }

    function wa() {
        return !1
    }

    function xa() {
        try {
            return d.activeElement
        } catch (a) {
        }
    }

    function ya(a, b, c, d, e, f) {
        var g, h;
        if ("object" == typeof b) {
            "string" != typeof c && (d = d || c, c = void 0);
            for (h in b) ya(a, h, c, d, b[h], f);
            return a
        }
        if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = wa; else if (!e) return a;
        return 1 === f && (g = e, e = function (a) {
            return r().off(a), g.apply(this, arguments)
        }, e.guid = g.guid || (g.guid = r.guid++)), a.each(function () {
            r.event.add(this, b, e, d, c)
        })
    }

    r.event = {
        global: {}, add: function (a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = W.get(a);
            if (q) {
                c.handler && (f = c, c = f.handler, e = f.selector), e && r.find.matchesSelector(ra, e), c.guid || (c.guid = r.guid++), (i = q.events) || (i = q.events = {}), (g = q.handle) || (g = q.handle = function (b) {
                    return "undefined" != typeof r && r.event.triggered !== b.type ? r.event.dispatch.apply(a, arguments) : void 0
                }), b = (b || "").match(L) || [""], j = b.length;
                while (j--) h = ua.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n && (l = r.event.special[n] || {}, n = (e ? l.delegateType : l.bindType) || n, l = r.event.special[n] || {}, k = r.extend({
                    type: n,
                    origType: p,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && r.expr.match.needsContext.test(e),
                    namespace: o.join(".")
                }, f), (m = i[n]) || (m = i[n] = [], m.delegateCount = 0, l.setup && l.setup.call(a, d, o, g) !== !1 || a.addEventListener && a.addEventListener(n, g)), l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), r.event.global[n] = !0)
            }
        }, remove: function (a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = W.hasData(a) && W.get(a);
            if (q && (i = q.events)) {
                b = (b || "").match(L) || [""], j = b.length;
                while (j--) if (h = ua.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
                    l = r.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length;
                    while (f--) k = m[f], !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && ("**" !== d || !k.selector) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
                    g && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || r.removeEvent(a, n, q.handle), delete i[n])
                } else for (n in i) r.event.remove(a, n + b[j], c, d, !0);
                r.isEmptyObject(i) && W.remove(a, "handle events")
            }
        }, dispatch: function (a) {
            var b = r.event.fix(a), c, d, e, f, g, h, i = new Array(arguments.length),
                j = (W.get(this, "events") || {})[b.type] || [], k = r.event.special[b.type] || {};
            for (i[0] = b, c = 1; c < arguments.length; c++) i[c] = arguments[c];
            if (b.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, b) !== !1) {
                h = r.event.handlers.call(this, b, j), c = 0;
                while ((f = h[c++]) && !b.isPropagationStopped()) {
                    b.currentTarget = f.elem, d = 0;
                    while ((g = f.handlers[d++]) && !b.isImmediatePropagationStopped()) b.rnamespace && !b.rnamespace.test(g.namespace) || (b.handleObj = g, b.data = g.data, e = ((r.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== e && (b.result = e) === !1 && (b.preventDefault(), b.stopPropagation()))
                }
                return k.postDispatch && k.postDispatch.call(this, b), b.result
            }
        }, handlers: function (a, b) {
            var c, d, e, f, g, h = [], i = b.delegateCount, j = a.target;
            if (i && j.nodeType && !("click" === a.type && a.button >= 1)) for (; j !== this; j = j.parentNode || this) if (1 === j.nodeType && ("click" !== a.type || j.disabled !== !0)) {
                for (f = [], g = {}, c = 0; c < i; c++) d = b[c], e = d.selector + " ", void 0 === g[e] && (g[e] = d.needsContext ? r(e, this).index(j) > -1 : r.find(e, this, null, [j]).length), g[e] && f.push(d);
                f.length && h.push({elem: j, handlers: f})
            }
            return j = this, i < b.length && h.push({elem: j, handlers: b.slice(i)}), h
        }, addProp: function (a, b) {
            Object.defineProperty(r.Event.prototype, a, {
                enumerable: !0,
                configurable: !0,
                get: r.isFunction(b) ? function () {
                    if (this.originalEvent) return b(this.originalEvent)
                } : function () {
                    if (this.originalEvent) return this.originalEvent[a]
                },
                set: function (b) {
                    Object.defineProperty(this, a, {enumerable: !0, configurable: !0, writable: !0, value: b})
                }
            })
        }, fix: function (a) {
            return a[r.expando] ? a : new r.Event(a)
        }, special: {
            load: {noBubble: !0}, focus: {
                trigger: function () {
                    if (this !== xa() && this.focus) return this.focus(), !1
                }, delegateType: "focusin"
            }, blur: {
                trigger: function () {
                    if (this === xa() && this.blur) return this.blur(), !1
                }, delegateType: "focusout"
            }, click: {
                trigger: function () {
                    if ("checkbox" === this.type && this.click && B(this, "input")) return this.click(), !1
                }, _default: function (a) {
                    return B(a.target, "a")
                }
            }, beforeunload: {
                postDispatch: function (a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
                }
            }
        }
    }, r.removeEvent = function (a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c)
    }, r.Event = function (a, b) {
        return this instanceof r.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? va : wa, this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target, this.currentTarget = a.currentTarget, this.relatedTarget = a.relatedTarget) : this.type = a, b && r.extend(this, b), this.timeStamp = a && a.timeStamp || r.now(), void (this[r.expando] = !0)) : new r.Event(a, b)
    }, r.Event.prototype = {
        constructor: r.Event,
        isDefaultPrevented: wa,
        isPropagationStopped: wa,
        isImmediatePropagationStopped: wa,
        isSimulated: !1,
        preventDefault: function () {
            var a = this.originalEvent;
            this.isDefaultPrevented = va, a && !this.isSimulated && a.preventDefault()
        },
        stopPropagation: function () {
            var a = this.originalEvent;
            this.isPropagationStopped = va, a && !this.isSimulated && a.stopPropagation()
        },
        stopImmediatePropagation: function () {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = va, a && !this.isSimulated && a.stopImmediatePropagation(), this.stopPropagation()
        }
    }, r.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function (a) {
            var b = a.button;
            return null == a.which && sa.test(a.type) ? null != a.charCode ? a.charCode : a.keyCode : !a.which && void 0 !== b && ta.test(a.type) ? 1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0 : a.which
        }
    }, r.event.addProp), r.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (a, b) {
        r.event.special[a] = {
            delegateType: b, bindType: b, handle: function (a) {
                var c, d = this, e = a.relatedTarget, f = a.handleObj;
                return e && (e === d || r.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
            }
        }
    }), r.fn.extend({
        on: function (a, b, c, d) {
            return ya(this, a, b, c, d)
        }, one: function (a, b, c, d) {
            return ya(this, a, b, c, d, 1)
        }, off: function (a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) return d = a.handleObj, r(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
            if ("object" == typeof a) {
                for (e in a) this.off(e, b, a[e]);
                return this
            }
            return b !== !1 && "function" != typeof b || (c = b, b = void 0), c === !1 && (c = wa), this.each(function () {
                r.event.remove(this, a, c, b)
            })
        }
    });
    var za = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        Aa = /<script|<style|<link/i, Ba = /checked\s*(?:[^=]|=\s*.checked.)/i, Ca = /^true\/(.*)/,
        Da = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

    function Ea(a, b) {
        return B(a, "table") && B(11 !== b.nodeType ? b : b.firstChild, "tr") ? r(">tbody", a)[0] || a : a
    }

    function Fa(a) {
        return a.type = (null !== a.getAttribute("type")) + "/" + a.type, a
    }

    function Ga(a) {
        var b = Ca.exec(a.type);
        return b ? a.type = b[1] : a.removeAttribute("type"), a
    }

    function Ha(a, b) {
        var c, d, e, f, g, h, i, j;
        if (1 === b.nodeType) {
            if (W.hasData(a) && (f = W.access(a), g = W.set(b, f), j = f.events)) {
                delete g.handle, g.events = {};
                for (e in j) for (c = 0, d = j[e].length; c < d; c++) r.event.add(b, e, j[e][c])
            }
            X.hasData(a) && (h = X.access(a), i = r.extend({}, h), X.set(b, i))
        }
    }

    function Ia(a, b) {
        var c = b.nodeName.toLowerCase();
        "input" === c && ja.test(a.type) ? b.checked = a.checked : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue)
    }

    function Ja(a, b, c, d) {
        b = g.apply([], b);
        var e, f, h, i, j, k, l = 0, m = a.length, n = m - 1, q = b[0], s = r.isFunction(q);
        if (s || m > 1 && "string" == typeof q && !o.checkClone && Ba.test(q)) return a.each(function (e) {
            var f = a.eq(e);
            s && (b[0] = q.call(this, e, f.html())), Ja(f, b, c, d)
        });
        if (m && (e = qa(b, a[0].ownerDocument, !1, a, d), f = e.firstChild, 1 === e.childNodes.length && (e = f), f || d)) {
            for (h = r.map(na(e, "script"), Fa), i = h.length; l < m; l++) j = e, l !== n && (j = r.clone(j, !0, !0), i && r.merge(h, na(j, "script"))), c.call(a[l], j, l);
            if (i) for (k = h[h.length - 1].ownerDocument, r.map(h, Ga), l = 0; l < i; l++) j = h[l], la.test(j.type || "") && !W.access(j, "globalEval") && r.contains(k, j) && (j.src ? r._evalUrl && r._evalUrl(j.src) : p(j.textContent.replace(Da, ""), k))
        }
        return a
    }

    function Ka(a, b, c) {
        for (var d, e = b ? r.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || r.cleanData(na(d)), d.parentNode && (c && r.contains(d.ownerDocument, d) && oa(na(d, "script")), d.parentNode.removeChild(d));
        return a
    }

    r.extend({
        htmlPrefilter: function (a) {
            return a.replace(za, "<$1></$2>")
        }, clone: function (a, b, c) {
            var d, e, f, g, h = a.cloneNode(!0), i = r.contains(a.ownerDocument, a);
            if (!(o.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || r.isXMLDoc(a))) for (g = na(h), f = na(a), d = 0, e = f.length; d < e; d++) Ia(f[d], g[d]);
            if (b) if (c) for (f = f || na(a), g = g || na(h), d = 0, e = f.length; d < e; d++) Ha(f[d], g[d]); else Ha(a, h);
            return g = na(h, "script"), g.length > 0 && oa(g, !i && na(a, "script")), h
        }, cleanData: function (a) {
            for (var b, c, d, e = r.event.special, f = 0; void 0 !== (c = a[f]); f++) if (U(c)) {
                if (b = c[W.expando]) {
                    if (b.events) for (d in b.events) e[d] ? r.event.remove(c, d) : r.removeEvent(c, d, b.handle);
                    c[W.expando] = void 0
                }
                c[X.expando] && (c[X.expando] = void 0)
            }
        }
    }), r.fn.extend({
        detach: function (a) {
            return Ka(this, a, !0)
        }, remove: function (a) {
            return Ka(this, a)
        }, text: function (a) {
            return T(this, function (a) {
                return void 0 === a ? r.text(this) : this.empty().each(function () {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = a)
                })
            }, null, a, arguments.length)
        }, append: function () {
            return Ja(this, arguments, function (a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Ea(this, a);
                    b.appendChild(a)
                }
            })
        }, prepend: function () {
            return Ja(this, arguments, function (a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = Ea(this, a);
                    b.insertBefore(a, b.firstChild)
                }
            })
        }, before: function () {
            return Ja(this, arguments, function (a) {
                this.parentNode && this.parentNode.insertBefore(a, this)
            })
        }, after: function () {
            return Ja(this, arguments, function (a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
            })
        }, empty: function () {
            for (var a, b = 0; null != (a = this[b]); b++) 1 === a.nodeType && (r.cleanData(na(a, !1)), a.textContent = "");
            return this
        }, clone: function (a, b) {
            return a = null != a && a, b = null == b ? a : b, this.map(function () {
                return r.clone(this, a, b)
            })
        }, html: function (a) {
            return T(this, function (a) {
                var b = this[0] || {}, c = 0, d = this.length;
                if (void 0 === a && 1 === b.nodeType) return b.innerHTML;
                if ("string" == typeof a && !Aa.test(a) && !ma[(ka.exec(a) || ["", ""])[1].toLowerCase()]) {
                    a = r.htmlPrefilter(a);
                    try {
                        for (; c < d; c++) b = this[c] || {}, 1 === b.nodeType && (r.cleanData(na(b, !1)), b.innerHTML = a);
                        b = 0
                    } catch (e) {
                    }
                }
                b && this.empty().append(a)
            }, null, a, arguments.length)
        }, replaceWith: function () {
            var a = [];
            return Ja(this, arguments, function (b) {
                var c = this.parentNode;
                r.inArray(this, a) < 0 && (r.cleanData(na(this)), c && c.replaceChild(b, this))
            }, a)
        }
    }), r.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (a, b) {
        r.fn[a] = function (a) {
            for (var c, d = [], e = r(a), f = e.length - 1, g = 0; g <= f; g++) c = g === f ? this : this.clone(!0), r(e[g])[b](c), h.apply(d, c.get());
            return this.pushStack(d)
        }
    });
    var La = /^margin/, Ma = new RegExp("^(" + aa + ")(?!px)[a-z%]+$", "i"), Na = function (b) {
        var c = b.ownerDocument.defaultView;
        return c && c.opener || (c = a), c.getComputedStyle(b)
    };
    !function () {
        function b() {
            if (i) {
                i.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", i.innerHTML = "", ra.appendChild(h);
                var b = a.getComputedStyle(i);
                c = "1%" !== b.top, g = "2px" === b.marginLeft, e = "4px" === b.width, i.style.marginRight = "50%", f = "4px" === b.marginRight, ra.removeChild(h), i = null
            }
        }

        var c, e, f, g, h = d.createElement("div"), i = d.createElement("div");
        i.style && (i.style.backgroundClip = "content-box", i.cloneNode(!0).style.backgroundClip = "", o.clearCloneStyle = "content-box" === i.style.backgroundClip, h.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", h.appendChild(i), r.extend(o, {
            pixelPosition: function () {
                return b(), c
            }, boxSizingReliable: function () {
                return b(), e
            }, pixelMarginRight: function () {
                return b(), f
            }, reliableMarginLeft: function () {
                return b(), g
            }
        }))
    }();

    function Oa(a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || Na(a), c && (g = c.getPropertyValue(b) || c[b], "" !== g || r.contains(a.ownerDocument, a) || (g = r.style(a, b)), !o.pixelMarginRight() && Ma.test(g) && La.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + "" : g
    }

    function Pa(a, b) {
        return {
            get: function () {
                return a() ? void delete this.get : (this.get = b).apply(this, arguments)
            }
        }
    }

    var Qa = /^(none|table(?!-c[ea]).+)/, Ra = /^--/,
        Sa = {position: "absolute", visibility: "hidden", display: "block"},
        Ta = {letterSpacing: "0", fontWeight: "400"}, Ua = ["Webkit", "Moz", "ms"], Va = d.createElement("div").style;

    function Wa(a) {
        if (a in Va) return a;
        var b = a[0].toUpperCase() + a.slice(1), c = Ua.length;
        while (c--) if (a = Ua[c] + b, a in Va) return a
    }

    function Xa(a) {
        var b = r.cssProps[a];
        return b || (b = r.cssProps[a] = Wa(a) || a), b
    }

    function Ya(a, b, c) {
        var d = ba.exec(b);
        return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b
    }

    function Za(a, b, c, d, e) {
        var f, g = 0;
        for (f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0; f < 4; f += 2) "margin" === c && (g += r.css(a, c + ca[f], !0, e)), d ? ("content" === c && (g -= r.css(a, "padding" + ca[f], !0, e)), "margin" !== c && (g -= r.css(a, "border" + ca[f] + "Width", !0, e))) : (g += r.css(a, "padding" + ca[f], !0, e), "padding" !== c && (g += r.css(a, "border" + ca[f] + "Width", !0, e)));
        return g
    }

    function $a(a, b, c) {
        var d, e = Na(a), f = Oa(a, b, e), g = "border-box" === r.css(a, "boxSizing", !1, e);
        return Ma.test(f) ? f : (d = g && (o.boxSizingReliable() || f === a.style[b]), "auto" === f && (f = a["offset" + b[0].toUpperCase() + b.slice(1)]), f = parseFloat(f) || 0, f + Za(a, b, c || (g ? "border" : "content"), d, e) + "px")
    }

    r.extend({
        cssHooks: {
            opacity: {
                get: function (a, b) {
                    if (b) {
                        var c = Oa(a, "opacity");
                        return "" === c ? "1" : c
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {float: "cssFloat"},
        style: function (a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e, f, g, h = r.camelCase(b), i = Ra.test(b), j = a.style;
                return i || (b = Xa(h)), g = r.cssHooks[b] || r.cssHooks[h], void 0 === c ? g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : j[b] : (f = typeof c, "string" === f && (e = ba.exec(c)) && e[1] && (c = fa(a, b, e), f = "number"), null != c && c === c && ("number" === f && (c += e && e[3] || (r.cssNumber[h] ? "" : "px")), o.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (j[b] = "inherit"), g && "set" in g && void 0 === (c = g.set(a, c, d)) || (i ? j.setProperty(b, c) : j[b] = c)), void 0)
            }
        },
        css: function (a, b, c, d) {
            var e, f, g, h = r.camelCase(b), i = Ra.test(b);
            return i || (b = Xa(h)), g = r.cssHooks[b] || r.cssHooks[h], g && "get" in g && (e = g.get(a, !0, c)), void 0 === e && (e = Oa(a, b, d)), "normal" === e && b in Ta && (e = Ta[b]), "" === c || c ? (f = parseFloat(e), c === !0 || isFinite(f) ? f || 0 : e) : e
        }
    }), r.each(["height", "width"], function (a, b) {
        r.cssHooks[b] = {
            get: function (a, c, d) {
                if (c) return !Qa.test(r.css(a, "display")) || a.getClientRects().length && a.getBoundingClientRect().width ? $a(a, b, d) : ea(a, Sa, function () {
                    return $a(a, b, d)
                })
            }, set: function (a, c, d) {
                var e, f = d && Na(a), g = d && Za(a, b, d, "border-box" === r.css(a, "boxSizing", !1, f), f);
                return g && (e = ba.exec(c)) && "px" !== (e[3] || "px") && (a.style[b] = c, c = r.css(a, b)), Ya(a, c, g)
            }
        }
    }), r.cssHooks.marginLeft = Pa(o.reliableMarginLeft, function (a, b) {
        if (b) return (parseFloat(Oa(a, "marginLeft")) || a.getBoundingClientRect().left - ea(a, {marginLeft: 0}, function () {
            return a.getBoundingClientRect().left
        })) + "px"
    }), r.each({margin: "", padding: "", border: "Width"}, function (a, b) {
        r.cssHooks[a + b] = {
            expand: function (c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; d < 4; d++) e[a + ca[d] + b] = f[d] || f[d - 2] || f[0];
                return e
            }
        }, La.test(a) || (r.cssHooks[a + b].set = Ya)
    }), r.fn.extend({
        css: function (a, b) {
            return T(this, function (a, b, c) {
                var d, e, f = {}, g = 0;
                if (Array.isArray(b)) {
                    for (d = Na(a), e = b.length; g < e; g++) f[b[g]] = r.css(a, b[g], !1, d);
                    return f
                }
                return void 0 !== c ? r.style(a, b, c) : r.css(a, b)
            }, a, b, arguments.length > 1)
        }
    });

    function _a(a, b, c, d, e) {
        return new _a.prototype.init(a, b, c, d, e)
    }

    r.Tween = _a, _a.prototype = {
        constructor: _a, init: function (a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || r.easing._default, this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (r.cssNumber[c] ? "" : "px")
        }, cur: function () {
            var a = _a.propHooks[this.prop];
            return a && a.get ? a.get(this) : _a.propHooks._default.get(this)
        }, run: function (a) {
            var b, c = _a.propHooks[this.prop];
            return this.options.duration ? this.pos = b = r.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : _a.propHooks._default.set(this), this
        }
    }, _a.prototype.init.prototype = _a.prototype, _a.propHooks = {
        _default: {
            get: function (a) {
                var b;
                return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = r.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0)
            }, set: function (a) {
                r.fx.step[a.prop] ? r.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[r.cssProps[a.prop]] && !r.cssHooks[a.prop] ? a.elem[a.prop] = a.now : r.style(a.elem, a.prop, a.now + a.unit)
            }
        }
    }, _a.propHooks.scrollTop = _a.propHooks.scrollLeft = {
        set: function (a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
        }
    }, r.easing = {
        linear: function (a) {
            return a
        }, swing: function (a) {
            return .5 - Math.cos(a * Math.PI) / 2
        }, _default: "swing"
    }, r.fx = _a.prototype.init, r.fx.step = {};
    var ab, bb, cb = /^(?:toggle|show|hide)$/, db = /queueHooks$/;

    function eb() {
        bb && (d.hidden === !1 && a.requestAnimationFrame ? a.requestAnimationFrame(eb) : a.setTimeout(eb, r.fx.interval), r.fx.tick())
    }

    function fb() {
        return a.setTimeout(function () {
            ab = void 0
        }), ab = r.now()
    }

    function gb(a, b) {
        var c, d = 0, e = {height: a};
        for (b = b ? 1 : 0; d < 4; d += 2 - b) c = ca[d], e["margin" + c] = e["padding" + c] = a;
        return b && (e.opacity = e.width = a), e
    }

    function hb(a, b, c) {
        for (var d, e = (kb.tweeners[b] || []).concat(kb.tweeners["*"]), f = 0, g = e.length; f < g; f++) if (d = e[f].call(c, b, a)) return d
    }

    function ib(a, b, c) {
        var d, e, f, g, h, i, j, k, l = "width" in b || "height" in b, m = this, n = {}, o = a.style,
            p = a.nodeType && da(a), q = W.get(a, "fxshow");
        c.queue || (g = r._queueHooks(a, "fx"), null == g.unqueued && (g.unqueued = 0, h = g.empty.fire, g.empty.fire = function () {
            g.unqueued || h()
        }), g.unqueued++, m.always(function () {
            m.always(function () {
                g.unqueued--, r.queue(a, "fx").length || g.empty.fire()
            })
        }));
        for (d in b) if (e = b[d], cb.test(e)) {
            if (delete b[d], f = f || "toggle" === e, e === (p ? "hide" : "show")) {
                if ("show" !== e || !q || void 0 === q[d]) continue;
                p = !0
            }
            n[d] = q && q[d] || r.style(a, d)
        }
        if (i = !r.isEmptyObject(b), i || !r.isEmptyObject(n)) {
            l && 1 === a.nodeType && (c.overflow = [o.overflow, o.overflowX, o.overflowY], j = q && q.display, null == j && (j = W.get(a, "display")), k = r.css(a, "display"), "none" === k && (j ? k = j : (ia([a], !0), j = a.style.display || j, k = r.css(a, "display"), ia([a]))), ("inline" === k || "inline-block" === k && null != j) && "none" === r.css(a, "float") && (i || (m.done(function () {
                o.display = j
            }), null == j && (k = o.display, j = "none" === k ? "" : k)), o.display = "inline-block")), c.overflow && (o.overflow = "hidden", m.always(function () {
                o.overflow = c.overflow[0], o.overflowX = c.overflow[1], o.overflowY = c.overflow[2]
            })), i = !1;
            for (d in n) i || (q ? "hidden" in q && (p = q.hidden) : q = W.access(a, "fxshow", {display: j}), f && (q.hidden = !p), p && ia([a], !0), m.done(function () {
                p || ia([a]), W.remove(a, "fxshow");
                for (d in n) r.style(a, d, n[d])
            })), i = hb(p ? q[d] : 0, d, m), d in q || (q[d] = i.start, p && (i.end = i.start, i.start = 0))
        }
    }

    function jb(a, b) {
        var c, d, e, f, g;
        for (c in a) if (d = r.camelCase(c), e = b[d], f = a[c], Array.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = r.cssHooks[d], g && "expand" in g) {
            f = g.expand(f), delete a[d];
            for (c in f) c in a || (a[c] = f[c], b[c] = e)
        } else b[d] = e
    }

    function kb(a, b, c) {
        var d, e, f = 0, g = kb.prefilters.length, h = r.Deferred().always(function () {
            delete i.elem
        }), i = function () {
            if (e) return !1;
            for (var b = ab || fb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; g < i; g++) j.tweens[g].run(f);
            return h.notifyWith(a, [j, f, c]), f < 1 && i ? c : (i || h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j]), !1)
        }, j = h.promise({
            elem: a,
            props: r.extend({}, b),
            opts: r.extend(!0, {specialEasing: {}, easing: r.easing._default}, c),
            originalProperties: b,
            originalOptions: c,
            startTime: ab || fb(),
            duration: c.duration,
            tweens: [],
            createTween: function (b, c) {
                var d = r.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                return j.tweens.push(d), d
            },
            stop: function (b) {
                var c = 0, d = b ? j.tweens.length : 0;
                if (e) return this;
                for (e = !0; c < d; c++) j.tweens[c].run(1);
                return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this
            }
        }), k = j.props;
        for (jb(k, j.opts.specialEasing); f < g; f++) if (d = kb.prefilters[f].call(j, a, k, j.opts)) return r.isFunction(d.stop) && (r._queueHooks(j.elem, j.opts.queue).stop = r.proxy(d.stop, d)), d;
        return r.map(k, hb, j), r.isFunction(j.opts.start) && j.opts.start.call(a, j), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always), r.fx.timer(r.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j
    }

    r.Animation = r.extend(kb, {
        tweeners: {
            "*": [function (a, b) {
                var c = this.createTween(a, b);
                return fa(c.elem, a, ba.exec(b), c), c
            }]
        }, tweener: function (a, b) {
            r.isFunction(a) ? (b = a, a = ["*"]) : a = a.match(L);
            for (var c, d = 0, e = a.length; d < e; d++) c = a[d], kb.tweeners[c] = kb.tweeners[c] || [], kb.tweeners[c].unshift(b)
        }, prefilters: [ib], prefilter: function (a, b) {
            b ? kb.prefilters.unshift(a) : kb.prefilters.push(a)
        }
    }), r.speed = function (a, b, c) {
        var d = a && "object" == typeof a ? r.extend({}, a) : {
            complete: c || !c && b || r.isFunction(a) && a,
            duration: a,
            easing: c && b || b && !r.isFunction(b) && b
        };
        return r.fx.off ? d.duration = 0 : "number" != typeof d.duration && (d.duration in r.fx.speeds ? d.duration = r.fx.speeds[d.duration] : d.duration = r.fx.speeds._default), null != d.queue && d.queue !== !0 || (d.queue = "fx"), d.old = d.complete, d.complete = function () {
            r.isFunction(d.old) && d.old.call(this), d.queue && r.dequeue(this, d.queue)
        }, d
    }, r.fn.extend({
        fadeTo: function (a, b, c, d) {
            return this.filter(da).css("opacity", 0).show().end().animate({opacity: b}, a, c, d)
        }, animate: function (a, b, c, d) {
            var e = r.isEmptyObject(a), f = r.speed(b, c, d), g = function () {
                var b = kb(this, r.extend({}, a), f);
                (e || W.get(this, "finish")) && b.stop(!0)
            };
            return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
        }, stop: function (a, b, c) {
            var d = function (a) {
                var b = a.stop;
                delete a.stop, b(c)
            };
            return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function () {
                var b = !0, e = null != a && a + "queueHooks", f = r.timers, g = W.get(this);
                if (e) g[e] && g[e].stop && d(g[e]); else for (e in g) g[e] && g[e].stop && db.test(e) && d(g[e]);
                for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
                !b && c || r.dequeue(this, a)
            })
        }, finish: function (a) {
            return a !== !1 && (a = a || "fx"), this.each(function () {
                var b, c = W.get(this), d = c[a + "queue"], e = c[a + "queueHooks"], f = r.timers, g = d ? d.length : 0;
                for (c.finish = !0, r.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
                for (b = 0; b < g; b++) d[b] && d[b].finish && d[b].finish.call(this);
                delete c.finish
            })
        }
    }), r.each(["toggle", "show", "hide"], function (a, b) {
        var c = r.fn[b];
        r.fn[b] = function (a, d, e) {
            return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(gb(b, !0), a, d, e)
        }
    }), r.each({
        slideDown: gb("show"),
        slideUp: gb("hide"),
        slideToggle: gb("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (a, b) {
        r.fn[a] = function (a, c, d) {
            return this.animate(b, a, c, d)
        }
    }), r.timers = [], r.fx.tick = function () {
        var a, b = 0, c = r.timers;
        for (ab = r.now(); b < c.length; b++) a = c[b], a() || c[b] !== a || c.splice(b--, 1);
        c.length || r.fx.stop(), ab = void 0
    }, r.fx.timer = function (a) {
        r.timers.push(a), r.fx.start()
    }, r.fx.interval = 13, r.fx.start = function () {
        bb || (bb = !0, eb())
    }, r.fx.stop = function () {
        bb = null
    }, r.fx.speeds = {slow: 600, fast: 200, _default: 400}, r.fn.delay = function (b, c) {
        return b = r.fx ? r.fx.speeds[b] || b : b, c = c || "fx", this.queue(c, function (c, d) {
            var e = a.setTimeout(c, b);
            d.stop = function () {
                a.clearTimeout(e)
            }
        })
    }, function () {
        var a = d.createElement("input"), b = d.createElement("select"), c = b.appendChild(d.createElement("option"));
        a.type = "checkbox", o.checkOn = "" !== a.value, o.optSelected = c.selected, a = d.createElement("input"), a.value = "t", a.type = "radio", o.radioValue = "t" === a.value
    }();
    var lb, mb = r.expr.attrHandle;
    r.fn.extend({
        attr: function (a, b) {
            return T(this, r.attr, a, b, arguments.length > 1)
        }, removeAttr: function (a) {
            return this.each(function () {
                r.removeAttr(this, a)
            })
        }
    }), r.extend({
        attr: function (a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return "undefined" == typeof a.getAttribute ? r.prop(a, b, c) : (1 === f && r.isXMLDoc(a) || (e = r.attrHooks[b.toLowerCase()] || (r.expr.match.bool.test(b) ? lb : void 0)), void 0 !== c ? null === c ? void r.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : (d = r.find.attr(a, b), null == d ? void 0 : d))
        }, attrHooks: {
            type: {
                set: function (a, b) {
                    if (!o.radioValue && "radio" === b && B(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b
                    }
                }
            }
        }, removeAttr: function (a, b) {
            var c, d = 0, e = b && b.match(L);
            if (e && 1 === a.nodeType) while (c = e[d++]) a.removeAttribute(c)
        }
    }), lb = {
        set: function (a, b, c) {
            return b === !1 ? r.removeAttr(a, c) : a.setAttribute(c, c), c
        }
    }, r.each(r.expr.match.bool.source.match(/\w+/g), function (a, b) {
        var c = mb[b] || r.find.attr;
        mb[b] = function (a, b, d) {
            var e, f, g = b.toLowerCase();
            return d || (f = mb[g], mb[g] = e, e = null != c(a, b, d) ? g : null, mb[g] = f), e
        }
    });
    var nb = /^(?:input|select|textarea|button)$/i, ob = /^(?:a|area)$/i;
    r.fn.extend({
        prop: function (a, b) {
            return T(this, r.prop, a, b, arguments.length > 1)
        }, removeProp: function (a) {
            return this.each(function () {
                delete this[r.propFix[a] || a]
            })
        }
    }), r.extend({
        prop: function (a, b, c) {
            var d, e, f = a.nodeType;
            if (3 !== f && 8 !== f && 2 !== f) return 1 === f && r.isXMLDoc(a) || (b = r.propFix[b] || b, e = r.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]
        }, propHooks: {
            tabIndex: {
                get: function (a) {
                    var b = r.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : nb.test(a.nodeName) || ob.test(a.nodeName) && a.href ? 0 : -1
                }
            }
        }, propFix: {for: "htmlFor", class: "className"}
    }), o.optSelected || (r.propHooks.selected = {
        get: function (a) {
            var b = a.parentNode;
            return b && b.parentNode && b.parentNode.selectedIndex, null
        }, set: function (a) {
            var b = a.parentNode;
            b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex)
        }
    }), r.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        r.propFix[this.toLowerCase()] = this
    });

    function pb(a) {
        var b = a.match(L) || [];
        return b.join(" ")
    }

    function qb(a) {
        return a.getAttribute && a.getAttribute("class") || ""
    }

    r.fn.extend({
        addClass: function (a) {
            var b, c, d, e, f, g, h, i = 0;
            if (r.isFunction(a)) return this.each(function (b) {
                r(this).addClass(a.call(this, b, qb(this)))
            });
            if ("string" == typeof a && a) {
                b = a.match(L) || [];
                while (c = this[i++]) if (e = qb(c), d = 1 === c.nodeType && " " + pb(e) + " ") {
                    g = 0;
                    while (f = b[g++]) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
                    h = pb(d), e !== h && c.setAttribute("class", h)
                }
            }
            return this
        }, removeClass: function (a) {
            var b, c, d, e, f, g, h, i = 0;
            if (r.isFunction(a)) return this.each(function (b) {
                r(this).removeClass(a.call(this, b, qb(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof a && a) {
                b = a.match(L) || [];
                while (c = this[i++]) if (e = qb(c), d = 1 === c.nodeType && " " + pb(e) + " ") {
                    g = 0;
                    while (f = b[g++]) while (d.indexOf(" " + f + " ") > -1) d = d.replace(" " + f + " ", " ");
                    h = pb(d), e !== h && c.setAttribute("class", h)
                }
            }
            return this
        }, toggleClass: function (a, b) {
            var c = typeof a;
            return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : r.isFunction(a) ? this.each(function (c) {
                r(this).toggleClass(a.call(this, c, qb(this), b), b)
            }) : this.each(function () {
                var b, d, e, f;
                if ("string" === c) {
                    d = 0, e = r(this), f = a.match(L) || [];
                    while (b = f[d++]) e.hasClass(b) ? e.removeClass(b) : e.addClass(b)
                } else void 0 !== a && "boolean" !== c || (b = qb(this), b && W.set(this, "__className__", b), this.setAttribute && this.setAttribute("class", b || a === !1 ? "" : W.get(this, "__className__") || ""))
            })
        }, hasClass: function (a) {
            var b, c, d = 0;
            b = " " + a + " ";
            while (c = this[d++]) if (1 === c.nodeType && (" " + pb(qb(c)) + " ").indexOf(b) > -1) return !0;
            return !1
        }
    });
    var rb = /\r/g;
    r.fn.extend({
        val: function (a) {
            var b, c, d, e = this[0];
            {
                if (arguments.length) return d = r.isFunction(a), this.each(function (c) {
                    var e;
                    1 === this.nodeType && (e = d ? a.call(this, c, r(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : Array.isArray(e) && (e = r.map(e, function (a) {
                        return null == a ? "" : a + ""
                    })), b = r.valHooks[this.type] || r.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e))
                });
                if (e) return b = r.valHooks[e.type] || r.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(rb, "") : null == c ? "" : c)
            }
        }
    }), r.extend({
        valHooks: {
            option: {
                get: function (a) {
                    var b = r.find.attr(a, "value");
                    return null != b ? b : pb(r.text(a))
                }
            }, select: {
                get: function (a) {
                    var b, c, d, e = a.options, f = a.selectedIndex, g = "select-one" === a.type, h = g ? null : [],
                        i = g ? f + 1 : e.length;
                    for (d = f < 0 ? i : g ? f : 0; d < i; d++) if (c = e[d], (c.selected || d === f) && !c.disabled && (!c.parentNode.disabled || !B(c.parentNode, "optgroup"))) {
                        if (b = r(c).val(), g) return b;
                        h.push(b)
                    }
                    return h
                }, set: function (a, b) {
                    var c, d, e = a.options, f = r.makeArray(b), g = e.length;
                    while (g--) d = e[g], (d.selected = r.inArray(r.valHooks.option.get(d), f) > -1) && (c = !0);
                    return c || (a.selectedIndex = -1), f
                }
            }
        }
    }), r.each(["radio", "checkbox"], function () {
        r.valHooks[this] = {
            set: function (a, b) {
                if (Array.isArray(b)) return a.checked = r.inArray(r(a).val(), b) > -1
            }
        }, o.checkOn || (r.valHooks[this].get = function (a) {
            return null === a.getAttribute("value") ? "on" : a.value
        })
    });
    var sb = /^(?:focusinfocus|focusoutblur)$/;
    r.extend(r.event, {
        trigger: function (b, c, e, f) {
            var g, h, i, j, k, m, n, o = [e || d], p = l.call(b, "type") ? b.type : b,
                q = l.call(b, "namespace") ? b.namespace.split(".") : [];
            if (h = i = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !sb.test(p + r.event.triggered) && (p.indexOf(".") > -1 && (q = p.split("."), p = q.shift(), q.sort()), k = p.indexOf(":") < 0 && "on" + p, b = b[r.expando] ? b : new r.Event(p, "object" == typeof b && b), b.isTrigger = f ? 2 : 3, b.namespace = q.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : r.makeArray(c, [b]), n = r.event.special[p] || {}, f || !n.trigger || n.trigger.apply(e, c) !== !1)) {
                if (!f && !n.noBubble && !r.isWindow(e)) {
                    for (j = n.delegateType || p, sb.test(j + p) || (h = h.parentNode); h; h = h.parentNode) o.push(h), i = h;
                    i === (e.ownerDocument || d) && o.push(i.defaultView || i.parentWindow || a)
                }
                g = 0;
                while ((h = o[g++]) && !b.isPropagationStopped()) b.type = g > 1 ? j : n.bindType || p, m = (W.get(h, "events") || {})[b.type] && W.get(h, "handle"), m && m.apply(h, c), m = k && h[k], m && m.apply && U(h) && (b.result = m.apply(h, c), b.result === !1 && b.preventDefault());
                return b.type = p, f || b.isDefaultPrevented() || n._default && n._default.apply(o.pop(), c) !== !1 || !U(e) || k && r.isFunction(e[p]) && !r.isWindow(e) && (i = e[k], i && (e[k] = null), r.event.triggered = p, e[p](), r.event.triggered = void 0, i && (e[k] = i)), b.result
            }
        }, simulate: function (a, b, c) {
            var d = r.extend(new r.Event, c, {type: a, isSimulated: !0});
            r.event.trigger(d, null, b)
        }
    }), r.fn.extend({
        trigger: function (a, b) {
            return this.each(function () {
                r.event.trigger(a, b, this)
            })
        }, triggerHandler: function (a, b) {
            var c = this[0];
            if (c) return r.event.trigger(a, b, c, !0)
        }
    }), r.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (a, b) {
        r.fn[b] = function (a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
        }
    }), r.fn.extend({
        hover: function (a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        }
    }), o.focusin = "onfocusin" in a, o.focusin || r.each({focus: "focusin", blur: "focusout"}, function (a, b) {
        var c = function (a) {
            r.event.simulate(b, a.target, r.event.fix(a))
        };
        r.event.special[b] = {
            setup: function () {
                var d = this.ownerDocument || this, e = W.access(d, b);
                e || d.addEventListener(a, c, !0), W.access(d, b, (e || 0) + 1)
            }, teardown: function () {
                var d = this.ownerDocument || this, e = W.access(d, b) - 1;
                e ? W.access(d, b, e) : (d.removeEventListener(a, c, !0), W.remove(d, b))
            }
        }
    });
    var tb = a.location, ub = r.now(), vb = /\?/;
    r.parseXML = function (b) {
        var c;
        if (!b || "string" != typeof b) return null;
        try {
            c = (new a.DOMParser).parseFromString(b, "text/xml")
        } catch (d) {
            c = void 0
        }
        return c && !c.getElementsByTagName("parsererror").length || r.error("Invalid XML: " + b), c
    };
    var wb = /\[\]$/, xb = /\r?\n/g, yb = /^(?:submit|button|image|reset|file)$/i,
        zb = /^(?:input|select|textarea|keygen)/i;

    function Ab(a, b, c, d) {
        var e;
        if (Array.isArray(b)) r.each(b, function (b, e) {
            c || wb.test(a) ? d(a, e) : Ab(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d)
        }); else if (c || "object" !== r.type(b)) d(a, b); else for (e in b) Ab(a + "[" + e + "]", b[e], c, d)
    }

    r.param = function (a, b) {
        var c, d = [], e = function (a, b) {
            var c = r.isFunction(b) ? b() : b;
            d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c)
        };
        if (Array.isArray(a) || a.jquery && !r.isPlainObject(a)) r.each(a, function () {
            e(this.name, this.value)
        }); else for (c in a) Ab(c, a[c], b, e);
        return d.join("&")
    }, r.fn.extend({
        serialize: function () {
            return r.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var a = r.prop(this, "elements");
                return a ? r.makeArray(a) : this
            }).filter(function () {
                var a = this.type;
                return this.name && !r(this).is(":disabled") && zb.test(this.nodeName) && !yb.test(a) && (this.checked || !ja.test(a))
            }).map(function (a, b) {
                var c = r(this).val();
                return null == c ? null : Array.isArray(c) ? r.map(c, function (a) {
                    return {name: b.name, value: a.replace(xb, "\r\n")}
                }) : {name: b.name, value: c.replace(xb, "\r\n")}
            }).get()
        }
    });
    var Bb = /%20/g, Cb = /#.*$/, Db = /([?&])_=[^&]*/, Eb = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Fb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, Gb = /^(?:GET|HEAD)$/, Hb = /^\/\//, Ib = {},
        Jb = {}, Kb = "*/".concat("*"), Lb = d.createElement("a");
    Lb.href = tb.href;

    function Mb(a) {
        return function (b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0, f = b.toLowerCase().match(L) || [];
            if (r.isFunction(c)) while (d = f[e++]) "+" === d[0] ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
        }
    }

    function Nb(a, b, c, d) {
        var e = {}, f = a === Jb;

        function g(h) {
            var i;
            return e[h] = !0, r.each(a[h] || [], function (a, h) {
                var j = h(b, c, d);
                return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1)
            }), i
        }

        return g(b.dataTypes[0]) || !e["*"] && g("*")
    }

    function Ob(a, b) {
        var c, d, e = r.ajaxSettings.flatOptions || {};
        for (c in b) void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);
        return d && r.extend(!0, a, d), a
    }

    function Pb(a, b, c) {
        var d, e, f, g, h = a.contents, i = a.dataTypes;
        while ("*" === i[0]) i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
        if (d) for (e in h) if (h[e] && h[e].test(d)) {
            i.unshift(e);
            break
        }
        if (i[0] in c) f = i[0]; else {
            for (e in c) {
                if (!i[0] || a.converters[e + " " + i[0]]) {
                    f = e;
                    break
                }
                g || (g = e)
            }
            f = f || g
        }
        if (f) return f !== i[0] && i.unshift(f), c[f]
    }

    function Qb(a, b, c, d) {
        var e, f, g, h, i, j = {}, k = a.dataTypes.slice();
        if (k[1]) for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
        f = k.shift();
        while (f) if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift()) if ("*" === f) f = i; else if ("*" !== i && i !== f) {
            if (g = j[i + " " + f] || j["* " + f], !g) for (e in j) if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
                break
            }
            if (g !== !0) if (g && a["throws"]) b = g(b); else try {
                b = g(b)
            } catch (l) {
                return {state: "parsererror", error: g ? l : "No conversion from " + i + " to " + f}
            }
        }
        return {state: "success", data: b}
    }

    r.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: tb.href,
            type: "GET",
            isLocal: Fb.test(tb.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Kb,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": JSON.parse, "text xml": r.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (a, b) {
            return b ? Ob(Ob(a, r.ajaxSettings), b) : Ob(r.ajaxSettings, a)
        },
        ajaxPrefilter: Mb(Ib),
        ajaxTransport: Mb(Jb),
        ajax: function (b, c) {
            "object" == typeof b && (c = b, b = void 0), c = c || {};
            var e, f, g, h, i, j, k, l, m, n, o = r.ajaxSetup({}, c), p = o.context || o,
                q = o.context && (p.nodeType || p.jquery) ? r(p) : r.event, s = r.Deferred(),
                t = r.Callbacks("once memory"), u = o.statusCode || {}, v = {}, w = {}, x = "canceled", y = {
                    readyState: 0, getResponseHeader: function (a) {
                        var b;
                        if (k) {
                            if (!h) {
                                h = {};
                                while (b = Eb.exec(g)) h[b[1].toLowerCase()] = b[2]
                            }
                            b = h[a.toLowerCase()]
                        }
                        return null == b ? null : b
                    }, getAllResponseHeaders: function () {
                        return k ? g : null
                    }, setRequestHeader: function (a, b) {
                        return null == k && (a = w[a.toLowerCase()] = w[a.toLowerCase()] || a, v[a] = b), this
                    }, overrideMimeType: function (a) {
                        return null == k && (o.mimeType = a), this
                    }, statusCode: function (a) {
                        var b;
                        if (a) if (k) y.always(a[y.status]); else for (b in a) u[b] = [u[b], a[b]];
                        return this
                    }, abort: function (a) {
                        var b = a || x;
                        return e && e.abort(b), A(0, b), this
                    }
                };
            if (s.promise(y), o.url = ((b || o.url || tb.href) + "").replace(Hb, tb.protocol + "//"), o.type = c.method || c.type || o.method || o.type, o.dataTypes = (o.dataType || "*").toLowerCase().match(L) || [""], null == o.crossDomain) {
                j = d.createElement("a");
                try {
                    j.href = o.url, j.href = j.href, o.crossDomain = Lb.protocol + "//" + Lb.host != j.protocol + "//" + j.host
                } catch (z) {
                    o.crossDomain = !0
                }
            }
            if (o.data && o.processData && "string" != typeof o.data && (o.data = r.param(o.data, o.traditional)), Nb(Ib, o, c, y), k) return y;
            l = r.event && o.global, l && 0 === r.active++ && r.event.trigger("ajaxStart"), o.type = o.type.toUpperCase(), o.hasContent = !Gb.test(o.type), f = o.url.replace(Cb, ""), o.hasContent ? o.data && o.processData && 0 === (o.contentType || "").indexOf("application/x-www-form-urlencoded") && (o.data = o.data.replace(Bb, "+")) : (n = o.url.slice(f.length), o.data && (f += (vb.test(f) ? "&" : "?") + o.data, delete o.data), o.cache === !1 && (f = f.replace(Db, "$1"), n = (vb.test(f) ? "&" : "?") + "_=" + ub++ + n), o.url = f + n), o.ifModified && (r.lastModified[f] && y.setRequestHeader("If-Modified-Since", r.lastModified[f]), r.etag[f] && y.setRequestHeader("If-None-Match", r.etag[f])), (o.data && o.hasContent && o.contentType !== !1 || c.contentType) && y.setRequestHeader("Content-Type", o.contentType), y.setRequestHeader("Accept", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + ("*" !== o.dataTypes[0] ? ", " + Kb + "; q=0.01" : "") : o.accepts["*"]);
            for (m in o.headers) y.setRequestHeader(m, o.headers[m]);
            if (o.beforeSend && (o.beforeSend.call(p, y, o) === !1 || k)) return y.abort();
            if (x = "abort", t.add(o.complete), y.done(o.success), y.fail(o.error), e = Nb(Jb, o, c, y)) {
                if (y.readyState = 1, l && q.trigger("ajaxSend", [y, o]), k) return y;
                o.async && o.timeout > 0 && (i = a.setTimeout(function () {
                    y.abort("timeout")
                }, o.timeout));
                try {
                    k = !1, e.send(v, A)
                } catch (z) {
                    if (k) throw z;
                    A(-1, z)
                }
            } else A(-1, "No Transport");

            function A(b, c, d, h) {
                var j, m, n, v, w, x = c;
                k || (k = !0, i && a.clearTimeout(i), e = void 0, g = h || "", y.readyState = b > 0 ? 4 : 0, j = b >= 200 && b < 300 || 304 === b, d && (v = Pb(o, y, d)), v = Qb(o, v, y, j), j ? (o.ifModified && (w = y.getResponseHeader("Last-Modified"), w && (r.lastModified[f] = w), w = y.getResponseHeader("etag"), w && (r.etag[f] = w)), 204 === b || "HEAD" === o.type ? x = "nocontent" : 304 === b ? x = "notmodified" : (x = v.state, m = v.data, n = v.error, j = !n)) : (n = x, !b && x || (x = "error", b < 0 && (b = 0))), y.status = b, y.statusText = (c || x) + "", j ? s.resolveWith(p, [m, x, y]) : s.rejectWith(p, [y, x, n]), y.statusCode(u), u = void 0, l && q.trigger(j ? "ajaxSuccess" : "ajaxError", [y, o, j ? m : n]), t.fireWith(p, [y, x]), l && (q.trigger("ajaxComplete", [y, o]), --r.active || r.event.trigger("ajaxStop")))
            }

            return y
        },
        getJSON: function (a, b, c) {
            return r.get(a, b, c, "json")
        },
        getScript: function (a, b) {
            return r.get(a, void 0, b, "script")
        }
    }), r.each(["get", "post"], function (a, b) {
        r[b] = function (a, c, d, e) {
            return r.isFunction(c) && (e = e || d, d = c, c = void 0), r.ajax(r.extend({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            }, r.isPlainObject(a) && a))
        }
    }), r._evalUrl = function (a) {
        return r.ajax({url: a, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, throws: !0})
    }, r.fn.extend({
        wrapAll: function (a) {
            var b;
            return this[0] && (r.isFunction(a) && (a = a.call(this[0])), b = r(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function () {
                var a = this;
                while (a.firstElementChild) a = a.firstElementChild;
                return a
            }).append(this)), this
        }, wrapInner: function (a) {
            return r.isFunction(a) ? this.each(function (b) {
                r(this).wrapInner(a.call(this, b))
            }) : this.each(function () {
                var b = r(this), c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a)
            })
        }, wrap: function (a) {
            var b = r.isFunction(a);
            return this.each(function (c) {
                r(this).wrapAll(b ? a.call(this, c) : a)
            })
        }, unwrap: function (a) {
            return this.parent(a).not("body").each(function () {
                r(this).replaceWith(this.childNodes)
            }), this
        }
    }), r.expr.pseudos.hidden = function (a) {
        return !r.expr.pseudos.visible(a)
    }, r.expr.pseudos.visible = function (a) {
        return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length)
    }, r.ajaxSettings.xhr = function () {
        try {
            return new a.XMLHttpRequest
        } catch (b) {
        }
    };
    var Rb = {0: 200, 1223: 204}, Sb = r.ajaxSettings.xhr();
    o.cors = !!Sb && "withCredentials" in Sb, o.ajax = Sb = !!Sb, r.ajaxTransport(function (b) {
        var c, d;
        if (o.cors || Sb && !b.crossDomain) return {
            send: function (e, f) {
                var g, h = b.xhr();
                if (h.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields) for (g in b.xhrFields) h[g] = b.xhrFields[g];
                b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType), b.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest");
                for (g in e) h.setRequestHeader(g, e[g]);
                c = function (a) {
                    return function () {
                        c && (c = d = h.onload = h.onerror = h.onabort = h.onreadystatechange = null, "abort" === a ? h.abort() : "error" === a ? "number" != typeof h.status ? f(0, "error") : f(h.status, h.statusText) : f(Rb[h.status] || h.status, h.statusText, "text" !== (h.responseType || "text") || "string" != typeof h.responseText ? {binary: h.response} : {text: h.responseText}, h.getAllResponseHeaders()))
                    }
                }, h.onload = c(), d = h.onerror = c("error"), void 0 !== h.onabort ? h.onabort = d : h.onreadystatechange = function () {
                    4 === h.readyState && a.setTimeout(function () {
                        c && d()
                    })
                }, c = c("abort");
                try {
                    h.send(b.hasContent && b.data || null)
                } catch (i) {
                    if (c) throw i
                }
            }, abort: function () {
                c && c()
            }
        }
    }), r.ajaxPrefilter(function (a) {
        a.crossDomain && (a.contents.script = !1)
    }), r.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /\b(?:java|ecma)script\b/},
        converters: {
            "text script": function (a) {
                return r.globalEval(a), a
            }
        }
    }), r.ajaxPrefilter("script", function (a) {
        void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET")
    }), r.ajaxTransport("script", function (a) {
        if (a.crossDomain) {
            var b, c;
            return {
                send: function (e, f) {
                    b = r("<script>").prop({charset: a.scriptCharset, src: a.url}).on("load error", c = function (a) {
                        b.remove(), c = null, a && f("error" === a.type ? 404 : 200, a.type)
                    }), d.head.appendChild(b[0])
                }, abort: function () {
                    c && c()
                }
            }
        }
    });
    var Tb = [], Ub = /(=)\?(?=&|$)|\?\?/;
    r.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var a = Tb.pop() || r.expando + "_" + ub++;
            return this[a] = !0, a
        }
    }), r.ajaxPrefilter("json jsonp", function (b, c, d) {
        var e, f, g,
            h = b.jsonp !== !1 && (Ub.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && Ub.test(b.data) && "data");
        if (h || "jsonp" === b.dataTypes[0]) return e = b.jsonpCallback = r.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(Ub, "$1" + e) : b.jsonp !== !1 && (b.url += (vb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function () {
            return g || r.error(e + " was not called"), g[0]
        }, b.dataTypes[0] = "json", f = a[e], a[e] = function () {
            g = arguments
        }, d.always(function () {
            void 0 === f ? r(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, Tb.push(e)), g && r.isFunction(f) && f(g[0]), g = f = void 0
        }), "script"
    }), o.createHTMLDocument = function () {
        var a = d.implementation.createHTMLDocument("").body;
        return a.innerHTML = "<form></form><form></form>", 2 === a.childNodes.length
    }(), r.parseHTML = function (a, b, c) {
        if ("string" != typeof a) return [];
        "boolean" == typeof b && (c = b, b = !1);
        var e, f, g;
        return b || (o.createHTMLDocument ? (b = d.implementation.createHTMLDocument(""), e = b.createElement("base"), e.href = d.location.href, b.head.appendChild(e)) : b = d), f = C.exec(a), g = !c && [], f ? [b.createElement(f[1])] : (f = qa([a], b, g), g && g.length && r(g).remove(), r.merge([], f.childNodes))
    }, r.fn.load = function (a, b, c) {
        var d, e, f, g = this, h = a.indexOf(" ");
        return h > -1 && (d = pb(a.slice(h)), a = a.slice(0, h)), r.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), g.length > 0 && r.ajax({
            url: a,
            type: e || "GET",
            dataType: "html",
            data: b
        }).done(function (a) {
            f = arguments, g.html(d ? r("<div>").append(r.parseHTML(a)).find(d) : a)
        }).always(c && function (a, b) {
            g.each(function () {
                c.apply(this, f || [a.responseText, b, a])
            })
        }), this
    }, r.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) {
        r.fn[b] = function (a) {
            return this.on(b, a)
        }
    }), r.expr.pseudos.animated = function (a) {
        return r.grep(r.timers, function (b) {
            return a === b.elem
        }).length
    }, r.offset = {
        setOffset: function (a, b, c) {
            var d, e, f, g, h, i, j, k = r.css(a, "position"), l = r(a), m = {};
            "static" === k && (a.style.position = "relative"), h = l.offset(), f = r.css(a, "top"), i = r.css(a, "left"), j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), r.isFunction(b) && (b = b.call(a, c, r.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
        }
    }, r.fn.extend({
        offset: function (a) {
            if (arguments.length) return void 0 === a ? this : this.each(function (b) {
                r.offset.setOffset(this, a, b)
            });
            var b, c, d, e, f = this[0];
            if (f) return f.getClientRects().length ? (d = f.getBoundingClientRect(), b = f.ownerDocument, c = b.documentElement, e = b.defaultView, {
                top: d.top + e.pageYOffset - c.clientTop,
                left: d.left + e.pageXOffset - c.clientLeft
            }) : {top: 0, left: 0}
        }, position: function () {
            if (this[0]) {
                var a, b, c = this[0], d = {top: 0, left: 0};
                return "fixed" === r.css(c, "position") ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), B(a[0], "html") || (d = a.offset()), d = {
                    top: d.top + r.css(a[0], "borderTopWidth", !0),
                    left: d.left + r.css(a[0], "borderLeftWidth", !0)
                }), {top: b.top - d.top - r.css(c, "marginTop", !0), left: b.left - d.left - r.css(c, "marginLeft", !0)}
            }
        }, offsetParent: function () {
            return this.map(function () {
                var a = this.offsetParent;
                while (a && "static" === r.css(a, "position")) a = a.offsetParent;
                return a || ra
            })
        }
    }), r.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (a, b) {
        var c = "pageYOffset" === b;
        r.fn[a] = function (d) {
            return T(this, function (a, d, e) {
                var f;
                return r.isWindow(a) ? f = a : 9 === a.nodeType && (f = a.defaultView), void 0 === e ? f ? f[b] : a[d] : void (f ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset) : a[d] = e)
            }, a, d, arguments.length)
        }
    }), r.each(["top", "left"], function (a, b) {
        r.cssHooks[b] = Pa(o.pixelPosition, function (a, c) {
            if (c) return c = Oa(a, b), Ma.test(c) ? r(a).position()[b] + "px" : c
        })
    }), r.each({Height: "height", Width: "width"}, function (a, b) {
        r.each({padding: "inner" + a, content: b, "": "outer" + a}, function (c, d) {
            r.fn[d] = function (e, f) {
                var g = arguments.length && (c || "boolean" != typeof e),
                    h = c || (e === !0 || f === !0 ? "margin" : "border");
                return T(this, function (b, c, e) {
                    var f;
                    return r.isWindow(b) ? 0 === d.indexOf("outer") ? b["inner" + a] : b.document.documentElement["client" + a] : 9 === b.nodeType ? (f = b.documentElement, Math.max(b.body["scroll" + a], f["scroll" + a], b.body["offset" + a], f["offset" + a], f["client" + a])) : void 0 === e ? r.css(b, c, h) : r.style(b, c, e, h)
                }, b, g ? e : void 0, g)
            }
        })
    }), r.fn.extend({
        bind: function (a, b, c) {
            return this.on(a, null, b, c)
        }, unbind: function (a, b) {
            return this.off(a, null, b)
        }, delegate: function (a, b, c, d) {
            return this.on(b, a, c, d)
        }, undelegate: function (a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
        }
    }), r.holdReady = function (a) {
        a ? r.readyWait++ : r.ready(!0)
    }, r.isArray = Array.isArray, r.parseJSON = JSON.parse, r.nodeName = B, "function" == typeof define && define.amd && define("jquery", [], function () {
        return r
    });
    var Vb = a.jQuery, Wb = a.$;
    return r.noConflict = function (b) {
        return a.$ === r && (a.$ = Wb), b && a.jQuery === r && (a.jQuery = Vb), r
    }, b || (a.jQuery = a.$ = r), r
});
jQuery.easing["jswing"] = jQuery.easing["swing"];
jQuery.extend(jQuery.easing, {
    def: "easeOutQuad", swing: function (x, t, b, c, d) {
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d)
    }, easeInQuad: function (x, t, b, c, d) {
        return c * (t /= d) * t + b
    }, easeOutQuad: function (x, t, b, c, d) {
        return -c * (t /= d) * (t - 2) + b
    }, easeInOutQuad: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t + b;
        return -c / 2 * (--t * (t - 2) - 1) + b
    }, easeInCubic: function (x, t, b, c, d) {
        return c * (t /= d) * t * t + b
    }, easeOutCubic: function (x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t + 1) + b
    }, easeInOutCubic: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t + 2) + b
    }, easeInQuart: function (x, t, b, c, d) {
        return c * (t /= d) * t * t * t + b
    }, easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b
    }, easeInOutQuart: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
        return -c / 2 * ((t -= 2) * t * t * t - 2) + b
    }, easeInQuint: function (x, t, b, c, d) {
        return c * (t /= d) * t * t * t * t + b
    }, easeOutQuint: function (x, t, b, c, d) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b
    }, easeInOutQuint: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t * t * t + 2) + b
    }, easeInSine: function (x, t, b, c, d) {
        return -c * Math.cos(t / d * (Math.PI / 2)) + c + b
    }, easeOutSine: function (x, t, b, c, d) {
        return c * Math.sin(t / d * (Math.PI / 2)) + b
    }, easeInOutSine: function (x, t, b, c, d) {
        return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b
    }, easeInExpo: function (x, t, b, c, d) {
        return t == 0 ? b : c * Math.pow(2, 10 * (t / d - 1)) + b
    }, easeOutExpo: function (x, t, b, c, d) {
        return t == d ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b
    }, easeInOutExpo: function (x, t, b, c, d) {
        if (t == 0) return b;
        if (t == d) return b + c;
        if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b
    }, easeInCirc: function (x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b
    }, easeOutCirc: function (x, t, b, c, d) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b
    }, easeInOutCirc: function (x, t, b, c, d) {
        if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b
    }, easeInElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b
    }, easeOutElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d) == 1) return b + c;
        if (!p) p = d * .3;
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b
    }, easeInOutElastic: function (x, t, b, c, d) {
        var s = 1.70158;
        var p = 0;
        var a = c;
        if (t == 0) return b;
        if ((t /= d / 2) == 2) return b + c;
        if (!p) p = d * (.3 * 1.5);
        if (a < Math.abs(c)) {
            a = c;
            var s = p / 4
        } else var s = p / (2 * Math.PI) * Math.asin(c / a);
        if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b
    }, easeInBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b
    }, easeOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b
    }, easeInOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= 1.525) + 1) * t - s)) + b;
        return c / 2 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + b
    }, easeInBounce: function (x, t, b, c, d) {
        return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b
    }, easeOutBounce: function (x, t, b, c, d) {
        if ((t /= d) < 1 / 2.75) {
            return c * (7.5625 * t * t) + b
        } else if (t < 2 / 2.75) {
            return c * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + b
        } else if (t < 2.5 / 2.75) {
            return c * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + b
        } else {
            return c * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + b
        }
    }, easeInOutBounce: function (x, t, b, c, d) {
        if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
        return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b
    }
});
(function (global, factory) {
    typeof exports === "object" && typeof module !== "undefined" ? module.exports = factory() : typeof define === "function" && define.amd ? define(factory) : (global = global || self, global.Swiper = factory())
})(this, function () {
    "use strict";

    function isObject(obj) {
        return obj !== null && typeof obj === "object" && "constructor" in obj && obj.constructor === Object
    }

    function extend(target, src) {
        if (target === void 0) {
            target = {}
        }
        if (src === void 0) {
            src = {}
        }
        Object.keys(src).forEach(function (key) {
            if (typeof target[key] === "undefined") {
                target[key] = src[key]
            } else if (isObject(src[key]) && isObject(target[key]) && Object.keys(src[key]).length > 0) {
                extend(target[key], src[key])
            }
        })
    }

    var doc = typeof document !== "undefined" ? document : {};
    var ssrDocument = {
        body: {}, addEventListener: function () {
        }, removeEventListener: function () {
        }, activeElement: {
            blur: function () {
            }, nodeName: ""
        }, querySelector: function () {
            return null
        }, querySelectorAll: function () {
            return []
        }, getElementById: function () {
            return null
        }, createEvent: function () {
            return {
                initEvent: function () {
                }
            }
        }, createElement: function () {
            return {
                children: [], childNodes: [], style: {}, setAttribute: function () {
                }, getElementsByTagName: function () {
                    return []
                }
            }
        }, createElementNS: function () {
            return {}
        }, importNode: function () {
            return null
        }, location: {hash: "", host: "", hostname: "", href: "", origin: "", pathname: "", protocol: "", search: ""}
    };
    extend(doc, ssrDocument);
    var win = typeof window !== "undefined" ? window : {};
    var ssrWindow = {
        document: ssrDocument,
        navigator: {userAgent: ""},
        location: {hash: "", host: "", hostname: "", href: "", origin: "", pathname: "", protocol: "", search: ""},
        history: {
            replaceState: function () {
            }, pushState: function () {
            }, go: function () {
            }, back: function () {
            }
        },
        CustomEvent: function CustomEvent() {
            return this
        },
        addEventListener: function () {
        },
        removeEventListener: function () {
        },
        getComputedStyle: function () {
            return {
                getPropertyValue: function () {
                    return ""
                }
            }
        },
        Image: function () {
        },
        Date: function () {
        },
        screen: {},
        setTimeout: function () {
        },
        clearTimeout: function () {
        },
        matchMedia: function () {
            return {}
        }
    };
    extend(win, ssrWindow);
    var Dom7 = function Dom7(arr) {
        var self = this;
        for (var i = 0; i < arr.length; i += 1) {
            self[i] = arr[i]
        }
        self.length = arr.length;
        return this
    };

    function $(selector, context) {
        var arr = [];
        var i = 0;
        if (selector && !context) {
            if (selector instanceof Dom7) {
                return selector
            }
        }
        if (selector) {
            if (typeof selector === "string") {
                var els;
                var tempParent;
                var html = selector.trim();
                if (html.indexOf("<") >= 0 && html.indexOf(">") >= 0) {
                    var toCreate = "div";
                    if (html.indexOf("<li") === 0) {
                        toCreate = "ul"
                    }
                    if (html.indexOf("<tr") === 0) {
                        toCreate = "tbody"
                    }
                    if (html.indexOf("<td") === 0 || html.indexOf("<th") === 0) {
                        toCreate = "tr"
                    }
                    if (html.indexOf("<tbody") === 0) {
                        toCreate = "table"
                    }
                    if (html.indexOf("<option") === 0) {
                        toCreate = "select"
                    }
                    tempParent = doc.createElement(toCreate);
                    tempParent.innerHTML = html;
                    for (i = 0; i < tempParent.childNodes.length; i += 1) {
                        arr.push(tempParent.childNodes[i])
                    }
                } else {
                    if (!context && selector[0] === "#" && !selector.match(/[ .<>:~]/)) {
                        els = [doc.getElementById(selector.trim().split("#")[1])]
                    } else {
                        els = (context || doc).querySelectorAll(selector.trim())
                    }
                    for (i = 0; i < els.length; i += 1) {
                        if (els[i]) {
                            arr.push(els[i])
                        }
                    }
                }
            } else if (selector.nodeType || selector === win || selector === doc) {
                arr.push(selector)
            } else if (selector.length > 0 && selector[0].nodeType) {
                for (i = 0; i < selector.length; i += 1) {
                    arr.push(selector[i])
                }
            }
        }
        return new Dom7(arr)
    }

    $.fn = Dom7.prototype;
    $.Class = Dom7;
    $.Dom7 = Dom7;

    function unique(arr) {
        var uniqueArray = [];
        for (var i = 0; i < arr.length; i += 1) {
            if (uniqueArray.indexOf(arr[i]) === -1) {
                uniqueArray.push(arr[i])
            }
        }
        return uniqueArray
    }

    function addClass(className) {
        if (typeof className === "undefined") {
            return this
        }
        var classes = className.split(" ");
        for (var i = 0; i < classes.length; i += 1) {
            for (var j = 0; j < this.length; j += 1) {
                if (typeof this[j] !== "undefined" && typeof this[j].classList !== "undefined") {
                    this[j].classList.add(classes[i])
                }
            }
        }
        return this
    }

    function removeClass(className) {
        var classes = className.split(" ");
        for (var i = 0; i < classes.length; i += 1) {
            for (var j = 0; j < this.length; j += 1) {
                if (typeof this[j] !== "undefined" && typeof this[j].classList !== "undefined") {
                    this[j].classList.remove(classes[i])
                }
            }
        }
        return this
    }

    function hasClass(className) {
        if (!this[0]) {
            return false
        }
        return this[0].classList.contains(className)
    }

    function toggleClass(className) {
        var classes = className.split(" ");
        for (var i = 0; i < classes.length; i += 1) {
            for (var j = 0; j < this.length; j += 1) {
                if (typeof this[j] !== "undefined" && typeof this[j].classList !== "undefined") {
                    this[j].classList.toggle(classes[i])
                }
            }
        }
        return this
    }

    function attr(attrs, value) {
        var arguments$1 = arguments;
        if (arguments.length === 1 && typeof attrs === "string") {
            if (this[0]) {
                return this[0].getAttribute(attrs)
            }
            return undefined
        }
        for (var i = 0; i < this.length; i += 1) {
            if (arguments$1.length === 2) {
                this[i].setAttribute(attrs, value)
            } else {
                for (var attrName in attrs) {
                    this[i][attrName] = attrs[attrName];
                    this[i].setAttribute(attrName, attrs[attrName])
                }
            }
        }
        return this
    }

    function removeAttr(attr) {
        for (var i = 0; i < this.length; i += 1) {
            this[i].removeAttribute(attr)
        }
        return this
    }

    function data(key, value) {
        var el;
        if (typeof value === "undefined") {
            el = this[0];
            if (el) {
                if (el.dom7ElementDataStorage && key in el.dom7ElementDataStorage) {
                    return el.dom7ElementDataStorage[key]
                }
                var dataKey = el.getAttribute("data-" + key);
                if (dataKey) {
                    return dataKey
                }
                return undefined
            }
            return undefined
        }
        for (var i = 0; i < this.length; i += 1) {
            el = this[i];
            if (!el.dom7ElementDataStorage) {
                el.dom7ElementDataStorage = {}
            }
            el.dom7ElementDataStorage[key] = value
        }
        return this
    }

    function transform(transform) {
        for (var i = 0; i < this.length; i += 1) {
            var elStyle = this[i].style;
            elStyle.webkitTransform = transform;
            elStyle.transform = transform
        }
        return this
    }

    function transition(duration) {
        if (typeof duration !== "string") {
            duration = duration + "ms"
        }
        for (var i = 0; i < this.length; i += 1) {
            var elStyle = this[i].style;
            elStyle.webkitTransitionDuration = duration;
            elStyle.transitionDuration = duration
        }
        return this
    }

    function on() {
        var assign;
        var args = [], len = arguments.length;
        while (len--) args[len] = arguments[len];
        var eventType = args[0];
        var targetSelector = args[1];
        var listener = args[2];
        var capture = args[3];
        if (typeof args[1] === "function") {
            assign = args, eventType = assign[0], listener = assign[1], capture = assign[2];
            targetSelector = undefined
        }
        if (!capture) {
            capture = false
        }

        function handleLiveEvent(e) {
            var target = e.target;
            if (!target) {
                return
            }
            var eventData = e.target.dom7EventData || [];
            if (eventData.indexOf(e) < 0) {
                eventData.unshift(e)
            }
            if ($(target).is(targetSelector)) {
                listener.apply(target, eventData)
            } else {
                var parents = $(target).parents();
                for (var k = 0; k < parents.length; k += 1) {
                    if ($(parents[k]).is(targetSelector)) {
                        listener.apply(parents[k], eventData)
                    }
                }
            }
        }

        function handleEvent(e) {
            var eventData = e && e.target ? e.target.dom7EventData || [] : [];
            if (eventData.indexOf(e) < 0) {
                eventData.unshift(e)
            }
            listener.apply(this, eventData)
        }

        var events = eventType.split(" ");
        var j;
        for (var i = 0; i < this.length; i += 1) {
            var el = this[i];
            if (!targetSelector) {
                for (j = 0; j < events.length; j += 1) {
                    var event = events[j];
                    if (!el.dom7Listeners) {
                        el.dom7Listeners = {}
                    }
                    if (!el.dom7Listeners[event]) {
                        el.dom7Listeners[event] = []
                    }
                    el.dom7Listeners[event].push({listener: listener, proxyListener: handleEvent});
                    el.addEventListener(event, handleEvent, capture)
                }
            } else {
                for (j = 0; j < events.length; j += 1) {
                    var event$1 = events[j];
                    if (!el.dom7LiveListeners) {
                        el.dom7LiveListeners = {}
                    }
                    if (!el.dom7LiveListeners[event$1]) {
                        el.dom7LiveListeners[event$1] = []
                    }
                    el.dom7LiveListeners[event$1].push({listener: listener, proxyListener: handleLiveEvent});
                    el.addEventListener(event$1, handleLiveEvent, capture)
                }
            }
        }
        return this
    }

    function off() {
        var assign;
        var args = [], len = arguments.length;
        while (len--) args[len] = arguments[len];
        var eventType = args[0];
        var targetSelector = args[1];
        var listener = args[2];
        var capture = args[3];
        if (typeof args[1] === "function") {
            assign = args, eventType = assign[0], listener = assign[1], capture = assign[2];
            targetSelector = undefined
        }
        if (!capture) {
            capture = false
        }
        var events = eventType.split(" ");
        for (var i = 0; i < events.length; i += 1) {
            var event = events[i];
            for (var j = 0; j < this.length; j += 1) {
                var el = this[j];
                var handlers = void 0;
                if (!targetSelector && el.dom7Listeners) {
                    handlers = el.dom7Listeners[event]
                } else if (targetSelector && el.dom7LiveListeners) {
                    handlers = el.dom7LiveListeners[event]
                }
                if (handlers && handlers.length) {
                    for (var k = handlers.length - 1; k >= 0; k -= 1) {
                        var handler = handlers[k];
                        if (listener && handler.listener === listener) {
                            el.removeEventListener(event, handler.proxyListener, capture);
                            handlers.splice(k, 1)
                        } else if (listener && handler.listener && handler.listener.dom7proxy && handler.listener.dom7proxy === listener) {
                            el.removeEventListener(event, handler.proxyListener, capture);
                            handlers.splice(k, 1)
                        } else if (!listener) {
                            el.removeEventListener(event, handler.proxyListener, capture);
                            handlers.splice(k, 1)
                        }
                    }
                }
            }
        }
        return this
    }

    function trigger() {
        var args = [], len = arguments.length;
        while (len--) args[len] = arguments[len];
        var events = args[0].split(" ");
        var eventData = args[1];
        for (var i = 0; i < events.length; i += 1) {
            var event = events[i];
            for (var j = 0; j < this.length; j += 1) {
                var el = this[j];
                var evt = void 0;
                try {
                    evt = new win.CustomEvent(event, {detail: eventData, bubbles: true, cancelable: true})
                } catch (e) {
                    evt = doc.createEvent("Event");
                    evt.initEvent(event, true, true);
                    evt.detail = eventData
                }
                el.dom7EventData = args.filter(function (data, dataIndex) {
                    return dataIndex > 0
                });
                el.dispatchEvent(evt);
                el.dom7EventData = [];
                delete el.dom7EventData
            }
        }
        return this
    }

    function transitionEnd(callback) {
        var events = ["webkitTransitionEnd", "transitionend"];
        var dom = this;
        var i;

        function fireCallBack(e) {
            if (e.target !== this) {
                return
            }
            callback.call(this, e);
            for (i = 0; i < events.length; i += 1) {
                dom.off(events[i], fireCallBack)
            }
        }

        if (callback) {
            for (i = 0; i < events.length; i += 1) {
                dom.on(events[i], fireCallBack)
            }
        }
        return this
    }

    function outerWidth(includeMargins) {
        if (this.length > 0) {
            if (includeMargins) {
                var styles = this.styles();
                return this[0].offsetWidth + parseFloat(styles.getPropertyValue("margin-right")) + parseFloat(styles.getPropertyValue("margin-left"))
            }
            return this[0].offsetWidth
        }
        return null
    }

    function outerHeight(includeMargins) {
        if (this.length > 0) {
            if (includeMargins) {
                var styles = this.styles();
                return this[0].offsetHeight + parseFloat(styles.getPropertyValue("margin-top")) + parseFloat(styles.getPropertyValue("margin-bottom"))
            }
            return this[0].offsetHeight
        }
        return null
    }

    function offset() {
        if (this.length > 0) {
            var el = this[0];
            var box = el.getBoundingClientRect();
            var body = doc.body;
            var clientTop = el.clientTop || body.clientTop || 0;
            var clientLeft = el.clientLeft || body.clientLeft || 0;
            var scrollTop = el === win ? win.scrollY : el.scrollTop;
            var scrollLeft = el === win ? win.scrollX : el.scrollLeft;
            return {top: box.top + scrollTop - clientTop, left: box.left + scrollLeft - clientLeft}
        }
        return null
    }

    function styles() {
        if (this[0]) {
            return win.getComputedStyle(this[0], null)
        }
        return {}
    }

    function css(props, value) {
        var i;
        if (arguments.length === 1) {
            if (typeof props === "string") {
                if (this[0]) {
                    return win.getComputedStyle(this[0], null).getPropertyValue(props)
                }
            } else {
                for (i = 0; i < this.length; i += 1) {
                    for (var prop in props) {
                        this[i].style[prop] = props[prop]
                    }
                }
                return this
            }
        }
        if (arguments.length === 2 && typeof props === "string") {
            for (i = 0; i < this.length; i += 1) {
                this[i].style[props] = value
            }
            return this
        }
        return this
    }

    function each(callback) {
        if (!callback) {
            return this
        }
        for (var i = 0; i < this.length; i += 1) {
            if (callback.call(this[i], i, this[i]) === false) {
                return this
            }
        }
        return this
    }

    function filter(callback) {
        var matchedItems = [];
        var dom = this;
        for (var i = 0; i < dom.length; i += 1) {
            if (callback.call(dom[i], i, dom[i])) {
                matchedItems.push(dom[i])
            }
        }
        return new Dom7(matchedItems)
    }

    function html(html) {
        if (typeof html === "undefined") {
            return this[0] ? this[0].innerHTML : undefined
        }
        for (var i = 0; i < this.length; i += 1) {
            this[i].innerHTML = html
        }
        return this
    }

    function text(text) {
        if (typeof text === "undefined") {
            if (this[0]) {
                return this[0].textContent.trim()
            }
            return null
        }
        for (var i = 0; i < this.length; i += 1) {
            this[i].textContent = text
        }
        return this
    }

    function is(selector) {
        var el = this[0];
        var compareWith;
        var i;
        if (!el || typeof selector === "undefined") {
            return false
        }
        if (typeof selector === "string") {
            if (el.matches) {
                return el.matches(selector)
            } else if (el.webkitMatchesSelector) {
                return el.webkitMatchesSelector(selector)
            } else if (el.msMatchesSelector) {
                return el.msMatchesSelector(selector)
            }
            compareWith = $(selector);
            for (i = 0; i < compareWith.length; i += 1) {
                if (compareWith[i] === el) {
                    return true
                }
            }
            return false
        } else if (selector === doc) {
            return el === doc
        } else if (selector === win) {
            return el === win
        }
        if (selector.nodeType || selector instanceof Dom7) {
            compareWith = selector.nodeType ? [selector] : selector;
            for (i = 0; i < compareWith.length; i += 1) {
                if (compareWith[i] === el) {
                    return true
                }
            }
            return false
        }
        return false
    }

    function index() {
        var child = this[0];
        var i;
        if (child) {
            i = 0;
            while ((child = child.previousSibling) !== null) {
                if (child.nodeType === 1) {
                    i += 1
                }
            }
            return i
        }
        return undefined
    }

    function eq(index) {
        if (typeof index === "undefined") {
            return this
        }
        var length = this.length;
        var returnIndex;
        if (index > length - 1) {
            return new Dom7([])
        }
        if (index < 0) {
            returnIndex = length + index;
            if (returnIndex < 0) {
                return new Dom7([])
            }
            return new Dom7([this[returnIndex]])
        }
        return new Dom7([this[index]])
    }

    function append() {
        var args = [], len = arguments.length;
        while (len--) args[len] = arguments[len];
        var newChild;
        for (var k = 0; k < args.length; k += 1) {
            newChild = args[k];
            for (var i = 0; i < this.length; i += 1) {
                if (typeof newChild === "string") {
                    var tempDiv = doc.createElement("div");
                    tempDiv.innerHTML = newChild;
                    while (tempDiv.firstChild) {
                        this[i].appendChild(tempDiv.firstChild)
                    }
                } else if (newChild instanceof Dom7) {
                    for (var j = 0; j < newChild.length; j += 1) {
                        this[i].appendChild(newChild[j])
                    }
                } else {
                    this[i].appendChild(newChild)
                }
            }
        }
        return this
    }

    function prepend(newChild) {
        var i;
        var j;
        for (i = 0; i < this.length; i += 1) {
            if (typeof newChild === "string") {
                var tempDiv = doc.createElement("div");
                tempDiv.innerHTML = newChild;
                for (j = tempDiv.childNodes.length - 1; j >= 0; j -= 1) {
                    this[i].insertBefore(tempDiv.childNodes[j], this[i].childNodes[0])
                }
            } else if (newChild instanceof Dom7) {
                for (j = 0; j < newChild.length; j += 1) {
                    this[i].insertBefore(newChild[j], this[i].childNodes[0])
                }
            } else {
                this[i].insertBefore(newChild, this[i].childNodes[0])
            }
        }
        return this
    }

    function next(selector) {
        if (this.length > 0) {
            if (selector) {
                if (this[0].nextElementSibling && $(this[0].nextElementSibling).is(selector)) {
                    return new Dom7([this[0].nextElementSibling])
                }
                return new Dom7([])
            }
            if (this[0].nextElementSibling) {
                return new Dom7([this[0].nextElementSibling])
            }
            return new Dom7([])
        }
        return new Dom7([])
    }

    function nextAll(selector) {
        var nextEls = [];
        var el = this[0];
        if (!el) {
            return new Dom7([])
        }
        while (el.nextElementSibling) {
            var next = el.nextElementSibling;
            if (selector) {
                if ($(next).is(selector)) {
                    nextEls.push(next)
                }
            } else {
                nextEls.push(next)
            }
            el = next
        }
        return new Dom7(nextEls)
    }

    function prev(selector) {
        if (this.length > 0) {
            var el = this[0];
            if (selector) {
                if (el.previousElementSibling && $(el.previousElementSibling).is(selector)) {
                    return new Dom7([el.previousElementSibling])
                }
                return new Dom7([])
            }
            if (el.previousElementSibling) {
                return new Dom7([el.previousElementSibling])
            }
            return new Dom7([])
        }
        return new Dom7([])
    }

    function prevAll(selector) {
        var prevEls = [];
        var el = this[0];
        if (!el) {
            return new Dom7([])
        }
        while (el.previousElementSibling) {
            var prev = el.previousElementSibling;
            if (selector) {
                if ($(prev).is(selector)) {
                    prevEls.push(prev)
                }
            } else {
                prevEls.push(prev)
            }
            el = prev
        }
        return new Dom7(prevEls)
    }

    function parent(selector) {
        var parents = [];
        for (var i = 0; i < this.length; i += 1) {
            if (this[i].parentNode !== null) {
                if (selector) {
                    if ($(this[i].parentNode).is(selector)) {
                        parents.push(this[i].parentNode)
                    }
                } else {
                    parents.push(this[i].parentNode)
                }
            }
        }
        return $(unique(parents))
    }

    function parents(selector) {
        var parents = [];
        for (var i = 0; i < this.length; i += 1) {
            var parent = this[i].parentNode;
            while (parent) {
                if (selector) {
                    if ($(parent).is(selector)) {
                        parents.push(parent)
                    }
                } else {
                    parents.push(parent)
                }
                parent = parent.parentNode
            }
        }
        return $(unique(parents))
    }

    function closest(selector) {
        var closest = this;
        if (typeof selector === "undefined") {
            return new Dom7([])
        }
        if (!closest.is(selector)) {
            closest = closest.parents(selector).eq(0)
        }
        return closest
    }

    function find(selector) {
        var foundElements = [];
        for (var i = 0; i < this.length; i += 1) {
            var found = this[i].querySelectorAll(selector);
            for (var j = 0; j < found.length; j += 1) {
                foundElements.push(found[j])
            }
        }
        return new Dom7(foundElements)
    }

    function children(selector) {
        var children = [];
        for (var i = 0; i < this.length; i += 1) {
            var childNodes = this[i].childNodes;
            for (var j = 0; j < childNodes.length; j += 1) {
                if (!selector) {
                    if (childNodes[j].nodeType === 1) {
                        children.push(childNodes[j])
                    }
                } else if (childNodes[j].nodeType === 1 && $(childNodes[j]).is(selector)) {
                    children.push(childNodes[j])
                }
            }
        }
        return new Dom7(unique(children))
    }

    function remove() {
        for (var i = 0; i < this.length; i += 1) {
            if (this[i].parentNode) {
                this[i].parentNode.removeChild(this[i])
            }
        }
        return this
    }

    function add() {
        var args = [], len = arguments.length;
        while (len--) args[len] = arguments[len];
        var dom = this;
        var i;
        var j;
        for (i = 0; i < args.length; i += 1) {
            var toAdd = $(args[i]);
            for (j = 0; j < toAdd.length; j += 1) {
                dom[dom.length] = toAdd[j];
                dom.length += 1
            }
        }
        return dom
    }

    var Methods = {
        addClass: addClass,
        removeClass: removeClass,
        hasClass: hasClass,
        toggleClass: toggleClass,
        attr: attr,
        removeAttr: removeAttr,
        data: data,
        transform: transform,
        transition: transition,
        on: on,
        off: off,
        trigger: trigger,
        transitionEnd: transitionEnd,
        outerWidth: outerWidth,
        outerHeight: outerHeight,
        offset: offset,
        css: css,
        each: each,
        html: html,
        text: text,
        is: is,
        index: index,
        eq: eq,
        append: append,
        prepend: prepend,
        next: next,
        nextAll: nextAll,
        prev: prev,
        prevAll: prevAll,
        parent: parent,
        parents: parents,
        closest: closest,
        find: find,
        children: children,
        filter: filter,
        remove: remove,
        add: add,
        styles: styles
    };
    Object.keys(Methods).forEach(function (methodName) {
        $.fn[methodName] = $.fn[methodName] || Methods[methodName]
    });
    var Utils = {
        deleteProps: function deleteProps(obj) {
            var object = obj;
            Object.keys(object).forEach(function (key) {
                try {
                    object[key] = null
                } catch (e) {
                }
                try {
                    delete object[key]
                } catch (e) {
                }
            })
        }, nextTick: function nextTick(callback, delay) {
            if (delay === void 0) delay = 0;
            return setTimeout(callback, delay)
        }, now: function now() {
            return Date.now()
        }, getTranslate: function getTranslate(el, axis) {
            if (axis === void 0) axis = "x";
            var matrix;
            var curTransform;
            var transformMatrix;
            var curStyle = win.getComputedStyle(el, null);
            if (win.WebKitCSSMatrix) {
                curTransform = curStyle.transform || curStyle.webkitTransform;
                if (curTransform.split(",").length > 6) {
                    curTransform = curTransform.split(", ").map(function (a) {
                        return a.replace(",", ".")
                    }).join(", ")
                }
                transformMatrix = new win.WebKitCSSMatrix(curTransform === "none" ? "" : curTransform)
            } else {
                transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform || curStyle.transform || curStyle.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,");
                matrix = transformMatrix.toString().split(",")
            }
            if (axis === "x") {
                if (win.WebKitCSSMatrix) {
                    curTransform = transformMatrix.m41
                } else if (matrix.length === 16) {
                    curTransform = parseFloat(matrix[12])
                } else {
                    curTransform = parseFloat(matrix[4])
                }
            }
            if (axis === "y") {
                if (win.WebKitCSSMatrix) {
                    curTransform = transformMatrix.m42
                } else if (matrix.length === 16) {
                    curTransform = parseFloat(matrix[13])
                } else {
                    curTransform = parseFloat(matrix[5])
                }
            }
            return curTransform || 0
        }, parseUrlQuery: function parseUrlQuery(url) {
            var query = {};
            var urlToParse = url || win.location.href;
            var i;
            var params;
            var param;
            var length;
            if (typeof urlToParse === "string" && urlToParse.length) {
                urlToParse = urlToParse.indexOf("?") > -1 ? urlToParse.replace(/\S*\?/, "") : "";
                params = urlToParse.split("&").filter(function (paramsPart) {
                    return paramsPart !== ""
                });
                length = params.length;
                for (i = 0; i < length; i += 1) {
                    param = params[i].replace(/#\S+/g, "").split("=");
                    query[decodeURIComponent(param[0])] = typeof param[1] === "undefined" ? undefined : decodeURIComponent(param[1]) || ""
                }
            }
            return query
        }, isObject: function isObject(o) {
            return typeof o === "object" && o !== null && o.constructor && o.constructor === Object
        }, extend: function extend() {
            var args = [], len$1 = arguments.length;
            while (len$1--) args[len$1] = arguments[len$1];
            var to = Object(args[0]);
            for (var i = 1; i < args.length; i += 1) {
                var nextSource = args[i];
                if (nextSource !== undefined && nextSource !== null) {
                    var keysArray = Object.keys(Object(nextSource));
                    for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex += 1) {
                        var nextKey = keysArray[nextIndex];
                        var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                        if (desc !== undefined && desc.enumerable) {
                            if (Utils.isObject(to[nextKey]) && Utils.isObject(nextSource[nextKey])) {
                                Utils.extend(to[nextKey], nextSource[nextKey])
                            } else if (!Utils.isObject(to[nextKey]) && Utils.isObject(nextSource[nextKey])) {
                                to[nextKey] = {};
                                Utils.extend(to[nextKey], nextSource[nextKey])
                            } else {
                                to[nextKey] = nextSource[nextKey]
                            }
                        }
                    }
                }
            }
            return to
        }
    };
    var Support = function Support() {
        return {
            touch: !!("ontouchstart" in win || win.DocumentTouch && doc instanceof win.DocumentTouch),
            pointerEvents: !!win.PointerEvent && "maxTouchPoints" in win.navigator && win.navigator.maxTouchPoints >= 0,
            observer: function checkObserver() {
                return "MutationObserver" in win || "WebkitMutationObserver" in win
            }(),
            passiveListener: function checkPassiveListener() {
                var supportsPassive = false;
                try {
                    var opts = Object.defineProperty({}, "passive", {
                        get: function get() {
                            supportsPassive = true
                        }
                    });
                    win.addEventListener("testPassiveListener", null, opts)
                } catch (e) {
                }
                return supportsPassive
            }(),
            gestures: function checkGestures() {
                return "ongesturestart" in win
            }()
        }
    }();
    var SwiperClass = function SwiperClass(params) {
        if (params === void 0) params = {};
        var self = this;
        self.params = params;
        self.eventsListeners = {};
        if (self.params && self.params.on) {
            Object.keys(self.params.on).forEach(function (eventName) {
                self.on(eventName, self.params.on[eventName])
            })
        }
    };
    var staticAccessors = {components: {configurable: true}};
    SwiperClass.prototype.on = function on(events, handler, priority) {
        var self = this;
        if (typeof handler !== "function") {
            return self
        }
        var method = priority ? "unshift" : "push";
        events.split(" ").forEach(function (event) {
            if (!self.eventsListeners[event]) {
                self.eventsListeners[event] = []
            }
            self.eventsListeners[event][method](handler)
        });
        return self
    };
    SwiperClass.prototype.once = function once(events, handler, priority) {
        var self = this;
        if (typeof handler !== "function") {
            return self
        }

        function onceHandler() {
            var args = [], len = arguments.length;
            while (len--) args[len] = arguments[len];
            self.off(events, onceHandler);
            if (onceHandler.f7proxy) {
                delete onceHandler.f7proxy
            }
            handler.apply(self, args)
        }

        onceHandler.f7proxy = handler;
        return self.on(events, onceHandler, priority)
    };
    SwiperClass.prototype.off = function off(events, handler) {
        var self = this;
        if (!self.eventsListeners) {
            return self
        }
        events.split(" ").forEach(function (event) {
            if (typeof handler === "undefined") {
                self.eventsListeners[event] = []
            } else if (self.eventsListeners[event] && self.eventsListeners[event].length) {
                self.eventsListeners[event].forEach(function (eventHandler, index) {
                    if (eventHandler === handler || eventHandler.f7proxy && eventHandler.f7proxy === handler) {
                        self.eventsListeners[event].splice(index, 1)
                    }
                })
            }
        });
        return self
    };
    SwiperClass.prototype.emit = function emit() {
        var args = [], len = arguments.length;
        while (len--) args[len] = arguments[len];
        var self = this;
        if (!self.eventsListeners) {
            return self
        }
        var events;
        var data;
        var context;
        if (typeof args[0] === "string" || Array.isArray(args[0])) {
            events = args[0];
            data = args.slice(1, args.length);
            context = self
        } else {
            events = args[0].events;
            data = args[0].data;
            context = args[0].context || self
        }
        var eventsArray = Array.isArray(events) ? events : events.split(" ");
        eventsArray.forEach(function (event) {
            if (self.eventsListeners && self.eventsListeners[event]) {
                var handlers = [];
                self.eventsListeners[event].forEach(function (eventHandler) {
                    handlers.push(eventHandler)
                });
                handlers.forEach(function (eventHandler) {
                    eventHandler.apply(context, data)
                })
            }
        });
        return self
    };
    SwiperClass.prototype.useModulesParams = function useModulesParams(instanceParams) {
        var instance = this;
        if (!instance.modules) {
            return
        }
        Object.keys(instance.modules).forEach(function (moduleName) {
            var module = instance.modules[moduleName];
            if (module.params) {
                Utils.extend(instanceParams, module.params)
            }
        })
    };
    SwiperClass.prototype.useModules = function useModules(modulesParams) {
        if (modulesParams === void 0) modulesParams = {};
        var instance = this;
        if (!instance.modules) {
            return
        }
        Object.keys(instance.modules).forEach(function (moduleName) {
            var module = instance.modules[moduleName];
            var moduleParams = modulesParams[moduleName] || {};
            if (module.instance) {
                Object.keys(module.instance).forEach(function (modulePropName) {
                    var moduleProp = module.instance[modulePropName];
                    if (typeof moduleProp === "function") {
                        instance[modulePropName] = moduleProp.bind(instance)
                    } else {
                        instance[modulePropName] = moduleProp
                    }
                })
            }
            if (module.on && instance.on) {
                Object.keys(module.on).forEach(function (moduleEventName) {
                    instance.on(moduleEventName, module.on[moduleEventName])
                })
            }
            if (module.create) {
                module.create.bind(instance)(moduleParams)
            }
        })
    };
    staticAccessors.components.set = function (components) {
        var Class = this;
        if (!Class.use) {
            return
        }
        Class.use(components)
    };
    SwiperClass.installModule = function installModule(module) {
        var params = [], len = arguments.length - 1;
        while (len-- > 0) params[len] = arguments[len + 1];
        var Class = this;
        if (!Class.prototype.modules) {
            Class.prototype.modules = {}
        }
        var name = module.name || Object.keys(Class.prototype.modules).length + "_" + Utils.now();
        Class.prototype.modules[name] = module;
        if (module.proto) {
            Object.keys(module.proto).forEach(function (key) {
                Class.prototype[key] = module.proto[key]
            })
        }
        if (module.static) {
            Object.keys(module.static).forEach(function (key) {
                Class[key] = module.static[key]
            })
        }
        if (module.install) {
            module.install.apply(Class, params)
        }
        return Class
    };
    SwiperClass.use = function use(module) {
        var params = [], len = arguments.length - 1;
        while (len-- > 0) params[len] = arguments[len + 1];
        var Class = this;
        if (Array.isArray(module)) {
            module.forEach(function (m) {
                return Class.installModule(m)
            });
            return Class
        }
        return Class.installModule.apply(Class, [module].concat(params))
    };
    Object.defineProperties(SwiperClass, staticAccessors);

    function updateSize() {
        var swiper = this;
        var width;
        var height;
        var $el = swiper.$el;
        if (typeof swiper.params.width !== "undefined") {
            width = swiper.params.width
        } else {
            width = $el[0].clientWidth
        }
        if (typeof swiper.params.height !== "undefined") {
            height = swiper.params.height
        } else {
            height = $el[0].clientHeight
        }
        if (width === 0 && swiper.isHorizontal() || height === 0 && swiper.isVertical()) {
            return
        }
        width = width - parseInt($el.css("padding-left"), 10) - parseInt($el.css("padding-right"), 10);
        height = height - parseInt($el.css("padding-top"), 10) - parseInt($el.css("padding-bottom"), 10);
        Utils.extend(swiper, {width: width, height: height, size: swiper.isHorizontal() ? width : height})
    }

    function updateSlides() {
        var swiper = this;
        var params = swiper.params;
        var $wrapperEl = swiper.$wrapperEl;
        var swiperSize = swiper.size;
        var rtl = swiper.rtlTranslate;
        var wrongRTL = swiper.wrongRTL;
        var isVirtual = swiper.virtual && params.virtual.enabled;
        var previousSlidesLength = isVirtual ? swiper.virtual.slides.length : swiper.slides.length;
        var slides = $wrapperEl.children("." + swiper.params.slideClass);
        var slidesLength = isVirtual ? swiper.virtual.slides.length : slides.length;
        var snapGrid = [];
        var slidesGrid = [];
        var slidesSizesGrid = [];

        function slidesForMargin(slideIndex) {
            if (!params.cssMode) {
                return true
            }
            if (slideIndex === slides.length - 1) {
                return false
            }
            return true
        }

        var offsetBefore = params.slidesOffsetBefore;
        if (typeof offsetBefore === "function") {
            offsetBefore = params.slidesOffsetBefore.call(swiper)
        }
        var offsetAfter = params.slidesOffsetAfter;
        if (typeof offsetAfter === "function") {
            offsetAfter = params.slidesOffsetAfter.call(swiper)
        }
        var previousSnapGridLength = swiper.snapGrid.length;
        var previousSlidesGridLength = swiper.snapGrid.length;
        var spaceBetween = params.spaceBetween;
        var slidePosition = -offsetBefore;
        var prevSlideSize = 0;
        var index = 0;
        if (typeof swiperSize === "undefined") {
            return
        }
        if (typeof spaceBetween === "string" && spaceBetween.indexOf("%") >= 0) {
            spaceBetween = parseFloat(spaceBetween.replace("%", "")) / 100 * swiperSize
        }
        swiper.virtualSize = -spaceBetween;
        if (rtl) {
            slides.css({marginLeft: "", marginTop: ""})
        } else {
            slides.css({marginRight: "", marginBottom: ""})
        }
        var slidesNumberEvenToRows;
        if (params.slidesPerColumn > 1) {
            if (Math.floor(slidesLength / params.slidesPerColumn) === slidesLength / swiper.params.slidesPerColumn) {
                slidesNumberEvenToRows = slidesLength
            } else {
                slidesNumberEvenToRows = Math.ceil(slidesLength / params.slidesPerColumn) * params.slidesPerColumn
            }
            if (params.slidesPerView !== "auto" && params.slidesPerColumnFill === "row") {
                slidesNumberEvenToRows = Math.max(slidesNumberEvenToRows, params.slidesPerView * params.slidesPerColumn)
            }
        }
        var slideSize;
        var slidesPerColumn = params.slidesPerColumn;
        var slidesPerRow = slidesNumberEvenToRows / slidesPerColumn;
        var numFullColumns = Math.floor(slidesLength / params.slidesPerColumn);
        for (var i = 0; i < slidesLength; i += 1) {
            slideSize = 0;
            var slide = slides.eq(i);
            if (params.slidesPerColumn > 1) {
                var newSlideOrderIndex = void 0;
                var column = void 0;
                var row = void 0;
                if (params.slidesPerColumnFill === "row" && params.slidesPerGroup > 1) {
                    var groupIndex = Math.floor(i / (params.slidesPerGroup * params.slidesPerColumn));
                    var slideIndexInGroup = i - params.slidesPerColumn * params.slidesPerGroup * groupIndex;
                    var columnsInGroup = groupIndex === 0 ? params.slidesPerGroup : Math.min(Math.ceil((slidesLength - groupIndex * slidesPerColumn * params.slidesPerGroup) / slidesPerColumn), params.slidesPerGroup);
                    row = Math.floor(slideIndexInGroup / columnsInGroup);
                    column = slideIndexInGroup - row * columnsInGroup + groupIndex * params.slidesPerGroup;
                    newSlideOrderIndex = column + row * slidesNumberEvenToRows / slidesPerColumn;
                    slide.css({
                        "-webkit-box-ordinal-group": newSlideOrderIndex,
                        "-moz-box-ordinal-group": newSlideOrderIndex,
                        "-ms-flex-order": newSlideOrderIndex,
                        "-webkit-order": newSlideOrderIndex,
                        order: newSlideOrderIndex
                    })
                } else if (params.slidesPerColumnFill === "column") {
                    column = Math.floor(i / slidesPerColumn);
                    row = i - column * slidesPerColumn;
                    if (column > numFullColumns || column === numFullColumns && row === slidesPerColumn - 1) {
                        row += 1;
                        if (row >= slidesPerColumn) {
                            row = 0;
                            column += 1
                        }
                    }
                } else {
                    row = Math.floor(i / slidesPerRow);
                    column = i - row * slidesPerRow
                }
                slide.css("margin-" + (swiper.isHorizontal() ? "top" : "left"), row !== 0 && params.spaceBetween && params.spaceBetween + "px")
            }
            if (slide.css("display") === "none") {
                continue
            }
            if (params.slidesPerView === "auto") {
                var slideStyles = win.getComputedStyle(slide[0], null);
                var currentTransform = slide[0].style.transform;
                var currentWebKitTransform = slide[0].style.webkitTransform;
                if (currentTransform) {
                    slide[0].style.transform = "none"
                }
                if (currentWebKitTransform) {
                    slide[0].style.webkitTransform = "none"
                }
                if (params.roundLengths) {
                    slideSize = swiper.isHorizontal() ? slide.outerWidth(true) : slide.outerHeight(true)
                } else {
                    if (swiper.isHorizontal()) {
                        var width = parseFloat(slideStyles.getPropertyValue("width"));
                        var paddingLeft = parseFloat(slideStyles.getPropertyValue("padding-left"));
                        var paddingRight = parseFloat(slideStyles.getPropertyValue("padding-right"));
                        var marginLeft = parseFloat(slideStyles.getPropertyValue("margin-left"));
                        var marginRight = parseFloat(slideStyles.getPropertyValue("margin-right"));
                        var boxSizing = slideStyles.getPropertyValue("box-sizing");
                        if (boxSizing && boxSizing === "border-box") {
                            slideSize = width + marginLeft + marginRight
                        } else {
                            slideSize = width + paddingLeft + paddingRight + marginLeft + marginRight
                        }
                    } else {
                        var height = parseFloat(slideStyles.getPropertyValue("height"));
                        var paddingTop = parseFloat(slideStyles.getPropertyValue("padding-top"));
                        var paddingBottom = parseFloat(slideStyles.getPropertyValue("padding-bottom"));
                        var marginTop = parseFloat(slideStyles.getPropertyValue("margin-top"));
                        var marginBottom = parseFloat(slideStyles.getPropertyValue("margin-bottom"));
                        var boxSizing$1 = slideStyles.getPropertyValue("box-sizing");
                        if (boxSizing$1 && boxSizing$1 === "border-box") {
                            slideSize = height + marginTop + marginBottom
                        } else {
                            slideSize = height + paddingTop + paddingBottom + marginTop + marginBottom
                        }
                    }
                }
                if (currentTransform) {
                    slide[0].style.transform = currentTransform
                }
                if (currentWebKitTransform) {
                    slide[0].style.webkitTransform = currentWebKitTransform
                }
                if (params.roundLengths) {
                    slideSize = Math.floor(slideSize)
                }
            } else {
                slideSize = (swiperSize - (params.slidesPerView - 1) * spaceBetween) / params.slidesPerView;
                if (params.roundLengths) {
                    slideSize = Math.floor(slideSize)
                }
                if (slides[i]) {
                    if (swiper.isHorizontal()) {
                        slides[i].style.width = slideSize + "px"
                    } else {
                        slides[i].style.height = slideSize + "px"
                    }
                }
            }
            if (slides[i]) {
                slides[i].swiperSlideSize = slideSize
            }
            slidesSizesGrid.push(slideSize);
            if (params.centeredSlides) {
                slidePosition = slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;
                if (prevSlideSize === 0 && i !== 0) {
                    slidePosition = slidePosition - swiperSize / 2 - spaceBetween
                }
                if (i === 0) {
                    slidePosition = slidePosition - swiperSize / 2 - spaceBetween
                }
                if (Math.abs(slidePosition) < 1 / 1e3) {
                    slidePosition = 0
                }
                if (params.roundLengths) {
                    slidePosition = Math.floor(slidePosition)
                }
                if (index % params.slidesPerGroup === 0) {
                    snapGrid.push(slidePosition)
                }
                slidesGrid.push(slidePosition)
            } else {
                if (params.roundLengths) {
                    slidePosition = Math.floor(slidePosition)
                }
                if ((index - Math.min(swiper.params.slidesPerGroupSkip, index)) % swiper.params.slidesPerGroup === 0) {
                    snapGrid.push(slidePosition)
                }
                slidesGrid.push(slidePosition);
                slidePosition = slidePosition + slideSize + spaceBetween
            }
            swiper.virtualSize += slideSize + spaceBetween;
            prevSlideSize = slideSize;
            index += 1
        }
        swiper.virtualSize = Math.max(swiper.virtualSize, swiperSize) + offsetAfter;
        var newSlidesGrid;
        if (rtl && wrongRTL && (params.effect === "slide" || params.effect === "coverflow")) {
            $wrapperEl.css({width: swiper.virtualSize + params.spaceBetween + "px"})
        }
        if (params.setWrapperSize) {
            if (swiper.isHorizontal()) {
                $wrapperEl.css({width: swiper.virtualSize + params.spaceBetween + "px"})
            } else {
                $wrapperEl.css({height: swiper.virtualSize + params.spaceBetween + "px"})
            }
        }
        if (params.slidesPerColumn > 1) {
            swiper.virtualSize = (slideSize + params.spaceBetween) * slidesNumberEvenToRows;
            swiper.virtualSize = Math.ceil(swiper.virtualSize / params.slidesPerColumn) - params.spaceBetween;
            if (swiper.isHorizontal()) {
                $wrapperEl.css({width: swiper.virtualSize + params.spaceBetween + "px"})
            } else {
                $wrapperEl.css({height: swiper.virtualSize + params.spaceBetween + "px"})
            }
            if (params.centeredSlides) {
                newSlidesGrid = [];
                for (var i$1 = 0; i$1 < snapGrid.length; i$1 += 1) {
                    var slidesGridItem = snapGrid[i$1];
                    if (params.roundLengths) {
                        slidesGridItem = Math.floor(slidesGridItem)
                    }
                    if (snapGrid[i$1] < swiper.virtualSize + snapGrid[0]) {
                        newSlidesGrid.push(slidesGridItem)
                    }
                }
                snapGrid = newSlidesGrid
            }
        }
        if (!params.centeredSlides) {
            newSlidesGrid = [];
            for (var i$2 = 0; i$2 < snapGrid.length; i$2 += 1) {
                var slidesGridItem$1 = snapGrid[i$2];
                if (params.roundLengths) {
                    slidesGridItem$1 = Math.floor(slidesGridItem$1)
                }
                if (snapGrid[i$2] <= swiper.virtualSize - swiperSize) {
                    newSlidesGrid.push(slidesGridItem$1)
                }
            }
            snapGrid = newSlidesGrid;
            if (Math.floor(swiper.virtualSize - swiperSize) - Math.floor(snapGrid[snapGrid.length - 1]) > 1) {
                snapGrid.push(swiper.virtualSize - swiperSize)
            }
        }
        if (snapGrid.length === 0) {
            snapGrid = [0]
        }
        if (params.spaceBetween !== 0) {
            if (swiper.isHorizontal()) {
                if (rtl) {
                    slides.filter(slidesForMargin).css({marginLeft: spaceBetween + "px"})
                } else {
                    slides.filter(slidesForMargin).css({marginRight: spaceBetween + "px"})
                }
            } else {
                slides.filter(slidesForMargin).css({marginBottom: spaceBetween + "px"})
            }
        }
        if (params.centeredSlides && params.centeredSlidesBounds) {
            var allSlidesSize = 0;
            slidesSizesGrid.forEach(function (slideSizeValue) {
                allSlidesSize += slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0)
            });
            allSlidesSize -= params.spaceBetween;
            var maxSnap = allSlidesSize - swiperSize;
            snapGrid = snapGrid.map(function (snap) {
                if (snap < 0) {
                    return -offsetBefore
                }
                if (snap > maxSnap) {
                    return maxSnap + offsetAfter
                }
                return snap
            })
        }
        if (params.centerInsufficientSlides) {
            var allSlidesSize$1 = 0;
            slidesSizesGrid.forEach(function (slideSizeValue) {
                allSlidesSize$1 += slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0)
            });
            allSlidesSize$1 -= params.spaceBetween;
            if (allSlidesSize$1 < swiperSize) {
                var allSlidesOffset = (swiperSize - allSlidesSize$1) / 2;
                snapGrid.forEach(function (snap, snapIndex) {
                    snapGrid[snapIndex] = snap - allSlidesOffset
                });
                slidesGrid.forEach(function (snap, snapIndex) {
                    slidesGrid[snapIndex] = snap + allSlidesOffset
                })
            }
        }
        Utils.extend(swiper, {
            slides: slides,
            snapGrid: snapGrid,
            slidesGrid: slidesGrid,
            slidesSizesGrid: slidesSizesGrid
        });
        if (slidesLength !== previousSlidesLength) {
            swiper.emit("slidesLengthChange")
        }
        if (snapGrid.length !== previousSnapGridLength) {
            if (swiper.params.watchOverflow) {
                swiper.checkOverflow()
            }
            swiper.emit("snapGridLengthChange")
        }
        if (slidesGrid.length !== previousSlidesGridLength) {
            swiper.emit("slidesGridLengthChange")
        }
        if (params.watchSlidesProgress || params.watchSlidesVisibility) {
            swiper.updateSlidesOffset()
        }
    }

    function updateAutoHeight(speed) {
        var swiper = this;
        var activeSlides = [];
        var newHeight = 0;
        var i;
        if (typeof speed === "number") {
            swiper.setTransition(speed)
        } else if (speed === true) {
            swiper.setTransition(swiper.params.speed)
        }
        if (swiper.params.slidesPerView !== "auto" && swiper.params.slidesPerView > 1) {
            if (swiper.params.centeredSlides) {
                swiper.visibleSlides.each(function (index, slide) {
                    activeSlides.push(slide)
                })
            } else {
                for (i = 0; i < Math.ceil(swiper.params.slidesPerView); i += 1) {
                    var index = swiper.activeIndex + i;
                    if (index > swiper.slides.length) {
                        break
                    }
                    activeSlides.push(swiper.slides.eq(index)[0])
                }
            }
        } else {
            activeSlides.push(swiper.slides.eq(swiper.activeIndex)[0])
        }
        for (i = 0; i < activeSlides.length; i += 1) {
            if (typeof activeSlides[i] !== "undefined") {
                var height = activeSlides[i].offsetHeight;
                newHeight = height > newHeight ? height : newHeight
            }
        }
        if (newHeight) {
            swiper.$wrapperEl.css("height", newHeight + "px")
        }
    }

    function updateSlidesOffset() {
        var swiper = this;
        var slides = swiper.slides;
        for (var i = 0; i < slides.length; i += 1) {
            slides[i].swiperSlideOffset = swiper.isHorizontal() ? slides[i].offsetLeft : slides[i].offsetTop
        }
    }

    function updateSlidesProgress(translate) {
        if (translate === void 0) translate = this && this.translate || 0;
        var swiper = this;
        var params = swiper.params;
        var slides = swiper.slides;
        var rtl = swiper.rtlTranslate;
        if (slides.length === 0) {
            return
        }
        if (typeof slides[0].swiperSlideOffset === "undefined") {
            swiper.updateSlidesOffset()
        }
        var offsetCenter = -translate;
        if (rtl) {
            offsetCenter = translate
        }
        slides.removeClass(params.slideVisibleClass);
        swiper.visibleSlidesIndexes = [];
        swiper.visibleSlides = [];
        for (var i = 0; i < slides.length; i += 1) {
            var slide = slides[i];
            var slideProgress = (offsetCenter + (params.centeredSlides ? swiper.minTranslate() : 0) - slide.swiperSlideOffset) / (slide.swiperSlideSize + params.spaceBetween);
            if (params.watchSlidesVisibility || params.centeredSlides && params.autoHeight) {
                var slideBefore = -(offsetCenter - slide.swiperSlideOffset);
                var slideAfter = slideBefore + swiper.slidesSizesGrid[i];
                var isVisible = slideBefore >= 0 && slideBefore < swiper.size - 1 || slideAfter > 1 && slideAfter <= swiper.size || slideBefore <= 0 && slideAfter >= swiper.size;
                if (isVisible) {
                    swiper.visibleSlides.push(slide);
                    swiper.visibleSlidesIndexes.push(i);
                    slides.eq(i).addClass(params.slideVisibleClass)
                }
            }
            slide.progress = rtl ? -slideProgress : slideProgress
        }
        swiper.visibleSlides = $(swiper.visibleSlides)
    }

    function updateProgress(translate) {
        var swiper = this;
        if (typeof translate === "undefined") {
            var multiplier = swiper.rtlTranslate ? -1 : 1;
            translate = swiper && swiper.translate && swiper.translate * multiplier || 0
        }
        var params = swiper.params;
        var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
        var progress = swiper.progress;
        var isBeginning = swiper.isBeginning;
        var isEnd = swiper.isEnd;
        var wasBeginning = isBeginning;
        var wasEnd = isEnd;
        if (translatesDiff === 0) {
            progress = 0;
            isBeginning = true;
            isEnd = true
        } else {
            progress = (translate - swiper.minTranslate()) / translatesDiff;
            isBeginning = progress <= 0;
            isEnd = progress >= 1
        }
        Utils.extend(swiper, {progress: progress, isBeginning: isBeginning, isEnd: isEnd});
        if (params.watchSlidesProgress || params.watchSlidesVisibility || params.centeredSlides && params.autoHeight) {
            swiper.updateSlidesProgress(translate)
        }
        if (isBeginning && !wasBeginning) {
            swiper.emit("reachBeginning toEdge")
        }
        if (isEnd && !wasEnd) {
            swiper.emit("reachEnd toEdge")
        }
        if (wasBeginning && !isBeginning || wasEnd && !isEnd) {
            swiper.emit("fromEdge")
        }
        swiper.emit("progress", progress)
    }

    function updateSlidesClasses() {
        var swiper = this;
        var slides = swiper.slides;
        var params = swiper.params;
        var $wrapperEl = swiper.$wrapperEl;
        var activeIndex = swiper.activeIndex;
        var realIndex = swiper.realIndex;
        var isVirtual = swiper.virtual && params.virtual.enabled;
        slides.removeClass(params.slideActiveClass + " " + params.slideNextClass + " " + params.slidePrevClass + " " + params.slideDuplicateActiveClass + " " + params.slideDuplicateNextClass + " " + params.slideDuplicatePrevClass);
        var activeSlide;
        if (isVirtual) {
            activeSlide = swiper.$wrapperEl.find("." + params.slideClass + '[data-swiper-slide-index="' + activeIndex + '"]')
        } else {
            activeSlide = slides.eq(activeIndex)
        }
        activeSlide.addClass(params.slideActiveClass);
        if (params.loop) {
            if (activeSlide.hasClass(params.slideDuplicateClass)) {
                $wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ')[data-swiper-slide-index="' + realIndex + '"]').addClass(params.slideDuplicateActiveClass)
            } else {
                $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + '[data-swiper-slide-index="' + realIndex + '"]').addClass(params.slideDuplicateActiveClass)
            }
        }
        var nextSlide = activeSlide.nextAll("." + params.slideClass).eq(0).addClass(params.slideNextClass);
        if (params.loop && nextSlide.length === 0) {
            nextSlide = slides.eq(0);
            nextSlide.addClass(params.slideNextClass)
        }
        var prevSlide = activeSlide.prevAll("." + params.slideClass).eq(0).addClass(params.slidePrevClass);
        if (params.loop && prevSlide.length === 0) {
            prevSlide = slides.eq(-1);
            prevSlide.addClass(params.slidePrevClass)
        }
        if (params.loop) {
            if (nextSlide.hasClass(params.slideDuplicateClass)) {
                $wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ')[data-swiper-slide-index="' + nextSlide.attr("data-swiper-slide-index") + '"]').addClass(params.slideDuplicateNextClass)
            } else {
                $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + '[data-swiper-slide-index="' + nextSlide.attr("data-swiper-slide-index") + '"]').addClass(params.slideDuplicateNextClass)
            }
            if (prevSlide.hasClass(params.slideDuplicateClass)) {
                $wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ')[data-swiper-slide-index="' + prevSlide.attr("data-swiper-slide-index") + '"]').addClass(params.slideDuplicatePrevClass)
            } else {
                $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + '[data-swiper-slide-index="' + prevSlide.attr("data-swiper-slide-index") + '"]').addClass(params.slideDuplicatePrevClass)
            }
        }
    }

    function updateActiveIndex(newActiveIndex) {
        var swiper = this;
        var translate = swiper.rtlTranslate ? swiper.translate : -swiper.translate;
        var slidesGrid = swiper.slidesGrid;
        var snapGrid = swiper.snapGrid;
        var params = swiper.params;
        var previousIndex = swiper.activeIndex;
        var previousRealIndex = swiper.realIndex;
        var previousSnapIndex = swiper.snapIndex;
        var activeIndex = newActiveIndex;
        var snapIndex;
        if (typeof activeIndex === "undefined") {
            for (var i = 0; i < slidesGrid.length; i += 1) {
                if (typeof slidesGrid[i + 1] !== "undefined") {
                    if (translate >= slidesGrid[i] && translate < slidesGrid[i + 1] - (slidesGrid[i + 1] - slidesGrid[i]) / 2) {
                        activeIndex = i
                    } else if (translate >= slidesGrid[i] && translate < slidesGrid[i + 1]) {
                        activeIndex = i + 1
                    }
                } else if (translate >= slidesGrid[i]) {
                    activeIndex = i
                }
            }
            if (params.normalizeSlideIndex) {
                if (activeIndex < 0 || typeof activeIndex === "undefined") {
                    activeIndex = 0
                }
            }
        }
        if (snapGrid.indexOf(translate) >= 0) {
            snapIndex = snapGrid.indexOf(translate)
        } else {
            var skip = Math.min(params.slidesPerGroupSkip, activeIndex);
            snapIndex = skip + Math.floor((activeIndex - skip) / params.slidesPerGroup)
        }
        if (snapIndex >= snapGrid.length) {
            snapIndex = snapGrid.length - 1
        }
        if (activeIndex === previousIndex) {
            if (snapIndex !== previousSnapIndex) {
                swiper.snapIndex = snapIndex;
                swiper.emit("snapIndexChange")
            }
            return
        }
        var realIndex = parseInt(swiper.slides.eq(activeIndex).attr("data-swiper-slide-index") || activeIndex, 10);
        Utils.extend(swiper, {
            snapIndex: snapIndex,
            realIndex: realIndex,
            previousIndex: previousIndex,
            activeIndex: activeIndex
        });
        swiper.emit("activeIndexChange");
        swiper.emit("snapIndexChange");
        if (previousRealIndex !== realIndex) {
            swiper.emit("realIndexChange")
        }
        if (swiper.initialized || swiper.params.runCallbacksOnInit) {
            swiper.emit("slideChange")
        }
    }

    function updateClickedSlide(e) {
        var swiper = this;
        var params = swiper.params;
        var slide = $(e.target).closest("." + params.slideClass)[0];
        var slideFound = false;
        if (slide) {
            for (var i = 0; i < swiper.slides.length; i += 1) {
                if (swiper.slides[i] === slide) {
                    slideFound = true
                }
            }
        }
        if (slide && slideFound) {
            swiper.clickedSlide = slide;
            if (swiper.virtual && swiper.params.virtual.enabled) {
                swiper.clickedIndex = parseInt($(slide).attr("data-swiper-slide-index"), 10)
            } else {
                swiper.clickedIndex = $(slide).index()
            }
        } else {
            swiper.clickedSlide = undefined;
            swiper.clickedIndex = undefined;
            return
        }
        if (params.slideToClickedSlide && swiper.clickedIndex !== undefined && swiper.clickedIndex !== swiper.activeIndex) {
            swiper.slideToClickedSlide()
        }
    }

    var update = {
        updateSize: updateSize,
        updateSlides: updateSlides,
        updateAutoHeight: updateAutoHeight,
        updateSlidesOffset: updateSlidesOffset,
        updateSlidesProgress: updateSlidesProgress,
        updateProgress: updateProgress,
        updateSlidesClasses: updateSlidesClasses,
        updateActiveIndex: updateActiveIndex,
        updateClickedSlide: updateClickedSlide
    };

    function getTranslate(axis) {
        if (axis === void 0) axis = this.isHorizontal() ? "x" : "y";
        var swiper = this;
        var params = swiper.params;
        var rtl = swiper.rtlTranslate;
        var translate = swiper.translate;
        var $wrapperEl = swiper.$wrapperEl;
        if (params.virtualTranslate) {
            return rtl ? -translate : translate
        }
        if (params.cssMode) {
            return translate
        }
        var currentTranslate = Utils.getTranslate($wrapperEl[0], axis);
        if (rtl) {
            currentTranslate = -currentTranslate
        }
        return currentTranslate || 0
    }

    function setTranslate(translate, byController) {
        var swiper = this;
        var rtl = swiper.rtlTranslate;
        var params = swiper.params;
        var $wrapperEl = swiper.$wrapperEl;
        var wrapperEl = swiper.wrapperEl;
        var progress = swiper.progress;
        var x = 0;
        var y = 0;
        var z = 0;
        if (swiper.isHorizontal()) {
            x = rtl ? -translate : translate
        } else {
            y = translate
        }
        if (params.roundLengths) {
            x = Math.floor(x);
            y = Math.floor(y)
        }
        if (params.cssMode) {
            wrapperEl[swiper.isHorizontal() ? "scrollLeft" : "scrollTop"] = swiper.isHorizontal() ? -x : -y
        } else if (!params.virtualTranslate) {
            $wrapperEl.transform("translate3d(" + x + "px, " + y + "px, " + z + "px)")
        }
        swiper.previousTranslate = swiper.translate;
        swiper.translate = swiper.isHorizontal() ? x : y;
        var newProgress;
        var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
        if (translatesDiff === 0) {
            newProgress = 0
        } else {
            newProgress = (translate - swiper.minTranslate()) / translatesDiff
        }
        if (newProgress !== progress) {
            swiper.updateProgress(translate)
        }
        swiper.emit("setTranslate", swiper.translate, byController)
    }

    function minTranslate() {
        return -this.snapGrid[0]
    }

    function maxTranslate() {
        return -this.snapGrid[this.snapGrid.length - 1]
    }

    function translateTo(translate, speed, runCallbacks, translateBounds, internal) {
        var obj;
        if (translate === void 0) translate = 0;
        if (speed === void 0) speed = this.params.speed;
        if (runCallbacks === void 0) runCallbacks = true;
        if (translateBounds === void 0) translateBounds = true;
        var swiper = this;
        var params = swiper.params;
        var wrapperEl = swiper.wrapperEl;
        if (swiper.animating && params.preventInteractionOnTransition) {
            return false
        }
        var minTranslate = swiper.minTranslate();
        var maxTranslate = swiper.maxTranslate();
        var newTranslate;
        if (translateBounds && translate > minTranslate) {
            newTranslate = minTranslate
        } else if (translateBounds && translate < maxTranslate) {
            newTranslate = maxTranslate
        } else {
            newTranslate = translate
        }
        swiper.updateProgress(newTranslate);
        if (params.cssMode) {
            var isH = swiper.isHorizontal();
            if (speed === 0) {
                wrapperEl[isH ? "scrollLeft" : "scrollTop"] = -newTranslate
            } else {
                if (wrapperEl.scrollTo) {
                    wrapperEl.scrollTo((obj = {}, obj[isH ? "left" : "top"] = -newTranslate, obj.behavior = "smooth", obj))
                } else {
                    wrapperEl[isH ? "scrollLeft" : "scrollTop"] = -newTranslate
                }
            }
            return true
        }
        if (speed === 0) {
            swiper.setTransition(0);
            swiper.setTranslate(newTranslate);
            if (runCallbacks) {
                swiper.emit("beforeTransitionStart", speed, internal);
                swiper.emit("transitionEnd")
            }
        } else {
            swiper.setTransition(speed);
            swiper.setTranslate(newTranslate);
            if (runCallbacks) {
                swiper.emit("beforeTransitionStart", speed, internal);
                swiper.emit("transitionStart")
            }
            if (!swiper.animating) {
                swiper.animating = true;
                if (!swiper.onTranslateToWrapperTransitionEnd) {
                    swiper.onTranslateToWrapperTransitionEnd = function transitionEnd(e) {
                        if (!swiper || swiper.destroyed) {
                            return
                        }
                        if (e.target !== this) {
                            return
                        }
                        swiper.$wrapperEl[0].removeEventListener("transitionend", swiper.onTranslateToWrapperTransitionEnd);
                        swiper.$wrapperEl[0].removeEventListener("webkitTransitionEnd", swiper.onTranslateToWrapperTransitionEnd);
                        swiper.onTranslateToWrapperTransitionEnd = null;
                        delete swiper.onTranslateToWrapperTransitionEnd;
                        if (runCallbacks) {
                            swiper.emit("transitionEnd")
                        }
                    }
                }
                swiper.$wrapperEl[0].addEventListener("transitionend", swiper.onTranslateToWrapperTransitionEnd);
                swiper.$wrapperEl[0].addEventListener("webkitTransitionEnd", swiper.onTranslateToWrapperTransitionEnd)
            }
        }
        return true
    }

    var translate = {
        getTranslate: getTranslate,
        setTranslate: setTranslate,
        minTranslate: minTranslate,
        maxTranslate: maxTranslate,
        translateTo: translateTo
    };

    function setTransition(duration, byController) {
        var swiper = this;
        if (!swiper.params.cssMode) {
            swiper.$wrapperEl.transition(duration)
        }
        swiper.emit("setTransition", duration, byController)
    }

    function transitionStart(runCallbacks, direction) {
        if (runCallbacks === void 0) runCallbacks = true;
        var swiper = this;
        var activeIndex = swiper.activeIndex;
        var params = swiper.params;
        var previousIndex = swiper.previousIndex;
        if (params.cssMode) {
            return
        }
        if (params.autoHeight) {
            swiper.updateAutoHeight()
        }
        var dir = direction;
        if (!dir) {
            if (activeIndex > previousIndex) {
                dir = "next"
            } else if (activeIndex < previousIndex) {
                dir = "prev"
            } else {
                dir = "reset"
            }
        }
        swiper.emit("transitionStart");
        if (runCallbacks && activeIndex !== previousIndex) {
            if (dir === "reset") {
                swiper.emit("slideResetTransitionStart");
                return
            }
            swiper.emit("slideChangeTransitionStart");
            if (dir === "next") {
                swiper.emit("slideNextTransitionStart")
            } else {
                swiper.emit("slidePrevTransitionStart")
            }
        }
    }

    function transitionEnd$1(runCallbacks, direction) {
        if (runCallbacks === void 0) runCallbacks = true;
        var swiper = this;
        var activeIndex = swiper.activeIndex;
        var previousIndex = swiper.previousIndex;
        var params = swiper.params;
        swiper.animating = false;
        if (params.cssMode) {
            return
        }
        swiper.setTransition(0);
        var dir = direction;
        if (!dir) {
            if (activeIndex > previousIndex) {
                dir = "next"
            } else if (activeIndex < previousIndex) {
                dir = "prev"
            } else {
                dir = "reset"
            }
        }
        swiper.emit("transitionEnd");
        if (runCallbacks && activeIndex !== previousIndex) {
            if (dir === "reset") {
                swiper.emit("slideResetTransitionEnd");
                return
            }
            swiper.emit("slideChangeTransitionEnd");
            if (dir === "next") {
                swiper.emit("slideNextTransitionEnd")
            } else {
                swiper.emit("slidePrevTransitionEnd")
            }
        }
    }

    var transition$1 = {setTransition: setTransition, transitionStart: transitionStart, transitionEnd: transitionEnd$1};

    function slideTo(index, speed, runCallbacks, internal) {
        var obj;
        if (index === void 0) index = 0;
        if (speed === void 0) speed = this.params.speed;
        if (runCallbacks === void 0) runCallbacks = true;
        var swiper = this;
        var slideIndex = index;
        if (slideIndex < 0) {
            slideIndex = 0
        }
        var params = swiper.params;
        var snapGrid = swiper.snapGrid;
        var slidesGrid = swiper.slidesGrid;
        var previousIndex = swiper.previousIndex;
        var activeIndex = swiper.activeIndex;
        var rtl = swiper.rtlTranslate;
        var wrapperEl = swiper.wrapperEl;
        if (swiper.animating && params.preventInteractionOnTransition) {
            return false
        }
        var skip = Math.min(swiper.params.slidesPerGroupSkip, slideIndex);
        var snapIndex = skip + Math.floor((slideIndex - skip) / swiper.params.slidesPerGroup);
        if (snapIndex >= snapGrid.length) {
            snapIndex = snapGrid.length - 1
        }
        if ((activeIndex || params.initialSlide || 0) === (previousIndex || 0) && runCallbacks) {
            swiper.emit("beforeSlideChangeStart")
        }
        var translate = -snapGrid[snapIndex];
        swiper.updateProgress(translate);
        if (params.normalizeSlideIndex) {
            for (var i = 0; i < slidesGrid.length; i += 1) {
                if (-Math.floor(translate * 100) >= Math.floor(slidesGrid[i] * 100)) {
                    slideIndex = i
                }
            }
        }
        if (swiper.initialized && slideIndex !== activeIndex) {
            if (!swiper.allowSlideNext && translate < swiper.translate && translate < swiper.minTranslate()) {
                return false
            }
            if (!swiper.allowSlidePrev && translate > swiper.translate && translate > swiper.maxTranslate()) {
                if ((activeIndex || 0) !== slideIndex) {
                    return false
                }
            }
        }
        var direction;
        if (slideIndex > activeIndex) {
            direction = "next"
        } else if (slideIndex < activeIndex) {
            direction = "prev"
        } else {
            direction = "reset"
        }
        if (rtl && -translate === swiper.translate || !rtl && translate === swiper.translate) {
            swiper.updateActiveIndex(slideIndex);
            if (params.autoHeight) {
                swiper.updateAutoHeight()
            }
            swiper.updateSlidesClasses();
            if (params.effect !== "slide") {
                swiper.setTranslate(translate)
            }
            if (direction !== "reset") {
                swiper.transitionStart(runCallbacks, direction);
                swiper.transitionEnd(runCallbacks, direction)
            }
            return false
        }
        if (params.cssMode) {
            var isH = swiper.isHorizontal();
            var t = -translate;
            if (rtl) {
                t = wrapperEl.scrollWidth - wrapperEl.offsetWidth - t
            }
            if (speed === 0) {
                wrapperEl[isH ? "scrollLeft" : "scrollTop"] = t
            } else {
                if (wrapperEl.scrollTo) {
                    wrapperEl.scrollTo((obj = {}, obj[isH ? "left" : "top"] = t, obj.behavior = "smooth", obj))
                } else {
                    wrapperEl[isH ? "scrollLeft" : "scrollTop"] = t
                }
            }
            return true
        }
        if (speed === 0) {
            swiper.setTransition(0);
            swiper.setTranslate(translate);
            swiper.updateActiveIndex(slideIndex);
            swiper.updateSlidesClasses();
            swiper.emit("beforeTransitionStart", speed, internal);
            swiper.transitionStart(runCallbacks, direction);
            swiper.transitionEnd(runCallbacks, direction)
        } else {
            swiper.setTransition(speed);
            swiper.setTranslate(translate);
            swiper.updateActiveIndex(slideIndex);
            swiper.updateSlidesClasses();
            swiper.emit("beforeTransitionStart", speed, internal);
            swiper.transitionStart(runCallbacks, direction);
            if (!swiper.animating) {
                swiper.animating = true;
                if (!swiper.onSlideToWrapperTransitionEnd) {
                    swiper.onSlideToWrapperTransitionEnd = function transitionEnd(e) {
                        if (!swiper || swiper.destroyed) {
                            return
                        }
                        if (e.target !== this) {
                            return
                        }
                        swiper.$wrapperEl[0].removeEventListener("transitionend", swiper.onSlideToWrapperTransitionEnd);
                        swiper.$wrapperEl[0].removeEventListener("webkitTransitionEnd", swiper.onSlideToWrapperTransitionEnd);
                        swiper.onSlideToWrapperTransitionEnd = null;
                        delete swiper.onSlideToWrapperTransitionEnd;
                        swiper.transitionEnd(runCallbacks, direction)
                    }
                }
                swiper.$wrapperEl[0].addEventListener("transitionend", swiper.onSlideToWrapperTransitionEnd);
                swiper.$wrapperEl[0].addEventListener("webkitTransitionEnd", swiper.onSlideToWrapperTransitionEnd)
            }
        }
        return true
    }

    function slideToLoop(index, speed, runCallbacks, internal) {
        if (index === void 0) index = 0;
        if (speed === void 0) speed = this.params.speed;
        if (runCallbacks === void 0) runCallbacks = true;
        var swiper = this;
        var newIndex = index;
        if (swiper.params.loop) {
            newIndex += swiper.loopedSlides
        }
        return swiper.slideTo(newIndex, speed, runCallbacks, internal)
    }

    function slideNext(speed, runCallbacks, internal) {
        if (speed === void 0) speed = this.params.speed;
        if (runCallbacks === void 0) runCallbacks = true;
        var swiper = this;
        var params = swiper.params;
        var animating = swiper.animating;
        var increment = swiper.activeIndex < params.slidesPerGroupSkip ? 1 : params.slidesPerGroup;
        if (params.loop) {
            if (animating) {
                return false
            }
            swiper.loopFix();
            swiper._clientLeft = swiper.$wrapperEl[0].clientLeft
        }
        return swiper.slideTo(swiper.activeIndex + increment, speed, runCallbacks, internal)
    }

    function slidePrev(speed, runCallbacks, internal) {
        if (speed === void 0) speed = this.params.speed;
        if (runCallbacks === void 0) runCallbacks = true;
        var swiper = this;
        var params = swiper.params;
        var animating = swiper.animating;
        var snapGrid = swiper.snapGrid;
        var slidesGrid = swiper.slidesGrid;
        var rtlTranslate = swiper.rtlTranslate;
        if (params.loop) {
            if (animating) {
                return false
            }
            swiper.loopFix();
            swiper._clientLeft = swiper.$wrapperEl[0].clientLeft
        }
        var translate = rtlTranslate ? swiper.translate : -swiper.translate;

        function normalize(val) {
            if (val < 0) {
                return -Math.floor(Math.abs(val))
            }
            return Math.floor(val)
        }

        var normalizedTranslate = normalize(translate);
        var normalizedSnapGrid = snapGrid.map(function (val) {
            return normalize(val)
        });
        var normalizedSlidesGrid = slidesGrid.map(function (val) {
            return normalize(val)
        });
        var currentSnap = snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate)];
        var prevSnap = snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate) - 1];
        if (typeof prevSnap === "undefined" && params.cssMode) {
            snapGrid.forEach(function (snap) {
                if (!prevSnap && normalizedTranslate >= snap) {
                    prevSnap = snap
                }
            })
        }
        var prevIndex;
        if (typeof prevSnap !== "undefined") {
            prevIndex = slidesGrid.indexOf(prevSnap);
            if (prevIndex < 0) {
                prevIndex = swiper.activeIndex - 1
            }
        }
        return swiper.slideTo(prevIndex, speed, runCallbacks, internal)
    }

    function slideReset(speed, runCallbacks, internal) {
        if (speed === void 0) speed = this.params.speed;
        if (runCallbacks === void 0) runCallbacks = true;
        var swiper = this;
        return swiper.slideTo(swiper.activeIndex, speed, runCallbacks, internal)
    }

    function slideToClosest(speed, runCallbacks, internal, threshold) {
        if (speed === void 0) speed = this.params.speed;
        if (runCallbacks === void 0) runCallbacks = true;
        if (threshold === void 0) threshold = .5;
        var swiper = this;
        var index = swiper.activeIndex;
        var skip = Math.min(swiper.params.slidesPerGroupSkip, index);
        var snapIndex = skip + Math.floor((index - skip) / swiper.params.slidesPerGroup);
        var translate = swiper.rtlTranslate ? swiper.translate : -swiper.translate;
        if (translate >= swiper.snapGrid[snapIndex]) {
            var currentSnap = swiper.snapGrid[snapIndex];
            var nextSnap = swiper.snapGrid[snapIndex + 1];
            if (translate - currentSnap > (nextSnap - currentSnap) * threshold) {
                index += swiper.params.slidesPerGroup
            }
        } else {
            var prevSnap = swiper.snapGrid[snapIndex - 1];
            var currentSnap$1 = swiper.snapGrid[snapIndex];
            if (translate - prevSnap <= (currentSnap$1 - prevSnap) * threshold) {
                index -= swiper.params.slidesPerGroup
            }
        }
        index = Math.max(index, 0);
        index = Math.min(index, swiper.slidesGrid.length - 1);
        return swiper.slideTo(index, speed, runCallbacks, internal)
    }

    function slideToClickedSlide() {
        var swiper = this;
        var params = swiper.params;
        var $wrapperEl = swiper.$wrapperEl;
        var slidesPerView = params.slidesPerView === "auto" ? swiper.slidesPerViewDynamic() : params.slidesPerView;
        var slideToIndex = swiper.clickedIndex;
        var realIndex;
        if (params.loop) {
            if (swiper.animating) {
                return
            }
            realIndex = parseInt($(swiper.clickedSlide).attr("data-swiper-slide-index"), 10);
            if (params.centeredSlides) {
                if (slideToIndex < swiper.loopedSlides - slidesPerView / 2 || slideToIndex > swiper.slides.length - swiper.loopedSlides + slidesPerView / 2) {
                    swiper.loopFix();
                    slideToIndex = $wrapperEl.children("." + params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.' + params.slideDuplicateClass + ")").eq(0).index();
                    Utils.nextTick(function () {
                        swiper.slideTo(slideToIndex)
                    })
                } else {
                    swiper.slideTo(slideToIndex)
                }
            } else if (slideToIndex > swiper.slides.length - slidesPerView) {
                swiper.loopFix();
                slideToIndex = $wrapperEl.children("." + params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.' + params.slideDuplicateClass + ")").eq(0).index();
                Utils.nextTick(function () {
                    swiper.slideTo(slideToIndex)
                })
            } else {
                swiper.slideTo(slideToIndex)
            }
        } else {
            swiper.slideTo(slideToIndex)
        }
    }

    var slide = {
        slideTo: slideTo,
        slideToLoop: slideToLoop,
        slideNext: slideNext,
        slidePrev: slidePrev,
        slideReset: slideReset,
        slideToClosest: slideToClosest,
        slideToClickedSlide: slideToClickedSlide
    };

    function loopCreate() {
        var swiper = this;
        var params = swiper.params;
        var $wrapperEl = swiper.$wrapperEl;
        $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass).remove();
        var slides = $wrapperEl.children("." + params.slideClass);
        if (params.loopFillGroupWithBlank) {
            var blankSlidesNum = params.slidesPerGroup - slides.length % params.slidesPerGroup;
            if (blankSlidesNum !== params.slidesPerGroup) {
                for (var i = 0; i < blankSlidesNum; i += 1) {
                    var blankNode = $(doc.createElement("div")).addClass(params.slideClass + " " + params.slideBlankClass);
                    $wrapperEl.append(blankNode)
                }
                slides = $wrapperEl.children("." + params.slideClass)
            }
        }
        if (params.slidesPerView === "auto" && !params.loopedSlides) {
            params.loopedSlides = slides.length
        }
        swiper.loopedSlides = Math.ceil(parseFloat(params.loopedSlides || params.slidesPerView, 10));
        swiper.loopedSlides += params.loopAdditionalSlides;
        if (swiper.loopedSlides > slides.length) {
            swiper.loopedSlides = slides.length
        }
        var prependSlides = [];
        var appendSlides = [];
        slides.each(function (index, el) {
            var slide = $(el);
            if (index < swiper.loopedSlides) {
                appendSlides.push(el)
            }
            if (index < slides.length && index >= slides.length - swiper.loopedSlides) {
                prependSlides.push(el)
            }
            slide.attr("data-swiper-slide-index", index)
        });
        for (var i$1 = 0; i$1 < appendSlides.length; i$1 += 1) {
            $wrapperEl.append($(appendSlides[i$1].cloneNode(true)).addClass(params.slideDuplicateClass))
        }
        for (var i$2 = prependSlides.length - 1; i$2 >= 0; i$2 -= 1) {
            $wrapperEl.prepend($(prependSlides[i$2].cloneNode(true)).addClass(params.slideDuplicateClass))
        }
    }

    function loopFix() {
        var swiper = this;
        swiper.emit("beforeLoopFix");
        var activeIndex = swiper.activeIndex;
        var slides = swiper.slides;
        var loopedSlides = swiper.loopedSlides;
        var allowSlidePrev = swiper.allowSlidePrev;
        var allowSlideNext = swiper.allowSlideNext;
        var snapGrid = swiper.snapGrid;
        var rtl = swiper.rtlTranslate;
        var newIndex;
        swiper.allowSlidePrev = true;
        swiper.allowSlideNext = true;
        var snapTranslate = -snapGrid[activeIndex];
        var diff = snapTranslate - swiper.getTranslate();
        if (activeIndex < loopedSlides) {
            newIndex = slides.length - loopedSlides * 3 + activeIndex;
            newIndex += loopedSlides;
            var slideChanged = swiper.slideTo(newIndex, 0, false, true);
            if (slideChanged && diff !== 0) {
                swiper.setTranslate((rtl ? -swiper.translate : swiper.translate) - diff)
            }
        } else if (activeIndex >= slides.length - loopedSlides) {
            newIndex = -slides.length + activeIndex + loopedSlides;
            newIndex += loopedSlides;
            var slideChanged$1 = swiper.slideTo(newIndex, 0, false, true);
            if (slideChanged$1 && diff !== 0) {
                swiper.setTranslate((rtl ? -swiper.translate : swiper.translate) - diff)
            }
        }
        swiper.allowSlidePrev = allowSlidePrev;
        swiper.allowSlideNext = allowSlideNext;
        swiper.emit("loopFix")
    }

    function loopDestroy() {
        var swiper = this;
        var $wrapperEl = swiper.$wrapperEl;
        var params = swiper.params;
        var slides = swiper.slides;
        $wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + ",." + params.slideClass + "." + params.slideBlankClass).remove();
        slides.removeAttr("data-swiper-slide-index")
    }

    var loop = {loopCreate: loopCreate, loopFix: loopFix, loopDestroy: loopDestroy};

    function setGrabCursor(moving) {
        var swiper = this;
        if (Support.touch || !swiper.params.simulateTouch || swiper.params.watchOverflow && swiper.isLocked || swiper.params.cssMode) {
            return
        }
        var el = swiper.el;
        el.style.cursor = "move";
        el.style.cursor = moving ? "-webkit-grabbing" : "-webkit-grab";
        el.style.cursor = moving ? "-moz-grabbin" : "-moz-grab";
        el.style.cursor = moving ? "grabbing" : "grab"
    }

    function unsetGrabCursor() {
        var swiper = this;
        if (Support.touch || swiper.params.watchOverflow && swiper.isLocked || swiper.params.cssMode) {
            return
        }
        swiper.el.style.cursor = ""
    }

    var grabCursor = {setGrabCursor: setGrabCursor, unsetGrabCursor: unsetGrabCursor};

    function appendSlide(slides) {
        var swiper = this;
        var $wrapperEl = swiper.$wrapperEl;
        var params = swiper.params;
        if (params.loop) {
            swiper.loopDestroy()
        }
        if (typeof slides === "object" && "length" in slides) {
            for (var i = 0; i < slides.length; i += 1) {
                if (slides[i]) {
                    $wrapperEl.append(slides[i])
                }
            }
        } else {
            $wrapperEl.append(slides)
        }
        if (params.loop) {
            swiper.loopCreate()
        }
        if (!(params.observer && Support.observer)) {
            swiper.update()
        }
    }

    function prependSlide(slides) {
        var swiper = this;
        var params = swiper.params;
        var $wrapperEl = swiper.$wrapperEl;
        var activeIndex = swiper.activeIndex;
        if (params.loop) {
            swiper.loopDestroy()
        }
        var newActiveIndex = activeIndex + 1;
        if (typeof slides === "object" && "length" in slides) {
            for (var i = 0; i < slides.length; i += 1) {
                if (slides[i]) {
                    $wrapperEl.prepend(slides[i])
                }
            }
            newActiveIndex = activeIndex + slides.length
        } else {
            $wrapperEl.prepend(slides)
        }
        if (params.loop) {
            swiper.loopCreate()
        }
        if (!(params.observer && Support.observer)) {
            swiper.update()
        }
        swiper.slideTo(newActiveIndex, 0, false)
    }

    function addSlide(index, slides) {
        var swiper = this;
        var $wrapperEl = swiper.$wrapperEl;
        var params = swiper.params;
        var activeIndex = swiper.activeIndex;
        var activeIndexBuffer = activeIndex;
        if (params.loop) {
            activeIndexBuffer -= swiper.loopedSlides;
            swiper.loopDestroy();
            swiper.slides = $wrapperEl.children("." + params.slideClass)
        }
        var baseLength = swiper.slides.length;
        if (index <= 0) {
            swiper.prependSlide(slides);
            return
        }
        if (index >= baseLength) {
            swiper.appendSlide(slides);
            return
        }
        var newActiveIndex = activeIndexBuffer > index ? activeIndexBuffer + 1 : activeIndexBuffer;
        var slidesBuffer = [];
        for (var i = baseLength - 1; i >= index; i -= 1) {
            var currentSlide = swiper.slides.eq(i);
            currentSlide.remove();
            slidesBuffer.unshift(currentSlide)
        }
        if (typeof slides === "object" && "length" in slides) {
            for (var i$1 = 0; i$1 < slides.length; i$1 += 1) {
                if (slides[i$1]) {
                    $wrapperEl.append(slides[i$1])
                }
            }
            newActiveIndex = activeIndexBuffer > index ? activeIndexBuffer + slides.length : activeIndexBuffer
        } else {
            $wrapperEl.append(slides)
        }
        for (var i$2 = 0; i$2 < slidesBuffer.length; i$2 += 1) {
            $wrapperEl.append(slidesBuffer[i$2])
        }
        if (params.loop) {
            swiper.loopCreate()
        }
        if (!(params.observer && Support.observer)) {
            swiper.update()
        }
        if (params.loop) {
            swiper.slideTo(newActiveIndex + swiper.loopedSlides, 0, false)
        } else {
            swiper.slideTo(newActiveIndex, 0, false)
        }
    }

    function removeSlide(slidesIndexes) {
        var swiper = this;
        var params = swiper.params;
        var $wrapperEl = swiper.$wrapperEl;
        var activeIndex = swiper.activeIndex;
        var activeIndexBuffer = activeIndex;
        if (params.loop) {
            activeIndexBuffer -= swiper.loopedSlides;
            swiper.loopDestroy();
            swiper.slides = $wrapperEl.children("." + params.slideClass)
        }
        var newActiveIndex = activeIndexBuffer;
        var indexToRemove;
        if (typeof slidesIndexes === "object" && "length" in slidesIndexes) {
            for (var i = 0; i < slidesIndexes.length; i += 1) {
                indexToRemove = slidesIndexes[i];
                if (swiper.slides[indexToRemove]) {
                    swiper.slides.eq(indexToRemove).remove()
                }
                if (indexToRemove < newActiveIndex) {
                    newActiveIndex -= 1
                }
            }
            newActiveIndex = Math.max(newActiveIndex, 0)
        } else {
            indexToRemove = slidesIndexes;
            if (swiper.slides[indexToRemove]) {
                swiper.slides.eq(indexToRemove).remove()
            }
            if (indexToRemove < newActiveIndex) {
                newActiveIndex -= 1
            }
            newActiveIndex = Math.max(newActiveIndex, 0)
        }
        if (params.loop) {
            swiper.loopCreate()
        }
        if (!(params.observer && Support.observer)) {
            swiper.update()
        }
        if (params.loop) {
            swiper.slideTo(newActiveIndex + swiper.loopedSlides, 0, false)
        } else {
            swiper.slideTo(newActiveIndex, 0, false)
        }
    }

    function removeAllSlides() {
        var swiper = this;
        var slidesIndexes = [];
        for (var i = 0; i < swiper.slides.length; i += 1) {
            slidesIndexes.push(i)
        }
        swiper.removeSlide(slidesIndexes)
    }

    var manipulation = {
        appendSlide: appendSlide,
        prependSlide: prependSlide,
        addSlide: addSlide,
        removeSlide: removeSlide,
        removeAllSlides: removeAllSlides
    };
    var Device = function Device() {
        var platform = win.navigator.platform;
        var ua = win.navigator.userAgent;
        var device = {
            ios: false,
            android: false,
            androidChrome: false,
            desktop: false,
            iphone: false,
            ipod: false,
            ipad: false,
            edge: false,
            ie: false,
            firefox: false,
            macos: false,
            windows: false,
            cordova: !!(win.cordova || win.phonegap),
            phonegap: !!(win.cordova || win.phonegap),
            electron: false
        };
        var screenWidth = win.screen.width;
        var screenHeight = win.screen.height;
        var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
        var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
        var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
        var iphone = !ipad && ua.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
        var ie = ua.indexOf("MSIE ") >= 0 || ua.indexOf("Trident/") >= 0;
        var edge = ua.indexOf("Edge/") >= 0;
        var firefox = ua.indexOf("Gecko/") >= 0 && ua.indexOf("Firefox/") >= 0;
        var windows = platform === "Win32";
        var electron = ua.toLowerCase().indexOf("electron") >= 0;
        var macos = platform === "MacIntel";
        if (!ipad && macos && Support.touch && (screenWidth === 1024 && screenHeight === 1366 || screenWidth === 834 && screenHeight === 1194 || screenWidth === 834 && screenHeight === 1112 || screenWidth === 768 && screenHeight === 1024)) {
            ipad = ua.match(/(Version)\/([\d.]+)/);
            macos = false
        }
        device.ie = ie;
        device.edge = edge;
        device.firefox = firefox;
        if (android && !windows) {
            device.os = "android";
            device.osVersion = android[2];
            device.android = true;
            device.androidChrome = ua.toLowerCase().indexOf("chrome") >= 0
        }
        if (ipad || iphone || ipod) {
            device.os = "ios";
            device.ios = true
        }
        if (iphone && !ipod) {
            device.osVersion = iphone[2].replace(/_/g, ".");
            device.iphone = true
        }
        if (ipad) {
            device.osVersion = ipad[2].replace(/_/g, ".");
            device.ipad = true
        }
        if (ipod) {
            device.osVersion = ipod[3] ? ipod[3].replace(/_/g, ".") : null;
            device.ipod = true
        }
        if (device.ios && device.osVersion && ua.indexOf("Version/") >= 0) {
            if (device.osVersion.split(".")[0] === "10") {
                device.osVersion = ua.toLowerCase().split("version/")[1].split(" ")[0]
            }
        }
        device.webView = !!((iphone || ipad || ipod) && (ua.match(/.*AppleWebKit(?!.*Safari)/i) || win.navigator.standalone)) || win.matchMedia && win.matchMedia("(display-mode: standalone)").matches;
        device.webview = device.webView;
        device.standalone = device.webView;
        device.desktop = !(device.ios || device.android) || electron;
        if (device.desktop) {
            device.electron = electron;
            device.macos = macos;
            device.windows = windows;
            if (device.macos) {
                device.os = "macos"
            }
            if (device.windows) {
                device.os = "windows"
            }
        }
        device.pixelRatio = win.devicePixelRatio || 1;
        return device
    }();

    function onTouchStart(event) {
        var swiper = this;
        var data = swiper.touchEventsData;
        var params = swiper.params;
        var touches = swiper.touches;
        if (swiper.animating && params.preventInteractionOnTransition) {
            return
        }
        var e = event;
        if (e.originalEvent) {
            e = e.originalEvent
        }
        var $targetEl = $(e.target);
        if (params.touchEventsTarget === "wrapper") {
            if (!$targetEl.closest(swiper.wrapperEl).length) {
                return
            }
        }
        data.isTouchEvent = e.type === "touchstart";
        if (!data.isTouchEvent && "which" in e && e.which === 3) {
            return
        }
        if (!data.isTouchEvent && "button" in e && e.button > 0) {
            return
        }
        if (data.isTouched && data.isMoved) {
            return
        }
        if (params.noSwiping && $targetEl.closest(params.noSwipingSelector ? params.noSwipingSelector : "." + params.noSwipingClass)[0]) {
            swiper.allowClick = true;
            return
        }
        if (params.swipeHandler) {
            if (!$targetEl.closest(params.swipeHandler)[0]) {
                return
            }
        }
        touches.currentX = e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX;
        touches.currentY = e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY;
        var startX = touches.currentX;
        var startY = touches.currentY;
        var edgeSwipeDetection = params.edgeSwipeDetection || params.iOSEdgeSwipeDetection;
        var edgeSwipeThreshold = params.edgeSwipeThreshold || params.iOSEdgeSwipeThreshold;
        if (edgeSwipeDetection && (startX <= edgeSwipeThreshold || startX >= win.screen.width - edgeSwipeThreshold)) {
            return
        }
        Utils.extend(data, {
            isTouched: true,
            isMoved: false,
            allowTouchCallbacks: true,
            isScrolling: undefined,
            startMoving: undefined
        });
        touches.startX = startX;
        touches.startY = startY;
        data.touchStartTime = Utils.now();
        swiper.allowClick = true;
        swiper.updateSize();
        swiper.swipeDirection = undefined;
        if (params.threshold > 0) {
            data.allowThresholdMove = false
        }
        if (e.type !== "touchstart") {
            var preventDefault = true;
            if ($targetEl.is(data.formElements)) {
                preventDefault = false
            }
            if (doc.activeElement && $(doc.activeElement).is(data.formElements) && doc.activeElement !== $targetEl[0]) {
                doc.activeElement.blur()
            }
            var shouldPreventDefault = preventDefault && swiper.allowTouchMove && params.touchStartPreventDefault;
            if (params.touchStartForcePreventDefault || shouldPreventDefault) {
                e.preventDefault()
            }
        }
        swiper.emit("touchStart", e)
    }

    function onTouchMove(event) {
        var swiper = this;
        var data = swiper.touchEventsData;
        var params = swiper.params;
        var touches = swiper.touches;
        var rtl = swiper.rtlTranslate;
        var e = event;
        if (e.originalEvent) {
            e = e.originalEvent
        }
        if (!data.isTouched) {
            if (data.startMoving && data.isScrolling) {
                swiper.emit("touchMoveOpposite", e)
            }
            return
        }
        if (data.isTouchEvent && e.type !== "touchmove") {
            return
        }
        var targetTouch = e.type === "touchmove" && e.targetTouches && (e.targetTouches[0] || e.changedTouches[0]);
        var pageX = e.type === "touchmove" ? targetTouch.pageX : e.pageX;
        var pageY = e.type === "touchmove" ? targetTouch.pageY : e.pageY;
        if (e.preventedByNestedSwiper) {
            touches.startX = pageX;
            touches.startY = pageY;
            return
        }
        if (!swiper.allowTouchMove) {
            swiper.allowClick = false;
            if (data.isTouched) {
                Utils.extend(touches, {startX: pageX, startY: pageY, currentX: pageX, currentY: pageY});
                data.touchStartTime = Utils.now()
            }
            return
        }
        if (data.isTouchEvent && params.touchReleaseOnEdges && !params.loop) {
            if (swiper.isVertical()) {
                if (pageY < touches.startY && swiper.translate <= swiper.maxTranslate() || pageY > touches.startY && swiper.translate >= swiper.minTranslate()) {
                    data.isTouched = false;
                    data.isMoved = false;
                    return
                }
            } else if (pageX < touches.startX && swiper.translate <= swiper.maxTranslate() || pageX > touches.startX && swiper.translate >= swiper.minTranslate()) {
                return
            }
        }
        if (data.isTouchEvent && doc.activeElement) {
            if (e.target === doc.activeElement && $(e.target).is(data.formElements)) {
                data.isMoved = true;
                swiper.allowClick = false;
                return
            }
        }
        if (data.allowTouchCallbacks) {
            swiper.emit("touchMove", e)
        }
        if (e.targetTouches && e.targetTouches.length > 1) {
            return
        }
        touches.currentX = pageX;
        touches.currentY = pageY;
        var diffX = touches.currentX - touches.startX;
        var diffY = touches.currentY - touches.startY;
        if (swiper.params.threshold && Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2)) < swiper.params.threshold) {
            return
        }
        if (typeof data.isScrolling === "undefined") {
            var touchAngle;
            if (swiper.isHorizontal() && touches.currentY === touches.startY || swiper.isVertical() && touches.currentX === touches.startX) {
                data.isScrolling = false
            } else {
                if (diffX * diffX + diffY * diffY >= 25) {
                    touchAngle = Math.atan2(Math.abs(diffY), Math.abs(diffX)) * 180 / Math.PI;
                    data.isScrolling = swiper.isHorizontal() ? touchAngle > params.touchAngle : 90 - touchAngle > params.touchAngle
                }
            }
        }
        if (data.isScrolling) {
            swiper.emit("touchMoveOpposite", e)
        }
        if (typeof data.startMoving === "undefined") {
            if (touches.currentX !== touches.startX || touches.currentY !== touches.startY) {
                data.startMoving = true
            }
        }
        if (data.isScrolling) {
            data.isTouched = false;
            return
        }
        if (!data.startMoving) {
            return
        }
        swiper.allowClick = false;
        if (!params.cssMode && e.cancelable) {
            e.preventDefault()
        }
        if (params.touchMoveStopPropagation && !params.nested) {
            e.stopPropagation()
        }
        if (!data.isMoved) {
            if (params.loop) {
                swiper.loopFix()
            }
            data.startTranslate = swiper.getTranslate();
            swiper.setTransition(0);
            if (swiper.animating) {
                swiper.$wrapperEl.trigger("webkitTransitionEnd transitionend")
            }
            data.allowMomentumBounce = false;
            if (params.grabCursor && (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)) {
                swiper.setGrabCursor(true)
            }
            swiper.emit("sliderFirstMove", e)
        }
        swiper.emit("sliderMove", e);
        data.isMoved = true;
        var diff = swiper.isHorizontal() ? diffX : diffY;
        touches.diff = diff;
        diff *= params.touchRatio;
        if (rtl) {
            diff = -diff
        }
        swiper.swipeDirection = diff > 0 ? "prev" : "next";
        data.currentTranslate = diff + data.startTranslate;
        var disableParentSwiper = true;
        var resistanceRatio = params.resistanceRatio;
        if (params.touchReleaseOnEdges) {
            resistanceRatio = 0
        }
        if (diff > 0 && data.currentTranslate > swiper.minTranslate()) {
            disableParentSwiper = false;
            if (params.resistance) {
                data.currentTranslate = swiper.minTranslate() - 1 + Math.pow(-swiper.minTranslate() + data.startTranslate + diff, resistanceRatio)
            }
        } else if (diff < 0 && data.currentTranslate < swiper.maxTranslate()) {
            disableParentSwiper = false;
            if (params.resistance) {
                data.currentTranslate = swiper.maxTranslate() + 1 - Math.pow(swiper.maxTranslate() - data.startTranslate - diff, resistanceRatio)
            }
        }
        if (disableParentSwiper) {
            e.preventedByNestedSwiper = true
        }
        if (!swiper.allowSlideNext && swiper.swipeDirection === "next" && data.currentTranslate < data.startTranslate) {
            data.currentTranslate = data.startTranslate
        }
        if (!swiper.allowSlidePrev && swiper.swipeDirection === "prev" && data.currentTranslate > data.startTranslate) {
            data.currentTranslate = data.startTranslate
        }
        if (params.threshold > 0) {
            if (Math.abs(diff) > params.threshold || data.allowThresholdMove) {
                if (!data.allowThresholdMove) {
                    data.allowThresholdMove = true;
                    touches.startX = touches.currentX;
                    touches.startY = touches.currentY;
                    data.currentTranslate = data.startTranslate;
                    touches.diff = swiper.isHorizontal() ? touches.currentX - touches.startX : touches.currentY - touches.startY;
                    return
                }
            } else {
                data.currentTranslate = data.startTranslate;
                return
            }
        }
        if (!params.followFinger || params.cssMode) {
            return
        }
        if (params.freeMode || params.watchSlidesProgress || params.watchSlidesVisibility) {
            swiper.updateActiveIndex();
            swiper.updateSlidesClasses()
        }
        if (params.freeMode) {
            if (data.velocities.length === 0) {
                data.velocities.push({
                    position: touches[swiper.isHorizontal() ? "startX" : "startY"],
                    time: data.touchStartTime
                })
            }
            data.velocities.push({
                position: touches[swiper.isHorizontal() ? "currentX" : "currentY"],
                time: Utils.now()
            })
        }
        swiper.updateProgress(data.currentTranslate);
        swiper.setTranslate(data.currentTranslate)
    }

    function onTouchEnd(event) {
        var swiper = this;
        var data = swiper.touchEventsData;
        var params = swiper.params;
        var touches = swiper.touches;
        var rtl = swiper.rtlTranslate;
        var $wrapperEl = swiper.$wrapperEl;
        var slidesGrid = swiper.slidesGrid;
        var snapGrid = swiper.snapGrid;
        var e = event;
        if (e.originalEvent) {
            e = e.originalEvent
        }
        if (data.allowTouchCallbacks) {
            swiper.emit("touchEnd", e)
        }
        data.allowTouchCallbacks = false;
        if (!data.isTouched) {
            if (data.isMoved && params.grabCursor) {
                swiper.setGrabCursor(false)
            }
            data.isMoved = false;
            data.startMoving = false;
            return
        }
        if (params.grabCursor && data.isMoved && data.isTouched && (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)) {
            swiper.setGrabCursor(false)
        }
        var touchEndTime = Utils.now();
        var timeDiff = touchEndTime - data.touchStartTime;
        if (swiper.allowClick) {
            swiper.updateClickedSlide(e);
            swiper.emit("tap click", e);
            if (timeDiff < 300 && touchEndTime - data.lastClickTime < 300) {
                swiper.emit("doubleTap doubleClick", e)
            }
        }
        data.lastClickTime = Utils.now();
        Utils.nextTick(function () {
            if (!swiper.destroyed) {
                swiper.allowClick = true
            }
        });
        if (!data.isTouched || !data.isMoved || !swiper.swipeDirection || touches.diff === 0 || data.currentTranslate === data.startTranslate) {
            data.isTouched = false;
            data.isMoved = false;
            data.startMoving = false;
            return
        }
        data.isTouched = false;
        data.isMoved = false;
        data.startMoving = false;
        var currentPos;
        if (params.followFinger) {
            currentPos = rtl ? swiper.translate : -swiper.translate
        } else {
            currentPos = -data.currentTranslate
        }
        if (params.cssMode) {
            return
        }
        if (params.freeMode) {
            if (currentPos < -swiper.minTranslate()) {
                swiper.slideTo(swiper.activeIndex);
                return
            }
            if (currentPos > -swiper.maxTranslate()) {
                if (swiper.slides.length < snapGrid.length) {
                    swiper.slideTo(snapGrid.length - 1)
                } else {
                    swiper.slideTo(swiper.slides.length - 1)
                }
                return
            }
            if (params.freeModeMomentum) {
                if (data.velocities.length > 1) {
                    var lastMoveEvent = data.velocities.pop();
                    var velocityEvent = data.velocities.pop();
                    var distance = lastMoveEvent.position - velocityEvent.position;
                    var time = lastMoveEvent.time - velocityEvent.time;
                    swiper.velocity = distance / time;
                    swiper.velocity /= 2;
                    if (Math.abs(swiper.velocity) < params.freeModeMinimumVelocity) {
                        swiper.velocity = 0
                    }
                    if (time > 150 || Utils.now() - lastMoveEvent.time > 300) {
                        swiper.velocity = 0
                    }
                } else {
                    swiper.velocity = 0
                }
                swiper.velocity *= params.freeModeMomentumVelocityRatio;
                data.velocities.length = 0;
                var momentumDuration = 1e3 * params.freeModeMomentumRatio;
                var momentumDistance = swiper.velocity * momentumDuration;
                var newPosition = swiper.translate + momentumDistance;
                if (rtl) {
                    newPosition = -newPosition
                }
                var doBounce = false;
                var afterBouncePosition;
                var bounceAmount = Math.abs(swiper.velocity) * 20 * params.freeModeMomentumBounceRatio;
                var needsLoopFix;
                if (newPosition < swiper.maxTranslate()) {
                    if (params.freeModeMomentumBounce) {
                        if (newPosition + swiper.maxTranslate() < -bounceAmount) {
                            newPosition = swiper.maxTranslate() - bounceAmount
                        }
                        afterBouncePosition = swiper.maxTranslate();
                        doBounce = true;
                        data.allowMomentumBounce = true
                    } else {
                        newPosition = swiper.maxTranslate()
                    }
                    if (params.loop && params.centeredSlides) {
                        needsLoopFix = true
                    }
                } else if (newPosition > swiper.minTranslate()) {
                    if (params.freeModeMomentumBounce) {
                        if (newPosition - swiper.minTranslate() > bounceAmount) {
                            newPosition = swiper.minTranslate() + bounceAmount
                        }
                        afterBouncePosition = swiper.minTranslate();
                        doBounce = true;
                        data.allowMomentumBounce = true
                    } else {
                        newPosition = swiper.minTranslate()
                    }
                    if (params.loop && params.centeredSlides) {
                        needsLoopFix = true
                    }
                } else if (params.freeModeSticky) {
                    var nextSlide;
                    for (var j = 0; j < snapGrid.length; j += 1) {
                        if (snapGrid[j] > -newPosition) {
                            nextSlide = j;
                            break
                        }
                    }
                    if (Math.abs(snapGrid[nextSlide] - newPosition) < Math.abs(snapGrid[nextSlide - 1] - newPosition) || swiper.swipeDirection === "next") {
                        newPosition = snapGrid[nextSlide]
                    } else {
                        newPosition = snapGrid[nextSlide - 1]
                    }
                    newPosition = -newPosition
                }
                if (needsLoopFix) {
                    swiper.once("transitionEnd", function () {
                        swiper.loopFix()
                    })
                }
                if (swiper.velocity !== 0) {
                    if (rtl) {
                        momentumDuration = Math.abs((-newPosition - swiper.translate) / swiper.velocity)
                    } else {
                        momentumDuration = Math.abs((newPosition - swiper.translate) / swiper.velocity)
                    }
                    if (params.freeModeSticky) {
                        var moveDistance = Math.abs((rtl ? -newPosition : newPosition) - swiper.translate);
                        var currentSlideSize = swiper.slidesSizesGrid[swiper.activeIndex];
                        if (moveDistance < currentSlideSize) {
                            momentumDuration = params.speed
                        } else if (moveDistance < 2 * currentSlideSize) {
                            momentumDuration = params.speed * 1.5
                        } else {
                            momentumDuration = params.speed * 2.5
                        }
                    }
                } else if (params.freeModeSticky) {
                    swiper.slideToClosest();
                    return
                }
                if (params.freeModeMomentumBounce && doBounce) {
                    swiper.updateProgress(afterBouncePosition);
                    swiper.setTransition(momentumDuration);
                    swiper.setTranslate(newPosition);
                    swiper.transitionStart(true, swiper.swipeDirection);
                    swiper.animating = true;
                    $wrapperEl.transitionEnd(function () {
                        if (!swiper || swiper.destroyed || !data.allowMomentumBounce) {
                            return
                        }
                        swiper.emit("momentumBounce");
                        swiper.setTransition(params.speed);
                        setTimeout(function () {
                            swiper.setTranslate(afterBouncePosition);
                            $wrapperEl.transitionEnd(function () {
                                if (!swiper || swiper.destroyed) {
                                    return
                                }
                                swiper.transitionEnd()
                            })
                        }, 0)
                    })
                } else if (swiper.velocity) {
                    swiper.updateProgress(newPosition);
                    swiper.setTransition(momentumDuration);
                    swiper.setTranslate(newPosition);
                    swiper.transitionStart(true, swiper.swipeDirection);
                    if (!swiper.animating) {
                        swiper.animating = true;
                        $wrapperEl.transitionEnd(function () {
                            if (!swiper || swiper.destroyed) {
                                return
                            }
                            swiper.transitionEnd()
                        })
                    }
                } else {
                    swiper.updateProgress(newPosition)
                }
                swiper.updateActiveIndex();
                swiper.updateSlidesClasses()
            } else if (params.freeModeSticky) {
                swiper.slideToClosest();
                return
            }
            if (!params.freeModeMomentum || timeDiff >= params.longSwipesMs) {
                swiper.updateProgress();
                swiper.updateActiveIndex();
                swiper.updateSlidesClasses()
            }
            return
        }
        var stopIndex = 0;
        var groupSize = swiper.slidesSizesGrid[0];
        for (var i = 0; i < slidesGrid.length; i += i < params.slidesPerGroupSkip ? 1 : params.slidesPerGroup) {
            var increment$1 = i < params.slidesPerGroupSkip - 1 ? 1 : params.slidesPerGroup;
            if (typeof slidesGrid[i + increment$1] !== "undefined") {
                if (currentPos >= slidesGrid[i] && currentPos < slidesGrid[i + increment$1]) {
                    stopIndex = i;
                    groupSize = slidesGrid[i + increment$1] - slidesGrid[i]
                }
            } else if (currentPos >= slidesGrid[i]) {
                stopIndex = i;
                groupSize = slidesGrid[slidesGrid.length - 1] - slidesGrid[slidesGrid.length - 2]
            }
        }
        var ratio = (currentPos - slidesGrid[stopIndex]) / groupSize;
        var increment = stopIndex < params.slidesPerGroupSkip - 1 ? 1 : params.slidesPerGroup;
        if (timeDiff > params.longSwipesMs) {
            if (!params.longSwipes) {
                swiper.slideTo(swiper.activeIndex);
                return
            }
            if (swiper.swipeDirection === "next") {
                if (ratio >= params.longSwipesRatio) {
                    swiper.slideTo(stopIndex + increment)
                } else {
                    swiper.slideTo(stopIndex)
                }
            }
            if (swiper.swipeDirection === "prev") {
                if (ratio > 1 - params.longSwipesRatio) {
                    swiper.slideTo(stopIndex + increment)
                } else {
                    swiper.slideTo(stopIndex)
                }
            }
        } else {
            if (!params.shortSwipes) {
                swiper.slideTo(swiper.activeIndex);
                return
            }
            var isNavButtonTarget = swiper.navigation && (e.target === swiper.navigation.nextEl || e.target === swiper.navigation.prevEl);
            if (!isNavButtonTarget) {
                if (swiper.swipeDirection === "next") {
                    swiper.slideTo(stopIndex + increment)
                }
                if (swiper.swipeDirection === "prev") {
                    swiper.slideTo(stopIndex)
                }
            } else if (e.target === swiper.navigation.nextEl) {
                swiper.slideTo(stopIndex + increment)
            } else {
                swiper.slideTo(stopIndex)
            }
        }
    }

    function onResize() {
        var swiper = this;
        var params = swiper.params;
        var el = swiper.el;
        if (el && el.offsetWidth === 0) {
            return
        }
        if (params.breakpoints) {
            swiper.setBreakpoint()
        }
        var allowSlideNext = swiper.allowSlideNext;
        var allowSlidePrev = swiper.allowSlidePrev;
        var snapGrid = swiper.snapGrid;
        swiper.allowSlideNext = true;
        swiper.allowSlidePrev = true;
        swiper.updateSize();
        swiper.updateSlides();
        swiper.updateSlidesClasses();
        if ((params.slidesPerView === "auto" || params.slidesPerView > 1) && swiper.isEnd && !swiper.isBeginning && !swiper.params.centeredSlides) {
            swiper.slideTo(swiper.slides.length - 1, 0, false, true)
        } else {
            swiper.slideTo(swiper.activeIndex, 0, false, true)
        }
        if (swiper.autoplay && swiper.autoplay.running && swiper.autoplay.paused) {
            swiper.autoplay.run()
        }
        swiper.allowSlidePrev = allowSlidePrev;
        swiper.allowSlideNext = allowSlideNext;
        if (swiper.params.watchOverflow && snapGrid !== swiper.snapGrid) {
            swiper.checkOverflow()
        }
    }

    function onClick(e) {
        var swiper = this;
        if (!swiper.allowClick) {
            if (swiper.params.preventClicks) {
                e.preventDefault()
            }
            if (swiper.params.preventClicksPropagation && swiper.animating) {
                e.stopPropagation();
                e.stopImmediatePropagation()
            }
        }
    }

    function onScroll() {
        var swiper = this;
        var wrapperEl = swiper.wrapperEl;
        var rtlTranslate = swiper.rtlTranslate;
        swiper.previousTranslate = swiper.translate;
        if (swiper.isHorizontal()) {
            if (rtlTranslate) {
                swiper.translate = wrapperEl.scrollWidth - wrapperEl.offsetWidth - wrapperEl.scrollLeft
            } else {
                swiper.translate = -wrapperEl.scrollLeft
            }
        } else {
            swiper.translate = -wrapperEl.scrollTop
        }
        if (swiper.translate === -0) {
            swiper.translate = 0
        }
        swiper.updateActiveIndex();
        swiper.updateSlidesClasses();
        var newProgress;
        var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
        if (translatesDiff === 0) {
            newProgress = 0
        } else {
            newProgress = (swiper.translate - swiper.minTranslate()) / translatesDiff
        }
        if (newProgress !== swiper.progress) {
            swiper.updateProgress(rtlTranslate ? -swiper.translate : swiper.translate)
        }
        swiper.emit("setTranslate", swiper.translate, false)
    }

    var dummyEventAttached = false;

    function dummyEventListener() {
    }

    function attachEvents() {
        var swiper = this;
        var params = swiper.params;
        var touchEvents = swiper.touchEvents;
        var el = swiper.el;
        var wrapperEl = swiper.wrapperEl;
        swiper.onTouchStart = onTouchStart.bind(swiper);
        swiper.onTouchMove = onTouchMove.bind(swiper);
        swiper.onTouchEnd = onTouchEnd.bind(swiper);
        if (params.cssMode) {
            swiper.onScroll = onScroll.bind(swiper)
        }
        swiper.onClick = onClick.bind(swiper);
        var capture = !!params.nested;
        if (!Support.touch && Support.pointerEvents) {
            el.addEventListener(touchEvents.start, swiper.onTouchStart, false);
            doc.addEventListener(touchEvents.move, swiper.onTouchMove, capture);
            doc.addEventListener(touchEvents.end, swiper.onTouchEnd, false)
        } else {
            if (Support.touch) {
                var passiveListener = touchEvents.start === "touchstart" && Support.passiveListener && params.passiveListeners ? {
                    passive: true,
                    capture: false
                } : false;
                el.addEventListener(touchEvents.start, swiper.onTouchStart, passiveListener);
                el.addEventListener(touchEvents.move, swiper.onTouchMove, Support.passiveListener ? {
                    passive: false,
                    capture: capture
                } : capture);
                el.addEventListener(touchEvents.end, swiper.onTouchEnd, passiveListener);
                if (touchEvents.cancel) {
                    el.addEventListener(touchEvents.cancel, swiper.onTouchEnd, passiveListener)
                }
                if (!dummyEventAttached) {
                    doc.addEventListener("touchstart", dummyEventListener);
                    dummyEventAttached = true
                }
            }
            if (params.simulateTouch && !Device.ios && !Device.android || params.simulateTouch && !Support.touch && Device.ios) {
                el.addEventListener("mousedown", swiper.onTouchStart, false);
                doc.addEventListener("mousemove", swiper.onTouchMove, capture);
                doc.addEventListener("mouseup", swiper.onTouchEnd, false)
            }
        }
        if (params.preventClicks || params.preventClicksPropagation) {
            el.addEventListener("click", swiper.onClick, true)
        }
        if (params.cssMode) {
            wrapperEl.addEventListener("scroll", swiper.onScroll)
        }
        if (params.updateOnWindowResize) {
            swiper.on(Device.ios || Device.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", onResize, true)
        } else {
            swiper.on("observerUpdate", onResize, true)
        }
    }

    function detachEvents() {
        var swiper = this;
        var params = swiper.params;
        var touchEvents = swiper.touchEvents;
        var el = swiper.el;
        var wrapperEl = swiper.wrapperEl;
        var capture = !!params.nested;
        if (!Support.touch && Support.pointerEvents) {
            el.removeEventListener(touchEvents.start, swiper.onTouchStart, false);
            doc.removeEventListener(touchEvents.move, swiper.onTouchMove, capture);
            doc.removeEventListener(touchEvents.end, swiper.onTouchEnd, false)
        } else {
            if (Support.touch) {
                var passiveListener = touchEvents.start === "onTouchStart" && Support.passiveListener && params.passiveListeners ? {
                    passive: true,
                    capture: false
                } : false;
                el.removeEventListener(touchEvents.start, swiper.onTouchStart, passiveListener);
                el.removeEventListener(touchEvents.move, swiper.onTouchMove, capture);
                el.removeEventListener(touchEvents.end, swiper.onTouchEnd, passiveListener);
                if (touchEvents.cancel) {
                    el.removeEventListener(touchEvents.cancel, swiper.onTouchEnd, passiveListener)
                }
            }
            if (params.simulateTouch && !Device.ios && !Device.android || params.simulateTouch && !Support.touch && Device.ios) {
                el.removeEventListener("mousedown", swiper.onTouchStart, false);
                doc.removeEventListener("mousemove", swiper.onTouchMove, capture);
                doc.removeEventListener("mouseup", swiper.onTouchEnd, false)
            }
        }
        if (params.preventClicks || params.preventClicksPropagation) {
            el.removeEventListener("click", swiper.onClick, true)
        }
        if (params.cssMode) {
            wrapperEl.removeEventListener("scroll", swiper.onScroll)
        }
        swiper.off(Device.ios || Device.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", onResize)
    }

    var events = {attachEvents: attachEvents, detachEvents: detachEvents};

    function setBreakpoint() {
        var swiper = this;
        var activeIndex = swiper.activeIndex;
        var initialized = swiper.initialized;
        var loopedSlides = swiper.loopedSlides;
        if (loopedSlides === void 0) loopedSlides = 0;
        var params = swiper.params;
        var $el = swiper.$el;
        var breakpoints = params.breakpoints;
        if (!breakpoints || breakpoints && Object.keys(breakpoints).length === 0) {
            return
        }
        var breakpoint = swiper.getBreakpoint(breakpoints);
        if (breakpoint && swiper.currentBreakpoint !== breakpoint) {
            var breakpointOnlyParams = breakpoint in breakpoints ? breakpoints[breakpoint] : undefined;
            if (breakpointOnlyParams) {
                ["slidesPerView", "spaceBetween", "slidesPerGroup", "slidesPerGroupSkip", "slidesPerColumn"].forEach(function (param) {
                    var paramValue = breakpointOnlyParams[param];
                    if (typeof paramValue === "undefined") {
                        return
                    }
                    if (param === "slidesPerView" && (paramValue === "AUTO" || paramValue === "auto")) {
                        breakpointOnlyParams[param] = "auto"
                    } else if (param === "slidesPerView") {
                        breakpointOnlyParams[param] = parseFloat(paramValue)
                    } else {
                        breakpointOnlyParams[param] = parseInt(paramValue, 10)
                    }
                })
            }
            var breakpointParams = breakpointOnlyParams || swiper.originalParams;
            var wasMultiRow = params.slidesPerColumn > 1;
            var isMultiRow = breakpointParams.slidesPerColumn > 1;
            if (wasMultiRow && !isMultiRow) {
                $el.removeClass(params.containerModifierClass + "multirow " + params.containerModifierClass + "multirow-column")
            } else if (!wasMultiRow && isMultiRow) {
                $el.addClass(params.containerModifierClass + "multirow");
                if (breakpointParams.slidesPerColumnFill === "column") {
                    $el.addClass(params.containerModifierClass + "multirow-column")
                }
            }
            var directionChanged = breakpointParams.direction && breakpointParams.direction !== params.direction;
            var needsReLoop = params.loop && (breakpointParams.slidesPerView !== params.slidesPerView || directionChanged);
            if (directionChanged && initialized) {
                swiper.changeDirection()
            }
            Utils.extend(swiper.params, breakpointParams);
            Utils.extend(swiper, {
                allowTouchMove: swiper.params.allowTouchMove,
                allowSlideNext: swiper.params.allowSlideNext,
                allowSlidePrev: swiper.params.allowSlidePrev
            });
            swiper.currentBreakpoint = breakpoint;
            if (needsReLoop && initialized) {
                swiper.loopDestroy();
                swiper.loopCreate();
                swiper.updateSlides();
                swiper.slideTo(activeIndex - loopedSlides + swiper.loopedSlides, 0, false)
            }
            swiper.emit("breakpoint", breakpointParams)
        }
    }

    function getBreakpoint(breakpoints) {
        if (!breakpoints) {
            return undefined
        }
        var breakpoint = false;
        var points = Object.keys(breakpoints).map(function (point) {
            if (typeof point === "string" && point.indexOf("@") === 0) {
                var minRatio = parseFloat(point.substr(1));
                var value = win.innerHeight * minRatio;
                return {value: value, point: point}
            }
            return {value: point, point: point}
        });
        points.sort(function (a, b) {
            return parseInt(a.value, 10) - parseInt(b.value, 10)
        });
        for (var i = 0; i < points.length; i += 1) {
            var ref = points[i];
            var point = ref.point;
            var value = ref.value;
            if (value <= win.innerWidth) {
                breakpoint = point
            }
        }
        return breakpoint || "max"
    }

    var breakpoints = {setBreakpoint: setBreakpoint, getBreakpoint: getBreakpoint};

    function addClasses() {
        var swiper = this;
        var classNames = swiper.classNames;
        var params = swiper.params;
        var rtl = swiper.rtl;
        var $el = swiper.$el;
        var suffixes = [];
        suffixes.push("initialized");
        suffixes.push(params.direction);
        if (params.freeMode) {
            suffixes.push("free-mode")
        }
        if (params.autoHeight) {
            suffixes.push("autoheight")
        }
        if (rtl) {
            suffixes.push("rtl")
        }
        if (params.slidesPerColumn > 1) {
            suffixes.push("multirow");
            if (params.slidesPerColumnFill === "column") {
                suffixes.push("multirow-column")
            }
        }
        if (Device.android) {
            suffixes.push("android")
        }
        if (Device.ios) {
            suffixes.push("ios")
        }
        if (params.cssMode) {
            suffixes.push("css-mode")
        }
        suffixes.forEach(function (suffix) {
            classNames.push(params.containerModifierClass + suffix)
        });
        $el.addClass(classNames.join(" "))
    }

    function removeClasses() {
        var swiper = this;
        var $el = swiper.$el;
        var classNames = swiper.classNames;
        $el.removeClass(classNames.join(" "))
    }

    var classes = {addClasses: addClasses, removeClasses: removeClasses};

    function loadImage(imageEl, src, srcset, sizes, checkForComplete, callback) {
        var image;

        function onReady() {
            if (callback) {
                callback()
            }
        }

        var isPicture = $(imageEl).parent("picture")[0];
        if (!isPicture && (!imageEl.complete || !checkForComplete)) {
            if (src) {
                image = new win.Image;
                image.onload = onReady;
                image.onerror = onReady;
                if (sizes) {
                    image.sizes = sizes
                }
                if (srcset) {
                    image.srcset = srcset
                }
                if (src) {
                    image.src = src
                }
            } else {
                onReady()
            }
        } else {
            onReady()
        }
    }

    function preloadImages() {
        var swiper = this;
        swiper.imagesToLoad = swiper.$el.find("img");

        function onReady() {
            if (typeof swiper === "undefined" || swiper === null || !swiper || swiper.destroyed) {
                return
            }
            if (swiper.imagesLoaded !== undefined) {
                swiper.imagesLoaded += 1
            }
            if (swiper.imagesLoaded === swiper.imagesToLoad.length) {
                if (swiper.params.updateOnImagesReady) {
                    swiper.update()
                }
                swiper.emit("imagesReady")
            }
        }

        for (var i = 0; i < swiper.imagesToLoad.length; i += 1) {
            var imageEl = swiper.imagesToLoad[i];
            swiper.loadImage(imageEl, imageEl.currentSrc || imageEl.getAttribute("src"), imageEl.srcset || imageEl.getAttribute("srcset"), imageEl.sizes || imageEl.getAttribute("sizes"), true, onReady)
        }
    }

    var images = {loadImage: loadImage, preloadImages: preloadImages};

    function checkOverflow() {
        var swiper = this;
        var params = swiper.params;
        var wasLocked = swiper.isLocked;
        var lastSlidePosition = swiper.slides.length > 0 && params.slidesOffsetBefore + params.spaceBetween * (swiper.slides.length - 1) + swiper.slides[0].offsetWidth * swiper.slides.length;
        if (params.slidesOffsetBefore && params.slidesOffsetAfter && lastSlidePosition) {
            swiper.isLocked = lastSlidePosition <= swiper.size
        } else {
            swiper.isLocked = swiper.snapGrid.length === 1
        }
        swiper.allowSlideNext = !swiper.isLocked;
        swiper.allowSlidePrev = !swiper.isLocked;
        if (wasLocked !== swiper.isLocked) {
            swiper.emit(swiper.isLocked ? "lock" : "unlock")
        }
        if (wasLocked && wasLocked !== swiper.isLocked) {
            swiper.isEnd = false;
            swiper.navigation.update()
        }
    }

    var checkOverflow$1 = {checkOverflow: checkOverflow};
    var defaults = {
        init: true,
        direction: "horizontal",
        touchEventsTarget: "container",
        initialSlide: 0,
        speed: 300,
        cssMode: false,
        updateOnWindowResize: true,
        preventInteractionOnTransition: false,
        edgeSwipeDetection: false,
        edgeSwipeThreshold: 20,
        freeMode: false,
        freeModeMomentum: true,
        freeModeMomentumRatio: 1,
        freeModeMomentumBounce: true,
        freeModeMomentumBounceRatio: 1,
        freeModeMomentumVelocityRatio: 1,
        freeModeSticky: false,
        freeModeMinimumVelocity: .02,
        autoHeight: false,
        setWrapperSize: false,
        virtualTranslate: false,
        effect: "slide",
        breakpoints: undefined,
        spaceBetween: 0,
        slidesPerView: 1,
        slidesPerColumn: 1,
        slidesPerColumnFill: "column",
        slidesPerGroup: 1,
        slidesPerGroupSkip: 0,
        centeredSlides: false,
        centeredSlidesBounds: false,
        slidesOffsetBefore: 0,
        slidesOffsetAfter: 0,
        normalizeSlideIndex: true,
        centerInsufficientSlides: false,
        watchOverflow: false,
        roundLengths: false,
        touchRatio: 1,
        touchAngle: 45,
        simulateTouch: true,
        shortSwipes: true,
        longSwipes: true,
        longSwipesRatio: .5,
        longSwipesMs: 300,
        followFinger: true,
        allowTouchMove: true,
        threshold: 0,
        touchMoveStopPropagation: false,
        touchStartPreventDefault: true,
        touchStartForcePreventDefault: false,
        touchReleaseOnEdges: false,
        uniqueNavElements: true,
        resistance: true,
        resistanceRatio: .85,
        watchSlidesProgress: false,
        watchSlidesVisibility: false,
        grabCursor: false,
        preventClicks: true,
        preventClicksPropagation: true,
        slideToClickedSlide: false,
        preloadImages: true,
        updateOnImagesReady: true,
        loop: false,
        loopAdditionalSlides: 0,
        loopedSlides: null,
        loopFillGroupWithBlank: false,
        allowSlidePrev: true,
        allowSlideNext: true,
        swipeHandler: null,
        noSwiping: true,
        noSwipingClass: "swiper-no-swiping",
        noSwipingSelector: null,
        passiveListeners: true,
        containerModifierClass: "swiper-container-",
        slideClass: "swiper-slide",
        slideBlankClass: "swiper-slide-invisible-blank",
        slideActiveClass: "swiper-slide-active",
        slideDuplicateActiveClass: "swiper-slide-duplicate-active",
        slideVisibleClass: "swiper-slide-visible",
        slideDuplicateClass: "swiper-slide-duplicate",
        slideNextClass: "swiper-slide-next",
        slideDuplicateNextClass: "swiper-slide-duplicate-next",
        slidePrevClass: "swiper-slide-prev",
        slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
        wrapperClass: "swiper-wrapper",
        runCallbacksOnInit: true
    };
    var prototypes = {
        update: update,
        translate: translate,
        transition: transition$1,
        slide: slide,
        loop: loop,
        grabCursor: grabCursor,
        manipulation: manipulation,
        events: events,
        breakpoints: breakpoints,
        checkOverflow: checkOverflow$1,
        classes: classes,
        images: images
    };
    var extendedDefaults = {};
    var Swiper = function (SwiperClass) {
        function Swiper() {
            var assign;
            var args = [], len = arguments.length;
            while (len--) args[len] = arguments[len];
            var el;
            var params;
            if (args.length === 1 && args[0].constructor && args[0].constructor === Object) {
                params = args[0]
            } else {
                assign = args, el = assign[0], params = assign[1]
            }
            if (!params) {
                params = {}
            }
            params = Utils.extend({}, params);
            if (el && !params.el) {
                params.el = el
            }
            SwiperClass.call(this, params);
            Object.keys(prototypes).forEach(function (prototypeGroup) {
                Object.keys(prototypes[prototypeGroup]).forEach(function (protoMethod) {
                    if (!Swiper.prototype[protoMethod]) {
                        Swiper.prototype[protoMethod] = prototypes[prototypeGroup][protoMethod]
                    }
                })
            });
            var swiper = this;
            if (typeof swiper.modules === "undefined") {
                swiper.modules = {}
            }
            Object.keys(swiper.modules).forEach(function (moduleName) {
                var module = swiper.modules[moduleName];
                if (module.params) {
                    var moduleParamName = Object.keys(module.params)[0];
                    var moduleParams = module.params[moduleParamName];
                    if (typeof moduleParams !== "object" || moduleParams === null) {
                        return
                    }
                    if (!(moduleParamName in params && "enabled" in moduleParams)) {
                        return
                    }
                    if (params[moduleParamName] === true) {
                        params[moduleParamName] = {enabled: true}
                    }
                    if (typeof params[moduleParamName] === "object" && !("enabled" in params[moduleParamName])) {
                        params[moduleParamName].enabled = true
                    }
                    if (!params[moduleParamName]) {
                        params[moduleParamName] = {enabled: false}
                    }
                }
            });
            var swiperParams = Utils.extend({}, defaults);
            swiper.useModulesParams(swiperParams);
            swiper.params = Utils.extend({}, swiperParams, extendedDefaults, params);
            swiper.originalParams = Utils.extend({}, swiper.params);
            swiper.passedParams = Utils.extend({}, params);
            swiper.$ = $;
            var $el = $(swiper.params.el);
            el = $el[0];
            if (!el) {
                return undefined
            }
            if ($el.length > 1) {
                var swipers = [];
                $el.each(function (index, containerEl) {
                    var newParams = Utils.extend({}, params, {el: containerEl});
                    swipers.push(new Swiper(newParams))
                });
                return swipers
            }
            el.swiper = swiper;
            $el.data("swiper", swiper);
            var $wrapperEl;
            if (el && el.shadowRoot && el.shadowRoot.querySelector) {
                $wrapperEl = $(el.shadowRoot.querySelector("." + swiper.params.wrapperClass));
                $wrapperEl.children = function (options) {
                    return $el.children(options)
                }
            } else {
                $wrapperEl = $el.children("." + swiper.params.wrapperClass)
            }
            Utils.extend(swiper, {
                $el: $el,
                el: el,
                $wrapperEl: $wrapperEl,
                wrapperEl: $wrapperEl[0],
                classNames: [],
                slides: $(),
                slidesGrid: [],
                snapGrid: [],
                slidesSizesGrid: [],
                isHorizontal: function isHorizontal() {
                    return swiper.params.direction === "horizontal"
                },
                isVertical: function isVertical() {
                    return swiper.params.direction === "vertical"
                },
                rtl: el.dir.toLowerCase() === "rtl" || $el.css("direction") === "rtl",
                rtlTranslate: swiper.params.direction === "horizontal" && (el.dir.toLowerCase() === "rtl" || $el.css("direction") === "rtl"),
                wrongRTL: $wrapperEl.css("display") === "-webkit-box",
                activeIndex: 0,
                realIndex: 0,
                isBeginning: true,
                isEnd: false,
                translate: 0,
                previousTranslate: 0,
                progress: 0,
                velocity: 0,
                animating: false,
                allowSlideNext: swiper.params.allowSlideNext,
                allowSlidePrev: swiper.params.allowSlidePrev,
                touchEvents: function touchEvents() {
                    var touch = ["touchstart", "touchmove", "touchend", "touchcancel"];
                    var desktop = ["mousedown", "mousemove", "mouseup"];
                    if (Support.pointerEvents) {
                        desktop = ["pointerdown", "pointermove", "pointerup"]
                    }
                    swiper.touchEventsTouch = {start: touch[0], move: touch[1], end: touch[2], cancel: touch[3]};
                    swiper.touchEventsDesktop = {start: desktop[0], move: desktop[1], end: desktop[2]};
                    return Support.touch || !swiper.params.simulateTouch ? swiper.touchEventsTouch : swiper.touchEventsDesktop
                }(),
                touchEventsData: {
                    isTouched: undefined,
                    isMoved: undefined,
                    allowTouchCallbacks: undefined,
                    touchStartTime: undefined,
                    isScrolling: undefined,
                    currentTranslate: undefined,
                    startTranslate: undefined,
                    allowThresholdMove: undefined,
                    formElements: "input, select, option, textarea, button, video, label",
                    lastClickTime: Utils.now(),
                    clickTimeout: undefined,
                    velocities: [],
                    allowMomentumBounce: undefined,
                    isTouchEvent: undefined,
                    startMoving: undefined
                },
                allowClick: true,
                allowTouchMove: swiper.params.allowTouchMove,
                touches: {startX: 0, startY: 0, currentX: 0, currentY: 0, diff: 0},
                imagesToLoad: [],
                imagesLoaded: 0
            });
            swiper.useModules();
            if (swiper.params.init) {
                swiper.init()
            }
            return swiper
        }

        if (SwiperClass) Swiper.__proto__ = SwiperClass;
        Swiper.prototype = Object.create(SwiperClass && SwiperClass.prototype);
        Swiper.prototype.constructor = Swiper;
        var staticAccessors = {
            extendedDefaults: {configurable: true},
            defaults: {configurable: true},
            Class: {configurable: true},
            $: {configurable: true}
        };
        Swiper.prototype.slidesPerViewDynamic = function slidesPerViewDynamic() {
            var swiper = this;
            var params = swiper.params;
            var slides = swiper.slides;
            var slidesGrid = swiper.slidesGrid;
            var swiperSize = swiper.size;
            var activeIndex = swiper.activeIndex;
            var spv = 1;
            if (params.centeredSlides) {
                var slideSize = slides[activeIndex].swiperSlideSize;
                var breakLoop;
                for (var i = activeIndex + 1; i < slides.length; i += 1) {
                    if (slides[i] && !breakLoop) {
                        slideSize += slides[i].swiperSlideSize;
                        spv += 1;
                        if (slideSize > swiperSize) {
                            breakLoop = true
                        }
                    }
                }
                for (var i$1 = activeIndex - 1; i$1 >= 0; i$1 -= 1) {
                    if (slides[i$1] && !breakLoop) {
                        slideSize += slides[i$1].swiperSlideSize;
                        spv += 1;
                        if (slideSize > swiperSize) {
                            breakLoop = true
                        }
                    }
                }
            } else {
                for (var i$2 = activeIndex + 1; i$2 < slides.length; i$2 += 1) {
                    if (slidesGrid[i$2] - slidesGrid[activeIndex] < swiperSize) {
                        spv += 1
                    }
                }
            }
            return spv
        };
        Swiper.prototype.update = function update() {
            var swiper = this;
            if (!swiper || swiper.destroyed) {
                return
            }
            var snapGrid = swiper.snapGrid;
            var params = swiper.params;
            if (params.breakpoints) {
                swiper.setBreakpoint()
            }
            swiper.updateSize();
            swiper.updateSlides();
            swiper.updateProgress();
            swiper.updateSlidesClasses();

            function setTranslate() {
                var translateValue = swiper.rtlTranslate ? swiper.translate * -1 : swiper.translate;
                var newTranslate = Math.min(Math.max(translateValue, swiper.maxTranslate()), swiper.minTranslate());
                swiper.setTranslate(newTranslate);
                swiper.updateActiveIndex();
                swiper.updateSlidesClasses()
            }

            var translated;
            if (swiper.params.freeMode) {
                setTranslate();
                if (swiper.params.autoHeight) {
                    swiper.updateAutoHeight()
                }
            } else {
                if ((swiper.params.slidesPerView === "auto" || swiper.params.slidesPerView > 1) && swiper.isEnd && !swiper.params.centeredSlides) {
                    translated = swiper.slideTo(swiper.slides.length - 1, 0, false, true)
                } else {
                    translated = swiper.slideTo(swiper.activeIndex, 0, false, true)
                }
                if (!translated) {
                    setTranslate()
                }
            }
            if (params.watchOverflow && snapGrid !== swiper.snapGrid) {
                swiper.checkOverflow()
            }
            swiper.emit("update")
        };
        Swiper.prototype.changeDirection = function changeDirection(newDirection, needUpdate) {
            if (needUpdate === void 0) needUpdate = true;
            var swiper = this;
            var currentDirection = swiper.params.direction;
            if (!newDirection) {
                newDirection = currentDirection === "horizontal" ? "vertical" : "horizontal"
            }
            if (newDirection === currentDirection || newDirection !== "horizontal" && newDirection !== "vertical") {
                return swiper
            }
            swiper.$el.removeClass("" + swiper.params.containerModifierClass + currentDirection).addClass("" + swiper.params.containerModifierClass + newDirection);
            swiper.params.direction = newDirection;
            swiper.slides.each(function (slideIndex, slideEl) {
                if (newDirection === "vertical") {
                    slideEl.style.width = ""
                } else {
                    slideEl.style.height = ""
                }
            });
            swiper.emit("changeDirection");
            if (needUpdate) {
                swiper.update()
            }
            return swiper
        };
        Swiper.prototype.init = function init() {
            var swiper = this;
            if (swiper.initialized) {
                return
            }
            swiper.emit("beforeInit");
            if (swiper.params.breakpoints) {
                swiper.setBreakpoint()
            }
            swiper.addClasses();
            if (swiper.params.loop) {
                swiper.loopCreate()
            }
            swiper.updateSize();
            swiper.updateSlides();
            if (swiper.params.watchOverflow) {
                swiper.checkOverflow()
            }
            if (swiper.params.grabCursor) {
                swiper.setGrabCursor()
            }
            if (swiper.params.preloadImages) {
                swiper.preloadImages()
            }
            if (swiper.params.loop) {
                swiper.slideTo(swiper.params.initialSlide + swiper.loopedSlides, 0, swiper.params.runCallbacksOnInit)
            } else {
                swiper.slideTo(swiper.params.initialSlide, 0, swiper.params.runCallbacksOnInit)
            }
            swiper.attachEvents();
            swiper.initialized = true;
            swiper.emit("init")
        };
        Swiper.prototype.destroy = function destroy(deleteInstance, cleanStyles) {
            if (deleteInstance === void 0) deleteInstance = true;
            if (cleanStyles === void 0) cleanStyles = true;
            var swiper = this;
            var params = swiper.params;
            var $el = swiper.$el;
            var $wrapperEl = swiper.$wrapperEl;
            var slides = swiper.slides;
            if (typeof swiper.params === "undefined" || swiper.destroyed) {
                return null
            }
            swiper.emit("beforeDestroy");
            swiper.initialized = false;
            swiper.detachEvents();
            if (params.loop) {
                swiper.loopDestroy()
            }
            if (cleanStyles) {
                swiper.removeClasses();
                $el.removeAttr("style");
                $wrapperEl.removeAttr("style");
                if (slides && slides.length) {
                    slides.removeClass([params.slideVisibleClass, params.slideActiveClass, params.slideNextClass, params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index")
                }
            }
            swiper.emit("destroy");
            Object.keys(swiper.eventsListeners).forEach(function (eventName) {
                swiper.off(eventName)
            });
            if (deleteInstance !== false) {
                swiper.$el[0].swiper = null;
                swiper.$el.data("swiper", null);
                Utils.deleteProps(swiper)
            }
            swiper.destroyed = true;
            return null
        };
        Swiper.extendDefaults = function extendDefaults(newDefaults) {
            Utils.extend(extendedDefaults, newDefaults)
        };
        staticAccessors.extendedDefaults.get = function () {
            return extendedDefaults
        };
        staticAccessors.defaults.get = function () {
            return defaults
        };
        staticAccessors.Class.get = function () {
            return SwiperClass
        };
        staticAccessors.$.get = function () {
            return $
        };
        Object.defineProperties(Swiper, staticAccessors);
        return Swiper
    }(SwiperClass);
    var Device$1 = {name: "device", proto: {device: Device}, static: {device: Device}};
    var Support$1 = {name: "support", proto: {support: Support}, static: {support: Support}};
    var Browser = function Browser() {
        function isSafari() {
            var ua = win.navigator.userAgent.toLowerCase();
            return ua.indexOf("safari") >= 0 && ua.indexOf("chrome") < 0 && ua.indexOf("android") < 0
        }

        return {
            isEdge: !!win.navigator.userAgent.match(/Edge/g),
            isSafari: isSafari(),
            isWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(win.navigator.userAgent)
        }
    }();
    var Browser$1 = {name: "browser", proto: {browser: Browser}, static: {browser: Browser}};
    var Resize = {
        name: "resize", create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                resize: {
                    resizeHandler: function resizeHandler() {
                        if (!swiper || swiper.destroyed || !swiper.initialized) {
                            return
                        }
                        swiper.emit("beforeResize");
                        swiper.emit("resize")
                    }, orientationChangeHandler: function orientationChangeHandler() {
                        if (!swiper || swiper.destroyed || !swiper.initialized) {
                            return
                        }
                        swiper.emit("orientationchange")
                    }
                }
            })
        }, on: {
            init: function init() {
                var swiper = this;
                win.addEventListener("resize", swiper.resize.resizeHandler);
                win.addEventListener("orientationchange", swiper.resize.orientationChangeHandler)
            }, destroy: function destroy() {
                var swiper = this;
                win.removeEventListener("resize", swiper.resize.resizeHandler);
                win.removeEventListener("orientationchange", swiper.resize.orientationChangeHandler)
            }
        }
    };
    var Observer = {
        func: win.MutationObserver || win.WebkitMutationObserver, attach: function attach(target, options) {
            if (options === void 0) options = {};
            var swiper = this;
            var ObserverFunc = Observer.func;
            var observer = new ObserverFunc(function (mutations) {
                if (mutations.length === 1) {
                    swiper.emit("observerUpdate", mutations[0]);
                    return
                }
                var observerUpdate = function observerUpdate() {
                    swiper.emit("observerUpdate", mutations[0])
                };
                if (win.requestAnimationFrame) {
                    win.requestAnimationFrame(observerUpdate)
                } else {
                    win.setTimeout(observerUpdate, 0)
                }
            });
            observer.observe(target, {
                attributes: typeof options.attributes === "undefined" ? true : options.attributes,
                childList: typeof options.childList === "undefined" ? true : options.childList,
                characterData: typeof options.characterData === "undefined" ? true : options.characterData
            });
            swiper.observer.observers.push(observer)
        }, init: function init() {
            var swiper = this;
            if (!Support.observer || !swiper.params.observer) {
                return
            }
            if (swiper.params.observeParents) {
                var containerParents = swiper.$el.parents();
                for (var i = 0; i < containerParents.length; i += 1) {
                    swiper.observer.attach(containerParents[i])
                }
            }
            swiper.observer.attach(swiper.$el[0], {childList: swiper.params.observeSlideChildren});
            swiper.observer.attach(swiper.$wrapperEl[0], {attributes: false})
        }, destroy: function destroy() {
            var swiper = this;
            swiper.observer.observers.forEach(function (observer) {
                observer.disconnect()
            });
            swiper.observer.observers = []
        }
    };
    var Observer$1 = {
        name: "observer",
        params: {observer: false, observeParents: false, observeSlideChildren: false},
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                observer: {
                    init: Observer.init.bind(swiper),
                    attach: Observer.attach.bind(swiper),
                    destroy: Observer.destroy.bind(swiper),
                    observers: []
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                swiper.observer.init()
            }, destroy: function destroy() {
                var swiper = this;
                swiper.observer.destroy()
            }
        }
    };
    var Virtual = {
        update: function update(force) {
            var swiper = this;
            var ref = swiper.params;
            var slidesPerView = ref.slidesPerView;
            var slidesPerGroup = ref.slidesPerGroup;
            var centeredSlides = ref.centeredSlides;
            var ref$1 = swiper.params.virtual;
            var addSlidesBefore = ref$1.addSlidesBefore;
            var addSlidesAfter = ref$1.addSlidesAfter;
            var ref$2 = swiper.virtual;
            var previousFrom = ref$2.from;
            var previousTo = ref$2.to;
            var slides = ref$2.slides;
            var previousSlidesGrid = ref$2.slidesGrid;
            var renderSlide = ref$2.renderSlide;
            var previousOffset = ref$2.offset;
            swiper.updateActiveIndex();
            var activeIndex = swiper.activeIndex || 0;
            var offsetProp;
            if (swiper.rtlTranslate) {
                offsetProp = "right"
            } else {
                offsetProp = swiper.isHorizontal() ? "left" : "top"
            }
            var slidesAfter;
            var slidesBefore;
            if (centeredSlides) {
                slidesAfter = Math.floor(slidesPerView / 2) + slidesPerGroup + addSlidesBefore;
                slidesBefore = Math.floor(slidesPerView / 2) + slidesPerGroup + addSlidesAfter
            } else {
                slidesAfter = slidesPerView + (slidesPerGroup - 1) + addSlidesBefore;
                slidesBefore = slidesPerGroup + addSlidesAfter
            }
            var from = Math.max((activeIndex || 0) - slidesBefore, 0);
            var to = Math.min((activeIndex || 0) + slidesAfter, slides.length - 1);
            var offset = (swiper.slidesGrid[from] || 0) - (swiper.slidesGrid[0] || 0);
            Utils.extend(swiper.virtual, {from: from, to: to, offset: offset, slidesGrid: swiper.slidesGrid});

            function onRendered() {
                swiper.updateSlides();
                swiper.updateProgress();
                swiper.updateSlidesClasses();
                if (swiper.lazy && swiper.params.lazy.enabled) {
                    swiper.lazy.load()
                }
            }

            if (previousFrom === from && previousTo === to && !force) {
                if (swiper.slidesGrid !== previousSlidesGrid && offset !== previousOffset) {
                    swiper.slides.css(offsetProp, offset + "px")
                }
                swiper.updateProgress();
                return
            }
            if (swiper.params.virtual.renderExternal) {
                swiper.params.virtual.renderExternal.call(swiper, {
                    offset: offset,
                    from: from,
                    to: to,
                    slides: function getSlides() {
                        var slidesToRender = [];
                        for (var i = from; i <= to; i += 1) {
                            slidesToRender.push(slides[i])
                        }
                        return slidesToRender
                    }()
                });
                onRendered();
                return
            }
            var prependIndexes = [];
            var appendIndexes = [];
            if (force) {
                swiper.$wrapperEl.find("." + swiper.params.slideClass).remove()
            } else {
                for (var i = previousFrom; i <= previousTo; i += 1) {
                    if (i < from || i > to) {
                        swiper.$wrapperEl.find("." + swiper.params.slideClass + '[data-swiper-slide-index="' + i + '"]').remove()
                    }
                }
            }
            for (var i$1 = 0; i$1 < slides.length; i$1 += 1) {
                if (i$1 >= from && i$1 <= to) {
                    if (typeof previousTo === "undefined" || force) {
                        appendIndexes.push(i$1)
                    } else {
                        if (i$1 > previousTo) {
                            appendIndexes.push(i$1)
                        }
                        if (i$1 < previousFrom) {
                            prependIndexes.push(i$1)
                        }
                    }
                }
            }
            appendIndexes.forEach(function (index) {
                swiper.$wrapperEl.append(renderSlide(slides[index], index))
            });
            prependIndexes.sort(function (a, b) {
                return b - a
            }).forEach(function (index) {
                swiper.$wrapperEl.prepend(renderSlide(slides[index], index))
            });
            swiper.$wrapperEl.children(".swiper-slide").css(offsetProp, offset + "px");
            onRendered()
        }, renderSlide: function renderSlide(slide, index) {
            var swiper = this;
            var params = swiper.params.virtual;
            if (params.cache && swiper.virtual.cache[index]) {
                return swiper.virtual.cache[index]
            }
            var $slideEl = params.renderSlide ? $(params.renderSlide.call(swiper, slide, index)) : $('<div class="' + swiper.params.slideClass + '" data-swiper-slide-index="' + index + '">' + slide + "</div>");
            if (!$slideEl.attr("data-swiper-slide-index")) {
                $slideEl.attr("data-swiper-slide-index", index)
            }
            if (params.cache) {
                swiper.virtual.cache[index] = $slideEl
            }
            return $slideEl
        }, appendSlide: function appendSlide(slides) {
            var swiper = this;
            if (typeof slides === "object" && "length" in slides) {
                for (var i = 0; i < slides.length; i += 1) {
                    if (slides[i]) {
                        swiper.virtual.slides.push(slides[i])
                    }
                }
            } else {
                swiper.virtual.slides.push(slides)
            }
            swiper.virtual.update(true)
        }, prependSlide: function prependSlide(slides) {
            var swiper = this;
            var activeIndex = swiper.activeIndex;
            var newActiveIndex = activeIndex + 1;
            var numberOfNewSlides = 1;
            if (Array.isArray(slides)) {
                for (var i = 0; i < slides.length; i += 1) {
                    if (slides[i]) {
                        swiper.virtual.slides.unshift(slides[i])
                    }
                }
                newActiveIndex = activeIndex + slides.length;
                numberOfNewSlides = slides.length
            } else {
                swiper.virtual.slides.unshift(slides)
            }
            if (swiper.params.virtual.cache) {
                var cache = swiper.virtual.cache;
                var newCache = {};
                Object.keys(cache).forEach(function (cachedIndex) {
                    var $cachedEl = cache[cachedIndex];
                    var cachedElIndex = $cachedEl.attr("data-swiper-slide-index");
                    if (cachedElIndex) {
                        $cachedEl.attr("data-swiper-slide-index", parseInt(cachedElIndex, 10) + 1)
                    }
                    newCache[parseInt(cachedIndex, 10) + numberOfNewSlides] = $cachedEl
                });
                swiper.virtual.cache = newCache
            }
            swiper.virtual.update(true);
            swiper.slideTo(newActiveIndex, 0)
        }, removeSlide: function removeSlide(slidesIndexes) {
            var swiper = this;
            if (typeof slidesIndexes === "undefined" || slidesIndexes === null) {
                return
            }
            var activeIndex = swiper.activeIndex;
            if (Array.isArray(slidesIndexes)) {
                for (var i = slidesIndexes.length - 1; i >= 0; i -= 1) {
                    swiper.virtual.slides.splice(slidesIndexes[i], 1);
                    if (swiper.params.virtual.cache) {
                        delete swiper.virtual.cache[slidesIndexes[i]]
                    }
                    if (slidesIndexes[i] < activeIndex) {
                        activeIndex -= 1
                    }
                    activeIndex = Math.max(activeIndex, 0)
                }
            } else {
                swiper.virtual.slides.splice(slidesIndexes, 1);
                if (swiper.params.virtual.cache) {
                    delete swiper.virtual.cache[slidesIndexes]
                }
                if (slidesIndexes < activeIndex) {
                    activeIndex -= 1
                }
                activeIndex = Math.max(activeIndex, 0)
            }
            swiper.virtual.update(true);
            swiper.slideTo(activeIndex, 0)
        }, removeAllSlides: function removeAllSlides() {
            var swiper = this;
            swiper.virtual.slides = [];
            if (swiper.params.virtual.cache) {
                swiper.virtual.cache = {}
            }
            swiper.virtual.update(true);
            swiper.slideTo(0, 0)
        }
    };
    var Virtual$1 = {
        name: "virtual",
        params: {
            virtual: {
                enabled: false,
                slides: [],
                cache: true,
                renderSlide: null,
                renderExternal: null,
                addSlidesBefore: 0,
                addSlidesAfter: 0
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                virtual: {
                    update: Virtual.update.bind(swiper),
                    appendSlide: Virtual.appendSlide.bind(swiper),
                    prependSlide: Virtual.prependSlide.bind(swiper),
                    removeSlide: Virtual.removeSlide.bind(swiper),
                    removeAllSlides: Virtual.removeAllSlides.bind(swiper),
                    renderSlide: Virtual.renderSlide.bind(swiper),
                    slides: swiper.params.virtual.slides,
                    cache: {}
                }
            })
        },
        on: {
            beforeInit: function beforeInit() {
                var swiper = this;
                if (!swiper.params.virtual.enabled) {
                    return
                }
                swiper.classNames.push(swiper.params.containerModifierClass + "virtual");
                var overwriteParams = {watchSlidesProgress: true};
                Utils.extend(swiper.params, overwriteParams);
                Utils.extend(swiper.originalParams, overwriteParams);
                if (!swiper.params.initialSlide) {
                    swiper.virtual.update()
                }
            }, setTranslate: function setTranslate() {
                var swiper = this;
                if (!swiper.params.virtual.enabled) {
                    return
                }
                swiper.virtual.update()
            }
        }
    };
    var Keyboard = {
        handle: function handle(event) {
            var swiper = this;
            var rtl = swiper.rtlTranslate;
            var e = event;
            if (e.originalEvent) {
                e = e.originalEvent
            }
            var kc = e.keyCode || e.charCode;
            if (!swiper.allowSlideNext && (swiper.isHorizontal() && kc === 39 || swiper.isVertical() && kc === 40 || kc === 34)) {
                return false
            }
            if (!swiper.allowSlidePrev && (swiper.isHorizontal() && kc === 37 || swiper.isVertical() && kc === 38 || kc === 33)) {
                return false
            }
            if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey) {
                return undefined
            }
            if (doc.activeElement && doc.activeElement.nodeName && (doc.activeElement.nodeName.toLowerCase() === "input" || doc.activeElement.nodeName.toLowerCase() === "textarea")) {
                return undefined
            }
            if (swiper.params.keyboard.onlyInViewport && (kc === 33 || kc === 34 || kc === 37 || kc === 39 || kc === 38 || kc === 40)) {
                var inView = false;
                if (swiper.$el.parents("." + swiper.params.slideClass).length > 0 && swiper.$el.parents("." + swiper.params.slideActiveClass).length === 0) {
                    return undefined
                }
                var windowWidth = win.innerWidth;
                var windowHeight = win.innerHeight;
                var swiperOffset = swiper.$el.offset();
                if (rtl) {
                    swiperOffset.left -= swiper.$el[0].scrollLeft
                }
                var swiperCoord = [[swiperOffset.left, swiperOffset.top], [swiperOffset.left + swiper.width, swiperOffset.top], [swiperOffset.left, swiperOffset.top + swiper.height], [swiperOffset.left + swiper.width, swiperOffset.top + swiper.height]];
                for (var i = 0; i < swiperCoord.length; i += 1) {
                    var point = swiperCoord[i];
                    if (point[0] >= 0 && point[0] <= windowWidth && point[1] >= 0 && point[1] <= windowHeight) {
                        inView = true
                    }
                }
                if (!inView) {
                    return undefined
                }
            }
            if (swiper.isHorizontal()) {
                if (kc === 33 || kc === 34 || kc === 37 || kc === 39) {
                    if (e.preventDefault) {
                        e.preventDefault()
                    } else {
                        e.returnValue = false
                    }
                }
                if ((kc === 34 || kc === 39) && !rtl || (kc === 33 || kc === 37) && rtl) {
                    swiper.slideNext()
                }
                if ((kc === 33 || kc === 37) && !rtl || (kc === 34 || kc === 39) && rtl) {
                    swiper.slidePrev()
                }
            } else {
                if (kc === 33 || kc === 34 || kc === 38 || kc === 40) {
                    if (e.preventDefault) {
                        e.preventDefault()
                    } else {
                        e.returnValue = false
                    }
                }
                if (kc === 34 || kc === 40) {
                    swiper.slideNext()
                }
                if (kc === 33 || kc === 38) {
                    swiper.slidePrev()
                }
            }
            swiper.emit("keyPress", kc);
            return undefined
        }, enable: function enable() {
            var swiper = this;
            if (swiper.keyboard.enabled) {
                return
            }
            $(doc).on("keydown", swiper.keyboard.handle);
            swiper.keyboard.enabled = true
        }, disable: function disable() {
            var swiper = this;
            if (!swiper.keyboard.enabled) {
                return
            }
            $(doc).off("keydown", swiper.keyboard.handle);
            swiper.keyboard.enabled = false
        }
    };
    var Keyboard$1 = {
        name: "keyboard",
        params: {keyboard: {enabled: false, onlyInViewport: true}},
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                keyboard: {
                    enabled: false,
                    enable: Keyboard.enable.bind(swiper),
                    disable: Keyboard.disable.bind(swiper),
                    handle: Keyboard.handle.bind(swiper)
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                if (swiper.params.keyboard.enabled) {
                    swiper.keyboard.enable()
                }
            }, destroy: function destroy() {
                var swiper = this;
                if (swiper.keyboard.enabled) {
                    swiper.keyboard.disable()
                }
            }
        }
    };

    function isEventSupported() {
        var eventName = "onwheel";
        var isSupported = eventName in doc;
        if (!isSupported) {
            var element = doc.createElement("div");
            element.setAttribute(eventName, "return;");
            isSupported = typeof element[eventName] === "function"
        }
        if (!isSupported && doc.implementation && doc.implementation.hasFeature && doc.implementation.hasFeature("", "") !== true) {
            isSupported = doc.implementation.hasFeature("Events.wheel", "3.0")
        }
        return isSupported
    }

    var Mousewheel = {
        lastScrollTime: Utils.now(), lastEventBeforeSnap: undefined, recentWheelEvents: [], event: function event() {
            if (win.navigator.userAgent.indexOf("firefox") > -1) {
                return "DOMMouseScroll"
            }
            return isEventSupported() ? "wheel" : "mousewheel"
        }, normalize: function normalize(e) {
            var PIXEL_STEP = 10;
            var LINE_HEIGHT = 40;
            var PAGE_HEIGHT = 800;
            var sX = 0;
            var sY = 0;
            var pX = 0;
            var pY = 0;
            if ("detail" in e) {
                sY = e.detail
            }
            if ("wheelDelta" in e) {
                sY = -e.wheelDelta / 120
            }
            if ("wheelDeltaY" in e) {
                sY = -e.wheelDeltaY / 120
            }
            if ("wheelDeltaX" in e) {
                sX = -e.wheelDeltaX / 120
            }
            if ("axis" in e && e.axis === e.HORIZONTAL_AXIS) {
                sX = sY;
                sY = 0
            }
            pX = sX * PIXEL_STEP;
            pY = sY * PIXEL_STEP;
            if ("deltaY" in e) {
                pY = e.deltaY
            }
            if ("deltaX" in e) {
                pX = e.deltaX
            }
            if (e.shiftKey && !pX) {
                pX = pY;
                pY = 0
            }
            if ((pX || pY) && e.deltaMode) {
                if (e.deltaMode === 1) {
                    pX *= LINE_HEIGHT;
                    pY *= LINE_HEIGHT
                } else {
                    pX *= PAGE_HEIGHT;
                    pY *= PAGE_HEIGHT
                }
            }
            if (pX && !sX) {
                sX = pX < 1 ? -1 : 1
            }
            if (pY && !sY) {
                sY = pY < 1 ? -1 : 1
            }
            return {spinX: sX, spinY: sY, pixelX: pX, pixelY: pY}
        }, handleMouseEnter: function handleMouseEnter() {
            var swiper = this;
            swiper.mouseEntered = true
        }, handleMouseLeave: function handleMouseLeave() {
            var swiper = this;
            swiper.mouseEntered = false
        }, handle: function handle(event) {
            var e = event;
            var swiper = this;
            var params = swiper.params.mousewheel;
            if (swiper.params.cssMode) {
                e.preventDefault()
            }
            var target = swiper.$el;
            if (swiper.params.mousewheel.eventsTarged !== "container") {
                target = $(swiper.params.mousewheel.eventsTarged)
            }
            if (!swiper.mouseEntered && !target[0].contains(e.target) && !params.releaseOnEdges) {
                return true
            }
            if (e.originalEvent) {
                e = e.originalEvent
            }
            var delta = 0;
            var rtlFactor = swiper.rtlTranslate ? -1 : 1;
            var data = Mousewheel.normalize(e);
            if (params.forceToAxis) {
                if (swiper.isHorizontal()) {
                    if (Math.abs(data.pixelX) > Math.abs(data.pixelY)) {
                        delta = -data.pixelX * rtlFactor
                    } else {
                        return true
                    }
                } else if (Math.abs(data.pixelY) > Math.abs(data.pixelX)) {
                    delta = -data.pixelY
                } else {
                    return true
                }
            } else {
                delta = Math.abs(data.pixelX) > Math.abs(data.pixelY) ? -data.pixelX * rtlFactor : -data.pixelY
            }
            if (delta === 0) {
                return true
            }
            if (params.invert) {
                delta = -delta
            }
            if (!swiper.params.freeMode) {
                var newEvent = {time: Utils.now(), delta: Math.abs(delta), direction: Math.sign(delta), raw: event};
                var recentWheelEvents = swiper.mousewheel.recentWheelEvents;
                if (recentWheelEvents.length >= 2) {
                    recentWheelEvents.shift()
                }
                var prevEvent = recentWheelEvents.length ? recentWheelEvents[recentWheelEvents.length - 1] : undefined;
                recentWheelEvents.push(newEvent);
                if (prevEvent) {
                    if (newEvent.direction !== prevEvent.direction || newEvent.delta > prevEvent.delta || newEvent.time > prevEvent.time + 150) {
                        swiper.mousewheel.animateSlider(newEvent)
                    }
                } else {
                    swiper.mousewheel.animateSlider(newEvent)
                }
                if (swiper.mousewheel.releaseScroll(newEvent)) {
                    return true
                }
            } else {
                var newEvent$1 = {time: Utils.now(), delta: Math.abs(delta), direction: Math.sign(delta)};
                var ref = swiper.mousewheel;
                var lastEventBeforeSnap = ref.lastEventBeforeSnap;
                var ignoreWheelEvents = lastEventBeforeSnap && newEvent$1.time < lastEventBeforeSnap.time + 500 && newEvent$1.delta <= lastEventBeforeSnap.delta && newEvent$1.direction === lastEventBeforeSnap.direction;
                if (!ignoreWheelEvents) {
                    swiper.mousewheel.lastEventBeforeSnap = undefined;
                    if (swiper.params.loop) {
                        swiper.loopFix()
                    }
                    var position = swiper.getTranslate() + delta * params.sensitivity;
                    var wasBeginning = swiper.isBeginning;
                    var wasEnd = swiper.isEnd;
                    if (position >= swiper.minTranslate()) {
                        position = swiper.minTranslate()
                    }
                    if (position <= swiper.maxTranslate()) {
                        position = swiper.maxTranslate()
                    }
                    swiper.setTransition(0);
                    swiper.setTranslate(position);
                    swiper.updateProgress();
                    swiper.updateActiveIndex();
                    swiper.updateSlidesClasses();
                    if (!wasBeginning && swiper.isBeginning || !wasEnd && swiper.isEnd) {
                        swiper.updateSlidesClasses()
                    }
                    if (swiper.params.freeModeSticky) {
                        clearTimeout(swiper.mousewheel.timeout);
                        swiper.mousewheel.timeout = undefined;
                        var recentWheelEvents$1 = swiper.mousewheel.recentWheelEvents;
                        if (recentWheelEvents$1.length >= 15) {
                            recentWheelEvents$1.shift()
                        }
                        var prevEvent$1 = recentWheelEvents$1.length ? recentWheelEvents$1[recentWheelEvents$1.length - 1] : undefined;
                        var firstEvent = recentWheelEvents$1[0];
                        recentWheelEvents$1.push(newEvent$1);
                        if (prevEvent$1 && (newEvent$1.delta > prevEvent$1.delta || newEvent$1.direction !== prevEvent$1.direction)) {
                            recentWheelEvents$1.splice(0)
                        } else if (recentWheelEvents$1.length >= 15 && newEvent$1.time - firstEvent.time < 500 && firstEvent.delta - newEvent$1.delta >= 1 && newEvent$1.delta <= 6) {
                            var snapToThreshold = delta > 0 ? .8 : .2;
                            swiper.mousewheel.lastEventBeforeSnap = newEvent$1;
                            recentWheelEvents$1.splice(0);
                            swiper.mousewheel.timeout = Utils.nextTick(function () {
                                swiper.slideToClosest(swiper.params.speed, true, undefined, snapToThreshold)
                            }, 0)
                        }
                        if (!swiper.mousewheel.timeout) {
                            swiper.mousewheel.timeout = Utils.nextTick(function () {
                                var snapToThreshold = .5;
                                swiper.mousewheel.lastEventBeforeSnap = newEvent$1;
                                recentWheelEvents$1.splice(0);
                                swiper.slideToClosest(swiper.params.speed, true, undefined, snapToThreshold)
                            }, 500)
                        }
                    }
                    if (!ignoreWheelEvents) {
                        swiper.emit("scroll", e)
                    }
                    if (swiper.params.autoplay && swiper.params.autoplayDisableOnInteraction) {
                        swiper.autoplay.stop()
                    }
                    if (position === swiper.minTranslate() || position === swiper.maxTranslate()) {
                        return true
                    }
                }
            }
            if (e.preventDefault) {
                e.preventDefault()
            } else {
                e.returnValue = false
            }
            return false
        }, animateSlider: function animateSlider(newEvent) {
            var swiper = this;
            if (newEvent.delta >= 6 && Utils.now() - swiper.mousewheel.lastScrollTime < 60) {
                return true
            }
            if (newEvent.direction < 0) {
                if ((!swiper.isEnd || swiper.params.loop) && !swiper.animating) {
                    swiper.slideNext();
                    swiper.emit("scroll", newEvent.raw)
                }
            } else if ((!swiper.isBeginning || swiper.params.loop) && !swiper.animating) {
                swiper.slidePrev();
                swiper.emit("scroll", newEvent.raw)
            }
            swiper.mousewheel.lastScrollTime = (new win.Date).getTime();
            return false
        }, releaseScroll: function releaseScroll(newEvent) {
            var swiper = this;
            var params = swiper.params.mousewheel;
            if (newEvent.direction < 0) {
                if (swiper.isEnd && !swiper.params.loop && params.releaseOnEdges) {
                    return true
                }
            } else if (swiper.isBeginning && !swiper.params.loop && params.releaseOnEdges) {
                return true
            }
            return false
        }, enable: function enable() {
            var swiper = this;
            var event = Mousewheel.event();
            if (swiper.params.cssMode) {
                swiper.wrapperEl.removeEventListener(event, swiper.mousewheel.handle);
                return true
            }
            if (!event) {
                return false
            }
            if (swiper.mousewheel.enabled) {
                return false
            }
            var target = swiper.$el;
            if (swiper.params.mousewheel.eventsTarged !== "container") {
                target = $(swiper.params.mousewheel.eventsTarged)
            }
            target.on("mouseenter", swiper.mousewheel.handleMouseEnter);
            target.on("mouseleave", swiper.mousewheel.handleMouseLeave);
            target.on(event, swiper.mousewheel.handle);
            swiper.mousewheel.enabled = true;
            return true
        }, disable: function disable() {
            var swiper = this;
            var event = Mousewheel.event();
            if (swiper.params.cssMode) {
                swiper.wrapperEl.addEventListener(event, swiper.mousewheel.handle);
                return true
            }
            if (!event) {
                return false
            }
            if (!swiper.mousewheel.enabled) {
                return false
            }
            var target = swiper.$el;
            if (swiper.params.mousewheel.eventsTarged !== "container") {
                target = $(swiper.params.mousewheel.eventsTarged)
            }
            target.off(event, swiper.mousewheel.handle);
            swiper.mousewheel.enabled = false;
            return true
        }
    };
    var Mousewheel$1 = {
        name: "mousewheel",
        params: {
            mousewheel: {
                enabled: false,
                releaseOnEdges: false,
                invert: false,
                forceToAxis: false,
                sensitivity: 1,
                eventsTarged: "container"
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                mousewheel: {
                    enabled: false,
                    enable: Mousewheel.enable.bind(swiper),
                    disable: Mousewheel.disable.bind(swiper),
                    handle: Mousewheel.handle.bind(swiper),
                    handleMouseEnter: Mousewheel.handleMouseEnter.bind(swiper),
                    handleMouseLeave: Mousewheel.handleMouseLeave.bind(swiper),
                    animateSlider: Mousewheel.animateSlider.bind(swiper),
                    releaseScroll: Mousewheel.releaseScroll.bind(swiper),
                    lastScrollTime: Utils.now(),
                    lastEventBeforeSnap: undefined,
                    recentWheelEvents: []
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                if (!swiper.params.mousewheel.enabled && swiper.params.cssMode) {
                    swiper.mousewheel.disable()
                }
                if (swiper.params.mousewheel.enabled) {
                    swiper.mousewheel.enable()
                }
            }, destroy: function destroy() {
                var swiper = this;
                if (swiper.params.cssMode) {
                    swiper.mousewheel.enable()
                }
                if (swiper.mousewheel.enabled) {
                    swiper.mousewheel.disable()
                }
            }
        }
    };
    var Navigation = {
        update: function update() {
            var swiper = this;
            var params = swiper.params.navigation;
            if (swiper.params.loop) {
                return
            }
            var ref = swiper.navigation;
            var $nextEl = ref.$nextEl;
            var $prevEl = ref.$prevEl;
            if ($prevEl && $prevEl.length > 0) {
                if (swiper.isBeginning) {
                    $prevEl.addClass(params.disabledClass)
                } else {
                    $prevEl.removeClass(params.disabledClass)
                }
                $prevEl[swiper.params.watchOverflow && swiper.isLocked ? "addClass" : "removeClass"](params.lockClass)
            }
            if ($nextEl && $nextEl.length > 0) {
                if (swiper.isEnd) {
                    $nextEl.addClass(params.disabledClass)
                } else {
                    $nextEl.removeClass(params.disabledClass)
                }
                $nextEl[swiper.params.watchOverflow && swiper.isLocked ? "addClass" : "removeClass"](params.lockClass)
            }
        }, onPrevClick: function onPrevClick(e) {
            var swiper = this;
            e.preventDefault();
            if (swiper.isBeginning && !swiper.params.loop) {
                return
            }
            swiper.slidePrev()
        }, onNextClick: function onNextClick(e) {
            var swiper = this;
            e.preventDefault();
            if (swiper.isEnd && !swiper.params.loop) {
                return
            }
            swiper.slideNext()
        }, init: function init() {
            var swiper = this;
            var params = swiper.params.navigation;
            if (!(params.nextEl || params.prevEl)) {
                return
            }
            var $nextEl;
            var $prevEl;
            if (params.nextEl) {
                $nextEl = $(params.nextEl);
                if (swiper.params.uniqueNavElements && typeof params.nextEl === "string" && $nextEl.length > 1 && swiper.$el.find(params.nextEl).length === 1) {
                    $nextEl = swiper.$el.find(params.nextEl)
                }
            }
            if (params.prevEl) {
                $prevEl = $(params.prevEl);
                if (swiper.params.uniqueNavElements && typeof params.prevEl === "string" && $prevEl.length > 1 && swiper.$el.find(params.prevEl).length === 1) {
                    $prevEl = swiper.$el.find(params.prevEl)
                }
            }
            if ($nextEl && $nextEl.length > 0) {
                $nextEl.on("click", swiper.navigation.onNextClick)
            }
            if ($prevEl && $prevEl.length > 0) {
                $prevEl.on("click", swiper.navigation.onPrevClick)
            }
            Utils.extend(swiper.navigation, {
                $nextEl: $nextEl,
                nextEl: $nextEl && $nextEl[0],
                $prevEl: $prevEl,
                prevEl: $prevEl && $prevEl[0]
            })
        }, destroy: function destroy() {
            var swiper = this;
            var ref = swiper.navigation;
            var $nextEl = ref.$nextEl;
            var $prevEl = ref.$prevEl;
            if ($nextEl && $nextEl.length) {
                $nextEl.off("click", swiper.navigation.onNextClick);
                $nextEl.removeClass(swiper.params.navigation.disabledClass)
            }
            if ($prevEl && $prevEl.length) {
                $prevEl.off("click", swiper.navigation.onPrevClick);
                $prevEl.removeClass(swiper.params.navigation.disabledClass)
            }
        }
    };
    var Navigation$1 = {
        name: "navigation",
        params: {
            navigation: {
                nextEl: null,
                prevEl: null,
                hideOnClick: false,
                disabledClass: "swiper-button-disabled",
                hiddenClass: "swiper-button-hidden",
                lockClass: "swiper-button-lock"
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                navigation: {
                    init: Navigation.init.bind(swiper),
                    update: Navigation.update.bind(swiper),
                    destroy: Navigation.destroy.bind(swiper),
                    onNextClick: Navigation.onNextClick.bind(swiper),
                    onPrevClick: Navigation.onPrevClick.bind(swiper)
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                swiper.navigation.init();
                swiper.navigation.update()
            }, toEdge: function toEdge() {
                var swiper = this;
                swiper.navigation.update()
            }, fromEdge: function fromEdge() {
                var swiper = this;
                swiper.navigation.update()
            }, destroy: function destroy() {
                var swiper = this;
                swiper.navigation.destroy()
            }, click: function click(e) {
                var swiper = this;
                var ref = swiper.navigation;
                var $nextEl = ref.$nextEl;
                var $prevEl = ref.$prevEl;
                if (swiper.params.navigation.hideOnClick && !$(e.target).is($prevEl) && !$(e.target).is($nextEl)) {
                    var isHidden;
                    if ($nextEl) {
                        isHidden = $nextEl.hasClass(swiper.params.navigation.hiddenClass)
                    } else if ($prevEl) {
                        isHidden = $prevEl.hasClass(swiper.params.navigation.hiddenClass)
                    }
                    if (isHidden === true) {
                        swiper.emit("navigationShow", swiper)
                    } else {
                        swiper.emit("navigationHide", swiper)
                    }
                    if ($nextEl) {
                        $nextEl.toggleClass(swiper.params.navigation.hiddenClass)
                    }
                    if ($prevEl) {
                        $prevEl.toggleClass(swiper.params.navigation.hiddenClass)
                    }
                }
            }
        }
    };
    var Pagination = {
        update: function update() {
            var swiper = this;
            var rtl = swiper.rtl;
            var params = swiper.params.pagination;
            if (!params.el || !swiper.pagination.el || !swiper.pagination.$el || swiper.pagination.$el.length === 0) {
                return
            }
            var slidesLength = swiper.virtual && swiper.params.virtual.enabled ? swiper.virtual.slides.length : swiper.slides.length;
            var $el = swiper.pagination.$el;
            var current;
            var total = swiper.params.loop ? Math.ceil((slidesLength - swiper.loopedSlides * 2) / swiper.params.slidesPerGroup) : swiper.snapGrid.length;
            if (swiper.params.loop) {
                current = Math.ceil((swiper.activeIndex - swiper.loopedSlides) / swiper.params.slidesPerGroup);
                if (current > slidesLength - 1 - swiper.loopedSlides * 2) {
                    current -= slidesLength - swiper.loopedSlides * 2
                }
                if (current > total - 1) {
                    current -= total
                }
                if (current < 0 && swiper.params.paginationType !== "bullets") {
                    current = total + current
                }
            } else if (typeof swiper.snapIndex !== "undefined") {
                current = swiper.snapIndex
            } else {
                current = swiper.activeIndex || 0
            }
            if (params.type === "bullets" && swiper.pagination.bullets && swiper.pagination.bullets.length > 0) {
                var bullets = swiper.pagination.bullets;
                var firstIndex;
                var lastIndex;
                var midIndex;
                if (params.dynamicBullets) {
                    swiper.pagination.bulletSize = bullets.eq(0)[swiper.isHorizontal() ? "outerWidth" : "outerHeight"](true);
                    $el.css(swiper.isHorizontal() ? "width" : "height", swiper.pagination.bulletSize * (params.dynamicMainBullets + 4) + "px");
                    if (params.dynamicMainBullets > 1 && swiper.previousIndex !== undefined) {
                        swiper.pagination.dynamicBulletIndex += current - swiper.previousIndex;
                        if (swiper.pagination.dynamicBulletIndex > params.dynamicMainBullets - 1) {
                            swiper.pagination.dynamicBulletIndex = params.dynamicMainBullets - 1
                        } else if (swiper.pagination.dynamicBulletIndex < 0) {
                            swiper.pagination.dynamicBulletIndex = 0
                        }
                    }
                    firstIndex = current - swiper.pagination.dynamicBulletIndex;
                    lastIndex = firstIndex + (Math.min(bullets.length, params.dynamicMainBullets) - 1);
                    midIndex = (lastIndex + firstIndex) / 2
                }
                bullets.removeClass(params.bulletActiveClass + " " + params.bulletActiveClass + "-next " + params.bulletActiveClass + "-next-next " + params.bulletActiveClass + "-prev " + params.bulletActiveClass + "-prev-prev " + params.bulletActiveClass + "-main");
                if ($el.length > 1) {
                    bullets.each(function (index, bullet) {
                        var $bullet = $(bullet);
                        var bulletIndex = $bullet.index();
                        if (bulletIndex === current) {
                            $bullet.addClass(params.bulletActiveClass)
                        }
                        if (params.dynamicBullets) {
                            if (bulletIndex >= firstIndex && bulletIndex <= lastIndex) {
                                $bullet.addClass(params.bulletActiveClass + "-main")
                            }
                            if (bulletIndex === firstIndex) {
                                $bullet.prev().addClass(params.bulletActiveClass + "-prev").prev().addClass(params.bulletActiveClass + "-prev-prev")
                            }
                            if (bulletIndex === lastIndex) {
                                $bullet.next().addClass(params.bulletActiveClass + "-next").next().addClass(params.bulletActiveClass + "-next-next")
                            }
                        }
                    })
                } else {
                    var $bullet = bullets.eq(current);
                    var bulletIndex = $bullet.index();
                    $bullet.addClass(params.bulletActiveClass);
                    if (params.dynamicBullets) {
                        var $firstDisplayedBullet = bullets.eq(firstIndex);
                        var $lastDisplayedBullet = bullets.eq(lastIndex);
                        for (var i = firstIndex; i <= lastIndex; i += 1) {
                            bullets.eq(i).addClass(params.bulletActiveClass + "-main")
                        }
                        if (swiper.params.loop) {
                            if (bulletIndex >= bullets.length - params.dynamicMainBullets) {
                                for (var i$1 = params.dynamicMainBullets; i$1 >= 0; i$1 -= 1) {
                                    bullets.eq(bullets.length - i$1).addClass(params.bulletActiveClass + "-main")
                                }
                                bullets.eq(bullets.length - params.dynamicMainBullets - 1).addClass(params.bulletActiveClass + "-prev")
                            } else {
                                $firstDisplayedBullet.prev().addClass(params.bulletActiveClass + "-prev").prev().addClass(params.bulletActiveClass + "-prev-prev");
                                $lastDisplayedBullet.next().addClass(params.bulletActiveClass + "-next").next().addClass(params.bulletActiveClass + "-next-next")
                            }
                        } else {
                            $firstDisplayedBullet.prev().addClass(params.bulletActiveClass + "-prev").prev().addClass(params.bulletActiveClass + "-prev-prev");
                            $lastDisplayedBullet.next().addClass(params.bulletActiveClass + "-next").next().addClass(params.bulletActiveClass + "-next-next")
                        }
                    }
                }
                if (params.dynamicBullets) {
                    var dynamicBulletsLength = Math.min(bullets.length, params.dynamicMainBullets + 4);
                    var bulletsOffset = (swiper.pagination.bulletSize * dynamicBulletsLength - swiper.pagination.bulletSize) / 2 - midIndex * swiper.pagination.bulletSize;
                    var offsetProp = rtl ? "right" : "left";
                    bullets.css(swiper.isHorizontal() ? offsetProp : "top", bulletsOffset + "px")
                }
            }
            if (params.type === "fraction") {
                $el.find("." + params.currentClass).text(params.formatFractionCurrent(current + 1));
                $el.find("." + params.totalClass).text(params.formatFractionTotal(total))
            }
            if (params.type === "progressbar") {
                var progressbarDirection;
                if (params.progressbarOpposite) {
                    progressbarDirection = swiper.isHorizontal() ? "vertical" : "horizontal"
                } else {
                    progressbarDirection = swiper.isHorizontal() ? "horizontal" : "vertical"
                }
                var scale = (current + 1) / total;
                var scaleX = 1;
                var scaleY = 1;
                if (progressbarDirection === "horizontal") {
                    scaleX = scale
                } else {
                    scaleY = scale
                }
                $el.find("." + params.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + scaleX + ") scaleY(" + scaleY + ")").transition(swiper.params.speed)
            }
            if (params.type === "custom" && params.renderCustom) {
                $el.html(params.renderCustom(swiper, current + 1, total));
                swiper.emit("paginationRender", swiper, $el[0])
            } else {
                swiper.emit("paginationUpdate", swiper, $el[0])
            }
            $el[swiper.params.watchOverflow && swiper.isLocked ? "addClass" : "removeClass"](params.lockClass)
        }, render: function render() {
            var swiper = this;
            var params = swiper.params.pagination;
            if (!params.el || !swiper.pagination.el || !swiper.pagination.$el || swiper.pagination.$el.length === 0) {
                return
            }
            var slidesLength = swiper.virtual && swiper.params.virtual.enabled ? swiper.virtual.slides.length : swiper.slides.length;
            var $el = swiper.pagination.$el;
            var paginationHTML = "";
            if (params.type === "bullets") {
                var numberOfBullets = swiper.params.loop ? Math.ceil((slidesLength - swiper.loopedSlides * 2) / swiper.params.slidesPerGroup) : swiper.snapGrid.length;
                for (var i = 0; i < numberOfBullets; i += 1) {
                    if (params.renderBullet) {
                        paginationHTML += params.renderBullet.call(swiper, i, params.bulletClass)
                    } else {
                        paginationHTML += "<" + params.bulletElement + ' class="' + params.bulletClass + '"></' + params.bulletElement + ">"
                    }
                }
                $el.html(paginationHTML);
                swiper.pagination.bullets = $el.find("." + params.bulletClass)
            }
            if (params.type === "fraction") {
                if (params.renderFraction) {
                    paginationHTML = params.renderFraction.call(swiper, params.currentClass, params.totalClass)
                } else {
                    paginationHTML = '<span class="' + params.currentClass + '"></span>' + " / " + '<span class="' + params.totalClass + '"></span>'
                }
                $el.html(paginationHTML)
            }
            if (params.type === "progressbar") {
                if (params.renderProgressbar) {
                    paginationHTML = params.renderProgressbar.call(swiper, params.progressbarFillClass)
                } else {
                    paginationHTML = '<span class="' + params.progressbarFillClass + '"></span>'
                }
                $el.html(paginationHTML)
            }
            if (params.type !== "custom") {
                swiper.emit("paginationRender", swiper.pagination.$el[0])
            }
        }, init: function init() {
            var swiper = this;
            var params = swiper.params.pagination;
            if (!params.el) {
                return
            }
            var $el = $(params.el);
            if ($el.length === 0) {
                return
            }
            if (swiper.params.uniqueNavElements && typeof params.el === "string" && $el.length > 1) {
                $el = swiper.$el.find(params.el)
            }
            if (params.type === "bullets" && params.clickable) {
                $el.addClass(params.clickableClass)
            }
            $el.addClass(params.modifierClass + params.type);
            if (params.type === "bullets" && params.dynamicBullets) {
                $el.addClass("" + params.modifierClass + params.type + "-dynamic");
                swiper.pagination.dynamicBulletIndex = 0;
                if (params.dynamicMainBullets < 1) {
                    params.dynamicMainBullets = 1
                }
            }
            if (params.type === "progressbar" && params.progressbarOpposite) {
                $el.addClass(params.progressbarOppositeClass)
            }
            if (params.clickable) {
                $el.on("click", "." + params.bulletClass, function onClick(e) {
                    e.preventDefault();
                    var index = $(this).index() * swiper.params.slidesPerGroup;
                    if (swiper.params.loop) {
                        index += swiper.loopedSlides
                    }
                    swiper.slideTo(index)
                })
            }
            Utils.extend(swiper.pagination, {$el: $el, el: $el[0]})
        }, destroy: function destroy() {
            var swiper = this;
            var params = swiper.params.pagination;
            if (!params.el || !swiper.pagination.el || !swiper.pagination.$el || swiper.pagination.$el.length === 0) {
                return
            }
            var $el = swiper.pagination.$el;
            $el.removeClass(params.hiddenClass);
            $el.removeClass(params.modifierClass + params.type);
            if (swiper.pagination.bullets) {
                swiper.pagination.bullets.removeClass(params.bulletActiveClass)
            }
            if (params.clickable) {
                $el.off("click", "." + params.bulletClass)
            }
        }
    };
    var Pagination$1 = {
        name: "pagination",
        params: {
            pagination: {
                el: null,
                bulletElement: "span",
                clickable: false,
                hideOnClick: false,
                renderBullet: null,
                renderProgressbar: null,
                renderFraction: null,
                renderCustom: null,
                progressbarOpposite: false,
                type: "bullets",
                dynamicBullets: false,
                dynamicMainBullets: 1,
                formatFractionCurrent: function (number) {
                    return number
                },
                formatFractionTotal: function (number) {
                    return number
                },
                bulletClass: "swiper-pagination-bullet",
                bulletActiveClass: "swiper-pagination-bullet-active",
                modifierClass: "swiper-pagination-",
                currentClass: "swiper-pagination-current",
                totalClass: "swiper-pagination-total",
                hiddenClass: "swiper-pagination-hidden",
                progressbarFillClass: "swiper-pagination-progressbar-fill",
                progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
                clickableClass: "swiper-pagination-clickable",
                lockClass: "swiper-pagination-lock"
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                pagination: {
                    init: Pagination.init.bind(swiper),
                    render: Pagination.render.bind(swiper),
                    update: Pagination.update.bind(swiper),
                    destroy: Pagination.destroy.bind(swiper),
                    dynamicBulletIndex: 0
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                swiper.pagination.init();
                swiper.pagination.render();
                swiper.pagination.update()
            }, activeIndexChange: function activeIndexChange() {
                var swiper = this;
                if (swiper.params.loop) {
                    swiper.pagination.update()
                } else if (typeof swiper.snapIndex === "undefined") {
                    swiper.pagination.update()
                }
            }, snapIndexChange: function snapIndexChange() {
                var swiper = this;
                if (!swiper.params.loop) {
                    swiper.pagination.update()
                }
            }, slidesLengthChange: function slidesLengthChange() {
                var swiper = this;
                if (swiper.params.loop) {
                    swiper.pagination.render();
                    swiper.pagination.update()
                }
            }, snapGridLengthChange: function snapGridLengthChange() {
                var swiper = this;
                if (!swiper.params.loop) {
                    swiper.pagination.render();
                    swiper.pagination.update()
                }
            }, destroy: function destroy() {
                var swiper = this;
                swiper.pagination.destroy()
            }, click: function click(e) {
                var swiper = this;
                if (swiper.params.pagination.el && swiper.params.pagination.hideOnClick && swiper.pagination.$el.length > 0 && !$(e.target).hasClass(swiper.params.pagination.bulletClass)) {
                    var isHidden = swiper.pagination.$el.hasClass(swiper.params.pagination.hiddenClass);
                    if (isHidden === true) {
                        swiper.emit("paginationShow", swiper)
                    } else {
                        swiper.emit("paginationHide", swiper)
                    }
                    swiper.pagination.$el.toggleClass(swiper.params.pagination.hiddenClass)
                }
            }
        }
    };
    var Scrollbar = {
        setTranslate: function setTranslate() {
            var swiper = this;
            if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) {
                return
            }
            var scrollbar = swiper.scrollbar;
            var rtl = swiper.rtlTranslate;
            var progress = swiper.progress;
            var dragSize = scrollbar.dragSize;
            var trackSize = scrollbar.trackSize;
            var $dragEl = scrollbar.$dragEl;
            var $el = scrollbar.$el;
            var params = swiper.params.scrollbar;
            var newSize = dragSize;
            var newPos = (trackSize - dragSize) * progress;
            if (rtl) {
                newPos = -newPos;
                if (newPos > 0) {
                    newSize = dragSize - newPos;
                    newPos = 0
                } else if (-newPos + dragSize > trackSize) {
                    newSize = trackSize + newPos
                }
            } else if (newPos < 0) {
                newSize = dragSize + newPos;
                newPos = 0
            } else if (newPos + dragSize > trackSize) {
                newSize = trackSize - newPos
            }
            if (swiper.isHorizontal()) {
                $dragEl.transform("translate3d(" + newPos + "px, 0, 0)");
                $dragEl[0].style.width = newSize + "px"
            } else {
                $dragEl.transform("translate3d(0px, " + newPos + "px, 0)");
                $dragEl[0].style.height = newSize + "px"
            }
            if (params.hide) {
                clearTimeout(swiper.scrollbar.timeout);
                $el[0].style.opacity = 1;
                swiper.scrollbar.timeout = setTimeout(function () {
                    $el[0].style.opacity = 0;
                    $el.transition(400)
                }, 1e3)
            }
        }, setTransition: function setTransition(duration) {
            var swiper = this;
            if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) {
                return
            }
            swiper.scrollbar.$dragEl.transition(duration)
        }, updateSize: function updateSize() {
            var swiper = this;
            if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) {
                return
            }
            var scrollbar = swiper.scrollbar;
            var $dragEl = scrollbar.$dragEl;
            var $el = scrollbar.$el;
            $dragEl[0].style.width = "";
            $dragEl[0].style.height = "";
            var trackSize = swiper.isHorizontal() ? $el[0].offsetWidth : $el[0].offsetHeight;
            var divider = swiper.size / swiper.virtualSize;
            var moveDivider = divider * (trackSize / swiper.size);
            var dragSize;
            if (swiper.params.scrollbar.dragSize === "auto") {
                dragSize = trackSize * divider
            } else {
                dragSize = parseInt(swiper.params.scrollbar.dragSize, 10)
            }
            if (swiper.isHorizontal()) {
                $dragEl[0].style.width = dragSize + "px"
            } else {
                $dragEl[0].style.height = dragSize + "px"
            }
            if (divider >= 1) {
                $el[0].style.display = "none"
            } else {
                $el[0].style.display = ""
            }
            if (swiper.params.scrollbar.hide) {
                $el[0].style.opacity = 0
            }
            Utils.extend(scrollbar, {
                trackSize: trackSize,
                divider: divider,
                moveDivider: moveDivider,
                dragSize: dragSize
            });
            scrollbar.$el[swiper.params.watchOverflow && swiper.isLocked ? "addClass" : "removeClass"](swiper.params.scrollbar.lockClass)
        }, getPointerPosition: function getPointerPosition(e) {
            var swiper = this;
            if (swiper.isHorizontal()) {
                return e.type === "touchstart" || e.type === "touchmove" ? e.targetTouches[0].clientX : e.clientX
            }
            return e.type === "touchstart" || e.type === "touchmove" ? e.targetTouches[0].clientY : e.clientY
        }, setDragPosition: function setDragPosition(e) {
            var swiper = this;
            var scrollbar = swiper.scrollbar;
            var rtl = swiper.rtlTranslate;
            var $el = scrollbar.$el;
            var dragSize = scrollbar.dragSize;
            var trackSize = scrollbar.trackSize;
            var dragStartPos = scrollbar.dragStartPos;
            var positionRatio;
            positionRatio = (scrollbar.getPointerPosition(e) - $el.offset()[swiper.isHorizontal() ? "left" : "top"] - (dragStartPos !== null ? dragStartPos : dragSize / 2)) / (trackSize - dragSize);
            positionRatio = Math.max(Math.min(positionRatio, 1), 0);
            if (rtl) {
                positionRatio = 1 - positionRatio
            }
            var position = swiper.minTranslate() + (swiper.maxTranslate() - swiper.minTranslate()) * positionRatio;
            swiper.updateProgress(position);
            swiper.setTranslate(position);
            swiper.updateActiveIndex();
            swiper.updateSlidesClasses()
        }, onDragStart: function onDragStart(e) {
            var swiper = this;
            var params = swiper.params.scrollbar;
            var scrollbar = swiper.scrollbar;
            var $wrapperEl = swiper.$wrapperEl;
            var $el = scrollbar.$el;
            var $dragEl = scrollbar.$dragEl;
            swiper.scrollbar.isTouched = true;
            swiper.scrollbar.dragStartPos = e.target === $dragEl[0] || e.target === $dragEl ? scrollbar.getPointerPosition(e) - e.target.getBoundingClientRect()[swiper.isHorizontal() ? "left" : "top"] : null;
            e.preventDefault();
            e.stopPropagation();
            $wrapperEl.transition(100);
            $dragEl.transition(100);
            scrollbar.setDragPosition(e);
            clearTimeout(swiper.scrollbar.dragTimeout);
            $el.transition(0);
            if (params.hide) {
                $el.css("opacity", 1)
            }
            if (swiper.params.cssMode) {
                swiper.$wrapperEl.css("scroll-snap-type", "none")
            }
            swiper.emit("scrollbarDragStart", e)
        }, onDragMove: function onDragMove(e) {
            var swiper = this;
            var scrollbar = swiper.scrollbar;
            var $wrapperEl = swiper.$wrapperEl;
            var $el = scrollbar.$el;
            var $dragEl = scrollbar.$dragEl;
            if (!swiper.scrollbar.isTouched) {
                return
            }
            if (e.preventDefault) {
                e.preventDefault()
            } else {
                e.returnValue = false
            }
            scrollbar.setDragPosition(e);
            $wrapperEl.transition(0);
            $el.transition(0);
            $dragEl.transition(0);
            swiper.emit("scrollbarDragMove", e)
        }, onDragEnd: function onDragEnd(e) {
            var swiper = this;
            var params = swiper.params.scrollbar;
            var scrollbar = swiper.scrollbar;
            var $wrapperEl = swiper.$wrapperEl;
            var $el = scrollbar.$el;
            if (!swiper.scrollbar.isTouched) {
                return
            }
            swiper.scrollbar.isTouched = false;
            if (swiper.params.cssMode) {
                swiper.$wrapperEl.css("scroll-snap-type", "");
                $wrapperEl.transition("")
            }
            if (params.hide) {
                clearTimeout(swiper.scrollbar.dragTimeout);
                swiper.scrollbar.dragTimeout = Utils.nextTick(function () {
                    $el.css("opacity", 0);
                    $el.transition(400)
                }, 1e3)
            }
            swiper.emit("scrollbarDragEnd", e);
            if (params.snapOnRelease) {
                swiper.slideToClosest()
            }
        }, enableDraggable: function enableDraggable() {
            var swiper = this;
            if (!swiper.params.scrollbar.el) {
                return
            }
            var scrollbar = swiper.scrollbar;
            var touchEventsTouch = swiper.touchEventsTouch;
            var touchEventsDesktop = swiper.touchEventsDesktop;
            var params = swiper.params;
            var $el = scrollbar.$el;
            var target = $el[0];
            var activeListener = Support.passiveListener && params.passiveListeners ? {
                passive: false,
                capture: false
            } : false;
            var passiveListener = Support.passiveListener && params.passiveListeners ? {
                passive: true,
                capture: false
            } : false;
            if (!Support.touch) {
                target.addEventListener(touchEventsDesktop.start, swiper.scrollbar.onDragStart, activeListener);
                doc.addEventListener(touchEventsDesktop.move, swiper.scrollbar.onDragMove, activeListener);
                doc.addEventListener(touchEventsDesktop.end, swiper.scrollbar.onDragEnd, passiveListener)
            } else {
                target.addEventListener(touchEventsTouch.start, swiper.scrollbar.onDragStart, activeListener);
                target.addEventListener(touchEventsTouch.move, swiper.scrollbar.onDragMove, activeListener);
                target.addEventListener(touchEventsTouch.end, swiper.scrollbar.onDragEnd, passiveListener)
            }
        }, disableDraggable: function disableDraggable() {
            var swiper = this;
            if (!swiper.params.scrollbar.el) {
                return
            }
            var scrollbar = swiper.scrollbar;
            var touchEventsTouch = swiper.touchEventsTouch;
            var touchEventsDesktop = swiper.touchEventsDesktop;
            var params = swiper.params;
            var $el = scrollbar.$el;
            var target = $el[0];
            var activeListener = Support.passiveListener && params.passiveListeners ? {
                passive: false,
                capture: false
            } : false;
            var passiveListener = Support.passiveListener && params.passiveListeners ? {
                passive: true,
                capture: false
            } : false;
            if (!Support.touch) {
                target.removeEventListener(touchEventsDesktop.start, swiper.scrollbar.onDragStart, activeListener);
                doc.removeEventListener(touchEventsDesktop.move, swiper.scrollbar.onDragMove, activeListener);
                doc.removeEventListener(touchEventsDesktop.end, swiper.scrollbar.onDragEnd, passiveListener)
            } else {
                target.removeEventListener(touchEventsTouch.start, swiper.scrollbar.onDragStart, activeListener);
                target.removeEventListener(touchEventsTouch.move, swiper.scrollbar.onDragMove, activeListener);
                target.removeEventListener(touchEventsTouch.end, swiper.scrollbar.onDragEnd, passiveListener)
            }
        }, init: function init() {
            var swiper = this;
            if (!swiper.params.scrollbar.el) {
                return
            }
            var scrollbar = swiper.scrollbar;
            var $swiperEl = swiper.$el;
            var params = swiper.params.scrollbar;
            var $el = $(params.el);
            if (swiper.params.uniqueNavElements && typeof params.el === "string" && $el.length > 1 && $swiperEl.find(params.el).length === 1) {
                $el = $swiperEl.find(params.el)
            }
            var $dragEl = $el.find("." + swiper.params.scrollbar.dragClass);
            if ($dragEl.length === 0) {
                $dragEl = $('<div class="' + swiper.params.scrollbar.dragClass + '"></div>');
                $el.append($dragEl)
            }
            Utils.extend(scrollbar, {$el: $el, el: $el[0], $dragEl: $dragEl, dragEl: $dragEl[0]});
            if (params.draggable) {
                scrollbar.enableDraggable()
            }
        }, destroy: function destroy() {
            var swiper = this;
            swiper.scrollbar.disableDraggable()
        }
    };
    var Scrollbar$1 = {
        name: "scrollbar",
        params: {
            scrollbar: {
                el: null,
                dragSize: "auto",
                hide: false,
                draggable: false,
                snapOnRelease: true,
                lockClass: "swiper-scrollbar-lock",
                dragClass: "swiper-scrollbar-drag"
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                scrollbar: {
                    init: Scrollbar.init.bind(swiper),
                    destroy: Scrollbar.destroy.bind(swiper),
                    updateSize: Scrollbar.updateSize.bind(swiper),
                    setTranslate: Scrollbar.setTranslate.bind(swiper),
                    setTransition: Scrollbar.setTransition.bind(swiper),
                    enableDraggable: Scrollbar.enableDraggable.bind(swiper),
                    disableDraggable: Scrollbar.disableDraggable.bind(swiper),
                    setDragPosition: Scrollbar.setDragPosition.bind(swiper),
                    getPointerPosition: Scrollbar.getPointerPosition.bind(swiper),
                    onDragStart: Scrollbar.onDragStart.bind(swiper),
                    onDragMove: Scrollbar.onDragMove.bind(swiper),
                    onDragEnd: Scrollbar.onDragEnd.bind(swiper),
                    isTouched: false,
                    timeout: null,
                    dragTimeout: null
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                swiper.scrollbar.init();
                swiper.scrollbar.updateSize();
                swiper.scrollbar.setTranslate()
            }, update: function update() {
                var swiper = this;
                swiper.scrollbar.updateSize()
            }, resize: function resize() {
                var swiper = this;
                swiper.scrollbar.updateSize()
            }, observerUpdate: function observerUpdate() {
                var swiper = this;
                swiper.scrollbar.updateSize()
            }, setTranslate: function setTranslate() {
                var swiper = this;
                swiper.scrollbar.setTranslate()
            }, setTransition: function setTransition(duration) {
                var swiper = this;
                swiper.scrollbar.setTransition(duration)
            }, destroy: function destroy() {
                var swiper = this;
                swiper.scrollbar.destroy()
            }
        }
    };
    var Parallax = {
        setTransform: function setTransform(el, progress) {
            var swiper = this;
            var rtl = swiper.rtl;
            var $el = $(el);
            var rtlFactor = rtl ? -1 : 1;
            var p = $el.attr("data-swiper-parallax") || "0";
            var x = $el.attr("data-swiper-parallax-x");
            var y = $el.attr("data-swiper-parallax-y");
            var scale = $el.attr("data-swiper-parallax-scale");
            var opacity = $el.attr("data-swiper-parallax-opacity");
            if (x || y) {
                x = x || "0";
                y = y || "0"
            } else if (swiper.isHorizontal()) {
                x = p;
                y = "0"
            } else {
                y = p;
                x = "0"
            }
            if (x.indexOf("%") >= 0) {
                x = parseInt(x, 10) * progress * rtlFactor + "%"
            } else {
                x = x * progress * rtlFactor + "px"
            }
            if (y.indexOf("%") >= 0) {
                y = parseInt(y, 10) * progress + "%"
            } else {
                y = y * progress + "px"
            }
            if (typeof opacity !== "undefined" && opacity !== null) {
                var currentOpacity = opacity - (opacity - 1) * (1 - Math.abs(progress));
                $el[0].style.opacity = currentOpacity
            }
            if (typeof scale === "undefined" || scale === null) {
                $el.transform("translate3d(" + x + ", " + y + ", 0px)")
            } else {
                var currentScale = scale - (scale - 1) * (1 - Math.abs(progress));
                $el.transform("translate3d(" + x + ", " + y + ", 0px) scale(" + currentScale + ")")
            }
        }, setTranslate: function setTranslate() {
            var swiper = this;
            var $el = swiper.$el;
            var slides = swiper.slides;
            var progress = swiper.progress;
            var snapGrid = swiper.snapGrid;
            $el.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (index, el) {
                swiper.parallax.setTransform(el, progress)
            });
            slides.each(function (slideIndex, slideEl) {
                var slideProgress = slideEl.progress;
                if (swiper.params.slidesPerGroup > 1 && swiper.params.slidesPerView !== "auto") {
                    slideProgress += Math.ceil(slideIndex / 2) - progress * (snapGrid.length - 1)
                }
                slideProgress = Math.min(Math.max(slideProgress, -1), 1);
                $(slideEl).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (index, el) {
                    swiper.parallax.setTransform(el, slideProgress)
                })
            })
        }, setTransition: function setTransition(duration) {
            if (duration === void 0) duration = this.params.speed;
            var swiper = this;
            var $el = swiper.$el;
            $el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (index, parallaxEl) {
                var $parallaxEl = $(parallaxEl);
                var parallaxDuration = parseInt($parallaxEl.attr("data-swiper-parallax-duration"), 10) || duration;
                if (duration === 0) {
                    parallaxDuration = 0
                }
                $parallaxEl.transition(parallaxDuration)
            })
        }
    };
    var Parallax$1 = {
        name: "parallax", params: {parallax: {enabled: false}}, create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                parallax: {
                    setTransform: Parallax.setTransform.bind(swiper),
                    setTranslate: Parallax.setTranslate.bind(swiper),
                    setTransition: Parallax.setTransition.bind(swiper)
                }
            })
        }, on: {
            beforeInit: function beforeInit() {
                var swiper = this;
                if (!swiper.params.parallax.enabled) {
                    return
                }
                swiper.params.watchSlidesProgress = true;
                swiper.originalParams.watchSlidesProgress = true
            }, init: function init() {
                var swiper = this;
                if (!swiper.params.parallax.enabled) {
                    return
                }
                swiper.parallax.setTranslate()
            }, setTranslate: function setTranslate() {
                var swiper = this;
                if (!swiper.params.parallax.enabled) {
                    return
                }
                swiper.parallax.setTranslate()
            }, setTransition: function setTransition(duration) {
                var swiper = this;
                if (!swiper.params.parallax.enabled) {
                    return
                }
                swiper.parallax.setTransition(duration)
            }
        }
    };
    var Zoom = {
        getDistanceBetweenTouches: function getDistanceBetweenTouches(e) {
            if (e.targetTouches.length < 2) {
                return 1
            }
            var x1 = e.targetTouches[0].pageX;
            var y1 = e.targetTouches[0].pageY;
            var x2 = e.targetTouches[1].pageX;
            var y2 = e.targetTouches[1].pageY;
            var distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
            return distance
        }, onGestureStart: function onGestureStart(e) {
            var swiper = this;
            var params = swiper.params.zoom;
            var zoom = swiper.zoom;
            var gesture = zoom.gesture;
            zoom.fakeGestureTouched = false;
            zoom.fakeGestureMoved = false;
            if (!Support.gestures) {
                if (e.type !== "touchstart" || e.type === "touchstart" && e.targetTouches.length < 2) {
                    return
                }
                zoom.fakeGestureTouched = true;
                gesture.scaleStart = Zoom.getDistanceBetweenTouches(e)
            }
            if (!gesture.$slideEl || !gesture.$slideEl.length) {
                gesture.$slideEl = $(e.target).closest("." + swiper.params.slideClass);
                if (gesture.$slideEl.length === 0) {
                    gesture.$slideEl = swiper.slides.eq(swiper.activeIndex)
                }
                gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas, picture, .swiper-zoom-target");
                gesture.$imageWrapEl = gesture.$imageEl.parent("." + params.containerClass);
                gesture.maxRatio = gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;
                if (gesture.$imageWrapEl.length === 0) {
                    gesture.$imageEl = undefined;
                    return
                }
            }
            if (gesture.$imageEl) {
                gesture.$imageEl.transition(0)
            }
            swiper.zoom.isScaling = true
        }, onGestureChange: function onGestureChange(e) {
            var swiper = this;
            var params = swiper.params.zoom;
            var zoom = swiper.zoom;
            var gesture = zoom.gesture;
            if (!Support.gestures) {
                if (e.type !== "touchmove" || e.type === "touchmove" && e.targetTouches.length < 2) {
                    return
                }
                zoom.fakeGestureMoved = true;
                gesture.scaleMove = Zoom.getDistanceBetweenTouches(e)
            }
            if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
                return
            }
            if (Support.gestures) {
                zoom.scale = e.scale * zoom.currentScale
            } else {
                zoom.scale = gesture.scaleMove / gesture.scaleStart * zoom.currentScale
            }
            if (zoom.scale > gesture.maxRatio) {
                zoom.scale = gesture.maxRatio - 1 + Math.pow(zoom.scale - gesture.maxRatio + 1, .5)
            }
            if (zoom.scale < params.minRatio) {
                zoom.scale = params.minRatio + 1 - Math.pow(params.minRatio - zoom.scale + 1, .5)
            }
            gesture.$imageEl.transform("translate3d(0,0,0) scale(" + zoom.scale + ")")
        }, onGestureEnd: function onGestureEnd(e) {
            var swiper = this;
            var params = swiper.params.zoom;
            var zoom = swiper.zoom;
            var gesture = zoom.gesture;
            if (!Support.gestures) {
                if (!zoom.fakeGestureTouched || !zoom.fakeGestureMoved) {
                    return
                }
                if (e.type !== "touchend" || e.type === "touchend" && e.changedTouches.length < 2 && !Device.android) {
                    return
                }
                zoom.fakeGestureTouched = false;
                zoom.fakeGestureMoved = false
            }
            if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
                return
            }
            zoom.scale = Math.max(Math.min(zoom.scale, gesture.maxRatio), params.minRatio);
            gesture.$imageEl.transition(swiper.params.speed).transform("translate3d(0,0,0) scale(" + zoom.scale + ")");
            zoom.currentScale = zoom.scale;
            zoom.isScaling = false;
            if (zoom.scale === 1) {
                gesture.$slideEl = undefined
            }
        }, onTouchStart: function onTouchStart(e) {
            var swiper = this;
            var zoom = swiper.zoom;
            var gesture = zoom.gesture;
            var image = zoom.image;
            if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
                return
            }
            if (image.isTouched) {
                return
            }
            if (Device.android && e.cancelable) {
                e.preventDefault()
            }
            image.isTouched = true;
            image.touchesStart.x = e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX;
            image.touchesStart.y = e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY
        }, onTouchMove: function onTouchMove(e) {
            var swiper = this;
            var zoom = swiper.zoom;
            var gesture = zoom.gesture;
            var image = zoom.image;
            var velocity = zoom.velocity;
            if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
                return
            }
            swiper.allowClick = false;
            if (!image.isTouched || !gesture.$slideEl) {
                return
            }
            if (!image.isMoved) {
                image.width = gesture.$imageEl[0].offsetWidth;
                image.height = gesture.$imageEl[0].offsetHeight;
                image.startX = Utils.getTranslate(gesture.$imageWrapEl[0], "x") || 0;
                image.startY = Utils.getTranslate(gesture.$imageWrapEl[0], "y") || 0;
                gesture.slideWidth = gesture.$slideEl[0].offsetWidth;
                gesture.slideHeight = gesture.$slideEl[0].offsetHeight;
                gesture.$imageWrapEl.transition(0);
                if (swiper.rtl) {
                    image.startX = -image.startX;
                    image.startY = -image.startY
                }
            }
            var scaledWidth = image.width * zoom.scale;
            var scaledHeight = image.height * zoom.scale;
            if (scaledWidth < gesture.slideWidth && scaledHeight < gesture.slideHeight) {
                return
            }
            image.minX = Math.min(gesture.slideWidth / 2 - scaledWidth / 2, 0);
            image.maxX = -image.minX;
            image.minY = Math.min(gesture.slideHeight / 2 - scaledHeight / 2, 0);
            image.maxY = -image.minY;
            image.touchesCurrent.x = e.type === "touchmove" ? e.targetTouches[0].pageX : e.pageX;
            image.touchesCurrent.y = e.type === "touchmove" ? e.targetTouches[0].pageY : e.pageY;
            if (!image.isMoved && !zoom.isScaling) {
                if (swiper.isHorizontal() && (Math.floor(image.minX) === Math.floor(image.startX) && image.touchesCurrent.x < image.touchesStart.x || Math.floor(image.maxX) === Math.floor(image.startX) && image.touchesCurrent.x > image.touchesStart.x)) {
                    image.isTouched = false;
                    return
                }
                if (!swiper.isHorizontal() && (Math.floor(image.minY) === Math.floor(image.startY) && image.touchesCurrent.y < image.touchesStart.y || Math.floor(image.maxY) === Math.floor(image.startY) && image.touchesCurrent.y > image.touchesStart.y)) {
                    image.isTouched = false;
                    return
                }
            }
            if (e.cancelable) {
                e.preventDefault()
            }
            e.stopPropagation();
            image.isMoved = true;
            image.currentX = image.touchesCurrent.x - image.touchesStart.x + image.startX;
            image.currentY = image.touchesCurrent.y - image.touchesStart.y + image.startY;
            if (image.currentX < image.minX) {
                image.currentX = image.minX + 1 - Math.pow(image.minX - image.currentX + 1, .8)
            }
            if (image.currentX > image.maxX) {
                image.currentX = image.maxX - 1 + Math.pow(image.currentX - image.maxX + 1, .8)
            }
            if (image.currentY < image.minY) {
                image.currentY = image.minY + 1 - Math.pow(image.minY - image.currentY + 1, .8)
            }
            if (image.currentY > image.maxY) {
                image.currentY = image.maxY - 1 + Math.pow(image.currentY - image.maxY + 1, .8)
            }
            if (!velocity.prevPositionX) {
                velocity.prevPositionX = image.touchesCurrent.x
            }
            if (!velocity.prevPositionY) {
                velocity.prevPositionY = image.touchesCurrent.y
            }
            if (!velocity.prevTime) {
                velocity.prevTime = Date.now()
            }
            velocity.x = (image.touchesCurrent.x - velocity.prevPositionX) / (Date.now() - velocity.prevTime) / 2;
            velocity.y = (image.touchesCurrent.y - velocity.prevPositionY) / (Date.now() - velocity.prevTime) / 2;
            if (Math.abs(image.touchesCurrent.x - velocity.prevPositionX) < 2) {
                velocity.x = 0
            }
            if (Math.abs(image.touchesCurrent.y - velocity.prevPositionY) < 2) {
                velocity.y = 0
            }
            velocity.prevPositionX = image.touchesCurrent.x;
            velocity.prevPositionY = image.touchesCurrent.y;
            velocity.prevTime = Date.now();
            gesture.$imageWrapEl.transform("translate3d(" + image.currentX + "px, " + image.currentY + "px,0)")
        }, onTouchEnd: function onTouchEnd() {
            var swiper = this;
            var zoom = swiper.zoom;
            var gesture = zoom.gesture;
            var image = zoom.image;
            var velocity = zoom.velocity;
            if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
                return
            }
            if (!image.isTouched || !image.isMoved) {
                image.isTouched = false;
                image.isMoved = false;
                return
            }
            image.isTouched = false;
            image.isMoved = false;
            var momentumDurationX = 300;
            var momentumDurationY = 300;
            var momentumDistanceX = velocity.x * momentumDurationX;
            var newPositionX = image.currentX + momentumDistanceX;
            var momentumDistanceY = velocity.y * momentumDurationY;
            var newPositionY = image.currentY + momentumDistanceY;
            if (velocity.x !== 0) {
                momentumDurationX = Math.abs((newPositionX - image.currentX) / velocity.x)
            }
            if (velocity.y !== 0) {
                momentumDurationY = Math.abs((newPositionY - image.currentY) / velocity.y)
            }
            var momentumDuration = Math.max(momentumDurationX, momentumDurationY);
            image.currentX = newPositionX;
            image.currentY = newPositionY;
            var scaledWidth = image.width * zoom.scale;
            var scaledHeight = image.height * zoom.scale;
            image.minX = Math.min(gesture.slideWidth / 2 - scaledWidth / 2, 0);
            image.maxX = -image.minX;
            image.minY = Math.min(gesture.slideHeight / 2 - scaledHeight / 2, 0);
            image.maxY = -image.minY;
            image.currentX = Math.max(Math.min(image.currentX, image.maxX), image.minX);
            image.currentY = Math.max(Math.min(image.currentY, image.maxY), image.minY);
            gesture.$imageWrapEl.transition(momentumDuration).transform("translate3d(" + image.currentX + "px, " + image.currentY + "px,0)")
        }, onTransitionEnd: function onTransitionEnd() {
            var swiper = this;
            var zoom = swiper.zoom;
            var gesture = zoom.gesture;
            if (gesture.$slideEl && swiper.previousIndex !== swiper.activeIndex) {
                if (gesture.$imageEl) {
                    gesture.$imageEl.transform("translate3d(0,0,0) scale(1)")
                }
                if (gesture.$imageWrapEl) {
                    gesture.$imageWrapEl.transform("translate3d(0,0,0)")
                }
                zoom.scale = 1;
                zoom.currentScale = 1;
                gesture.$slideEl = undefined;
                gesture.$imageEl = undefined;
                gesture.$imageWrapEl = undefined
            }
        }, toggle: function toggle(e) {
            var swiper = this;
            var zoom = swiper.zoom;
            if (zoom.scale && zoom.scale !== 1) {
                zoom.out()
            } else {
                zoom.in(e)
            }
        }, in: function in$1(e) {
            var swiper = this;
            var zoom = swiper.zoom;
            var params = swiper.params.zoom;
            var gesture = zoom.gesture;
            var image = zoom.image;
            if (!gesture.$slideEl) {
                if (swiper.params.virtual && swiper.params.virtual.enabled && swiper.virtual) {
                    gesture.$slideEl = swiper.$wrapperEl.children("." + swiper.params.slideActiveClass)
                } else {
                    gesture.$slideEl = swiper.slides.eq(swiper.activeIndex)
                }
                gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas, picture, .swiper-zoom-target");
                gesture.$imageWrapEl = gesture.$imageEl.parent("." + params.containerClass)
            }
            if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
                return
            }
            gesture.$slideEl.addClass("" + params.zoomedSlideClass);
            var touchX;
            var touchY;
            var offsetX;
            var offsetY;
            var diffX;
            var diffY;
            var translateX;
            var translateY;
            var imageWidth;
            var imageHeight;
            var scaledWidth;
            var scaledHeight;
            var translateMinX;
            var translateMinY;
            var translateMaxX;
            var translateMaxY;
            var slideWidth;
            var slideHeight;
            if (typeof image.touchesStart.x === "undefined" && e) {
                touchX = e.type === "touchend" ? e.changedTouches[0].pageX : e.pageX;
                touchY = e.type === "touchend" ? e.changedTouches[0].pageY : e.pageY
            } else {
                touchX = image.touchesStart.x;
                touchY = image.touchesStart.y
            }
            zoom.scale = gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;
            zoom.currentScale = gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;
            if (e) {
                slideWidth = gesture.$slideEl[0].offsetWidth;
                slideHeight = gesture.$slideEl[0].offsetHeight;
                offsetX = gesture.$slideEl.offset().left;
                offsetY = gesture.$slideEl.offset().top;
                diffX = offsetX + slideWidth / 2 - touchX;
                diffY = offsetY + slideHeight / 2 - touchY;
                imageWidth = gesture.$imageEl[0].offsetWidth;
                imageHeight = gesture.$imageEl[0].offsetHeight;
                scaledWidth = imageWidth * zoom.scale;
                scaledHeight = imageHeight * zoom.scale;
                translateMinX = Math.min(slideWidth / 2 - scaledWidth / 2, 0);
                translateMinY = Math.min(slideHeight / 2 - scaledHeight / 2, 0);
                translateMaxX = -translateMinX;
                translateMaxY = -translateMinY;
                translateX = diffX * zoom.scale;
                translateY = diffY * zoom.scale;
                if (translateX < translateMinX) {
                    translateX = translateMinX
                }
                if (translateX > translateMaxX) {
                    translateX = translateMaxX
                }
                if (translateY < translateMinY) {
                    translateY = translateMinY
                }
                if (translateY > translateMaxY) {
                    translateY = translateMaxY
                }
            } else {
                translateX = 0;
                translateY = 0
            }
            gesture.$imageWrapEl.transition(300).transform("translate3d(" + translateX + "px, " + translateY + "px,0)");
            gesture.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + zoom.scale + ")")
        }, out: function out() {
            var swiper = this;
            var zoom = swiper.zoom;
            var params = swiper.params.zoom;
            var gesture = zoom.gesture;
            if (!gesture.$slideEl) {
                if (swiper.params.virtual && swiper.params.virtual.enabled && swiper.virtual) {
                    gesture.$slideEl = swiper.$wrapperEl.children("." + swiper.params.slideActiveClass)
                } else {
                    gesture.$slideEl = swiper.slides.eq(swiper.activeIndex)
                }
                gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas, picture, .swiper-zoom-target");
                gesture.$imageWrapEl = gesture.$imageEl.parent("." + params.containerClass)
            }
            if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
                return
            }
            zoom.scale = 1;
            zoom.currentScale = 1;
            gesture.$imageWrapEl.transition(300).transform("translate3d(0,0,0)");
            gesture.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)");
            gesture.$slideEl.removeClass("" + params.zoomedSlideClass);
            gesture.$slideEl = undefined
        }, enable: function enable() {
            var swiper = this;
            var zoom = swiper.zoom;
            if (zoom.enabled) {
                return
            }
            zoom.enabled = true;
            var passiveListener = swiper.touchEvents.start === "touchstart" && Support.passiveListener && swiper.params.passiveListeners ? {
                passive: true,
                capture: false
            } : false;
            var activeListenerWithCapture = Support.passiveListener ? {passive: false, capture: true} : true;
            var slideSelector = "." + swiper.params.slideClass;
            if (Support.gestures) {
                swiper.$wrapperEl.on("gesturestart", slideSelector, zoom.onGestureStart, passiveListener);
                swiper.$wrapperEl.on("gesturechange", slideSelector, zoom.onGestureChange, passiveListener);
                swiper.$wrapperEl.on("gestureend", slideSelector, zoom.onGestureEnd, passiveListener)
            } else if (swiper.touchEvents.start === "touchstart") {
                swiper.$wrapperEl.on(swiper.touchEvents.start, slideSelector, zoom.onGestureStart, passiveListener);
                swiper.$wrapperEl.on(swiper.touchEvents.move, slideSelector, zoom.onGestureChange, activeListenerWithCapture);
                swiper.$wrapperEl.on(swiper.touchEvents.end, slideSelector, zoom.onGestureEnd, passiveListener);
                if (swiper.touchEvents.cancel) {
                    swiper.$wrapperEl.on(swiper.touchEvents.cancel, slideSelector, zoom.onGestureEnd, passiveListener)
                }
            }
            swiper.$wrapperEl.on(swiper.touchEvents.move, "." + swiper.params.zoom.containerClass, zoom.onTouchMove, activeListenerWithCapture)
        }, disable: function disable() {
            var swiper = this;
            var zoom = swiper.zoom;
            if (!zoom.enabled) {
                return
            }
            swiper.zoom.enabled = false;
            var passiveListener = swiper.touchEvents.start === "touchstart" && Support.passiveListener && swiper.params.passiveListeners ? {
                passive: true,
                capture: false
            } : false;
            var activeListenerWithCapture = Support.passiveListener ? {passive: false, capture: true} : true;
            var slideSelector = "." + swiper.params.slideClass;
            if (Support.gestures) {
                swiper.$wrapperEl.off("gesturestart", slideSelector, zoom.onGestureStart, passiveListener);
                swiper.$wrapperEl.off("gesturechange", slideSelector, zoom.onGestureChange, passiveListener);
                swiper.$wrapperEl.off("gestureend", slideSelector, zoom.onGestureEnd, passiveListener)
            } else if (swiper.touchEvents.start === "touchstart") {
                swiper.$wrapperEl.off(swiper.touchEvents.start, slideSelector, zoom.onGestureStart, passiveListener);
                swiper.$wrapperEl.off(swiper.touchEvents.move, slideSelector, zoom.onGestureChange, activeListenerWithCapture);
                swiper.$wrapperEl.off(swiper.touchEvents.end, slideSelector, zoom.onGestureEnd, passiveListener);
                if (swiper.touchEvents.cancel) {
                    swiper.$wrapperEl.off(swiper.touchEvents.cancel, slideSelector, zoom.onGestureEnd, passiveListener)
                }
            }
            swiper.$wrapperEl.off(swiper.touchEvents.move, "." + swiper.params.zoom.containerClass, zoom.onTouchMove, activeListenerWithCapture)
        }
    };
    var Zoom$1 = {
        name: "zoom",
        params: {
            zoom: {
                enabled: false,
                maxRatio: 3,
                minRatio: 1,
                toggle: true,
                containerClass: "swiper-zoom-container",
                zoomedSlideClass: "swiper-slide-zoomed"
            }
        },
        create: function create() {
            var swiper = this;
            var zoom = {
                enabled: false,
                scale: 1,
                currentScale: 1,
                isScaling: false,
                gesture: {
                    $slideEl: undefined,
                    slideWidth: undefined,
                    slideHeight: undefined,
                    $imageEl: undefined,
                    $imageWrapEl: undefined,
                    maxRatio: 3
                },
                image: {
                    isTouched: undefined,
                    isMoved: undefined,
                    currentX: undefined,
                    currentY: undefined,
                    minX: undefined,
                    minY: undefined,
                    maxX: undefined,
                    maxY: undefined,
                    width: undefined,
                    height: undefined,
                    startX: undefined,
                    startY: undefined,
                    touchesStart: {},
                    touchesCurrent: {}
                },
                velocity: {
                    x: undefined,
                    y: undefined,
                    prevPositionX: undefined,
                    prevPositionY: undefined,
                    prevTime: undefined
                }
            };
            "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function (methodName) {
                zoom[methodName] = Zoom[methodName].bind(swiper)
            });
            Utils.extend(swiper, {zoom: zoom});
            var scale = 1;
            Object.defineProperty(swiper.zoom, "scale", {
                get: function get() {
                    return scale
                }, set: function set(value) {
                    if (scale !== value) {
                        var imageEl = swiper.zoom.gesture.$imageEl ? swiper.zoom.gesture.$imageEl[0] : undefined;
                        var slideEl = swiper.zoom.gesture.$slideEl ? swiper.zoom.gesture.$slideEl[0] : undefined;
                        swiper.emit("zoomChange", value, imageEl, slideEl)
                    }
                    scale = value
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                if (swiper.params.zoom.enabled) {
                    swiper.zoom.enable()
                }
            }, destroy: function destroy() {
                var swiper = this;
                swiper.zoom.disable()
            }, touchStart: function touchStart(e) {
                var swiper = this;
                if (!swiper.zoom.enabled) {
                    return
                }
                swiper.zoom.onTouchStart(e)
            }, touchEnd: function touchEnd(e) {
                var swiper = this;
                if (!swiper.zoom.enabled) {
                    return
                }
                swiper.zoom.onTouchEnd(e)
            }, doubleTap: function doubleTap(e) {
                var swiper = this;
                if (swiper.params.zoom.enabled && swiper.zoom.enabled && swiper.params.zoom.toggle) {
                    swiper.zoom.toggle(e)
                }
            }, transitionEnd: function transitionEnd() {
                var swiper = this;
                if (swiper.zoom.enabled && swiper.params.zoom.enabled) {
                    swiper.zoom.onTransitionEnd()
                }
            }, slideChange: function slideChange() {
                var swiper = this;
                if (swiper.zoom.enabled && swiper.params.zoom.enabled && swiper.params.cssMode) {
                    swiper.zoom.onTransitionEnd()
                }
            }
        }
    };
    var Lazy = {
        loadInSlide: function loadInSlide(index, loadInDuplicate) {
            if (loadInDuplicate === void 0) loadInDuplicate = true;
            var swiper = this;
            var params = swiper.params.lazy;
            if (typeof index === "undefined") {
                return
            }
            if (swiper.slides.length === 0) {
                return
            }
            var isVirtual = swiper.virtual && swiper.params.virtual.enabled;
            var $slideEl = isVirtual ? swiper.$wrapperEl.children("." + swiper.params.slideClass + '[data-swiper-slide-index="' + index + '"]') : swiper.slides.eq(index);
            var $images = $slideEl.find("." + params.elementClass + ":not(." + params.loadedClass + "):not(." + params.loadingClass + ")");
            if ($slideEl.hasClass(params.elementClass) && !$slideEl.hasClass(params.loadedClass) && !$slideEl.hasClass(params.loadingClass)) {
                $images = $images.add($slideEl[0])
            }
            if ($images.length === 0) {
                return
            }
            $images.each(function (imageIndex, imageEl) {
                var $imageEl = $(imageEl);
                $imageEl.addClass(params.loadingClass);
                var background = $imageEl.attr("data-background");
                var src = $imageEl.attr("data-src");
                var srcset = $imageEl.attr("data-srcset");
                var sizes = $imageEl.attr("data-sizes");
                var $pictureEl = $imageEl.parent("picture");
                swiper.loadImage($imageEl[0], src || background, srcset, sizes, false, function () {
                    if (typeof swiper === "undefined" || swiper === null || !swiper || swiper && !swiper.params || swiper.destroyed) {
                        return
                    }
                    if (background) {
                        $imageEl.css("background-image", 'url("' + background + '")');
                        $imageEl.removeAttr("data-background")
                    } else {
                        if (srcset) {
                            $imageEl.attr("srcset", srcset);
                            $imageEl.removeAttr("data-srcset")
                        }
                        if (sizes) {
                            $imageEl.attr("sizes", sizes);
                            $imageEl.removeAttr("data-sizes")
                        }
                        if ($pictureEl.length) {
                            $pictureEl.children("source").each(function (sourceIndex, sourceEl) {
                                var $source = $(sourceEl);
                                if ($source.attr("data-srcset")) {
                                    $source.attr("srcset", $source.attr("data-srcset"));
                                    $source.removeAttr("data-srcset")
                                }
                            })
                        }
                        if (src) {
                            $imageEl.attr("src", src);
                            $imageEl.removeAttr("data-src")
                        }
                    }
                    $imageEl.addClass(params.loadedClass).removeClass(params.loadingClass);
                    $slideEl.find("." + params.preloaderClass).remove();
                    if (swiper.params.loop && loadInDuplicate) {
                        var slideOriginalIndex = $slideEl.attr("data-swiper-slide-index");
                        if ($slideEl.hasClass(swiper.params.slideDuplicateClass)) {
                            var originalSlide = swiper.$wrapperEl.children('[data-swiper-slide-index="' + slideOriginalIndex + '"]:not(.' + swiper.params.slideDuplicateClass + ")");
                            swiper.lazy.loadInSlide(originalSlide.index(), false)
                        } else {
                            var duplicatedSlide = swiper.$wrapperEl.children("." + swiper.params.slideDuplicateClass + '[data-swiper-slide-index="' + slideOriginalIndex + '"]');
                            swiper.lazy.loadInSlide(duplicatedSlide.index(), false)
                        }
                    }
                    swiper.emit("lazyImageReady", $slideEl[0], $imageEl[0]);
                    if (swiper.params.autoHeight) {
                        swiper.updateAutoHeight()
                    }
                });
                swiper.emit("lazyImageLoad", $slideEl[0], $imageEl[0])
            })
        }, load: function load() {
            var swiper = this;
            var $wrapperEl = swiper.$wrapperEl;
            var swiperParams = swiper.params;
            var slides = swiper.slides;
            var activeIndex = swiper.activeIndex;
            var isVirtual = swiper.virtual && swiperParams.virtual.enabled;
            var params = swiperParams.lazy;
            var slidesPerView = swiperParams.slidesPerView;
            if (slidesPerView === "auto") {
                slidesPerView = 0
            }

            function slideExist(index) {
                if (isVirtual) {
                    if ($wrapperEl.children("." + swiperParams.slideClass + '[data-swiper-slide-index="' + index + '"]').length) {
                        return true
                    }
                } else if (slides[index]) {
                    return true
                }
                return false
            }

            function slideIndex(slideEl) {
                if (isVirtual) {
                    return $(slideEl).attr("data-swiper-slide-index")
                }
                return $(slideEl).index()
            }

            if (!swiper.lazy.initialImageLoaded) {
                swiper.lazy.initialImageLoaded = true
            }
            if (swiper.params.watchSlidesVisibility) {
                $wrapperEl.children("." + swiperParams.slideVisibleClass).each(function (elIndex, slideEl) {
                    var index = isVirtual ? $(slideEl).attr("data-swiper-slide-index") : $(slideEl).index();
                    swiper.lazy.loadInSlide(index)
                })
            } else if (slidesPerView > 1) {
                for (var i = activeIndex; i < activeIndex + slidesPerView; i += 1) {
                    if (slideExist(i)) {
                        swiper.lazy.loadInSlide(i)
                    }
                }
            } else {
                swiper.lazy.loadInSlide(activeIndex)
            }
            if (params.loadPrevNext) {
                if (slidesPerView > 1 || params.loadPrevNextAmount && params.loadPrevNextAmount > 1) {
                    var amount = params.loadPrevNextAmount;
                    var spv = slidesPerView;
                    var maxIndex = Math.min(activeIndex + spv + Math.max(amount, spv), slides.length);
                    var minIndex = Math.max(activeIndex - Math.max(spv, amount), 0);
                    for (var i$1 = activeIndex + slidesPerView; i$1 < maxIndex; i$1 += 1) {
                        if (slideExist(i$1)) {
                            swiper.lazy.loadInSlide(i$1)
                        }
                    }
                    for (var i$2 = minIndex; i$2 < activeIndex; i$2 += 1) {
                        if (slideExist(i$2)) {
                            swiper.lazy.loadInSlide(i$2)
                        }
                    }
                } else {
                    var nextSlide = $wrapperEl.children("." + swiperParams.slideNextClass);
                    if (nextSlide.length > 0) {
                        swiper.lazy.loadInSlide(slideIndex(nextSlide))
                    }
                    var prevSlide = $wrapperEl.children("." + swiperParams.slidePrevClass);
                    if (prevSlide.length > 0) {
                        swiper.lazy.loadInSlide(slideIndex(prevSlide))
                    }
                }
            }
        }
    };
    var Lazy$1 = {
        name: "lazy",
        params: {
            lazy: {
                enabled: false,
                loadPrevNext: false,
                loadPrevNextAmount: 1,
                loadOnTransitionStart: false,
                elementClass: "swiper-lazy",
                loadingClass: "swiper-lazy-loading",
                loadedClass: "swiper-lazy-loaded",
                preloaderClass: "swiper-lazy-preloader"
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                lazy: {
                    initialImageLoaded: false,
                    load: Lazy.load.bind(swiper),
                    loadInSlide: Lazy.loadInSlide.bind(swiper)
                }
            })
        },
        on: {
            beforeInit: function beforeInit() {
                var swiper = this;
                if (swiper.params.lazy.enabled && swiper.params.preloadImages) {
                    swiper.params.preloadImages = false
                }
            }, init: function init() {
                var swiper = this;
                if (swiper.params.lazy.enabled && !swiper.params.loop && swiper.params.initialSlide === 0) {
                    swiper.lazy.load()
                }
            }, scroll: function scroll() {
                var swiper = this;
                if (swiper.params.freeMode && !swiper.params.freeModeSticky) {
                    swiper.lazy.load()
                }
            }, resize: function resize() {
                var swiper = this;
                if (swiper.params.lazy.enabled) {
                    swiper.lazy.load()
                }
            }, scrollbarDragMove: function scrollbarDragMove() {
                var swiper = this;
                if (swiper.params.lazy.enabled) {
                    swiper.lazy.load()
                }
            }, transitionStart: function transitionStart() {
                var swiper = this;
                if (swiper.params.lazy.enabled) {
                    if (swiper.params.lazy.loadOnTransitionStart || !swiper.params.lazy.loadOnTransitionStart && !swiper.lazy.initialImageLoaded) {
                        swiper.lazy.load()
                    }
                }
            }, transitionEnd: function transitionEnd() {
                var swiper = this;
                if (swiper.params.lazy.enabled && !swiper.params.lazy.loadOnTransitionStart) {
                    swiper.lazy.load()
                }
            }, slideChange: function slideChange() {
                var swiper = this;
                if (swiper.params.lazy.enabled && swiper.params.cssMode) {
                    swiper.lazy.load()
                }
            }
        }
    };
    var Controller = {
        LinearSpline: function LinearSpline(x, y) {
            var binarySearch = function search() {
                var maxIndex;
                var minIndex;
                var guess;
                return function (array, val) {
                    minIndex = -1;
                    maxIndex = array.length;
                    while (maxIndex - minIndex > 1) {
                        guess = maxIndex + minIndex >> 1;
                        if (array[guess] <= val) {
                            minIndex = guess
                        } else {
                            maxIndex = guess
                        }
                    }
                    return maxIndex
                }
            }();
            this.x = x;
            this.y = y;
            this.lastIndex = x.length - 1;
            var i1;
            var i3;
            this.interpolate = function interpolate(x2) {
                if (!x2) {
                    return 0
                }
                i3 = binarySearch(this.x, x2);
                i1 = i3 - 1;
                return (x2 - this.x[i1]) * (this.y[i3] - this.y[i1]) / (this.x[i3] - this.x[i1]) + this.y[i1]
            };
            return this
        }, getInterpolateFunction: function getInterpolateFunction(c) {
            var swiper = this;
            if (!swiper.controller.spline) {
                swiper.controller.spline = swiper.params.loop ? new Controller.LinearSpline(swiper.slidesGrid, c.slidesGrid) : new Controller.LinearSpline(swiper.snapGrid, c.snapGrid)
            }
        }, setTranslate: function setTranslate(setTranslate$1, byController) {
            var swiper = this;
            var controlled = swiper.controller.control;
            var multiplier;
            var controlledTranslate;

            function setControlledTranslate(c) {
                var translate = swiper.rtlTranslate ? -swiper.translate : swiper.translate;
                if (swiper.params.controller.by === "slide") {
                    swiper.controller.getInterpolateFunction(c);
                    controlledTranslate = -swiper.controller.spline.interpolate(-translate)
                }
                if (!controlledTranslate || swiper.params.controller.by === "container") {
                    multiplier = (c.maxTranslate() - c.minTranslate()) / (swiper.maxTranslate() - swiper.minTranslate());
                    controlledTranslate = (translate - swiper.minTranslate()) * multiplier + c.minTranslate()
                }
                if (swiper.params.controller.inverse) {
                    controlledTranslate = c.maxTranslate() - controlledTranslate
                }
                c.updateProgress(controlledTranslate);
                c.setTranslate(controlledTranslate, swiper);
                c.updateActiveIndex();
                c.updateSlidesClasses()
            }

            if (Array.isArray(controlled)) {
                for (var i = 0; i < controlled.length; i += 1) {
                    if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                        setControlledTranslate(controlled[i])
                    }
                }
            } else if (controlled instanceof Swiper && byController !== controlled) {
                setControlledTranslate(controlled)
            }
        }, setTransition: function setTransition(duration, byController) {
            var swiper = this;
            var controlled = swiper.controller.control;
            var i;

            function setControlledTransition(c) {
                c.setTransition(duration, swiper);
                if (duration !== 0) {
                    c.transitionStart();
                    if (c.params.autoHeight) {
                        Utils.nextTick(function () {
                            c.updateAutoHeight()
                        })
                    }
                    c.$wrapperEl.transitionEnd(function () {
                        if (!controlled) {
                            return
                        }
                        if (c.params.loop && swiper.params.controller.by === "slide") {
                            c.loopFix()
                        }
                        c.transitionEnd()
                    })
                }
            }

            if (Array.isArray(controlled)) {
                for (i = 0; i < controlled.length; i += 1) {
                    if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                        setControlledTransition(controlled[i])
                    }
                }
            } else if (controlled instanceof Swiper && byController !== controlled) {
                setControlledTransition(controlled)
            }
        }
    };
    var Controller$1 = {
        name: "controller",
        params: {controller: {control: undefined, inverse: false, by: "slide"}},
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                controller: {
                    control: swiper.params.controller.control,
                    getInterpolateFunction: Controller.getInterpolateFunction.bind(swiper),
                    setTranslate: Controller.setTranslate.bind(swiper),
                    setTransition: Controller.setTransition.bind(swiper)
                }
            })
        },
        on: {
            update: function update() {
                var swiper = this;
                if (!swiper.controller.control) {
                    return
                }
                if (swiper.controller.spline) {
                    swiper.controller.spline = undefined;
                    delete swiper.controller.spline
                }
            }, resize: function resize() {
                var swiper = this;
                if (!swiper.controller.control) {
                    return
                }
                if (swiper.controller.spline) {
                    swiper.controller.spline = undefined;
                    delete swiper.controller.spline
                }
            }, observerUpdate: function observerUpdate() {
                var swiper = this;
                if (!swiper.controller.control) {
                    return
                }
                if (swiper.controller.spline) {
                    swiper.controller.spline = undefined;
                    delete swiper.controller.spline
                }
            }, setTranslate: function setTranslate(translate, byController) {
                var swiper = this;
                if (!swiper.controller.control) {
                    return
                }
                swiper.controller.setTranslate(translate, byController)
            }, setTransition: function setTransition(duration, byController) {
                var swiper = this;
                if (!swiper.controller.control) {
                    return
                }
                swiper.controller.setTransition(duration, byController)
            }
        }
    };
    var a11y = {
        makeElFocusable: function makeElFocusable($el) {
            $el.attr("tabIndex", "0");
            return $el
        }, makeElNotFocusable: function makeElNotFocusable($el) {
            $el.attr("tabIndex", "-1");
            return $el
        }, addElRole: function addElRole($el, role) {
            $el.attr("role", role);
            return $el
        }, addElLabel: function addElLabel($el, label) {
            $el.attr("aria-label", label);
            return $el
        }, disableEl: function disableEl($el) {
            $el.attr("aria-disabled", true);
            return $el
        }, enableEl: function enableEl($el) {
            $el.attr("aria-disabled", false);
            return $el
        }, onEnterKey: function onEnterKey(e) {
            var swiper = this;
            var params = swiper.params.a11y;
            if (e.keyCode !== 13) {
                return
            }
            var $targetEl = $(e.target);
            if (swiper.navigation && swiper.navigation.$nextEl && $targetEl.is(swiper.navigation.$nextEl)) {
                if (!(swiper.isEnd && !swiper.params.loop)) {
                    swiper.slideNext()
                }
                if (swiper.isEnd) {
                    swiper.a11y.notify(params.lastSlideMessage)
                } else {
                    swiper.a11y.notify(params.nextSlideMessage)
                }
            }
            if (swiper.navigation && swiper.navigation.$prevEl && $targetEl.is(swiper.navigation.$prevEl)) {
                if (!(swiper.isBeginning && !swiper.params.loop)) {
                    swiper.slidePrev()
                }
                if (swiper.isBeginning) {
                    swiper.a11y.notify(params.firstSlideMessage)
                } else {
                    swiper.a11y.notify(params.prevSlideMessage)
                }
            }
            if (swiper.pagination && $targetEl.is("." + swiper.params.pagination.bulletClass)) {
                $targetEl[0].click()
            }
        }, notify: function notify(message) {
            var swiper = this;
            var notification = swiper.a11y.liveRegion;
            if (notification.length === 0) {
                return
            }
            notification.html("");
            notification.html(message)
        }, updateNavigation: function updateNavigation() {
            var swiper = this;
            if (swiper.params.loop || !swiper.navigation) {
                return
            }
            var ref = swiper.navigation;
            var $nextEl = ref.$nextEl;
            var $prevEl = ref.$prevEl;
            if ($prevEl && $prevEl.length > 0) {
                if (swiper.isBeginning) {
                    swiper.a11y.disableEl($prevEl);
                    swiper.a11y.makeElNotFocusable($prevEl)
                } else {
                    swiper.a11y.enableEl($prevEl);
                    swiper.a11y.makeElFocusable($prevEl)
                }
            }
            if ($nextEl && $nextEl.length > 0) {
                if (swiper.isEnd) {
                    swiper.a11y.disableEl($nextEl);
                    swiper.a11y.makeElNotFocusable($nextEl)
                } else {
                    swiper.a11y.enableEl($nextEl);
                    swiper.a11y.makeElFocusable($nextEl)
                }
            }
        }, updatePagination: function updatePagination() {
            var swiper = this;
            var params = swiper.params.a11y;
            if (swiper.pagination && swiper.params.pagination.clickable && swiper.pagination.bullets && swiper.pagination.bullets.length) {
                swiper.pagination.bullets.each(function (bulletIndex, bulletEl) {
                    var $bulletEl = $(bulletEl);
                    swiper.a11y.makeElFocusable($bulletEl);
                    swiper.a11y.addElRole($bulletEl, "button");
                    swiper.a11y.addElLabel($bulletEl, params.paginationBulletMessage.replace(/\{\{index\}\}/, $bulletEl.index() + 1))
                })
            }
        }, init: function init() {
            var swiper = this;
            swiper.$el.append(swiper.a11y.liveRegion);
            var params = swiper.params.a11y;
            var $nextEl;
            var $prevEl;
            if (swiper.navigation && swiper.navigation.$nextEl) {
                $nextEl = swiper.navigation.$nextEl
            }
            if (swiper.navigation && swiper.navigation.$prevEl) {
                $prevEl = swiper.navigation.$prevEl
            }
            if ($nextEl) {
                swiper.a11y.makeElFocusable($nextEl);
                swiper.a11y.addElRole($nextEl, "button");
                swiper.a11y.addElLabel($nextEl, params.nextSlideMessage);
                $nextEl.on("keydown", swiper.a11y.onEnterKey)
            }
            if ($prevEl) {
                swiper.a11y.makeElFocusable($prevEl);
                swiper.a11y.addElRole($prevEl, "button");
                swiper.a11y.addElLabel($prevEl, params.prevSlideMessage);
                $prevEl.on("keydown", swiper.a11y.onEnterKey)
            }
            if (swiper.pagination && swiper.params.pagination.clickable && swiper.pagination.bullets && swiper.pagination.bullets.length) {
                swiper.pagination.$el.on("keydown", "." + swiper.params.pagination.bulletClass, swiper.a11y.onEnterKey)
            }
        }, destroy: function destroy() {
            var swiper = this;
            if (swiper.a11y.liveRegion && swiper.a11y.liveRegion.length > 0) {
                swiper.a11y.liveRegion.remove()
            }
            var $nextEl;
            var $prevEl;
            if (swiper.navigation && swiper.navigation.$nextEl) {
                $nextEl = swiper.navigation.$nextEl
            }
            if (swiper.navigation && swiper.navigation.$prevEl) {
                $prevEl = swiper.navigation.$prevEl
            }
            if ($nextEl) {
                $nextEl.off("keydown", swiper.a11y.onEnterKey)
            }
            if ($prevEl) {
                $prevEl.off("keydown", swiper.a11y.onEnterKey)
            }
            if (swiper.pagination && swiper.params.pagination.clickable && swiper.pagination.bullets && swiper.pagination.bullets.length) {
                swiper.pagination.$el.off("keydown", "." + swiper.params.pagination.bulletClass, swiper.a11y.onEnterKey)
            }
        }
    };
    var A11y = {
        name: "a11y",
        params: {
            a11y: {
                enabled: true,
                notificationClass: "swiper-notification",
                prevSlideMessage: "Previous slide",
                nextSlideMessage: "Next slide",
                firstSlideMessage: "This is the first slide",
                lastSlideMessage: "This is the last slide",
                paginationBulletMessage: "Go to slide {{index}}"
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {a11y: {liveRegion: $('<span class="' + swiper.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>')}});
            Object.keys(a11y).forEach(function (methodName) {
                swiper.a11y[methodName] = a11y[methodName].bind(swiper)
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                if (!swiper.params.a11y.enabled) {
                    return
                }
                swiper.a11y.init();
                swiper.a11y.updateNavigation()
            }, toEdge: function toEdge() {
                var swiper = this;
                if (!swiper.params.a11y.enabled) {
                    return
                }
                swiper.a11y.updateNavigation()
            }, fromEdge: function fromEdge() {
                var swiper = this;
                if (!swiper.params.a11y.enabled) {
                    return
                }
                swiper.a11y.updateNavigation()
            }, paginationUpdate: function paginationUpdate() {
                var swiper = this;
                if (!swiper.params.a11y.enabled) {
                    return
                }
                swiper.a11y.updatePagination()
            }, destroy: function destroy() {
                var swiper = this;
                if (!swiper.params.a11y.enabled) {
                    return
                }
                swiper.a11y.destroy()
            }
        }
    };
    var History = {
        init: function init() {
            var swiper = this;
            if (!swiper.params.history) {
                return
            }
            if (!win.history || !win.history.pushState) {
                swiper.params.history.enabled = false;
                swiper.params.hashNavigation.enabled = true;
                return
            }
            var history = swiper.history;
            history.initialized = true;
            history.paths = History.getPathValues();
            if (!history.paths.key && !history.paths.value) {
                return
            }
            history.scrollToSlide(0, history.paths.value, swiper.params.runCallbacksOnInit);
            if (!swiper.params.history.replaceState) {
                win.addEventListener("popstate", swiper.history.setHistoryPopState)
            }
        }, destroy: function destroy() {
            var swiper = this;
            if (!swiper.params.history.replaceState) {
                win.removeEventListener("popstate", swiper.history.setHistoryPopState)
            }
        }, setHistoryPopState: function setHistoryPopState() {
            var swiper = this;
            swiper.history.paths = History.getPathValues();
            swiper.history.scrollToSlide(swiper.params.speed, swiper.history.paths.value, false)
        }, getPathValues: function getPathValues() {
            var pathArray = win.location.pathname.slice(1).split("/").filter(function (part) {
                return part !== ""
            });
            var total = pathArray.length;
            var key = pathArray[total - 2];
            var value = pathArray[total - 1];
            return {key: key, value: value}
        }, setHistory: function setHistory(key, index) {
            var swiper = this;
            if (!swiper.history.initialized || !swiper.params.history.enabled) {
                return
            }
            var slide = swiper.slides.eq(index);
            var value = History.slugify(slide.attr("data-history"));
            if (!win.location.pathname.includes(key)) {
                value = key + "/" + value
            }
            var currentState = win.history.state;
            if (currentState && currentState.value === value) {
                return
            }
            if (swiper.params.history.replaceState) {
                win.history.replaceState({value: value}, null, value)
            } else {
                win.history.pushState({value: value}, null, value)
            }
        }, slugify: function slugify(text) {
            return text.toString().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
        }, scrollToSlide: function scrollToSlide(speed, value, runCallbacks) {
            var swiper = this;
            if (value) {
                for (var i = 0, length = swiper.slides.length; i < length; i += 1) {
                    var slide = swiper.slides.eq(i);
                    var slideHistory = History.slugify(slide.attr("data-history"));
                    if (slideHistory === value && !slide.hasClass(swiper.params.slideDuplicateClass)) {
                        var index = slide.index();
                        swiper.slideTo(index, speed, runCallbacks)
                    }
                }
            } else {
                swiper.slideTo(0, speed, runCallbacks)
            }
        }
    };
    var History$1 = {
        name: "history",
        params: {history: {enabled: false, replaceState: false, key: "slides"}},
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                history: {
                    init: History.init.bind(swiper),
                    setHistory: History.setHistory.bind(swiper),
                    setHistoryPopState: History.setHistoryPopState.bind(swiper),
                    scrollToSlide: History.scrollToSlide.bind(swiper),
                    destroy: History.destroy.bind(swiper)
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                if (swiper.params.history.enabled) {
                    swiper.history.init()
                }
            }, destroy: function destroy() {
                var swiper = this;
                if (swiper.params.history.enabled) {
                    swiper.history.destroy()
                }
            }, transitionEnd: function transitionEnd() {
                var swiper = this;
                if (swiper.history.initialized) {
                    swiper.history.setHistory(swiper.params.history.key, swiper.activeIndex)
                }
            }, slideChange: function slideChange() {
                var swiper = this;
                if (swiper.history.initialized && swiper.params.cssMode) {
                    swiper.history.setHistory(swiper.params.history.key, swiper.activeIndex)
                }
            }
        }
    };
    var HashNavigation = {
        onHashCange: function onHashCange() {
            var swiper = this;
            swiper.emit("hashChange");
            var newHash = doc.location.hash.replace("#", "");
            var activeSlideHash = swiper.slides.eq(swiper.activeIndex).attr("data-hash");
            if (newHash !== activeSlideHash) {
                var newIndex = swiper.$wrapperEl.children("." + swiper.params.slideClass + '[data-hash="' + newHash + '"]').index();
                if (typeof newIndex === "undefined") {
                    return
                }
                swiper.slideTo(newIndex)
            }
        }, setHash: function setHash() {
            var swiper = this;
            if (!swiper.hashNavigation.initialized || !swiper.params.hashNavigation.enabled) {
                return
            }
            if (swiper.params.hashNavigation.replaceState && win.history && win.history.replaceState) {
                win.history.replaceState(null, null, "#" + swiper.slides.eq(swiper.activeIndex).attr("data-hash") || "");
                swiper.emit("hashSet")
            } else {
                var slide = swiper.slides.eq(swiper.activeIndex);
                var hash = slide.attr("data-hash") || slide.attr("data-history");
                doc.location.hash = hash || "";
                swiper.emit("hashSet")
            }
        }, init: function init() {
            var swiper = this;
            if (!swiper.params.hashNavigation.enabled || swiper.params.history && swiper.params.history.enabled) {
                return
            }
            swiper.hashNavigation.initialized = true;
            var hash = doc.location.hash.replace("#", "");
            if (hash) {
                var speed = 0;
                for (var i = 0, length = swiper.slides.length; i < length; i += 1) {
                    var slide = swiper.slides.eq(i);
                    var slideHash = slide.attr("data-hash") || slide.attr("data-history");
                    if (slideHash === hash && !slide.hasClass(swiper.params.slideDuplicateClass)) {
                        var index = slide.index();
                        swiper.slideTo(index, speed, swiper.params.runCallbacksOnInit, true)
                    }
                }
            }
            if (swiper.params.hashNavigation.watchState) {
                $(win).on("hashchange", swiper.hashNavigation.onHashCange)
            }
        }, destroy: function destroy() {
            var swiper = this;
            if (swiper.params.hashNavigation.watchState) {
                $(win).off("hashchange", swiper.hashNavigation.onHashCange)
            }
        }
    };
    var HashNavigation$1 = {
        name: "hash-navigation",
        params: {hashNavigation: {enabled: false, replaceState: false, watchState: false}},
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                hashNavigation: {
                    initialized: false,
                    init: HashNavigation.init.bind(swiper),
                    destroy: HashNavigation.destroy.bind(swiper),
                    setHash: HashNavigation.setHash.bind(swiper),
                    onHashCange: HashNavigation.onHashCange.bind(swiper)
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                if (swiper.params.hashNavigation.enabled) {
                    swiper.hashNavigation.init()
                }
            }, destroy: function destroy() {
                var swiper = this;
                if (swiper.params.hashNavigation.enabled) {
                    swiper.hashNavigation.destroy()
                }
            }, transitionEnd: function transitionEnd() {
                var swiper = this;
                if (swiper.hashNavigation.initialized) {
                    swiper.hashNavigation.setHash()
                }
            }, slideChange: function slideChange() {
                var swiper = this;
                if (swiper.hashNavigation.initialized && swiper.params.cssMode) {
                    swiper.hashNavigation.setHash()
                }
            }
        }
    };
    var Autoplay = {
        run: function run() {
            var swiper = this;
            var $activeSlideEl = swiper.slides.eq(swiper.activeIndex);
            var delay = swiper.params.autoplay.delay;
            if ($activeSlideEl.attr("data-swiper-autoplay")) {
                delay = $activeSlideEl.attr("data-swiper-autoplay") || swiper.params.autoplay.delay
            }
            clearTimeout(swiper.autoplay.timeout);
            swiper.autoplay.timeout = Utils.nextTick(function () {
                if (swiper.params.autoplay.reverseDirection) {
                    if (swiper.params.loop) {
                        swiper.loopFix();
                        swiper.slidePrev(swiper.params.speed, true, true);
                        swiper.emit("autoplay")
                    } else if (!swiper.isBeginning) {
                        swiper.slidePrev(swiper.params.speed, true, true);
                        swiper.emit("autoplay")
                    } else if (!swiper.params.autoplay.stopOnLastSlide) {
                        swiper.slideTo(swiper.slides.length - 1, swiper.params.speed, true, true);
                        swiper.emit("autoplay")
                    } else {
                        swiper.autoplay.stop()
                    }
                } else if (swiper.params.loop) {
                    swiper.loopFix();
                    swiper.slideNext(swiper.params.speed, true, true);
                    swiper.emit("autoplay")
                } else if (!swiper.isEnd) {
                    swiper.slideNext(swiper.params.speed, true, true);
                    swiper.emit("autoplay")
                } else if (!swiper.params.autoplay.stopOnLastSlide) {
                    swiper.slideTo(0, swiper.params.speed, true, true);
                    swiper.emit("autoplay")
                } else {
                    swiper.autoplay.stop()
                }
                if (swiper.params.cssMode && swiper.autoplay.running) {
                    swiper.autoplay.run()
                }
            }, delay)
        }, start: function start() {
            var swiper = this;
            if (typeof swiper.autoplay.timeout !== "undefined") {
                return false
            }
            if (swiper.autoplay.running) {
                return false
            }
            swiper.autoplay.running = true;
            swiper.emit("autoplayStart");
            swiper.autoplay.run();
            return true
        }, stop: function stop() {
            var swiper = this;
            if (!swiper.autoplay.running) {
                return false
            }
            if (typeof swiper.autoplay.timeout === "undefined") {
                return false
            }
            if (swiper.autoplay.timeout) {
                clearTimeout(swiper.autoplay.timeout);
                swiper.autoplay.timeout = undefined
            }
            swiper.autoplay.running = false;
            swiper.emit("autoplayStop");
            return true
        }, pause: function pause(speed) {
            var swiper = this;
            if (!swiper.autoplay.running) {
                return
            }
            if (swiper.autoplay.paused) {
                return
            }
            if (swiper.autoplay.timeout) {
                clearTimeout(swiper.autoplay.timeout)
            }
            swiper.autoplay.paused = true;
            if (speed === 0 || !swiper.params.autoplay.waitForTransition) {
                swiper.autoplay.paused = false;
                swiper.autoplay.run()
            } else {
                swiper.$wrapperEl[0].addEventListener("transitionend", swiper.autoplay.onTransitionEnd);
                swiper.$wrapperEl[0].addEventListener("webkitTransitionEnd", swiper.autoplay.onTransitionEnd)
            }
        }
    };
    var Autoplay$1 = {
        name: "autoplay",
        params: {
            autoplay: {
                enabled: false,
                delay: 3e3,
                waitForTransition: true,
                disableOnInteraction: true,
                stopOnLastSlide: false,
                reverseDirection: false
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                autoplay: {
                    running: false,
                    paused: false,
                    run: Autoplay.run.bind(swiper),
                    start: Autoplay.start.bind(swiper),
                    stop: Autoplay.stop.bind(swiper),
                    pause: Autoplay.pause.bind(swiper),
                    onVisibilityChange: function onVisibilityChange() {
                        if (document.visibilityState === "hidden" && swiper.autoplay.running) {
                            swiper.autoplay.pause()
                        }
                        if (document.visibilityState === "visible" && swiper.autoplay.paused) {
                            swiper.autoplay.run();
                            swiper.autoplay.paused = false
                        }
                    },
                    onTransitionEnd: function onTransitionEnd(e) {
                        if (!swiper || swiper.destroyed || !swiper.$wrapperEl) {
                            return
                        }
                        if (e.target !== this) {
                            return
                        }
                        swiper.$wrapperEl[0].removeEventListener("transitionend", swiper.autoplay.onTransitionEnd);
                        swiper.$wrapperEl[0].removeEventListener("webkitTransitionEnd", swiper.autoplay.onTransitionEnd);
                        swiper.autoplay.paused = false;
                        if (!swiper.autoplay.running) {
                            swiper.autoplay.stop()
                        } else {
                            swiper.autoplay.run()
                        }
                    }
                }
            })
        },
        on: {
            init: function init() {
                var swiper = this;
                if (swiper.params.autoplay.enabled) {
                    swiper.autoplay.start();
                    document.addEventListener("visibilitychange", swiper.autoplay.onVisibilityChange)
                }
            }, beforeTransitionStart: function beforeTransitionStart(speed, internal) {
                var swiper = this;
                if (swiper.autoplay.running) {
                    if (internal || !swiper.params.autoplay.disableOnInteraction) {
                        swiper.autoplay.pause(speed)
                    } else {
                        swiper.autoplay.stop()
                    }
                }
            }, sliderFirstMove: function sliderFirstMove() {
                var swiper = this;
                if (swiper.autoplay.running) {
                    if (swiper.params.autoplay.disableOnInteraction) {
                        swiper.autoplay.stop()
                    } else {
                        swiper.autoplay.pause()
                    }
                }
            }, touchEnd: function touchEnd() {
                var swiper = this;
                if (swiper.params.cssMode && swiper.autoplay.paused && !swiper.params.autoplay.disableOnInteraction) {
                    swiper.autoplay.run()
                }
            }, destroy: function destroy() {
                var swiper = this;
                if (swiper.autoplay.running) {
                    swiper.autoplay.stop()
                }
                document.removeEventListener("visibilitychange", swiper.autoplay.onVisibilityChange)
            }
        }
    };
    var Fade = {
        setTranslate: function setTranslate() {
            var swiper = this;
            var slides = swiper.slides;
            for (var i = 0; i < slides.length; i += 1) {
                var $slideEl = swiper.slides.eq(i);
                var offset = $slideEl[0].swiperSlideOffset;
                var tx = -offset;
                if (!swiper.params.virtualTranslate) {
                    tx -= swiper.translate
                }
                var ty = 0;
                if (!swiper.isHorizontal()) {
                    ty = tx;
                    tx = 0
                }
                var slideOpacity = swiper.params.fadeEffect.crossFade ? Math.max(1 - Math.abs($slideEl[0].progress), 0) : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
                $slideEl.css({opacity: slideOpacity}).transform("translate3d(" + tx + "px, " + ty + "px, 0px)")
            }
        }, setTransition: function setTransition(duration) {
            var swiper = this;
            var slides = swiper.slides;
            var $wrapperEl = swiper.$wrapperEl;
            slides.transition(duration);
            if (swiper.params.virtualTranslate && duration !== 0) {
                var eventTriggered = false;
                slides.transitionEnd(function () {
                    if (eventTriggered) {
                        return
                    }
                    if (!swiper || swiper.destroyed) {
                        return
                    }
                    eventTriggered = true;
                    swiper.animating = false;
                    var triggerEvents = ["webkitTransitionEnd", "transitionend"];
                    for (var i = 0; i < triggerEvents.length; i += 1) {
                        $wrapperEl.trigger(triggerEvents[i])
                    }
                })
            }
        }
    };
    var EffectFade = {
        name: "effect-fade", params: {fadeEffect: {crossFade: false}}, create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                fadeEffect: {
                    setTranslate: Fade.setTranslate.bind(swiper),
                    setTransition: Fade.setTransition.bind(swiper)
                }
            })
        }, on: {
            beforeInit: function beforeInit() {
                var swiper = this;
                if (swiper.params.effect !== "fade") {
                    return
                }
                swiper.classNames.push(swiper.params.containerModifierClass + "fade");
                var overwriteParams = {
                    slidesPerView: 1,
                    slidesPerColumn: 1,
                    slidesPerGroup: 1,
                    watchSlidesProgress: true,
                    spaceBetween: 0,
                    virtualTranslate: true
                };
                Utils.extend(swiper.params, overwriteParams);
                Utils.extend(swiper.originalParams, overwriteParams)
            }, setTranslate: function setTranslate() {
                var swiper = this;
                if (swiper.params.effect !== "fade") {
                    return
                }
                swiper.fadeEffect.setTranslate()
            }, setTransition: function setTransition(duration) {
                var swiper = this;
                if (swiper.params.effect !== "fade") {
                    return
                }
                swiper.fadeEffect.setTransition(duration)
            }
        }
    };
    var Cube = {
        setTranslate: function setTranslate() {
            var swiper = this;
            var $el = swiper.$el;
            var $wrapperEl = swiper.$wrapperEl;
            var slides = swiper.slides;
            var swiperWidth = swiper.width;
            var swiperHeight = swiper.height;
            var rtl = swiper.rtlTranslate;
            var swiperSize = swiper.size;
            var params = swiper.params.cubeEffect;
            var isHorizontal = swiper.isHorizontal();
            var isVirtual = swiper.virtual && swiper.params.virtual.enabled;
            var wrapperRotate = 0;
            var $cubeShadowEl;
            if (params.shadow) {
                if (isHorizontal) {
                    $cubeShadowEl = $wrapperEl.find(".swiper-cube-shadow");
                    if ($cubeShadowEl.length === 0) {
                        $cubeShadowEl = $('<div class="swiper-cube-shadow"></div>');
                        $wrapperEl.append($cubeShadowEl)
                    }
                    $cubeShadowEl.css({height: swiperWidth + "px"})
                } else {
                    $cubeShadowEl = $el.find(".swiper-cube-shadow");
                    if ($cubeShadowEl.length === 0) {
                        $cubeShadowEl = $('<div class="swiper-cube-shadow"></div>');
                        $el.append($cubeShadowEl)
                    }
                }
            }
            for (var i = 0; i < slides.length; i += 1) {
                var $slideEl = slides.eq(i);
                var slideIndex = i;
                if (isVirtual) {
                    slideIndex = parseInt($slideEl.attr("data-swiper-slide-index"), 10)
                }
                var slideAngle = slideIndex * 90;
                var round = Math.floor(slideAngle / 360);
                if (rtl) {
                    slideAngle = -slideAngle;
                    round = Math.floor(-slideAngle / 360)
                }
                var progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
                var tx = 0;
                var ty = 0;
                var tz = 0;
                if (slideIndex % 4 === 0) {
                    tx = -round * 4 * swiperSize;
                    tz = 0
                } else if ((slideIndex - 1) % 4 === 0) {
                    tx = 0;
                    tz = -round * 4 * swiperSize
                } else if ((slideIndex - 2) % 4 === 0) {
                    tx = swiperSize + round * 4 * swiperSize;
                    tz = swiperSize
                } else if ((slideIndex - 3) % 4 === 0) {
                    tx = -swiperSize;
                    tz = 3 * swiperSize + swiperSize * 4 * round
                }
                if (rtl) {
                    tx = -tx
                }
                if (!isHorizontal) {
                    ty = tx;
                    tx = 0
                }
                var transform = "rotateX(" + (isHorizontal ? 0 : -slideAngle) + "deg) rotateY(" + (isHorizontal ? slideAngle : 0) + "deg) translate3d(" + tx + "px, " + ty + "px, " + tz + "px)";
                if (progress <= 1 && progress > -1) {
                    wrapperRotate = slideIndex * 90 + progress * 90;
                    if (rtl) {
                        wrapperRotate = -slideIndex * 90 - progress * 90
                    }
                }
                $slideEl.transform(transform);
                if (params.slideShadows) {
                    var shadowBefore = isHorizontal ? $slideEl.find(".swiper-slide-shadow-left") : $slideEl.find(".swiper-slide-shadow-top");
                    var shadowAfter = isHorizontal ? $slideEl.find(".swiper-slide-shadow-right") : $slideEl.find(".swiper-slide-shadow-bottom");
                    if (shadowBefore.length === 0) {
                        shadowBefore = $('<div class="swiper-slide-shadow-' + (isHorizontal ? "left" : "top") + '"></div>');
                        $slideEl.append(shadowBefore)
                    }
                    if (shadowAfter.length === 0) {
                        shadowAfter = $('<div class="swiper-slide-shadow-' + (isHorizontal ? "right" : "bottom") + '"></div>');
                        $slideEl.append(shadowAfter)
                    }
                    if (shadowBefore.length) {
                        shadowBefore[0].style.opacity = Math.max(-progress, 0)
                    }
                    if (shadowAfter.length) {
                        shadowAfter[0].style.opacity = Math.max(progress, 0)
                    }
                }
            }
            $wrapperEl.css({
                "-webkit-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
                "-moz-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
                "-ms-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
                "transform-origin": "50% 50% -" + swiperSize / 2 + "px"
            });
            if (params.shadow) {
                if (isHorizontal) {
                    $cubeShadowEl.transform("translate3d(0px, " + (swiperWidth / 2 + params.shadowOffset) + "px, " + -swiperWidth / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + params.shadowScale + ")")
                } else {
                    var shadowAngle = Math.abs(wrapperRotate) - Math.floor(Math.abs(wrapperRotate) / 90) * 90;
                    var multiplier = 1.5 - (Math.sin(shadowAngle * 2 * Math.PI / 360) / 2 + Math.cos(shadowAngle * 2 * Math.PI / 360) / 2);
                    var scale1 = params.shadowScale;
                    var scale2 = params.shadowScale / multiplier;
                    var offset = params.shadowOffset;
                    $cubeShadowEl.transform("scale3d(" + scale1 + ", 1, " + scale2 + ") translate3d(0px, " + (swiperHeight / 2 + offset) + "px, " + -swiperHeight / 2 / scale2 + "px) rotateX(-90deg)")
                }
            }
            var zFactor = Browser.isSafari || Browser.isWebView ? -swiperSize / 2 : 0;
            $wrapperEl.transform("translate3d(0px,0," + zFactor + "px) rotateX(" + (swiper.isHorizontal() ? 0 : wrapperRotate) + "deg) rotateY(" + (swiper.isHorizontal() ? -wrapperRotate : 0) + "deg)")
        }, setTransition: function setTransition(duration) {
            var swiper = this;
            var $el = swiper.$el;
            var slides = swiper.slides;
            slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration);
            if (swiper.params.cubeEffect.shadow && !swiper.isHorizontal()) {
                $el.find(".swiper-cube-shadow").transition(duration)
            }
        }
    };
    var EffectCube = {
        name: "effect-cube",
        params: {cubeEffect: {slideShadows: true, shadow: true, shadowOffset: 20, shadowScale: .94}},
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                cubeEffect: {
                    setTranslate: Cube.setTranslate.bind(swiper),
                    setTransition: Cube.setTransition.bind(swiper)
                }
            })
        },
        on: {
            beforeInit: function beforeInit() {
                var swiper = this;
                if (swiper.params.effect !== "cube") {
                    return
                }
                swiper.classNames.push(swiper.params.containerModifierClass + "cube");
                swiper.classNames.push(swiper.params.containerModifierClass + "3d");
                var overwriteParams = {
                    slidesPerView: 1,
                    slidesPerColumn: 1,
                    slidesPerGroup: 1,
                    watchSlidesProgress: true,
                    resistanceRatio: 0,
                    spaceBetween: 0,
                    centeredSlides: false,
                    virtualTranslate: true
                };
                Utils.extend(swiper.params, overwriteParams);
                Utils.extend(swiper.originalParams, overwriteParams)
            }, setTranslate: function setTranslate() {
                var swiper = this;
                if (swiper.params.effect !== "cube") {
                    return
                }
                swiper.cubeEffect.setTranslate()
            }, setTransition: function setTransition(duration) {
                var swiper = this;
                if (swiper.params.effect !== "cube") {
                    return
                }
                swiper.cubeEffect.setTransition(duration)
            }
        }
    };
    var Flip = {
        setTranslate: function setTranslate() {
            var swiper = this;
            var slides = swiper.slides;
            var rtl = swiper.rtlTranslate;
            for (var i = 0; i < slides.length; i += 1) {
                var $slideEl = slides.eq(i);
                var progress = $slideEl[0].progress;
                if (swiper.params.flipEffect.limitRotation) {
                    progress = Math.max(Math.min($slideEl[0].progress, 1), -1)
                }
                var offset = $slideEl[0].swiperSlideOffset;
                var rotate = -180 * progress;
                var rotateY = rotate;
                var rotateX = 0;
                var tx = -offset;
                var ty = 0;
                if (!swiper.isHorizontal()) {
                    ty = tx;
                    tx = 0;
                    rotateX = -rotateY;
                    rotateY = 0
                } else if (rtl) {
                    rotateY = -rotateY
                }
                $slideEl[0].style.zIndex = -Math.abs(Math.round(progress)) + slides.length;
                if (swiper.params.flipEffect.slideShadows) {
                    var shadowBefore = swiper.isHorizontal() ? $slideEl.find(".swiper-slide-shadow-left") : $slideEl.find(".swiper-slide-shadow-top");
                    var shadowAfter = swiper.isHorizontal() ? $slideEl.find(".swiper-slide-shadow-right") : $slideEl.find(".swiper-slide-shadow-bottom");
                    if (shadowBefore.length === 0) {
                        shadowBefore = $('<div class="swiper-slide-shadow-' + (swiper.isHorizontal() ? "left" : "top") + '"></div>');
                        $slideEl.append(shadowBefore)
                    }
                    if (shadowAfter.length === 0) {
                        shadowAfter = $('<div class="swiper-slide-shadow-' + (swiper.isHorizontal() ? "right" : "bottom") + '"></div>');
                        $slideEl.append(shadowAfter)
                    }
                    if (shadowBefore.length) {
                        shadowBefore[0].style.opacity = Math.max(-progress, 0)
                    }
                    if (shadowAfter.length) {
                        shadowAfter[0].style.opacity = Math.max(progress, 0)
                    }
                }
                $slideEl.transform("translate3d(" + tx + "px, " + ty + "px, 0px) rotateX(" + rotateX + "deg) rotateY(" + rotateY + "deg)")
            }
        }, setTransition: function setTransition(duration) {
            var swiper = this;
            var slides = swiper.slides;
            var activeIndex = swiper.activeIndex;
            var $wrapperEl = swiper.$wrapperEl;
            slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration);
            if (swiper.params.virtualTranslate && duration !== 0) {
                var eventTriggered = false;
                slides.eq(activeIndex).transitionEnd(function onTransitionEnd() {
                    if (eventTriggered) {
                        return
                    }
                    if (!swiper || swiper.destroyed) {
                        return
                    }
                    eventTriggered = true;
                    swiper.animating = false;
                    var triggerEvents = ["webkitTransitionEnd", "transitionend"];
                    for (var i = 0; i < triggerEvents.length; i += 1) {
                        $wrapperEl.trigger(triggerEvents[i])
                    }
                })
            }
        }
    };
    var EffectFlip = {
        name: "effect-flip",
        params: {flipEffect: {slideShadows: true, limitRotation: true}},
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                flipEffect: {
                    setTranslate: Flip.setTranslate.bind(swiper),
                    setTransition: Flip.setTransition.bind(swiper)
                }
            })
        },
        on: {
            beforeInit: function beforeInit() {
                var swiper = this;
                if (swiper.params.effect !== "flip") {
                    return
                }
                swiper.classNames.push(swiper.params.containerModifierClass + "flip");
                swiper.classNames.push(swiper.params.containerModifierClass + "3d");
                var overwriteParams = {
                    slidesPerView: 1,
                    slidesPerColumn: 1,
                    slidesPerGroup: 1,
                    watchSlidesProgress: true,
                    spaceBetween: 0,
                    virtualTranslate: true
                };
                Utils.extend(swiper.params, overwriteParams);
                Utils.extend(swiper.originalParams, overwriteParams)
            }, setTranslate: function setTranslate() {
                var swiper = this;
                if (swiper.params.effect !== "flip") {
                    return
                }
                swiper.flipEffect.setTranslate()
            }, setTransition: function setTransition(duration) {
                var swiper = this;
                if (swiper.params.effect !== "flip") {
                    return
                }
                swiper.flipEffect.setTransition(duration)
            }
        }
    };
    var Coverflow = {
        setTranslate: function setTranslate() {
            var swiper = this;
            var swiperWidth = swiper.width;
            var swiperHeight = swiper.height;
            var slides = swiper.slides;
            var $wrapperEl = swiper.$wrapperEl;
            var slidesSizesGrid = swiper.slidesSizesGrid;
            var params = swiper.params.coverflowEffect;
            var isHorizontal = swiper.isHorizontal();
            var transform = swiper.translate;
            var center = isHorizontal ? -transform + swiperWidth / 2 : -transform + swiperHeight / 2;
            var rotate = isHorizontal ? params.rotate : -params.rotate;
            var translate = params.depth;
            for (var i = 0, length = slides.length; i < length; i += 1) {
                var $slideEl = slides.eq(i);
                var slideSize = slidesSizesGrid[i];
                var slideOffset = $slideEl[0].swiperSlideOffset;
                var offsetMultiplier = (center - slideOffset - slideSize / 2) / slideSize * params.modifier;
                var rotateY = isHorizontal ? rotate * offsetMultiplier : 0;
                var rotateX = isHorizontal ? 0 : rotate * offsetMultiplier;
                var translateZ = -translate * Math.abs(offsetMultiplier);
                var stretch = params.stretch;
                if (typeof stretch === "string" && stretch.indexOf("%") !== -1) {
                    stretch = parseFloat(params.stretch) / 100 * slideSize
                }
                var translateY = isHorizontal ? 0 : stretch * offsetMultiplier;
                var translateX = isHorizontal ? stretch * offsetMultiplier : 0;
                var scale = 1 - (1 - params.scale) * Math.abs(offsetMultiplier);
                if (Math.abs(translateX) < .001) {
                    translateX = 0
                }
                if (Math.abs(translateY) < .001) {
                    translateY = 0
                }
                if (Math.abs(translateZ) < .001) {
                    translateZ = 0
                }
                if (Math.abs(rotateY) < .001) {
                    rotateY = 0
                }
                if (Math.abs(rotateX) < .001) {
                    rotateX = 0
                }
                if (Math.abs(scale) < .001) {
                    scale = 0
                }
                var slideTransform = "translate3d(" + translateX + "px," + translateY + "px," + translateZ + "px)  rotateX(" + rotateX + "deg) rotateY(" + rotateY + "deg) scale(" + scale + ")";
                $slideEl.transform(slideTransform);
                $slideEl[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;
                if (params.slideShadows) {
                    var $shadowBeforeEl = isHorizontal ? $slideEl.find(".swiper-slide-shadow-left") : $slideEl.find(".swiper-slide-shadow-top");
                    var $shadowAfterEl = isHorizontal ? $slideEl.find(".swiper-slide-shadow-right") : $slideEl.find(".swiper-slide-shadow-bottom");
                    if ($shadowBeforeEl.length === 0) {
                        $shadowBeforeEl = $('<div class="swiper-slide-shadow-' + (isHorizontal ? "left" : "top") + '"></div>');
                        $slideEl.append($shadowBeforeEl)
                    }
                    if ($shadowAfterEl.length === 0) {
                        $shadowAfterEl = $('<div class="swiper-slide-shadow-' + (isHorizontal ? "right" : "bottom") + '"></div>');
                        $slideEl.append($shadowAfterEl)
                    }
                    if ($shadowBeforeEl.length) {
                        $shadowBeforeEl[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0
                    }
                    if ($shadowAfterEl.length) {
                        $shadowAfterEl[0].style.opacity = -offsetMultiplier > 0 ? -offsetMultiplier : 0
                    }
                }
            }
            if (Support.pointerEvents || Support.prefixedPointerEvents) {
                var ws = $wrapperEl[0].style;
                ws.perspectiveOrigin = center + "px 50%"
            }
        }, setTransition: function setTransition(duration) {
            var swiper = this;
            swiper.slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration)
        }
    };
    var EffectCoverflow = {
        name: "effect-coverflow",
        params: {coverflowEffect: {rotate: 50, stretch: 0, depth: 100, scale: 1, modifier: 1, slideShadows: true}},
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                coverflowEffect: {
                    setTranslate: Coverflow.setTranslate.bind(swiper),
                    setTransition: Coverflow.setTransition.bind(swiper)
                }
            })
        },
        on: {
            beforeInit: function beforeInit() {
                var swiper = this;
                if (swiper.params.effect !== "coverflow") {
                    return
                }
                swiper.classNames.push(swiper.params.containerModifierClass + "coverflow");
                swiper.classNames.push(swiper.params.containerModifierClass + "3d");
                swiper.params.watchSlidesProgress = true;
                swiper.originalParams.watchSlidesProgress = true
            }, setTranslate: function setTranslate() {
                var swiper = this;
                if (swiper.params.effect !== "coverflow") {
                    return
                }
                swiper.coverflowEffect.setTranslate()
            }, setTransition: function setTransition(duration) {
                var swiper = this;
                if (swiper.params.effect !== "coverflow") {
                    return
                }
                swiper.coverflowEffect.setTransition(duration)
            }
        }
    };
    var Thumbs = {
        init: function init() {
            var swiper = this;
            var ref = swiper.params;
            var thumbsParams = ref.thumbs;
            var SwiperClass = swiper.constructor;
            if (thumbsParams.swiper instanceof SwiperClass) {
                swiper.thumbs.swiper = thumbsParams.swiper;
                Utils.extend(swiper.thumbs.swiper.originalParams, {
                    watchSlidesProgress: true,
                    slideToClickedSlide: false
                });
                Utils.extend(swiper.thumbs.swiper.params, {watchSlidesProgress: true, slideToClickedSlide: false})
            } else if (Utils.isObject(thumbsParams.swiper)) {
                swiper.thumbs.swiper = new SwiperClass(Utils.extend({}, thumbsParams.swiper, {
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,
                    slideToClickedSlide: false
                }));
                swiper.thumbs.swiperCreated = true
            }
            swiper.thumbs.swiper.$el.addClass(swiper.params.thumbs.thumbsContainerClass);
            swiper.thumbs.swiper.on("tap", swiper.thumbs.onThumbClick)
        }, onThumbClick: function onThumbClick() {
            var swiper = this;
            var thumbsSwiper = swiper.thumbs.swiper;
            if (!thumbsSwiper) {
                return
            }
            var clickedIndex = thumbsSwiper.clickedIndex;
            var clickedSlide = thumbsSwiper.clickedSlide;
            if (clickedSlide && $(clickedSlide).hasClass(swiper.params.thumbs.slideThumbActiveClass)) {
                return
            }
            if (typeof clickedIndex === "undefined" || clickedIndex === null) {
                return
            }
            var slideToIndex;
            if (thumbsSwiper.params.loop) {
                slideToIndex = parseInt($(thumbsSwiper.clickedSlide).attr("data-swiper-slide-index"), 10)
            } else {
                slideToIndex = clickedIndex
            }
            if (swiper.params.loop) {
                var currentIndex = swiper.activeIndex;
                if (swiper.slides.eq(currentIndex).hasClass(swiper.params.slideDuplicateClass)) {
                    swiper.loopFix();
                    swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
                    currentIndex = swiper.activeIndex
                }
                var prevIndex = swiper.slides.eq(currentIndex).prevAll('[data-swiper-slide-index="' + slideToIndex + '"]').eq(0).index();
                var nextIndex = swiper.slides.eq(currentIndex).nextAll('[data-swiper-slide-index="' + slideToIndex + '"]').eq(0).index();
                if (typeof prevIndex === "undefined") {
                    slideToIndex = nextIndex
                } else if (typeof nextIndex === "undefined") {
                    slideToIndex = prevIndex
                } else if (nextIndex - currentIndex < currentIndex - prevIndex) {
                    slideToIndex = nextIndex
                } else {
                    slideToIndex = prevIndex
                }
            }
            swiper.slideTo(slideToIndex)
        }, update: function update(initial) {
            var swiper = this;
            var thumbsSwiper = swiper.thumbs.swiper;
            if (!thumbsSwiper) {
                return
            }
            var slidesPerView = thumbsSwiper.params.slidesPerView === "auto" ? thumbsSwiper.slidesPerViewDynamic() : thumbsSwiper.params.slidesPerView;
            var autoScrollOffset = swiper.params.thumbs.autoScrollOffset;
            var useOffset = autoScrollOffset && !thumbsSwiper.params.loop;
            if (swiper.realIndex !== thumbsSwiper.realIndex || useOffset) {
                var currentThumbsIndex = thumbsSwiper.activeIndex;
                var newThumbsIndex;
                var direction;
                if (thumbsSwiper.params.loop) {
                    if (thumbsSwiper.slides.eq(currentThumbsIndex).hasClass(thumbsSwiper.params.slideDuplicateClass)) {
                        thumbsSwiper.loopFix();
                        thumbsSwiper._clientLeft = thumbsSwiper.$wrapperEl[0].clientLeft;
                        currentThumbsIndex = thumbsSwiper.activeIndex
                    }
                    var prevThumbsIndex = thumbsSwiper.slides.eq(currentThumbsIndex).prevAll('[data-swiper-slide-index="' + swiper.realIndex + '"]').eq(0).index();
                    var nextThumbsIndex = thumbsSwiper.slides.eq(currentThumbsIndex).nextAll('[data-swiper-slide-index="' + swiper.realIndex + '"]').eq(0).index();
                    if (typeof prevThumbsIndex === "undefined") {
                        newThumbsIndex = nextThumbsIndex
                    } else if (typeof nextThumbsIndex === "undefined") {
                        newThumbsIndex = prevThumbsIndex
                    } else if (nextThumbsIndex - currentThumbsIndex === currentThumbsIndex - prevThumbsIndex) {
                        newThumbsIndex = currentThumbsIndex
                    } else if (nextThumbsIndex - currentThumbsIndex < currentThumbsIndex - prevThumbsIndex) {
                        newThumbsIndex = nextThumbsIndex
                    } else {
                        newThumbsIndex = prevThumbsIndex
                    }
                    direction = swiper.activeIndex > swiper.previousIndex ? "next" : "prev"
                } else {
                    newThumbsIndex = swiper.realIndex;
                    direction = newThumbsIndex > swiper.previousIndex ? "next" : "prev"
                }
                if (useOffset) {
                    newThumbsIndex += direction === "next" ? autoScrollOffset : -1 * autoScrollOffset
                }
                if (thumbsSwiper.visibleSlidesIndexes && thumbsSwiper.visibleSlidesIndexes.indexOf(newThumbsIndex) < 0) {
                    if (thumbsSwiper.params.centeredSlides) {
                        if (newThumbsIndex > currentThumbsIndex) {
                            newThumbsIndex = newThumbsIndex - Math.floor(slidesPerView / 2) + 1
                        } else {
                            newThumbsIndex = newThumbsIndex + Math.floor(slidesPerView / 2) - 1
                        }
                    } else if (newThumbsIndex > currentThumbsIndex) {
                        newThumbsIndex = newThumbsIndex - slidesPerView + 1
                    }
                    thumbsSwiper.slideTo(newThumbsIndex, initial ? 0 : undefined)
                }
            }
            var thumbsToActivate = 1;
            var thumbActiveClass = swiper.params.thumbs.slideThumbActiveClass;
            if (swiper.params.slidesPerView > 1 && !swiper.params.centeredSlides) {
                thumbsToActivate = swiper.params.slidesPerView
            }
            if (!swiper.params.thumbs.multipleActiveThumbs) {
                thumbsToActivate = 1
            }
            thumbsToActivate = Math.floor(thumbsToActivate);
            thumbsSwiper.slides.removeClass(thumbActiveClass);
            if (thumbsSwiper.params.loop || thumbsSwiper.params.virtual && thumbsSwiper.params.virtual.enabled) {
                for (var i = 0; i < thumbsToActivate; i += 1) {
                    thumbsSwiper.$wrapperEl.children('[data-swiper-slide-index="' + (swiper.realIndex + i) + '"]').addClass(thumbActiveClass)
                }
            } else {
                for (var i$1 = 0; i$1 < thumbsToActivate; i$1 += 1) {
                    thumbsSwiper.slides.eq(swiper.realIndex + i$1).addClass(thumbActiveClass)
                }
            }
        }
    };
    var Thumbs$1 = {
        name: "thumbs",
        params: {
            thumbs: {
                swiper: null,
                multipleActiveThumbs: true,
                autoScrollOffset: 0,
                slideThumbActiveClass: "swiper-slide-thumb-active",
                thumbsContainerClass: "swiper-container-thumbs"
            }
        },
        create: function create() {
            var swiper = this;
            Utils.extend(swiper, {
                thumbs: {
                    swiper: null,
                    init: Thumbs.init.bind(swiper),
                    update: Thumbs.update.bind(swiper),
                    onThumbClick: Thumbs.onThumbClick.bind(swiper)
                }
            })
        },
        on: {
            beforeInit: function beforeInit() {
                var swiper = this;
                var ref = swiper.params;
                var thumbs = ref.thumbs;
                if (!thumbs || !thumbs.swiper) {
                    return
                }
                swiper.thumbs.init();
                swiper.thumbs.update(true)
            }, slideChange: function slideChange() {
                var swiper = this;
                if (!swiper.thumbs.swiper) {
                    return
                }
                swiper.thumbs.update()
            }, update: function update() {
                var swiper = this;
                if (!swiper.thumbs.swiper) {
                    return
                }
                swiper.thumbs.update()
            }, resize: function resize() {
                var swiper = this;
                if (!swiper.thumbs.swiper) {
                    return
                }
                swiper.thumbs.update()
            }, observerUpdate: function observerUpdate() {
                var swiper = this;
                if (!swiper.thumbs.swiper) {
                    return
                }
                swiper.thumbs.update()
            }, setTransition: function setTransition(duration) {
                var swiper = this;
                var thumbsSwiper = swiper.thumbs.swiper;
                if (!thumbsSwiper) {
                    return
                }
                thumbsSwiper.setTransition(duration)
            }, beforeDestroy: function beforeDestroy() {
                var swiper = this;
                var thumbsSwiper = swiper.thumbs.swiper;
                if (!thumbsSwiper) {
                    return
                }
                if (swiper.thumbs.swiperCreated && thumbsSwiper) {
                    thumbsSwiper.destroy()
                }
            }
        }
    };
    var components = [Device$1, Support$1, Browser$1, Resize, Observer$1, Virtual$1, Keyboard$1, Mousewheel$1, Navigation$1, Pagination$1, Scrollbar$1, Parallax$1, Zoom$1, Lazy$1, Controller$1, A11y, History$1, HashNavigation$1, Autoplay$1, EffectFade, EffectCube, EffectFlip, EffectCoverflow, Thumbs$1];
    if (typeof Swiper.use === "undefined") {
        Swiper.use = Swiper.Class.use;
        Swiper.installModule = Swiper.Class.installModule
    }
    Swiper.use(components);
    return Swiper
});
(function (window, document, $, undefined) {
    "use strict";
    window.console = window.console || {
        info: function (stuff) {
        }
    };
    if (!$) {
        return
    }
    if ($.fn.fancybox) {
        console.info("fancyBox already initialized");
        return
    }
    var defaults = {
        closeExisting: false,
        loop: false,
        gutter: 50,
        keyboard: true,
        preventCaptionOverlap: true,
        arrows: true,
        infobar: true,
        smallBtn: "auto",
        toolbar: "auto",
        buttons: ["zoom", "slideShow", "thumbs", "close"],
        idleTime: 3,
        protect: false,
        modal: false,
        image: {preload: false},
        ajax: {settings: {data: {fancybox: true}}},
        iframe: {
            tpl: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" allowfullscreen="allowfullscreen" allow="autoplay; fullscreen" src=""></iframe>',
            preload: true,
            css: {},
            attr: {scrolling: "auto"}
        },
        video: {
            tpl: '<video class="fancybox-video" controls controlsList="nodownload" poster="{{poster}}">' + '<source src="{{src}}" type="{{format}}" />' + 'Sorry, your browser doesn\'t support embedded videos, <a href="{{src}}">download</a> and watch with your favorite video player!' + "</video>",
            format: "",
            autoStart: true
        },
        defaultType: "image",
        animationEffect: "zoom",
        animationDuration: 366,
        zoomOpacity: "auto",
        transitionEffect: "fade",
        transitionDuration: 366,
        slideClass: "",
        baseClass: "",
        baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' + '<div class="fancybox-bg"></div>' + '<div class="fancybox-inner">' + '<div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div>' + '<div class="fancybox-toolbar">{{buttons}}</div>' + '<div class="fancybox-navigation">{{arrows}}</div>' + '<div class="fancybox-stage"></div>' + '<div class="fancybox-caption"><div class="fancybox-caption__body"></div></div>' + "</div>" + "</div>",
        spinnerTpl: '<div class="fancybox-loading"></div>',
        errorTpl: '<div class="fancybox-error"><p>{{ERROR}}</p></div>',
        btnTpl: {
            download: '<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="{{DOWNLOAD}}" href="javascript:;">' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.62 17.09V19H5.38v-1.91zm-2.97-6.96L17 11.45l-5 4.87-5-4.87 1.36-1.32 2.68 2.64V5h1.92v7.77z"/></svg>' + "</a>",
            zoom: '<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="{{ZOOM}}">' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.7 17.3l-3-3a5.9 5.9 0 0 0-.6-7.6 5.9 5.9 0 0 0-8.4 0 5.9 5.9 0 0 0 0 8.4 5.9 5.9 0 0 0 7.7.7l3 3a1 1 0 0 0 1.3 0c.4-.5.4-1 0-1.5zM8.1 13.8a4 4 0 0 1 0-5.7 4 4 0 0 1 5.7 0 4 4 0 0 1 0 5.7 4 4 0 0 1-5.7 0z"/></svg>' + "</button>",
            close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"/></svg>' + "</button>",
            arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' + '<div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.28 15.7l-1.34 1.37L5 12l4.94-5.07 1.34 1.38-2.68 2.72H19v1.94H8.6z"/></svg></div>' + "</button>",
            arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' + '<div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15.4 12.97l-2.68 2.72 1.34 1.38L19 12l-4.94-5.07-1.34 1.38 2.68 2.72H5v1.94z"/></svg></div>' + "</button>",
            smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">' + '<svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"/></svg>' + "</button>"
        },
        parentEl: "body",
        hideScrollbar: true,
        autoFocus: true,
        backFocus: true,
        trapFocus: true,
        fullScreen: {autoStart: false},
        touch: {vertical: true, momentum: true},
        hash: null,
        media: {},
        slideShow: {autoStart: false, speed: 3e3},
        thumbs: {autoStart: false, hideOnClose: true, parentEl: ".fancybox-container", axis: "y"},
        wheel: "auto",
        onInit: $.noop,
        beforeLoad: $.noop,
        afterLoad: $.noop,
        beforeShow: $.noop,
        afterShow: $.noop,
        beforeClose: $.noop,
        afterClose: $.noop,
        onActivate: $.noop,
        onDeactivate: $.noop,
        clickContent: function (current, event) {
            return current.type === "image" ? "zoom" : false
        },
        clickSlide: "close",
        clickOutside: "close",
        dblclickContent: false,
        dblclickSlide: false,
        dblclickOutside: false,
        mobile: {
            preventCaptionOverlap: false, idleTime: false, clickContent: function (current, event) {
                return current.type === "image" ? "toggleControls" : false
            }, clickSlide: function (current, event) {
                return current.type === "image" ? "toggleControls" : "close"
            }, dblclickContent: function (current, event) {
                return current.type === "image" ? "zoom" : false
            }, dblclickSlide: function (current, event) {
                return current.type === "image" ? "zoom" : false
            }
        },
        lang: "en",
        i18n: {
            en: {
                CLOSE: "Close",
                NEXT: "Next",
                PREV: "Previous",
                ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
                PLAY_START: "Start slideshow",
                PLAY_STOP: "Pause slideshow",
                FULL_SCREEN: "Full screen",
                THUMBS: "Thumbnails",
                DOWNLOAD: "Download",
                SHARE: "Share",
                ZOOM: "Zoom"
            },
            de: {
                CLOSE: "Schlie&szlig;en",
                NEXT: "Weiter",
                PREV: "Zur&uuml;ck",
                ERROR: "Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es sp&auml;ter nochmal.",
                PLAY_START: "Diaschau starten",
                PLAY_STOP: "Diaschau beenden",
                FULL_SCREEN: "Vollbild",
                THUMBS: "Vorschaubilder",
                DOWNLOAD: "Herunterladen",
                SHARE: "Teilen",
                ZOOM: "Vergr&ouml;&szlig;ern"
            }
        }
    };
    var $W = $(window);
    var $D = $(document);
    var called = 0;
    var isQuery = function (obj) {
        return obj && obj.hasOwnProperty && obj instanceof $
    };
    var requestAFrame = function () {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || function (callback) {
            return window.setTimeout(callback, 1e3 / 60)
        }
    }();
    var cancelAFrame = function () {
        return window.cancelAnimationFrame || window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || window.oCancelAnimationFrame || function (id) {
            window.clearTimeout(id)
        }
    }();
    var transitionEnd = function () {
        var el = document.createElement("fakeelement"), t;
        var transitions = {
            transition: "transitionend",
            OTransition: "oTransitionEnd",
            MozTransition: "transitionend",
            WebkitTransition: "webkitTransitionEnd"
        };
        for (t in transitions) {
            if (el.style[t] !== undefined) {
                return transitions[t]
            }
        }
        return "transitionend"
    }();
    var forceRedraw = function ($el) {
        return $el && $el.length && $el[0].offsetHeight
    };
    var mergeOpts = function (opts1, opts2) {
        var rez = $.extend(true, {}, opts1, opts2);
        $.each(opts2, function (key, value) {
            if ($.isArray(value)) {
                rez[key] = value
            }
        });
        return rez
    };
    var inViewport = function (elem) {
        var elemCenter, rez;
        if (!elem || elem.ownerDocument !== document) {
            return false
        }
        $(".fancybox-container").css("pointer-events", "none");
        elemCenter = {
            x: elem.getBoundingClientRect().left + elem.offsetWidth / 2,
            y: elem.getBoundingClientRect().top + elem.offsetHeight / 2
        };
        rez = document.elementFromPoint(elemCenter.x, elemCenter.y) === elem;
        $(".fancybox-container").css("pointer-events", "");
        return rez
    };
    var FancyBox = function (content, opts, index) {
        var self = this;
        self.opts = mergeOpts({index: index}, $.fancybox.defaults);
        if ($.isPlainObject(opts)) {
            self.opts = mergeOpts(self.opts, opts)
        }
        if ($.fancybox.isMobile) {
            self.opts = mergeOpts(self.opts, self.opts.mobile)
        }
        self.id = self.opts.id || ++called;
        self.currIndex = parseInt(self.opts.index, 10) || 0;
        self.prevIndex = null;
        self.prevPos = null;
        self.currPos = 0;
        self.firstRun = true;
        self.group = [];
        self.slides = {};
        self.addContent(content);
        if (!self.group.length) {
            return
        }
        self.init()
    };
    $.extend(FancyBox.prototype, {
        init: function () {
            var self = this, firstItem = self.group[self.currIndex], firstItemOpts = firstItem.opts, $container,
                buttonStr;
            if (firstItemOpts.closeExisting) {
                $.fancybox.close(true)
            }
            $("body").addClass("fancybox-active");
            if (!$.fancybox.getInstance() && firstItemOpts.hideScrollbar !== false && !$.fancybox.isMobile && document.body.scrollHeight > window.innerHeight) {
                $("head").append('<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar{margin-right:' + (window.innerWidth - document.documentElement.clientWidth) + "px;}</style>");
                $("body").addClass("compensate-for-scrollbar")
            }
            buttonStr = "";
            $.each(firstItemOpts.buttons, function (index, value) {
                buttonStr += firstItemOpts.btnTpl[value] || ""
            });
            $container = $(self.translate(self, firstItemOpts.baseTpl.replace("{{buttons}}", buttonStr).replace("{{arrows}}", firstItemOpts.btnTpl.arrowLeft + firstItemOpts.btnTpl.arrowRight))).attr("id", "fancybox-container-" + self.id).addClass(firstItemOpts.baseClass).data("FancyBox", self).appendTo(firstItemOpts.parentEl);
            self.$refs = {container: $container};
            ["bg", "inner", "infobar", "toolbar", "stage", "caption", "navigation"].forEach(function (item) {
                self.$refs[item] = $container.find(".fancybox-" + item)
            });
            self.trigger("onInit");
            self.activate();
            self.jumpTo(self.currIndex)
        }, translate: function (obj, str) {
            var arr = obj.opts.i18n[obj.opts.lang] || obj.opts.i18n.en;
            return str.replace(/\{\{(\w+)\}\}/g, function (match, n) {
                return arr[n] === undefined ? match : arr[n]
            })
        }, addContent: function (content) {
            var self = this, items = $.makeArray(content), thumbs;
            $.each(items, function (i, item) {
                var obj = {}, opts = {}, $item, type, found, src, srcParts;
                if ($.isPlainObject(item)) {
                    obj = item;
                    opts = item.opts || item
                } else if ($.type(item) === "object" && $(item).length) {
                    $item = $(item);
                    opts = $item.data() || {};
                    opts = $.extend(true, {}, opts, opts.options);
                    opts.$orig = $item;
                    obj.src = self.opts.src || opts.src || $item.attr("href");
                    if (!obj.type && !obj.src) {
                        obj.type = "inline";
                        obj.src = item
                    }
                } else {
                    obj = {type: "html", src: item + ""}
                }
                obj.opts = $.extend(true, {}, self.opts, opts);
                if ($.isArray(opts.buttons)) {
                    obj.opts.buttons = opts.buttons
                }
                if ($.fancybox.isMobile && obj.opts.mobile) {
                    obj.opts = mergeOpts(obj.opts, obj.opts.mobile)
                }
                type = obj.type || obj.opts.type;
                src = obj.src || "";
                if (!type && src) {
                    if (found = src.match(/\.(mp4|mov|ogv|webm)((\?|#).*)?$/i)) {
                        type = "video";
                        if (!obj.opts.video.format) {
                            obj.opts.video.format = "video/" + (found[1] === "ogv" ? "ogg" : found[1])
                        }
                    } else if (src.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i)) {
                        type = "image"
                    } else if (src.match(/\.(pdf)((\?|#).*)?$/i)) {
                        type = "iframe";
                        obj = $.extend(true, obj, {contentType: "pdf", opts: {iframe: {preload: false}}})
                    } else if (src.charAt(0) === "#") {
                        type = "inline"
                    }
                }
                if (type) {
                    obj.type = type
                } else {
                    self.trigger("objectNeedsType", obj)
                }
                if (!obj.contentType) {
                    obj.contentType = $.inArray(obj.type, ["html", "inline", "ajax"]) > -1 ? "html" : obj.type
                }
                obj.index = self.group.length;
                if (obj.opts.smallBtn == "auto") {
                    obj.opts.smallBtn = $.inArray(obj.type, ["html", "inline", "ajax"]) > -1
                }
                if (obj.opts.toolbar === "auto") {
                    obj.opts.toolbar = !obj.opts.smallBtn
                }
                obj.$thumb = obj.opts.$thumb || null;
                if (obj.opts.$trigger && obj.index === self.opts.index) {
                    obj.$thumb = obj.opts.$trigger.find("img:first");
                    if (obj.$thumb.length) {
                        obj.opts.$orig = obj.opts.$trigger
                    }
                }
                if (!(obj.$thumb && obj.$thumb.length) && obj.opts.$orig) {
                    obj.$thumb = obj.opts.$orig.find("img:first")
                }
                if (obj.$thumb && !obj.$thumb.length) {
                    obj.$thumb = null
                }
                obj.thumb = obj.opts.thumb || (obj.$thumb ? obj.$thumb[0].src : null);
                if ($.type(obj.opts.caption) === "function") {
                    obj.opts.caption = obj.opts.caption.apply(item, [self, obj])
                }
                if ($.type(self.opts.caption) === "function") {
                    obj.opts.caption = self.opts.caption.apply(item, [self, obj])
                }
                if (!(obj.opts.caption instanceof $)) {
                    obj.opts.caption = obj.opts.caption === undefined ? "" : obj.opts.caption + ""
                }
                if (obj.type === "ajax") {
                    srcParts = src.split(/\s+/, 2);
                    if (srcParts.length > 1) {
                        obj.src = srcParts.shift();
                        obj.opts.filter = srcParts.shift()
                    }
                }
                if (obj.opts.modal) {
                    obj.opts = $.extend(true, obj.opts, {
                        trapFocus: true,
                        infobar: 0,
                        toolbar: 0,
                        smallBtn: 0,
                        keyboard: 0,
                        slideShow: 0,
                        fullScreen: 0,
                        thumbs: 0,
                        touch: 0,
                        clickContent: false,
                        clickSlide: false,
                        clickOutside: false,
                        dblclickContent: false,
                        dblclickSlide: false,
                        dblclickOutside: false
                    })
                }
                self.group.push(obj)
            });
            if (Object.keys(self.slides).length) {
                self.updateControls();
                thumbs = self.Thumbs;
                if (thumbs && thumbs.isActive) {
                    thumbs.create();
                    thumbs.focus()
                }
            }
        }, addEvents: function () {
            var self = this;
            self.removeEvents();
            self.$refs.container.on("click.fb-close", "[data-fancybox-close]", function (e) {
                e.stopPropagation();
                e.preventDefault();
                self.close(e)
            }).on("touchstart.fb-prev click.fb-prev", "[data-fancybox-prev]", function (e) {
                e.stopPropagation();
                e.preventDefault();
                self.previous()
            }).on("touchstart.fb-next click.fb-next", "[data-fancybox-next]", function (e) {
                e.stopPropagation();
                e.preventDefault();
                self.next()
            }).on("click.fb", "[data-fancybox-zoom]", function (e) {
                self[self.isScaledDown() ? "scaleToActual" : "scaleToFit"]()
            });
            $W.on("orientationchange.fb resize.fb", function (e) {
                if (e && e.originalEvent && e.originalEvent.type === "resize") {
                    if (self.requestId) {
                        cancelAFrame(self.requestId)
                    }
                    self.requestId = requestAFrame(function () {
                        self.update(e)
                    })
                } else {
                    if (self.current && self.current.type === "iframe") {
                        self.$refs.stage.hide()
                    }
                    setTimeout(function () {
                        self.$refs.stage.show();
                        self.update(e)
                    }, $.fancybox.isMobile ? 600 : 250)
                }
            });
            $D.on("keydown.fb", function (e) {
                var instance = $.fancybox ? $.fancybox.getInstance() : null, current = instance.current,
                    keycode = e.keyCode || e.which;
                if (keycode == 9) {
                    if (current.opts.trapFocus) {
                        self.focus(e)
                    }
                    return
                }
                if (!current.opts.keyboard || e.ctrlKey || e.altKey || e.shiftKey || $(e.target).is("input,textarea,video,audio,select")) {
                    return
                }
                if (keycode === 8 || keycode === 27) {
                    e.preventDefault();
                    self.close(e);
                    return
                }
                if (keycode === 37 || keycode === 38) {
                    e.preventDefault();
                    self.previous();
                    return
                }
                if (keycode === 39 || keycode === 40) {
                    e.preventDefault();
                    self.next();
                    return
                }
                self.trigger("afterKeydown", e, keycode)
            });
            if (self.group[self.currIndex].opts.idleTime) {
                self.idleSecondsCounter = 0;
                $D.on("mousemove.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle", function (e) {
                    self.idleSecondsCounter = 0;
                    if (self.isIdle) {
                        self.showControls()
                    }
                    self.isIdle = false
                });
                self.idleInterval = window.setInterval(function () {
                    self.idleSecondsCounter++;
                    if (self.idleSecondsCounter >= self.group[self.currIndex].opts.idleTime && !self.isDragging) {
                        self.isIdle = true;
                        self.idleSecondsCounter = 0;
                        self.hideControls()
                    }
                }, 1e3)
            }
        }, removeEvents: function () {
            var self = this;
            $W.off("orientationchange.fb resize.fb");
            $D.off("keydown.fb .fb-idle");
            this.$refs.container.off(".fb-close .fb-prev .fb-next");
            if (self.idleInterval) {
                window.clearInterval(self.idleInterval);
                self.idleInterval = null
            }
        }, previous: function (duration) {
            return this.jumpTo(this.currPos - 1, duration)
        }, next: function (duration) {
            return this.jumpTo(this.currPos + 1, duration)
        }, jumpTo: function (pos, duration) {
            var self = this, groupLen = self.group.length, firstRun, isMoved, loop, current, previous, slidePos,
                stagePos, prop, diff;
            if (self.isDragging || self.isClosing || self.isAnimating && self.firstRun) {
                return
            }
            pos = parseInt(pos, 10);
            loop = self.current ? self.current.opts.loop : self.opts.loop;
            if (!loop && (pos < 0 || pos >= groupLen)) {
                return false
            }
            firstRun = self.firstRun = !Object.keys(self.slides).length;
            previous = self.current;
            self.prevIndex = self.currIndex;
            self.prevPos = self.currPos;
            current = self.createSlide(pos);
            if (groupLen > 1) {
                if (loop || current.index < groupLen - 1) {
                    self.createSlide(pos + 1)
                }
                if (loop || current.index > 0) {
                    self.createSlide(pos - 1)
                }
            }
            self.current = current;
            self.currIndex = current.index;
            self.currPos = current.pos;
            self.trigger("beforeShow", firstRun);
            self.updateControls();
            current.forcedDuration = undefined;
            if ($.isNumeric(duration)) {
                current.forcedDuration = duration
            } else {
                duration = current.opts[firstRun ? "animationDuration" : "transitionDuration"]
            }
            duration = parseInt(duration, 10);
            isMoved = self.isMoved(current);
            current.$slide.addClass("fancybox-slide--current");
            if (firstRun) {
                if (current.opts.animationEffect && duration) {
                    self.$refs.container.css("transition-duration", duration + "ms")
                }
                self.$refs.container.addClass("fancybox-is-open").trigger("focus");
                self.loadSlide(current);
                self.preload("image");
                return
            }
            slidePos = $.fancybox.getTranslate(previous.$slide);
            stagePos = $.fancybox.getTranslate(self.$refs.stage);
            $.each(self.slides, function (index, slide) {
                $.fancybox.stop(slide.$slide, true)
            });
            if (previous.pos !== current.pos) {
                previous.isComplete = false
            }
            previous.$slide.removeClass("fancybox-slide--complete fancybox-slide--current");
            if (isMoved) {
                diff = slidePos.left - (previous.pos * slidePos.width + previous.pos * previous.opts.gutter);
                $.each(self.slides, function (index, slide) {
                    slide.$slide.removeClass("fancybox-animated").removeClass(function (index, className) {
                        return (className.match(/(^|\s)fancybox-fx-\S+/g) || []).join(" ")
                    });
                    var leftPos = slide.pos * slidePos.width + slide.pos * slide.opts.gutter;
                    $.fancybox.setTranslate(slide.$slide, {top: 0, left: leftPos - stagePos.left + diff});
                    if (slide.pos !== current.pos) {
                        slide.$slide.addClass("fancybox-slide--" + (slide.pos > current.pos ? "next" : "previous"))
                    }
                    forceRedraw(slide.$slide);
                    $.fancybox.animate(slide.$slide, {
                        top: 0,
                        left: (slide.pos - current.pos) * slidePos.width + (slide.pos - current.pos) * slide.opts.gutter
                    }, duration, function () {
                        slide.$slide.css({
                            transform: "",
                            opacity: ""
                        }).removeClass("fancybox-slide--next fancybox-slide--previous");
                        if (slide.pos === self.currPos) {
                            self.complete()
                        }
                    })
                })
            } else if (duration && current.opts.transitionEffect) {
                prop = "fancybox-animated fancybox-fx-" + current.opts.transitionEffect;
                previous.$slide.addClass("fancybox-slide--" + (previous.pos > current.pos ? "next" : "previous"));
                $.fancybox.animate(previous.$slide, prop, duration, function () {
                    previous.$slide.removeClass(prop).removeClass("fancybox-slide--next fancybox-slide--previous")
                }, false)
            }
            if (current.isLoaded) {
                self.revealContent(current)
            } else {
                self.loadSlide(current)
            }
            self.preload("image")
        }, createSlide: function (pos) {
            var self = this, $slide, index;
            index = pos % self.group.length;
            index = index < 0 ? self.group.length + index : index;
            if (!self.slides[pos] && self.group[index]) {
                $slide = $('<div class="fancybox-slide"></div>').appendTo(self.$refs.stage);
                self.slides[pos] = $.extend(true, {}, self.group[index], {pos: pos, $slide: $slide, isLoaded: false});
                self.updateSlide(self.slides[pos])
            }
            return self.slides[pos]
        }, scaleToActual: function (x, y, duration) {
            var self = this, current = self.current, $content = current.$content,
                canvasWidth = $.fancybox.getTranslate(current.$slide).width,
                canvasHeight = $.fancybox.getTranslate(current.$slide).height, newImgWidth = current.width,
                newImgHeight = current.height, imgPos, posX, posY, scaleX, scaleY;
            if (self.isAnimating || self.isMoved() || !$content || !(current.type == "image" && current.isLoaded && !current.hasError)) {
                return
            }
            self.isAnimating = true;
            $.fancybox.stop($content);
            x = x === undefined ? canvasWidth * .5 : x;
            y = y === undefined ? canvasHeight * .5 : y;
            imgPos = $.fancybox.getTranslate($content);
            imgPos.top -= $.fancybox.getTranslate(current.$slide).top;
            imgPos.left -= $.fancybox.getTranslate(current.$slide).left;
            scaleX = newImgWidth / imgPos.width;
            scaleY = newImgHeight / imgPos.height;
            posX = canvasWidth * .5 - newImgWidth * .5;
            posY = canvasHeight * .5 - newImgHeight * .5;
            if (newImgWidth > canvasWidth) {
                posX = imgPos.left * scaleX - (x * scaleX - x);
                if (posX > 0) {
                    posX = 0
                }
                if (posX < canvasWidth - newImgWidth) {
                    posX = canvasWidth - newImgWidth
                }
            }
            if (newImgHeight > canvasHeight) {
                posY = imgPos.top * scaleY - (y * scaleY - y);
                if (posY > 0) {
                    posY = 0
                }
                if (posY < canvasHeight - newImgHeight) {
                    posY = canvasHeight - newImgHeight
                }
            }
            self.updateCursor(newImgWidth, newImgHeight);
            $.fancybox.animate($content, {
                top: posY,
                left: posX,
                scaleX: scaleX,
                scaleY: scaleY
            }, duration || 366, function () {
                self.isAnimating = false
            });
            if (self.SlideShow && self.SlideShow.isActive) {
                self.SlideShow.stop()
            }
        }, scaleToFit: function (duration) {
            var self = this, current = self.current, $content = current.$content, end;
            if (self.isAnimating || self.isMoved() || !$content || !(current.type == "image" && current.isLoaded && !current.hasError)) {
                return
            }
            self.isAnimating = true;
            $.fancybox.stop($content);
            end = self.getFitPos(current);
            self.updateCursor(end.width, end.height);
            $.fancybox.animate($content, {
                top: end.top,
                left: end.left,
                scaleX: end.width / $content.width(),
                scaleY: end.height / $content.height()
            }, duration || 366, function () {
                self.isAnimating = false
            })
        }, getFitPos: function (slide) {
            var self = this, $content = slide.$content, $slide = slide.$slide, width = slide.width || slide.opts.width,
                height = slide.height || slide.opts.height, maxWidth, maxHeight, minRatio, aspectRatio, rez = {};
            if (!slide.isLoaded || !$content || !$content.length) {
                return false
            }
            maxWidth = $.fancybox.getTranslate(self.$refs.stage).width;
            maxHeight = $.fancybox.getTranslate(self.$refs.stage).height;
            maxWidth -= parseFloat($slide.css("paddingLeft")) + parseFloat($slide.css("paddingRight")) + parseFloat($content.css("marginLeft")) + parseFloat($content.css("marginRight"));
            maxHeight -= parseFloat($slide.css("paddingTop")) + parseFloat($slide.css("paddingBottom")) + parseFloat($content.css("marginTop")) + parseFloat($content.css("marginBottom"));
            if (!width || !height) {
                width = maxWidth;
                height = maxHeight
            }
            minRatio = Math.min(1, maxWidth / width, maxHeight / height);
            width = minRatio * width;
            height = minRatio * height;
            if (width > maxWidth - .5) {
                width = maxWidth
            }
            if (height > maxHeight - .5) {
                height = maxHeight
            }
            if (slide.type === "image") {
                rez.top = Math.floor((maxHeight - height) * .5) + parseFloat($slide.css("paddingTop"));
                rez.left = Math.floor((maxWidth - width) * .5) + parseFloat($slide.css("paddingLeft"))
            } else if (slide.contentType === "video") {
                aspectRatio = slide.opts.width && slide.opts.height ? width / height : slide.opts.ratio || 16 / 9;
                if (height > width / aspectRatio) {
                    height = width / aspectRatio
                } else if (width > height * aspectRatio) {
                    width = height * aspectRatio
                }
            }
            rez.width = width;
            rez.height = height;
            return rez
        }, update: function (e) {
            var self = this;
            $.each(self.slides, function (key, slide) {
                self.updateSlide(slide, e)
            })
        }, updateSlide: function (slide, e) {
            var self = this, $content = slide && slide.$content, width = slide.width || slide.opts.width,
                height = slide.height || slide.opts.height, $slide = slide.$slide;
            self.adjustCaption(slide);
            if ($content && (width || height || slide.contentType === "video") && !slide.hasError) {
                $.fancybox.stop($content);
                $.fancybox.setTranslate($content, self.getFitPos(slide));
                if (slide.pos === self.currPos) {
                    self.isAnimating = false;
                    self.updateCursor()
                }
            }
            self.adjustLayout(slide);
            if ($slide.length) {
                $slide.trigger("refresh");
                if (slide.pos === self.currPos) {
                    self.$refs.toolbar.add(self.$refs.navigation.find(".fancybox-button--arrow_right")).toggleClass("compensate-for-scrollbar", $slide.get(0).scrollHeight > $slide.get(0).clientHeight)
                }
            }
            self.trigger("onUpdate", slide, e)
        }, centerSlide: function (duration) {
            var self = this, current = self.current, $slide = current.$slide;
            if (self.isClosing || !current) {
                return
            }
            $slide.siblings().css({transform: "", opacity: ""});
            $slide.parent().children().removeClass("fancybox-slide--previous fancybox-slide--next");
            $.fancybox.animate($slide, {
                top: 0,
                left: 0,
                opacity: 1
            }, duration === undefined ? 0 : duration, function () {
                $slide.css({transform: "", opacity: ""});
                if (!current.isComplete) {
                    self.complete()
                }
            }, false)
        }, isMoved: function (slide) {
            var current = slide || this.current, slidePos, stagePos;
            if (!current) {
                return false
            }
            stagePos = $.fancybox.getTranslate(this.$refs.stage);
            slidePos = $.fancybox.getTranslate(current.$slide);
            return !current.$slide.hasClass("fancybox-animated") && (Math.abs(slidePos.top - stagePos.top) > .5 || Math.abs(slidePos.left - stagePos.left) > .5)
        }, updateCursor: function (nextWidth, nextHeight) {
            var self = this, current = self.current, $container = self.$refs.container, canPan, isZoomable;
            if (!current || self.isClosing || !self.Guestures) {
                return
            }
            $container.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-zoomOut fancybox-can-swipe fancybox-can-pan");
            canPan = self.canPan(nextWidth, nextHeight);
            isZoomable = canPan ? true : self.isZoomable();
            $container.toggleClass("fancybox-is-zoomable", isZoomable);
            $("[data-fancybox-zoom]").prop("disabled", !isZoomable);
            if (canPan) {
                $container.addClass("fancybox-can-pan")
            } else if (isZoomable && (current.opts.clickContent === "zoom" || $.isFunction(current.opts.clickContent) && current.opts.clickContent(current) == "zoom")) {
                $container.addClass("fancybox-can-zoomIn")
            } else if (current.opts.touch && (current.opts.touch.vertical || self.group.length > 1) && current.contentType !== "video") {
                $container.addClass("fancybox-can-swipe")
            }
        }, isZoomable: function () {
            var self = this, current = self.current, fitPos;
            if (current && !self.isClosing && current.type === "image" && !current.hasError) {
                if (!current.isLoaded) {
                    return true
                }
                fitPos = self.getFitPos(current);
                if (fitPos && (current.width > fitPos.width || current.height > fitPos.height)) {
                    return true
                }
            }
            return false
        }, isScaledDown: function (nextWidth, nextHeight) {
            var self = this, rez = false, current = self.current, $content = current.$content;
            if (nextWidth !== undefined && nextHeight !== undefined) {
                rez = nextWidth < current.width && nextHeight < current.height
            } else if ($content) {
                rez = $.fancybox.getTranslate($content);
                rez = rez.width < current.width && rez.height < current.height
            }
            return rez
        }, canPan: function (nextWidth, nextHeight) {
            var self = this, current = self.current, pos = null, rez = false;
            if (current.type === "image" && (current.isComplete || nextWidth && nextHeight) && !current.hasError) {
                rez = self.getFitPos(current);
                if (nextWidth !== undefined && nextHeight !== undefined) {
                    pos = {width: nextWidth, height: nextHeight}
                } else if (current.isComplete) {
                    pos = $.fancybox.getTranslate(current.$content)
                }
                if (pos && rez) {
                    rez = Math.abs(pos.width - rez.width) > 1.5 || Math.abs(pos.height - rez.height) > 1.5
                }
            }
            return rez
        }, loadSlide: function (slide) {
            var self = this, type, $slide, ajaxLoad;
            if (slide.isLoading || slide.isLoaded) {
                return
            }
            slide.isLoading = true;
            if (self.trigger("beforeLoad", slide) === false) {
                slide.isLoading = false;
                return false
            }
            type = slide.type;
            $slide = slide.$slide;
            $slide.off("refresh").trigger("onReset").addClass(slide.opts.slideClass);
            switch (type) {
                case"image":
                    self.setImage(slide);
                    break;
                case"iframe":
                    self.setIframe(slide);
                    break;
                case"html":
                    self.setContent(slide, slide.src || slide.content);
                    break;
                case"video":
                    self.setContent(slide, slide.opts.video.tpl.replace(/\{\{src\}\}/gi, slide.src).replace("{{format}}", slide.opts.videoFormat || slide.opts.video.format || "").replace("{{poster}}", slide.thumb || ""));
                    break;
                case"inline":
                    if ($(slide.src).length) {
                        self.setContent(slide, $(slide.src))
                    } else {
                        self.setError(slide)
                    }
                    break;
                case"ajax":
                    self.showLoading(slide);
                    ajaxLoad = $.ajax($.extend({}, slide.opts.ajax.settings, {
                        url: slide.src,
                        success: function (data, textStatus) {
                            if (textStatus === "success") {
                                self.setContent(slide, data)
                            }
                        },
                        error: function (jqXHR, textStatus) {
                            if (jqXHR && textStatus !== "abort") {
                                self.setError(slide)
                            }
                        }
                    }));
                    $slide.one("onReset", function () {
                        ajaxLoad.abort()
                    });
                    break;
                default:
                    self.setError(slide);
                    break
            }
            return true
        }, setImage: function (slide) {
            var self = this, ghost;
            setTimeout(function () {
                var $img = slide.$image;
                if (!self.isClosing && slide.isLoading && (!$img || !$img.length || !$img[0].complete) && !slide.hasError) {
                    self.showLoading(slide)
                }
            }, 50);
            self.checkSrcset(slide);
            slide.$content = $('<div class="fancybox-content"></div>').addClass("fancybox-is-hidden").appendTo(slide.$slide.addClass("fancybox-slide--image"));
            if (slide.opts.preload !== false && slide.opts.width && slide.opts.height && slide.thumb) {
                slide.width = slide.opts.width;
                slide.height = slide.opts.height;
                ghost = document.createElement("img");
                ghost.onerror = function () {
                    $(this).remove();
                    slide.$ghost = null
                };
                ghost.onload = function () {
                    self.afterLoad(slide)
                };
                slide.$ghost = $(ghost).addClass("fancybox-image").appendTo(slide.$content).attr("src", slide.thumb)
            }
            self.setBigImage(slide)
        }, checkSrcset: function (slide) {
            var srcset = slide.opts.srcset || slide.opts.image.srcset, found, temp, pxRatio, windowWidth;
            if (srcset) {
                pxRatio = window.devicePixelRatio || 1;
                windowWidth = window.innerWidth * pxRatio;
                temp = srcset.split(",").map(function (el) {
                    var ret = {};
                    el.trim().split(/\s+/).forEach(function (el, i) {
                        var value = parseInt(el.substring(0, el.length - 1), 10);
                        if (i === 0) {
                            return ret.url = el
                        }
                        if (value) {
                            ret.value = value;
                            ret.postfix = el[el.length - 1]
                        }
                    });
                    return ret
                });
                temp.sort(function (a, b) {
                    return a.value - b.value
                });
                for (var j = 0; j < temp.length; j++) {
                    var el = temp[j];
                    if (el.postfix === "w" && el.value >= windowWidth || el.postfix === "x" && el.value >= pxRatio) {
                        found = el;
                        break
                    }
                }
                if (!found && temp.length) {
                    found = temp[temp.length - 1]
                }
                if (found) {
                    slide.src = found.url;
                    if (slide.width && slide.height && found.postfix == "w") {
                        slide.height = slide.width / slide.height * found.value;
                        slide.width = found.value
                    }
                    slide.opts.srcset = srcset
                }
            }
        }, setBigImage: function (slide) {
            var self = this, img = document.createElement("img"), $img = $(img);
            slide.$image = $img.one("error", function () {
                self.setError(slide)
            }).one("load", function () {
                var sizes;
                if (!slide.$ghost) {
                    self.resolveImageSlideSize(slide, this.naturalWidth, this.naturalHeight);
                    self.afterLoad(slide)
                }
                if (self.isClosing) {
                    return
                }
                if (slide.opts.srcset) {
                    sizes = slide.opts.sizes;
                    if (!sizes || sizes === "auto") {
                        sizes = (slide.width / slide.height > 1 && $W.width() / $W.height() > 1 ? "100" : Math.round(slide.width / slide.height * 100)) + "vw"
                    }
                    $img.attr("sizes", sizes).attr("srcset", slide.opts.srcset)
                }
                if (slide.$ghost) {
                    setTimeout(function () {
                        if (slide.$ghost && !self.isClosing) {
                            slide.$ghost.hide()
                        }
                    }, Math.min(300, Math.max(1e3, slide.height / 1600)))
                }
                self.hideLoading(slide)
            }).addClass("fancybox-image").attr("src", slide.src).appendTo(slide.$content);
            if ((img.complete || img.readyState == "complete") && $img.naturalWidth && $img.naturalHeight) {
                $img.trigger("load")
            } else if (img.error) {
                $img.trigger("error")
            }
        }, resolveImageSlideSize: function (slide, imgWidth, imgHeight) {
            var maxWidth = parseInt(slide.opts.width, 10), maxHeight = parseInt(slide.opts.height, 10);
            slide.width = imgWidth;
            slide.height = imgHeight;
            if (maxWidth > 0) {
                slide.width = maxWidth;
                slide.height = Math.floor(maxWidth * imgHeight / imgWidth)
            }
            if (maxHeight > 0) {
                slide.width = Math.floor(maxHeight * imgWidth / imgHeight);
                slide.height = maxHeight
            }
        }, setIframe: function (slide) {
            var self = this, opts = slide.opts.iframe, $slide = slide.$slide, $iframe;
            slide.$content = $('<div class="fancybox-content' + (opts.preload ? " fancybox-is-hidden" : "") + '"></div>').css(opts.css).appendTo($slide);
            $slide.addClass("fancybox-slide--" + slide.contentType);
            slide.$iframe = $iframe = $(opts.tpl.replace(/\{rnd\}/g, (new Date).getTime())).attr(opts.attr).appendTo(slide.$content);
            if (opts.preload) {
                self.showLoading(slide);
                $iframe.on("load.fb error.fb", function (e) {
                    this.isReady = 1;
                    slide.$slide.trigger("refresh");
                    self.afterLoad(slide)
                });
                $slide.on("refresh.fb", function () {
                    var $content = slide.$content, frameWidth = opts.css.width, frameHeight = opts.css.height,
                        $contents, $body;
                    if ($iframe[0].isReady !== 1) {
                        return
                    }
                    try {
                        $contents = $iframe.contents();
                        $body = $contents.find("body")
                    } catch (ignore) {
                    }
                    if ($body && $body.length && $body.children().length) {
                        $slide.css("overflow", "visible");
                        $content.css({width: "100%", "max-width": "100%", height: "9999px"});
                        if (frameWidth === undefined) {
                            frameWidth = Math.ceil(Math.max($body[0].clientWidth, $body.outerWidth(true)))
                        }
                        $content.css("width", frameWidth ? frameWidth : "").css("max-width", "");
                        if (frameHeight === undefined) {
                            frameHeight = Math.ceil(Math.max($body[0].clientHeight, $body.outerHeight(true)))
                        }
                        $content.css("height", frameHeight ? frameHeight : "");
                        $slide.css("overflow", "auto")
                    }
                    $content.removeClass("fancybox-is-hidden")
                })
            } else {
                self.afterLoad(slide)
            }
            $iframe.attr("src", slide.src);
            $slide.one("onReset", function () {
                try {
                    $(this).find("iframe").hide().unbind().attr("src", "//about:blank")
                } catch (ignore) {
                }
                $(this).off("refresh.fb").empty();
                slide.isLoaded = false;
                slide.isRevealed = false
            })
        }, setContent: function (slide, content) {
            var self = this;
            if (self.isClosing) {
                return
            }
            self.hideLoading(slide);
            if (slide.$content) {
                $.fancybox.stop(slide.$content)
            }
            slide.$slide.empty();
            if (isQuery(content) && content.parent().length) {
                if (content.hasClass("fancybox-content") || content.parent().hasClass("fancybox-content")) {
                    content.parents(".fancybox-slide").trigger("onReset")
                }
                slide.$placeholder = $("<div>").hide().insertAfter(content);
                content.css("display", "inline-block")
            } else if (!slide.hasError) {
                if ($.type(content) === "string") {
                    content = $("<div>").append($.trim(content)).contents()
                }
                if (slide.opts.filter) {
                    content = $("<div>").html(content).find(slide.opts.filter)
                }
            }
            slide.$slide.one("onReset", function () {
                $(this).find("video,audio").trigger("pause");
                if (slide.$placeholder) {
                    slide.$placeholder.after(content.removeClass("fancybox-content").hide()).remove();
                    slide.$placeholder = null
                }
                if (slide.$smallBtn) {
                    slide.$smallBtn.remove();
                    slide.$smallBtn = null
                }
                if (!slide.hasError) {
                    $(this).empty();
                    slide.isLoaded = false;
                    slide.isRevealed = false
                }
            });
            $(content).appendTo(slide.$slide);
            if ($(content).is("video,audio")) {
                $(content).addClass("fancybox-video");
                $(content).wrap("<div></div>");
                slide.contentType = "video";
                slide.opts.width = slide.opts.width || $(content).attr("width");
                slide.opts.height = slide.opts.height || $(content).attr("height")
            }
            slide.$content = slide.$slide.children().filter("div,form,main,video,audio,article,.fancybox-content").first();
            slide.$content.siblings().hide();
            if (!slide.$content.length) {
                slide.$content = slide.$slide.wrapInner("<div></div>").children().first()
            }
            slide.$content.addClass("fancybox-content");
            slide.$slide.addClass("fancybox-slide--" + slide.contentType);
            self.afterLoad(slide)
        }, setError: function (slide) {
            slide.hasError = true;
            slide.$slide.trigger("onReset").removeClass("fancybox-slide--" + slide.contentType).addClass("fancybox-slide--error");
            slide.contentType = "html";
            this.setContent(slide, this.translate(slide, slide.opts.errorTpl));
            if (slide.pos === this.currPos) {
                this.isAnimating = false
            }
        }, showLoading: function (slide) {
            var self = this;
            slide = slide || self.current;
            if (slide && !slide.$spinner) {
                slide.$spinner = $(self.translate(self, self.opts.spinnerTpl)).appendTo(slide.$slide).hide().fadeIn("fast")
            }
        }, hideLoading: function (slide) {
            var self = this;
            slide = slide || self.current;
            if (slide && slide.$spinner) {
                slide.$spinner.stop().remove();
                delete slide.$spinner
            }
        }, afterLoad: function (slide) {
            var self = this;
            if (self.isClosing) {
                return
            }
            slide.isLoading = false;
            slide.isLoaded = true;
            self.trigger("afterLoad", slide);
            self.hideLoading(slide);
            if (slide.opts.smallBtn && (!slide.$smallBtn || !slide.$smallBtn.length)) {
                slide.$smallBtn = $(self.translate(slide, slide.opts.btnTpl.smallBtn)).appendTo(slide.$content)
            }
            if (slide.opts.protect && slide.$content && !slide.hasError) {
                slide.$content.on("contextmenu.fb", function (e) {
                    if (e.button == 2) {
                        e.preventDefault()
                    }
                    return true
                });
                if (slide.type === "image") {
                    $('<div class="fancybox-spaceball"></div>').appendTo(slide.$content)
                }
            }
            self.adjustCaption(slide);
            self.adjustLayout(slide);
            if (slide.pos === self.currPos) {
                self.updateCursor()
            }
            self.revealContent(slide)
        }, adjustCaption: function (slide) {
            var self = this, current = slide || self.current, caption = current.opts.caption,
                preventOverlap = current.opts.preventCaptionOverlap, $caption = self.$refs.caption, $clone,
                captionH = false;
            $caption.toggleClass("fancybox-caption--separate", preventOverlap);
            if (preventOverlap && caption && caption.length) {
                if (current.pos !== self.currPos) {
                    $clone = $caption.clone().appendTo($caption.parent());
                    $clone.children().eq(0).empty().html(caption);
                    captionH = $clone.outerHeight(true);
                    $clone.empty().remove()
                } else if (self.$caption) {
                    captionH = self.$caption.outerHeight(true)
                }
                current.$slide.css("padding-bottom", captionH || "")
            }
        }, adjustLayout: function (slide) {
            var self = this, current = slide || self.current, scrollHeight, marginBottom, inlinePadding, actualPadding;
            if (current.isLoaded && current.opts.disableLayoutFix !== true) {
                current.$content.css("margin-bottom", "");
                if (current.$content.outerHeight() > current.$slide.height() + .5) {
                    inlinePadding = current.$slide[0].style["padding-bottom"];
                    actualPadding = current.$slide.css("padding-bottom");
                    if (parseFloat(actualPadding) > 0) {
                        scrollHeight = current.$slide[0].scrollHeight;
                        current.$slide.css("padding-bottom", 0);
                        if (Math.abs(scrollHeight - current.$slide[0].scrollHeight) < 1) {
                            marginBottom = actualPadding
                        }
                        current.$slide.css("padding-bottom", inlinePadding)
                    }
                }
                current.$content.css("margin-bottom", marginBottom)
            }
        }, revealContent: function (slide) {
            var self = this, $slide = slide.$slide, end = false, start = false, isMoved = self.isMoved(slide),
                isRevealed = slide.isRevealed, effect, effectClassName, duration, opacity;
            slide.isRevealed = true;
            effect = slide.opts[self.firstRun ? "animationEffect" : "transitionEffect"];
            duration = slide.opts[self.firstRun ? "animationDuration" : "transitionDuration"];
            duration = parseInt(slide.forcedDuration === undefined ? duration : slide.forcedDuration, 10);
            if (isMoved || slide.pos !== self.currPos || !duration) {
                effect = false
            }
            if (effect === "zoom") {
                if (slide.pos === self.currPos && duration && slide.type === "image" && !slide.hasError && (start = self.getThumbPos(slide))) {
                    end = self.getFitPos(slide)
                } else {
                    effect = "fade"
                }
            }
            if (effect === "zoom") {
                self.isAnimating = true;
                end.scaleX = end.width / start.width;
                end.scaleY = end.height / start.height;
                opacity = slide.opts.zoomOpacity;
                if (opacity == "auto") {
                    opacity = Math.abs(slide.width / slide.height - start.width / start.height) > .1
                }
                if (opacity) {
                    start.opacity = .1;
                    end.opacity = 1
                }
                $.fancybox.setTranslate(slide.$content.removeClass("fancybox-is-hidden"), start);
                forceRedraw(slide.$content);
                $.fancybox.animate(slide.$content, end, duration, function () {
                    self.isAnimating = false;
                    self.complete()
                });
                return
            }
            self.updateSlide(slide);
            if (!effect) {
                slide.$content.removeClass("fancybox-is-hidden");
                if (!isRevealed && isMoved && slide.type === "image" && !slide.hasError) {
                    slide.$content.hide().fadeIn("fast")
                }
                if (slide.pos === self.currPos) {
                    self.complete()
                }
                return
            }
            $.fancybox.stop($slide);
            effectClassName = "fancybox-slide--" + (slide.pos >= self.prevPos ? "next" : "previous") + " fancybox-animated fancybox-fx-" + effect;
            $slide.addClass(effectClassName).removeClass("fancybox-slide--current");
            slide.$content.removeClass("fancybox-is-hidden");
            forceRedraw($slide);
            if (slide.type !== "image") {
                slide.$content.hide().show(0)
            }
            $.fancybox.animate($slide, "fancybox-slide--current", duration, function () {
                $slide.removeClass(effectClassName).css({transform: "", opacity: ""});
                if (slide.pos === self.currPos) {
                    self.complete()
                }
            }, true)
        }, getThumbPos: function (slide) {
            var rez = false, $thumb = slide.$thumb, thumbPos, btw, brw, bbw, blw;
            if (!$thumb || !inViewport($thumb[0])) {
                return false
            }
            thumbPos = $.fancybox.getTranslate($thumb);
            btw = parseFloat($thumb.css("border-top-width") || 0);
            brw = parseFloat($thumb.css("border-right-width") || 0);
            bbw = parseFloat($thumb.css("border-bottom-width") || 0);
            blw = parseFloat($thumb.css("border-left-width") || 0);
            rez = {
                top: thumbPos.top + btw,
                left: thumbPos.left + blw,
                width: thumbPos.width - brw - blw,
                height: thumbPos.height - btw - bbw,
                scaleX: 1,
                scaleY: 1
            };
            return thumbPos.width > 0 && thumbPos.height > 0 ? rez : false
        }, complete: function () {
            var self = this, current = self.current, slides = {}, $el;
            if (self.isMoved() || !current.isLoaded) {
                return
            }
            if (!current.isComplete) {
                current.isComplete = true;
                current.$slide.siblings().trigger("onReset");
                self.preload("inline");
                forceRedraw(current.$slide);
                current.$slide.addClass("fancybox-slide--complete");
                $.each(self.slides, function (key, slide) {
                    if (slide.pos >= self.currPos - 1 && slide.pos <= self.currPos + 1) {
                        slides[slide.pos] = slide
                    } else if (slide) {
                        $.fancybox.stop(slide.$slide);
                        slide.$slide.off().remove()
                    }
                });
                self.slides = slides
            }
            self.isAnimating = false;
            self.updateCursor();
            self.trigger("afterShow");
            if (!!current.opts.video.autoStart) {
                current.$slide.find("video,audio").filter(":visible:first").trigger("play").one("ended", function () {
                    if (Document.exitFullscreen) {
                        Document.exitFullscreen()
                    } else if (this.webkitExitFullscreen) {
                        this.webkitExitFullscreen()
                    }
                    self.next()
                })
            }
            if (current.opts.autoFocus && current.contentType === "html") {
                $el = current.$content.find("input[autofocus]:enabled:visible:first");
                if ($el.length) {
                    $el.trigger("focus")
                } else {
                    self.focus(null, true)
                }
            }
            current.$slide.scrollTop(0).scrollLeft(0)
        }, preload: function (type) {
            var self = this, prev, next;
            if (self.group.length < 2) {
                return
            }
            next = self.slides[self.currPos + 1];
            prev = self.slides[self.currPos - 1];
            if (prev && prev.type === type) {
                self.loadSlide(prev)
            }
            if (next && next.type === type) {
                self.loadSlide(next)
            }
        }, focus: function (e, firstRun) {
            var self = this,
                focusableStr = ["a[href]", "area[href]", 'input:not([disabled]):not([type="hidden"]):not([aria-hidden])', "select:not([disabled]):not([aria-hidden])", "textarea:not([disabled]):not([aria-hidden])", "button:not([disabled]):not([aria-hidden])", "iframe", "object", "embed", "video", "audio", "[contenteditable]", '[tabindex]:not([tabindex^="-"])'].join(","),
                focusableItems, focusedItemIndex;
            if (self.isClosing) {
                return
            }
            if (e || !self.current || !self.current.isComplete) {
                focusableItems = self.$refs.container.find("*:visible")
            } else {
                focusableItems = self.current.$slide.find("*:visible" + (firstRun ? ":not(.fancybox-close-small)" : ""))
            }
            focusableItems = focusableItems.filter(focusableStr).filter(function () {
                return $(this).css("visibility") !== "hidden" && !$(this).hasClass("disabled")
            });
            if (focusableItems.length) {
                focusedItemIndex = focusableItems.index(document.activeElement);
                if (e && e.shiftKey) {
                    if (focusedItemIndex < 0 || focusedItemIndex == 0) {
                        e.preventDefault();
                        focusableItems.eq(focusableItems.length - 1).trigger("focus")
                    }
                } else {
                    if (focusedItemIndex < 0 || focusedItemIndex == focusableItems.length - 1) {
                        if (e) {
                            e.preventDefault()
                        }
                        focusableItems.eq(0).trigger("focus")
                    }
                }
            } else {
                self.$refs.container.trigger("focus")
            }
        }, activate: function () {
            var self = this;
            $(".fancybox-container").each(function () {
                var instance = $(this).data("FancyBox");
                if (instance && instance.id !== self.id && !instance.isClosing) {
                    instance.trigger("onDeactivate");
                    instance.removeEvents();
                    instance.isVisible = false
                }
            });
            self.isVisible = true;
            if (self.current || self.isIdle) {
                self.update();
                self.updateControls()
            }
            self.trigger("onActivate");
            self.addEvents()
        }, close: function (e, d) {
            var self = this, current = self.current, effect, duration, $content, domRect, opacity, start, end;
            var done = function () {
                self.cleanUp(e)
            };
            if (self.isClosing) {
                return false
            }
            self.isClosing = true;
            if (self.trigger("beforeClose", e) === false) {
                self.isClosing = false;
                requestAFrame(function () {
                    self.update()
                });
                return false
            }
            self.removeEvents();
            $content = current.$content;
            effect = current.opts.animationEffect;
            duration = $.isNumeric(d) ? d : effect ? current.opts.animationDuration : 0;
            current.$slide.removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated");
            if (e !== true) {
                $.fancybox.stop(current.$slide)
            } else {
                effect = false
            }
            current.$slide.siblings().trigger("onReset").remove();
            if (duration) {
                self.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing").css("transition-duration", duration + "ms")
            }
            self.hideLoading(current);
            self.hideControls(true);
            self.updateCursor();
            if (effect === "zoom" && !($content && duration && current.type === "image" && !self.isMoved() && !current.hasError && (end = self.getThumbPos(current)))) {
                effect = "fade"
            }
            if (effect === "zoom") {
                $.fancybox.stop($content);
                domRect = $.fancybox.getTranslate($content);
                start = {
                    top: domRect.top,
                    left: domRect.left,
                    scaleX: domRect.width / end.width,
                    scaleY: domRect.height / end.height,
                    width: end.width,
                    height: end.height
                };
                opacity = current.opts.zoomOpacity;
                if (opacity == "auto") {
                    opacity = Math.abs(current.width / current.height - end.width / end.height) > .1
                }
                if (opacity) {
                    end.opacity = 0
                }
                $.fancybox.setTranslate($content, start);
                forceRedraw($content);
                $.fancybox.animate($content, end, duration, done);
                return true
            }
            if (effect && duration) {
                $.fancybox.animate(current.$slide.addClass("fancybox-slide--previous").removeClass("fancybox-slide--current"), "fancybox-animated fancybox-fx-" + effect, duration, done)
            } else {
                if (e === true) {
                    setTimeout(done, duration)
                } else {
                    done()
                }
            }
            return true
        }, cleanUp: function (e) {
            var self = this, instance, $focus = self.current.opts.$orig, x, y;
            self.current.$slide.trigger("onReset");
            self.$refs.container.empty().remove();
            self.trigger("afterClose", e);
            if (!!self.current.opts.backFocus) {
                if (!$focus || !$focus.length || !$focus.is(":visible")) {
                    $focus = self.$trigger
                }
                if ($focus && $focus.length) {
                    x = window.scrollX;
                    y = window.scrollY;
                    $focus.trigger("focus");
                    $("html, body").scrollTop(y).scrollLeft(x)
                }
            }
            self.current = null;
            instance = $.fancybox.getInstance();
            if (instance) {
                instance.activate()
            } else {
                $("body").removeClass("fancybox-active compensate-for-scrollbar");
                $("#fancybox-style-noscroll").remove()
            }
        }, trigger: function (name, slide) {
            var args = Array.prototype.slice.call(arguments, 1), self = this,
                obj = slide && slide.opts ? slide : self.current, rez;
            if (obj) {
                args.unshift(obj)
            } else {
                obj = self
            }
            args.unshift(self);
            if ($.isFunction(obj.opts[name])) {
                rez = obj.opts[name].apply(obj, args)
            }
            if (rez === false) {
                return rez
            }
            if (name === "afterClose" || !self.$refs) {
                $D.trigger(name + ".fb", args)
            } else {
                self.$refs.container.trigger(name + ".fb", args)
            }
        }, updateControls: function () {
            var self = this, current = self.current, index = current.index, $container = self.$refs.container,
                $caption = self.$refs.caption, caption = current.opts.caption;
            current.$slide.trigger("refresh");
            if (caption && caption.length) {
                self.$caption = $caption;
                $caption.children().eq(0).html(caption)
            } else {
                self.$caption = null
            }
            if (!self.hasHiddenControls && !self.isIdle) {
                self.showControls()
            }
            $container.find("[data-fancybox-count]").html(self.group.length);
            $container.find("[data-fancybox-index]").html(index + 1);
            $container.find("[data-fancybox-prev]").prop("disabled", !current.opts.loop && index <= 0);
            $container.find("[data-fancybox-next]").prop("disabled", !current.opts.loop && index >= self.group.length - 1);
            if (current.type === "image") {
                $container.find("[data-fancybox-zoom]").show().end().find("[data-fancybox-download]").attr("href", current.opts.image.src || current.src).show()
            } else if (current.opts.toolbar) {
                $container.find("[data-fancybox-download],[data-fancybox-zoom]").hide()
            }
            if ($(document.activeElement).is(":hidden,[disabled]")) {
                self.$refs.container.trigger("focus")
            }
        }, hideControls: function (andCaption) {
            var self = this, arr = ["infobar", "toolbar", "nav"];
            if (andCaption || !self.current.opts.preventCaptionOverlap) {
                arr.push("caption")
            }
            this.$refs.container.removeClass(arr.map(function (i) {
                return "fancybox-show-" + i
            }).join(" "));
            this.hasHiddenControls = true
        }, showControls: function () {
            var self = this, opts = self.current ? self.current.opts : self.opts, $container = self.$refs.container;
            self.hasHiddenControls = false;
            self.idleSecondsCounter = 0;
            $container.toggleClass("fancybox-show-toolbar", !!(opts.toolbar && opts.buttons)).toggleClass("fancybox-show-infobar", !!(opts.infobar && self.group.length > 1)).toggleClass("fancybox-show-caption", !!self.$caption).toggleClass("fancybox-show-nav", !!(opts.arrows && self.group.length > 1)).toggleClass("fancybox-is-modal", !!opts.modal)
        }, toggleControls: function () {
            if (this.hasHiddenControls) {
                this.showControls()
            } else {
                this.hideControls()
            }
        }
    });
    $.fancybox = {
        version: "3.5.7",
        defaults: defaults,
        getInstance: function (command) {
            var instance = $('.fancybox-container:not(".fancybox-is-closing"):last').data("FancyBox"),
                args = Array.prototype.slice.call(arguments, 1);
            if (instance instanceof FancyBox) {
                if ($.type(command) === "string") {
                    instance[command].apply(instance, args)
                } else if ($.type(command) === "function") {
                    command.apply(instance, args)
                }
                return instance
            }
            return false
        },
        open: function (items, opts, index) {
            return new FancyBox(items, opts, index)
        },
        close: function (all) {
            var instance = this.getInstance();
            if (instance) {
                instance.close();
                if (all === true) {
                    this.close(all)
                }
            }
        },
        destroy: function () {
            this.close(true);
            $D.add("body").off("click.fb-start", "**")
        },
        isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
        use3d: function () {
            var div = document.createElement("div");
            return window.getComputedStyle && window.getComputedStyle(div) && window.getComputedStyle(div).getPropertyValue("transform") && !(document.documentMode && document.documentMode < 11)
        }(),
        getTranslate: function ($el) {
            var domRect;
            if (!$el || !$el.length) {
                return false
            }
            domRect = $el[0].getBoundingClientRect();
            return {
                top: domRect.top || 0,
                left: domRect.left || 0,
                width: domRect.width,
                height: domRect.height,
                opacity: parseFloat($el.css("opacity"))
            }
        },
        setTranslate: function ($el, props) {
            var str = "", css = {};
            if (!$el || !props) {
                return
            }
            if (props.left !== undefined || props.top !== undefined) {
                str = (props.left === undefined ? $el.position().left : props.left) + "px, " + (props.top === undefined ? $el.position().top : props.top) + "px";
                if (this.use3d) {
                    str = "translate3d(" + str + ", 0px)"
                } else {
                    str = "translate(" + str + ")"
                }
            }
            if (props.scaleX !== undefined && props.scaleY !== undefined) {
                str += " scale(" + props.scaleX + ", " + props.scaleY + ")"
            } else if (props.scaleX !== undefined) {
                str += " scaleX(" + props.scaleX + ")"
            }
            if (str.length) {
                css.transform = str
            }
            if (props.opacity !== undefined) {
                css.opacity = props.opacity
            }
            if (props.width !== undefined) {
                css.width = props.width
            }
            if (props.height !== undefined) {
                css.height = props.height
            }
            return $el.css(css)
        },
        animate: function ($el, to, duration, callback, leaveAnimationName) {
            var self = this, from;
            if ($.isFunction(duration)) {
                callback = duration;
                duration = null
            }
            self.stop($el);
            from = self.getTranslate($el);
            $el.on(transitionEnd, function (e) {
                if (e && e.originalEvent && (!$el.is(e.originalEvent.target) || e.originalEvent.propertyName == "z-index")) {
                    return
                }
                self.stop($el);
                if ($.isNumeric(duration)) {
                    $el.css("transition-duration", "")
                }
                if ($.isPlainObject(to)) {
                    if (to.scaleX !== undefined && to.scaleY !== undefined) {
                        self.setTranslate($el, {
                            top: to.top,
                            left: to.left,
                            width: from.width * to.scaleX,
                            height: from.height * to.scaleY,
                            scaleX: 1,
                            scaleY: 1
                        })
                    }
                } else if (leaveAnimationName !== true) {
                    $el.removeClass(to)
                }
                if ($.isFunction(callback)) {
                    callback(e)
                }
            });
            if ($.isNumeric(duration)) {
                $el.css("transition-duration", duration + "ms")
            }
            if ($.isPlainObject(to)) {
                if (to.scaleX !== undefined && to.scaleY !== undefined) {
                    delete to.width;
                    delete to.height;
                    if ($el.parent().hasClass("fancybox-slide--image")) {
                        $el.parent().addClass("fancybox-is-scaling")
                    }
                }
                $.fancybox.setTranslate($el, to)
            } else {
                $el.addClass(to)
            }
            $el.data("timer", setTimeout(function () {
                $el.trigger(transitionEnd)
            }, duration + 33))
        },
        stop: function ($el, callCallback) {
            if ($el && $el.length) {
                clearTimeout($el.data("timer"));
                if (callCallback) {
                    $el.trigger(transitionEnd)
                }
                $el.off(transitionEnd).css("transition-duration", "");
                $el.parent().removeClass("fancybox-is-scaling")
            }
        }
    };

    function _run(e, opts) {
        var items = [], index = 0, $target, value, instance;
        if (e && e.isDefaultPrevented()) {
            return
        }
        e.preventDefault();
        opts = opts || {};
        if (e && e.data) {
            opts = mergeOpts(e.data.options, opts)
        }
        $target = opts.$target || $(e.currentTarget).trigger("blur");
        instance = $.fancybox.getInstance();
        if (instance && instance.$trigger && instance.$trigger.is($target)) {
            return
        }
        if (opts.selector) {
            items = $(opts.selector)
        } else {
            value = $target.attr("data-fancybox") || "";
            if (value) {
                items = e.data ? e.data.items : [];
                items = items.length ? items.filter('[data-fancybox="' + value + '"]') : $('[data-fancybox="' + value + '"]')
            } else {
                items = [$target]
            }
        }
        index = $(items).index($target);
        if (index < 0) {
            index = 0
        }
        instance = $.fancybox.open(items, opts, index);
        instance.$trigger = $target
    }

    $.fn.fancybox = function (options) {
        var selector;
        options = options || {};
        selector = options.selector || false;
        if (selector) {
            $("body").off("click.fb-start", selector).on("click.fb-start", selector, {options: options}, _run)
        } else {
            this.off("click.fb-start").on("click.fb-start", {items: this, options: options}, _run)
        }
        return this
    };
    $D.on("click.fb-start", "[data-fancybox]", _run);
    $D.on("click.fb-start", "[data-fancybox-trigger]", function (e) {
        $('[data-fancybox="' + $(this).attr("data-fancybox-trigger") + '"]').eq($(this).attr("data-fancybox-index") || 0).trigger("click.fb-start", {$trigger: $(this)})
    });
    (function () {
        var buttonStr = ".fancybox-button", focusStr = "fancybox-focus", $pressed = null;
        $D.on("mousedown mouseup focus blur", buttonStr, function (e) {
            switch (e.type) {
                case"mousedown":
                    $pressed = $(this);
                    break;
                case"mouseup":
                    $pressed = null;
                    break;
                case"focusin":
                    $(buttonStr).removeClass(focusStr);
                    if (!$(this).is($pressed) && !$(this).is("[disabled]")) {
                        $(this).addClass(focusStr)
                    }
                    break;
                case"focusout":
                    $(buttonStr).removeClass(focusStr);
                    break
            }
        })
    })()
})(window, document, jQuery);
(function ($) {
    "use strict";
    var defaults = {
        youtube: {
            matcher: /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,
            params: {autoplay: 1, autohide: 1, fs: 1, rel: 0, hd: 1, wmode: "transparent", enablejsapi: 1, html5: 1},
            paramPlace: 8,
            type: "iframe",
            url: "https://www.youtube-nocookie.com/embed/$4",
            thumb: "https://img.youtube.com/vi/$4/hqdefault.jpg"
        },
        vimeo: {
            matcher: /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,
            params: {autoplay: 1, hd: 1, show_title: 1, show_byline: 1, show_portrait: 0, fullscreen: 1},
            paramPlace: 3,
            type: "iframe",
            url: "//player.vimeo.com/video/$2"
        },
        instagram: {
            matcher: /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
            type: "image",
            url: "//$1/p/$2/media/?size=l"
        },
        gmap_place: {
            matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,
            type: "iframe",
            url: function (rez) {
                return "//maps.google." + rez[2] + "/?ll=" + (rez[9] ? rez[9] + "&z=" + Math.floor(rez[10]) + (rez[12] ? rez[12].replace(/^\//, "&") : "") : rez[12] + "").replace(/\?/, "&") + "&output=" + (rez[12] && rez[12].indexOf("layer=c") > 0 ? "svembed" : "embed")
            }
        },
        gmap_search: {
            matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(maps\/search\/)(.*)/i,
            type: "iframe",
            url: function (rez) {
                return "//maps.google." + rez[2] + "/maps?q=" + rez[5].replace("query=", "q=").replace("api=1", "") + "&output=embed"
            }
        }
    };
    var format = function (url, rez, params) {
        if (!url) {
            return
        }
        params = params || "";
        if ($.type(params) === "object") {
            params = $.param(params, true)
        }
        $.each(rez, function (key, value) {
            url = url.replace("$" + key, value || "")
        });
        if (params.length) {
            url += (url.indexOf("?") > 0 ? "&" : "?") + params
        }
        return url
    };
    $(document).on("objectNeedsType.fb", function (e, instance, item) {
        var url = item.src || "", type = false, media, thumb, rez, params, urlParams, paramObj, provider;
        media = $.extend(true, {}, defaults, item.opts.media);
        $.each(media, function (providerName, providerOpts) {
            rez = url.match(providerOpts.matcher);
            if (!rez) {
                return
            }
            type = providerOpts.type;
            provider = providerName;
            paramObj = {};
            if (providerOpts.paramPlace && rez[providerOpts.paramPlace]) {
                urlParams = rez[providerOpts.paramPlace];
                if (urlParams[0] == "?") {
                    urlParams = urlParams.substring(1)
                }
                urlParams = urlParams.split("&");
                for (var m = 0; m < urlParams.length; ++m) {
                    var p = urlParams[m].split("=", 2);
                    if (p.length == 2) {
                        paramObj[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "))
                    }
                }
            }
            params = $.extend(true, {}, providerOpts.params, item.opts[providerName], paramObj);
            url = $.type(providerOpts.url) === "function" ? providerOpts.url.call(this, rez, params, item) : format(providerOpts.url, rez, params);
            thumb = $.type(providerOpts.thumb) === "function" ? providerOpts.thumb.call(this, rez, params, item) : format(providerOpts.thumb, rez);
            if (providerName === "youtube") {
                url = url.replace(/&t=((\d+)m)?(\d+)s/, function (match, p1, m, s) {
                    return "&start=" + ((m ? parseInt(m, 10) * 60 : 0) + parseInt(s, 10))
                })
            } else if (providerName === "vimeo") {
                url = url.replace("&%23", "#")
            }
            return false
        });
        if (type) {
            if (!item.opts.thumb && !(item.opts.$thumb && item.opts.$thumb.length)) {
                item.opts.thumb = thumb
            }
            if (type === "iframe") {
                item.opts = $.extend(true, item.opts, {iframe: {preload: false, attr: {scrolling: "no"}}})
            }
            $.extend(item, {
                type: type,
                src: url,
                origSrc: item.src,
                contentSource: provider,
                contentType: type === "image" ? "image" : provider == "gmap_place" || provider == "gmap_search" ? "map" : "video"
            })
        } else if (url) {
            item.type = item.opts.defaultType
        }
    });
    var VideoAPILoader = {
        youtube: {src: "https://www.youtube.com/iframe_api", class: "YT", loading: false, loaded: false},
        vimeo: {src: "https://player.vimeo.com/api/player.js", class: "Vimeo", loading: false, loaded: false},
        load: function (vendor) {
            var _this = this, script;
            if (this[vendor].loaded) {
                setTimeout(function () {
                    _this.done(vendor)
                });
                return
            }
            if (this[vendor].loading) {
                return
            }
            this[vendor].loading = true;
            script = document.createElement("script");
            script.type = "text/javascript";
            script.src = this[vendor].src;
            if (vendor === "youtube") {
                window.onYouTubeIframeAPIReady = function () {
                    _this[vendor].loaded = true;
                    _this.done(vendor)
                }
            } else {
                script.onload = function () {
                    _this[vendor].loaded = true;
                    _this.done(vendor)
                }
            }
            document.body.appendChild(script)
        },
        done: function (vendor) {
            var instance, $el, player;
            if (vendor === "youtube") {
                delete window.onYouTubeIframeAPIReady
            }
            instance = $.fancybox.getInstance();
            if (instance) {
                $el = instance.current.$content.find("iframe");
                if (vendor === "youtube" && YT !== undefined && YT) {
                    player = new YT.Player($el.attr("id"), {
                        events: {
                            onStateChange: function (e) {
                                if (e.data == 0) {
                                    instance.next()
                                }
                            }
                        }
                    })
                } else if (vendor === "vimeo" && Vimeo !== undefined && Vimeo) {
                    player = new Vimeo.Player($el);
                    player.on("ended", function () {
                        instance.next()
                    })
                }
            }
        }
    };
    $(document).on({
        "afterShow.fb": function (e, instance, current) {
            if (instance.group.length > 1 && (current.contentSource === "youtube" || current.contentSource === "vimeo")) {
                VideoAPILoader.load(current.contentSource)
            }
        }
    })
})(jQuery);
(function (window, document, $) {
    "use strict";
    var requestAFrame = function () {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || function (callback) {
            return window.setTimeout(callback, 1e3 / 60)
        }
    }();
    var cancelAFrame = function () {
        return window.cancelAnimationFrame || window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || window.oCancelAnimationFrame || function (id) {
            window.clearTimeout(id)
        }
    }();
    var getPointerXY = function (e) {
        var result = [];
        e = e.originalEvent || e || window.e;
        e = e.touches && e.touches.length ? e.touches : e.changedTouches && e.changedTouches.length ? e.changedTouches : [e];
        for (var key in e) {
            if (e[key].pageX) {
                result.push({x: e[key].pageX, y: e[key].pageY})
            } else if (e[key].clientX) {
                result.push({x: e[key].clientX, y: e[key].clientY})
            }
        }
        return result
    };
    var distance = function (point2, point1, what) {
        if (!point1 || !point2) {
            return 0
        }
        if (what === "x") {
            return point2.x - point1.x
        } else if (what === "y") {
            return point2.y - point1.y
        }
        return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2))
    };
    var isClickable = function ($el) {
        if ($el.is('a,area,button,[role="button"],input,label,select,summary,textarea,video,audio,iframe') || $.isFunction($el.get(0).onclick) || $el.data("selectable")) {
            return true
        }
        for (var i = 0, atts = $el[0].attributes, n = atts.length; i < n; i++) {
            if (atts[i].nodeName.substr(0, 14) === "data-fancybox-") {
                return true
            }
        }
        return false
    };
    var hasScrollbars = function (el) {
        var overflowY = window.getComputedStyle(el)["overflow-y"],
            overflowX = window.getComputedStyle(el)["overflow-x"],
            vertical = (overflowY === "scroll" || overflowY === "auto") && el.scrollHeight > el.clientHeight,
            horizontal = (overflowX === "scroll" || overflowX === "auto") && el.scrollWidth > el.clientWidth;
        return vertical || horizontal
    };
    var isScrollable = function ($el) {
        var rez = false;
        while (true) {
            rez = hasScrollbars($el.get(0));
            if (rez) {
                break
            }
            $el = $el.parent();
            if (!$el.length || $el.hasClass("fancybox-stage") || $el.is("body")) {
                break
            }
        }
        return rez
    };
    var Guestures = function (instance) {
        var self = this;
        self.instance = instance;
        self.$bg = instance.$refs.bg;
        self.$stage = instance.$refs.stage;
        self.$container = instance.$refs.container;
        self.destroy();
        self.$container.on("touchstart.fb.touch mousedown.fb.touch", $.proxy(self, "ontouchstart"))
    };
    Guestures.prototype.destroy = function () {
        var self = this;
        self.$container.off(".fb.touch");
        $(document).off(".fb.touch");
        if (self.requestId) {
            cancelAFrame(self.requestId);
            self.requestId = null
        }
        if (self.tapped) {
            clearTimeout(self.tapped);
            self.tapped = null
        }
    };
    Guestures.prototype.ontouchstart = function (e) {
        var self = this, $target = $(e.target), instance = self.instance, current = instance.current,
            $slide = current.$slide, $content = current.$content, isTouchDevice = e.type == "touchstart";
        if (isTouchDevice) {
            self.$container.off("mousedown.fb.touch")
        }
        if (e.originalEvent && e.originalEvent.button == 2) {
            return
        }
        if (!$slide.length || !$target.length || isClickable($target) || isClickable($target.parent())) {
            return
        }
        if (!$target.is("img") && e.originalEvent.clientX > $target[0].clientWidth + $target.offset().left) {
            return
        }
        if (!current || instance.isAnimating || current.$slide.hasClass("fancybox-animated")) {
            e.stopPropagation();
            e.preventDefault();
            return
        }
        self.realPoints = self.startPoints = getPointerXY(e);
        if (!self.startPoints.length) {
            return
        }
        if (current.touch) {
            e.stopPropagation()
        }
        self.startEvent = e;
        self.canTap = true;
        self.$target = $target;
        self.$content = $content;
        self.opts = current.opts.touch;
        self.isPanning = false;
        self.isSwiping = false;
        self.isZooming = false;
        self.isScrolling = false;
        self.canPan = instance.canPan();
        self.startTime = (new Date).getTime();
        self.distanceX = self.distanceY = self.distance = 0;
        self.canvasWidth = Math.round($slide[0].clientWidth);
        self.canvasHeight = Math.round($slide[0].clientHeight);
        self.contentLastPos = null;
        self.contentStartPos = $.fancybox.getTranslate(self.$content) || {top: 0, left: 0};
        self.sliderStartPos = $.fancybox.getTranslate($slide);
        self.stagePos = $.fancybox.getTranslate(instance.$refs.stage);
        self.sliderStartPos.top -= self.stagePos.top;
        self.sliderStartPos.left -= self.stagePos.left;
        self.contentStartPos.top -= self.stagePos.top;
        self.contentStartPos.left -= self.stagePos.left;
        $(document).off(".fb.touch").on(isTouchDevice ? "touchend.fb.touch touchcancel.fb.touch" : "mouseup.fb.touch mouseleave.fb.touch", $.proxy(self, "ontouchend")).on(isTouchDevice ? "touchmove.fb.touch" : "mousemove.fb.touch", $.proxy(self, "ontouchmove"));
        if ($.fancybox.isMobile) {
            document.addEventListener("scroll", self.onscroll, true)
        }
        if (!(self.opts || self.canPan) || !($target.is(self.$stage) || self.$stage.find($target).length)) {
            if ($target.is(".fancybox-image")) {
                e.preventDefault()
            }
            if (!($.fancybox.isMobile && $target.parents(".fancybox-caption").length)) {
                return
            }
        }
        self.isScrollable = isScrollable($target) || isScrollable($target.parent());
        if (!($.fancybox.isMobile && self.isScrollable)) {
            e.preventDefault()
        }
        if (self.startPoints.length === 1 || current.hasError) {
            if (self.canPan) {
                $.fancybox.stop(self.$content);
                self.isPanning = true
            } else {
                self.isSwiping = true
            }
            self.$container.addClass("fancybox-is-grabbing")
        }
        if (self.startPoints.length === 2 && current.type === "image" && (current.isLoaded || current.$ghost)) {
            self.canTap = false;
            self.isSwiping = false;
            self.isPanning = false;
            self.isZooming = true;
            $.fancybox.stop(self.$content);
            self.centerPointStartX = (self.startPoints[0].x + self.startPoints[1].x) * .5 - $(window).scrollLeft();
            self.centerPointStartY = (self.startPoints[0].y + self.startPoints[1].y) * .5 - $(window).scrollTop();
            self.percentageOfImageAtPinchPointX = (self.centerPointStartX - self.contentStartPos.left) / self.contentStartPos.width;
            self.percentageOfImageAtPinchPointY = (self.centerPointStartY - self.contentStartPos.top) / self.contentStartPos.height;
            self.startDistanceBetweenFingers = distance(self.startPoints[0], self.startPoints[1])
        }
    };
    Guestures.prototype.onscroll = function (e) {
        var self = this;
        self.isScrolling = true;
        document.removeEventListener("scroll", self.onscroll, true)
    };
    Guestures.prototype.ontouchmove = function (e) {
        var self = this;
        if (e.originalEvent.buttons !== undefined && e.originalEvent.buttons === 0) {
            self.ontouchend(e);
            return
        }
        if (self.isScrolling) {
            self.canTap = false;
            return
        }
        self.newPoints = getPointerXY(e);
        if (!(self.opts || self.canPan) || !self.newPoints.length || !self.newPoints.length) {
            return
        }
        if (!(self.isSwiping && self.isSwiping === true)) {
            e.preventDefault()
        }
        self.distanceX = distance(self.newPoints[0], self.startPoints[0], "x");
        self.distanceY = distance(self.newPoints[0], self.startPoints[0], "y");
        self.distance = distance(self.newPoints[0], self.startPoints[0]);
        if (self.distance > 0) {
            if (self.isSwiping) {
                self.onSwipe(e)
            } else if (self.isPanning) {
                self.onPan()
            } else if (self.isZooming) {
                self.onZoom()
            }
        }
    };
    Guestures.prototype.onSwipe = function (e) {
        var self = this, instance = self.instance, swiping = self.isSwiping, left = self.sliderStartPos.left || 0,
            angle;
        if (swiping === true) {
            if (Math.abs(self.distance) > 10) {
                self.canTap = false;
                if (instance.group.length < 2 && self.opts.vertical) {
                    self.isSwiping = "y"
                } else if (instance.isDragging || self.opts.vertical === false || self.opts.vertical === "auto" && $(window).width() > 800) {
                    self.isSwiping = "x"
                } else {
                    angle = Math.abs(Math.atan2(self.distanceY, self.distanceX) * 180 / Math.PI);
                    self.isSwiping = angle > 45 && angle < 135 ? "y" : "x"
                }
                if (self.isSwiping === "y" && $.fancybox.isMobile && self.isScrollable) {
                    self.isScrolling = true;
                    return
                }
                instance.isDragging = self.isSwiping;
                self.startPoints = self.newPoints;
                $.each(instance.slides, function (index, slide) {
                    var slidePos, stagePos;
                    $.fancybox.stop(slide.$slide);
                    slidePos = $.fancybox.getTranslate(slide.$slide);
                    stagePos = $.fancybox.getTranslate(instance.$refs.stage);
                    slide.$slide.css({
                        transform: "",
                        opacity: "",
                        "transition-duration": ""
                    }).removeClass("fancybox-animated").removeClass(function (index, className) {
                        return (className.match(/(^|\s)fancybox-fx-\S+/g) || []).join(" ")
                    });
                    if (slide.pos === instance.current.pos) {
                        self.sliderStartPos.top = slidePos.top - stagePos.top;
                        self.sliderStartPos.left = slidePos.left - stagePos.left
                    }
                    $.fancybox.setTranslate(slide.$slide, {
                        top: slidePos.top - stagePos.top,
                        left: slidePos.left - stagePos.left
                    })
                });
                if (instance.SlideShow && instance.SlideShow.isActive) {
                    instance.SlideShow.stop()
                }
            }
            return
        }
        if (swiping == "x") {
            if (self.distanceX > 0 && (self.instance.group.length < 2 || self.instance.current.index === 0 && !self.instance.current.opts.loop)) {
                left = left + Math.pow(self.distanceX, .8)
            } else if (self.distanceX < 0 && (self.instance.group.length < 2 || self.instance.current.index === self.instance.group.length - 1 && !self.instance.current.opts.loop)) {
                left = left - Math.pow(-self.distanceX, .8)
            } else {
                left = left + self.distanceX
            }
        }
        self.sliderLastPos = {top: swiping == "x" ? 0 : self.sliderStartPos.top + self.distanceY, left: left};
        if (self.requestId) {
            cancelAFrame(self.requestId);
            self.requestId = null
        }
        self.requestId = requestAFrame(function () {
            if (self.sliderLastPos) {
                $.each(self.instance.slides, function (index, slide) {
                    var pos = slide.pos - self.instance.currPos;
                    $.fancybox.setTranslate(slide.$slide, {
                        top: self.sliderLastPos.top,
                        left: self.sliderLastPos.left + pos * self.canvasWidth + pos * slide.opts.gutter
                    })
                });
                self.$container.addClass("fancybox-is-sliding")
            }
        })
    };
    Guestures.prototype.onPan = function () {
        var self = this;
        if (distance(self.newPoints[0], self.realPoints[0]) < ($.fancybox.isMobile ? 10 : 5)) {
            self.startPoints = self.newPoints;
            return
        }
        self.canTap = false;
        self.contentLastPos = self.limitMovement();
        if (self.requestId) {
            cancelAFrame(self.requestId)
        }
        self.requestId = requestAFrame(function () {
            $.fancybox.setTranslate(self.$content, self.contentLastPos)
        })
    };
    Guestures.prototype.limitMovement = function () {
        var self = this;
        var canvasWidth = self.canvasWidth;
        var canvasHeight = self.canvasHeight;
        var distanceX = self.distanceX;
        var distanceY = self.distanceY;
        var contentStartPos = self.contentStartPos;
        var currentOffsetX = contentStartPos.left;
        var currentOffsetY = contentStartPos.top;
        var currentWidth = contentStartPos.width;
        var currentHeight = contentStartPos.height;
        var minTranslateX, minTranslateY, maxTranslateX, maxTranslateY, newOffsetX, newOffsetY;
        if (currentWidth > canvasWidth) {
            newOffsetX = currentOffsetX + distanceX
        } else {
            newOffsetX = currentOffsetX
        }
        newOffsetY = currentOffsetY + distanceY;
        minTranslateX = Math.max(0, canvasWidth * .5 - currentWidth * .5);
        minTranslateY = Math.max(0, canvasHeight * .5 - currentHeight * .5);
        maxTranslateX = Math.min(canvasWidth - currentWidth, canvasWidth * .5 - currentWidth * .5);
        maxTranslateY = Math.min(canvasHeight - currentHeight, canvasHeight * .5 - currentHeight * .5);
        if (distanceX > 0 && newOffsetX > minTranslateX) {
            newOffsetX = minTranslateX - 1 + Math.pow(-minTranslateX + currentOffsetX + distanceX, .8) || 0
        }
        if (distanceX < 0 && newOffsetX < maxTranslateX) {
            newOffsetX = maxTranslateX + 1 - Math.pow(maxTranslateX - currentOffsetX - distanceX, .8) || 0
        }
        if (distanceY > 0 && newOffsetY > minTranslateY) {
            newOffsetY = minTranslateY - 1 + Math.pow(-minTranslateY + currentOffsetY + distanceY, .8) || 0
        }
        if (distanceY < 0 && newOffsetY < maxTranslateY) {
            newOffsetY = maxTranslateY + 1 - Math.pow(maxTranslateY - currentOffsetY - distanceY, .8) || 0
        }
        return {top: newOffsetY, left: newOffsetX}
    };
    Guestures.prototype.limitPosition = function (newOffsetX, newOffsetY, newWidth, newHeight) {
        var self = this;
        var canvasWidth = self.canvasWidth;
        var canvasHeight = self.canvasHeight;
        if (newWidth > canvasWidth) {
            newOffsetX = newOffsetX > 0 ? 0 : newOffsetX;
            newOffsetX = newOffsetX < canvasWidth - newWidth ? canvasWidth - newWidth : newOffsetX
        } else {
            newOffsetX = Math.max(0, canvasWidth / 2 - newWidth / 2)
        }
        if (newHeight > canvasHeight) {
            newOffsetY = newOffsetY > 0 ? 0 : newOffsetY;
            newOffsetY = newOffsetY < canvasHeight - newHeight ? canvasHeight - newHeight : newOffsetY
        } else {
            newOffsetY = Math.max(0, canvasHeight / 2 - newHeight / 2)
        }
        return {top: newOffsetY, left: newOffsetX}
    };
    Guestures.prototype.onZoom = function () {
        var self = this;
        var contentStartPos = self.contentStartPos;
        var currentWidth = contentStartPos.width;
        var currentHeight = contentStartPos.height;
        var currentOffsetX = contentStartPos.left;
        var currentOffsetY = contentStartPos.top;
        var endDistanceBetweenFingers = distance(self.newPoints[0], self.newPoints[1]);
        var pinchRatio = endDistanceBetweenFingers / self.startDistanceBetweenFingers;
        var newWidth = Math.floor(currentWidth * pinchRatio);
        var newHeight = Math.floor(currentHeight * pinchRatio);
        var translateFromZoomingX = (currentWidth - newWidth) * self.percentageOfImageAtPinchPointX;
        var translateFromZoomingY = (currentHeight - newHeight) * self.percentageOfImageAtPinchPointY;
        var centerPointEndX = (self.newPoints[0].x + self.newPoints[1].x) / 2 - $(window).scrollLeft();
        var centerPointEndY = (self.newPoints[0].y + self.newPoints[1].y) / 2 - $(window).scrollTop();
        var translateFromTranslatingX = centerPointEndX - self.centerPointStartX;
        var translateFromTranslatingY = centerPointEndY - self.centerPointStartY;
        var newOffsetX = currentOffsetX + (translateFromZoomingX + translateFromTranslatingX);
        var newOffsetY = currentOffsetY + (translateFromZoomingY + translateFromTranslatingY);
        var newPos = {top: newOffsetY, left: newOffsetX, scaleX: pinchRatio, scaleY: pinchRatio};
        self.canTap = false;
        self.newWidth = newWidth;
        self.newHeight = newHeight;
        self.contentLastPos = newPos;
        if (self.requestId) {
            cancelAFrame(self.requestId)
        }
        self.requestId = requestAFrame(function () {
            $.fancybox.setTranslate(self.$content, self.contentLastPos)
        })
    };
    Guestures.prototype.ontouchend = function (e) {
        var self = this;
        var swiping = self.isSwiping;
        var panning = self.isPanning;
        var zooming = self.isZooming;
        var scrolling = self.isScrolling;
        self.endPoints = getPointerXY(e);
        self.dMs = Math.max((new Date).getTime() - self.startTime, 1);
        self.$container.removeClass("fancybox-is-grabbing");
        $(document).off(".fb.touch");
        document.removeEventListener("scroll", self.onscroll, true);
        if (self.requestId) {
            cancelAFrame(self.requestId);
            self.requestId = null
        }
        self.isSwiping = false;
        self.isPanning = false;
        self.isZooming = false;
        self.isScrolling = false;
        self.instance.isDragging = false;
        if (self.canTap) {
            return self.onTap(e)
        }
        self.speed = 100;
        self.velocityX = self.distanceX / self.dMs * .5;
        self.velocityY = self.distanceY / self.dMs * .5;
        if (panning) {
            self.endPanning()
        } else if (zooming) {
            self.endZooming()
        } else {
            self.endSwiping(swiping, scrolling)
        }
        return
    };
    Guestures.prototype.endSwiping = function (swiping, scrolling) {
        var self = this, ret = false, len = self.instance.group.length, distanceX = Math.abs(self.distanceX),
            canAdvance = swiping == "x" && len > 1 && (self.dMs > 130 && distanceX > 10 || distanceX > 50),
            speedX = 300;
        self.sliderLastPos = null;
        if (swiping == "y" && !scrolling && Math.abs(self.distanceY) > 50) {
            $.fancybox.animate(self.instance.current.$slide, {
                top: self.sliderStartPos.top + self.distanceY + self.velocityY * 150,
                opacity: 0
            }, 200);
            ret = self.instance.close(true, 250)
        } else if (canAdvance && self.distanceX > 0) {
            ret = self.instance.previous(speedX)
        } else if (canAdvance && self.distanceX < 0) {
            ret = self.instance.next(speedX)
        }
        if (ret === false && (swiping == "x" || swiping == "y")) {
            self.instance.centerSlide(200)
        }
        self.$container.removeClass("fancybox-is-sliding")
    };
    Guestures.prototype.endPanning = function () {
        var self = this, newOffsetX, newOffsetY, newPos;
        if (!self.contentLastPos) {
            return
        }
        if (self.opts.momentum === false || self.dMs > 350) {
            newOffsetX = self.contentLastPos.left;
            newOffsetY = self.contentLastPos.top
        } else {
            newOffsetX = self.contentLastPos.left + self.velocityX * 500;
            newOffsetY = self.contentLastPos.top + self.velocityY * 500
        }
        newPos = self.limitPosition(newOffsetX, newOffsetY, self.contentStartPos.width, self.contentStartPos.height);
        newPos.width = self.contentStartPos.width;
        newPos.height = self.contentStartPos.height;
        $.fancybox.animate(self.$content, newPos, 366)
    };
    Guestures.prototype.endZooming = function () {
        var self = this;
        var current = self.instance.current;
        var newOffsetX, newOffsetY, newPos, reset;
        var newWidth = self.newWidth;
        var newHeight = self.newHeight;
        if (!self.contentLastPos) {
            return
        }
        newOffsetX = self.contentLastPos.left;
        newOffsetY = self.contentLastPos.top;
        reset = {top: newOffsetY, left: newOffsetX, width: newWidth, height: newHeight, scaleX: 1, scaleY: 1};
        $.fancybox.setTranslate(self.$content, reset);
        if (newWidth < self.canvasWidth && newHeight < self.canvasHeight) {
            self.instance.scaleToFit(150)
        } else if (newWidth > current.width || newHeight > current.height) {
            self.instance.scaleToActual(self.centerPointStartX, self.centerPointStartY, 150)
        } else {
            newPos = self.limitPosition(newOffsetX, newOffsetY, newWidth, newHeight);
            $.fancybox.animate(self.$content, newPos, 150)
        }
    };
    Guestures.prototype.onTap = function (e) {
        var self = this;
        var $target = $(e.target);
        var instance = self.instance;
        var current = instance.current;
        var endPoints = e && getPointerXY(e) || self.startPoints;
        var tapX = endPoints[0] ? endPoints[0].x - $(window).scrollLeft() - self.stagePos.left : 0;
        var tapY = endPoints[0] ? endPoints[0].y - $(window).scrollTop() - self.stagePos.top : 0;
        var where;
        var process = function (prefix) {
            var action = current.opts[prefix];
            if ($.isFunction(action)) {
                action = action.apply(instance, [current, e])
            }
            if (!action) {
                return
            }
            switch (action) {
                case"close":
                    instance.close(self.startEvent);
                    break;
                case"toggleControls":
                    instance.toggleControls();
                    break;
                case"next":
                    instance.next();
                    break;
                case"nextOrClose":
                    if (instance.group.length > 1) {
                        instance.next()
                    } else {
                        instance.close(self.startEvent)
                    }
                    break;
                case"zoom":
                    if (current.type == "image" && (current.isLoaded || current.$ghost)) {
                        if (instance.canPan()) {
                            instance.scaleToFit()
                        } else if (instance.isScaledDown()) {
                            instance.scaleToActual(tapX, tapY)
                        } else if (instance.group.length < 2) {
                            instance.close(self.startEvent)
                        }
                    }
                    break
            }
        };
        if (e.originalEvent && e.originalEvent.button == 2) {
            return
        }
        if (!$target.is("img") && tapX > $target[0].clientWidth + $target.offset().left) {
            return
        }
        if ($target.is(".fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container")) {
            where = "Outside"
        } else if ($target.is(".fancybox-slide")) {
            where = "Slide"
        } else if (instance.current.$content && instance.current.$content.find($target).addBack().filter($target).length) {
            where = "Content"
        } else {
            return
        }
        if (self.tapped) {
            clearTimeout(self.tapped);
            self.tapped = null;
            if (Math.abs(tapX - self.tapX) > 50 || Math.abs(tapY - self.tapY) > 50) {
                return this
            }
            process("dblclick" + where)
        } else {
            self.tapX = tapX;
            self.tapY = tapY;
            if (current.opts["dblclick" + where] && current.opts["dblclick" + where] !== current.opts["click" + where]) {
                self.tapped = setTimeout(function () {
                    self.tapped = null;
                    if (!instance.isAnimating) {
                        process("click" + where)
                    }
                }, 500)
            } else {
                process("click" + where)
            }
        }
        return this
    };
    $(document).on("onActivate.fb", function (e, instance) {
        if (instance && !instance.Guestures) {
            instance.Guestures = new Guestures(instance)
        }
    }).on("beforeClose.fb", function (e, instance) {
        if (instance && instance.Guestures) {
            instance.Guestures.destroy()
        }
    })
})(window, document, jQuery);
(function (document, $) {
    "use strict";
    $.extend(true, $.fancybox.defaults, {
        btnTpl: {slideShow: '<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}">' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.5 5.4v13.2l11-6.6z"/></svg>' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M8.33 5.75h2.2v12.5h-2.2V5.75zm5.15 0h2.2v12.5h-2.2V5.75z"/></svg>' + "</button>"},
        slideShow: {autoStart: false, speed: 3e3, progress: true}
    });
    var SlideShow = function (instance) {
        this.instance = instance;
        this.init()
    };
    $.extend(SlideShow.prototype, {
        timer: null, isActive: false, $button: null, init: function () {
            var self = this, instance = self.instance, opts = instance.group[instance.currIndex].opts.slideShow;
            self.$button = instance.$refs.toolbar.find("[data-fancybox-play]").on("click", function () {
                self.toggle()
            });
            if (instance.group.length < 2 || !opts) {
                self.$button.hide()
            } else if (opts.progress) {
                self.$progress = $('<div class="fancybox-progress"></div>').appendTo(instance.$refs.inner)
            }
        }, set: function (force) {
            var self = this, instance = self.instance, current = instance.current;
            if (current && (force === true || current.opts.loop || instance.currIndex < instance.group.length - 1)) {
                if (self.isActive && current.contentType !== "video") {
                    if (self.$progress) {
                        $.fancybox.animate(self.$progress.show(), {scaleX: 1}, current.opts.slideShow.speed)
                    }
                    self.timer = setTimeout(function () {
                        if (!instance.current.opts.loop && instance.current.index == instance.group.length - 1) {
                            instance.jumpTo(0)
                        } else {
                            instance.next()
                        }
                    }, current.opts.slideShow.speed)
                }
            } else {
                self.stop();
                instance.idleSecondsCounter = 0;
                instance.showControls()
            }
        }, clear: function () {
            var self = this;
            clearTimeout(self.timer);
            self.timer = null;
            if (self.$progress) {
                self.$progress.removeAttr("style").hide()
            }
        }, start: function () {
            var self = this, current = self.instance.current;
            if (current) {
                self.$button.attr("title", (current.opts.i18n[current.opts.lang] || current.opts.i18n.en).PLAY_STOP).removeClass("fancybox-button--play").addClass("fancybox-button--pause");
                self.isActive = true;
                if (current.isComplete) {
                    self.set(true)
                }
                self.instance.trigger("onSlideShowChange", true)
            }
        }, stop: function () {
            var self = this, current = self.instance.current;
            self.clear();
            self.$button.attr("title", (current.opts.i18n[current.opts.lang] || current.opts.i18n.en).PLAY_START).removeClass("fancybox-button--pause").addClass("fancybox-button--play");
            self.isActive = false;
            self.instance.trigger("onSlideShowChange", false);
            if (self.$progress) {
                self.$progress.removeAttr("style").hide()
            }
        }, toggle: function () {
            var self = this;
            if (self.isActive) {
                self.stop()
            } else {
                self.start()
            }
        }
    });
    $(document).on({
        "onInit.fb": function (e, instance) {
            if (instance && !instance.SlideShow) {
                instance.SlideShow = new SlideShow(instance)
            }
        }, "beforeShow.fb": function (e, instance, current, firstRun) {
            var SlideShow = instance && instance.SlideShow;
            if (firstRun) {
                if (SlideShow && current.opts.slideShow.autoStart) {
                    SlideShow.start()
                }
            } else if (SlideShow && SlideShow.isActive) {
                SlideShow.clear()
            }
        }, "afterShow.fb": function (e, instance, current) {
            var SlideShow = instance && instance.SlideShow;
            if (SlideShow && SlideShow.isActive) {
                SlideShow.set()
            }
        }, "afterKeydown.fb": function (e, instance, current, keypress, keycode) {
            var SlideShow = instance && instance.SlideShow;
            if (SlideShow && current.opts.slideShow && (keycode === 80 || keycode === 32) && !$(document.activeElement).is("button,a,input")) {
                keypress.preventDefault();
                SlideShow.toggle()
            }
        }, "beforeClose.fb onDeactivate.fb": function (e, instance) {
            var SlideShow = instance && instance.SlideShow;
            if (SlideShow) {
                SlideShow.stop()
            }
        }
    });
    $(document).on("visibilitychange", function () {
        var instance = $.fancybox.getInstance(), SlideShow = instance && instance.SlideShow;
        if (SlideShow && SlideShow.isActive) {
            if (document.hidden) {
                SlideShow.clear()
            } else {
                SlideShow.set()
            }
        }
    })
})(document, jQuery);
(function (document, $) {
    "use strict";
    var fn = function () {
        var fnMap = [["requestFullscreen", "exitFullscreen", "fullscreenElement", "fullscreenEnabled", "fullscreenchange", "fullscreenerror"], ["webkitRequestFullscreen", "webkitExitFullscreen", "webkitFullscreenElement", "webkitFullscreenEnabled", "webkitfullscreenchange", "webkitfullscreenerror"], ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitCurrentFullScreenElement", "webkitCancelFullScreen", "webkitfullscreenchange", "webkitfullscreenerror"], ["mozRequestFullScreen", "mozCancelFullScreen", "mozFullScreenElement", "mozFullScreenEnabled", "mozfullscreenchange", "mozfullscreenerror"], ["msRequestFullscreen", "msExitFullscreen", "msFullscreenElement", "msFullscreenEnabled", "MSFullscreenChange", "MSFullscreenError"]];
        var ret = {};
        for (var i = 0; i < fnMap.length; i++) {
            var val = fnMap[i];
            if (val && val[1] in document) {
                for (var j = 0; j < val.length; j++) {
                    ret[fnMap[0][j]] = val[j]
                }
                return ret
            }
        }
        return false
    }();
    if (fn) {
        var FullScreen = {
            request: function (elem) {
                elem = elem || document.documentElement;
                elem[fn.requestFullscreen](elem.ALLOW_KEYBOARD_INPUT)
            }, exit: function () {
                document[fn.exitFullscreen]()
            }, toggle: function (elem) {
                elem = elem || document.documentElement;
                if (this.isFullscreen()) {
                    this.exit()
                } else {
                    this.request(elem)
                }
            }, isFullscreen: function () {
                return Boolean(document[fn.fullscreenElement])
            }, enabled: function () {
                return Boolean(document[fn.fullscreenEnabled])
            }
        };
        $.extend(true, $.fancybox.defaults, {
            btnTpl: {fullScreen: '<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fsenter" title="{{FULL_SCREEN}}">' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z"/></svg>' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 16h3v3h2v-5H5zm3-8H5v2h5V5H8zm6 11h2v-3h3v-2h-5zm2-11V5h-2v5h5V8z"/></svg>' + "</button>"},
            fullScreen: {autoStart: false}
        });
        $(document).on(fn.fullscreenchange, function () {
            var isFullscreen = FullScreen.isFullscreen(), instance = $.fancybox.getInstance();
            if (instance) {
                if (instance.current && instance.current.type === "image" && instance.isAnimating) {
                    instance.isAnimating = false;
                    instance.update(true, true, 0);
                    if (!instance.isComplete) {
                        instance.complete()
                    }
                }
                instance.trigger("onFullscreenChange", isFullscreen);
                instance.$refs.container.toggleClass("fancybox-is-fullscreen", isFullscreen);
                instance.$refs.toolbar.find("[data-fancybox-fullscreen]").toggleClass("fancybox-button--fsenter", !isFullscreen).toggleClass("fancybox-button--fsexit", isFullscreen)
            }
        })
    }
    $(document).on({
        "onInit.fb": function (e, instance) {
            var $container;
            if (!fn) {
                instance.$refs.toolbar.find("[data-fancybox-fullscreen]").remove();
                return
            }
            if (instance && instance.group[instance.currIndex].opts.fullScreen) {
                $container = instance.$refs.container;
                $container.on("click.fb-fullscreen", "[data-fancybox-fullscreen]", function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    FullScreen.toggle()
                });
                if (instance.opts.fullScreen && instance.opts.fullScreen.autoStart === true) {
                    FullScreen.request()
                }
                instance.FullScreen = FullScreen
            } else if (instance) {
                instance.$refs.toolbar.find("[data-fancybox-fullscreen]").hide()
            }
        }, "afterKeydown.fb": function (e, instance, current, keypress, keycode) {
            if (instance && instance.FullScreen && keycode === 70) {
                keypress.preventDefault();
                instance.FullScreen.toggle()
            }
        }, "beforeClose.fb": function (e, instance) {
            if (instance && instance.FullScreen && instance.$refs.container.hasClass("fancybox-is-fullscreen")) {
                FullScreen.exit()
            }
        }
    })
})(document, jQuery);
(function (document, $) {
    "use strict";
    var CLASS = "fancybox-thumbs", CLASS_ACTIVE = CLASS + "-active";
    $.fancybox.defaults = $.extend(true, {
        btnTpl: {thumbs: '<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}">' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M14.59 14.59h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76H5.65V5.65z"/></svg>' + "</button>"},
        thumbs: {autoStart: false, hideOnClose: true, parentEl: ".fancybox-container", axis: "y"}
    }, $.fancybox.defaults);
    var FancyThumbs = function (instance) {
        this.init(instance)
    };
    $.extend(FancyThumbs.prototype, {
        $button: null, $grid: null, $list: null, isVisible: false, isActive: false, init: function (instance) {
            var self = this, group = instance.group, enabled = 0;
            self.instance = instance;
            self.opts = group[instance.currIndex].opts.thumbs;
            instance.Thumbs = self;
            self.$button = instance.$refs.toolbar.find("[data-fancybox-thumbs]");
            for (var i = 0, len = group.length; i < len; i++) {
                if (group[i].thumb) {
                    enabled++
                }
                if (enabled > 1) {
                    break
                }
            }
            if (enabled > 1 && !!self.opts) {
                self.$button.removeAttr("style").on("click", function () {
                    self.toggle()
                });
                self.isActive = true
            } else {
                self.$button.hide()
            }
        }, create: function () {
            var self = this, instance = self.instance, parentEl = self.opts.parentEl, list = [], src;
            if (!self.$grid) {
                self.$grid = $('<div class="' + CLASS + " " + CLASS + "-" + self.opts.axis + '"></div>').appendTo(instance.$refs.container.find(parentEl).addBack().filter(parentEl));
                self.$grid.on("click", "a", function () {
                    instance.jumpTo($(this).attr("data-index"))
                })
            }
            if (!self.$list) {
                self.$list = $('<div class="' + CLASS + '__list">').appendTo(self.$grid)
            }
            $.each(instance.group, function (i, item) {
                src = item.thumb;
                if (!src && item.type === "image") {
                    src = item.src
                }
                list.push('<a href="javascript:;" tabindex="0" data-index="' + i + '"' + (src && src.length ? ' style="background-image:url(' + src + ')"' : 'class="fancybox-thumbs-missing"') + "></a>")
            });
            self.$list[0].innerHTML = list.join("");
            if (self.opts.axis === "x") {
                self.$list.width(parseInt(self.$grid.css("padding-right"), 10) + instance.group.length * self.$list.children().eq(0).outerWidth(true))
            }
        }, focus: function (duration) {
            var self = this, $list = self.$list, $grid = self.$grid, thumb, thumbPos;
            if (!self.instance.current) {
                return
            }
            thumb = $list.children().removeClass(CLASS_ACTIVE).filter('[data-index="' + self.instance.current.index + '"]').addClass(CLASS_ACTIVE);
            thumbPos = thumb.position();
            if (self.opts.axis === "y" && (thumbPos.top < 0 || thumbPos.top > $list.height() - thumb.outerHeight())) {
                $list.stop().animate({scrollTop: $list.scrollTop() + thumbPos.top}, duration)
            } else if (self.opts.axis === "x" && (thumbPos.left < $grid.scrollLeft() || thumbPos.left > $grid.scrollLeft() + ($grid.width() - thumb.outerWidth()))) {
                $list.parent().stop().animate({scrollLeft: thumbPos.left}, duration)
            }
        }, update: function () {
            var that = this;
            that.instance.$refs.container.toggleClass("fancybox-show-thumbs", this.isVisible);
            if (that.isVisible) {
                if (!that.$grid) {
                    that.create()
                }
                that.instance.trigger("onThumbsShow");
                that.focus(0)
            } else if (that.$grid) {
                that.instance.trigger("onThumbsHide")
            }
            that.instance.update()
        }, hide: function () {
            this.isVisible = false;
            this.update()
        }, show: function () {
            this.isVisible = true;
            this.update()
        }, toggle: function () {
            this.isVisible = !this.isVisible;
            this.update()
        }
    });
    $(document).on({
        "onInit.fb": function (e, instance) {
            var Thumbs;
            if (instance && !instance.Thumbs) {
                Thumbs = new FancyThumbs(instance);
                if (Thumbs.isActive && Thumbs.opts.autoStart === true) {
                    Thumbs.show()
                }
            }
        }, "beforeShow.fb": function (e, instance, item, firstRun) {
            var Thumbs = instance && instance.Thumbs;
            if (Thumbs && Thumbs.isVisible) {
                Thumbs.focus(firstRun ? 0 : 250)
            }
        }, "afterKeydown.fb": function (e, instance, current, keypress, keycode) {
            var Thumbs = instance && instance.Thumbs;
            if (Thumbs && Thumbs.isActive && keycode === 71) {
                keypress.preventDefault();
                Thumbs.toggle()
            }
        }, "beforeClose.fb": function (e, instance) {
            var Thumbs = instance && instance.Thumbs;
            if (Thumbs && Thumbs.isVisible && Thumbs.opts.hideOnClose !== false) {
                Thumbs.$grid.hide()
            }
        }
    })
})(document, jQuery);
(function (document, $) {
    "use strict";
    $.extend(true, $.fancybox.defaults, {
        btnTpl: {share: '<button data-fancybox-share class="fancybox-button fancybox-button--share" title="{{SHARE}}">' + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M2.55 19c1.4-8.4 9.1-9.8 11.9-9.8V5l7 7-7 6.3v-3.5c-2.8 0-10.5 2.1-11.9 4.2z"/></svg>' + "</button>"},
        share: {
            url: function (instance, item) {
                return (!instance.currentHash && !(item.type === "inline" || item.type === "html") ? item.origSrc || item.src : false) || window.location
            },
            tpl: '<div class="fancybox-share">' + "<h1>{{SHARE}}</h1>" + "<p>" + '<a class="fancybox-share__button fancybox-share__button--fb" href="https://www.facebook.com/sharer/sharer.php?u={{url}}">' + '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m287 456v-299c0-21 6-35 35-35h38v-63c-7-1-29-3-55-3-54 0-91 33-91 94v306m143-254h-205v72h196" /></svg>' + "<span>Facebook</span>" + "</a>" + '<a class="fancybox-share__button fancybox-share__button--tw" href="https://twitter.com/intent/tweet?url={{url}}&text={{descr}}">' + '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m456 133c-14 7-31 11-47 13 17-10 30-27 37-46-15 10-34 16-52 20-61-62-157-7-141 75-68-3-129-35-169-85-22 37-11 86 26 109-13 0-26-4-37-9 0 39 28 72 65 80-12 3-25 4-37 2 10 33 41 57 77 57-42 30-77 38-122 34 170 111 378-32 359-208 16-11 30-25 41-42z" /></svg>' + "<span>Twitter</span>" + "</a>" + '<a class="fancybox-share__button fancybox-share__button--pt" href="https://www.pinterest.com/pin/create/button/?url={{url}}&description={{descr}}&media={{media}}">' + '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m265 56c-109 0-164 78-164 144 0 39 15 74 47 87 5 2 10 0 12-5l4-19c2-6 1-8-3-13-9-11-15-25-15-45 0-58 43-110 113-110 62 0 96 38 96 88 0 67-30 122-73 122-24 0-42-19-36-44 6-29 20-60 20-81 0-19-10-35-31-35-25 0-44 26-44 60 0 21 7 36 7 36l-30 125c-8 37-1 83 0 87 0 3 4 4 5 2 2-3 32-39 42-75l16-64c8 16 31 29 56 29 74 0 124-67 124-157 0-69-58-132-146-132z" fill="#fff"/></svg>' + "<span>Pinterest</span>" + "</a>" + "</p>" + '<p><input class="fancybox-share__input" type="text" value="{{url_raw}}" onclick="select()" /></p>' + "</div>"
        }
    });

    function escapeHtml(string) {
        var entityMap = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#39;",
            "/": "&#x2F;",
            "`": "&#x60;",
            "=": "&#x3D;"
        };
        return String(string).replace(/[&<>"'`=\/]/g, function (s) {
            return entityMap[s]
        })
    }

    $(document).on("click", "[data-fancybox-share]", function () {
        var instance = $.fancybox.getInstance(), current = instance.current || null, url, tpl;
        if (!current) {
            return
        }
        if ($.type(current.opts.share.url) === "function") {
            url = current.opts.share.url.apply(current, [instance, current])
        }
        tpl = current.opts.share.tpl.replace(/\{\{media\}\}/g, current.type === "image" ? encodeURIComponent(current.src) : "").replace(/\{\{url\}\}/g, encodeURIComponent(url)).replace(/\{\{url_raw\}\}/g, escapeHtml(url)).replace(/\{\{descr\}\}/g, instance.$caption ? encodeURIComponent(instance.$caption.text()) : "");
        $.fancybox.open({
            src: instance.translate(instance, tpl),
            type: "html",
            opts: {
                touch: false, animationEffect: false, afterLoad: function (shareInstance, shareCurrent) {
                    instance.$refs.container.one("beforeClose.fb", function () {
                        shareInstance.close(null, 0)
                    });
                    shareCurrent.$content.find(".fancybox-share__button").click(function () {
                        window.open(this.href, "Share", "width=550, height=450");
                        return false
                    })
                }, mobile: {autoFocus: false}
            }
        })
    })
})(document, jQuery);
(function (window, document, $) {
    "use strict";
    if (!$.escapeSelector) {
        $.escapeSelector = function (sel) {
            var rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g;
            var fcssescape = function (ch, asCodePoint) {
                if (asCodePoint) {
                    if (ch === "\0") {
                        return "�"
                    }
                    return ch.slice(0, -1) + "\\" + ch.charCodeAt(ch.length - 1).toString(16) + " "
                }
                return "\\" + ch
            };
            return (sel + "").replace(rcssescape, fcssescape)
        }
    }

    function parseUrl() {
        var hash = window.location.hash.substr(1), rez = hash.split("-"),
            index = rez.length > 1 && /^\+?\d+$/.test(rez[rez.length - 1]) ? parseInt(rez.pop(-1), 10) || 1 : 1,
            gallery = rez.join("-");
        return {hash: hash, index: index < 1 ? 1 : index, gallery: gallery}
    }

    function triggerFromUrl(url) {
        if (url.gallery !== "") {
            $("[data-fancybox='" + $.escapeSelector(url.gallery) + "']").eq(url.index - 1).focus().trigger("click.fb-start")
        }
    }

    function getGalleryID(instance) {
        var opts, ret;
        if (!instance) {
            return false
        }
        opts = instance.current ? instance.current.opts : instance.opts;
        ret = opts.hash || (opts.$orig ? opts.$orig.data("fancybox") || opts.$orig.data("fancybox-trigger") : "");
        return ret === "" ? false : ret
    }

    $(function () {
        if ($.fancybox.defaults.hash === false) {
            return
        }
        $(document).on({
            "onInit.fb": function (e, instance) {
                var url, gallery;
                if (instance.group[instance.currIndex].opts.hash === false) {
                    return
                }
                url = parseUrl();
                gallery = getGalleryID(instance);
                if (gallery && url.gallery && gallery == url.gallery) {
                    instance.currIndex = url.index - 1
                }
            }, "beforeShow.fb": function (e, instance, current, firstRun) {
                var gallery;
                if (!current || current.opts.hash === false) {
                    return
                }
                gallery = getGalleryID(instance);
                if (!gallery) {
                    return
                }
                instance.currentHash = gallery + (instance.group.length > 1 ? "-" + (current.index + 1) : "");
                if (window.location.hash === "#" + instance.currentHash) {
                    return
                }
                if (firstRun && !instance.origHash) {
                    instance.origHash = window.location.hash
                }
                if (instance.hashTimer) {
                    clearTimeout(instance.hashTimer)
                }
                instance.hashTimer = setTimeout(function () {
                    if ("replaceState" in window.history) {
                        window.history[firstRun ? "pushState" : "replaceState"]({}, document.title, window.location.pathname + window.location.search + "#" + instance.currentHash);
                        if (firstRun) {
                            instance.hasCreatedHistory = true
                        }
                    } else {
                        window.location.hash = instance.currentHash
                    }
                    instance.hashTimer = null
                }, 300)
            }, "beforeClose.fb": function (e, instance, current) {
                if (!current || current.opts.hash === false) {
                    return
                }
                clearTimeout(instance.hashTimer);
                if (instance.currentHash && instance.hasCreatedHistory) {
                    window.history.back()
                } else if (instance.currentHash) {
                    if ("replaceState" in window.history) {
                        window.history.replaceState({}, document.title, window.location.pathname + window.location.search + (instance.origHash || ""))
                    } else {
                        window.location.hash = instance.origHash
                    }
                }
                instance.currentHash = null
            }
        });
        $(window).on("hashchange.fb", function () {
            var url = parseUrl(), fb = null;
            $.each($(".fancybox-container").get().reverse(), function (index, value) {
                var tmp = $(value).data("FancyBox");
                if (tmp && tmp.currentHash) {
                    fb = tmp;
                    return false
                }
            });
            if (fb) {
                if (fb.currentHash !== url.gallery + "-" + url.index && !(url.index === 1 && fb.currentHash == url.gallery)) {
                    fb.currentHash = null;
                    fb.close()
                }
            } else if (url.gallery !== "") {
                triggerFromUrl(url)
            }
        });
        setTimeout(function () {
            if (!$.fancybox.getInstance()) {
                triggerFromUrl(parseUrl())
            }
        }, 50)
    })
})(window, document, jQuery);
(function (document, $) {
    "use strict";
    var prevTime = (new Date).getTime();
    $(document).on({
        "onInit.fb": function (e, instance, current) {
            instance.$refs.stage.on("mousewheel DOMMouseScroll wheel MozMousePixelScroll", function (e) {
                var current = instance.current, currTime = (new Date).getTime();
                if (instance.group.length < 2 || current.opts.wheel === false || current.opts.wheel === "auto" && current.type !== "image") {
                    return
                }
                e.preventDefault();
                e.stopPropagation();
                if (current.$slide.hasClass("fancybox-animated")) {
                    return
                }
                e = e.originalEvent || e;
                if (currTime - prevTime < 250) {
                    return
                }
                prevTime = currTime;
                instance[(-e.deltaY || -e.deltaX || e.wheelDelta || -e.detail) < 0 ? "next" : "previous"]()
            })
        }
    })
})(document, jQuery);
(function () {
    $(function () {
        var errorClass = "error";
        var $name = $("#js-order-call-name");
        var $phone = $("#js-order-call-phone");
        var $success = $("#js-order-call-success");
        var $input = $("#js-order-call-input");
        var submitSelector = "#js-order-call-submit";
        $name.click(function () {
            $name.removeClass(errorClass)
        });
        $phone.click(function () {
            $phone.removeClass(errorClass)
        });
        $(".js-order-call").fancybox({
            type: "inline",
            src: "#js-order-call",
            smallBtn: true,
            modal: false,
            buttons: ["close"],
            touch: false,
            lang: "ru",
            i18n: {ru: {CLOSE: "Закрыть"}},
            beforeShow: function () {
                $success.hide(0);
                $input.show(0);
                $name.removeClass(errorClass).val("");
                $phone.removeClass(errorClass).val("")
            }
        });
        $(".js-popup-close").click(function () {
            $.fancybox.close()
        });
        $(document).on("click", submitSelector, function () {
            $success.hide(0);
            $input.show(0);
            var name = $name.val();
            var phone = $phone.val();
            var errorClass = "error";
            if (name === "" || phone === "") {
                if (name === "") {
                    $name.addClass(errorClass)
                }
                if (phone === "") {
                    $phone.addClass(errorClass)
                }
            } else {
                $.ajax({
                    method: "post", url: "/ajax/sendMail.php", data: {n: name, p: phone}, success: function (e) {
                        console.log(e);
                        ym(69609880, "reachGoal", "order-call-form-submitted");
                        dataLayer.push({event: "order_call_form_submitted"});
                        $success.show(0);
                        $input.hide(0)
                    }
                })
            }
        })
    })
})();
(function () {
    $(function () {
        var errorClass = "error";
        var $name = $("#js-mortgage-name");
        var $phone = $("#js-mortgage-phone");
        var $success = $("#js-mortgage-success");
        var $input = $("#js-mortgage-input");
        var submitSelector = "#js-mortgage-submit";
        $name.click(function () {
            $name.removeClass(errorClass)
        });
        $phone.click(function () {
            $phone.removeClass(errorClass)
        });
        $(".js-mortgage").fancybox({
            type: "inline",
            src: "#js-mortgage",
            smallBtn: true,
            modal: false,
            buttons: ["close"],
            touch: false,
            lang: "ru",
            i18n: {ru: {CLOSE: "Закрыть"}},
            beforeShow: function () {
                $success.hide(0);
                $input.show(0);
                $name.removeClass(errorClass).val("");
                $phone.removeClass(errorClass).val("")
            }
        });
        $(".js-popup-close").click(function () {
            $.fancybox.close()
        });
        $(document).on("click", submitSelector, function () {
            $success.hide(0);
            $input.show(0);
            var name = $name.val();
            var phone = $phone.val();
            var errorClass = "error";
            if (name === "" || phone === "") {
                if (name === "") {
                    $name.addClass(errorClass)
                }
                if (phone === "") {
                    $phone.addClass(errorClass)
                }
            } else {
                $.ajax({
                    method: "post", url: "/mortgage/", data: {n: name, p: phone}, success: function () {
                        ym(69609880, "reachGoal", "mortgage-form-submitted");
                        dataLayer.push({event: "mortgage_form_submitted"});
                        $success.show(0);
                        $input.hide(0)
                    }
                })
            }
        })
    })
})();
(function () {
    var scrollToElement = function ($target) {
        $("html, body").animate({scrollTop: $target.offset().top - 46}, 1e3)
    };
    var getElementByHash = function (hash) {
        return $('[data-anchor="' + hash + '"]')
    };
    $(function () {
        $('.js-inner-link, a[href*="#"]').click(function () {
            var $t = $(this);
            var hash = $t.attr("href").split("#");
            if (hash.length === 2) {
                var $target = getElementByHash("#" + hash[1]);
                if ($target.length === 1) {
                    scrollToElement($target)
                }
            }
        })
    });
    $(window).on("load", function () {
        var $target = getElementByHash(window.location.hash);
        if ($target.length === 1) {
            scrollToElement($target)
        }
    })
})();
(function () {
    $(function () {
        var duration = 150;
        $(document).click(function (e) {
            $target = $(e.target);
            if ($target.hasClass("pop_btn") || $target.hasClass("brs_mid")) {
                return
            }
            $(".pop_win").animate({height: 0, opacity: 0}, {
                duration: duration, complete: function () {
                    $(this).css({display: "none", height: "auto", opacity: 1})
                }
            })
        });
        $(".popup_pop .pop_btn").click(function () {
            var $btn = $(this);
            var $wrap = $btn.parents(".popup_pop");
            var $popup = $(".pop_win", $wrap);
            $popup.animate({height: "toggle", opacity: "toggle"}, {duration: duration})
        })
    })
})();
(function () {
    $(function () {
        var wrapperClasses = [];
        for (var i = 1; i <= 555; i++) {
            wrapperClasses.push("js-slides-" + i)
        }
        var cfg = {
            spaceBetween: 5,
            autoHeight: false,
            grabCursor: true,
            nextButton: null,
            prevButton: null,
            loop: false,
            centerInsufficientSlides: true,
            watchOverflow: true,
            breakpoints: {
                100: {slidesPerView: 2, slidesPerGroup: 2},
                360: {slidesPerView: 2, slidesPerGroup: 2},
                620: {slidesPerView: 3, slidesPerGroup: 3},
                850: {slidesPerView: 4, slidesPerGroup: 4}
            }
        };
        var customCfg = {"js-slides-1": {}};
        $(wrapperClasses).each(function (i, wrapperClass) {
            var dotClass = "." + wrapperClass;
            var cfgLocal = $.extend({}, cfg);
            cfgLocal.navigation = {
                nextEl: dotClass + " .swiper-button-next",
                prevEl: dotClass + " .swiper-button-prev"
            };
            cfgLocal.pagination = {el: dotClass + " .swiper-pagination", type: "bullets", clickable: "bullets"};
            var c = cfgLocal;
            if (typeof customCfg[wrapperClass] !== "undefined") {
                c = $.extend(true, {}, c, customCfg[wrapperClass])
            }
            if ($(dotClass).length > 0) {
                new Swiper(dotClass + " .swiper-container", c)
            }
        });

        function adjustSwipersPagination() {
            var $pagers = $(".swiper-pagination");
            $pagers.each(function () {
                var $t = $(this);
                if ($(".swiper-pagination-bullet", $t).length === 1) {
                    $t.parents(".swiper-container-wrapper").addClass("swiper-disabled")
                }
            })
        }

        adjustSwipersPagination();
        $(window).resize(adjustSwipersPagination)
    })
})();
(function () {
    $(function () {
        if ($(".js-tabs-slider").length > 0) {
            var $btns = $("[data-tab-btn]");
            var $tabs = $("[data-tab]");
            var activeTabBtnClass = "tas_tab--active";
            new Swiper(".js-tabs-slider .swiper-container", {
                slidesPerView: "auto",
                centeredSlides: true,
                spaceBetween: 10,
                navigation: {
                    nextEl: ".js-tabs-slider .swiper-button-next",
                    prevEl: ".js-tabs-slider .swiper-button-prev"
                }
            });

            function renderTabs(activateDate) {
                $btns.each(function () {
                    var $t = $(this);
                    var date = $t.data("tab-btn");
                    if (activateDate === date) {
                        $t.addClass(activeTabBtnClass)
                    } else {
                        $t.removeClass(activeTabBtnClass)
                    }
                });
                $tabs.each(function () {
                    var $t = $(this);
                    var date = $t.data("tab");
                    if (activateDate === date) {
                        $t.show(0)
                    } else {
                        $t.hide(0)
                    }
                })
            }

            renderTabs($btns.eq(0).data("tab-btn"));
            $btns.click(function () {
                var $t = $(this);
                var date = $t.data("tab-btn");
                renderTabs(date)
            })
        }
    })
})();
$(function () {
    $(".show-media, [data-fancybox]").fancybox({
        image: {protect: true},
        helpers: {title: {type: "inside", position: "top"}},
        infobar: false,
        loop: true,
        buttons: ["zoom", "slideShow", "fullScreen", "close"],
        video: {
            tpl: '<video class="fancybox-video" controls controlsList="nodownload">' + '<source src="{{src}}" type="{{format}}" />' + 'К сожалению Ваш браузер не поддерживает встроенные видео. <a href="{{src}}">Загурзите</a> и посмотрите в вашем видео-проигрывателе!' + "</video>",
            format: "",
            autoStart: true
        },
        lang: "ru",
        i18n: {
            ru: {
                CLOSE: "Закрыть",
                NEXT: "Следующее",
                PREV: "Предыдущее",
                ERROR: "Ресурс недоступен. <br/> Попробуйте открыть позже.",
                PLAY_START: "Начать слайд-шоу",
                PLAY_STOP: "Пауза",
                FULL_SCREEN: "На весь экран",
                THUMBS: "Миниатюры",
                DOWNLOAD: "Загрузить",
                SHARE: "Поделиться",
                ZOOM: "Приблизить"
            }
        }
    })
});