<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->SetPageProperty("title", 'О проекте – Жилой коммплекс «Кедр»');
$APPLICATION->SetTitle('О проекте – Жилой коммплекс «Кедр»');
?>
    <div class="builder_bdr spot-bg" data-anchor="#o-zastroishike">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/squirrel-stroi.png" class="bdr_squirrel">
                    <h2 class="title-lg">О застройщике</h2>
                    <img class="bdr_logo" src="<?= DEFAULT_TEMPLATE_PATH ?>img/logo-stm.svg">
                    <div class="text-main">
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/o-proekte/o-zastroichike.php"]
                        );
                        ?>
                    </div>
                    <div class="bdr_slogan title-lg">
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/o-proekte/slogan.php"]
                        );
                        ?>
                    </div>
                    <div class="bdr_links">
                        <?php
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/o-proekte/links.php"]
                        );
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="reaches_rea bg-brown-lite">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="rea_block">
                        <div class="rea_title">
                            ГК «СТМ» за тринадцать лет работы построено:
                        </div>

                        <div class="rea_cells">
                            <div class="rea_cell">
                                <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/spot-building.svg" class="rea_img">
                                <div class="rea_text rea_text--01">
                                    <?php
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/o-proekte/postroeno-1.php"]
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="rea_cell">
                                <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/spot-plant.svg" class="rea_img">
                                <div class="rea_text rea_text--02">
                                    <?php
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/o-proekte/postroeno-1.php"]
                                    );
                                    ?>
                                </div>
                            </div>
                            <div class="rea_cell">
                                <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/spot-people.svg" class="rea_img">
                                <?php
                                $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/o-proekte/postroeno-1.php"]
                                );
                                ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="slides_sli bg-green-lite sli--green">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="sli_block">
                        <div class="sli_title title-lg">
                            Дом №9
                        </div>
                        <div class="sli_notice">
                            сдан
                        </div>
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:news.list", "sliderGallery", [
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                            "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                            "AJAX_MODE" => "N",    // Включить режим AJAX
                            "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                            "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                            "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
                            "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                            "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                            "CACHE_TYPE" => "A",    // Тип кеширования
                            "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                            "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                            "DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
                            "DISPLAY_DATE" => "N",    // Выводить дату элемента
                            "DISPLAY_NAME" => "Y",    // Выводить название элемента
                            "DISPLAY_PICTURE" => "N",    // Выводить изображение для анонса
                            "DISPLAY_PREVIEW_TEXT" => "N",    // Выводить текст анонса
                            "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                            "FIELD_CODE" => [],
                            "FILTER_NAME" => "",    // Фильтр
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                            "IBLOCK_ID" => "1",    // Код информационного блока
                            "IBLOCK_TYPE" => "content",    // Тип информационного блока (используется только для проверки)
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
                            "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                            "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                            "NEWS_COUNT" => "10000",    // Количество новостей на странице
                            "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                            "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                            "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                            "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                            "PAGER_TITLE" => "Новости",    // Название категорий
                            "PARENT_SECTION" => "",    // ID раздела
                            "PARENT_SECTION_CODE" => "dom-9",    // Код раздела
                            "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                            "PROPERTY_CODE" => [0 => "FILES"],
                            "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                            "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                            "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                            "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                            "SET_STATUS_404" => "N",    // Устанавливать статус 404
                            "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                            "SHOW_404" => "N",    // Показ специальной страницы
                            "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                            "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                            "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                            "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                            "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                        ],
                            false
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slides_sli bg-brown-lite" data-anchor="#hod-stroitelstva">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="sli_block">
                        <div class="title-lg">
                            <div class="xlite">
                                Ход строительства.
                            </div>
                            Дома №10 & №11
                        </div>
                        <?php
                        $APPLICATION->IncludeComponent("bitrix:news.list", "sliderGallery", [
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                            "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                            "AJAX_MODE" => "N",    // Включить режим AJAX
                            "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                            "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                            "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
                            "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                            "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                            "CACHE_TYPE" => "A",    // Тип кеширования
                            "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                            "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                            "DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
                            "DISPLAY_DATE" => "N",    // Выводить дату элемента
                            "DISPLAY_NAME" => "Y",    // Выводить название элемента
                            "DISPLAY_PICTURE" => "N",    // Выводить изображение для анонса
                            "DISPLAY_PREVIEW_TEXT" => "N",    // Выводить текст анонса
                            "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                            "FIELD_CODE" => [],
                            "FILTER_NAME" => "",    // Фильтр
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                            "IBLOCK_ID" => "1",    // Код информационного блока
                            "IBLOCK_TYPE" => "content",    // Тип информационного блока (используется только для проверки)
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
                            "INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
                            "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                            "NEWS_COUNT" => "10000",    // Количество новостей на странице
                            "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                            "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                            "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                            "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                            "PAGER_TITLE" => "Новости",    // Название категорий
                            "PARENT_SECTION" => "",    // ID раздела
                            "PARENT_SECTION_CODE" => "doma-10-11",    // Код раздела
                            "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                            "PROPERTY_CODE" => [0 => "FILES"],
                            "SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
                            "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                            "SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
                            "SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
                            "SET_STATUS_404" => "N",    // Устанавливать статус 404
                            "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                            "SHOW_404" => "N",    // Показ специальной страницы
                            "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                            "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                            "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                            "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                            "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                        ],
                            false
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="progress-video_prv slides_sli sli--green bg-green-lite">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="sli_block">
                        <div class="title-lg">
                            <div class="xlite">Прямая трансляция строительства.</div>
                            Дома&nbsp;№10&nbsp;&&nbsp;№11
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="prv_col">
                                    <iframe class="prv_frame" src="https://rtsp.me/embed/Ad3EddHS/" frameborder="0"
                                            allowfullscreen=""></iframe>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="prv_col">
                                    <iframe class="prv_frame" src="https://rtsp.me/embed/ntRnh688/" frameborder="0"
                                            allowfullscreen=""></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="docs_doc bg-brown-lite" data-anchor="#dokumenty">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="doc_block">
                        <div class="title-lg">Документы</div>
                        <div class="text-main">
                            <?php
                            $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/o-proekte/documents.php"]
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="novelties_nov" data-anchor="#novosti">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="title-lg">Новости</div>
                    <?php
                    $APPLICATION->IncludeComponent("bitrix:news.list", "news", Array(
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_DATE" => "N",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "2",	// Код информационного блока
		"IBLOCK_TYPE" => "content",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "10000",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "dom-10-11",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "FILES",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"COMPONENT_TEMPLATE" => "sliderGallery"
	),
	false
);
                    ?>
                </div>
            </div>
        </div>
    </div>

<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>