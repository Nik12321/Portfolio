<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');?>
    <div class="buildings-navs_bun bg-green-brown">
        <div class="container">
            <div class="row">
                <div class="bun_block col-sm-12">
                    <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/buildings-squirrel.jpg" class="bun_img">
                    <div class="bun_navs">
                        <div class="bun_nav bun_nav--left">
                            <span class="bun_nav-sym">
                                <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/cone-green-dark.svg" class="bun_nav-img">
                            </span>
                            <span class="bun_title">Дом №9</span>
                            <span class="bun_notice">дом сдан</span>
                        </div>
                        <a href="/dom-10/" class="bun_nav bun_nav--center">
                            <span class="bun_nav-sym">
                                <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/cone-green.svg" class="bun_nav-img">
                            </span>
                            <span class="bun_title">Дом №10</span>
                        </a>
                        <a href="/dom-11/" class="bun_nav bun_nav--right">
                            <span class="bun_nav-sym">
                                <img src="<?= DEFAULT_TEMPLATE_PATH ?>img/cone-brown.svg" class="bun_nav-img">
                            </span>
                            <span class="bun_title">Дом №11</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$APPLICATION->IncludeComponent(
    "bitrix:news",
    "apartments",
    array(
        "ADD_ELEMENT_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BROWSER_TITLE" => "-",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
        "DETAIL_DISPLAY_TOP_PAGER" => "N",
        "DETAIL_FIELD_CODE" => array(
            0 => "PREVIEW_PICTURE",
            1 => "DETAIL_PICTURE",
            2 => "",
        ),
        "DETAIL_PAGER_SHOW_ALL" => "Y",
        "DETAIL_PAGER_TEMPLATE" => "",
        "DETAIL_PAGER_TITLE" => "Страница",
        "DETAIL_PROPERTY_CODE" => array(
            0 => "BALCON_SQUARE",
            1 => "ROOM_SQUARE",
            2 => "HALL_SQUARE",
            3 => "KITCHEN_SQUARE",
            4 => "SQUARE",
            5 => "SU_SQUARE",
            6 => "TOTAL_PRICE",
            7 => "PRICE_FOR_M",
            8 => "",
        ),
        "DETAIL_SET_CANONICAL_URL" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "apartments",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
        "LIST_FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "LIST_PROPERTY_CODE" => array(
            0 => "SQUARE",
            1 => "",
        ),
        "MESSAGE_404" => "",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "NEWS_COUNT" => "1000",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PREVIEW_TRUNCATE_LEN" => "",
        "SEF_FOLDER" => "/",
        "SEF_MODE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N",
        "USE_CATEGORIES" => "N",
        "USE_FILTER" => "N",
        "USE_PERMISSIONS" => "N",
        "USE_RATING" => "N",
        "USE_REVIEW" => "N",
        "USE_RSS" => "N",
        "USE_SEARCH" => "N",
        "USE_SHARE" => "N",
        "COMPONENT_TEMPLATE" => "apartments",
        "SEF_URL_TEMPLATES" => array(
            "news" => "/",
            "section" => "#SECTION_CODE_PATH#/",
            "detail" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
        )
    ),
    false
);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>