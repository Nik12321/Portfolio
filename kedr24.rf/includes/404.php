<?php
$APPLICATION->SetPageProperty("title", "Страница не найдена – Жилой коммплекс «Кедр»");
$APPLICATION->SetTitle("Страница не найдена – Жилой коммплекс «Кедр»");
?>
<div class="error-404_er4 spot-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="title-lg">Ошибка 404</h1>
                <div class="text-main">К сожалению на нашем сайте такой страницы не существует.</div>
                <p class="text-center"><a href="/" class="er4_btn">Перейти на главную</a></p>
            </div>
        </div>
    </div>
</div>