<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    if (!empty($_POST['n']) && !empty($_POST['p'])) {
        
        $subject = 'кедр24.рф: заказ звонка';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $arEventFieldsMessage = array(
            "PHONE" => $_POST['p'],
            "NAME" =>  $_POST['n'],
            "IP" => $ip,
            "DEFAULT_EMAIL_FROM" =>  (CSite::GetByID("s1")->Fetch())["EMAIL"],
        );

        //Добавляем запись в гугл таблицу:
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "http://orders.bwsdev.ru/");
        $postData = [
            'phone' => $_POST['p'],
            'name' => $_POST['n'],
            'source' => 'Сайт кедр24.рф с формы заявки',
            'type' => "kedr"
        ];
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        $curlResult = curl_exec($curl);
        $curl = curl_close();


        CEvent::Send("GET_MESSAGE_FROM_USER", "s1", $arEventFieldsMessage, "Y", "");
    }
    die('ok');
}