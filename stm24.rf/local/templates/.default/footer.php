<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
<footer class="footer">
    <div class="footer--line"></div>
    <div class="container">
        <div class="footer--wrap">
            <div class="col md-hide">
                <h5>г. Красноярск</h5>
            </div>
            <div class="col">
                <h5 class="md-show">г. Красноярск</h5>
                <h5>
                    Адрес
                </h5>
                <p>
                    2-ая Брянская, 20 «К»,<br> 4-й этаж.
                </p>
            </div>
            <div class="col">
                <h5>
                    ТЕЛЕФОН
                </h5>
                <p>
                    <a href="tel:+74954141512">
                        +7-495-414-15-12
                    </a>
                    <a href="tel:+73912191604">
                        +7-391-219-16-04
                    </a>
                </p>
                <h5 class="sm-show">
                    ПОЧТА
                </h5>
                <p class="sm-show">
                    <a href="mailto:web@bauart.pro">
                        web@bauart.pro

                    </a>
                </p>
            </div>
            <div class="col sm-hide">
                <h5>
                    ПОЧТА
                </h5>
                <p>
                    <a href="mailto:web@bauart.pro">
                        web@bauart.pro

                    </a>
                </p>
            </div>
            <div class="col bauart">
                <a href="#">
                    <img class="" src="<?=DEFAULT_TEMPLATE_PATH?>/img/template/bauart.svg" alt="">
                </a>
            </div>
        </div>
    </div>
</footer>

</body>
</html>