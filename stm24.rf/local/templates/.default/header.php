<?php
use Bitrix\Main\Page\Asset;
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>СтройТехМонтаж</title>
    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="120x120" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <script src="https://api-maps.yandex.ru/2.1/?apikey=747b5c70-c476-46f9-96f3-fdd28ad707a4&lang=ru_RU"></script>
    <!-- FAVICON -->

    <?php
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH. '/css/style.css');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/lib/jquery-3.5.1.min.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/script.min.js");
    $APPLICATION->ShowHead();
    ?>
</head>
<body>
<svg style="display: none;" x="0px" y="0px" xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <filter id="mapFilter">

            <feColorMatrix type="matrix"
                           values="0.09019607843137255 0.09019607843137255 0.09019607843137255  0 0 0.1450980392156863 0.1450980392156863 0.1450980392156863  0 0 0.16862745098039217 0.16862745098039217 0.16862745098039217  0 0  0 0 0 1 0" />
        </filter>
    </defs>
</svg>
<div id="panel">
    <?$APPLICATION->ShowPanel();?>
</div>