<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(true);
//$videoIndex = 1;
?>
<main class="gray-bg grid">
    <div class="container-left">
        <div class="logo">
            <a href="/">
                <img src="<?= DEFAULT_TEMPLATE_PATH ?>/img/logo-gray.png" alt="">
            </a>
        </div>
        <button onclick="javascript:history.back(); return false;" class="page-back">
            <svg width="51" height="50" viewBox="0 0 51 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M50.4457 22.3315H9.90351L34.1692 0.983146C34.5572 0.639045 34.3217 0 33.8089 0H27.6767C27.4064 0 27.15 0.0983142 26.9491 0.273876L0.764025 23.3006C0.5242 23.5112 0.331861 23.7717 0.200042 24.0643C0.0682217 24.3569 0 24.6748 0 24.9965C0 25.3181 0.0682217 25.6361 0.200042 25.9287C0.331861 26.2213 0.5242 26.4817 0.764025 26.6924L27.1015 49.8596C27.2055 49.9508 27.3302 50 27.4618 50H33.802C34.3147 50 34.5503 49.3539 34.1623 49.0169L9.90351 27.6685H50.4457C50.7506 27.6685 51 27.4157 51 27.1067V22.8933C51 22.5843 50.7506 22.3315 50.4457 22.3315Z"/>
            </svg>
        </button>
    </div>
    <div class="container-right">
        <section class="detail">
            <h2>
                <?= $arResult["NAME"] ?>
            </h2>
            <div class="detail__text">
                <?php foreach ($arResult["PROPERTIES"] as $key => $property) : ?>
                    <?php if ($property["VALUE"]) : ?>
                        <p>
                            <b><?= $property["NAME"] ?>:</b> <?= $property["VALUE"] ?>
                        </p>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?php if ($arResult["DESCRIPTION"]) : ?>
                    <p>
                        <?= $arResult["DESCRIPTION"] ?>
                    </p>
                <?php endif; ?>
            </div>
            <?php if (count($arResult["ALL_IMAGES"])) : ?>
                <div class="detail__slider">
                    <!-- Slider main container -->
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <?php $index = 1; ?>
                            <?php foreach ($arResult["ALL_IMAGES"] as $image) : ?>
                                <div class="swiper-slide" id="sildeView-<?= $index ?>">
                                    <a href="<?= $image["OLD"] ?>">
                                        <img src="<?= $image["NEW"] ?>"/>
                                    </a>
                                </div>
                                <?php
                                if ($index > 10) {
                                    break;
                                }
                                $index++;
                                ?>
                            <?php endforeach; ?>
                        </div>
                        <!-- If we need pagination -->
                    </div>
                    <div class="swiper-pagination"></div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            <?php endif; ?>
            <?php if (count($arResult["FILES"]) || count($arResult["VIDEOS"])) : ?>
                <h3>Архив фото и видео процесса строительства.</h3>
                <div class="detail__gallery--wrap">
                    <?php $index = 1; ?>
                    <?php foreach ($arResult["FILES"] as $name => $array) : ?>
                        <div class="detail__gallery">
                            <div class="detail__gallery--images" id="lightgallery-<?= $index ?>">
                                <?php foreach ($array as $value) : ?>
                                    <a href="<?= $value["OLD"] ?>"><img src="<?= $value["NEW"] ?>"/></a>
                                <?php endforeach; ?>
                                <!--                            --><?php //if ($arResult["VIDEOS"][$name]) :?>
                                <!--                                --><?php //foreach($arResult["VIDEOS"][$name] as $video) :?>
                                <!--                                    <div style="display:none;" id="video-->
                                <? //=$videoIndex?><!--">-->
                                <!--                                        <video class="lg-video-object lg-html5" controls preload="none">-->
                                <!--                                            <source src="-->
                                <? //=$video["SRC"]?><!--" type="video/mp4">-->
                                <!--                                            Your browser does not support HTML5 video.-->
                                <!--                                        </video>-->
                                <!--                                    </div>-->
                                <!--                                    <a href="-->
                                <? //= $video["SRC"] ?><!--"  data-html="#video--><? //=$videoIndex?><!--">-->
                                <!--                                        <img src="-->
                                <? //= $video["SRC"] ?><!--"/>-->
                                <!--                                    </a>-->
                                <!--                                --><?php //$videoIndex++;?>
                                <!--                                --><?php //endforeach;?>
                                <!--                            --><?php //endif;?>
                            </div>
                            <div class="detail__gallery--title">
                                <?= $name ?>
                            </div>
                        </div>
                        <?php $index++; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </section>
    </div>
</main>
<script src="<?= DEFAULT_TEMPLATE_PATH ?>/js/lib/lightgallery.min.js"></script>
<script src="<?= DEFAULT_TEMPLATE_PATH ?>/js/lib/swiper-bundle.js"></script>
<script src="<?= DEFAULT_TEMPLATE_PATH ?>/js/lib/swiper-bundle.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", () => {
        if (document.querySelector('.swiper-container')) {
            var mySwiper = new Swiper('.swiper-container', {
                loop: false,
                autoHeight: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        }
        let sliderView = document.querySelectorAll('.swiper-slide');
        let gallery = document.querySelectorAll('.detail__gallery--images');

        for (var i = 0; i < sliderView.length; i++) {
            lightGallery(document.getElementById(`sildeView-${i + 1}`));
        }
        for (var i = 0; i < gallery.length; i++) {
            lightGallery(document.getElementById(`lightgallery-${i + 1}`));
        }
    });
</script>