<?php
$galleryIblockId = getIblockIdByCode("photoAlbum");
if ($arResult["PROPERTIES"]["FILES"]["VALUE"]) {
    $files = CIBlockElement::GetList(
        ["SORT" => "ASC", "ACTIVE_FROM" => "DESC"],
        ["IBLOCK_ID" => $galleryIblockId, "IBLOCK_SECTION_ID" => $arResult["PROPERTIES"]["FILES"]["VALUE"]],
        false,
        false,
        false,
        ["ID", "PROPERTY_FILES", "NAME"]
    );
    $allImages = [];
    $finalImageArray = $finalVideoArray = [];
    while ($file = $files->GetNextElement()) {
        $fields = $file->GetFields();
        $properties = $file->GetProperties();
        $finalImageArray[$fields["NAME"]] = [];
        $finalVideoArray[$fields["NAME"]] = [];
        foreach ($properties["FILES"]["VALUE"] as $value) {
            $fileArray = CFile::GetFileArray($value);
            $resizedFile = CFile::ResizeImageGet($fileArray, ["width" => 99999, "height" => 400], BX_RESIZE_IMAGE_PROPORTIONAL);
            ($resizedFile["src"])
                ?  $finalImageArray[$fields["NAME"]][] = ["NEW" => $resizedFile["src"], "OLD" => $fileArray["SRC"]]
                :  $finalImageArray[$fields["NAME"]][] =  ["NEW" => $fileArray["SRC"], "OLD" => $fileArray["SRC"]];
            if (count($allImages) < 10) {
                ($resizedFile["src"])
                    ?  $allImages[] = ["NEW" => $resizedFile["src"], "OLD" => $fileArray["SRC"]]
                    :  $allImages[] = ["NEW" => $fileArray["SRC"], "OLD" => $fileArray["SRC"]];
            }
        }
        foreach ($properties["VIDEO"]["VALUE"] as $value) {
            $fileArray = CFile::GetFileArray($value);
            if ($fileArray["SRC"]) {
                $finalVideoArray[$fields["NAME"]][] = ["SRC" => $fileArray["SRC"], "TYPE" => "VIDEO"];
            }
        }
    }
    $arResult["ALL_IMAGES"] = $allImages;
    $arResult["FILES"] = $finalImageArray;
    $arResult["VIDEOS"] = $finalVideoArray;
}

unset($arResult["PROPERTIES"]["FILES"]);
unset($arResult["PROPERTIES"]["TYPE"]);
unset($arResult["PROPERTIES"]["STATUS"]);
unset($arResult["PROPERTIES"]["COORDINATE"]);
$arResult["DESCRIPTION"] = $arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"];
unset($arResult["PROPERTIES"]["DESCRIPTION"]);