<?php
foreach ($arResult["ITEMS"] as &$arItem) {
    if ($arItem["PREVIEW_PICTURE"]["SRC"]) {
        $newPhoto = CFile::ResizeImageGet(
            $arItem["PREVIEW_PICTURE"],
            ["width"=> 52, "height" => 52],
            BX_RESIZE_IMAGE_EXACT ,
            false,
            100
        );
        if ($newPhoto["src"]) {
            $arItem["PREVIEW_PICTURE"]["SRC"] = $newPhoto["src"];
        }
    }
}
unset($arItem);