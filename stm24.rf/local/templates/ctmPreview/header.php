<?php
use Bitrix\Main\Page\Asset;
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>СтройТехМонтаж</title>
    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="120x120" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?=DEFAULT_TEMPLATE_PATH?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <script src="https://api-maps.yandex.ru/2.1/?apikey=747b5c70-c476-46f9-96f3-fdd28ad707a4&lang=ru_RU"></script>
    <!-- FAVICON -->

    <?php
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH. '/css/style.css');
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH. '/css/lightgallery.min.css');
    Asset::getInstance()->addCss(DEFAULT_TEMPLATE_PATH. '/css/swiper-bundle.min.css');
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/lib/jquery-3.5.1.min.js");
    Asset::getInstance()->addJs(DEFAULT_TEMPLATE_PATH . "/js/script.min.js");
    $APPLICATION->ShowHead();
    ?>
</head>
<body>
<div id="panel">
    <?$APPLICATION->ShowPanel();?>
</div>

<header>
    <div class="container contacts">
        <a href="tel:+73912178921" class="header__contacts">
            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.5672 4.04648L13.6951 2.17617C13.5631 2.04353 13.4061 1.93829 13.2332 1.8665C13.0604 1.7947 12.8751 1.75777 12.6879 1.75781C12.3064 1.75781 11.9479 1.90723 11.6789 2.17617L9.66445 4.19062C9.53182 4.32268 9.42658 4.47965 9.35478 4.6525C9.28299 4.82535 9.24605 5.01068 9.24609 5.19785C9.24609 5.5793 9.39551 5.93789 9.66445 6.20684L11.1375 7.67988C10.7927 8.43989 10.3133 9.13129 9.72246 9.7207C9.1331 10.313 8.44177 10.7941 7.68164 11.141L6.2086 9.66797C6.07654 9.53533 5.91957 9.43009 5.74672 9.3583C5.57387 9.2865 5.38854 9.24956 5.20137 9.24961C4.81992 9.24961 4.46133 9.39902 4.19238 9.66797L2.17617 11.6807C2.04337 11.813 1.93804 11.9702 1.86624 12.1434C1.79444 12.3165 1.75759 12.5022 1.75781 12.6896C1.75781 13.0711 1.90723 13.4297 2.17617 13.6986L4.04473 15.5672C4.47363 15.9979 5.06602 16.2422 5.67422 16.2422C5.80254 16.2422 5.92559 16.2316 6.04688 16.2105C8.41641 15.8203 10.7666 14.56 12.6633 12.665C14.5582 10.7719 15.8168 8.42344 16.2123 6.04688C16.3318 5.3209 16.091 4.57383 15.5672 4.04648Z" fill="white" />
            </svg>
            <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/headerContacts.php"]);?>
        </a>
        <?php $APPLICATION->IncludeComponent("bitrix:main.include", "", ["AREA_FILE_SHOW" => "file", "PATH" => "/includes/mail.php"]);?>
    </div>
    <div class="container">
        <div class="header__logo" data-map="Карта объектов">
            <a href="/">
                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/logo.png" alt="">
            </a>
        </div>
        <div class="map__filter">
            <form action="#" id="rdyForm">
                <div>
                    <input id="rdy-1" hidden type="checkbox" name="nonReady">
                    <label for="rdy-1">
                        Строящееся
                    </label>
                </div>
                <div>
                    <input id="rdy-2" hidden type="checkbox" name="ready">
                    <label for="rdy-2">
                        Готовое
                    </label>
                </div>
            </form>
            <!--  -->
            <form action="#" id="lifeForm">
                <div>
                    <input id="life-1" hidden type="checkbox" name="life">
                    <label for="life-1">
                        Жилое
                    </label>
                </div>
                <div>
                    <input id="life-2" hidden type="checkbox" name="unlife">
                    <label for="life-2">
                        Нежилое
                    </label>
                </div>
            </form>
        </div>

        <div class="contact-link">
            <a href="/contacts/">Контакты</a>
        </div>
    </div>
</header>