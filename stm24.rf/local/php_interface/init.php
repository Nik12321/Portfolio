<?php
define("DEFAULT_TEMPLATE_PATH", "/local/templates/.default/assets/");
CModule::IncludeModule("iblock");
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');

function PR($o, $toString = false, $ltf = false)
{
    $bt_src = debug_backtrace();
    if ($ltf) {
        $bt = $bt_src[1];
    } else {
        $bt = $bt_src[0];
    }
    $dRoot = $_SERVER["DOCUMENT_ROOT"];
    $dRoot = str_replace("/", "\\", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    $dRoot = str_replace("\\", "/", $dRoot);
    $bt["file"] = str_replace($dRoot, "", $bt["file"]);
    $output = '<div style="font-size:9pt; color:#000; background:#fff; border:1px dashed #000;">';
    $output .= '<div style="padding:3px 5px; background:#99CCFF; font-weight:bold;">File: ' . $bt["file"] . ' [' . $bt["line"] . '] ' . date("d.m.Y H:i:s") . '</div>';
    $output .= '<pre style="padding:10px;">' . var_export($o, true) . '</pre>';
    $output .= '</div>';
    if ($toString) {
        return $output;
    }
    echo $output;
}

function logToFile($filepath, $var, $unlink = false)
{
    $logFile = $_SERVER["DOCUMENT_ROOT"].$filepath;
    $dirpath = dirname($logFile);
    if(!file_exists($dirpath)) {
        mkdir($dirpath);
    }
    if($unlink) {
        unlink($logFile);
    }
    file_put_contents($logFile, print_r($var,true) . "\n\n", FILE_APPEND);
}

function getIblockIdByCode($code)
{
    $id = CIBlock::GetList([], ['CODE' => $code], false)->Fetch()["ID"];
    if ($id) {
        return $id;
    } else {
        return false;
    }
}